//
//  CommonRealmModels.swift
//  PEXit
//
//  Created by Md Ismail on 8/4/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation
import Realm
import RealmSwift
import ObjectMapper


class RealmString: Object {
    @objc dynamic var stringValue = ""
}

class ProfileRLMModel: Object, Mappable {
    
    @objc dynamic var uid: Int = Int()
    @objc dynamic var userName: String?
    @objc dynamic var profileTitle: String?
    @objc dynamic var profileViews: Int = Int()
    @objc dynamic var profileConnections: Int = Int()
    @objc dynamic var profilePendings: Int = Int()
    @objc dynamic var pimage:String?
    @objc dynamic var userEmail:String?
   
    override class func primaryKey() -> String? {
        return "uid"
    }
    convenience required init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        uid <- map["uid"]
        userName <- map["userName"]
        profileTitle <- map["profileTitle"]
        profileViews <- map["profileViews"]
        profileConnections <- map["profileConnections"]
        profilePendings <- map["profilePendings"]
        pimage <- map["pimage"]
        userEmail <- map["userEmail"]
    }
    
    
    //    MARK: fetch and delete functions
    
    static func fetchProfile() -> ProfileRLMModel?{
        let  profile = RealmDBManager.sharedManager.fetchObject(object: self)
        if profile != nil{
            return profile as! ProfileRLMModel
        }
        return nil
    }
    
    static func deleteAll(){
        if let realm = RealmDBManager.sharedManager.getRealm() {
            do {
                try realm.write() {
                    realm.delete(realm.objects(ProfileRLMModel.self))
                    realm.refresh()
                }
            } catch (let error as NSError) {
                
                print("ERROR: Realm: writing to database, ERROR: \(error.localizedDescription)")
            }
        }
    }
    
    static func deleteAll_withUId(Id:String){
        let pred = NSPredicate(format: "id == %@",Id)
        let objectsToDelete = RealmDBManager.sharedManager.fetchObjectsWithPredicate(object: ProfileRLMModel.self, pred: pred)
        if objectsToDelete != nil && (objectsToDelete?.count)! > 0{
            
            
            if let realm = RealmDBManager.sharedManager.getRealm() {
                do {
                    try realm.write() {
                        realm.delete(objectsToDelete!)
                        realm.refresh()
                    }
                } catch (let error as NSError) {
                    
                    print("ERROR: Realm: writing to database, ERROR: \(error.localizedDescription)")
                }
            }
        }
    }
}


class SharedUserModel : Object, Mappable{
    @objc dynamic var name: String?
    @objc dynamic var email: String?
    
    convenience required init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        name <- map["name"]
        email <- map["email"]
    }
    
}

class ListTransform<T:RealmSwift.Object> : TransformType where T:Mappable {
    typealias Object = List<T>
    typealias JSON = [AnyObject]
    
    let mapper = Mapper<T>()
    
    func transformFromJSON(_ value: Any?) -> Object? {
        let results = List<T>()
        if let objects = mapper.mapArray(JSONObject: value) {
            for object in objects {
                results.append(object)
            }
        }
        return results
    }
    
    func transformToJSON(_ value: Object?) -> JSON? {
        var results = [AnyObject]()
        if let value = value {
            for obj in value {
                let json = mapper.toJSON(obj)
                results.append(json as AnyObject)
            }
        }
        return results
    }
}

class PostRLMModel: Object, Mappable {
    @objc dynamic var id: Int = Int()
    @objc dynamic var suid: Double = Double()
    @objc dynamic var spid: Double = Double()
    @objc dynamic var uid: Int = Int()
    @objc dynamic var edited: Double = Double()
    @objc dynamic var commentCount: Int = Int()
    @objc dynamic var likeCount: Int = Int()
    @objc dynamic var likeStatus: Int = Int()
    @objc dynamic var hide_id: Int = Int()
    @objc dynamic var template: Int = Int()
    @objc dynamic var uniqid: String?
    @objc dynamic var spname: String?
//    @objc dynamic var edited:Int = Int()
//    @objc dynamic var likeCount: Int = Int()
//    @objc dynamic var likeStatus: Int = Int()
//    @objc dynamic var commentCount: Int = Int()
   
//    @objc dynamic var template: Double = Double()
    @objc dynamic var name: String?
    @objc dynamic var title: String?
    @objc dynamic var post: String?
    @objc dynamic var checkTime: String?
    @objc dynamic var profileImage: String?
    @objc dynamic var defaultTimeZone: String?
    @objc dynamic var video: String?
    @objc dynamic var youtube: String?
    @objc dynamic var link: String?
    @objc dynamic var url: String?
    @objc dynamic var shareWithKey: String?
    @objc dynamic var shareWithValue: String?
    @objc dynamic var timeAgo: String?
    @objc dynamic var imageHeight: Double = Double()
//    var individualUsers : List<SharedUserModel>?
     var individualUsers = List<SharedUserModel>()
    var docu: [String] {
        get {
            return _docu.map { $0.stringValue }
        } set {
            _docu.removeAll()
            _docu.append(objectsIn: newValue.map({ RealmString(value: [$0]) }))
        }
    }
    private let _docu = List<RealmString>()
    
    
    
    
    
    var image: [String] {
        get {
            return _image.map { $0.stringValue }
        } set {
            _image.removeAll()
            _image.append(objectsIn: newValue.map({ RealmString(value: [$0]) }))
        }
    }
    private let _image = List<RealmString>()
    
    
    var ppt: [String] {
        get {
            return _ppt.map { $0.stringValue }
        } set {
            _ppt.removeAll()
            _ppt.append(objectsIn: newValue.map({ RealmString(value: [$0]) }))
        }
    }
    private let _ppt = List<RealmString>()
    
    var allvid: [String] {
        get {
            return _allvid.map { $0.stringValue }
        } set {
            _allvid.removeAll()
            _allvid.append(objectsIn: newValue.map({ RealmString(value: [$0]) }))
        }
    }
    private let _allvid = List<RealmString>()
    
    convenience required init?(map: Map) {
        self.init()
    }
    //TESTING IF PRIMARY KEY IS WORKING/NOT
  /*  override static func primaryKey() -> String? {
        return "uniqid"
    } */
    
    override class func primaryKey() -> String? {
        return "uniqid"
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        suid <- map["suid"]
        spid <- map["spid"]
        uid <- map["uid"]
        edited <- map["edited"]
        
        commentCount <- map["commentCount"]
        likeCount <- map["likeCount"]
        likeStatus <- map["likeStatus"]
        hide_id <- map["hide_id"]
        template <- map["template"]
        spname <- map["spname"]
//        edited <- map["edited"]
        name <- map["name"]
        title <- map["title"]
        post <- map["post"]
        checkTime <- map["checkTime"]
        profileImage <- map["profileImage"]
        url <- map["url"]
        docu <- map["docu"]
        image <- map["image"]
        ppt <- map["ppt"]
        allvid <- map["allvid"]
        video <- map["video"]
        link <- map["link"]
        uniqid <- map["uniqid"]
        defaultTimeZone <- map["defaultTimeZone"]
        youtube <- map["youtube"]
        shareWithKey <- map["shareWithKey"]
        shareWithValue <- map["shareWithValue"]
        individualUsers <- (map["indmail"], ListTransform<SharedUserModel>())
        timeAgo <- map["timeAgo"]
        //individualUsers <- map["indmail"]
        
//        videov   not  parsed
//        link  not  parsed
    }
    
    
    //    MARK: fetch and delete functions
    static func deleteAll(){
        if let realm = RealmDBManager.sharedManager.getRealm() {
            do {
                try realm.write() {
                    realm.delete(realm.objects(PostRLMModel.self))
                    realm.refresh()
                }
            } catch (let error as NSError) {
                
                print("ERROR: Realm: writing to database, ERROR: \(error.localizedDescription)")
            }
        }
    }
    
    static func deleteAll_withPId(Id:Int){
        let pred = NSPredicate(format: "id == %d",Id)
        let objectsToDelete = RealmDBManager.sharedManager.fetchObjectsWithPredicate(object: PostRLMModel.self, pred: pred)
        if objectsToDelete != nil && (objectsToDelete?.count)! > 0{
            
            
            if let realm = RealmDBManager.sharedManager.getRealm() {
                do {
                    try realm.write() {
                        realm.delete(objectsToDelete!)
                        realm.refresh()
                    }
                } catch (let error as NSError) {
                    
                    print("ERROR: Realm: writing to database, ERROR: \(error.localizedDescription)")
                }
            }
        }
    }
    
    static func fetchPostModels() -> [PostRLMModel]{
        var  postModels = RealmDBManager.sharedManager.fetchObjects(object: self) as! [PostRLMModel]
//        postModels = postModels.sorted(by: { $0.id > $1.id })

        return postModels
    }
}
