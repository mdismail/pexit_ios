//
//  LoginModel.swift
//  PEXit
//
//  Created by Apple on 25/07/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class LoginModel: NSObject {

}

struct LoginRequest{
    var username : String?
    var password : String?
    var slogin_id : String?
    var email : String?
}

struct LoginResponse : Codable{
    var datas : LoginViewModel?
    var message : String?
    var status : Bool?
    var token : String?
   
}

struct LoginViewModel : Codable{
//    var profileViews : String?
//    var profilePendings : String?
//    var profileConnections : String?
    var profileTitle : String?
    var userName : String?
}

struct StaticUrlsModel : Codable{
    var status : Bool?
    var message : String?
    var datas : UrlListModel?
}

struct UrlListModel : Codable{
    var about : String?
    var faq : String?
    var terms : String?
    var pricing : String?
}

struct TransactionHistoryModel : Codable{
    var status : Bool?
    var message : String?
    var datas : [TransactionListModel]?
}

struct TransactionListModel : Codable{
    var orderid : String?
    var amount : String?
    var tax : String?
    var currency : String?
    var trans_date : String?
    var order_status : String?
    var services : String?
    var id : Int?
    var serid : Int?
    var shortName : String?
}
