//
//  AllBeans.swift
//  PEXit
//
//  Created by Apple on 11/07/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation
import UIKit


//MARK: For TJTextfield
struct TextfieldInfoStruct {
    
    var leftImage,rightImage: UIImage?
    var placeholderText,textfieldText: String?
    var leftView: UIView?

    init(leftImage: UIImage? = nil, rightImage: UIImage? = nil ,placeholderText: String? = nil ,textfieldText: String? = nil, leftView: UIView? = nil) {
        self.leftImage = leftImage
        self.rightImage = rightImage
        self.placeholderText = placeholderText
        self.textfieldText = textfieldText
        if leftView != nil{
                self.leftView = leftView
        }
    }
    
    mutating func updateValues(textfieldText: String?) -> TextfieldInfoStruct {
        self.textfieldText = textfieldText
        return self
    }
}


struct UserInfo {
    var profilePicId :String?
    var userId: Int?
    var headingValue, subHeadingValue: String?
    var attributedHeadingValue, attributedSubHeadingValue: NSAttributedString?
    var editProfileLblHidden = true
    
    init(profilePicId: String? = "",userId: Int? = nil, headingValue: String? = nil,subHeadingValue: String? = nil,attributedHeadingValue: NSAttributedString? = nil ,attributedSubHeadingValue: NSAttributedString? = nil, editProfileLblHidden: Bool? = true) {
        self.profilePicId = profilePicId
        self.userId = userId
        self.headingValue = headingValue
        self.subHeadingValue = subHeadingValue
        self.attributedHeadingValue = attributedHeadingValue
        self.attributedSubHeadingValue = attributedSubHeadingValue
        self.editProfileLblHidden = editProfileLblHidden!
    }
}


struct MultiLineButtonStruct{
    var headerText : String?
    var subHeaderText : String?
    var alignment : NSTextAlignment?
    var paragraphSpacing : CGFloat?
    init(headerText : String? = nil , subHeaderText : String? = nil ,alignment : NSTextAlignment? = .center , paragraphSpacing : CGFloat? = 10){
        self.headerText = headerText
        self.subHeaderText = subHeaderText
        self.alignment = alignment
        self.paragraphSpacing = paragraphSpacing
    }
    
}
