//
//  ItemMemoryStore.swift
//  PurchaseList
//
//  Created by Cesar Brenes on 11/1/16.
//  Copyright © 2016 Cesar Brenes. All rights reserved.
//

import UIKit

class ItemsSingleton: NSObject{
    static let sharedInstance = ItemsSingleton()
    var items = [TextfieldInfoStruct]()
//    var items = [TextfieldInfoStruct(leftImage: #imageLiteral(resourceName: "userRed"), rightImage: #imageLiteral(resourceName: "downArrow"), placeholderText: "abcd"),TextfieldInfoStruct(leftImage: #imageLiteral(resourceName: "envelope"), rightImage: #imageLiteral(resourceName: "downArrow"), placeholderText: "adfhjfashjd"),TextfieldInfoStruct(leftImage: #imageLiteral(resourceName: "displayName"), rightImage: #imageLiteral(resourceName: "downArrow"), placeholderText: "dfsfhj"),TextfieldInfoStruct(leftImage: #imageLiteral(resourceName: "lock"), rightImage: #imageLiteral(resourceName: "downArrow"), placeholderText: "dsdsfds"),TextfieldInfoStruct(leftImage: #imageLiteral(resourceName: "company"), rightImage: #imageLiteral(resourceName: "downArrow"), placeholderText: "fdsfdsjhg")]  //no need to set here ,until & unless u r having the same data throughout the app.
}


class ItemMemoryStore: ItemStoreProtocol {
   
    
    

    var items: [TextfieldInfoStruct] =  ItemsSingleton.sharedInstance.items
    
    func fetchAllItems(completionHandler:  (_ itemsArray: [TextfieldInfoStruct], _ error: ItemStoreError?) -> Void){
        completionHandler(ItemsSingleton.sharedInstance.items, nil)
       
    }
    
    func fetchItem(id: NSNumber, completionHandler: (TextfieldInfoStruct?, ItemStoreError?) -> Void) {
        
    }
    
    func deleteItem(id: NSNumber, completionHandler: (ItemStoreError?) -> Void) {
        
    }
    
    func createItem(item: TextfieldInfoStruct, completionHandler: (ItemStoreError?) -> Void) {
        
    }
    
    func updateItem(item: TextfieldInfoStruct, completionHandler: (ItemStoreError?) -> Void) {
        
    }
    
    /*
    func fetchItem(id: NSNumber, completionHandler:  (_ item: Item?, _ error: ItemStoreError?) -> Void){
        let result = items.filter({$0.id == id})
        completionHandler(result.first, nil)
    }
    
    func deleteItem(id: NSNumber, completionHandler: (_ error: ItemStoreError?) -> Void){
        if let index = getObjectIndex(id: id){
            items.remove(at: index)
            ItemsSingleton.sharedInstance.items = items
            completionHandler(nil)
        }
        else{
            completionHandler(ItemStoreError.CannotDelete("The id doesn't exist"))
        }
    }
    
    func createItem(item: Item, completionHandler:  (_ error: ItemStoreError?) -> Void){
        ItemsSingleton.sharedInstance.items.append(item)
        items = ItemsSingleton.sharedInstance.items
        completionHandler(nil)
    }
    
    
    func updateItem(item: Item, completionHandler: (_ error: ItemStoreError?) -> Void){
        if let index = getObjectIndex(id: item.id){
            items[index] = item
            ItemsSingleton.sharedInstance.items = items
            completionHandler(nil)
        }
        else{
            completionHandler(ItemStoreError.CannotUpdate("The id doesn't exist"))
        }
    }
    
    private func getObjectIndex(id: NSNumber?) -> Int?{
        if let id = id{
            let result = items.filter({$0.id == id})
            if result.count == 0{
                return nil
            }
            let index = items.index(of: result.first!)
            return index
        }
        return nil
    }
    
    */
    
    

}
