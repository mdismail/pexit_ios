//
//  AppDelegate.swift
//  PEXit
//
//  Created by Apple on 07/07/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import GoogleSignIn
import LinkedinSwift
import UserNotifications
//import FirebaseCore
//import FirebaseAnalytics
import Firebase
import FirebaseMessaging
//import FirebaseInstanceID
//import Braintree
import Fabric
import Crashlytics


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate , UNUserNotificationCenterDelegate , FIRMessagingDelegate{
    
    var window: UIWindow?
    var isGotPushNotification = false
    var pushDict: [AnyHashable : Any]?
    var currentViewController = UIViewController()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        _ = RealmDBManager.sharedManager.getRealm();
        IQKeyboardManager.shared().isEnabled = true

       // GIDSignIn.sharedInstance().clientID = "876834577966-k8e0eesnrfft210a0uaq29doi06o56ld.apps.googleusercontent.com"
         GIDSignIn.sharedInstance().clientID = "306263045400-8p7c63p310pj0hieb2qdh0kmf708pmaq.apps.googleusercontent.com"
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        if Defaults.getLoginStatus() == true{
            let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let controller = storyboard.instantiateViewController(withIdentifier: "TabBarController")
            controller.modalTransitionStyle = .crossDissolve
            window?.rootViewController = controller
            window?.makeKeyAndVisible()
        }
        if let notification = launchOptions?[.remoteNotification] as? [String: AnyObject] {
            
            // If your app wasn’t running and the user launches it by tapping the push notification, the push notification is passed to your app in the launchOptions
            
            let dict = notification["aps"] as! [String: AnyObject]
//            print(dict)
          //  KEY_WINDOW?.makeToast("Entered here")
            if let alertDict = dict["alert"] as?  [String: Any]{
//                 KEY_WINDOW?.makeToast(alertDict["title"]  as! String)
               // handleNotification(title:alertDict["title"] as! String)
                if let target = notification["targetScreen"] as? String{
//                    print("target is--->",target)
                    handleNotification(title:target)
                }
            }
         //   UIApplication.shared.applicationIconBadgeNumber = 0
        }
        
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) {
            [weak self] (granted, error) in
            if error != nil{
                
            }else{
                UNUserNotificationCenter.current().delegate = self
                FIRMessaging.messaging().remoteMessageDelegate = self
                DispatchQueue.main.async{
                    application.registerForRemoteNotifications()
                    FIRApp.configure()
                    
                }
            }
        }
        NotificationCenter.default.addObserver(self,selector: #selector(self.tokenRefreshNotification),name: NSNotification.Name.firInstanceIDTokenRefresh,object: nil)
        
        UINavigationBar.appearance().barTintColor = UIColor.black
//        BTAppSwitch.setReturnURLScheme(BRAINTREE_IDENTIFIER)
        Fabric.sharedSDK().debug = true
        Fabric.with([Crashlytics.self])
        return true
    }
    
    func connectToFCM() {
        // Won't connect since there is no token
        
        guard FIRInstanceID.instanceID().token() != nil else {
            return
        }
        tokenRefreshNotification()
        // Disconnect previous FCM connection if it exists.
        FIRMessaging.messaging().disconnect()
        FIRMessaging.messaging().connect { (error) in
            if error != nil {
                print("Unable to connect with FCM. \(error?.localizedDescription ?? "")")
            } else {
                print("Connected to FCM.")
            }
        }
    }
    
    
    func messaging(_ messaging: FIRMessaging, didReceiveRegistrationToken fcmToken: String) {
        let newToken = FIRInstanceID.instanceID().token()
        if newToken != nil{
            connectToFCM()
        }
        
    }
    
    func messaging(_ messaging: FIRMessaging, didRefreshRegistrationToken fcmToken: String) {
        print("Refreshed Token: \(fcmToken)")
    }
    
    func navigateToNotificationsScreen(navigationController : UINavigationController,isMessages : Bool? = false){
        let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let notificationController = mainStoryboard.instantiateViewController(withIdentifier: "notificationParentViewController") as! NotificationParentViewController
        
        if isMessages == true{
            notificationController.isFromMessageNotification = true
        }else{
            notificationController.isFromNotification = true
        }
        navigationController.pushViewController(notificationController, animated: true)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        
        // If your app was running and in the foreground
        // Or
        // If your app was running or suspended in the background and the user brings it to the foreground by tapping the push notification
        
//        print("didReceiveRemoteNotification \(userInfo)")
        //  FIRMessaging.messaging().appDidReceiveMessage(userInfo)
        guard let dict = userInfo["aps"]  as? [String: Any] else {
            print("Notification Parsing Error")
            return
        }
        print("DICT IS-->",dict)
        if let target = userInfo["targetScreen"] as? String{
//            print("target is--->",target)
            handleNotification(title:target as! String)
        }
//        if let alertDict = dict["alert"] as?  [String: Any]{
//             handleNotification(title:target as! String)
//        }
       
        
       
        
    }
    
    
    func handleNotification(title : String){
//        1. post
//        2. friend
//        3. group
//        4. message
        if title == "friend" || title == "group"{
            navigateToNotificationScreen()
        }
        else if title.hasSuffix("message"){
            navigateToMessageScreen()
        }else{
            navigateToHomeScreen()
        }
    }
    
    func navigateToHomeScreen(){
//         print("HOME")
        if let currentController = window?.rootViewController{
            if currentController is UITabBarController{
                let tabBarChildControllers = currentController.childViewControllers
                if  (currentController as! UITabBarController).selectedIndex != 0{
                    (currentController as! UITabBarController).selectedIndex = 0
                    if tabBarChildControllers[0] is UINavigationController && (tabBarChildControllers[0] as! UINavigationController).viewControllers.count > 0{
                        (tabBarChildControllers[0] as!
                           UINavigationController).popToRootViewController(animated: true)
                       
                    }
                }else{
                    if tabBarChildControllers[0] is UINavigationController && (tabBarChildControllers[0] as! UINavigationController).viewControllers.count > 0{
                        
                        let currentController = (tabBarChildControllers[0] as! UINavigationController).topViewController
                        if currentController is HomePageViewController{
                            (currentController as! HomePageViewController).isFromNotification = true
                        }else{
                            let parentController =  (tabBarChildControllers[0] as!
                                UINavigationController).viewControllers[0]
                            if parentController is HomePageViewController{
                                (parentController as! HomePageViewController).isFromNotification = true
                                (tabBarChildControllers[0] as!
                                    UINavigationController).popToRootViewController(animated: true)
                            }
                           
                        }
                       
                        
                    }
                }
                
            }
        }
    }
    
    
    func navigateToMessageScreen(){
//        print("MESSAGE")
        if let currentController = window?.rootViewController{
            if currentController is UITabBarController{
                let tabBarChildControllers = currentController.childViewControllers
                if  (currentController as! UITabBarController).selectedIndex != 0{
                    (currentController as! UITabBarController).selectedIndex = 0
                    if tabBarChildControllers[0] is UINavigationController && (tabBarChildControllers[0] as! UINavigationController).viewControllers.count > 0{
                        let currentController = (tabBarChildControllers[0] as! UINavigationController).topViewController
                        if currentController is NotificationParentViewController{
                            (currentController as! NotificationParentViewController).isFromMessageNotification = true
                             (currentController as! NotificationParentViewController).updateViewForNotification()
                        }else{
                            (tabBarChildControllers[0] as!
                                UINavigationController).popToRootViewController(animated: true)
                            navigateToNotificationsScreen(navigationController: (tabBarChildControllers[0] as! UINavigationController),isMessages: true)
                        }
                        
                    }
                }else{
                    if tabBarChildControllers[0] is UINavigationController && (tabBarChildControllers[0] as! UINavigationController).viewControllers.count > 0{
                        
                        let currentController = (tabBarChildControllers[0] as! UINavigationController).topViewController
                        //                        if currentController is HomePageViewController{
                        //                            (currentController as! HomePageViewController).isFromNotification = true
                        //                        }
                        if currentController is NotificationParentViewController{
                            (currentController as! NotificationParentViewController).isFromMessageNotification = true
                            (currentController as! NotificationParentViewController).updateViewForNotification()
                        }else{
                            navigateToNotificationsScreen(navigationController: (tabBarChildControllers[0] as! UINavigationController),isMessages: true)
                        }
                        
                    }
                }
                
            }
        }
    }
    
    func navigateToNotificationScreen(){
        if let currentController = window?.rootViewController{
            if currentController is UITabBarController{
                let tabBarChildControllers = currentController.childViewControllers
                if  (currentController as! UITabBarController).selectedIndex != 0{
                    (currentController as! UITabBarController).selectedIndex = 0
                    if tabBarChildControllers[0] is UINavigationController && (tabBarChildControllers[0] as! UINavigationController).viewControllers.count > 0{
                         let currentController = (tabBarChildControllers[0] as! UINavigationController).topViewController
                        if currentController is NotificationParentViewController{
                            (currentController as! NotificationParentViewController).isFromNotification = true
                            (currentController as! NotificationParentViewController).updateViewForNotification()
                        }else{
                            (tabBarChildControllers[0] as!
                                UINavigationController).popToRootViewController(animated: true)
                            navigateToNotificationsScreen(navigationController: (tabBarChildControllers[0] as! UINavigationController))
                        }
                      
                    }
                }else{
                    if tabBarChildControllers[0] is UINavigationController && (tabBarChildControllers[0] as! UINavigationController).viewControllers.count > 0{
                        
                        let currentController = (tabBarChildControllers[0] as! UINavigationController).topViewController
//                        if currentController is HomePageViewController{
//                            (currentController as! HomePageViewController).isFromNotification = true
//                        }
                        if currentController is NotificationParentViewController{
                            (currentController as! NotificationParentViewController).isFromNotification = true
                            (currentController as! NotificationParentViewController).updateViewForNotification()
                        }else{
                             navigateToNotificationsScreen(navigationController: (tabBarChildControllers[0] as! UINavigationController))
                        }
                        
                    }
                }
                
            }
        }
    }
    
    
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        // Print message ID.
       // print("Message ID: \(userInfo["gcm.message_id"]!)")
        
        // Print full message.
        print("%@", userInfo)
        completionHandler(.alert)
    }
    
    func application(_ application: UIApplication, handleActionWithIdentifier identifier: String?, forRemoteNotification userInfo: [AnyHashable : Any], withResponseInfo responseInfo: [AnyHashable : Any], completionHandler: @escaping () -> Void) {
//        print("identifier = \(identifier)")
        if let currentController = window?.rootViewController{
            if currentController is UITabBarController{
                (currentController as! UITabBarController).selectedIndex = 0
            }
        }
        
    }
    
    func applicationReceivedRemoteMessage(_ remoteMessage: FIRMessagingRemoteMessage) {
        // message
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        let tokenParts = deviceToken.map { data -> String in
            return String(format: "%02.2hhx", data)
        }
        
        let token = tokenParts.joined()
        print("Device Token: \(token)")
        
        //NEED TO CHANGE AT TIME OF PRODUCTION
        FIRInstanceID.instanceID().setAPNSToken(deviceToken, type: .prod)
        let fcmToken = FIRInstanceID.instanceID().token()
        print("FCM TOKEN IS ---->", fcmToken)
        if fcmToken != nil{
            connectToFCM()
        }
        
        //UserDefaults.standard.set(token, forKey: DEVICE_TOKEN)
    }
    
    @objc func tokenRefreshNotification(){
      //  let worker = SettingsWorker()
        let fcmToken = FIRInstanceID.instanceID().token()
        if fcmToken != nil{
            
           //  print("FCM TOKEN IS ----->",fcmToken)
             Defaults.saveFcmToken(token:fcmToken ?? "")
            if Defaults.getAccessToken() != ""{
                let params = ["deviceType":"ios","fcmToken":Defaults.getFcmToken()]
                let worker = SettingsWorker()
                worker.updateFCMToken(params: params)
            }
       
             //worker.updateFCMToken(params:params as! [String : String])
        }
       
    }
    
    
    func applicationWillResignActive(_ application: UIApplication) {
        
    }
    
     func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool{
       
        if LinkedinSwiftHelper.shouldHandle(url) {
            return LinkedinSwiftHelper.application(application, open: url, sourceApplication:sourceApplication , annotation: annotation)
        }
//        if url.scheme?.localizedCaseInsensitiveCompare(BRAINTREE_IDENTIFIER) == .orderedSame {
//            return BTAppSwitch.handleOpen(url, sourceApplication: sourceApplication)
//        }
        return FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
    }
    
    // support iOS 7 or 8, add the following method.
    
   /* func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        if url.scheme?.localizedCaseInsensitiveCompare(BRAINTREE_IDENTIFIER) == .orderedSame {
            return BTAppSwitch.handleOpen(url, sourceApplication: sourceApplication)
        }
        return false
    } */
    
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    //MARK:- For PrintingFontFamilies
    func printFontFamilies() {
        //        For printing the whole font families
        let fontFamilies = UIFont.familyNames
        for i in 0..<fontFamilies.count {
            let fontFamily = fontFamilies[i]
            let fontNames = UIFont.fontNames(forFamilyName: fontFamilies[i])
            print("\(fontFamily): \(fontNames)")
        }
    }
    
    
    //MARK:- For Shared Delegate
    class func sharedDelegate() -> AppDelegate{
        return UIApplication.shared.delegate as! AppDelegate
    }
    
}



