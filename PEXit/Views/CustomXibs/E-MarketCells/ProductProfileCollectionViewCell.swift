//
//  ProductProfileCollectionViewCell.swift
//  PEXit
//
//  Created by Apple on 11/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class ProductProfileCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var imgViewProduct: UIImageView!
   // @IBOutlet weak var lblProductDesc: UILabel!
    
    
    func setProductData(productModel : SupplierObjectModel){
        lblProductName.text = productModel.name
       // lblProductDesc.text = productModel.descrip?.htmlDecodedText()
        if let imageURL = URL(string:(productModel.image?.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!)!){
                    imgViewProduct.sd_setImage(with: imageURL, completed: nil)
                }
            }
    }
