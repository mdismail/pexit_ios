//
//  ProductDetailCell.swift
//  PEXit
//
//  Created by Apple on 11/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import WebKit
import PDFKit
import SDWebImage

protocol ProductDetailTapped{
    func btnTapped(section : Int)
}

class ProductDetailCell: UITableViewCell , WKUIDelegate{
    @IBOutlet weak var imageViewPdfThumbnail: UIImageView!
    @IBOutlet weak var btnViewDetail: UIButton!
    @IBOutlet weak var collectionViewGallery: UICollectionView!
    var delegate : ProductDetailTapped?
  
 //   @IBOutlet weak var pdfActivityIndicator: UIActivityIndicatorView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    @IBAction func btnTapFullViewTapped(_ sender: UIButton) {
        if let delegate = delegate{
            delegate.btnTapped(section: sender.tag)
        }
    }
    
    
    override func prepareForReuse() {
         self.imageViewPdfThumbnail.image = UIImage.init(named: "pdf")
    }
    
    func setData(urlString : String?){
        if urlString != nil{
            if let imageURL = URL(string:(urlString?.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!)!){
             let frameSize = self.frame.size.width
//                self.pdfActivityIndicator.isHidden = false
//                self.pdfActivityIndicator.startAnimating()
                DispatchQueue.global(qos: .background).async {
                    let image =  self.pdfThumbnail(url: imageURL,width:frameSize)
                 
                    DispatchQueue.main.async {
//                        self.pdfActivityIndicator.stopAnimating()
                        self.imageViewPdfThumbnail.image = image
                    }
                }
           }
        }
        
    }
    
    func pdfThumbnail(url: URL, width: CGFloat = 100) -> UIImage? {
        guard let data = try? Data(contentsOf: url),
            let page = PDFDocument(data: data)?.page(at: 0) else {
                return nil
        }
        
        let pageSize = page.bounds(for: .mediaBox)
        let pdfScale = width / pageSize.width
        
        // Apply if you're displaying the thumbnail on screen
        let scale = UIScreen.main.scale * pdfScale
        let screenSize = CGSize(width: pageSize.width * scale, height: pageSize.height * scale)
        
        return page.thumbnail(of: screenSize, for: .mediaBox)
    }

}

