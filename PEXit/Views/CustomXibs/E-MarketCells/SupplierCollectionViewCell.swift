//
//  SupplierCollectionViewCell.swift
//  PEXit
//
//  Created by Apple on 10/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class SupplierCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var lblProduct: UILabel!
    
    func setSupplierProductData(productModel : SupplierObjectModel){
        lblProduct.text = productModel.name
        if let imageUrl = productModel.image{
            let imageURL = URL(string:(imageUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!))
            imgProduct.sd_setImage(with: imageURL, placeholderImage: nil)

           // imgProduct.setImage(from: imageURL!)
            
        }
    }
    
}
