//
//  EMarketProductCell.swift
//  PEXit
//
//  Created by Apple on 02/08/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class EMarketProductCell: UITableViewCell {

    @IBOutlet weak var lblProduct: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblCategory: UILabel!
//    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var imgProduct: CachedImageView!
//    @IBOutlet weak var btnContactSupplier: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func prepareForReuse() {
     //   imgProduct.image = nil
        
    }

    @IBAction func btnContactSupplierClicked(_ sender: Any) {
    }
    
    func setProductData(productModel : ProductSupplierModel){
//        imgProduct.image = nil
        lblProduct.text = productModel.name
        lblLocation.text = productModel.location
        lblCategory.text = productModel.category
//        lblDescription.text = productModel.descrip?.htmlDecodedText()
        if let imageUrl = productModel.image{
            let imageURL = URL(string:(imageUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!))
//            imgProduct.sd_setImage(with: imageURL, placeholderImage: UIImage.init(named: "Header"))
            imgProduct.sd_setImage(with: imageURL, placeholderImage: nil)
           // imgProduct.loadImage(urlString: imageURL?.absoluteString ?? "")
//            if imageURL != nil{
//                imgProduct.setImage(from: imageURL!)
//            }
            
        }
    }
}
