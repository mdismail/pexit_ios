//
//  SupplierTableViewCell.swift
//  PEXit
//
//  Created by Apple on 10/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class SupplierTableViewCell: UITableViewCell , UICollectionViewDataSource , UICollectionViewDelegate , UICollectionViewDelegateFlowLayout{
    
    @IBOutlet weak var lblSupplierName: UILabel!
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var lblCategory: UILabel!
//    @IBOutlet weak var btnContactSupplier: UIButton!
    @IBOutlet weak var collectionViewProducts: UICollectionView!
    var currentSupplier : SupplierResponseModel?
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionViewProducts.register(UINib(nibName: "SupplierCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "supplierCollectionViewCell")
        collectionViewProducts.dataSource = self
        collectionViewProducts.delegate = self
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setSupplierData(supplierResponse : SupplierResponseModel){
        lblSupplierName.text = supplierResponse.name
        lblCategory.text =  "Location: \(supplierResponse.location ??? "")"
        lblProductName.text = "Products: \(supplierResponse.products ??? "")"
        currentSupplier = supplierResponse
        collectionViewProducts.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return currentSupplier?.productList?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let supplierProductCell = collectionView.dequeueReusableCell(withReuseIdentifier: "supplierCollectionViewCell", for: indexPath) as! SupplierCollectionViewCell
        supplierProductCell.setSupplierProductData(productModel:(currentSupplier?.productList![indexPath.row])!)
        return supplierProductCell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            return CGSize.init(width: 130 , height: collectionView.frame.size.height)
    }
}
