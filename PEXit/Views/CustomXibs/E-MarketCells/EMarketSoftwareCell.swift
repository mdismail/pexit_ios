//
//  EMarketSoftwareCell.swift
//  PEXit
//
//  Created by Apple on 03/08/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import TTTAttributedLabel
protocol SoftwareCellDelegate {
//    func pauseTapped(_ cell: EMarketSoftwareCell)
//    func resumeTapped(_ cell: EMarketSoftwareCell)
    func cancelTapped(_ cell: EMarketSoftwareCell)
    func downloadTapped(_ cell: EMarketSoftwareCell)
}


class EMarketSoftwareCell: UITableViewCell {
    var delegate: SoftwareCellDelegate?
    @IBOutlet weak var imageViewEMarket: CachedImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: TTTAttributedLabel!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    //    @IBOutlet weak var btnDownload: UIButton!
//    @IBOutlet weak var progressView: UIProgressView!
//    @IBOutlet weak var heightConstraintStackView: NSLayoutConstraint!
    
 //   @IBOutlet weak var btnReadMore: UIButton!
  //  @IBOutlet weak var btnCancel: UIButton!
//    @IBOutlet weak var btnPause: UIButton!
   // @IBOutlet weak var lblProgressBar: UILabel!
    @IBOutlet weak var lblServiceLocation: UILabel!
    var softwareArray : [SoftwareModel]?
    var expandingDelegate: ExpandingDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    
//    @IBAction func btnPauseClicked(_ sender: Any) {
//        if(btnPause.titleLabel!.text == "Pause") {
//            delegate?.pauseTapped(self)
//        } else {
//            delegate?.resumeTapped(self)
//        }
//    }
    
    @IBAction func btnCancelClicked(_ sender: Any) {
        delegate?.cancelTapped(self)
    }
    
    @IBAction func btnDownloadTapped(_ sender: Any) {
        delegate?.downloadTapped(self)
    }
    
    
    func configure(softwareModel: SoftwareModel, downloaded: Bool, download: Download?) {
         lblTitle.text = softwareModel.name
        bottomConstraint.constant = 10
//        btnReadMore.isHidden = true
//         lblDescription.text = softwareModel.descrip?.htmlDecodedText()
//         if softwareModel.translatedDescrip != nil{
//            lblDescription.text = softwareModel.translatedDescrip
//    }
        lblDescription.numberOfLines = 2
        let attributedString = NSAttributedString(string: "  Read More...", attributes: [
            .font: UIFont(name: "SFUIDisplay-Bold", size: 10)!,
            .foregroundColor: UIColor.red
            ])
        lblDescription.attributedTruncationToken = attributedString
        lblDescription.text = softwareModel.descrip?.htmlDecodedText()
//        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(HomeTableViewCell.descriptionTextTapped(_:)))
//        tapGesture.numberOfTapsRequired = 1
//        tapGesture.numberOfTouchesRequired = 1
//        lblPostDesc.addGestureRecognizer(tapGesture)
        
     //    lblDescription.numberOfLines = softwareModel.isExpanded! ? 0 : 3
//         if softwareModel.descrip != nil{
//            changeReadMoreVisibility(inputStr:lblDescription.text!,currentSender: btnReadMore.tag)
//        }
    
//         btnReadMore.setTitle(softwareModel.isExpanded! ? "Read less..." : "Read more...", for: .normal)
         if let imageUrl = softwareModel.logo{
            let imageURL = URL(string:(imageUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!))
//            imageViewEMarket.loadImage(urlString: imageURL?.absoluteString ?? "")
//            imageViewEMarket.sd_setImage(with: imageURL, placeholderImage: UIImage.init(named: "Header"))
            imageViewEMarket.sd_setImage(with: imageURL, placeholderImage: nil)
           // imageViewEMarket.setImage(from: imageURL!)
        }
        
        // Download controls are Pause/Resume, Cancel buttons, progress info
        var showDownloadControls = false
        // Non-nil Download object means a download is in progress
        if download != nil {
            showDownloadControls = true
//            lblProgressBar.text = (download?.isDownloading)! ? "Downloading..." : "Paused"
        }
//        btnCancel.isHidden = !showDownloadControls
//        progressView.isHidden = !showDownloadControls
//        lblProgressBar.isHidden = !showDownloadControls
       
        // If the track is already downloaded, enable cell selection and hide the Download button
        selectionStyle = downloaded ? UITableViewCellSelectionStyle.gray : UITableViewCellSelectionStyle.none
       // btnDownload.isHidden = downloaded || showDownloadControls
//        if (softwareModel.software?.isEmpty)!{
//            btnDownload.isHidden = true
//        }else{
//            btnDownload.isHidden = false
//        }
    }
    
    @IBAction func readMoreClicked(_ sender: UIButton) {
//        if softwareArray != nil{
//            let currentModel = softwareArray![sender.tag]
//            currentModel.isExpanded = !currentModel.isExpanded!
//            lblDescription.numberOfLines = currentModel.isExpanded! ? 0 : 3
//            btnReadMore.setTitle(currentModel.isExpanded! ? "Read less..." : "Read more...", for: .normal)
//            expandingDelegate?.moreTapped(cell: self)
//        }
    }
    
    
    func updateDisplay(progress: Float, totalSize : String) {
//        progressView.progress = progress
//        lblProgressBar.text = String(format: "%.1f%% of %@", progress * 100, totalSize)
//        print(lblProgressBar.text)
    }

    
    func setServicesData(serviceModel : ServiceModel){
//        heightConstraintStackView.constant = 0
        lblServiceLocation.isHidden = false
//        btnDownload.isHidden = true
//        btnCancel.isHidden = true
 //       btnReadMore.isHidden = true
        lblTitle.text = serviceModel.company
        lblDescription.text = serviceModel.industry
        let address: String? = serviceModel.city
        let city: String? = serviceModel.states
        let country: String? = serviceModel.country
        lblServiceLocation.text = [address,city,country].compactMap{$0}.joined(separator: ",")
        if let imageUrl = serviceModel.logo{
            let imageURL = URL(string:(imageUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!))
           // imageViewEMarket.setImage(from: imageURL!)
//            imageViewEMarket.loadImage(urlString: imageURL?.absoluteString ?? "")
//              imageViewEMarket.sd_setImage(with: imageURL, placeholderImage: UIImage.init(named: "Header"))
             imageViewEMarket.sd_setImage(with: imageURL, placeholderImage: nil)
        }
    }
    
    func changeReadMoreVisibility(inputStr : String , currentSender : Int){
//        if lblDescription.isTruncated{
//            btnReadMore.isHidden = false
//        }else{
//            btnReadMore.isHidden = true
//        }
//        let numberOfLines = Utils.numberOfLinesInLabel(yourString: inputStr, labelWidth: lblDescription.frame.size.width, labelHeight: lblDescription.frame.size.height, font: UIFont.init(name: "Arial", size: 12)!)
//        if numberOfLines > 3{
//            btnReadMore.isHidden = false
//
//        }else{
//            if numberOfLines == 3{
//                let currentModel = softwareArray![currentSender]
//                if currentModel.descrip?.htmlDecodedText() == inputStr{
//                    btnReadMore.isHidden = true
//                }else{
//                    btnReadMore.isHidden = false
//                }
//            }else{
//                btnReadMore.isHidden = true
//            }
//
//        }
    }

}

extension UILabel {
    
    var isTruncated: Bool {
        
        
        guard let labelText = text else {
            print("Label text nil")
            return false
        }
        
        let labelTextSize = (labelText as NSString).boundingRect(
            with: CGSize(width: frame.size.width, height: .greatestFiniteMagnitude),
            options: .usesLineFragmentOrigin,
            attributes: [.font: font],
            context: nil).size
        
        return labelTextSize.height > bounds.size.height
    }
    
    public var requiredHeight: CGFloat {
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: frame.width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 2
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        label.attributedText = attributedText
        label.sizeToFit()
        return label.frame.height
    }
}

