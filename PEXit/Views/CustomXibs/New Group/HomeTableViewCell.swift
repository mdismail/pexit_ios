//
//  HomeTableViewCell.swift
//  PEXit
//
//  Created by Apple on 30/07/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import TTTAttributedLabel
import StringExtensionHTML
import MIBadgeButton
import SDWebImage
import YouTubePlayer
import WebKit

protocol HomeDataDelegate{
    func cellTaped(indexPath: IndexPath)
    func postImageTapped(indexPath: IndexPath)
    func btnLikeTapped(indexPath : IndexPath)
    func btnCommentTapped(indexPath : IndexPath)
    func btnLikeCountTapped(indexPath : IndexPath)
    func btnMoreTapped(indexPath : IndexPath , sender : UIButton)
    func btnShareTapped(indexPath : IndexPath)
    func btnMediaTapped(indexPath : IndexPath)
    func btnViewProfileTapped(indexPath : IndexPath)
    func cellMediaHeight(indexPath: IndexPath,height:CGFloat)
}

extension HomeDataDelegate{
    func cellTaped(indexPath: IndexPath){}
    func postImageTapped(indexPath: IndexPath){}
    func btnLikeTapped(indexPath : IndexPath){}
    func btnCommentTapped(indexPath : IndexPath){}
    func btnLikeCountTapped(indexPath : IndexPath){}
    func btnMoreTapped(indexPath : IndexPath , sender : UIButton){}
    func btnShareTapped(indexPath : IndexPath){}
    func btnMediaTapped(indexPath : IndexPath){}
    func btnViewProfileTapped(indexPath : IndexPath){}
    func cellMediaHeight(indexPath: IndexPath,height:CGFloat){}
}

class HomeTableViewCell: UITableViewCell , TTTAttributedLabelDelegate{
    
    @IBOutlet weak var imgViewPostedBy: UIImageView!
    @IBOutlet weak var lblPostedBy: UILabel!
    @IBOutlet weak var lblPostTime: UILabel!
    @IBOutlet weak var lblSponsorContent: UILabel!
    @IBOutlet weak var lblPostDesc: TTTAttributedLabel!
    @IBOutlet weak var lblAdDesc: TTTAttributedLabel!
    @IBOutlet weak var viewMedia: UIWebView!
    @IBOutlet weak var constraintPostImgHeight: NSLayoutConstraint!
    @IBOutlet weak var constraintMediaHeight: NSLayoutConstraint!
    @IBOutlet weak var imgViewPost: UIImageView!
    @IBOutlet weak var btnViewProfile: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblAdvertisment: UILabel!
    @IBOutlet weak var viewProfile: UIView!
    @IBOutlet weak var btnEditDelete: UIButton!
    @IBOutlet weak var btnCamera: MIBadgeButton!
    @IBOutlet weak var btnLink: MIBadgeButton!
    @IBOutlet weak var btnVideo: MIBadgeButton!
    @IBOutlet weak var btnYouTube: MIBadgeButton!
    @IBOutlet weak var btnPPt: MIBadgeButton!
    @IBOutlet weak var btnFile: UIButton!
    @IBOutlet weak var btnLike: UIButton!
    @IBOutlet weak var btnLikeCount: UIButton!
   
    @IBOutlet weak var lblStatusOfPost: UILabel!
    @IBOutlet weak var btnComment: UIButton!
    @IBOutlet weak var btnShare: UIButton!
    @IBOutlet weak var viewAttatchment: UIStackView!
    @IBOutlet weak var heightProfileConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var lblSponsorHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var heightConstraintStackView: NSLayoutConstraint!
    @IBOutlet weak var lblAds: UILabel!
    @IBOutlet weak var iconAds: UIImageView!
    var delegate : HomeDataDelegate?
    var currentIndex : IndexPath?
    var homePostModel : PostRLMModel?
    var youTubePlayer : YouTubePlayerView?
    var sentCount = 0
    var recieveCount = 0
    override func awakeFromNib() {
        super.awakeFromNib()
       
        //        setUpBadgeButtonUI()
        // Initialization code
    }
 
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    override func prepareForReuse() {
       // removeImageViewgesture()
        removeDescgesture()
        self.layer.borderWidth = 0.0
        self.layer.cornerRadius = 0.0
        heightConstraintStackView.constant = 30
        heightProfileConstraint.constant = 65
        iconAds.isHidden = true
        lblAds.isHidden = true
        lblAdDesc.isHidden = true
        lblPostDesc.isHidden = false
        lblAdDesc.text = ""
        lblPostDesc.text = ""
        lblSponsorContent.isHidden = true
        lblSponsorHeightConstraint.constant = 0
        imgViewPostedBy.image = nil
        youTubePlayer?.removeFromSuperview()
    }
    
    func setUpBadgeButtonUI(){
        imgViewPostedBy.image = nil
        for button in viewAttatchment.subviews{
            if button is MIBadgeButton{
                let badgeButton = button as! MIBadgeButton
                badgeButton.badgeBackgroundColor = UIColor.init(red: 192.0/255.0, green: 0.0, blue: 0.0, alpha: 1.0)
                badgeButton.badgeEdgeInsets = UIEdgeInsetsMake(5, 0, 0, 10)
                badgeButton.badgeString = String(0)
            }
        }
    }
    
    
    
    func attributedLabel(_ label: TTTAttributedLabel!, didSelectLinkWith url: URL!) {
//        print("link clicked",url)
//        guard let url = url else { return true }
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            // openURL(_:) is deprecated in iOS 10+.
            UIApplication.shared.openURL(url)
        }
    }
    func attributedLabel(_ label: TTTAttributedLabel!, didLongPressLinkWith url: URL!, at point: CGPoint) {
        print("link long clicked",url)
    }
    
    @IBAction func viewPostDetails(_ sender: Any) {
        if let delegate = delegate , let index = currentIndex{
            delegate.cellTaped(indexPath: index)
        }
    }
    
    func setDataOnCell(homeDataModel : PostRLMModel,isCurrentUser:Bool = false,currentHeight:CGFloat){
        lblAdDesc.text = ""
        lblPostDesc.text = ""
        lblSponsorContent.isHidden = true
        lblAdDesc.isHidden = true
        lblAdDesc.enabledTextCheckingTypes = NSTextCheckingResult.CheckingType.link.rawValue
        lblAdDesc.delegate = self
        viewMedia.loadRequest(URLRequest.init(url: URL.init(string: "about:blank")!))
       // btnEditDelete.isHidden = !isCurrentUser
        constraintPostImgHeight.constant = 0
        constraintMediaHeight.constant = 0
//        if homeDataModel.template == 1{
//            btnEditDelete.isHidden = true // For sponsored content hide the edit button
//        }
        
        if (homeDataModel.template == 1 || homeDataModel.template == 2) && isCurrentUser{
            btnEditDelete.isHidden = true
        }else{
            btnEditDelete.isHidden = false
        }
        
        homePostModel = homeDataModel
        switch homeDataModel.template{
        case 0,1:
          //  removeImageViewgesture()
            removeDescgesture()
            if homeDataModel.template == 0 {
                lblSponsorHeightConstraint.constant = 0
                self.layer.borderWidth = 0.0
                self.layer.cornerRadius = 0.0
            } else {
                lblSponsorHeightConstraint.constant = 12
                lblSponsorContent.isHidden = false
                lblSponsorContent.text = "Sponsored Content"
                self.layer.borderColor = THEME_RED_COLOR.cgColor
                self.layer.borderWidth = 2.0
                self.layer.cornerRadius = 4.0
            }
            heightProfileConstraint.constant = 65
            lblStatusOfPost.text = "Posted"
            // self.backgroundColor = UIColor.white
            viewProfile.isHidden = false
             viewAttatchment.isHidden = false
//            lblAdvertisment.isHidden = true
            // lblPostedBy.text = homeDataModel.name
           
            
            lblTitle.isHidden = false
            lblPostDesc.numberOfLines = 4
            lblAdDesc.isHidden = true
            lblPostDesc.isHidden = false
            if homeDataModel.template == 0{
                heightConstraintStackView.constant = 30
                btnLike.isHidden = false
                btnComment.isHidden = false
                btnLikeCount.isHidden = false
                btnShare.isHidden = false
                if homeDataModel.spname != nil && !((homeDataModel.spname?.isEmpty)!){
                    if homeDataModel.edited == 1{
                        lblStatusOfPost.text = "Edited & Shared \(homeDataModel.spname ?? "")'s Post"
                    }else{
                      lblStatusOfPost.text = "Shared \(homeDataModel.spname ?? "")'s Post"
                    }
                }else{
                   lblStatusOfPost.text = "Posted"
                }
            }else{
                heightConstraintStackView.constant = 0
                btnLike.isHidden = true
                btnComment.isHidden = true
                btnLikeCount.isHidden = true
                btnShare.isHidden = true
                lblStatusOfPost.text = "Sponsored"
            }
            layoutIfNeeded()
            
            btnYouTube.setTitle((homeDataModel.video?.isEmpty)! ? String(0) : String((homeDataModel.video?.components(separatedBy: ","))?.count ?? 0), for: .normal)
        case 2:
         //   addGestureToImage()
            heightConstraintStackView.constant = 0
            heightProfileConstraint.constant = 0
            self.layer.borderColor = THEME_RED_COLOR.cgColor
            self.layer.borderWidth = 2.0
            self.layer.cornerRadius = 4.0
            lblSponsorHeightConstraint.constant = 12
            viewProfile.isHidden = true
            lblSponsorContent.isHidden = false
            lblSponsorContent.text = "Advertisment"
            lblAdDesc.isHidden = false
            lblAdDesc.text = (homeDataModel.post?.stringByStrippingHTMLTags)?.stringByDecodingHTMLEntities
            viewAttatchment.isHidden = true
//            lblAdvertisment.isHidden = false
            btnLike.isHidden = true
            btnComment.isHidden = true
            btnLikeCount.isHidden = true
            btnShare.isHidden = true
            lblPostDesc.isHidden = true
            layoutIfNeeded()
            btnYouTube.setTitle((homeDataModel.youtube?.isEmpty)! ? String(0) : String((homeDataModel.youtube?.components(separatedBy: ","))?.count ?? 0), for: .normal)
            
        default:
            print("Default")
            
        }
        lblTitle.text = homeDataModel.title
        btnFile.setTitle(String(homeDataModel.docu.count), for: .normal)
        btnVideo.setTitle(String(homeDataModel.allvid.count), for: .normal)
        btnPPt.setTitle(String(homeDataModel.ppt.count), for: .normal)
        btnCamera.setTitle(String(homeDataModel.image.count), for: .normal)
        if homeDataModel.image.count > 0{
           imgViewPost.isHidden = false

           let imageURL = URL(string:(homeDataModel.image[0].addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!))
            if (currentHeight == 0) {
                self.constraintPostImgHeight.constant = self.imgViewPost.frame.size.width
            } else {
               
                self.constraintPostImgHeight.constant = currentHeight
            }
            imgViewPost.sd_setImage(with: imageURL!, placeholderImage: nil, options: [], completed: { (downloadedImage, error, cache, url) in
                if (currentHeight == 0 && downloadedImage != nil) {
                    if (self.constraintPostImgHeight.constant == 0) {
                        self.constraintPostImgHeight.constant = self.imgViewPost.frame.size.width
                    }
                    let scaledHeight = self.getAspectRatioAccordingToiPhones(cellImageFrame: self.imgViewPost.frame.size,downloadedImage: downloadedImage!)
                    if scaledHeight != self.constraintPostImgHeight.constant {
                      
                        DispatchQueue.main.async {
                        if !homeDataModel.isInvalidated{
                            let realm = RealmDBManager.sharedManager.realm
                            realm?.beginWrite()
                            homeDataModel.imageHeight = Double(scaledHeight)
                          
                            try! realm?.commitWrite()
                        }
                        else{
                            print("INVALIDDDDDDD OBJECt")
                            }
                        }
                        self.constraintPostImgHeight.constant = scaledHeight
                            if let delegate = self.delegate{
                                delegate.cellMediaHeight(indexPath: self.currentIndex!,height:self.constraintPostImgHeight.constant)
                            }
                    }
                }
            })
        }else{
            constraintPostImgHeight.constant = 0
        }

        if homeDataModel.template == 1 || homeDataModel.template == 2{
//            iconAds.isHidden = false
//             lblAds.isHidden = false
//            lblAds.text =  homeDataModel.template == 1 ? "Sc" : "Ads"
        }else{
            iconAds.isHidden = true
            lblAds.isHidden = true
        }
        
        
        if homeDataModel.template == 0 ||  homeDataModel.template == 1{
            if !(homeDataModel.video?.isEmpty)! && homeDataModel.image.count == 0{
                if let firstYouTubeUrl = homeDataModel.video?.components(separatedBy: ",")[0]{
                    constraintPostImgHeight.constant = 0

                    constraintMediaHeight.constant = viewMedia.frame.size.width/1.5
                    // constraintPostImgHeight.constant = 0
                    self.bringSubview(toFront: viewMedia)
                    setUpViewForYouTubeVideo(youTubeVideoUrl: firstYouTubeUrl)
                }
                
            }else{
                constraintMediaHeight.constant = 0
            }
        }
        
        
        if homeDataModel.template == 0 ||  homeDataModel.template == 1{
            if (homeDataModel.allvid.count > 0) && homeDataModel.image.count == 0 && constraintMediaHeight.constant == 0{
                let firstvideoUrl = homeDataModel.allvid[0]
                    constraintMediaHeight.constant = viewMedia.frame.size.width/1.5
                    // constraintPostImgHeight.constant = 0
                    self.bringSubview(toFront: viewMedia)
                setUpViewForVideo(videoUrl: firstvideoUrl)


            }
//            else{
//                constraintMediaHeight.constant = 0
//            }
        }
     
        
        
//        constraintMediaHeight.constant = 0
        if homeDataModel.template == 2{
            if !(homeDataModel.youtube?.isEmpty)! && constraintPostImgHeight.constant == 0{
              //  viewMedia.isHidden = false
              //  imgViewPost.isHidden = true
                // constraintMediaHeight.constant = 150
                constraintMediaHeight.constant = viewMedia.frame.size.width/1.5
                // constraintPostImgHeight.constant = 0
                 self.bringSubview(toFront: viewMedia)
                 setUpViewForYouTubeVideo(youTubeVideoUrl: homeDataModel.youtube)

            }else{
               constraintMediaHeight.constant = 0
            }
        }
        btnLink.setTitle((homeDataModel.link?.isEmpty)! ? String(0) : String((homeDataModel.link?.components(separatedBy: ","))?.count ?? 0), for: .normal)
        for mediaButton in viewAttatchment.subviews{
            (mediaButton as! UIButton).addTarget(self, action: #selector(HomeTableViewCell.mediaButtonTapped(_:)), for: .touchUpInside)
        }
        switch homeDataModel.hide_id{
        case 0:
            lblPostedBy.text = homeDataModel.name
            if let profileImage = homeDataModel.profileImage{
                imgViewPostedBy.setImage(from: URL(string:profileImage)!)
            }
        case 1:
            setUpForHiddenIdentity()
        default:
            break
        }
        if homeDataModel.template != 2{
            let attributedString = NSAttributedString(string: "  [Read More...]", attributes: [
                .font: UIFont(name: "SFUIDisplay-Bold", size: 10)!,
                .foregroundColor: UIColor.red
                ])
            lblPostDesc.attributedTruncationToken = attributedString
           
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(HomeTableViewCell.descriptionTextTapped(_:)))
            tapGesture.numberOfTapsRequired = 1
            tapGesture.numberOfTouchesRequired = 1
            lblPostDesc.addGestureRecognizer(tapGesture)
            lblPostDesc.text = (homeDataModel.post?.stringByStrippingHTMLTags)?.stringByDecodingHTMLEntities
        }
       
        imgViewPostedBy.makeImageRounded()
        
        btnLikeCount.setTitle(String(homeDataModel.likeCount), for:.normal)
        btnComment.setTitle(String(homeDataModel.commentCount), for: .normal)
        if homeDataModel.likeStatus == 1{
            btnLike.alpha = 1
        }else{
            btnLike.alpha = 0.3
        }
        btnLike.setImage(UIImage(named: "like"), for: .normal)
        btnLike.addTarget(self, action: #selector(HomeTableViewCell.btnLikeTapped(_:)), for: .touchUpInside)
        btnComment.addTarget(self, action: #selector(HomeTableViewCell.btnCommentTapped(_:)), for: .touchUpInside)
        btnLikeCount.addTarget(self, action: #selector(HomeTableViewCell.btnLikeCountTapped(_:)), for: .touchUpInside)
        btnEditDelete.addTarget(self, action: #selector(HomeTableViewCell.btnMoreTapped(_:)), for: .touchUpInside)
        btnShare.addTarget(self, action: #selector(HomeTableViewCell.btnShareTapped(_:)), for: .touchUpInside)
        lblPostTime.text = homeDataModel.timeAgo
//        print("TEMPLATE",homeDataModel.template,lblPostDesc.numberOfLines,lblAdDesc.numberOfLines,lblPostDesc.text,lblAdDesc.text,lblPostDesc.isHidden,lblAdDesc.isHidden)
        //getTimeAgoString()
    }
    
    
    func getAspectRatioAccordingToiPhones(cellImageFrame:CGSize,downloadedImage: UIImage)->CGFloat {
        let scaleFactor = CGFloat(0.2)

        let effectiveHeight = (downloadedImage.size.height * cellImageFrame.width)/downloadedImage.size.width
        if effectiveHeight > cellImageFrame.width{
            return cellImageFrame.width
        }
        let effectiveHeightMin = effectiveHeight * (CGFloat(1) - scaleFactor)
        let effectiveHeightMax = effectiveHeight * (CGFloat(1) + scaleFactor)
        if effectiveHeightMin <= constraintPostImgHeight.constant &&
            effectiveHeightMax >= constraintPostImgHeight.constant {
            return constraintPostImgHeight.constant
        }
        return(effectiveHeight)
    }
    
    func addGestureToImage(){
       
         let tapGesture = UITapGestureRecognizer(target: self, action: #selector(HomeTableViewCell.imageTapped(_:)))
        // add it to the image view;
        imgViewPost.addGestureRecognizer(tapGesture)
        // make sure imageView can be interacted with by user
        imgViewPost.isUserInteractionEnabled = true
    }
    
    func removeImageViewgesture(){
        self.contentView.subviews.lazy.filter { $0 is UIImageView }
            .flatMap { $0.gestureRecognizers ?? [] }
            .forEach { $0.view?.removeGestureRecognizer($0) }
    }
    
    func removeDescgesture(){
        self.contentView.subviews.lazy.filter { $0 is UILabel }
            .flatMap { $0.gestureRecognizers ?? [] }
            .forEach { $0.view?.removeGestureRecognizer($0) }
    }
    
    
   @objc func imageTapped(_ sender : UITapGestureRecognizer) {
        // if the tapped view is a UIImageView then set it to imageview
        if let imageView = sender.view as? UIImageView {
           // print("Image Tapped")
            if let delegate = delegate , let index = currentIndex{
                delegate.postImageTapped(indexPath: index)
            }
            //Here you can initiate your new ViewController
            
        }
    }
    
    func setUpForHiddenIdentity(){
        lblPostedBy.text = "Unknown"
        
    }
    
    func updateBtnLike(){
        if ServiceManager.shared().isInternetAvailable == true{
            if homePostModel?.likeStatus == 1{
                btnLikeCount.setTitle(String((homePostModel?.likeCount)! - 1), for:.normal)
                btnLike.alpha = 0.3
            }else{
                btnLike.alpha = 1
                btnLikeCount.setTitle(String((homePostModel?.likeCount)! + 1), for:.normal)
            }
        }
        
    }
    
    func getTimeAgoString(){
        let postDate = Utils.getDateFromString(dateStr:homePostModel?.checkTime,timeZone:homePostModel?.defaultTimeZone)
        lblPostTime.text = postDate?.timeAgoSinceDate(numericDates: false) ?? ""
       
    }
    
    @objc func descriptionTextTapped(_ sender : UITapGestureRecognizer){
        if let delegate = delegate , let index = currentIndex{
            delegate.cellTaped(indexPath: index)
        }
    }
    
    @objc func btnLikeTapped(_ sender : UIButton){
        updateBtnLike()
        btnLike.isUserInteractionEnabled = false
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + .seconds(2)) { [weak self] in
            self?.btnLike.isUserInteractionEnabled = true
        }
        if let delegate = delegate , let index = currentIndex{
            delegate.btnLikeTapped(indexPath: index)
        }
    }
    
    @objc func mediaButtonTapped(_ sender : UIButton){
        if let delegate = delegate , let index = currentIndex{
            delegate.btnMediaTapped(indexPath: index)
        }
    }
    
    @objc func btnCommentTapped(_ sender : UIButton){
        if let delegate = delegate , let index = currentIndex{
            delegate.btnCommentTapped(indexPath: index)
        }
    }
    
    @objc func btnLikeCountTapped(_ sender : UIButton){
        if let delegate = delegate , let index = currentIndex{
            delegate.btnLikeCountTapped(indexPath: index)
        }
    }
    
    @objc func btnMoreTapped(_ sender : UIButton){
        if let delegate = delegate , let index = currentIndex{
            delegate.btnMoreTapped(indexPath: index ,sender: sender)
        }
    }
    
    @IBAction func btnViewProfileClicked(_ sender: UIButton) {
        if let delegate = delegate , let index = currentIndex{
            delegate.btnViewProfileTapped(indexPath: index)
        }
    }
    @objc func btnShareTapped(_ sender : UIButton){
        if let delegate = delegate , let index = currentIndex{
            delegate.btnShareTapped(indexPath: index)
        }
    }
    
    func setUpViewForYouTubeVideo(youTubeVideoUrl:String?){
 
        viewMedia.backgroundColor = UIColor.red
        if (youTubeVideoUrl?.isValidYouTubeUrl)!{
            if let myVideoURL = youTubeVideoUrl{
                let videoId = myVideoURL.youtubeID
               
                let reqObj = URLRequest(url: URL(string:"https://www.youtube.com/embed/\(videoId!)")!)
                print(reqObj.url)
               // viewMedia.mediaPlaybackRequiresUserAction = true
                viewMedia.loadRequest(reqObj)
                
            }
        }
    }
    
    func setUpViewForVideo(videoUrl:String?){
        if let mediaURL:URL = URL(string: videoUrl!) {
            let thumbnailImage = "video_thumbnail_frame.jpg"
            let embedHTML = "<html><head></head><body><video width=\"\(viewMedia.frame.size.width - 10)\" height=\"\(constraintMediaHeight.constant - 20)\" controls poster = \"\(thumbnailImage)\"><source src=\"\(mediaURL)\"></video></body></html>"
//            print(embedHTML)
            viewMedia.loadHTMLString(embedHTML, baseURL: Bundle.main.bundleURL)
        }
    }
}




