//
//  HomeDetailCollectionViewCell.swift
//  PEXit
//
//  Created by Apple on 31/07/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import AVFoundation
import YouTubePlayer
import SDWebImage

protocol DeleteCurrentAttatchment{
    func deleteCurrentAttatchment(sender : UIButton , indexPath : IndexPath?)
}

class HomeDetailCollectionViewCell: UICollectionViewCell ,YouTubePlayerDelegate{
    @IBOutlet weak var imgViewMedia: UIImageView!
    @IBOutlet weak var viewVideo: VideoContainerView!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var lblLink: UILabel!
    @IBOutlet weak var btnPlay: UIButton!
    
    var imageUrl : String?
    var youTubePlayer : YouTubePlayerView?
    var playerLayer : AVPlayerLayer?
    var delegate : DeleteCurrentAttatchment?
    var currentIndexPath : IndexPath?
//    override var isSelected: Bool {
//        didSet {
//            self.alpha = isSelected ? 0.5 : 1.0
//            self.contentView.alpha = isSelected ? 0.5 : 1.0
//            print("ALpha is -->",alpha)
//        }
//    }
    
    override func prepareForReuse() {
      imgViewMedia.image = nil
      lblLink.isHidden = true
      imgViewMedia.isHidden = true
      btnPlay.isHidden = true
      viewVideo.isHidden = true
      youTubePlayer?.removeFromSuperview()
        if let layerP = playerLayer{
            layerP.removeFromSuperlayer()
        }
    
    }
    
    func setUpViewForImage(url:String? = nil,isPresentation: Bool? = false , isFile:Bool? = false){
        let longPress = UILongPressGestureRecognizer.init(target: self, action: #selector(longPress(_:)))
        longPress.minimumPressDuration = 1.0
        self.addGestureRecognizer(longPress)
        lblLink.isHidden = true
        imgViewMedia.image = nil
        imgViewMedia.isHidden = false
        viewVideo.isHidden = true
        btnPlay.isHidden = true
        btnDelete.isHidden = true

        if let imageUrl = url{
            let imageURL = URL(string:(imageUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!))
            imgViewMedia.setImage(from: imageURL!)
     
        }
        if isPresentation == true{
            imgViewMedia.image = UIImage.init(named: "pptPreview")
        }
        if isFile == true{
            imgViewMedia.image = UIImage.init(named: "docsPreview")
        }
    }
   
    @objc func longPress(_ sender : UILongPressGestureRecognizer){
        print("LONG PRESS GESTURE")
       
        if let delegate = delegate{
//            self.backgroundColor = UIColor.lightGray
            delegate.deleteCurrentAttatchment(sender: btnDelete,indexPath: currentIndexPath)
           
            
        }
    }
    
    func setUpViewForVideo(videoUrl:String?){
        let longPress = UILongPressGestureRecognizer.init(target: self, action: #selector(longPress(_:)))
        longPress.minimumPressDuration = 1.0
        self.addGestureRecognizer(longPress)
        lblLink.isHidden = true
        imgViewMedia.isHidden = true
        viewVideo.isHidden = false
        btnPlay.isHidden = false
        btnDelete.isHidden = true
        if let layerP = playerLayer{
            layerP.removeFromSuperlayer()
        }
        youTubePlayer?.removeFromSuperview()
        if let videoUrl = videoUrl{
            let player = AVPlayer(url: URL(string:(videoUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!))!)
            playerLayer = AVPlayerLayer(player: player)
            playerLayer?.backgroundColor = UIColor.white.cgColor
            playerLayer?.frame = viewVideo.bounds
            playerLayer?.videoGravity = .resizeAspectFill
            
            self.viewVideo.layer.addSublayer(playerLayer!)
            self.bringSubview(toFront: btnPlay)
            self.viewVideo.clipsToBounds = true
            self.viewVideo.layer.masksToBounds = true
        }
        
    }
    
    func setUpViewForLink(link:String?){
        lblLink.isHidden = false
        imgViewMedia.isHidden = true
        viewVideo.isHidden = true
        btnPlay.isHidden = true
        lblLink.text = link
        lblLink.setTextAsLink()
    }
    
    func setUpViewForYouTubeVideo(youTubeVideoUrl:String?){
        lblLink.isHidden = true
        imgViewMedia.isHidden = true
        btnPlay.isHidden = true
        viewVideo.isHidden = false
        youTubePlayer?.removeFromSuperview()
        if let layerP = playerLayer{
            layerP.removeFromSuperlayer()
        }
        youTubePlayer = YouTubePlayerView()
        youTubePlayer?.frame = viewVideo.bounds
        youTubePlayer?.backgroundColor = UIColor.red
        self.bringSubview(toFront: youTubePlayer!)
        viewVideo.addSubview(youTubePlayer!)
        self.viewVideo.clipsToBounds = true
        self.viewVideo.layer.masksToBounds = true
        if (youTubeVideoUrl?.isValidYouTubeUrl)!{
            if let myVideoURL = youTubeVideoUrl{

                
                let youTubeUrl = URL(string:myVideoURL)
                
                youTubeUrl?.expandURLWithCompletionHandler(completionHandler: {
                    expandedURL in
                  
                    var urlString = expandedURL?.absoluteString
                    urlString = urlString?.replacingOccurrences(of: "&feature=youtu.be", with: "", options: NSString.CompareOptions.literal, range:nil)

                    DispatchQueue.main.async {
                        self.youTubePlayer!.loadVideoURL(URL(string:urlString!)!)
                    }
                    
                    
                })
                

                
            }
        }
   }
    
    @IBAction func btnDeleteClicked(_ sender: UIButton) {
        if let delegate = delegate{
            delegate.deleteCurrentAttatchment(sender: sender,indexPath: currentIndexPath)
        }
    }
    
    
}
