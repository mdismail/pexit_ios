//
//  ProfileButton.swift
//  PEXit
//
//  Created by ATS on 29/07/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import SDWebImage


extension UIView {
    var parentViewController: UIViewController? {
        for responder in sequence(first: self, next: { $0.next }) {
            if let viewController = responder as? UIViewController {
                return viewController
            }
        }
        return nil
    }
}

//extension UIView{
//    func viewController(_ view: UIView) -> UIViewController {
//        var responder: UIResponder? = view
//        while !(responder is UIViewController) {
//            responder = responder?.next
//            if nil == responder {
//                break
//            }
//        }
//        return (responder as? UIViewController)!
//    }
//}



class ProfileButton: UIButton {

    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var subHeadingLabel: UILabel!
    @IBOutlet weak var btnEditProfile: UILabel!
    
    var userInfo = UserInfo(){
        didSet
        {
            //profileImageView.sd_setImage(with: URL(string:FILE_DOWNLOAD(fileId: userInfo.profilePicId!)), placeholderImage: PROFILE_PLACEHOLDER)
            profileImageView.makeImageRounded()
            profileImageView.sd_setImage(with: URL(string:userInfo.profilePicId!), placeholderImage: PROFILE_PLACEHOLDER)
            if let headingValuex = userInfo.headingValue {
                headingLabel.text = headingValuex
            }
            else if userInfo.attributedHeadingValue != nil
            {
                headingLabel.attributedText = userInfo.attributedHeadingValue
            }else
            {
                headingLabel.text = nil
            }
            btnEditProfile.isHidden = userInfo.editProfileLblHidden
            if let subHeadingValuex = userInfo.subHeadingValue {
                subHeadingLabel.text = subHeadingValuex
            }
            else if userInfo.attributedSubHeadingValue != nil
            {
                subHeadingLabel.attributedText = userInfo.attributedSubHeadingValue
            }else
            {
                subHeadingLabel.text = nil
            }
        }
    }
    
    // MARK: - For loading from Nib
    override func awakeAfter(using aDecoder: NSCoder) -> Any? {
        return self.loadFromNibIfEmbeddedInDifferentNib()
    }
    

    override func awakeFromNib() {
        super.awakeFromNib()
        setUp()
    }
    
    
    private func setUp() {
        self.removeTarget(nil, action: nil, for: .allEvents)
       //For doing some common setups, which is valid throughout the app.
        let controller = self.parentViewController
        
        print("CONTROLLER IS--->",controller)
        if controller is HomePageViewController{
             self.addTarget(HomePageViewController(), action: #selector(HomePageViewController.goToProfilePage), for: .touchUpInside)
        }
        if controller is NewPostController{
            self.addTarget(NewPostController(), action: #selector(NewPostController.goToProfilePage), for: .touchUpInside)
        }
       
    }

 
//    @objc func goToProfilePage()
//    {
//        print("we can go to profile page by using this is \(String(describing: userInfo.userId))")
//
 //   }
}
