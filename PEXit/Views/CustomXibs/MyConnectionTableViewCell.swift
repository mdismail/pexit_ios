//
//  MyConnectionTableViewCell.swift
//  PEXit
//
//  Created by Apple on 21/09/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class MyConnectionTableViewCell: UITableViewCell {

    @IBOutlet weak var imgViewUser: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imgViewUser.makeImageRounded()
        // Initialization code
    }

    func setData(userDetails : Connections.FetchConnections.UserRecord){
        lblUserName.text = userDetails.username
        if let profileImage = userDetails.pimage{
            if let imageUrl = URL(string: profileImage){
                imgViewUser.setImage(from:imageUrl)
            }
        }
        let state: String? = userDetails.userstate
        let city: String? = userDetails.location
        let country: String? = userDetails.country
        
        lblLocation.text = ([city,state,country].filter{$0 != ""}).compactMap{$0}.joined(separator: ",")
        
    }

}
