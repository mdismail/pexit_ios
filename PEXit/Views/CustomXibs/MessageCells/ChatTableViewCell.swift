//
//  ChatTableViewCell.swift
//  PEXit
//
//  Created by Apple on 04/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

infix operator ???: NilCoalescingPrecedence

public func ???<T>(optional: T?, defaultValue: @autoclosure () -> String) -> String {
    switch optional {
    case let value?: return String(describing: value)
    case nil: return defaultValue()
    }
}

class ChatTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imgViewUser: UIImageView!
    @IBOutlet weak var lblUsername: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblLeftConversation: UILabel!
    @IBOutlet weak var viewChatMessage: UIView!
    
    @IBOutlet weak var lblForwarded: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setData(chatModel : ConversationList){
        if chatModel.leftStatus == 1{
            lblLeftConversation.isHidden = false
            viewChatMessage.isHidden = true
            if chatModel.fromid == Defaults.getUserID(){
                lblLeftConversation.text = "You left the conversation"
            }else{
                lblLeftConversation.text =  "\(chatModel.fpname ??? "") left the conversation"
                
            }
            
        }else{
            lblLeftConversation.isHidden = true
            viewChatMessage.isHidden = false
            lblMessage.textColor = UIColor.black
            if chatModel.translatedMessage != nil{
                lblMessage.text = chatModel.translatedMessage
            }else{
                lblMessage.text = chatModel.message?.htmlDecodedText()
            }
            lblTime.text = chatModel.timeAgo
            
            //  getTimeAgoString(conversationModel: chatModel)
            imgViewUser.makeImageRounded()
            if chatModel.fromid == Defaults.getUserID(){
                lblUsername.text = "You"
                changeMessageTextColor(chatModel: chatModel)
            }else{
                lblUsername.text = chatModel.fpname
            }
            if let userImage = chatModel.fpimage{
                imgViewUser.sd_setImage(with: URL(string:userImage) , placeholderImage: nil)
            }
            if chatModel.forward == 1{
                lblForwarded.isHidden = false
            }else{
                lblForwarded.isHidden = true
            }
            
        }
    }
    
    func changeMessageTextColor(chatModel : ConversationList){
        switch chatModel.unreadMessage{
        case 0:
            lblMessage.textColor = THEME_RED_COLOR
        case 1:
            lblMessage.textColor = UIColor.init(red: 0, green: 161.0/255.0, blue: 215.0/255.0, alpha: 1.0)
        case 2:
            lblMessage.textColor = UIColor.black
        default:
            lblMessage.textColor = UIColor.black
        }
    }
    
    func getTimeAgoString(conversationModel : ConversationList){
        let postDate = Utils.getDateFromString(dateStr:conversationModel.date, timeZone: "UTC")
        lblTime.text = postDate?.timeAgoSinceDate(numericDates: false) ?? ""
        print("TIME AGO STR 1--->",lblTime.text)
    }
    
}
