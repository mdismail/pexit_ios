//
//  MessageListTableViewCell.swift
//  PEXit
//
//  Created by Apple on 03/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import MIBadgeButton
import TTTAttributedLabel



class MessageListTableViewCell: UITableViewCell , TTTAttributedLabelDelegate{
    
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var lblName: TTTAttributedLabel!
    @IBOutlet weak var imgViewProfile: UIImageView!
    @IBOutlet weak var groupAvatarView: MDGroupAvatarView!
    @IBOutlet weak var btnMessageCount: MIBadgeButton!
    @IBOutlet weak var btnAcceptRequest: UIButton!
    @IBOutlet weak var btnRejectRequest: UIButton!
    
    var currentModel : NotificationListModel?
    var delegate : ItemSelected?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        lblMessage.text = ""
        lblName.text = ""
        imgViewProfile.makeImageRounded()
       // groupAvatarView.makeViewRounded()
        btnMessageCount.badgeEdgeInsets = UIEdgeInsetsMake(10, 0, 0, 15)
        btnMessageCount.badgeTextColor = UIColor.white
        btnMessageCount.badgeBackgroundColor = THEME_RED_COLOR
        // Initialization code
    }
    
    
    
    override func prepareForReuse() {
        lblName.text = ""
        lblMessage.text = ""
        imgViewProfile.image = nil
        
        // lblName.clearActionDictionary()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func setMessageData(messageModel : MessageListModel){
        groupAvatarView.isHidden = true
        imgViewProfile.isHidden = false

        if messageModel.type == 1{
            if let userImage = messageModel.pimage![0].pimage{
                imgViewProfile.sd_setImage(with: URL(string:userImage) , placeholderImage: nil)
            }
            lblName.text = messageModel.pimage![0].uname
        }
        else{
            if let userImage = messageModel.gimage{
                imgViewProfile.sd_setImage(with: URL(string:userImage) , placeholderImage: nil)
            }
            messageModel.pimage = messageModel.pimage?.filter{$0.muid != Defaults.getUserID()}
            if (messageModel.pimage?.count)! <= 3{
                lblName.text = messageModel.pimage?.compactMap{$0.uname}.joined(separator: ",")
                
            }else{
                for i in 0...2{
//                    lblName.text = (lblName.text ?? "") + (messageModel.pimage![i].uname ?? "")
//                    lblName.text = lblName.text + ","
                    lblName.text = "\(lblName.text ?? "")\(messageModel.pimage![i].uname ?? ""),"
                }
//                lblName.text = messageModel.pimage?.count > 3 ? lblName.text! + " + \((messageModel.pimage?.count)!-3) more" : String(lblName.text!.dropLast())
                
                lblName.text = messageModel.pimage?.count > 3 ? "\(lblName.text!) \((messageModel.pimage?.count)!-3) more" : String((lblName.text as! String).dropLast())
            }
            setGroupProfileImage(messageModel: messageModel)
        }
        
        if messageModel.unreadChatCount > 0{
            btnMessageCount.isHidden = false
            groupAvatarView.backgroundColor = THEME_BACKGROUND_COLOR
            btnMessageCount.badgeString = "\(messageModel.unreadChatCount!)"
            self.backgroundColor = THEME_BACKGROUND_COLOR
        }else{
            groupAvatarView.backgroundColor = UIColor.clear

            btnMessageCount.isHidden = true
            self.backgroundColor = UIColor.white
        }
        
        lblMessage.text = messageModel.lastMessage?.htmlDecodedText()
    }
    
    func setGroupProfileImage(messageModel : MessageListModel){
        groupAvatarView.isHidden = false
        imgViewProfile.isHidden = true
        var urls = [URL]()
        for messageModel in messageModel.pimage!{
            if let profileImg = messageModel.pimage{
                urls.append(URL(string:profileImg)!)
            }
        }
        
        groupAvatarView.setAvatarUrl(urls, realTotal: (messageModel.pimage?.count)!)
    }
    
    
    func addHyperLinks(typeOfLink:String,strValue:String,idOfStr:String,completeStr:String){
        let linkRange = (completeStr).range(of: strValue)
        // let linkUrl = URL(string:strValue)!
        lblName.addLink(to: URL(string:"\(typeOfLink):\(idOfStr)")! , with:(completeStr).nsRange(from: linkRange!))
    }
    
    
    func setNotificationData(notificationModel : NotificationListModel){
        btnAcceptRequest.isHidden = true
        btnRejectRequest.isHidden = true
        currentModel = notificationModel
        let attributes = [NSAttributedStringKey.font: UIFont(name: "Arial-BoldMT", size: 14)!,
                          NSAttributedStringKey.foregroundColor: UIColor.init(red: 0, green: (124.0/255.0), blue: (246.0/255.0), alpha: 1.0)]
        
        let messageAttributes = [NSAttributedStringKey.font: UIFont(name: "Arial-BoldMT", size: 14)!,
                          NSAttributedStringKey.foregroundColor: UIColor.black]
        
        lblName.numberOfLines = 0
        let attributedStr = NSMutableAttributedString()
        lblName.isUserInteractionEnabled = true
        lblName.linkAttributes = attributes
        if notificationModel.name != nil{
            let attributedName = NSAttributedString.init(string: (notificationModel.name ??? ""), attributes: attributes)
            attributedStr.append(attributedName)
        }
        
        if notificationModel.username != nil{
            let attributedName = NSAttributedString.init(string: (notificationModel.username ??? ""), attributes: attributes)
            attributedStr.append(attributedName)
        }
        attributedStr.append(NSAttributedString.init(string: (notificationModel.message ??? ""),attributes:messageAttributes))
        lblName.attributedText =  attributedStr
        lblName.delegate = self
        
        if notificationModel.gname != nil{
            let attributedGroupName = NSAttributedString.init(string: (notificationModel.gname ??? ""), attributes: attributes)
            attributedStr.append(attributedGroupName)
            lblName.attributedText = attributedStr
            imgViewProfile.setImage(string: notificationModel.gname
                ?? "G")
        }
        //Adding Hyperlinks to text
        
        if notificationModel.name != nil{
            addHyperLinks(typeOfLink: "USERNAME", strValue: notificationModel.name!, idOfStr: String(notificationModel.fid!), completeStr: attributedStr.string)
        }
        if notificationModel.username != nil{
            addHyperLinks(typeOfLink: "USERNAME", strValue: notificationModel.username!, idOfStr: notificationModel.created_by!, completeStr: attributedStr.string)
        }
        if notificationModel.gname != nil{
            addHyperLinks(typeOfLink: "GROUP", strValue: notificationModel.gname!, idOfStr: String(notificationModel.gid!), completeStr: attributedStr.string)
        }
        if let userImage = notificationModel.pimage{
            imgViewProfile.sd_setImage(with: URL(string:userImage) , placeholderImage: nil)
        }
        if (notificationModel.template == 1 && notificationModel.type == 2) || (notificationModel.template == 2){
            btnAcceptRequest.isHidden = false
            btnRejectRequest.isHidden = false
            
        }
        
      
    }
    
    @IBAction func btnAcceptRequestClicked(_ sender: UIButton) {
       delegate?.optionSelected(option: "ACCEPT_REQUEST:\(sender.tag)")
    }
    
    @IBAction func btnRejectRequestClicked(_ sender: UIButton) {
        delegate?.optionSelected(option: "REJECT_REQUEST:\(sender.tag)")
    }
    
    func attributedLabel(_ label: TTTAttributedLabel!, didSelectLinkWith url: URL!) {
        print("URL IS--->", url)
        if let delegate = delegate{
            delegate.optionSelected(option: url.absoluteString)
        }
      }
}


extension StringProtocol where Index == String.Index {
    func nsRange(from range: Range<Index>) -> NSRange {
        return NSRange(range, in: self)
    }
}
