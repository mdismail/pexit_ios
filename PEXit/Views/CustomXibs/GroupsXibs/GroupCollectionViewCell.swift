//
//  GroupCollectionViewCell.swift
//  PEXit
//
//  Created by Apple on 26/09/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

enum TYPE_OF_GROUP_ACTIONS : Int{
    case JoinGroup = 1
    case EditGroup = 2
    case ViewPosts = 3
    case CreateGroup = 4
    case LeaveGroup = 5
   
}

protocol SendGroupAction{
    func sendAction(type:Int,sender:UIButton)
}

class GroupCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var lblGroupName: UILabel!
    @IBOutlet weak var imgViewGroup: UIImageView!
    @IBOutlet weak var lblGroupDesc: UILabel!
    @IBOutlet weak var btnJoinLeave: UIButton!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var btnViewPosts: UIButton!
    @IBOutlet weak var btnViewGroupDetails: UIButton!
    var delegate : SendGroupAction?
    
    @IBAction func btnJoinGroupClicked(_ sender: UIButton) {
        if let delegate = delegate{
            delegate.sendAction(type:TYPE_OF_GROUP_ACTIONS.JoinGroup.rawValue,sender: sender)
        }
    }
    
    @IBAction func btnEditGroupClicked(_ sender: UIButton) {
        if let delegate = delegate{
            delegate.sendAction(type:TYPE_OF_GROUP_ACTIONS.EditGroup.rawValue,sender: sender)
        }
    }
    
    @IBAction func btnViewPostsClicked(_ sender: UIButton) {
        if let delegate = delegate{
            delegate.sendAction(type:TYPE_OF_GROUP_ACTIONS.ViewPosts.rawValue,sender: sender)
        }
    }
    func setGroupData(groupModel : GroupListModel){
        lblGroupName.text = groupModel.gname
        imgViewGroup.makeImageRounded()

        lblGroupDesc.text = (groupModel.gdescription?.stringByStrippingHTMLTags)?.stringByDecodingHTMLEntities
        if groupModel.editKey == 1{
            btnEdit.isHidden = false
        }else{
            btnEdit.isHidden = true
        }
        if groupModel.viewKey == 1{
            btnViewPosts.isHidden = false
        }else{
            btnViewPosts.isHidden = true
        }
        btnJoinLeave.isHidden = true
        if groupModel.joinKey == 1{
            btnJoinLeave.isHidden = false
            btnJoinLeave.setTitle("Join", for: .normal)
        }
        if groupModel.userKey == 1 || groupModel.adminKey == 1{
            btnJoinLeave.isHidden = false
            btnJoinLeave.setTitle("Leave", for: .normal)
        }
    }
    
}
