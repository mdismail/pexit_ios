//
//  EditSeviceTableViewCell.swift
//  PEXit
//
//  Created by Apple on 18/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import TTTAttributedLabel

class EditSeviceTableViewCell: UITableViewCell {
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblSubCategory: UILabel!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblCategory: TTTAttributedLabel!
//    @IBOutlet weak var btnReadMore: UIButton!
//    @IBOutlet weak var lblSoftwareLink: UITextView!
//    @IBOutlet weak var constraintLinkHeight: NSLayoutConstraint!
//    @IBOutlet weak var topConstraint: NSLayoutConstraint!
//    @IBOutlet weak var linkToTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var imgWidthConstraint: NSLayoutConstraint!
    //   var isExpanded: Bool = false
    var delegate: ExpandingDelegate?
    var sponsorArray : [SponsorModel]?
    var softwareArray : [ServiceProductModel]?
    var currentType : String?
    
    override func awakeFromNib() {
        super.awakeFromNib()
//        lblSoftwareLink.leftAlignTextView()
        initialSetUp()
     }
    
    func initialSetUp(){
        lblCategory.numberOfLines = 1
      //  lblCategory.bounds.size.height = lblCategory.requiredHeight
        lblCategory.text = ""
    }
    
    override func prepareForReuse() {
       initialSetUp()
    }
    
   
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setProductData(productModel : ServiceProductModel){
//        btnReadMore.isHidden = true
//        topConstraint.constant = 4
        lblName.text = productModel.name
        lblCategory.numberOfLines = 1
        lblCategory.text = "Category: \(productModel.category ??? "None")"
        lblSubCategory.text = "Status: \(productModel.active == 0 ? "Expired" : "Active") "
        if let logo = productModel.logo{
             imgView.sd_setImage(with: URL(string:logo) , placeholderImage: nil)
        }
       
        if productModel.stype == "free"{
            btnEdit.setImage(UIImage.init(named: "edit_gray"), for: .normal)
            productModel.expired == 1 ? (btnEdit.isHidden = true) : (btnEdit.isHidden = false)
        }
        if productModel.stype == "paid"{
            btnEdit.isHidden = false
            if productModel.renew == 1{
                btnEdit.setImage(UIImage.init(named: "renew"), for: .normal)
            }else{
                btnEdit.setImage(UIImage.init(named: "edit_gray"), for: .normal)
            }
        }
    }
    
    func setServiceData(productModel : ServiceProductModel){
//        btnReadMore.isHidden = true
//        topConstraint.constant = 4
        lblCategory.numberOfLines = 1
        lblSubCategory.numberOfLines = 1
        lblName.text = productModel.company
        lblCategory.text = productModel.industry
//        let country: String? = productModel.country
//        let state: String? = productModel.states
//        let city: String? = productModel.city
        lblSubCategory.text = "Status: \(productModel.active == 0 ? "Expired" : "Active") "
//        lblSubCategory.text = [country,state,city].compactMap{$0}.joined(separator: ",")
        if let logo = productModel.logo{
            imgView.sd_setImage(with: URL(string:logo) , placeholderImage: nil)
        }
        if productModel.stype == "free"{
            btnEdit.setImage(UIImage.init(named: "edit_gray"), for: .normal)
            productModel.expired == 1 ? (btnEdit.isHidden = true) : (btnEdit.isHidden = false)
        }
        if productModel.stype == "paid"{
            btnEdit.isHidden = false
            if productModel.renew == 1{
                btnEdit.setImage(UIImage.init(named: "renew"), for: .normal)
            }else{
                btnEdit.setImage(UIImage.init(named: "edit_gray"), for: .normal)
            }
        }
    }
    
    func setSoftwareData(productModel : ServiceProductModel){
        
//        btnReadMore.isHidden = true
//        constraintLinkHeight.constant = 0
//        topConstraint.constant = 4
//        lblCategory.numberOfLines = 1
//        lblSubCategory.numberOfLines = 1
        lblName.text = productModel.name
        let attributedString = NSAttributedString(string: "  Read More...", attributes: [
            .font: UIFont(name: "SFUIDisplay-Bold", size: 10)!,
            .foregroundColor: UIColor.red
            ])
       // lblCategory.attributedTruncationToken = attributedString
        lblCategory.text = productModel.descrip?.htmlDecodedText()
        lblSubCategory.text = "Status: \(productModel.active == 0 ? "Expired" : "Active") "
        if let logo = productModel.logo{
            imgView.sd_setImage(with: URL(string:logo) , placeholderImage: nil)
        }
        
        if productModel.stype == "free"{
            btnEdit.setImage(UIImage.init(named: "edit_gray"), for: .normal)
            productModel.expired == 1 ? (btnEdit.isHidden = true) : (btnEdit.isHidden = false)
        }
        if productModel.stype == "paid"{
            btnEdit.isHidden = false
            if productModel.renew == 1{
                btnEdit.setImage(UIImage.init(named: "renew"), for: .normal)
            }else{
                btnEdit.setImage(UIImage.init(named: "edit_gray"), for: .normal)
            }
        }
  }
    
 
    func setSponosrData(sponsorModel : SponsorModel){
        imgWidthConstraint.constant = 0
//        constraintLinkHeight.constant = 0
        lblName.text = "Title: " + (sponsorModel.title ?? "")
//        topConstraint.constant = 4
        lblCategory.text = "Description: " + (sponsorModel.post?.htmlDecodedText() ?? "")
        lblCategory.numberOfLines = 1
       // btnReadMore.isHidden = true
       // lblCategory.numberOfLines = sponsorModel.isExpanded! ? 0 : 2
       // changeReadMoreVisibility(inputStr: (sponsorModel.post?.htmlDecodedText())!)
       // btnReadMore.setTitle( sponsorModel.isExpanded! ? "Read less..." : "Read more...", for: .normal)
        lblSubCategory.text = "Status: \(sponsorModel.active == 0 ? "Expired" : "Active")"
        
        if sponsorModel.stype == "free"{
            btnEdit.setImage(UIImage.init(named: "edit_gray"), for: .normal)
            sponsorModel.expired == 1 ? (btnEdit.isHidden = true) : (btnEdit.isHidden = false)
        }
        if sponsorModel.stype == "paid"{
            btnEdit.isHidden = false
            if sponsorModel.renew == 1{
                btnEdit.setImage(UIImage.init(named: "renew"), for: .normal)
            }else{
                btnEdit.setImage(UIImage.init(named: "edit_gray"), for: .normal)
            }
            
        }
    }
/*
    func changeReadMoreVisibility(inputStr : String,isExpanded:Bool? = false,currentModel:ServiceProductModel? = nil){
        
        if lblCategory.isTruncated || isExpanded!{
            btnReadMore.isHidden = false
           
//            linkToTopConstraint.constant = lblCategory.frame.size.height + btnReadMore.frame.size.height + 8
//            topConstraint.constant = lblCategory.frame.size.height +  constraintLinkHeight.constant + btnReadMore.frame.size.height + 8
//            self.layoutIfNeeded()
//            delegate?.moreTapped(cell: self)

        }else{
            btnReadMore.isHidden = true
        }
            
//            linkToTopConstraint.constant = 8
//            topConstraint.constant = lblCategory.frame.size.height + constraintLinkHeight.constant +  8
//            self.layoutIfNeeded()
//             delegate?.moreTapped(cell: self)

        } */
      
    }
    

