//
//  AdsListTableViewCell.swift
//  PEXit
//
//  Created by ats on 22/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import ExpandableLabel
import YouTubePlayer

protocol ExpandingDelegate {
    func moreTapped(cell: Any)
}

class AdsListTableViewCell: UITableViewCell {
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblAdDesc: UILabel!
    @IBOutlet weak var youTubeView: VideoContainerView!
    @IBOutlet weak var lblAdTitle: UILabel!
    @IBOutlet weak var btnMore: UIButton!
    @IBOutlet weak var lblLink: UITextView!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var btnEdit: UIButton!
    
    @IBOutlet weak var btnReadMoreHeightConst: NSLayoutConstraint!
    @IBOutlet weak var heightConstraintLblLink: NSLayoutConstraint!
    // var isExpanded: Bool = false
    var delegate: ExpandingDelegate?
    var adsArray : [AdsModel]?
   
    override func awakeFromNib() {
        super.awakeFromNib()
        lblAdDesc.text = nil
        imgView.isHidden = true
        youTubeView.isHidden = true
        lblLink.leftAlignTextView()
        //isExpanded = false
        // Initialization code
    }

    override func prepareForReuse() {
        super.prepareForReuse()
      //  lblAdDesc.collapsed = true
        lblAdDesc.text = nil
        imgView.isHidden = true
        youTubeView.isHidden = true
        //isExpanded = false
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func btnMoreTapped(_ sender: UIButton) {
        let currentModel = adsArray![sender.tag]
        currentModel.isExpanded = !currentModel.isExpanded!
        lblAdDesc.numberOfLines = currentModel.isExpanded! ? 0 : 2
        btnMore.setTitle( currentModel.isExpanded! ? "Read less..." : "Read more...", for: .normal)
            delegate?.moreTapped(cell: self)
        
    }
    
    
    func setAdsData(adsModel:AdsModel){
        lblAdTitle.text = adsModel.title
        btnReadMoreHeightConst.constant = 0
        btnMore.isHidden = true
//        if (adsModel.url?.isEmpty)!{
            heightConstraintLblLink.constant = 0
//        }else{
//            lblLink.text = adsModel.url
//            heightConstraintLblLink.constant = 30
//        }
        lblAdDesc.numberOfLines = 2
        lblStatus.text = "Status: " + (adsModel.expired == 1 ? "Expired" : "Active")
        if adsModel.post != nil && !(adsModel.post?.isEmpty)!{
            let numberOfLines = Utils.numberOfLinesInLabel(yourString: (adsModel.post?.htmlDecodedText())!, labelWidth: lblAdDesc.frame.size.width, labelHeight: lblAdDesc.frame.size.height, font: UIFont.init(name: "Arial", size: 14)!)
            
            if numberOfLines >= 2{
                btnMore.isHidden = false
                btnReadMoreHeightConst.constant = 25
            }else{
                btnMore.isHidden = true
                btnReadMoreHeightConst.constant = 0
            }
        }
        lblAdDesc.numberOfLines = adsModel.isExpanded! ? 0 : 2
        btnMore.setTitle(adsModel.isExpanded! ? "Read less..." : "Read more...", for: .normal)
        lblAdDesc.text = adsModel.post?.htmlDecodedText()
        if adsModel.image != nil && !(adsModel.image?.isEmpty)! {
            imgView.isHidden = false
            imgView.sd_setImage(with: URL(string:adsModel.image!) , placeholderImage: nil)
        }
        if adsModel.utub != nil && !(adsModel.utub?.isEmpty)! && (adsModel.utub?.isValidYouTubeUrl)!{
            setUpVideoView(videoUrl: adsModel.utub)
        }
        if adsModel.stype == "free"{
            btnEdit.setImage(UIImage.init(named: "edit_gray"), for: .normal)
            adsModel.expired == 1 ? (btnEdit.isHidden = true) : (btnEdit.isHidden = false)
        }
        if adsModel.stype == "paid"{
            btnEdit.isHidden = false
            if adsModel.renew == 1{
                btnEdit.setImage(UIImage.init(named: "renew"), for: .normal)
            }else{
                btnEdit.setImage(UIImage.init(named: "edit_gray"), for: .normal)
            }
            
        }
    }
    
    func setUpVideoView(videoUrl : String?){
        youTubeView.isHidden = false
        let youTubePlayer = YouTubePlayerView()
        youTubePlayer.frame = youTubeView.bounds
        youTubePlayer.backgroundColor = UIColor.red
        self.bringSubview(toFront: youTubePlayer)
        youTubeView.addSubview(youTubePlayer)
        youTubeView.clipsToBounds = true
        youTubeView.layer.masksToBounds = true
        if (videoUrl?.isValidYouTubeUrl)!{
            if let myVideoURL = videoUrl{
                let youTubeUrl = URL(string:myVideoURL)
                youTubeUrl?.expandURLWithCompletionHandler(completionHandler: {
                    expandedURL in
                    
                    var urlString = expandedURL?.absoluteString
                    urlString = urlString?.replacingOccurrences(of: "&feature=youtu.be", with: "", options: NSString.CompareOptions.literal, range:nil)
                   
                    DispatchQueue.main.async {
                        youTubePlayer.loadVideoURL(URL(string:urlString!)!)
                    }
                    
                    
                })
                
            }
        }
    }
    
}
