//
//  PostNewJobTableViewCell.swift
//  PEXit
//
//  Created by ats on 20/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

protocol ExpandingCellDelegate {
    func updated(height: CGFloat,currentTextView:UITextView)
}

class PostNewJobTableViewCell: UITableViewCell {
    @IBOutlet weak var txtView: UITextView!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var viewTxtView: UIView!
    @IBOutlet weak var viewJobOptions: UIView!
    @IBOutlet weak var txtFieldInput: TJTextField!
    @IBOutlet weak var btnApplyWithProfile: UIButton!
    @IBOutlet weak var btnExternalSite: UIButton!
    @IBOutlet weak var viewAttachment: UIView!
    @IBOutlet weak var btnShowMyProfile: UIButton!
    @IBOutlet weak var txtFieldEmail: TJTextField!
    @IBOutlet weak var txtFieldLink: TJTextField!
    @IBOutlet weak var viewAttachmentContainer: UIView!
    
    @IBOutlet weak var lblPlaceholderText: UILabel!
    @IBOutlet weak var lblDescPlaceholderText: UILabel!
    var delegate: ExpandingCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        txtView.leftAlignTextView()
       // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func hideViews(){
        btnSubmit.isHidden = true
        viewTxtView.isHidden = true
        viewJobOptions.isHidden = true
        viewAttachment.isHidden = true
        txtFieldInput.isHidden = true
        lblPlaceholderText.isHidden = true
        lblDescPlaceholderText.isHidden = true
       
    }

    func configureTextfield(_ textFieldDetails:TextfieldInfoStruct) {
        lblPlaceholderText.isHidden = false
       // txtFieldInput.placeholder = textFieldDetails.placeholderText
        txtFieldInput.accessibilityLabel = textFieldDetails.placeholderText
        txtFieldInput.rightImage = textFieldDetails.rightImage
        txtFieldInput.text = textFieldDetails.textfieldText
        lblPlaceholderText.text = textFieldDetails.placeholderText
    }
    
    
    
    
    func setUpJobOptionsView(showMyProfile:Bool?=false,isExternalLink:Bool?=false, linkstr:String? = ""){
        if showMyProfile == true{
            btnShowMyProfile.isSelected = true
        }else{
            btnShowMyProfile.isSelected = false
        }
        if isExternalLink == true{
            btnExternalSite.isSelected = true
            btnApplyWithProfile.isSelected = false
        }else{
            btnExternalSite.isSelected = false
            btnApplyWithProfile.isSelected = true
        }
        txtFieldEmail.text = ProfileRLMModel.fetchProfile()?.userEmail
        txtFieldEmail.isUserInteractionEnabled = false
        txtFieldLink.text = linkstr
    }
    
    
    func configureTxtView(placeholderStr : String , currentText : String? = nil){
        viewTxtView.isHidden = false
        lblDescPlaceholderText.isHidden = false
        txtView.delegate = self
        txtView.text = currentText?.htmlDecodedText()
        bringSubview(toFront: viewTxtView)
        lblDescPlaceholderText.text = placeholderStr
        txtView.accessibilityLabel = placeholderStr
        //txtView.placeholder = placeholderStr
        //txtView.updateTextViewPlacholder()
        calculateTxtViewHeight(txtView)
    }
  
}

extension PostNewJobTableViewCell : UITextViewDelegate{
    
    func textViewDidChange(_ textView: UITextView) {
        calculateTxtViewHeight(textView)
    }
    
    func calculateTxtViewHeight(_ textView: UITextView){
        textView.updateTextViewPlacholder()
        let height = textView.newHeight(withBaseHeight: 35)
        delegate?.updated(height: height,currentTextView: textView)
    }
}
