//
//  EditJobTableViewCell.swift
//  PEXit
//
//  Created by Apple on 18/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import ExpandableLabel

class EditJobTableViewCell: UITableViewCell{
   
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblJobFunction: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblJobDesc: UILabel!
    @IBOutlet weak var lblCountry: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblCompanyName: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var btnEdit: UIButton!
    
    override func prepareForReuse() {
        super.prepareForReuse()
      //  lblJobDesc.collapsed = true
        lblJobDesc.text = nil
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
       // lblJobDesc.delegate = self
//        lblJobDesc.collapsedAttributedLink = NSAttributedString(string: "Read More")
//        lblJobDesc.expandedAttributedLink = NSAttributedString(string: "Read Less")
       // lblJobDesc.setLessLinkWith(lessLink: "Close", attributes: [NSForegroundColorAttributeName:UIColor.red], position: nil)
       // lblJobDesc.ellipsis = NSAttributedString(string: "...")
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func btnEditClicked(_ sender: Any) {
        print("Test")
    }
    
    func setJobData(jobModel : JobDetailModel){
       
        lblTitle.text = jobModel.title
        lblCompanyName.text = jobModel.cmpny
        lblJobFunction.text = jobModel.funct
        lblLocation.text = jobModel.addr
        lblCountry.text = jobModel.country
        lblJobDesc.text = jobModel.jdesc
        lblStatus.text = jobModel.active == 1 ? "Active" : "Inactive"
        if let logo = jobModel.logo{
            imgView.sd_setImage(with: URL(string:logo) , placeholderImage: nil)
        }
        if jobModel.stype == "free"{
            btnEdit.setImage(UIImage.init(named: "edit_gray"), for: .normal)
            jobModel.expired == 1 ? (btnEdit.isHidden = true) : (btnEdit.isHidden = false)
        }
        if jobModel.stype == "paid"{
            btnEdit.isHidden = false
            if jobModel.renew == 1{
                btnEdit.setImage(UIImage.init(named: "renew"), for: .normal)
            }else{
                 btnEdit.setImage(UIImage.init(named: "edit_gray"), for: .normal)
            }
            //jobModel.renew == 1 ? (btnEdit.isHidden = true) : (btnEdit.isHidden = false)
            // if productModel.expired == true && productModel.paid
        }
        
        
    }

}
