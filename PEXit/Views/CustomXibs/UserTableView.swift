//
//  UserTableView.swift
//  PEXit
//
//  Created by Apple on 01/08/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import SDWebImage

class UserTableView: UITableView ,UITableViewDataSource ,UITableViewDelegate {

    var connectionResponse : Connections.FetchConnections.Response?
    override func awakeFromNib() {
        self.register(UINib(nibName: "UserTableViewCell", bundle: nil), forCellReuseIdentifier: "UserTableViewCell")
        self.dataSource = self
        self.tableFooterView = UIView()
    }
    
    func setUpView(connectionResp : Connections.FetchConnections.Response?){
        connectionResponse = connectionResp
        self.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return connectionResponse?.datas?.count ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserTableViewCell", for: indexPath) as! UserTableViewCell
        cell.lblUserName.text = connectionResponse?.datas![indexPath.row].username
        if let imageUrl = URL(string: (connectionResponse?.datas![indexPath.row].pimage)!){
            cell.imgViewUser.setImage(from:imageUrl)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
}
