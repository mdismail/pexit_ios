//
//  TransactionHistoryTableViewCell.swift
//  PEXit
//
//  Created by Apple on 28/09/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class TransactionHistoryTableViewCell: UITableViewCell {

    @IBOutlet weak var lblOrderId: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lbltax: UILabel!
    @IBOutlet weak var lblCurrency: UILabel!
    @IBOutlet weak var lblTransactionDate: UILabel!
    @IBOutlet weak var lblTransactionStatus: UILabel!
    @IBOutlet weak var lblTypeOfService: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setTransactionData(transactionModel : TransactionListModel){
        lblOrderId.text = transactionModel.orderid
        lblAmount.text = "Amount: " + transactionModel.amount!
        lbltax.text = "Tax: " + transactionModel.tax!
        lblCurrency.text = "Currency: " + transactionModel.currency!
        lblTransactionDate.text = "Transaction Date: " + transactionModel.trans_date!
        lblTransactionStatus.text = "Status: " + transactionModel.order_status!
        lblTypeOfService.text = transactionModel.services!
        
    }

}
