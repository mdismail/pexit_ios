//
//  ConnectionCollectionViewCell.swift
//  PEXit
//
//  Created by Apple on 21/09/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

protocol SendFriendId{
    func sendFriendId(friendId : Int)
}

class ConnectionCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imgViewUser: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var btnConnect: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    var delegate : SendFriendId?
    override func awakeFromNib(){
        imgViewUser.makeImageRounded()
    }
    
    @IBAction func btnConnectClicked(_ sender: UIButton) {
        if let delegate = delegate{
            delegate.sendFriendId(friendId : sender.tag)
        }
    }
    func setData(userDetails : Connections.FetchConnections.UserRecord){
        lblUserName.text = userDetails.username 
        if let profileImage = userDetails.pimage{
            if let imageUrl = URL(string: profileImage){
                imgViewUser.setImage(from:imageUrl)
            }
        }
        if userDetails.friendStatus == 0{
            if userDetails.blockStatus == 1{
                btnConnect.setTitle("Unblock", for: .normal)
            }else{
                btnConnect.setTitle("Connect", for: .normal)
            }
            btnConnect.isHidden = false
        }else{
            btnConnect.isHidden = true
        }
        let state: String? = userDetails.userstate
        let city: String? = userDetails.location
        let country: String? = userDetails.country
        lblTitle.text = userDetails.title
        lblLocation.text = ([city,state,country].filter{$0 != ""}).compactMap{$0}.joined(separator: ",")
        
    }
}
