//
//  ProfileHeaderView.swift
//  PEXit
//
//  Created by Apple on 06/08/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
protocol HeaderTapped{
    func indexOfHeaderTapped(index : Int)
}

class ProfileHeaderView: UITableViewHeaderFooterView {
    @IBOutlet weak var headerTitle: UILabel!
    @IBOutlet weak var btnAddRecord: UIButton!
    var delegate : HeaderTapped?
    
    override func awakeFromNib(){
        btnAddRecord.imageView?.clipsToBounds = false
        btnAddRecord.imageView?.contentMode = .center
    }
    
    @IBAction func addNewRecordTapped(_ sender: UIButton) {
        if let delegate = delegate {
            delegate.indexOfHeaderTapped(index: sender.tag)
        }
    }
    
    
}
