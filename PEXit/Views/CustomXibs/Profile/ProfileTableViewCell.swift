//
//  ProfileTableViewCell.swift
//  PEXit
//
//  Created by Apple on 06/08/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import Material
import SDWebImage

protocol SaveUserProfile{
    func saveUserProfile()
}

class ProfileTableViewCell: UITableViewCell {

    @IBOutlet weak var txtFieldProfile: TJTextField!
    @IBOutlet weak var lblFieldName: UILabel!
    @IBOutlet weak var viewProfileHeader: UIView!
//    @IBOutlet weak var txtFieldName: TJTextField!
//    @IBOutlet weak var txtFieldProfession: TJTextField!
//    @IBOutlet weak var txtFieldJobTitle: TJTextField!
    @IBOutlet weak var imgViewUser: UIImageView!
    @IBOutlet weak var txtViewProfileSummary: UITextView!
    @IBOutlet weak var viewAttachment: UIView!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnChangeImage: UIButton!
    @IBOutlet weak var viewProfileSummary: UIView!
    @IBOutlet weak var collectionViewProfileMedia: UICollectionView!
    @IBOutlet weak var txtFieldProfileLeading: NSLayoutConstraint!
    @IBOutlet weak var btnCodePicker: UIButton!
    var delegate : SaveUserProfile?
   
    override func awakeFromNib() {
        super.awakeFromNib()
        imgViewUser.makeImageRounded()
        btnCodePicker.add(border:.bottom,color:.black,width:0.7)
      //  txtViewProfileSummary.placeholder = "Description"
        txtViewProfileSummary.leftAlignTextView()
        //txtViewProfileSummary.updateTextViewPlacholder()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        txtFieldProfile.leftImage = nil
        txtFieldProfile.rightImage = nil
        txtFieldProfile.placeholder = nil
        txtFieldProfile.accessibilityLabel = nil
        txtFieldProfile.text = nil
        txtFieldProfile.tag = 0
        txtFieldProfile.delegate = nil
        btnCodePicker.isHidden = true
        txtFieldProfileLeading.constant = 20
        txtViewProfileSummary.leftAlignTextView()
    }
    
    func configureTextfield(_ textFieldDetails:TextfieldInfoStruct,textColor:UIColor? = .white,countryCode:String? = nil) {
        txtFieldProfile.accessibilityLabel = textFieldDetails.placeholderText
        if textFieldDetails.placeholderText ==  "Phone Number"
        {
            txtFieldProfile.keyboardType = .numberPad
            btnCodePicker.isHidden = false
            btnCodePicker.setTitle(countryCode ?? "Code", for: .normal)
            txtFieldProfileLeading.constant = 75
        }else{
            txtFieldProfile.keyboardType = .default
            btnCodePicker.isHidden = true
            txtFieldProfileLeading.constant = 20
        }
        
        lblFieldName.text = textFieldDetails.placeholderText
        txtFieldProfile.rightImage = textFieldDetails.rightImage
        if textFieldDetails.placeholderText ==  "Email"{
            txtFieldProfile.isUserInteractionEnabled = false
        }else{
           txtFieldProfile.isUserInteractionEnabled = true
        }
        if textFieldDetails.placeholderText ==  "Phone Number" && textFieldDetails.textfieldText == ":"{
             txtFieldProfile.text = ""
        }else{
            if textFieldDetails.placeholderText ==  "Email" || textFieldDetails.placeholderText ==  "Name"{
                txtFieldProfile.text = textFieldDetails.textfieldText
            }else{
                txtFieldProfile.text = textFieldDetails.textfieldText?.capitalized
            }
            
//            txtFieldProfile.text = textFieldDetails.placeholderText ==  "Email" ?  textFieldDetails.textfieldText : textFieldDetails.textfieldText?.capitalized
            
        }
    }
    

    
    @IBAction func btnSaveProfileClicked(_ sender: Any) {
        if let delegate = delegate{
            delegate.saveUserProfile()
        }
    }
    func setProfileHeaderData(profileHeaderDict : [String : String]){
//            txtFieldName.text = profileHeaderDict["uname"]
//            txtFieldJobTitle.text = profileHeaderDict["ptitle"]
//            txtFieldProfession.text = profileHeaderDict["puserType"]
        
        let attributedStr =   try? NSAttributedString(htmlString: profileHeaderDict["psummary"] ?? "")
        let mutableAttributedStr = NSMutableAttributedString.init(attributedString: attributedStr ?? NSAttributedString.init(string: ""))
        let currentFont = UIFont().Arial_Regular(n:14)
        var noSpaceAttributedString = mutableAttributedStr.trimmedAttributedString(set: CharacterSet.whitespacesAndNewlines)
        noSpaceAttributedString.setFontFace(font: currentFont, color: .black)
        txtViewProfileSummary.attributedText = noSpaceAttributedString
//
//        let currentFont = UIFont(name:"Arial", size: 14)
//        let profileStr = try? NSMutableAttributedString(HTMLString: profileHeaderDict["psummary"] ?? "", font: currentFont)
//        if let profileDesc = profileStr{
//
//             txtViewProfileSummary.attributedText = profileDesc
//        }
        
        
//        let mutableStr = NSMutableAttributedString.init(string: profileHeaderDict["psummary"] ?? "")
//        txtViewProfileSummary.attributedText = mutableStr.htmlAttributed(family: "Arial", size: 14, color: .black)
//        
        

        if userProfileSummary != ""{
        
            let attributedStr =   try? NSAttributedString(htmlString: userProfileSummary)
            let mutableAttributedStr = NSMutableAttributedString.init(attributedString: attributedStr ?? NSAttributedString.init(string: ""))

            let noSpaceAttributedString = mutableAttributedStr.trimmedAttributedString(set: CharacterSet.whitespacesAndNewlines)
            noSpaceAttributedString.setFontFace(font: currentFont, color: .black)
            txtViewProfileSummary.attributedText = noSpaceAttributedString
//            let profileStr = try? NSMutableAttributedString(HTMLString: userProfileSummary , font: currentFont)
//            if let profileDesc = profileStr{
//                txtViewProfileSummary.attributedText = profileDesc
//            }
           
            
        }
        //txtViewProfileSummary.updateTextViewPlacholder()
        if let userImg = profileHeaderDict["pimage"] , !userImg.isEmpty{
                 imgViewUser.sd_setImage(with: URL(string:userImg) , placeholderImage: nil)
        }
       
    }
}
