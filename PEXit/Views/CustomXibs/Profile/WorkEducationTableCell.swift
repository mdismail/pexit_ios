//
//  WorkEducationTableCell.swift
//  PEXit
//
//  Created by Apple on 06/08/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class CustomCollectionView : UICollectionView{
    var indexPath : IndexPath?
    var rowNumber : Int!
}

protocol DeleteOrgModel{
    func deleteOrganization(indexPath : IndexPath)
    func cancelClicked()
    func addClicked(indexPath : IndexPath)
    func editClicked(indexPath : IndexPath)
    
}

class WorkEducationTableCell: UITableViewCell {
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var trailingConstraintHeader: NSLayoutConstraint!
    @IBOutlet weak var lblOrganizationName: UILabel!
    @IBOutlet weak var lblTimePeriod: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblJobDescription: UILabel!
    @IBOutlet weak var collectionViewMedia: CustomCollectionView!
    @IBOutlet weak var heightConstraintCollectionView: NSLayoutConstraint!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var viewAttachment: UIView!
    @IBOutlet weak var viewActions: UIView!
    @IBOutlet weak var viewAddRecord: UIView!
    @IBOutlet weak var viewData: UIView!
    @IBOutlet weak var btnAdd: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    
    var delegate : DeleteOrgModel?
    var currentIndex : IndexPath?
    var countOfMediaItems = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setExperienceData(experienceModel : ExperienceModel){
//        heightConstraintCollectionView.constant = 100
        countOfMediaItems = (experienceModel.pexpphot.count) + (experienceModel.pexpfle.count) + (experienceModel.pexpprest.count)
        if experienceModel.pexplnk != nil && !(experienceModel.pexplnk?.isEmpty)!{
            countOfMediaItems = countOfMediaItems + (experienceModel.pexplnk?.components(separatedBy: ","))!.count
        }
        if experienceModel.pexpvdeo != nil && !(experienceModel.pexpvdeo?.isEmpty)!{
            countOfMediaItems = countOfMediaItems + (experienceModel.pexpvdeo?.components(separatedBy: ","))!.count
        }
        
        heightConstraintCollectionView.constant = countOfMediaItems > 0 ? 100 : 0
        lblLocation.isHidden = false
        lblJobDescription.isHidden = false
        let dateformatter = DateFormatter()
        lblHeader.text = experienceModel.pexptitle
//        lblOrganizationName.text = experienceModel.pexpcompanyname
        var fromMonth = ""
        var toMonth = ""
       // let dateFormatter = DateFormatter()
        if experienceModel.pexpfrom_mon > 0{
            fromMonth = "\(dateformatter.getMonthName(monthNumber: experienceModel.pexpfrom_mon-1))"
        }
        if experienceModel.pexpto_mon > 0{
            toMonth = "\(dateformatter.getMonthName(monthNumber: experienceModel.pexpto_mon-1))"
        }
        if experienceModel.pexpiscurrent == "isCurrent" || experienceModel.pexpto_yr == 0{
             lblOrganizationName.text = "\(fromMonth) \(experienceModel.pexpfrom_yr) - Till Now"
        }else{
            lblOrganizationName.text = "\(fromMonth) \(experienceModel.pexpfrom_yr) - \(toMonth) \(experienceModel.pexpto_yr)"
        }
        lblTimePeriod.text  = experienceModel.pexpcompanyname
        lblLocation.text = experienceModel.pexplocation
        lblJobDescription.text = experienceModel.pexpdes
        
       // self.collectionViewMedia.reloadData()
//        collectionViewMedia.setNeedsLayout()
        collectionViewMedia.layoutIfNeeded()
        collectionViewMedia.layoutSubviews()
        self.collectionViewMedia.reloadData()

            }
            
//        })
//    }
    
    func setEducationData(educationModel : EducationModel){
        
        countOfMediaItems = (educationModel.peduphot.count) + (educationModel.pedufle.count) + (educationModel.peduprest.count)
        if educationModel.pedulnk != nil && !(educationModel.pedulnk?.isEmpty)!{
            countOfMediaItems = countOfMediaItems + (educationModel.pedulnk?.components(separatedBy: ","))!.count
        }
        if educationModel.peduvdeo != nil && !(educationModel.peduvdeo?.isEmpty)!{
            countOfMediaItems = countOfMediaItems + (educationModel.peduvdeo?.components(separatedBy: ","))!.count
        }
        heightConstraintCollectionView.constant = countOfMediaItems > 0 ? 100 : 0
        lblLocation.isHidden = false
        lblJobDescription.isHidden = false
        lblHeader.text = educationModel.peduschoolDegree
        lblOrganizationName.text = educationModel.pedupschoolname
        lblTimePeriod.text = educationModel.peduschoolstudy
        var schoolFromYear = ""
        var schoolToYear = ""
        if educationModel.peduschoolfromyear != 0{
            schoolFromYear = "\(educationModel.peduschoolfromyear)"
        }
        if educationModel.peduschooltoyear != 0{
            schoolToYear = "\(educationModel.peduschooltoyear)"
        }
        if !schoolFromYear.isEmpty && !schoolToYear.isEmpty{
             lblLocation.text =  schoolFromYear + " - " + schoolToYear
        }
        else if !schoolFromYear.isEmpty{
            lblLocation.text =  schoolFromYear
        }
        else if !schoolToYear.isEmpty{
            lblLocation.text =  schoolToYear
        }else{
             lblLocation.text = ""
        }
        
        lblJobDescription.text = educationModel.peduexp_desc
//        DispatchQueue.main.asyncAfter(deadline: .now() + 0.0005, execute: {
//            if self.heightConstraintCollectionView.constant > 0 && self.currentIndex?.row == self.collectionViewMedia.rowNumber{
//        self.collectionViewMedia.reloadData()
//        collectionViewMedia.setNeedsLayout()
        collectionViewMedia.layoutIfNeeded()
        collectionViewMedia.layoutSubviews()
        self.collectionViewMedia.reloadData()
            }
            
//        })
    
        
//    }
    
    func setHonorsData(honorsModel : HonorsModel){
        let dateformatter = DateFormatter()
        lblHeader.text = honorsModel.phontitle
        var monthNumber = 0
        if honorsModel.phonmonth > 0{
            monthNumber = honorsModel.phonmonth - 1
        }
        lblOrganizationName.text = "Awarded in \(dateformatter.getMonthName(monthNumber: monthNumber)) \(honorsModel.phonyear)"
        lblTimePeriod.text = honorsModel.phondesc
        lblLocation.text = ""
        lblJobDescription.text = ""
        var countOfMediaItems = (honorsModel.phonphot.count) + (honorsModel.phonfle.count) + (honorsModel.phonprest.count)
        if honorsModel.phonlnk != nil && !(honorsModel.phonlnk?.isEmpty)!{
            countOfMediaItems = countOfMediaItems + (honorsModel.phonlnk?.components(separatedBy: ","))!.count
        }
        if honorsModel.phonvdeo != nil && !(honorsModel.phonvdeo?.isEmpty)!{
            countOfMediaItems = countOfMediaItems + (honorsModel.phonvdeo?.components(separatedBy: ","))!.count
        }
        heightConstraintCollectionView.constant = countOfMediaItems > 0 ? 100 : 0
//        DispatchQueue.main.asyncAfter(deadline: .now() + 0.0005, execute: {
//            if self.heightConstraintCollectionView.constant > 0 && self.currentIndex?.row == self.collectionViewMedia.rowNumber{
//        self.collectionViewMedia.reloadData()
//        collectionViewMedia.setNeedsLayout()
        collectionViewMedia.layoutIfNeeded()
        collectionViewMedia.layoutSubviews()
        self.collectionViewMedia.reloadData()
            }
            
//        })
    
       
//    }
    
    func setCompanyData(companyModel : CompanyModel){
        lblHeader.text = companyModel.pcomcountry
        lblOrganizationName.text = companyModel.pcomstates
        lblTimePeriod.text = companyModel.pcomcity
        lblLocation.isHidden = true
        lblJobDescription.isHidden = true
        heightConstraintCollectionView.constant = 100 
//        DispatchQueue.main.asyncAfter(deadline: .now() + 0.0005, execute: {
//            if self.heightConstraintCollectionView.constant > 0 && self.currentIndex?.row == self.collectionViewMedia.rowNumber{
//        self.collectionViewMedia.reloadData()
//        collectionViewMedia.setNeedsLayout()
        collectionViewMedia.layoutIfNeeded()
        collectionViewMedia.layoutSubviews()
        self.collectionViewMedia.reloadData()
//            }
        
     //   })

    }
    @IBAction func btnEditClicked(_ sender: UIButton) {
        if let delegate = self.delegate{
            delegate.editClicked(indexPath: currentIndex!)
        }
    }
    
    @IBAction func btnDeleteClicked(_ sender: UIButton) {
        if let delegate = self.delegate{
            delegate.deleteOrganization(indexPath: currentIndex!)
        }
    }
    
    @IBAction func btnAddTapped(_ sender: Any) {
        if let delegate = self.delegate{
            delegate.addClicked(indexPath: currentIndex!)
        }
    }
    @IBAction func btnCancelTapped(_ sender: Any) {
        if let delegate = self.delegate{
            delegate.cancelClicked()
        }
    }
}



