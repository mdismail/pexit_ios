//
//  JobSearchCell.swift
//  PEXit
//
//  Created by Apple on 02/08/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class JobSearchCell: UICollectionViewCell {
    
    @IBOutlet weak var lblSearchText: UILabel!
    @IBOutlet weak var btnSearch: UIButton!
}
