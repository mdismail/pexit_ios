//
//  JobResultTableViewCell.swift
//  PEXit
//
//  Created by Apple on 02/08/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class JobResultTableViewCell: UITableViewCell {
    @IBOutlet weak var imgCompany: CachedImageView!
    @IBOutlet weak var lblJobTitle: UILabel!
    @IBOutlet weak var lblCompanyName: UILabel!
    @IBOutlet weak var lblLocations: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setUpData(jobModel : JobModel){
        lblJobTitle.text = jobModel.title
        lblCompanyName.text = jobModel.cmpny
        var address: String? = jobModel.addr
        var city: String? = jobModel.states
        var country: String? = jobModel.country
        address = (address?.isEmpty)! ? "" : address
        city = (city?.isEmpty)! ? nil : city
        country = (country?.isEmpty)! ? nil : country

        
        lblLocations.text = [address,city,country].compactMap{$0}.joined(separator: ",")
//            .flatMap{$0}
//            .joinWithSeparator(",")
       
//        lblLocations.text = "\(jobModel.addr!),\(jobModel.states),\(jobModel.country)"
        if let imageUrl = jobModel.logo{
            let imageURL = URL(string:(imageUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!))
            
//            imgCompany.loadImage(urlString: (imageURL?.absoluteString) ?? "")
//              imgCompany.sd_setImage(with: imageURL, placeholderImage: UIImage.init(named: "Header"))
            imgCompany.sd_setImage(with: imageURL, placeholderImage: nil)
           // imgCompany.setImage(from: imageURL!)
            
        }
    }

}
