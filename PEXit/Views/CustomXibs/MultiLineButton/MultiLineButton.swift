//
//  MultiLineButton.swift
//  PEXit
//
//  Created by Apple on 30/07/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class MultiLineButton: UIButton {
    @IBOutlet weak var btnTitle: UIButton!
    
    var buttonDetails = MultiLineButtonStruct(){
        didSet
        {
            let attributedString = NSMutableAttributedString(string: buttonDetails.headerText!,attributes : [
                .font :UIFont(name: "Arial-BoldMT", size: 18)!,
                .foregroundColor: UIColor.black
                ])
            
            let subTitleString = NSAttributedString(string: buttonDetails.subHeaderText!,attributes : [
                .font :UIFont(name: "Arial-BoldMT", size: 10)!,
                .foregroundColor: UIColor.black
                
                ])
            attributedString.append(NSAttributedString(string: "\n"))
            attributedString.append(subTitleString)
            let style = NSMutableParagraphStyle()
            style.alignment = buttonDetails.alignment!
            style.paragraphSpacing = buttonDetails.paragraphSpacing!
            attributedString.addAttribute(.paragraphStyle, value: style, range: NSRange(location: 0, length: attributedString.length))
            self.titleLabel?.numberOfLines = 2
            self.setAttributedTitle(attributedString, for: .normal)
    }
    }
    
    // MARK: - For loading from Nib
    override func awakeAfter(using aDecoder: NSCoder) -> Any? {
        return self.loadFromNibIfEmbeddedInDifferentNib()
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
       // setUp()
    }
    
  
}
