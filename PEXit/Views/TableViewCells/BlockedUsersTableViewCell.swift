//
//  BlockedUsersTableViewCell.swift
//  PEXit
//
//  Created by ats on 27/03/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class BlockedUsersTableViewCell: UITableViewCell {

    @IBOutlet weak var imgViewuser: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var btnUnblockUser: UIButton!
    var unblockClicked : ((Int) -> Void)?
    override func awakeFromNib() {
        super.awakeFromNib()
        imgViewuser.makeImageRounded()
        // Initialization code
    }

    func setUserData(userRecord : Connections.FetchConnections.UserRecord){
        lblUserName.text = userRecord.username
        if let userImage = userRecord.pimage{
            imgViewuser.sd_setImage(with: URL(string:userImage) , placeholderImage: nil)
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    @IBAction func btnUnblockClicked(_ sender: UIButton) {
        unblockClicked?(sender.tag)
    }
    
}
