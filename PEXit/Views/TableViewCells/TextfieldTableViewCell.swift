//
//  TextfieldTableViewCell.swift
//  PEXit
//
//  Created by Apple on 11/07/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class TextfieldTableViewCell: UITableViewCell {

    
    @IBOutlet weak var textfield: TJTextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
 
    
    override func prepareForReuse() {
        super.prepareForReuse()
        textfield.leftImage = nil
        textfield.rightImage = nil
        textfield.placeholder = nil
        textfield.text = nil
        textfield.tag = 0
        textfield.delegate = nil
    }
    
    func configureTextfield(_ textFieldDetails:TextfieldInfoStruct,textColor:UIColor? = .white) {
        textfield.placeholder = textFieldDetails.placeholderText
        textfield.leftImage = textFieldDetails.leftImage
        textfield.rightImage = textFieldDetails.rightImage
        textfield.lineColor = textColor!
        textfield.textColor = textColor!
        textfield.attributedPlaceholder = NSAttributedString(string: textFieldDetails.placeholderText!, attributes: [.foregroundColor : textColor])
        
        
//        textfield.attributedPlaceholder = NSAttributedString(string: textFieldDetails.placeholderText!, attributes:[])
           
        textfield.text = textFieldDetails.textfieldText ?? nil
//        if textfield.placeholder == "Select Country" {
//            textfield.isUserInteractionEnabled = false
//        }else
//        {
//            textfield.isUserInteractionEnabled = true
//        }
    }
}
