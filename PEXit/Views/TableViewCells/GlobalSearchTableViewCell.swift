//
//  GlobalSearchTableViewCell.swift
//  PEXit
//
//  Created by ats on 16/11/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class GlobalSearchTableViewCell: UITableViewCell {
    @IBOutlet var lblSearchOption: UILabel!
    @IBOutlet var btnSelect: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
