//
//  LoadingView.swift
//  Sqoolink
//
//  Created by Narendar N on 03/11/17.
//  Copyright © 2017 Narendar N. All rights reserved.
//

import UIKit
class LoadingView: UIView {
    fileprivate var timer: Timer?
    fileprivate var animateViews = [UIView]()
    var animateBarCornerRadius:CGFloat = 2
    fileprivate var animating : Bool = false
    fileprivate var yAnimation : CAKeyframeAnimation?
    fileprivate var xAnimation : CAKeyframeAnimation?
    var hidesWhenStopped : Bool = false
    var animater =  UIDynamicAnimator()
    
    func startAnimation() {
        addAnimation()
    }
    func stopAnimation() {
        removeAnimation()
    }
}

extension LoadingView {
    
    fileprivate func addAnimation() {
        
        // 1: Set LoaderView properties
        DispatchQueue.main.async {
            self.backgroundColor = UIColor.clear
            self.clipsToBounds = true
            self.animating = true
            
            let blurEffectView = UIVisualEffectView(effect: nil)
            blurEffectView.frame = BOUNDS
            blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            blurEffectView.backgroundColor = UIColor.clear

            let logo  = UIImageView.init(frame: CGRect.init(x: SCREEN_WIDTH/2-25, y: SCREEN_HEIGHT/2-25, width: 50, height: 50))
            logo.layer.masksToBounds = true
            logo.layer.cornerRadius = 8
            logo.image = UIImage.init(named: "appspinner")//icon.png
            blurEffectView.contentView.addSubview(logo)
            
            self.animater = UIDynamicAnimator(referenceView: blurEffectView)
            let gravityBehavior = UIGravityBehavior(items: [logo])
            
            self.animater.addBehavior(gravityBehavior )
            let collisionBehavior = UICollisionBehavior(items: [logo])
            collisionBehavior.setTranslatesReferenceBoundsIntoBoundary(with: UIEdgeInsets.init(top: 0, left: 0, bottom: SCREEN_HEIGHT/2-25, right: 0))
            
            self.animater.addBehavior(collisionBehavior )
            let elasticityBehavior = UIDynamicItemBehavior(items: [logo])
            elasticityBehavior.allowsRotation = true
            elasticityBehavior.elasticity = 0.7
            self.animater.addBehavior(elasticityBehavior )


            let rotations = 1.0
            let duration = 5.0
            // 360 degree
//            var rotationAnimation: CABasicAnimation?
//            rotationAnimation = CABasicAnimation(keyPath: "transform.rotation.z")
//            rotationAnimation?.toValue = .pi * 2.0 * rotations * duration
//            rotationAnimation?.duration = duration
//            rotationAnimation?.isCumulative = true
//            rotationAnimation?.repeatCount = Float(APP_MAX_INT)
//            self.imageView?.layer.add(rotationAnimation!, forKey: "rotationAnimation")

            //Flip
            let rotationAnimation = CABasicAnimation.init(keyPath: "transform.rotation.y")
            rotationAnimation.toValue = NSNumber.init(value: Double.pi * 2.0 * rotations * duration)
            rotationAnimation.duration = duration
            rotationAnimation.isCumulative = true
            rotationAnimation.repeatCount = Float(INT_MAX)
            logo.layer.add(rotationAnimation, forKey: "rotationAnimation")
            KEY_WINDOW?.addSubview(blurEffectView)
  
        }
    }
    
    func isAnimating() -> Bool {
        return self.animating
    }
    
    fileprivate func removeAnimation() {
        
        if !self.isAnimating() {
            return
        }
        
        DispatchQueue.main.async {
            self.animating = false

            _ = KEY_WINDOW?.subviews.map {
                if let blurEffectView = $0 as? UIVisualEffectView{
                    _ = blurEffectView.subviews.map({ $0.removeFromSuperview() })
                    $0.removeFromSuperview()
                }
            }

            for view in (KEY_WINDOW?.subviews)! {
                if view.tag == LOADING_IMAGE_TAG {
                    view.removeFromSuperview()
                } else if view.tag == LOADING_VIEW_TAG {
                    view.removeFromSuperview()
                }
            }
             self.layer.isHidden = self.hidesWhenStopped
            self.layer.removeAllAnimations()
            self.isHidden = true
        }
     }
   
}

class LoadingIndicator : UIActivityIndicatorView{
    override init(frame: CGRect) {
        super.init(frame: frame)
      //  self.isHidden = false
        self.activityIndicatorViewStyle = .gray
        self.startAnimating()
    }
    
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
//    override func stopAnimating() {
//        DispatchQueue.main.async{
//            self.isHidden = true
//            self.stopAnimating()
//        }
//
//    }
    
//    class func getActivityIndicator(){
//        let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
//        spinner.startAnimating()
//        spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
//    }
}


class MyIndicator: UIActivityIndicatorView {
    
    let imageView = UIImageView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        imageView.frame = bounds
        imageView.image = UIImage(named: "appspinner")
        imageView.layer.masksToBounds = true
        imageView.layer.cornerRadius = 8
        imageView.contentMode = .scaleAspectFit
        imageView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(imageView)
    }
    
    required init(coder: NSCoder) {
        fatalError()
    }
    
    override func startAnimating() {
        isHidden = false
        rotate()
    }
    
    override func stopAnimating() {
        isHidden = true
        removeRotation()
    }
    
    private func rotate() {
        let rotation : CABasicAnimation = CABasicAnimation(keyPath: "transform.rotation.z")
        rotation.toValue = NSNumber(value: Double.pi * 2)
        rotation.duration = 1
        rotation.isCumulative = true
        rotation.repeatCount = Float.greatestFiniteMagnitude
        self.imageView.layer.add(rotation, forKey: "rotationAnimation")
    }
    
    private func removeRotation() {
        self.imageView.layer.removeAnimation(forKey: "rotationAnimation")
    }
}

