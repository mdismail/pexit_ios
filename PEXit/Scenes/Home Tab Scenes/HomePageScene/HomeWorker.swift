//
//  HomeWorker.swift
//  PEXit
//
//  Created by Apple on 27/07/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation
import ObjectMapper

class HomeWorker{
    func fetchPosts(request : SearchResult.FetchPosts.Request,completionHandler: @escaping(_ records : [AnyObject]?,_ totalCount:Int? ,_ profileModel : AnyObject?) -> Void) {
        
        var params = ["flag":request.flag ?? 0,"records":request.records ?? 10,"key":request.key ?? "","page":request.page ?? 1,"type":request.type,"location":request.location] as! [String:Any]
        if request.type == 0{
             params["pads"] = LOADED_ADS
             params["psponsor"] = LOADED_SPONSOR
        }
        //In case of products
        if request.type == 3 || request.type == 4{
            params["pctype"] = request.pctype
            if request.type == 3{
                params["category"] = request.category
            }

        }
        
        if request.type == 5{
             params["sttype"] = request.softwareType
        }
        if request.page == 1{
             START_LOADING_VIEW()
        }
//        print("PARAMS ARE ---->", params)
       
        ServiceManager.methodType(requestType: POST_REQUEST, url: HOME_SEARCH, params: params as NSDictionary, paramsData: nil, completion: { (response,_ responseData, statusCode) in
            STOP_LOADING_VIEW()
            //   let decoder = JSONDecoder()
            if response != nil{
                do{
                    print("Response issss ---->",response)
                    if (response as! [String : AnyObject])["status"] as? Bool == true{
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                            
                            do{
                                
                                let json = try JSONSerialization.jsonObject(with: responseData!, options: []) as? Dictionary<String , Any>
                                
                                switch request.type{
                                case 0 :
                                    LOADED_ADS = (response as! [String : AnyObject])["pads"] as? String ?? ""
                                    LOADED_SPONSOR = (response as! [String : AnyObject])["psponsor"]  as? String ?? ""
                                    if request.isOtherUser != true{
                                        let profileModel = Mapper<ProfileRLMModel>().map(JSONObject:json!["profile"] as! [String:Any])
                                        ProfileRLMModel.deleteAll()
                                        RealmDBManager.sharedManager.addOrUpdateObject(object: profileModel!)
                                        if request.page == 1 {
                                            PostRLMModel.deleteAll()
                                        }
                                    }
                                    let postList = json!["datas"] as! [[String:Any]]
                                    var postsArray = [PostRLMModel]()
                                    for post in postList{
                                        let postModel = Mapper<PostRLMModel>().map(JSONObject:post)
                                        if request.isOtherUser == true{
                                            postsArray.append(postModel!)
                                        }else{
                                            RealmDBManager.sharedManager.addOrUpdateObject(object: postModel!)
                                        }
                                    }
                                    if request.isOtherUser == true{
                                        completionHandler(postsArray,0,ProfileRLMModel.fetchProfile()!)
                                    }else{
                                        let postModels = PostRLMModel.fetchPostModels()
                                    completionHandler(postModels,0,ProfileRLMModel.fetchProfile()!)
                                    }
                                    
                                    
                                case 1:
                                    do{
                                        let decoder = JSONDecoder()
                                        let responseConnections = try decoder.decode(Connections.FetchConnections.Response.self, from: responseData!)
                                        let usersArray = responseConnections.datas
                                        completionHandler(usersArray as [AnyObject]?,0, ProfileRLMModel.fetchProfile()!)
                                    }catch{
                                        
                                    }
                                case 2:
                                    
                                    if request.page == 1 {
                                        JobModel.deleteAll()
                                    }
                                    
                                    let jobList = json!["datas"] as! [[String:Any]]
                                    let jobCount = json!["totalRecords"] as? Int
                                    for job in jobList{
                                        let jobModel = Mapper<JobModel>().map(JSONObject:job )
                                        RealmDBManager.sharedManager.addOrUpdateObject(object: jobModel!)
                                        
                                    }
                                    let jobModels = JobModel.fetchJobModels()
                                    completionHandler(jobModels,jobCount,ProfileRLMModel.fetchProfile()!)
                                case 3:
                                    
                                    if request.page == 1 {
                                        ProductSupplierModel.deleteAll()
                                    }
                                    
                                    let productList = json!["datas"] as! [[String:Any]]
                                    let productCount = json!["totalRecords"] as? Int
                                    for product in productList{
                                        let productModel = Mapper<ProductSupplierModel>().map(JSONObject:product)
                                        RealmDBManager.sharedManager.addOrUpdateObject(object: productModel!)
                                        
                                    }
                                    let productModels = ProductSupplierModel.fetchProductModels()
                                    completionHandler(productModels,productCount,ProfileRLMModel.fetchProfile()!)
                                    
                                    
                                    
                                case 4:
                                  
                                    do{
                                        let decoder = JSONDecoder()
                                        let supplierCount = json!["totalRecords"] as? Int
                                        let responseSuppliers = try decoder.decode(SupplierModel.self, from: responseData!)
                                        let suppliersArray = responseSuppliers.datas
                                        completionHandler(suppliersArray as [AnyObject]?,supplierCount, ProfileRLMModel.fetchProfile()!)
                                    }catch{
                                        
                                    }
                                    
                                case 5:
                                    do{
                                        let decoder = JSONDecoder()
                                        let responseSoftware = try decoder.decode(SoftwareResponseModel.self, from: responseData!)
                                        let softwareArray = responseSoftware.datas
                                        completionHandler(softwareArray as [AnyObject]?,0, ProfileRLMModel.fetchProfile()!)
                                    }catch{
                                        
                                    }
                                    
                                case 6:
                                    do{
                                        let decoder = JSONDecoder()
                                        let serviceCount = json!["totalRecords"] as? Int
                                        let responseServices = try decoder.decode(ServiceResponseModel.self, from: responseData!)
                                        let serviceArray = responseServices.datas
                                        completionHandler(serviceArray as [AnyObject]?,serviceCount, ProfileRLMModel.fetchProfile()!)
                                    }catch{
                                        
                                    }
                                    
                                    //                                    let jobList = json!["datas"] as! [[String:Any]]
                                    //                                    for job in jobList{
                                    //                                        let jobModel = Mapper<ProductSupplierModel>().map(JSONObject:job )
                                    //                                        RealmDBManager.sharedManager.addOrUpdateObject(object: jobModel!)
                                    //
                                    //                                    }
                                    //                                    let productModels = ProductSupplierModel.fetchProductModels()
                                    //                                    completionHandler(productModels,ProfileRLMModel.fetchProfile()!)
                                    
                                    
                                default:
                                    break
                                }
                                
                                
                                
                                
                            }//end of do
                            catch{
                                print(error)
                            }
                        })
                    }
                    
                    
                    
                }catch{
                    STOP_LOADING_VIEW()
                    KEY_WINDOW?.makeToast(error.localizedDescription)
                    print("error")
                }
            }
            
            
        }) { (response, statusCode) in
            STOP_LOADING_VIEW()
           //  KEY_WINDOW?.makeToast(UNKNOWN_ERROR_MSG)
        }
    }
    
    
    func fetchConnectionsFromServer(connectionType:String,completionHandler: @escaping(_ message : String? ,_ connections : Connections.FetchConnections.Response?) -> Void){
        START_LOADING_VIEW()
        ServiceManager.methodType(requestType: GET_REQUEST, url: GET_COUNT_API(connectionType), params: nil, paramsData: nil, completion: { (response,_ responseData, statusCode) in
            STOP_LOADING_VIEW()
            let decoder = JSONDecoder()
            do{
                //                print("Response issss ---->",response)
                let responseConnections = try decoder.decode(Connections.FetchConnections.Response.self, from: responseData!)
                completionHandler("", responseConnections)
            }catch{
                
                //                completionHandler((response as! [String : Any])["message"] as? String, nil)
            }
            
        }) { (response, statusCode) in
            STOP_LOADING_VIEW()
        }
    }
    
    func fetchListOfUser(postId:Int,completionHandler: @escaping(_ message : String? ,_ connections : Connections.FetchConnections.Response?) -> Void){
        START_LOADING_VIEW()
        ServiceManager.methodType(requestType: GET_REQUEST, url: GET_LIKE_USERS_URL(postId), params: nil, paramsData: nil, completion: { (response,_ responseData, statusCode) in
            STOP_LOADING_VIEW()
            let decoder = JSONDecoder()
            do{
                let responseConnections = try decoder.decode(Connections.FetchConnections.Response.self, from: responseData!)
                completionHandler("",responseConnections)
            }catch{
                //                completionHandler((response as! [String : Any])["message"] as? String, nil)
                print("error")
            }
            
        }) { (response, statusCode) in
            STOP_LOADING_VIEW()
        }
    }
    
    func likeAPost(request : LikePost.LikeAPost.Request,completionHandler: @escaping(_ response : LikePost.LikeAPost.Response) -> Void) {
        
        let params = ["oid":request.oid ?? 0,"pid":request.pid ?? 0,"type":request.type ?? "like"] as! [String:Any]
        //        START_LOADING_VIEW()
        ServiceManager.methodType(requestType: POST_REQUEST, url: LIKE_POST, params: params as NSDictionary, paramsData: nil,completion: { (response,_ responseData, statusCode) in
            //            STOP_LOADING_VIEW()
            let decoder = JSONDecoder()
            do{
                //                print("Response issss ---->",response)
                let responseOfLike = try decoder.decode(LikePost.LikeAPost.Response.self, from: responseData!)
                completionHandler(responseOfLike)
            }catch{
                
                print("error")
            }
            
        }) { (response, statusCode) in
            
        }
    }
    
    func deleteAPost(postId:Int,completionHandler:@escaping(_ response : LikePost.LikeAPost.Response) -> Void) {
        
        START_LOADING_VIEW()
        ServiceManager.methodType(requestType: DELETE_REQUEST, url: DELETE_POST(postId), params: nil, paramsData: nil,completion: { (response,_ responseData, statusCode) in
            STOP_LOADING_VIEW()
            let decoder = JSONDecoder()
            do{
                let responseOfDelete = try decoder.decode(LikePost.LikeAPost.Response.self, from: responseData!)
                completionHandler(responseOfDelete)
            }catch{
                
                print("error")
            }
            
        }) { (response, statusCode) in
            STOP_LOADING_VIEW()
        }
    }
    
    func getLanguagesFromMicrosoftServer(completionHandler:@escaping(_ response : Translation) -> Void){
       
            let langAddress = "https://dev.microsofttranslator.com/languages?api-version=3.0&scope=translation"
            URLSession.shared.dataTask(with: URL(string: langAddress)!) { (data, response, error) in
            if error != nil {
                print(error)
            } else {
                if let usableData = data {
                    let decoder = JSONDecoder()
                    do{
                        let responseData = try decoder.decode(Translation.self, from: data!)
                        completionHandler(responseData)
                    }catch{
                        print("error")
                    }
                }
            }
            }.resume()
    }
  
         /*   let jsonDecoder1 = JSONDecoder()
            var languages: Translation?
            
            languages = try! jsonDecoder1.decode(Translation.self, from: jsonLangData)
            var eachLangInfo = AllLangDetails(code: " ", name: " ", nativeName: " ", dir: " ") //Use this instance to populate and then append to the array instance
            
            for languageValues in languages!.translation.values {
                eachLangInfo.name = languageValues.name
                eachLangInfo.nativeName = languageValues.nativeName
                eachLangInfo.dir = languageValues.dir
                arrayLangInfo.append(eachLangInfo)
            }
            
            let countOfLanguages = languages?.translation.count
            var counter = 0
            
            for languageKey in languages!.translation.keys {
                
                if counter < countOfLanguages! {
                    arrayLangInfo[counter].code = languageKey
                    counter += 1
                }
            }
            
            arrayLangInfo.sort(by: {$0.name < $1.name}) //sort the structs based on the language name
        } */
    func reportAbuse(model:ReportAbuseModel,completionHandler:@escaping(_ response : CommonModel.BaseModel) -> Void) {
        
        START_LOADING_VIEW()
        let params = ["moduleType":model.moduleType ?? "","mid":model.mid ,"moid":model.moid ,"rcategory":model.rcategory ?? ""] as [String : Any]
        print("Parms report abuse--->",params)
        ServiceManager.methodType(requestType: POST_REQUEST, url: REPORT_ABUSE_API, params: params as NSDictionary, paramsData: nil,completion: { (response,_ responseData, statusCode) in
            STOP_LOADING_VIEW()
            let decoder = JSONDecoder()
            do{
                let response = try decoder.decode(CommonModel.BaseModel.self, from: responseData!)
                completionHandler(response)
            }catch{
                print("error")
            }
            
        }) { (response, statusCode) in
            STOP_LOADING_VIEW()
        }
    }
    
    
    func blockUser(idOfUser: Int,type:String? = "block",completionHandler:@escaping(_ response : CommonModel.BaseModel) -> Void) {
        START_LOADING_VIEW()
        let params = ["type":type ?? "block","fid":idOfUser] as [String : Any]
        ServiceManager.methodType(requestType: POST_REQUEST, url: BLOCK_USER, params: params as NSDictionary, paramsData: nil,completion: { (response,_ responseData, statusCode) in
            STOP_LOADING_VIEW()
            let decoder = JSONDecoder()
            do{
                let response = try decoder.decode(CommonModel.BaseModel.self, from: responseData!)
                completionHandler(response)
            }catch{
                print("error")
            }
            
        }) { (response, statusCode) in
            STOP_LOADING_VIEW()
        }
    }
}
    

