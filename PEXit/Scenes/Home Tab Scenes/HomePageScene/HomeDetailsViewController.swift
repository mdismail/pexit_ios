//
//  HomeDetailsViewController.swift
//  PEXit
//
//  Created by Apple on 22/08/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import Presentr
import AVKit
import SafariServices
import Material

class HomeDetailsViewController: UIViewController , UITableViewDataSource, UITableViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout{

    @IBOutlet weak var lblTitle: UILabel!
  //  @IBOutlet weak var parentScrollView: UIScrollView!
    @IBOutlet weak var txtViewPost: UITextView!
    @IBOutlet weak var tblMedia: UITableView!
  //  @IBOutlet weak var constraintTblMediaHeight: NSLayoutConstraint!
    var postModel : PostRLMModel!
    var youTubeArray = [String]()
    var linksArray = [String]()
    var arrayLangInfo = [AllLangDetails]()

   // var collectionViewType : TypeOfAttatchment!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addBackButton(withTitle: "Post Details")
        if !(postModel.link?.isEmpty)!{
            linksArray = (postModel.link?.components(separatedBy: ","))!
            linksArray = linksArray.filter{$0 != ""}
        }
        if postModel.template == 2{
            if !(postModel.youtube?.isEmpty)!{
                youTubeArray = (postModel.youtube?.components(separatedBy: ","))!
                youTubeArray = youTubeArray.filter{$0 != ""}

            }
        }else{
            if !(postModel.video?.isEmpty)!{
                youTubeArray = (postModel.video?.components(separatedBy: ","))!
                youTubeArray = youTubeArray.filter{$0 != ""}

            }
        }
        setUpView()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        guard let headerView = tblMedia.tableHeaderView else {
            return
        }
        let size = headerView.systemLayoutSizeFitting(UILayoutFittingCompressedSize)
        
        if headerView.frame.size.height != size.height {
            headerView.frame.size.height = size.height
            
            tblMedia.tableHeaderView = headerView
            tblMedia.tableFooterView = UIView()
            tblMedia.layoutIfNeeded()
        }
    }
    
    func setUpView(){
        lblTitle.text = postModel.title
//        txtViewPost.text =
//    (postModel.post?.stringByStrippingHTMLTags)?.stringByDecodingHTMLEntities
        if let postDesc = postModel.post?.getHtmlAttributedStr(){
            txtViewPost.attributedText = postDesc
            
        }
        tblMedia.tableFooterView = UIView()
        tblMedia.reloadData()
        }
    
    @IBAction func btnTranslateClicked(_ sender: Any) {
        showListOfLanguages()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 6
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0{
            return 40
        }
        return 100
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int){
        view.tintColor = UIColor.clear
        let header = view as! UITableViewHeaderFooterView
        header.textLabel?.textColor = THEME_RED_COLOR
        header.textLabel?.text =  header.textLabel?.text?.capitalized
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section{
        case 0:
            return linksArray.count > 0 ? "Links" : ""
        case 1:
            return postModel.image.count > 0 ? "Images" : ""
        case 2:
            return postModel.allvid.count > 0 ?"Videos" : ""
        case 3:
            return postModel.docu.count > 0 ?"Documents" : ""
        case 4:
            return postModel.ppt.count > 0 ?"Presentations" : ""
        case 5:
            return youTubeArray.count > 0 ?"Youtube Videos" : ""
        default:
            return "Media"
            
        }
       
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section{
        case 0:
            return linksArray.count > 0 ? linksArray.count : 0
        case 1:
            return postModel.image.count > 0 ? 1 : 0
        case 2:
            return postModel.allvid.count > 0 ? 1 : 0
        case 3:
            return postModel.docu.count > 0 ? 1 : 0
        case 4:
            return postModel.ppt.count > 0 ? 1 : 0
        case 5:
            return youTubeArray.count > 0 ? 1 : 0
        default:
            return 0
            
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section{
        case 0:
            return linksArray.count > 0 ? 30 : 0
        case 1:
            return postModel.image.count > 0 ? 30 : 0
        case 2:
            return postModel.allvid.count > 0 ? 30 : 0
        case 3:
            return postModel.docu.count > 0 ? 30 : 0
        case 4:
            return postModel.ppt.count > 0 ? 30 : 0
        case 5:
            return youTubeArray.count > 0 ? 30 : 0
        default:
            return 0
            
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "linkCell", for: indexPath)
            cell.textLabel?.text = linksArray[indexPath.row]
            cell.textLabel?.setTextAsLink()
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "homeDetailCell", for: indexPath) as! HomeDetailTableViewCell
            cell.mediaCollectionView.register(UINib(nibName: "HomeDetailCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "homeDetailsCell")
            cell.mediaCollectionView.dataSource = self
            cell.mediaCollectionView.delegate = self
            cell.mediaCollectionView.tag = indexPath.section
            cell.mediaCollectionView.reloadData()
            return cell
        }
       
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0{
             openLinks(linkUrl:linksArray[indexPath.row])
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        switch collectionView.tag{
        case 0:
            return linksArray.count
        case 1:
            return postModel.image.count
        case 2:
            return postModel.allvid.count
        case 3:
            return postModel.docu.count
        case 4:
            return postModel.ppt.count
        case 5:
            return youTubeArray.count
        default:
            return 0
        }

    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "homeDetailsCell", for: indexPath) as! HomeDetailCollectionViewCell
        switch collectionView.tag{
        case 0:
            cell.setUpViewForLink(link: linksArray[indexPath.row])
        case 1:
            cell.setUpViewForImage(url:postModel.image[indexPath.row])
        case 2:
            cell.setUpViewForVideo(videoUrl: postModel.allvid[indexPath.row])
        case 3:
            cell.setUpViewForImage(isFile : true)
        case 4:
            cell.setUpViewForImage(isPresentation: true)
        case 5:
             cell.setUpViewForYouTubeVideo(youTubeVideoUrl: youTubeArray[indexPath.row])
        default :
            print("Default Statement")
        }

        return cell
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch collectionView.tag{
        case 0 :
            let size = (linksArray[indexPath.row]).size(withAttributes: nil)
            return CGSize(width:size.width + 50,height:30)
        case 1,2,3,4:
            return CGSize.init(width: collectionView.frame.size.width / 3, height: collectionView.frame.size.height)
        case 5:
            return CGSize.init(width: collectionView.frame.size.width / 2, height: collectionView.frame.size.height)
            
        default:
            return CGSize.init(width: collectionView.frame.size.width / 3, height: collectionView.frame.size.height)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch collectionView.tag{
        case 0:
            openLinks(linkUrl:linksArray[indexPath.row])
           
        case 1:
            let photoController = AppToolbarController(rootViewController: PhotoViewController(imagesArray:postModel.image,index: indexPath.row))
            present(photoController, animated: true, completion: nil)

           // cell.setUpViewForImage(url:postModel.image[indexPath.row])
        case 2:
             openVideoPlayer(videoUrl: postModel.allvid[indexPath.row])
          
        case 3:
            showAttatchmentView(mediaUrl: postModel.docu[indexPath.row])
        case 4:
             showAttatchmentView(mediaUrl: postModel.ppt[indexPath.row])
        case 5:
            print("You Tube Video")
           
        default :
            print("Default Statement")
       
        }
    }

}


extension HomeDetailsViewController : ItemSelected{
    //MARK: - LANGUAGE TRANSLATION
    
    func showListOfLanguages(){
        getLanguages { (languagesArray,completeLanguageDetails) in
            self.arrayLangInfo = completeLanguageDetails
            self.presentLanguagesView(languageArray: languagesArray)
        }
    }
    
   
    
    func presentLanguagesView(languageArray : [String]){
        DispatchQueue.main.async {
            let presenter = Presentr(presentationType: .popup)
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "commonViewController") as! CommonViewController
            // controller.isFromHome = true
            controller.isLanguageUpdation = true
            controller.itemsArray = languageArray
            controller.delegate = self
            self.customPresentViewController(presenter, viewController: controller, animated: true)
        }
    }
    
    func optionSelected(option: String?) {
        CURRENT_APP_LANGUAGE = arrayLangInfo.filter{$0.name == option}[0].code
        SELECTED_LANGUAGE = arrayLangInfo.filter{$0.name == option}[0].name
        translateToCurrentSelectedLanguage()
    }
   
    func translateToCurrentSelectedLanguage(){
        let translationWorker = LanguageTranslationWoker()
//        var lblArray = [txtViewPost]
        // let serviceDesc = softwareModel?.descrip?.htmlDecodedText()
        let postDesc = postModel?.post
        translationWorker.translateCurrentString(newLanguageCode: CURRENT_APP_LANGUAGE, stringsToTranslate: [postDesc ?? ""],textType:"html") { (response) in
            if response != nil{
                if let translations = response as? [ReturnedJson]{
                    DispatchQueue.main.async {
//                        var i = 0
                        for value in translations{
                            
                            for translatedStr in value.translations{
                                if let postDesc = translatedStr.text.getHtmlAttributedStr(){
                                    self.txtViewPost.attributedText = postDesc
                                    
                                }
//                                // lblArray[i]?.text = translatedStr.text
//                                i = i + 1
                                
                                
                            }
                            
                        }
                    }
                }
            }
        }
    }
}
