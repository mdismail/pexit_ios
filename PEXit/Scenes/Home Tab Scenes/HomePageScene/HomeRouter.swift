//
//  HomeRouter.swift
//  PEXit
//
//  Created by Apple on 27/07/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation
import UIKit
protocol HomeRouterInput{
    func navigateToPostDetail(indexPath : IndexPath)
}

class HomeRouter:NSObject,HomeRouterInput,UIPopoverPresentationControllerDelegate{
    
    weak var controller : HomePageViewController!
    
    func navigateToPostDetail(indexPath : IndexPath) {
        controller.performSegue(withIdentifier: "homeToDetail", sender: indexPath)
    }
    
    func navigateToComments(indexPath : IndexPath){
        
        controller.performSegue(withIdentifier: "homeToComments", sender: indexPath)
    }
    
    func navigateToShare(indexPath : IndexPath){
        controller.performSegue(withIdentifier: "homeToShare", sender: indexPath)
    }
    
    func navigateToPostController(sender:Any){
        controller.performSegue(withIdentifier: "homeToPost", sender: sender)
    }
    
    func navigateToUserDetails(indexPath : IndexPath){
        let currentPostModel = controller.records[indexPath.row]
        controller.isPostSelected = true
        let userID = currentPostModel.uid
        let groupDetailsController = controller.storyboard?.instantiateViewController(withIdentifier: "groupDetailViewController") as! GroupDetailViewController
        groupDetailsController.userId = userID
    controller.navigationController?.pushViewController(groupDetailsController, animated: true)
    }
    
    func navigateToPopUpController(sender:UIButton,optionsArray:[String]?,isReport:Bool?=false){
        
            let popUpController = controller.storyboard?.instantiateViewController(withIdentifier: "commonViewController") as! CommonViewController
            popUpController.modalPresentationStyle = .popover
        if isReport ?? false{
             popUpController.preferredContentSize = CGSize(width : controller.view.frame.size.width / 1.5, height : CGFloat((optionsArray?.count)! * COMMON_CONTROLLER_ROW_HEIGHT) + 15)
        }else{
             popUpController.preferredContentSize = CGSize(width : controller.view.frame.size.width / 2 , height : CGFloat((optionsArray?.count)! * COMMON_CONTROLLER_ROW_HEIGHT) + 15)
        }
        
            let popOver = popUpController.popoverPresentationController
            popOver?.delegate = self
            popOver?.permittedArrowDirections = .up
            popOver?.sourceView = sender
            popOver?.sourceRect = sender.bounds
            popUpController.delegate = controller
            popUpController.itemsArray = optionsArray
            popUpController.isFromHome = true
            popUpController.isReport = isReport ?? false
            controller.present(popUpController, animated: true, completion: nil)
        
    }
    
    
    func passDataToNextScene(segue: UIStoryboardSegue,sender:Any?){
        controller.isPostSelected = true
        if segue.identifier == "homeToDetail"{
            let destinationController = segue.destination as! HomeDetailsViewController
            if let indexPath = sender as? IndexPath{
                destinationController.postModel = controller.records[indexPath.row]
            }
        }
        if segue.identifier == "homeToComments"{
            let destinationController = segue.destination as! CommentViewController
            if let indexPath = sender as? IndexPath{
                destinationController.indexPath = indexPath
                destinationController.delegate = controller
                destinationController.postModel = controller.records[indexPath.row]
            }
        }
        
        if segue.identifier == "homeToPost"{
            let destinationController = segue.destination as! NewPostController
            controller.isNewPost = true
            if let indexPath = sender as? IndexPath{
                destinationController.isFromEdit = true
                destinationController.currentPost = controller.records[indexPath.row]
            }else{
                
            }
        }
        
        if segue.identifier == "homeToShare"{
            let destinationController = segue.destination as! NewPostController
            if let indexPath = sender as? IndexPath{
                destinationController.isFromShare = true
                destinationController.currentPost = controller.records[indexPath.row]
            }
        }
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return .none
    }
    
    
    
}
