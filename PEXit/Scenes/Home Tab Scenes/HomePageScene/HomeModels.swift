//
//  HomeModels.swift
//  PEXit
//
//  Created by Apple on 27/07/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation

struct SearchResult{
    
    struct FetchPosts{
        struct Request{
            var page : Int?
            var records : Int?
            var key : String? = ""
            var flag : Int? = 0
            var type : Int? = 0
            var location : String? = ""
            var isOtherUser : Bool? = false
            var pctype : String? = ""
            var category : String? = ""
            var softwareType : String? = ""
//            var psponsor : String? = ""
//            var pads : String? = ""
        }
        struct Response : Codable{
            var status: Bool?
            var message : String?
            var datas : [PostRecord]?
            var profile : ProfileModel?
        }
        
        struct PostRecord : Codable{
            var post : String?
            var title : String?
            var profileImage : String?
            var docu : [String]?
            var link : String?
            var ppt : [String]?
            var image : [String]?
            var allvid : [String]?
            var name : String?
            var video : String?
            var uid : Int?
            var template : Int?
            var youTubeVideoArray : [String]?
            var linksArray : [String]?
        }
        
        struct ProfileModel : Codable{
            var userName : String?
            var profileTitle : String?
            var profileViews : Int?
            var profileConnections : Int?
            var profilePendings : Int?
            var uid : Int?
        }
        
    }
    
}

struct LikePost{
    
    struct LikeAPost{
        struct Request{
            var oid : Int?
            var pid : Int?
            var type : String? = ""
            
        }
        struct Response : Codable{
            var status: Bool?
            var message : String?
            
        }
        
    }
    
}


struct Connections {
    struct FetchConnections{
        struct Request{
            var connectionType : String?
        }
        
        struct Response : Codable{
            var status: Bool?
            var message : String?
            var datas : [UserRecord]?
        }
        struct UserRecord : Codable{
            var id : Int?
            var username : String?
            var pimage : String?
            var location : String?
            var userstate : String?
            var country : String?
            var friendStatus : Int?
            var blockStatus : Int?
            var title : String?
        }
    }
    
}

struct Translation: Codable {
    var translation: [String: LanguageDetails]
    
}
struct LanguageDetails: Codable {
    var name: String
    var nativeName: String
    var dir: String
   // var code: String
}
