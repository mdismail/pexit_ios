//
//  HomeConfigurator.swift
//  PEXit
//
//  Created by Apple on 27/07/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation
import UIKit

extension HomePageViewController: HomePresenterOutput{
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        router.passDataToNextScene(segue: segue, sender: sender)
    }
}

extension HomeInteractor : HomeViewControllerOutput{
    
    
}

extension HomePresenter: HomeInteractorOutput{
    
}

class HomeConfigurator{
    
    class var sharedInstance : HomeConfigurator{
        struct Static{
            static let instance = HomeConfigurator()
        }
        return Static.instance
    }
   
    
    func configure(viewController : HomePageViewController){
        let router = HomeRouter()
        router.controller = viewController
        
        let presenter = HomePresenter()
        presenter.homePresenterOutput = viewController
        
        let interactor = HomeInteractor()
        interactor.homeInteractorOutput = presenter
        
        viewController.router = router
        viewController.output = interactor
        
        
        
    }
    
    
}
