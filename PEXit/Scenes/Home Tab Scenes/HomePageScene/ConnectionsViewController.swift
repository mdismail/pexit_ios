//
//  ConnectionsViewController.swift
//  PEXit
//
//  Created by Apple on 06/08/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
protocol UserSelected{
    func userSelected(user:Connections.FetchConnections.UserRecord)
}

class ConnectionsViewController: UIViewController , UITableViewDataSource , UITableViewDelegate{
    @IBOutlet weak var tblConnections : UITableView!
    var delegate : UserSelected?
    var connectionResponse : Connections.FetchConnections.Response?
    override func viewDidLoad() {
        super.viewDidLoad()
        tblConnections.register(UINib(nibName: "UserTableViewCell", bundle: nil), forCellReuseIdentifier: "UserTableViewCell")
       // tblConnections.dataSource = self
        //tblConnections.delegate = self
        tblConnections.tableFooterView = UIView()
        // Do any additional setup after loading the view.
    }

 
    func setUpView(connectionResp : Connections.FetchConnections.Response?){
        connectionResponse = connectionResp
        tblConnections.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return connectionResponse?.datas?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserTableViewCell", for: indexPath) as! UserTableViewCell
        cell.lblUserName.text = connectionResponse?.datas![indexPath.row].username
        
        if let imageUrl = URL(string: (connectionResponse?.datas![indexPath.row].pimage)!){
            cell.imgViewUser.makeImageRounded()
            cell.imgViewUser.setImage(from:imageUrl)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let currentUser = connectionResponse?.datas![indexPath.row]
        if let delegate = delegate{
            delegate.userSelected(user: currentUser!)
        }
        dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }

}
