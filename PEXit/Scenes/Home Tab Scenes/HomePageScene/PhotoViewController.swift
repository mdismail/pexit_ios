//
//  PhotoViewController.swift
//  PEXit
//
//  Created by Apple on 23/08/18.
//  Copyright © 2018 Apple. All rights reserved.
//


import UIKit
import Material
import Motion
import SDWebImage

class PhotoViewController: UIViewController {
    fileprivate var closeButton: IconButton!
    fileprivate var collectionView: UICollectionView!
    fileprivate var index: Int
    var dataSourceItems = [DataSourceItem]()
    var imagesArray : [String]?
    
    public required init?(coder aDecoder: NSCoder) {
        index = 0
        super.init(coder: aDecoder)
    }
    
    public init(imagesArray:[String],index: Int) {
        self.imagesArray = imagesArray
        self.index = index
        super.init(nibName: nil, bundle: nil)
    }
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        
        prepareCloseButton()
        preparePhotos()
        prepareCollectionView()
        prepareToolbar()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
}

extension PhotoViewController {
    func prepareCloseButton() {
        closeButton = IconButton(image: Icon.cm.close)
        closeButton.addTarget(self, action: #selector(handleCloseButton(button:)) , for: .touchUpInside)
    }
    
    fileprivate func preparePhotos() {
        
        imagesArray?.forEach{ [weak self, w = view.bounds.width] in
//            let imageData:NSData = NSData(contentsOf: URL(string:$0)!)!
//            guard let image = UIImage(data: imageData as Data) else{
//                return
//            }
            let imageUrl = URL(string:$0)
            self?.dataSourceItems.append(DataSourceItem(data: imageUrl, width: w))
        }
    }
    
    fileprivate func prepareCollectionView() {
        let w = view.bounds.width
        
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        layout.scrollDirection = .horizontal
        layout.itemSize = CGSize(width: w, height: w)
        
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.backgroundColor = .clear
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.isPagingEnabled = true
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.showsVerticalScrollIndicator = false
        collectionView.register(PhotoCollectionViewCell.self, forCellWithReuseIdentifier: "PhotoCollectionViewCell")
        view.layout(collectionView).center(offsetY: -44).width(w).height(w)
        collectionView.scrollRectToVisible(CGRect(x: w * CGFloat(index), y: 0, width: w, height: w), animated: false)
    }
    
    func prepareToolbar() {
        guard let toolbar = toolbarController?.toolbar else {
            return
        }
        
        toolbar.titleLabel.text = "Image"
//        toolbar.detailLabel.text = "July 19 2017"
        toolbar.leftViews = [closeButton]
    }
}

extension PhotoViewController: CollectionViewDataSource {
    @objc
    open func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    @objc
    open func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataSourceItems.count
    }
    
    @objc
    open func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PhotoCollectionViewCell", for: indexPath) as! PhotoCollectionViewCell
        
        guard let imageUrl = dataSourceItems[indexPath.item].data as? URL else {
            return cell
        }
        
       
        cell.imageView.setImage(from:imageUrl)
        cell.imageView.motionIdentifier = "photo_\(indexPath.item)"
        
        return cell
    }
}

extension PhotoViewController: CollectionViewDelegate {}


fileprivate extension PhotoViewController {
    @objc
    func handleCloseButton(button: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}
