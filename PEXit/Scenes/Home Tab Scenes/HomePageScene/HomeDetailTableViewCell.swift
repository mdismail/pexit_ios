//
//  HomeDetailTableViewCell.swift
//  PEXit
//
//  Created by Apple on 22/08/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class HomeDetailTableViewCell: UITableViewCell {

    @IBOutlet weak var mediaCollectionView: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.mediaCollectionView.register(UINib(nibName: "HomeDetailCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "homeDetailsCell")
        // Initialization code
    }
    
    override func prepareForReuse(){
      
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
