//
//  PhotoCollectionViewCell.swift
//  PEXit
//
//  Created by Apple on 23/08/18.
//  Copyright © 2018 Apple. All rights reserved.
//


import UIKit
import Material

class PhotoCollectionViewCell: UICollectionViewCell {
    open var imageView: UIImageView!
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        prepareImageView()
    }
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        prepareImageView()
    }
}

extension PhotoCollectionViewCell {
    fileprivate func prepareImageView() {
        imageView = UIImageView()
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFit
        contentView.clipsToBounds = true
        contentView.layout(imageView).edges()
    }
}
