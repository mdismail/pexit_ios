//
//  HomeInteractor.swift
//  PEXit
//
//  Created by Apple on 27/07/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation

protocol HomeInteractorInput {
     func fetchPosts(request : SearchResult.FetchPosts.Request)
     func updateLikeStatus(request : LikePost.LikeAPost.Request)
}

protocol HomeInteractorOutput{
    func presentFetchResults(response: [PostRLMModel],profile:ProfileRLMModel)
    func passLikeStatus(response : LikePost.LikeAPost.Response)
}

class HomeInteractor : HomeInteractorInput{
    var homeInteractorOutput : HomeInteractorOutput!
    var worker = HomeWorker()
    
    func fetchPosts(request: SearchResult.FetchPosts.Request) {
        worker.fetchPosts(request: request) { [weak self](response,totalCount,profileResponse) in
            (self?.homeInteractorOutput.presentFetchResults(response: response as! [PostRLMModel],profile: profileResponse as! ProfileRLMModel))!
        }
    }
    
    func updateLikeStatus(request : LikePost.LikeAPost.Request){
        worker.likeAPost(request: request) {  [weak self](response) in
            self?.homeInteractorOutput.passLikeStatus(response:response)
        }
    }
}
