//
//  HomePresenter.swift
//  PEXit
//
//  Created by Apple on 27/07/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation
import UIKit

protocol HomePresenterInput {
    func presentFetchResults(response:[PostRLMModel],profile:ProfileRLMModel)
    func passLikeStatus(response : LikePost.LikeAPost.Response)
}
protocol HomePresenterOutput : class{
    func displaySearchedResults(response:[PostRLMModel],profileData:ProfileRLMModel)
    func displayLikeStatus(response: LikePost.LikeAPost.Response)
}


class HomePresenter : HomePresenterInput{
 
    
    weak var homePresenterOutput : HomePresenterOutput!
    
    func presentFetchResults(response:[PostRLMModel],profile:ProfileRLMModel) {
    //    print("Fetched records :------>",response.viewModel?.datas)
//        let postsRecords = response.datas
//        let profileData = response.profile
       
        homePresenterOutput.displaySearchedResults(response: response,profileData: profile)
       
    }
    
    func passLikeStatus(response : LikePost.LikeAPost.Response){
        homePresenterOutput.displayLikeStatus(response: response)
    }
}
