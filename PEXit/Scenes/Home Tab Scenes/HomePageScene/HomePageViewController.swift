//
//  HomePageViewController.swift
//  PEXit
//
//  Created by Apple on 09/07/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import Presentr
import Realm
import Firebase
import Crashlytics

//*****used after parsing to create an array of structs with language information
struct AllLangDetails: Codable {
    var code = String()
    var name = String()
    var nativeName = String()
    var dir = String()
}

protocol HomeViewControllerInput{
    func displaySearchedResults(response:[PostRLMModel],profileData:ProfileRLMModel)
    func displayLikeStatus(response: LikePost.LikeAPost.Response)
    // func displayConnectionResults(response:[SearchResult.FetchPosts.PostRecord],profileResponse: SearchResult.FetchPosts.ProfileModel)
}

protocol HomeViewControllerOutput{
    func fetchPosts(request : SearchResult.FetchPosts.Request)
    func updateLikeStatus(request : LikePost.LikeAPost.Request)
    //func fetchProfileCounts(request : Connections.FetchConnections.Request)
}

class HomePageViewController: UIViewController , HomeViewControllerInput , ItemSelected , UserSelected  , PassCommentDelegate {
    
    var output : HomeViewControllerOutput!
    var router : HomeRouter!
    var records : [PostRLMModel] = []
    var currentTag : Int?
    var othersPost : String?
    var isNewPost = false
    //    var arrayOfHeights = [CGFloat]()
    
    var languageDetailsArray = [LanguageDetails]()
    //var profileModel : ProfileRLMModel?
    var arrayLangInfo = [AllLangDetails]() //array of structs for language info
    @IBOutlet weak var tblPosts: UITableView!
    @IBOutlet weak var btnProfile: ProfileButton!
    @IBOutlet weak var btnProfileViewedBy: MultiLineButton!
    @IBOutlet weak var btnConnections: MultiLineButton!
    @IBOutlet weak var btnPendingInvites: MultiLineButton!
    @IBOutlet weak var viewHeader: UIView!
    var currentPostModel : PostRLMModel?
    var global_keywords = ""
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(HomePageViewController.handleRefresh(_:)),
                                 for: UIControlEvents.valueChanged)
        refreshControl.tintColor = UIColor.red
        
        return refreshControl
    }()
    var isFromNotification = Bool(){
        didSet{
            fetchPosts()
        }
    }
    var isMoreDataAvailable : Bool = true
    var pageNumber = 1
    var currentUserId : Int?
    var isPostSelected =  false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let userInfo = UserInfo(profilePicId: "", userId: 0, headingValue: "", subHeadingValue: "",editProfileLblHidden: false)
        btnProfile.userInfo = userInfo
        tblPosts.delegate = self
        tblPosts.tableFooterView = UIView()
        tblPosts.register(UINib(nibName: "HomeTableViewCell", bundle: nil), forCellReuseIdentifier: "HomeTableViewCell")
        tblPosts.addSubview(refreshControl)
        tblPosts.rowHeight = UITableViewAutomaticDimension
        tblPosts.estimatedRowHeight = 228
        setUpHomeScreen()
        initialSetUp()
        
        
        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        if UIApplication.shared.applicationIconBadgeNumber > 0{
            appNotificationBarButton.badgeString = String(UIApplication.shared.applicationIconBadgeNumber)
        }
        if isPostSelected == false || isNewPost == true{
            pageNumber = 1
            isMoreDataAvailable = true
            records.removeAll()
            tblPosts.reloadData()
            fetchPosts()
        }
        isNewPost = false
        isPostSelected = false
    }
    
    
    override func viewWillDisappear(_ animated: Bool){
        if tblPosts.tableFooterView is LoadingIndicator{
            (tblPosts.tableFooterView as! LoadingIndicator).stopAnimating()
        }
    }
    
    override func showLanguages(){
        getLanguages { (languagesArray,completeLanguageDetails) in
            self.arrayLangInfo = completeLanguageDetails
            self.presentLanguagesView(languageArray: languagesArray)
        }
    }
    
    override func searchLocallyUsingGlobal(searchKeyword : String, searchOptions : String){
        addGlobalSearchRefreshBtn()
        global_keywords = searchKeyword
        isPostSelected = false
        isNewPost = false
    }
    
    override func cleanUpGlobalSearch(previousSearchOption : String?){
        removeGlobalSearchRefreshBtn()
        pageNumber = 1
        global_keywords = ""
    }
    
    func presentLanguagesView(languageArray : [String]){
        DispatchQueue.main.async {
            let presenter = Presentr(presentationType: .popup)
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "commonViewController") as! CommonViewController
            //   controller.isFromHome = true
            controller.isLanguageUpdation = true
            controller.itemsArray = languageArray
            controller.delegate = self
            self.customPresentViewController(presenter, viewController: controller, animated: true)
        }
    }
    
    func setUpHomeScreen(){
        if othersPost != nil{
            addBackButton(withTitle: othersPost?.capitalized)
            viewHeader.isHidden = true
            var headerFrame = viewHeader.frame
            headerFrame.size = CGSize(width : viewHeader.frame.size.width , height : 0)
            viewHeader.frame = headerFrame
            self.view.layoutIfNeeded()
            
        }else{
            viewHeader.isHidden = false
            showNavigationBarWithLeftAndRightBarButtonItems(leftBarButtonImage: "Alert", searchBarButton: "Search", titleViewImage: "Header",showMessages:true)
        }
    }
    
    override func awakeFromNib(){
        super.awakeFromNib()
        HomeConfigurator.sharedInstance.configure(viewController: self)
    }
    
    func passIndexUpdated(indexPath:IndexPath?,currentModel: Comment.CommentModel?){
        if let indexPath = indexPath{
            tblPosts.reloadRows(at: [indexPath], with: .automatic)
        }
    }
    
    func fetchPosts(pageNumber : Int? = 1,numberOfRecords : Int? = 10){
        if pageNumber == 1{
            LOADED_SPONSOR = ""
            LOADED_ADS = ""
        }
        if othersPost == nil{
            DispatchQueue.main.async {
                self.records = PostRLMModel.fetchPostModels()
                //                for _ in self.records{
                //                    self.arrayOfHeights.append(CGFloat(0))
                //                }
                let profileModel = ProfileRLMModel.fetchProfile()
                if profileModel != nil{
                    self.setUpView(profileResponse: profileModel!)
                }
                self.tblPosts.reloadData()
            }
        }
        
        //Generate request to fetch posts from Server
        var request = SearchResult.FetchPosts.Request()
        request.page = pageNumber
        request.records = numberOfRecords
        if !global_keywords.isEmpty{
            let totalKeywords = global_keywords.components(separatedBy:",")
            if totalKeywords.count > 1{
                request.flag = 1
            }else{
                request.flag = 0
            }
        }
        request.key = global_keywords

        if othersPost != nil{
            request.key = othersPost
            request.isOtherUser = true
        }
        output.fetchPosts(request: request)
    }
    
    func likePost(likeStatus: String, currentModel : PostRLMModel){
        var request = LikePost.LikeAPost.Request()
        request.pid = currentModel.id
        request.oid = currentModel.uid
        request.type = likeStatus
        currentPostModel = currentModel
        output.updateLikeStatus(request: request)
    }
    
    func displayLikeStatus(response: LikePost.LikeAPost.Response){
//        print(response)
        DispatchQueue.main.async {
            let realm = RealmDBManager.sharedManager.realm
            realm?.beginWrite()
            if self.currentPostModel?.likeStatus == 1{
                self.currentPostModel?.likeStatus = 0
                self.currentPostModel?.likeCount = (self.currentPostModel?.likeCount)! - 1
            }else{
                self.currentPostModel?.likeStatus = 1
                self.currentPostModel?.likeCount = (self.currentPostModel?.likeCount)! + 1
            }
            try! realm?.commitWrite()
        }
    }
    
    func initialSetUp(){
        btnProfileViewedBy.tag = BUTTON_TAGS.PROFILE_VIEWS.rawValue
        btnConnections.tag = BUTTON_TAGS.CONNECTIONS.rawValue
        btnPendingInvites.tag = BUTTON_TAGS.PENDING_INVITES.rawValue
    }
    
    
    func displaySearchedResults(response:[PostRLMModel],profileData:ProfileRLMModel){
        DispatchQueue.main.async { [weak self] in
           // self?.tblPosts.contentOffset = CGPoint.zero
            if response.count < (NUMBER_OF_RECORDS * (self?.pageNumber)!){
                self?.isMoreDataAvailable = false
            }else{
                self?.isMoreDataAvailable = true
            }
            
            if !profileData.isInvalidated{
                self?.setUpView(profileResponse: profileData)
            }else{
                print("Invalid this time")
            }
            
            if self?.othersPost != nil{
                self?.records.append(contentsOf: response)
            }else{
                self?.records.removeAll()
                self?.records.append(contentsOf: response)
            }
            self?.tblPosts.tableFooterView?.isHidden = true
            //            if self?.pageNumber == 1 {
            //                self?.arrayOfHeights.removeAll()
            //            }
            //            for i in 1 ... (self?.records.count)! {
            //                if (i > self?.arrayOfHeights.count) {
            //                        self?.arrayOfHeights.append(CGFloat(0))
            //                }
            //            }
            self?.tblPosts.reloadData()
        }
        if response.count == 0 && pageNumber == 1{
            displayToast("No records found")
        }
    }
    
    
    func setUpView(profileResponse : ProfileRLMModel){
        let profileView = MultiLineButtonStruct(headerText:String(profileResponse.profileViews),subHeaderText:"Profile Viewed By")
        btnProfileViewedBy.buttonDetails = profileView
        let  pendingConnections = MultiLineButtonStruct(headerText:String(profileResponse.profileConnections),subHeaderText:"My Connections")
        btnConnections.buttonDetails = pendingConnections
        let pendingInvites = MultiLineButtonStruct(headerText:String(profileResponse.profilePendings),subHeaderText:"Pending Invites")
        btnPendingInvites.buttonDetails = pendingInvites
        let userInfo = UserInfo(profilePicId: profileResponse.pimage, userId: profileResponse.uid, headingValue: profileResponse.userName, subHeadingValue: profileResponse.profileTitle,editProfileLblHidden: false)
        btnProfile.userInfo = userInfo
        currentUserId = profileResponse.uid
        Defaults.setUserID(currentUserId!)
    }
    
    @objc func handleRefresh(_ refreshControl : UIRefreshControl){
        refreshView(previousSearchOption : "")
        self.refreshControl.endRefreshing()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            self.tblPosts.contentOffset = CGPoint.zero
        }
    }
    
    override func refreshView(previousSearchOption : String?){
        self.records.removeAll()
        tblPosts.reloadData()
        isMoreDataAvailable = true
        pageNumber = 1
        fetchPosts()
    }
    
    @IBAction func btnInviteContactClicked(_ sender: Any) {
        
        
    }
    
    @IBAction func btnAttachmentClicked(_ sender: Any) {
        router.navigateToPostController(sender: self)
    }
    
    @IBAction func btnShowConncetionsClicked(_ sender: UIButton) {
        //    Crashlytics.sharedInstance().crash()
        self.view.endEditing(true)
        switch BUTTON_TAGS(rawValue: sender.tag)!   {
        case .PROFILE_VIEWS:
            fetchConnections(connectionType: "0")
        case .CONNECTIONS:
            fetchConnections(connectionType: "1")
        case .PENDING_INVITES:
            fetchConnections(connectionType: "2")
            
        default: break
        }
        
    }
    
    func fetchConnections(connectionType : String){
        let worker = HomeWorker()
        worker.fetchConnectionsFromServer(connectionType: connectionType) { [weak self] (message,response) in
            if let response = response{
                if (response.datas?.count)! > 0{
                    self?.loadConnectionsView(response: response)
                }
                else{
                    self?.displayToast(response.message!)
                }
                
            }else{
                self?.displayToast(message ?? UNKNOWN_ERROR_MSG)
            }
            
        }
    }
    
    func loadConnectionsView(response : Connections.FetchConnections.Response){
        DispatchQueue.main.async {
            let presenter = Presentr(presentationType: .popup)
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "connectionsViewController") as! ConnectionsViewController
            controller.connectionResponse = response
            controller.delegate = self
            self.customPresentViewController(presenter, viewController: controller, animated: true)
        }
    }
    
    func userSelected(user:Connections.FetchConnections.UserRecord){
        let groupDetailsController = storyboard?.instantiateViewController(withIdentifier: "groupDetailViewController") as! GroupDetailViewController
        groupDetailsController.userId = user.id
        navigationController?.pushViewController(groupDetailsController, animated: true)
    }
    
    func optionSelected(option : String?){
        let worker = HomeWorker()
        if option == "Edit"{
            let postId = records[currentTag!].id
            router.navigateToPostController(sender: IndexPath(row:self.currentTag!,section:0))
        }
        else if option == "Delete"{
            let postId = records[currentTag!].id
            worker.deleteAPost(postId: postId) { [unowned self] (response) in
                if response.status == true{
                    self.records.remove(at: self.currentTag!)
                    DispatchQueue.main.async {
                        PostRLMModel.deleteAll_withPId(Id: postId)
                        self.tblPosts.reloadData()
                
                    }
                }
                self.displayToast(response.message!)
            }
        }
        else if option == "Report Abuse"{
            showAbuseCategoryOptions { [weak self] (option) in
                self?.showConfirmationOptionToBlock(itemToMarkAbuse: "Service", completion: { (index) in
                    if index == 0{
                        self?.reportAbuse(category: option)
                    }
                })
            }
        }
        else if option == "Block"{
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [weak self] in
               // self.showConfirmationToReportAbuse()
                self?.showConfirmationOptionToBlock(type:"block", completion: { (index) in
                    if index == 0{
                        self?.blockUser()
                    }
                })
            }
        }
        else{
            SELECTED_LANGUAGE = arrayLangInfo.filter{$0.name == option}[0].name
        }
    }
    
//    func showConfirmationToReportAbuse(){
//        self.showCustomAlert(CONFIRMATION_ALERT, message: REPORT_ABUSE_ALERT, okButtonTitle: UIAlertActionTitle.REPORT.rawValue, cancelButtonTitle: UIAlertActionTitle.CANCEL.rawValue) { [unowned self](action) in
//            switch action{
//            case .DELETE:
//               print("User choose to report abuse")
//            case .CANCEL:
//                print("Dismiss Alert")
//            default:
//                print("Test")
//            }
//        }
//    }
    
    func blockUser(){
        let worker = HomeWorker()
        let userToBeBlocked = records[currentTag!].uid
        let idOfPost = records[currentTag!].id
        worker.blockUser(idOfUser: userToBeBlocked) { (response) in
            if response.status == true{
                self.refreshView(previousSearchOption : "")
//                self.records.remove(at: self.currentTag!)
                DispatchQueue.main.async {
                    self.refreshView(previousSearchOption : "")
//                    PostRLMModel.deleteAll_withPId(Id: idOfPost)
//                    self.tblPosts.reloadData()
//
                }
            }
            self.displayToast(response.message!)
        }
    }
    
    func reportAbuse(category : String){
         let worker = HomeWorker()
         let postToBeReported = records[currentTag!]
         let reportAbuseModel = ReportAbuseModel()
         reportAbuseModel.rcategory = category
         reportAbuseModel.moid = postToBeReported.uid
         reportAbuseModel.mid = postToBeReported.id
         var moduleType = ""
         switch records[currentTag!].template{
         case 0:
            moduleType = "post"
         case 1:
            moduleType = "sponsor"
         case 2:
            moduleType = "ads"
         default:
            moduleType = "post"
          }
         reportAbuseModel.moduleType = moduleType
         worker.reportAbuse(model: reportAbuseModel) { (response) in
            if response.status == true{
                self.records.remove(at: self.currentTag!)
                DispatchQueue.main.async {
                    PostRLMModel.deleteAll_withPId(Id: postToBeReported.id)
                    self.tblPosts.reloadData()
                    
                }
            }
            self.displayToast(response.message!)
          }
    }
    
    override func showNotifications(_ sender:UIButton){
        let notificationController = self.storyboard?.instantiateViewController(withIdentifier: "notificationParentViewController")
        self.navigationController?.pushViewController(notificationController!, animated: true)
        
    }
    
    override func showMessages(_ sender:UIButton){
        let notificationController = self.storyboard?.instantiateViewController(withIdentifier: "notificationParentViewController") as! NotificationParentViewController
       
        notificationController.isFromMessageNotification = true
       self.navigationController?.pushViewController(notificationController, animated: true)
    }
}

extension HomePageViewController : UITableViewDataSource , HomeDataDelegate , UITableViewDelegate{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return records.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeTableViewCell", for: indexPath) as! HomeTableViewCell
        cell.currentIndex = indexPath
        cell.delegate = self
        cell.isHidden = false
        if records.count > 0{
            if !records[indexPath.row].isInvalidated{
//                print("Height is --->",records[indexPath.row].imageHeight)
                if currentUserId == records[indexPath.row].uid{
                    cell.setDataOnCell(homeDataModel:records[indexPath.row],isCurrentUser: true,currentHeight: CGFloat(records[indexPath.row].imageHeight))
                }else{
                    cell.setDataOnCell(homeDataModel: records[indexPath.row],isCurrentUser: false,currentHeight: CGFloat(records[indexPath.row].imageHeight))
                }
                cell.btnEditDelete.tag = indexPath.row
            }
            
        }
        
        
        
        //  cell.layoutIfNeeded()
        
        return cell
    }
    
    
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
//        let dataModel = records[indexPath.row]
//        
//        if dataModel.template != 2{
//            cellTaped(indexPath: indexPath)
//        }else{
//            if let adLink = dataModel.url{
//                if let adURL = URL(string:(adLink.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!)){
//                UIApplication.shared.open(adURL, options: [:], completionHandler: nil)
//            }
//           
//        }
//    }
//    }
    
    func btnMoreTapped(indexPath: IndexPath,sender:UIButton) {
        currentTag = sender.tag
        if currentUserId == records[indexPath.row].uid{
            router.navigateToPopUpController(sender: sender,optionsArray: ["Edit","Delete"])
        }else{
            if records[indexPath.row].shareWithKey == "all" || records[indexPath.row].template == 1 || records[indexPath.row].template == 2{
                router.navigateToPopUpController(sender: sender,optionsArray: ["Report Abuse"],isReport: true)
            }else{
              router.navigateToPopUpController(sender: sender,optionsArray: ["Block","Report Abuse"],isReport: true)
            }
            
        }
        
    }
    
    func btnViewProfileTapped(indexPath : IndexPath){
        let dataModel = records[indexPath.row]
        if dataModel.hide_id == 1{
            return
        }
        router.navigateToUserDetails(indexPath:indexPath)
    }
    
    func btnMediaTapped(indexPath: IndexPath) {
        cellTaped(indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        
        if isMoreDataAvailable == true && (indexPath.row == records.count - 1){
            isMoreDataAvailable = false
            pageNumber = pageNumber + 1
            
            tableView.tableFooterView = LoadingIndicator.init(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44)))
            tableView.tableFooterView?.isHidden = false
            fetchPosts(pageNumber: pageNumber, numberOfRecords: NUMBER_OF_RECORDS)
        }
    }
    
    func cellTaped(indexPath: IndexPath){
        //        print("Cell is Tappeddddd",indexPath)
       // router.navigateToPostDetail(indexPath: indexPath)
        
        let dataModel = records[indexPath.row]
        
        if dataModel.template != 2{
           router.navigateToPostDetail(indexPath: indexPath)
        }else{
            if let adLink = dataModel.url{
                if let adURL = URL(string:(adLink.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!)){
                    UIApplication.shared.open(adURL, options: [:], completionHandler: nil)
                }
                
            }
        }
        
    }
    
    func cellMediaHeight(indexPath: IndexPath,height:CGFloat){
        // arrayOfHeights[indexPath.row] = height
        // tblPosts.reloadRows(at: [indexPath], with: .none)
        tblPosts.beginUpdates()
        tblPosts.endUpdates()
        
    }
    
    func btnCommentTapped(indexPath : IndexPath){
        router.navigateToComments(indexPath: indexPath)
    }
    
    func btnShareTapped(indexPath : IndexPath){
        router.navigateToShare(indexPath: indexPath)
    }
    
    func btnLikeTapped(indexPath: IndexPath) {
        let currentModel = records[indexPath.row]
        var likeStatus : String?
        if currentModel.likeStatus == 0{
            likeStatus = "like"
            
        }else{
            likeStatus = "unlike"
            
        }
        
        likePost(likeStatus: likeStatus!, currentModel : currentModel)
    }
    
    func btnLikeCountTapped(indexPath : IndexPath){
        let currentModel = records[indexPath.row]
        if currentModel.likeCount > 0 {
            let worker = HomeWorker()
            worker.fetchListOfUser(postId: currentModel.id) { [weak self] (message,response) in
                if let response = response{
                    self?.loadConnectionsView(response: response)
                }else{
                    self?.displayToast(message ?? UNKNOWN_ERROR_MSG)
                }
                
            }
            
        }else{
            self.displayToast("Post not liked yet")
        }
    }
    
    func postImageTapped(indexPath: IndexPath) {
        isPostSelected = true
        let currentModel = records[indexPath.row]
        let photoController = AppToolbarController(rootViewController: PhotoViewController(imagesArray:currentModel.image,index: 0))
        present(photoController, animated: true, completion: nil)
    }
    
    
    @objc func goToProfilePage(){
        let profileController  = self.storyboard?.instantiateViewController(withIdentifier:"SettingsPageViewController")
//        let profileController  = self.storyboard?.instantiateViewController(withIdentifier:"profileSuperViewController")as! ProfileSuperViewController
//        profileController.isFromHome = true
        
        self.navigationController?.pushViewController(profileController!, animated: true)
    }
}
