//
//  NewPostModel.swift
//  PEXit
//
//  Created by Apple on 09/08/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation
import UIKit

struct NewPost{
    struct Request{
        var postatus : Int?
        var poshareWith : String?
        var indmail : String?
        var potitle : String?
        var popost : String?
        var podocu : [MediaAttatchment]?
        var poimage : [MediaAttatchment]?
        var povideo : String?
        var poppt : [MediaAttatchment]?
        var polink : String?
        var pohideId : Int = 0
        var poallvid : [MediaAttatchment]?
        var poedited : Int?
        
    }
    
    struct Response : Codable{
        var status : Bool?
        var message : String?
    }
}

struct ListOfUsersToShare{
    
    struct Response : Codable{
        var status : Bool?
        var message : String?
        var datas : [ShareUserModel]?
    }
    
    struct ShareUserModel : Codable{
        var skey: String?
        var svalue: String?
    }
}


struct ListOfIndividuals{
    
    struct Response : Codable{
        var status : Bool?
        var message : String?
        var datas : [IndividualUserModel]?
    }
    
    struct IndividualUserModel : Codable{
        var id: Int?
        var username: String?
        var email  : String?
    }
}

struct CommonModel{
    struct BaseModel : Codable{
        var status : Bool?
        var message : String?
    }
}
