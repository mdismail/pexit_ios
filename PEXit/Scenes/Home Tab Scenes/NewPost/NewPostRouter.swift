//
//  NewPostRouter.swift
//  PEXit
//
//  Created by Apple on 10/08/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation
import UIKit
import Material


protocol NewPostRouterInput{
    func navigateToPopUpContoller(sender:AnyObject, popUpDirection : UIPopoverArrowDirection?)
}

class NewPostRouter : NSObject,NewPostRouterInput,UIPopoverPresentationControllerDelegate{
    weak var controller : NewPostController!
    func navigateToPopUpContoller(sender: AnyObject , popUpDirection : UIPopoverArrowDirection? = .up) {
        let popUpController = controller.storyboard?.instantiateViewController(withIdentifier: "commonViewController") as! CommonViewController
        popUpController.modalPresentationStyle = .popover
        DispatchQueue.main.async { [weak self] in
            
            if sender is TextField{
                popUpController.preferredContentSize = CGSize(width : (sender as! TextField).frame.size.width , height : CGFloat((self?.controller.shareOptionsArray.count)! * COMMON_CONTROLLER_ROW_HEIGHT))
                popUpController.shareOptionsArray = self?.controller.shareOptionsArray
            }else{
                popUpController.preferredContentSize = CGSize(width : (self?.controller.view.frame.size.width)!/2 + 100 , height : CGFloat((self?.controller.individualUsersArray.count)! * COMMON_CONTROLLER_ROW_HEIGHT))
                popUpController.previousSelectedUsers = (self?.controller.individualSelectedArray)!
                popUpController.individualUsersArray = self?.controller.individualUsersArray
            }
            
            let popOver = popUpController.popoverPresentationController
            popOver?.delegate = self
            popOver?.permittedArrowDirections = popUpDirection!
            popOver?.sourceView = sender as! UIView
            popOver?.sourceRect = sender.bounds
            popUpController.delegate = self?.controller
            
            self?.controller.present(popUpController, animated: true, completion: nil)
        }
        
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return .none
    }

   
}
