//
//  NewPostPresenter.swift
//  PEXit
//
//  Created by Apple on 09/08/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation


protocol NewPostPresenterInput {
     func presentPostResult(response: NewPost.Response)
     func presentShareUsersResult(response: ListOfUsersToShare.Response?)
     func presentIndividualsResult(message:String?,response: ListOfIndividuals.Response?)
}
protocol NewPostPresenterOutput : class{
    func displayPostResult(response:NewPost.Response)
    func displayShareUsersResult(response:[ListOfUsersToShare.ShareUserModel]?)
    func displayIndividualsResult(message:String?,response: [ListOfIndividuals.IndividualUserModel]?)
}

class NewPostPresenter : NewPostPresenterInput{
   
    
    weak var newPostPresenterOutput : NewPostPresenterOutput!
    
    func presentPostResult(response: NewPost.Response){
        newPostPresenterOutput.displayPostResult(response: response)
    }
    
    func presentShareUsersResult(response: ListOfUsersToShare.Response?){
        let usersArray = response?.datas!
        newPostPresenterOutput.displayShareUsersResult(response: usersArray)
    }
    
    func presentIndividualsResult(message:String?,response: ListOfIndividuals.Response?){
       
            let usersArray = response?.datas!
            newPostPresenterOutput.displayIndividualsResult(message:message,response: usersArray)
        
        
    }
}
