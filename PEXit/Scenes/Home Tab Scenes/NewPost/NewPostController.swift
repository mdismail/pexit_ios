 //
//  NewPostController.swift
//  PEXit
//
//  Created by Apple on 08/08/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import Material

 
 let SHARE_KEY_FOR_INDIVIDUALS = "eid:"

protocol NewPostControllerInput{
    func displayPostResult(response:NewPost.Response)
    func displayShareUsersResult(response:[ListOfUsersToShare.ShareUserModel]?)
    func displayIndividualsResult(message:String?,response: [ListOfIndividuals.IndividualUserModel]?)
}

protocol NewPostControllerOutput{
    func sendPostToServer(apiName:String,request:NewPost.Request)
    func fetchShareUsers()
    func fetchIndividuals()
}


 class NewPostController: UIViewController , PassAttatchmentsToController , NewPostControllerInput , UITextFieldDelegate , ItemSelected , UITextViewDelegate,DeleteCurrentAttatchment  {
  
  private lazy var attachmentController: AttatchmentViewController = {
        var viewController = storyboard?.instantiateViewController(withIdentifier: "attachmentController") as! AttatchmentViewController
        viewController.delegate = self
        self.add(asChildViewController: viewController)
        return viewController
    }()
    @IBOutlet weak var txtFieldTitle: UITextField!
    @IBOutlet weak var heightConstraintAttatchmentView: NSLayoutConstraint!
    
    @IBOutlet weak var heightConstraintShareAttatchmetsView: NSLayoutConstraint!
    @IBOutlet weak var viewAttatchment: UIView!
    @IBOutlet weak var txtViewDescription: UITextView!
    @IBOutlet weak var constraintHeightDesc: NSLayoutConstraint!
    //@IBOutlet weak var txtFieldDescription: TextField!
    @IBOutlet weak var txtFieldShare: TextField!
    @IBOutlet weak var btnProfile: ProfileButton!
    @IBOutlet weak var collectionViewUsers: UICollectionView!
    @IBOutlet weak var collectionViewAttachmentsForShare: UICollectionView!
    @IBOutlet weak var btnHideIdentity: UIButton!
    @IBOutlet weak var btnShowIndividuals: UIButton!
    @IBOutlet weak var heightCollectionViewUsers: NSLayoutConstraint!
    
    @IBOutlet weak var constraintDescriptionConstant: NSLayoutConstraint!
    @IBOutlet weak var btnEdit: UIButton!
    var hideStatus = 0
    var output : NewPostControllerOutput!
    var router: NewPostRouter!
    var shareOptionsArray = [ListOfUsersToShare.ShareUserModel]()
    var individualUsersArray = [ListOfIndividuals.IndividualUserModel]()
    var individualSelectedArray = [ListOfIndividuals.IndividualUserModel]()
    var currentPost : PostRLMModel?
    var isFromEdit = false
    var isFromShare = false
    var shareWithId : String?
    var mediaArray = [String]()
    var youtubevideoArray = [String]()
    var linksArray = [String]()
    var count = 0
    var isMoreImageAvailable = true
    var isMoreDocuAvailable = true
    var isMorePptAvailable = true
    var isMoreVideoAvailable = true
    var isMoreyoutubeAvailable = true
    var isMorelinksAvailable = true
    override func viewDidLoad() {
        super.viewDidLoad()
        output.fetchShareUsers()
        setUpUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        IQKeyboardManager.shared().previousNextDisplayMode = .alwaysHide
    }
    
    override func viewWillDisappear(_ animated: Bool) {
         IQKeyboardManager.shared().previousNextDisplayMode = .alwaysShow
    }

    override func awakeFromNib() {
        
        NewPostConfigurator.sharedInstance.configure(viewController: self)
    }
    
    func setUpUI(){
        self.showNavigationBar()
        btnEdit.isHidden = true
        txtViewDescription.leftAlignTextView()
       // constraintDescriptionConstant.constant = 15
        txtFieldShare.delegate = self
        addBackButton(withTitle:"Post")
        let profileModel = ProfileRLMModel.fetchProfile()
        if profileModel != nil{
            let userInfo = UserInfo(profilePicId: profileModel?.pimage, userId: profileModel?.uid, headingValue: profileModel?.userName, subHeadingValue: profileModel?.profileTitle)
            btnProfile.userInfo = userInfo
            
        }
        txtFieldTitle.delegate = self
        txtViewDescription.delegate = self 
//        txtViewDescription.placeholder = textViewPlaceholder
//        txtViewDescription.updateTextViewPlacholder()
        collectionViewUsers.isHidden = true
        btnShowIndividuals.isHidden = true
        collectionViewUsers.register(UINib(nibName: "JobSearchCell", bundle: nil), forCellWithReuseIdentifier: "JobSearchCell")
       
        if isFromEdit{
            setUpViewForEditPost(userPost:currentPost!)
        }
        if isFromShare{
            setUpViewForShare(userPost:currentPost!)
        }
        addSubView(asChildViewController: attachmentController)

    }
    
    func addSubView(asChildViewController viewController: UIViewController) {
        // Add Child View Controller
        addChildViewController(viewController)
        // Add Child View as Subview
        viewAttatchment.addSubview(viewController.view)
        // Configure Child View
        viewController.view.frame = viewAttatchment.bounds
        viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        // Notify Child View Controller
        viewController.didMove(toParentViewController: self)
    }
 
    func setUpViewForEditPost(userPost:PostRLMModel){
        attachmentController.currentPostModel = userPost
        attachmentController.isFromEdit = true
        txtFieldTitle.text = userPost.title
        txtViewDescription.text = userPost.post?.htmlDecodedText()
        txtViewDescription.updateTextViewPlacholder()
//        txtViewDescription.textViewDidChange(txtViewDescription)
        if userPost.hide_id == 1{
            btnHideIdentity.isSelected = true
            hideStatus = 1
        }
        
        addPreviousIndividuals()
    }
    
    
    func addPreviousIndividuals(){
        if currentPost?.individualUsers.count > 0{
            for userModel in (currentPost?.individualUsers)!{
                var individualUserModel = ListOfIndividuals.IndividualUserModel()
                individualUserModel.username = userModel.name
                individualUserModel.email = userModel.email
                individualSelectedArray.append(individualUserModel)
                collectionViewUsers.isHidden = false
                heightCollectionViewUsers.constant = 40
                collectionViewUsers.register(UINib(nibName: "JobSearchCell", bundle: nil), forCellWithReuseIdentifier: "JobSearchCell")
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    self.collectionViewUsers.reloadData()
                    
                }
            }
        }
    }
    
    func setUpViewForShare(userPost : PostRLMModel){
         btnHideIdentity.isHidden = true
         //btnEdit.isHidden = false
        // constraintDescriptionConstant.constant = 60
         txtFieldTitle.text = userPost.title
         txtViewDescription.text = userPost.post?.htmlDecodedText()
         txtViewDescription.updateTextViewPlacholder()
         txtFieldTitle.isUserInteractionEnabled = false
//         txtViewDescription.isUserInteractionEnabled = false
         attachmentController.isFromShare = true
         mediaArray.append(contentsOf: userPost.image)
         mediaArray.append(contentsOf: userPost.docu)
         mediaArray.append(contentsOf: userPost.ppt)
         mediaArray.append(contentsOf:userPost.allvid)
         if !(userPost.link?.isEmpty)!{
            linksArray = (userPost.link?.components(separatedBy: ","))!
            mediaArray.append(contentsOf: linksArray)
         }
         if userPost.template == TEMPLATE_TYPES.POST_TEMPLATE.rawValue && !(userPost.video?.isEmpty)!{
            youtubevideoArray = (userPost.video?.components(separatedBy: ","))!
            mediaArray.append(contentsOf: youtubevideoArray)
//            youtubevideoArray.append(contentsOf: youTubeVideoArray!)
         }

        if userPost.template == TEMPLATE_TYPES.ADVERTISMENT_TEMPLATE.rawValue && !(userPost.youtube?.isEmpty)!{
            let youTubeVideoArray = userPost.youtube?.components(separatedBy: ",")
            mediaArray.append(contentsOf:youTubeVideoArray!)
            youtubevideoArray.append(contentsOf: youTubeVideoArray!)
        }
       
         collectionViewAttachmentsForShare.register(UINib(nibName: "HomeDetailCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "homeDetailsCell")
            if mediaArray.count > 0{
                heightConstraintShareAttatchmetsView.constant = 100
                collectionViewAttachmentsForShare.reloadData()
            }
         //   addPreviousIndividuals()
        }
   
    @IBAction func btnEditClicked(_ sender: Any) {
        txtViewDescription.isUserInteractionEnabled = true
    }
    
    @objc func goToProfilePage(){
        let profileController  = self.storyboard?.instantiateViewController(withIdentifier:"SettingsPageViewController")
//        let profileController  = self.storyboard?.instantiateViewController(withIdentifier:"profileSuperViewController") as! ProfileSuperViewController
//        profileController.isFromHome = true
        self.navigationController?.pushViewController(profileController!, animated: true)
    }
    
    
   func sendAttatchments(imageArray : [MediaAttatchment]?,videoArray:[MediaAttatchment]?,fileArray:[MediaAttatchment]?,pptArray:[MediaAttatchment]?,linksString:String?,youTubeLinkString:String?) {
      //  if Utils.validateFormAndShowToastWithRequiredFields([txtFieldTitle]){
        if txtFieldTitle.text != nil && !(txtFieldTitle.text?.isEmpty)!{
            if txtViewDescription.text.isEmpty || txtViewDescription.text.isEqual(textViewPlaceholder){
                displayToast("Please enter Description")
                return
            }
            var request = NewPost.Request()
            request.postatus = 1
            request.indmail = individualSelectedArray.count > 0 ? individualSelectedArray.map {$0.email! }.joined(separator: ",") : ""
            request.potitle = txtFieldTitle.text
            request.popost = txtViewDescription.text
            request.poimage = imageArray
            request.pohideId = hideStatus
           
            request.polink = linksString
            request.povideo = youTubeLinkString
            request.poallvid = videoArray
            request.podocu = fileArray
            request.poppt = pptArray
            request.poshareWith =  shareWithId
            if isFromShare{
                request.poedited = 0
                if currentPost?.post != txtViewDescription.text{
                    request.poedited = 1
                }
                output.sendPostToServer(apiName:SHARE_POST((currentPost?.id)!),request: request)
            }
            else if isFromEdit{
                output.sendPostToServer(apiName:UPDATE_POST((currentPost?.id)!),request: request)
            }
            else{
                output.sendPostToServer(apiName:SEND_POST,request: request)
            }
            
        }else{
            displayToast("Please enter Title")
    }
    
    }
    
    func updateHeightOfParentController(height : CGFloat){
        heightConstraintAttatchmentView.constant = height + 100
    }
    
    func displayPostResult(response: NewPost.Response) {
        print("Response is---->",response)
        displayToast(response.message!)
        self.backAction()
    }
    
    func displayShareUsersResult(response:[ListOfUsersToShare.ShareUserModel]?){
        
        if let response = response , response.count > 0{
            shareOptionsArray = response
            if currentPost != nil && isFromEdit{
                DispatchQueue.main.async {
                if self.currentPost?.shareWithKey != nil && !(self.currentPost?.shareWithKey?.isEmpty)!{
                    self.shareWithId = self.currentPost?.shareWithKey
                    self.txtFieldShare.text = self.currentPost?.shareWithValue
                    if self.shareWithId == SHARE_KEY_FOR_INDIVIDUALS{
                        self.updateUIForIndividuals()
                        }
                    }
                }
            }
            
            else{
                DispatchQueue.main.async {
                    self.txtFieldShare.text = self.shareOptionsArray[0].svalue
                    self.shareWithId = self.shareOptionsArray[0].skey
                }
            }
            
        }
     }
    
    func updateUIForIndividuals(){
        collectionViewUsers.isHidden = false
        btnShowIndividuals.isHidden = false
        
        btnHideIdentity.isHidden = true
    }
    
    func shareOptionSelected(shareModel : ListOfUsersToShare.ShareUserModel?){
        txtFieldShare.text = shareModel?.svalue
        shareWithId = shareModel?.skey
        if shareModel?.skey == SHARE_KEY_FOR_INDIVIDUALS{
           updateUIForIndividuals()
        }else{
            collectionViewUsers.isHidden = true
            btnShowIndividuals.isHidden = true
            if individualSelectedArray.count > 0{
                heightCollectionViewUsers.constant = 40
            }else{
                heightCollectionViewUsers.constant = 0
            }
            
            btnHideIdentity.isHidden = false
        }
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == txtFieldShare{
//            self.view.endEditing(true)
            if shareOptionsArray.count > 0{
                router.navigateToPopUpContoller(sender: txtFieldShare)
            }
//            else{
//                output.fetchShareUsers()
//            }
            return false
        }
        else{
            return true
        }
//        DispatchQueue.main.asyncAfter(deadline: .now() + 0.08) {
//            self.dismissKeyboard()
//        }
//        return true
    }
    
    
    
    func fetchIndividuals(){
        output.fetchIndividuals()
    }
    
    func textViewDidChange(_ textView: UITextView) {
        textView.updateTextViewPlacholder()
        
    }
    
   func displayIndividualsResult(message:String?,response: [ListOfIndividuals.IndividualUserModel]?){
        if let response = response{
             print("Response is------>",response)
             individualUsersArray = response
             individualUsersArray = individualUsersArray.filter{$0.id != Defaults.getUserID()}
             router.navigateToPopUpContoller(sender: btnShowIndividuals,popUpDirection:.any)
        }else{
            displayToast(message ?? UNKNOWN_ERROR_MSG)
        }
    }
    
    func individualUserSelected(userModel : [ListOfIndividuals.IndividualUserModel]?){
//        let newIndexPath = IndexPath(item: 0, section: 0)
//        individualSelectedArray.insert(userModel!, at: 0)
//        collectionViewUsers.insertItems(at: [newIndexPath])
        heightCollectionViewUsers.constant = 40
        individualSelectedArray = userModel!
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.collectionViewUsers.reloadData()

        }
    }
    
    @IBAction func btnHideIdentityClicked(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.isSelected{
           hideStatus = 1
        }else{
           hideStatus = 0
        }
    }
    
    @IBAction func btnShowIndividualsClicked(_ sender: Any) {
        if individualUsersArray.count > 0{
            router.navigateToPopUpContoller(sender: btnShowIndividuals,popUpDirection:.any)
        }else{
            fetchIndividuals()
        }
        
    }
    

}

extension NewPostController : UICollectionViewDelegate , UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collectionViewAttachmentsForShare && isFromShare == true{
            switch section{
            case 0:
                return (currentPost?.image.count)!
            case 1:
                return (currentPost?.docu.count)!
            case 2:
                return (currentPost?.allvid.count)!
            case 3:
                return (currentPost?.ppt.count)!
            case 4:
                return linksArray.count
            case 5:
                return youtubevideoArray.count
            default:
                return 0
            }
        }
        if collectionView == collectionViewUsers{
            return individualSelectedArray.count
        }
        return 0
        
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if collectionView == collectionViewAttachmentsForShare && isFromShare == true{
            return 6
        }
        return 1
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        if isFromShare && collectionView == collectionViewAttachmentsForShare{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "homeDetailsCell", for: indexPath) as! HomeDetailCollectionViewCell
            cell.imgViewMedia.image = nil
//            cell.btnDelete.isHidden = false
//            cell.btnDelete.tag = indexPath.row
            cell.delegate = self
            cell.currentIndexPath = indexPath
            switch indexPath.section{
            case 0:
                cell.setUpViewForImage(url:currentPost?.image[indexPath.row])
            case 1:
                cell.setUpViewForImage(isFile : true)
            case 2:
                cell.setUpViewForVideo(videoUrl : currentPost?.allvid[indexPath.row])
            case 3:
                cell.setUpViewForImage(isPresentation : true)
            case 4:
                cell.setUpViewForLink(link :linksArray[indexPath.row])
            case 5:
               cell.setUpViewForYouTubeVideo(youTubeVideoUrl: youtubevideoArray[indexPath.row])
            default:
                print("test")
            }
            return cell

        }
        else {
            let searchCell = collectionView.dequeueReusableCell(withReuseIdentifier: "JobSearchCell", for: indexPath) as! JobSearchCell
            searchCell.lblSearchText.text = individualSelectedArray[indexPath.row].username
            searchCell.lblSearchText.sizeToFit()
            searchCell.btnSearch.tag = indexPath.row
            searchCell.btnSearch.addTarget(self, action: #selector(deleteCell(sender:)), for: UIControlEvents.touchUpInside)
            return searchCell
        }

       
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if isFromShare && collectionView == collectionViewAttachmentsForShare{
            return CGSize.init(width: collectionView.frame.size.width / 3, height: collectionView.frame.size.height)
        }else{
            let size = (individualSelectedArray[indexPath.row].username)?.size(withAttributes: nil)
            return CGSize(width:size!.width + 50,height:40)
        }
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == collectionViewAttachmentsForShare{
            switch indexPath.section{
            case 0:
                let photoController = AppToolbarController(rootViewController: PhotoViewController(imagesArray:(currentPost?.image)!,index: indexPath.row))
                present(photoController, animated: true, completion: nil)
            case 1:
                showAttatchmentView(mediaUrl:(currentPost?.docu[indexPath.row])!)
            case 2:
                openVideoPlayer(videoUrl:(currentPost?.allvid[indexPath.row])!)
            case 3:
                showAttatchmentView(mediaUrl:(currentPost?.ppt[indexPath.row])!)
            case 4:
                openLinks(linkUrl:linksArray[indexPath.row])
            case 5:
                print("You Tube Video")
                
            default :
                print("Default Statement")
                
            }
        }
        
    }
    
    @objc func deleteCell(sender:UIButton){
        individualSelectedArray.remove(at: sender.tag)
        if individualSelectedArray.count == 0{
            heightCollectionViewUsers.constant = 0
        }
        collectionViewUsers.reloadData()
    }
    func deleteCurrentAttatchment(sender : UIButton , indexPath : IndexPath?)
     {
        print("ENTRY")
        if isFromShare{
            
            return
        }
        print("The attachment is -->",sender.tag)
        var currentSection = indexPath?.section
        var fileType = ""
        var fileName = ""
        switch currentSection   {
        case 0:
            fileName = (currentPost?.image[(indexPath?.row)!])!
            fileType = "photos"
        case 1:
            fileName = (currentPost?.docu[(indexPath?.row)!])!
            fileType = "documents"
        case 2:
            fileName = (currentPost?.allvid[(indexPath?.row)!])!
            fileType = "videos"
        case 3:
            fileName = (currentPost?.ppt[(indexPath?.row)!])!
            fileType = "presentations"
        default: break
        }
        print("vdvdfv")
        showCustomAlert(CONFIRMATION_ALERT, message: DELETE_ATTATCHMENT, okButtonTitle: UIAlertActionTitle.DELETE.rawValue, cancelButtonTitle: UIAlertActionTitle.CANCEL.rawValue) { [unowned self](action) in
            print("csc")
            
            switch action{
            case .DELETE:
                self.deleteAttatchment(fileType : fileType , sender : sender , fileName : fileName)
            case .CANCEL:
                print("Dismiss Alert")
            default:
                print("Dismiss Alert")
            }
        }
    }
    
    func deleteAttatchment(fileType : String , sender : UIButton , fileName:String?){
        let worker = NewPostWorker()
        let completeUrl = DELETE_ATTATCHMENT((self.currentPost?.id)!, fileType: fileType, fileName: (URL(string:(fileName?.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!)!)!.lastPathComponent))
        worker.deleteFile(completeURL : completeUrl, completionHandler: { [weak self] (message, responseModel) in
            if let response = responseModel{
                DispatchQueue.main.async {
//                    self?.previousFilesArray.remove(at:sender.tag)
//                    self?.collectionViewPreviousFiles.reloadData()
                    self?.displayToast((response.message)!)
                }
                
            }else{
                self?.displayToast(message!)
            }
        })
    }
    
  
}
