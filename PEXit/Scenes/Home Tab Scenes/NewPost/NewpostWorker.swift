//
//  NewpostWorker.swift
//  PEXit
//
//  Created by Apple on 09/08/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper

class NewPostWorker{
    func sendPostToServer(apiName:String,request: NewPost.Request,completionHandler:@escaping(_ responseModel : NewPost.Response) -> Void){

        START_LOADING_VIEW()
        var params = ["potitle":request.potitle!,"popost":request.popost!,"postatus":1,"poshareWith":request.poshareWith ?? "","indmail":request.indmail! ,"povideo":request.povideo ?? "","pohideId":request.pohideId,"polink":request.polink ?? ""] as [String : Any]
        if request.poedited != nil{
            params["poedited"] = request.poedited
        }
        AlamofireManager.shared().requestWith(apiUrl: apiName, imageData: request.poimage,videoData: request.poallvid,fileData: request.podocu,pptData: request.poppt, parameters: params,isPost: true) { (response, error) in
            STOP_LOADING_VIEW()
            print("Response is--->",response)
            if response != nil{
                if !(((response!["errors"] as? String)?.isEmpty)!) || !((response!["virus"] as? String)?.isEmpty)!{
                    Utils.showErrorFilesAlert(errorFiles : response!["errors"] as? String,corruptFiles : response!["virus"] as? String)
                }
                
                let decoder = JSONDecoder()
                do{
                    let jsonData = try JSONSerialization.data(withJSONObject: response, options: .prettyPrinted)
                    let reqJSONStr = String(data: jsonData, encoding: .utf8)
                    let data = reqJSONStr?.data(using: .utf8)
                    let responsePost = try decoder.decode(NewPost.Response.self, from: data!)
                    completionHandler(responsePost)
                }catch{
                    print("error")
                }
            }else{
                
                STOP_LOADING_VIEW()
//                setToastStatus(flag: false)
                KEY_WINDOW?.makeToast(error?.localizedDescription)
            }
            
            
        }
    }
    
    
    func fetchListOfUsers(completionHandler:@escaping(_ responseModel : ListOfUsersToShare.Response) -> Void){
        
            START_LOADING_VIEW()
            ServiceManager.methodType(requestType: GET_REQUEST, url: SHARE_WITH, params: nil, paramsData: nil,completion: { (response,_ responseData, statusCode) in
                STOP_LOADING_VIEW()
                
                    let decoder = JSONDecoder()
                    do{
                        //                print("Response issss ---->",response)
                        let shareUsersResponse = try decoder.decode( ListOfUsersToShare.Response.self, from: responseData!)
                        completionHandler(shareUsersResponse)
                    }catch{
                        
                        print("error")
                    }
                
                
                
                
            }) { (response, statusCode) in
                STOP_LOADING_VIEW()
            }
    }
    
    func fetchListOfIndividuals(completionHandler:@escaping(_ message:String?,_ responseModel : ListOfIndividuals.Response?) -> Void){
        
        START_LOADING_VIEW()
        ServiceManager.methodType(requestType: GET_REQUEST, url: LIST_OF_INDIVIDUALS, params: nil, paramsData: nil,completion: { (response,_ responseData, statusCode) in
            STOP_LOADING_VIEW()
            let decoder = JSONDecoder()
            do{
                //                print("Response issss ---->",response)
                let individualsUserResponse = try decoder.decode( ListOfIndividuals.Response.self, from: responseData!)
                completionHandler("",individualsUserResponse)
            }catch{
                completionHandler((response as! [String : Any])["message"] as? String, nil)
                print("error")
            }
            
        }) { (response, statusCode) in
            STOP_LOADING_VIEW()
        }
    }
    
    
    func deleteFile(completeURL:String,workProfileType:String?="",completionHandler:@escaping(_ message:String?,_ responseModel : CommonModel.BaseModel?) -> Void){
        START_LOADING_VIEW()
        
        ServiceManager.methodType(requestType: DELETE_REQUEST, url: completeURL, params: nil, paramsData: nil,completion: { (response,_ responseData, statusCode) in
            STOP_LOADING_VIEW()
            let decoder = JSONDecoder()
            if response != nil{
                let decoder = JSONDecoder()
                do{
                    
                   DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                    do{
                    let jsonData = try JSONSerialization.data(withJSONObject: response, options: .prettyPrinted)
                    let reqJSONStr = String(data: jsonData, encoding: .utf8)
                    let data = reqJSONStr?.data(using: .utf8)
                
                if !((workProfileType?.isEmpty)!){
                    let responsePost = try decoder.decode(NewPost.Response.self, from: data!)
                    if responsePost.status == true{
                        //                        let json = try JSONSerialization.jsonObject(with: response!, options: []) as? Dictionary<String , Any>
                        let json = try JSONSerialization.jsonObject(with: jsonData, options: []) as? Dictionary<String , Any>
                        let profileData = json!["datas"] as! [String:Any]
                        switch workProfileType{
                        case WORK_PROFILE_TYPE.Education.rawValue :
                            //                            let profileData = jsonData["datas"] as! [String:Any]
                            let educationModel = Mapper<EducationModel>().map(JSONObject:profileData)
                            RealmDBManager.sharedManager.addOrUpdateObject(object: educationModel!)
                            
                        case WORK_PROFILE_TYPE.Experience.rawValue:
                            //                               let profileData = jsonData["datas"] as! [String:Any]
                            let educationModel = Mapper<ExperienceModel>().map(JSONObject:profileData)
                            RealmDBManager.sharedManager.addOrUpdateObject(object: educationModel!)
                            
                        case WORK_PROFILE_TYPE.Honors.rawValue:
                            //                            let profileData = jsonData["datas"] as! [String:Any]
                            let educationModel = Mapper<HonorsModel>().map(JSONObject:profileData)
                            RealmDBManager.sharedManager.addOrUpdateObject(object: educationModel!)
                            
                        case WORK_PROFILE_TYPE.Company.rawValue:
                            //                            let profileData = jsonData["datas"] as! [String:Any]
                            let educationModel = Mapper<CompanyModel>().map(JSONObject:profileData)
                            RealmDBManager.sharedManager.addOrUpdateObject(object: educationModel!)
                        default:
                            break
                            
                        }
                        
                    }
                    }
                    }catch{
                        print(error)
                    }
                    })
                
                //                print("Response issss ---->",response)
                let deleteFileResponse = try decoder.decode( CommonModel.BaseModel.self, from: responseData!)
                completionHandler("",deleteFileResponse)
            }catch{
                completionHandler((response as! [String : Any])["message"] as? String, nil)
                print("error")
            }
            
            }else{
                
                STOP_LOADING_VIEW()
                //                    setToastStatus(flag: false)
//                KEY_WINDOW?.makeToast(error?.localizedDescription)
            }
            }){ (response, statusCode) in
                STOP_LOADING_VIEW()
        }
}
    
    
    func deleteServiceMedia(completeURL:String,completionHandler:@escaping(_ message:String?,_ responseModel : CommonModel.BaseModel?) -> Void){
  
        START_LOADING_VIEW()
//        print("The complete url is ---->", completeURL)
        ServiceManager.methodType(requestType: DELETE_REQUEST, url: completeURL, params: nil, paramsData: nil,completion: { (response,_ responseData, statusCode) in
//    print("The response url is ---->", response)
        
            STOP_LOADING_VIEW()
            let decoder = JSONDecoder()
            do{
                //                print("Response issss ---->",response)
                let deleteFileResponse = try decoder.decode(CommonModel.BaseModel.self, from: responseData!)
                completionHandler("",deleteFileResponse)
               
            }catch{
                completionHandler((response as! [String : Any])["message"] as? String, nil)
                print("error")
            }
            
        }) { (response, statusCode) in
            STOP_LOADING_VIEW()
        }
    }
    
}


