//
//  NewPostConfigurator.swift
//  PEXit
//
//  Created by Apple on 09/08/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation
import UIKit

extension NewPostController: NewPostPresenterOutput{
  
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//       
//    }
}

extension NewPostInteractor : NewPostControllerOutput{
    
    
}

extension NewPostPresenter: NewPostInteractorOutput{
    
}

class NewPostConfigurator{
    
    class var sharedInstance : NewPostConfigurator{
        struct Static{
            static let instance = NewPostConfigurator()
        }
        return Static.instance
    }
    
    
    func configure(viewController : NewPostController){
        let router = NewPostRouter()
        router.controller = viewController
        
        let presenter = NewPostPresenter()
        presenter.newPostPresenterOutput = viewController
        
        let interactor = NewPostInteractor()
        interactor.output = presenter
        
        viewController.router = router
        viewController.output = interactor
        
        
        
    }
    
    
}
