//
//  NewPostInteractor.swift
//  PEXit
//
//  Created by Apple on 09/08/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation

protocol NewPostInteractorInput {
    func sendPostToServer(apiName:String,request:NewPost.Request)
    func fetchShareUsers()
    func fetchIndividuals()
  
}

protocol NewPostInteractorOutput{
    func presentPostResult(response: NewPost.Response)
    func presentShareUsersResult(response: ListOfUsersToShare.Response?)
    func presentIndividualsResult(message:String?,response: ListOfIndividuals.Response?)
    
}
class NewPostInteractor : NewPostInteractorInput{
    var output : NewPostInteractorOutput!
    var worker = NewPostWorker()
    
    func sendPostToServer(apiName:String,request: NewPost.Request) {
        worker.sendPostToServer(apiName:apiName,request: request) { [weak self] (response) in
            self?.output.presentPostResult(response: response)
        }
    }
    
    func fetchShareUsers(){
        worker.fetchListOfUsers { [weak self] (response) in
            self?.output.presentShareUsersResult(response: response)
        }
    }
    
    func fetchIndividuals(){
        worker.fetchListOfIndividuals { [weak self] (message,response) in
            self?.output.presentIndividualsResult(message:message,response: response)
        }
    }
    
}
