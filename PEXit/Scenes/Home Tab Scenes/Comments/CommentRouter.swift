//
//  CommentRouter.swift
//  PEXit
//
//  Created by Apple on 20/08/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation
import UIKit

protocol CommentRouterInput{
    func navigateToUploadedFile(currentAttatchment : MediaAttatchment)
}

class CommentRouter : NSObject, UIPopoverPresentationControllerDelegate{
    weak var controller : CommentViewController!
    var destinationController : PostCommentController?
//    func navigateToUploadedFile(currentAttatchment : MediaAttatchment){
//        if destinationController != nil{
//            destinationController?.addNewFile(currentFile: currentAttatchment)
//        }
//    }
    
    func passDataToNextScene(segue: UIStoryboardSegue,sender:Any?){
        if segue.identifier == "commentToUploads"{
            destinationController = segue.destination as? PostCommentController
            destinationController?.parentController = controller
            destinationController?.delegate = controller
        }
    }
    
    
    
    func navigateToPopUpContoller(sender: AnyObject , popUpDirection : UIPopoverArrowDirection? = .down) {
        let popUpController = controller.storyboard?.instantiateViewController(withIdentifier: "commonViewController") as! CommonViewController
        popUpController.modalPresentationStyle = .popover
        DispatchQueue.main.async { [weak self] in
            
          popUpController.preferredContentSize = CGSize(width : (self?.controller.view.frame.size.width)!/2 + 100 , height : 50)
          popUpController.itemsArray = ["Translate"]
            
            let popOver = popUpController.popoverPresentationController
            popOver?.delegate = self
            popOver?.permittedArrowDirections = popUpDirection!
            popOver?.sourceView = sender as? UIView
            popOver?.sourceRect = sender.bounds
          //  popUpController.delegate = self?.controller
            
            self?.controller.present(popUpController, animated: true, completion: nil)
        }
        
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return .none
    }
    
    
    
    
}
