//
//  CommentViewController.swift
//  PEXit
//
//  Created by Apple on 07/08/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import MobileCoreServices
import Presentr


protocol PassCommentDelegate{
    func passIndexUpdated(indexPath:IndexPath? , currentModel: Comment.CommentModel?)
}

protocol CommentViewControllerInput{
    func displayComments(response: Comment.CommentResponse)
}

protocol CommentViewControllerOutput{
    func fetchComments(request : Comment.CommentRequest)
}

class CommentViewController: UIViewController , CommentViewControllerInput , UITableViewDataSource , UITableViewDelegate , PostCommentDelegate , ReplyComment{
    var output: CommentViewControllerOutput!
    var router: CommentRouter!
    var postModel : PostRLMModel?
    var commentModels = [Comment.CommentModel]()
    var currentCommentModel : Comment.CommentModel?
    var indexPath : IndexPath?
    var isReplyTapped = false
    @IBOutlet weak var tblComments: UITableView!
    @IBOutlet weak var heightConstraintCommentView: NSLayoutConstraint!
    @IBOutlet weak var tblReply: UITableView!
    @IBOutlet weak var constTblCommentHeight: NSLayoutConstraint!
    @IBOutlet weak var constTblReplyHeight: NSLayoutConstraint!
    @IBOutlet weak var constCommentViewLeading: NSLayoutConstraint!
    @IBOutlet weak var constTblReplyLeading: NSLayoutConstraint!
    var initialCommentViewHeight : CGFloat?
    var delegate : PassCommentDelegate?
    var arrayLangInfo = [AllLangDetails]()
  
    var currentIndex : Int?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addBackButton(withTitle: "Comments")
        initialCommentViewHeight = heightConstraintCommentView.constant
        let extraHeight = (tabBarController?.tabBar.frame.size.height)! + (navigationController?.navigationBar.frame.size.height)!
        constTblCommentHeight.constant = (self.view.frame.size.height - extraHeight - 20) - heightConstraintCommentView.constant
        constTblReplyHeight.constant = 0
        setUpTableView(tableView : tblReply)
        setUpTableView(tableView : tblComments)
        fetchComments()
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
           self.addBackButton(withTitle: "Comments")
    }
        
    func setUpTableView(tableView : UITableView){
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 40
        tableView.tableFooterView = UIView()
        tableView.register(UINib(nibName: "CommentTableViewCell", bundle: nil), forCellReuseIdentifier: "commentCell")
     
    }
    
    override func awakeFromNib() {
         CommentConfigurator.sharedInstance.configure(viewController: self)
    }
    
    override func backAction() {
        if let delegate = delegate{
            delegate.passIndexUpdated(indexPath: indexPath, currentModel: nil)
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- API CALLS
    func fetchComments(){
        var request = Comment.CommentRequest()
        request.pid = postModel?.id
        output.fetchComments(request: request)
        
    }
    
    func sendComment(comment: Comment.PostCommentRequest) {
        if comment.comment == nil || (comment.comment?.isEmpty)!{
            displayToast("Please enter comment")
            return
        }
        self.dismissKeyboard()
        var commentDetails = comment
        commentDetails.oid = postModel?.uid
        commentDetails.pid = postModel?.id
        if commentDetails.type == REPLY_TO_COMMENT{
            commentDetails.comntid = currentCommentModel?.cid
        }
        let worker = CommentWorker()
        worker.postComment(commentRequest: commentDetails,attachments: commentDetails.attachments) { (response) in
            if response.status == true && response.datas != nil{
                let postCommentModel = response.datas!
                self.router.destinationController?.clearData()
                self.heightConstraintCommentView.constant = self.initialCommentViewHeight!
                if commentDetails.type == ADD_NEW_COMMENT{
                    self.commentModels.append(postCommentModel)
                    DispatchQueue.main.async {
                        self.updateCommentCount()
                        self.tblComments.reloadData()
                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+0.1, execute:{
                            self.tblComments.scrollToBottom(animated: true)
                        })
                    }
                }else{
                    if let index = self.commentModels.index(where: { $0.cid == self.currentCommentModel?.cid}) {
                        var model = self.commentModels[index]
                        model.rlyComments?.append(postCommentModel)
                        self.commentModels[index] = model
                    }
                    
                    self.setUpViewAfterReply()
                }
            }
            self.displayToast(response.message!)
        }
    }
    
    
    func updateCommentCount(){
        DispatchQueue.main.async {
            let realm = RealmDBManager.sharedManager.realm
            realm?.beginWrite()
            self.postModel?.commentCount = (self.postModel?.commentCount ?? 0) + 1
            try! realm?.commitWrite()
        }
    }
    
    func displayComments(response: Comment.CommentResponse){
        print(response)
        commentModels = response.datas!
        DispatchQueue.main.async {
            self.tblComments.reloadData()
        }
    }
    
    //MARK:- TABLEVIEW DATASOURCE & DELEGATE
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch tableView {
        case tblComments :
            return isReplyTapped == true ?  1 :  commentModels.count
        case tblReply:
            return currentCommentModel == nil ? 0 : (currentCommentModel?.rlyComments!.count)!
        default:
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let commentCell = tableView.dequeueReusableCell(withIdentifier: "commentCell", for: indexPath) as! CommentTableViewCell
        commentCell.delegate = self
        switch tableView{
            case tblComments :
                if isReplyTapped == false{
                    commentCell.setUpView(commentModel: commentModels[indexPath.row])
                    commentCell.currentIndex = indexPath
                    commentCell.commentDelegate = self
                    currentCommentModel = commentModels[indexPath.row]
                    commentCell.btnReply.isHidden = false
                    
                    if commentModels[indexPath.row].rlyComments?.count > 0{
                        commentCell.btnReply.setTitle("Reply (\(commentModels[indexPath.row].rlyComments?.count ??? "0"))", for: .normal)
                    }else{
                        commentCell.btnReply.setTitle("Reply", for: .normal)
                    }
                    commentCell.btnTranslate.tag = indexPath.row
                    commentCell.btnReply.tag = indexPath.row
                    commentCell.btnTranslate.addTarget(self, action: #selector(translateComment(sender:)), for: .touchUpInside)
                }else{
                    commentCell.setUpView(commentModel: currentCommentModel!)
                }
            
            case tblReply :
                commentCell.setUpView(commentModel:  (currentCommentModel?.rlyComments![indexPath.row])!)
                commentCell.btnReply.isHidden = true
            default :
                print("Default")
        }
        commentCell.collectionViewMedia.register(UINib(nibName: "MediaReplyCell", bundle: nil), forCellWithReuseIdentifier: "mediaCell")
       return commentCell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let currentCommentModel = commentModels[indexPath.row]
        let currentIndex = indexPath.row
        navigateToReplyScreen(commentModel: currentCommentModel, index: currentIndex)
    }
    
    func navigateToReplyScreen(commentModel:Comment.CommentModel,index:Int){
         let replyController = self.storyboard?.instantiateViewController(withIdentifier: "replyViewController") as! ReplyViewController
        replyController.currentCommentModel = commentModel
        replyController.postModel = postModel
        replyController.indexOfCurrentCommment = index
        replyController.delegate = self
        self.navigationController?.pushViewController(replyController, animated: true)
    }
    
    //MARK:- Action Methods
    
    @objc func translateComment(sender : UIButton){
        currentIndex = sender.tag
        showListOfLanguages()
    }
    
    
    func sendReplyClicked(sender:UIButton) {
        self.view.endEditing(true)
        if !isReplyTapped{
            navigateToReplyScreen(commentModel: commentModels[sender.tag], index: sender.tag)
        }
    }
    
    func setUpViewAfterReply(){
        isReplyTapped = false
        constTblCommentHeight.constant = self.view.frame.size.height -  heightConstraintCommentView.constant
        constTblReplyHeight.constant = 0
        router.destinationController?.isReply = false
        constCommentViewLeading.constant = 0
        tblComments.reloadData()
    }
}

extension CommentViewController : PassCommentDelegate , ItemSelected , HomeDataDelegate{
   func passIndexUpdated(indexPath:IndexPath? , currentModel: Comment.CommentModel?) {
        if let indexPath = indexPath{
            if let commentModel = currentModel{
                 commentModels[indexPath.row] = commentModel
            }
           
            tblComments.reloadRows(at: [indexPath], with: .automatic)
        }
    }
    
    func cellTaped(indexPath: IndexPath){
//        let currentCell = tblComments.cellForRow(at: indexPath) as!
//        CommentTableViewCell
//        router.navigateToPopUpContoller(sender: currentCell.lblComment, popUpDirection: .down)
//        getLanguages { (languagesArray,completeLanguageDetails) in
//            self.arrayLangInfo = completeLanguageDetails
//            self.presentLanguagesView(languageArray: languagesArray)
//        }
    }
    
    func showListOfLanguages(){
        getLanguages { (languagesArray,completeLanguageDetails) in
            self.arrayLangInfo = completeLanguageDetails
            self.presentLanguagesView(languageArray: languagesArray)
        }
    }
    
    func presentLanguagesView(languageArray : [String]){
        DispatchQueue.main.async {
            let presenter = Presentr(presentationType: .popup)
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "commonViewController") as! CommonViewController
            // controller.isFromHome = true
            controller.isLanguageUpdation = true
            controller.itemsArray = languageArray
            controller.delegate = self
            self.customPresentViewController(presenter, viewController: controller, animated: true)
        }
    }
    
    func optionSelected(option: String?) {
        CURRENT_APP_LANGUAGE = arrayLangInfo.filter{$0.name == option}[0].code
        SELECTED_LANGUAGE = arrayLangInfo.filter{$0.name == option}[0].name
        translateToCurrentSelectedLanguage()
      
    }
    
    func translateToCurrentSelectedLanguage(){
        let translationWorker = LanguageTranslationWoker()
//        var lblArray = [lblDescription]
        var currentCommentModel = commentModels[currentIndex ?? 0]
        let currentComment = currentCommentModel.comment
        let currentIndexPath = IndexPath.init(row: currentIndex ?? 0, section: 0)
        translationWorker.translateCurrentString(newLanguageCode: CURRENT_APP_LANGUAGE, stringsToTranslate: [currentComment ?? ""]) { (response) in
            if response != nil{
                if let translations = response as? [ReturnedJson]{
                    DispatchQueue.main.async {
                        var i = 0
                        for value in translations{
                            
                            for translatedStr in value.translations{
                               // lblArray[i]?.text = translatedStr.text
                                self.commentModels[self.currentIndex ?? 0].translatedComment = translatedStr.text
//                                currentCommentModel.translatedComment = translatedStr.text
                                self.tblComments.reloadRows(at: [currentIndexPath], with: .automatic)
                                i = i + 1
                                
                                
                            }
                            
                        }
                    }
                }
            }
        }
    }
    
    
    
    
    
}
