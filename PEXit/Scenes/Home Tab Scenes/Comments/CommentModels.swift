//
//  CommentModels.swift
//  PEXit
//
//  Created by Apple on 07/08/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation

struct Comment{
        struct CommentRequest{
            var pid : Int?
        }
        struct CommentResponse : Codable{
            var status: Bool?
            var message : String?
            var datas : [CommentModel]?
        }
        struct CommentModel: Codable{
            var uimage: String?
            var uname : String?
            var comment : String?
            var translatedComment : String?
            var attach : [String]?
            var cid : Int?
            var rlyComments : [CommentModel]?
            
        }
//    struct ReplyModel : Codable{
//        var image: String?
//        var name : String?
//        var comment : String?
//        var attach : [String]?
//    }
    
    struct PostCommentRequest{
        var pid : Int?
        var type : String?
        var oid : Int?
        var comntid : Int?
        var comment : String?
        var attachments : [MediaAttatchment]?
        var images : [MediaAttatchment]?
        var videos : [MediaAttatchment]?
        
    }
    
    struct PostCommentResponse: Codable{
        var status: Bool?
        var message : String?
        var datas : CommentModel?
    }
   
        
}
    

