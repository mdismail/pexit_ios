//
//  WebViewController.swift
//  PEXit
//
//  Created by Apple on 22/08/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import WebKit

class WebViewController: UIViewController , WKUIDelegate{
    var urlString : String?
    var webView: WKWebView!
    
    override func loadView() {
        let webConfiguration = WKWebViewConfiguration()
        webView = WKWebView(frame: .zero, configuration: webConfiguration)
        webView.uiDelegate = self
        webView.scrollView.setZoomScale(3.0, animated: true)
        view = webView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if !(urlString?.isEmpty)!{
            let myURL = URL(string:(urlString?.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!)!)!
            let myRequest = URLRequest(url: myURL)
            webView.load(myRequest)
        }
       

    }

 
}


