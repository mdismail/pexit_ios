//
//  PostCommentController.swift
//  PEXit
//
//  Created by Apple on 21/08/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

protocol PostCommentDelegate{
    func sendComment(comment: Comment.PostCommentRequest)
}
class PostCommentController: UIViewController , UITableViewDelegate , UITableViewDataSource , UITextViewDelegate, PassAttatchmentsToController{
    @IBOutlet weak var txtViewComment: UITextView!
    @IBOutlet weak var viewAttachment: UIView!
    @IBOutlet weak var btnPostComment: UIButton!
   
    @IBOutlet weak var heightAttachmentView: NSLayoutConstraint!
    var fileArray = [MediaAttatchment]()
    @IBOutlet weak var tblFiles: UITableView!
    var delegate : PostCommentDelegate?
    weak var parentController : UIViewController?
    var isReply = false{
        didSet{
            if isReply == true{
                btnPostComment.setTitle("Reply",for:.normal)
            }else{
                btnPostComment.setTitle("Post",for:.normal)
            }
            
        }
    }
    private lazy var attachmentController: AttatchmentViewController = {
        var viewController = storyboard?.instantiateViewController(withIdentifier: "attachmentController") as! AttatchmentViewController
        viewController.isFromEdit = false
      //  viewController.serviceType = "Sponsor"
        viewController.delegate = self
        return viewController
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        txtViewComment.placeholder = "Write a comment"
        txtViewComment.delegate = self
        tblFiles.tableFooterView = UIView()
        self.addChildViewController(attachmentController)
        addSubView(asChildViewController: attachmentController)
        // Do any additional setup after loading the view.
    }

    @IBAction func btnUploadClicked(_ sender: Any) {
        // Attatch Files
        FileAttatchmentHandler.shared.showDocumentPicker(vc: self, files: ["public.data","public.content","public.image","public.movie","public.presentation"])
        FileAttatchmentHandler.shared.filePickedBlock = {[unowned self] (url) in
            print(url)
            if self.parentController is CommentViewController{
                (self.parentController as! CommentViewController).heightConstraintCommentView.constant = ((self.parentController as! CommentViewController).heightConstraintCommentView.constant) + 90
            }
            if self.parentController is ReplyViewController{
                 (self.parentController as! ReplyViewController).heightConstraintCommentView.constant = ((self.parentController as! ReplyViewController).heightConstraintCommentView.constant) + 90
            }
            
            print("File Name is ---->",url.lastPathComponent)
            let newIndexPath = IndexPath(item: 0, section: 0)
            do {
                let data = try Data(contentsOf: url, options: .mappedIfSafe)
                print(data)
                let media = MediaAttatchment(file: nil, fileName: url.lastPathComponent,fileData:data,fileExtension:url.mimeType())
                self.addNewFile(currentFile: media)
                
            } catch  {
            }
        }
    }
    
    @IBAction func btnPostCommentClicked(_ sender: Any) {
//        var postCommentRequest = Comment.PostCommentRequest()
//        postCommentRequest.comment = txtViewComment.text
//        postCommentRequest.attachments = fileArray
//        postCommentRequest.type = isReply ? REPLY_TO_COMMENT : ADD_NEW_COMMENT
//        delegate?.sendComment(comment: postCommentRequest)
    }
    
    func sendAttatchments(imageArray : [MediaAttatchment]?,videoArray:[MediaAttatchment]?,fileArray:[MediaAttatchment]?,pptArray:[MediaAttatchment]?,linksString:String?,youTubeLinkString:String?) {
        if txtViewComment.text == nil || (txtViewComment.text?.isEmpty)!{
            displayToast("Please enter comment")
            return
        }
        var postCommentRequest = Comment.PostCommentRequest()
        postCommentRequest.comment = txtViewComment.text
        postCommentRequest.attachments = fileArray
        postCommentRequest.images = imageArray
        postCommentRequest.videos = videoArray
        postCommentRequest.type = isReply ? REPLY_TO_COMMENT : ADD_NEW_COMMENT
        attachmentController.removeAllElements()
        delegate?.sendComment(comment: postCommentRequest)
    
    }
    
    
    func addSubView(asChildViewController viewController: UIViewController) {
        // Add Child View Controller
        addChildViewController(viewController)
        
        // Add Child View as Subview
        viewAttachment.addSubview(viewController.view)
        if viewController is AttatchmentViewController{
            (viewController as! AttatchmentViewController).btnPptAttatchment.isHidden = true
            (viewController as! AttatchmentViewController).btnLinkAttatchment.isHidden = true
            (viewController as! AttatchmentViewController).btnYouTubeLinkAttatchment.isHidden = true
            (viewController as! AttatchmentViewController).btnPost.setTitle("Send", for: .normal)
//            (viewController as! AttatchmentViewController).btnPost.contentHorizontalAlignment = .right
//            (viewController as! AttatchmentViewController).btnFileAttatchment.contentHorizontalAlignment = .left
//            (viewController as! AttatchmentViewController).btnImageAttatchment.contentHorizontalAlignment = .center
//            (viewController as! AttatchmentViewController).btnVideoAttatchment.contentHorizontalAlignment = .right
           
        }
        // Configure Child View
        viewController.view.frame = viewAttachment.bounds
        viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        // Notify Child View Controller
        viewController.didMove(toParentViewController: self)
    }
    
    func updateHeightOfParentController(height: CGFloat) {
        //Taking a constant
        heightAttachmentView.constant = DEFAULT_HEIGHT_ATTACHMENT_VIEW + height
        
        if self.parentController is CommentViewController{
            (self.parentController as! CommentViewController).heightConstraintCommentView.constant = heightAttachmentView.constant + 90
        }
        if self.parentController is ReplyViewController{
            (self.parentController as! ReplyViewController).heightConstraintCommentView.constant = heightAttachmentView.constant + 90
        }
        self.view.layoutIfNeeded()
    }
    
    func clearData(){
        txtViewComment.text = ""
        self.fileArray.removeAll()
        tblFiles.reloadData()
    }
    
    func addNewFile(currentFile : MediaAttatchment)
    {
        let newIndexPath = IndexPath(item: 0, section: 0)
        self.fileArray.insert(currentFile, at: 0)
        tblFiles.insertRows(at: [newIndexPath], with: .automatic)
    }
    
    // MARK:- TEXT VIEW DELEGATE
    
    func textViewDidChange(_ textView: UITextView) {
        textView.updateTextViewPlacholder()
    }
    
    // MARK:- TABLE VIEW DELEGATE
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fileArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let uploadCell = tableView.dequeueReusableCell(withIdentifier: "uploadListCell", for: indexPath) as! AttatchmentTableViewCell
        let currentAttactchement = fileArray[indexPath.row]
        uploadCell.btnDelete.tag = indexPath.row
        uploadCell.setData(currentAttatchment : currentAttactchement)
        return uploadCell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 85
    }
}




