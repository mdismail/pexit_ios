//
//  CommentWorker.swift
//  PEXit
//
//  Created by Apple on 07/08/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation

class CommentWorker{
    func fetchComments(postId : Int,completionHandler: @escaping(_ response : Comment.CommentResponse) -> Void) {
        
        let params = ["pid":postId] as [String:Any]
       
        START_LOADING_VIEW()
        ServiceManager.methodType(requestType: GET_REQUEST, url: GET_FETCH_COMMENTS_URL(postId), params: nil ,paramsData: nil,completion: { (response,_ responseData, statusCode) in
            STOP_LOADING_VIEW()
            let decoder = JSONDecoder()
            do{
                //                print("Response issss ---->",response)
                let commentsResponse = try decoder.decode(Comment.CommentResponse.self, from: responseData!)
                completionHandler(commentsResponse)
            }catch{
                DispatchQueue.main.async{
                    if ((response as! [String : Any])["message"] as? String) != nil{
                        if ((response as! [String : Any])["message"] as? String) != "No comment(s) found"{
                            KEY_WINDOW?.makeToast((response as! [String : Any])["message"] as? String)
                        }
                        
                    }
                }
               
                print("error")
            }
            
        }) { (response, statusCode) in
            STOP_LOADING_VIEW()
        }
    }
    
    
    func postComment(commentRequest : Comment.PostCommentRequest,attachments:[MediaAttatchment]? = nil  ,completionHandler: @escaping(_ response : Comment.PostCommentResponse) -> Void) {
        START_LOADING_VIEW()
        var params = ["oid":commentRequest.oid ?? 0,"pid":commentRequest.pid ?? 0,"type":commentRequest.type ?? "add","comment":commentRequest.comment ?? ""] as [String : Any]
        if commentRequest.type == "reply"{
            params["comntid"] = commentRequest.comntid!
        }
        AlamofireManager.shared().postComment(apiUrl: POST_COMMENT, attatchments: attachments,images: commentRequest.images,videos:commentRequest.videos, parameters: params) { (response, error) in
             STOP_LOADING_VIEW()
            let decoder = JSONDecoder()
            if response != nil{
                if !(((response!["errors"] as? String)?.isEmpty)!) || !((response!["virus"] as? String)?.isEmpty)!{
                    Utils.showErrorFilesAlert(errorFiles : response!["errors"] as? String,corruptFiles : response!["virus"] as? String)
                }
                do{
                    
                    let jsonData = try JSONSerialization.data(withJSONObject: response, options: .prettyPrinted)
                    let reqJSONStr = String(data: jsonData, encoding: .utf8)
                    let data = reqJSONStr?.data(using: .utf8)
                    
                    let responseComment = try decoder.decode(Comment.PostCommentResponse.self, from: data!)
                    completionHandler(responseComment)
                    
                }catch{
                    print("error")
                }
            }
            
            
        }

    }

}
