//
//  CommentInteractor.swift
//  PEXit
//
//  Created by Apple on 07/08/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation


protocol CommentInteractorInput{
    func fetchComments(request : Comment.CommentRequest)
    
}


protocol CommentInteractorOutput{
     func presentFetchedComments(response:Comment.CommentResponse)
    
}

class CommentInteractor : CommentInteractorInput{
    
    
    var commentInteractorOutput : CommentInteractorOutput!
    var worker = CommentWorker()
    
    func fetchComments(request : Comment.CommentRequest){
        
        worker.fetchComments(postId: request.pid!) { [weak self] (response) in
            self?.commentInteractorOutput.presentFetchedComments(response: response)
        }
    }
}
