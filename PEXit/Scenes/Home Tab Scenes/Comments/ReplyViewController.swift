//
//  ReplyViewController.swift
//  PEXit
//
//  Created by ats on 20/12/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import Presentr

class ReplyViewController: UIViewController, PostCommentDelegate {
    var currentCommentModel : Comment.CommentModel?
    var postModel : PostRLMModel?
    var indexOfCurrentCommment : Int?
    var destinationController : PostCommentController?
    @IBOutlet weak var heightConstraintCommentView: NSLayoutConstraint!
    @IBOutlet weak var tblReply: UITableView!
    @IBOutlet weak var lblComment: UILabel!
    @IBOutlet weak var lblUserNmae: UILabel!
    @IBOutlet weak var imgViewUser: UIImageView!
    var initialCommentViewHeight : CGFloat?
    var delegate : PassCommentDelegate?
    var arrayLangInfo = [AllLangDetails]()
    var currentIndex : Int?
    override func viewDidLoad() {
        super.viewDidLoad()
        initialCommentViewHeight = heightConstraintCommentView.constant
        setUpTableView(tableView: tblReply)
        if destinationController != nil{
            destinationController?.isReply = true
        }
        addBackButton(withTitle: "Replies")
        
        setCommentData()
        // Do any additional setup after loading the view.
    }
    
    func setCommentData(){
        lblUserNmae.text = currentCommentModel?.uname
        lblComment.text = currentCommentModel?.comment
        imgViewUser.makeImageRounded()
        if let userImage = currentCommentModel?.uimage{
            imgViewUser.sd_setImage(with: URL(string:userImage) , placeholderImage: nil)
        }
        
    }
    
    func setUpTableView(tableView : UITableView){
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 20
        tableView.tableFooterView = UIView()
        tableView.register(UINib(nibName: "CommentTableViewCell", bundle: nil), forCellReuseIdentifier: "commentCell")
        tblReply.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "replyToUploads"{
            destinationController = segue.destination as? PostCommentController
            destinationController?.parentController = self
            //
            destinationController?.delegate = self
        }
    }
    
    
    func sendComment(comment: Comment.PostCommentRequest) {
        if comment.comment == nil || (comment.comment?.isEmpty)!{
            displayToast("Please enter comment")
            return
        }
        self.dismissKeyboard()
        var commentDetails = comment
        commentDetails.oid = postModel?.uid
        commentDetails.pid = postModel?.id
        commentDetails.comntid = currentCommentModel?.cid
        
        let worker = CommentWorker()
        worker.postComment(commentRequest: commentDetails,attachments: commentDetails.attachments) { (response) in
            if response.status == true && response.datas != nil{
                let postCommentModel = response.datas!
                self.destinationController?.clearData()
                self.heightConstraintCommentView.constant = self.initialCommentViewHeight!
                self.currentCommentModel?.rlyComments?.append(postCommentModel)
                self.tblReply.reloadData()
            }
            self.displayToast(response.message!)
        }
        
    }
    
    
    override func backAction() {
        if let delegate = delegate{
            delegate.passIndexUpdated(indexPath: IndexPath.init(row: indexOfCurrentCommment ?? 0, section: 0),currentModel: self.currentCommentModel)
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    
}


extension ReplyViewController : UITableViewDataSource , UITableViewDelegate , ReplyComment , ItemSelected{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (currentCommentModel?.rlyComments!.count)!
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let commentCell = tableView.dequeueReusableCell(withIdentifier: "commentCell", for: indexPath) as! CommentTableViewCell
        commentCell.delegate = self
       // commentCell.btnReplyHeight.constant = 0
        commentCell.setUpView(commentModel:  (currentCommentModel?.rlyComments![indexPath.row])!)
        commentCell.btnReply.isHidden = true
        commentCell.btnReplyHeightConstant.constant = 0
        commentCell.btnTranslate.tag = indexPath.row
        commentCell.btnTranslate.addTarget(self, action: #selector(translateComment(sender:)), for: .touchUpInside)
        commentCell.collectionViewMedia.register(UINib(nibName: "MediaReplyCell", bundle: nil), forCellWithReuseIdentifier: "mediaCell")
        return commentCell
        
    }
    
    // Language Translation
    
    @objc func translateComment(sender : UIButton){
        currentIndex = sender.tag
        showListOfLanguages()
    }
    
    
    func showListOfLanguages(){
        getLanguages { (languagesArray,completeLanguageDetails) in
            self.arrayLangInfo = completeLanguageDetails
            self.presentLanguagesView(languageArray: languagesArray)
        }
    }
    
    func presentLanguagesView(languageArray : [String]){
        DispatchQueue.main.async {
            let presenter = Presentr(presentationType: .popup)
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "commonViewController") as! CommonViewController
            // controller.isFromHome = true
            controller.isLanguageUpdation = true
            controller.itemsArray = languageArray
            controller.delegate = self
            self.customPresentViewController(presenter, viewController: controller, animated: true)
        }
    }
    
    func optionSelected(option: String?) {
        CURRENT_APP_LANGUAGE = arrayLangInfo.filter{$0.name == option}[0].code
        SELECTED_LANGUAGE = arrayLangInfo.filter{$0.name == option}[0].name
        translateToCurrentSelectedLanguage()
        
    }
    
    func translateToCurrentSelectedLanguage(){
        let translationWorker = LanguageTranslationWoker()
        //        var lblArray = [lblDescription]
//        var currentCommentModel = currentCommentModel?.rlyComments[currentIndex ?? 0]
        let currentComment = currentCommentModel?.rlyComments![currentIndex ?? 0].comment
        let currentIndexPath = IndexPath.init(row: currentIndex ?? 0, section: 0)
        translationWorker.translateCurrentString(newLanguageCode: CURRENT_APP_LANGUAGE, stringsToTranslate: [currentComment ?? ""]) { (response) in
            if response != nil{
                if let translations = response as? [ReturnedJson]{
                    DispatchQueue.main.async {
                        var i = 0
                        for value in translations{
                            
                            for translatedStr in value.translations{
                                // lblArray[i]?.text = translatedStr.text
                                self.currentCommentModel?.rlyComments![self.currentIndex ?? 0].translatedComment = translatedStr.text
                                //                                currentCommentModel.translatedComment = translatedStr.text
                                self.tblReply.reloadRows(at: [currentIndexPath], with: .automatic)
                                i = i + 1
                                
                                
                            }
                            
                        }
                    }
                }
            }
        }
    }
    
  
}
