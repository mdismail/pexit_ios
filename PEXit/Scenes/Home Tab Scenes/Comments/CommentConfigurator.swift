//
//  CommentConfigurator.swift
//  PEXit
//
//  Created by Apple on 07/08/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation
import UIKit
extension CommentViewController: CommentPresenterOutput{
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        router.passDataToNextScene(segue: segue, sender: sender)
    }
}

extension CommentInteractor: CommentViewControllerOutput{
}

extension CommentPresenter: CommentInteractorOutput{
    
    
}



class CommentConfigurator{
    
    // MARK: Object lifecycle
    
    class var sharedInstance: CommentConfigurator{
        struct Static {
            static let instance =  CommentConfigurator()
        }
       
        return Static.instance
    }
    
    // MARK: Configuration
    
    func configure(viewController: CommentViewController){
        print("configure(viewController: InviteContactViewController) called by using class InviteContactConfigurator for setting the delegates")
        let router = CommentRouter()
        router.controller = viewController
        
        let presenter = CommentPresenter()
        presenter.output = viewController
        
        let interactor = CommentInteractor()
        interactor.commentInteractorOutput = presenter
        
        viewController.output = interactor
        viewController.router = router
    }
    
}
