//
//  CommentPresenter.swift
//  PEXit
//
//  Created by Apple on 07/08/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation
protocol CommentPresenterInput{
    func presentFetchedComments(response:Comment.CommentResponse)
}

protocol CommentPresenterOutput: class{
    func displayComments(response: Comment.CommentResponse)
}

class CommentPresenter : CommentPresenterInput{
    weak var output: CommentPresenterOutput!
    
    func presentFetchedComments(response:Comment.CommentResponse){
        output.displayComments(response: response)
    }
   
}
