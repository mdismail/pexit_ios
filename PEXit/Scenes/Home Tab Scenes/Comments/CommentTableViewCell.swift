//
//  CommentTableViewCell.swift
//  PEXit
//
//  Created by Apple on 07/08/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import SDWebImage


protocol ReplyComment{
    func sendReplyClicked(sender:UIButton)
    func showAttatchmentView(mediaUrl:String)
}

extension ReplyComment{
    func sendReplyClicked(sender:UIButton){
        
    }
    func showAttatchmentView(mediaUrl:String){
        
    }
}

class CommentTableViewCell: UITableViewCell , UICollectionViewDataSource , UICollectionViewDelegate{

    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var btnReply: UIButton!
    @IBOutlet weak var lblComment: UILabel!
    @IBOutlet weak var collectionViewMedia: UICollectionView!
    @IBOutlet weak var heightConstraintMediaView: NSLayoutConstraint!
    @IBOutlet weak var btnTranslate: UIButton!
    @IBOutlet weak var postCommentView: UIView!
    @IBOutlet weak var tblReply: UITableView!
    @IBOutlet weak var btnReplyHeightConstant: NSLayoutConstraint!
    
    @IBOutlet weak var btnReplyHeight: NSLayoutConstraint!
    //    @IBOutlet weak var heightConstraintTblView: NSLayoutConstraint!
    var currentModel : Comment.CommentModel?
    var delegate: ReplyComment?
    var currentIndex : IndexPath?
    var commentDelegate : HomeDataDelegate?
//     lazy var postCommentController: PostCommentController = {
//        // Instantiate View Controller
//        var viewController =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "postCommentController") as! PostCommentController
//        viewController.isReply = true
//        return viewController
//    }()
    override func awakeFromNib() {
        super.awakeFromNib()
        heightConstraintMediaView.constant = 0
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func setUpView(commentModel : Comment.CommentModel){
        lblUserName.text = commentModel.uname
        if commentModel.translatedComment != nil{
            lblComment.text = commentModel.translatedComment

        }else{
            lblComment.text = commentModel.comment

        }
        
        imgUser.makeImageRounded()
        if let userImage = commentModel.uimage{
            imgUser.sd_setImage(with: URL(string:userImage) , placeholderImage: nil)
        }
        currentModel  = commentModel
        lblComment.isUserInteractionEnabled = true
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(CommentTableViewCell.descriptionTextTapped(_:)))
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        lblComment.addGestureRecognizer(tapGesture)
        //collectionViewMedia.delegate = self
        collectionViewMedia.dataSource = self
        collectionViewMedia.delegate = self
        //heightConstraintTblView.constant = 0
        if currentModel!.attach?.count == 0{
             heightConstraintMediaView.constant = 0
        }else{
            heightConstraintMediaView.constant = 55
        }
        
        collectionViewMedia.reloadData()
    }
  
    
    @objc func descriptionTextTapped(_ sender : UITapGestureRecognizer){
        if let delegate = commentDelegate , let index = currentIndex{
            delegate.cellTaped(indexPath: index)
        }
    }
    
    
    @IBAction func replyToCommentClicked(_ sender: UIButton) {
        delegate?.sendReplyClicked(sender:sender)
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        switch section{
//        case 0:
            return currentModel!.attach?.count ?? 0
//        case 1:
//            return currentModel!.attach?.count ?? 0
//        case 2:
//            return currentModel!.attach?.count ?? 0
//        default:
//            return 0
//        }
}
       
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "mediaCell", for: indexPath) as! MediaReplyCell
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let mediaUrl = currentModel?.attach![indexPath.row]
        delegate?.showAttatchmentView(mediaUrl:mediaUrl!)
    }
    
   
}


