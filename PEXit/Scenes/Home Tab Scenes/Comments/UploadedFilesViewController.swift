//
//  UploadedFilesViewController.swift
//  PEXit
//
//  Created by Apple on 20/08/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class UploadedFilesViewController: UIViewController , UITableViewDataSource , UITableViewDelegate {
    var currentFile : MediaAttatchment?
    var fileArray = [MediaAttatchment]()
    @IBOutlet weak var tblFiles: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
//                            self.fileArray.insert((MediaAttatchment(file: nil, fileName: url.lastPathComponent,fileData:data,fileExtension:url.mimeType())), at: 0)
//                 self.currentArray = self.fileArray
//                    self.tblFiles.insertRows(at: [newIndexPath], with: .automatic)

    }
    
    func addNewFile(currentFile : MediaAttatchment)
    {
         let newIndexPath = IndexPath(item: 0, section: 0)
         self.fileArray.insert(currentFile, at: 0)
         tblFiles.insertRows(at: [newIndexPath], with: .automatic)
    }
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fileArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let uploadCell = tableView.dequeueReusableCell(withIdentifier: "uploadListCell", for: indexPath) as! AttatchmentTableViewCell
        let currentAttactchement = fileArray[indexPath.row]
        uploadCell.btnDelete.tag = indexPath.row
        uploadCell.setData(currentAttatchment : currentAttactchement)
        return uploadCell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 85
    }
}
