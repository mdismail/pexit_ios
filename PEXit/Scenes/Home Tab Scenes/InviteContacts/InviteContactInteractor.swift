//
//  InviteContactInteractor.swift
//  PEXit
//
//  Created by Apple on 01/08/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation
protocol InviteContactInteractorInput{
//    func fetchItems(request:RegistrationList.FetchItems.Request)
   
}


protocol InviteContactInteractorOutput{
//    func presentFetchedItems(response:RegistrationList.FetchItems.Response)
    
}

class InviteContactInteractor : InviteContactInteractorInput{
    var output: InviteContactInteractorOutput!
    var worker = ItemWorker(itemStore: ItemMemoryStore())
    var itemsArray: [TextfieldInfoStruct] = []
    
//    func fetchItems(request:RegistrationList.FetchItems.Request){
//        print("fetchItems(request:RegistrationList.FetchItems.Request) called by using RegistrationInteractorInput")
//        worker.fetchAllItems(completionHandler: {(items) in
//            let response = RegistrationList.FetchItems.Response(items: items)
//            itemsArray = items
//            output.presentFetchedItems(response: response)
//        })
//    }
}
