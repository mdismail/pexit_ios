//
//  InviteContactTableViewCell.swift
//  PEXit
//
//  Created by Apple on 07/08/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import Material

class InviteContactTableViewCell: UITableViewCell {
    @IBOutlet weak var txtFieldInvite: TJTextField!
    @IBOutlet weak var txtViewDesc: UITextView!
    @IBOutlet weak var viewBottom: UIView!
    @IBOutlet weak var btnSned: UIButton!
    
    @IBOutlet weak var lblDescHeader: UILabel!
    @IBOutlet weak var lblHeader: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        txtViewDesc.leftAlignTextView()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func configureTextfield(_ textFieldDetails:TextfieldInfoStruct) {
       // txtFieldInvite.placeholder = textFieldDetails.placeholderText
        lblHeader.text = textFieldDetails.placeholderText
        txtFieldInvite.accessibilityLabel = textFieldDetails.placeholderText
        
        if textFieldDetails.placeholderText == "Your E-Mail" || textFieldDetails.placeholderText == "Sender Email"{
            txtFieldInvite.isUserInteractionEnabled = false
            txtFieldInvite.text = ProfileRLMModel.fetchProfile()?.userEmail
        }
        
        if textFieldDetails.placeholderText == "Sender Name" {
            txtFieldInvite.isUserInteractionEnabled = false
            txtFieldInvite.text = ProfileRLMModel.fetchProfile()?.userName
        }
        
        if textFieldDetails.placeholderText == "Supplier Name"{
            txtFieldInvite.isUserInteractionEnabled = false
        }
        
        if let rightImage = textFieldDetails.rightImage{
            let rightView = UIImageView()
            rightView.image = rightImage
            txtFieldInvite.rightView = rightView
        }
        if textFieldDetails.placeholderText == "Sender Phone"{
            txtFieldInvite.keyboardType = UIKeyboardType.numberPad
        }
       // txtFieldInvite.placeholderAnimation = .default
      
    }
    
}

