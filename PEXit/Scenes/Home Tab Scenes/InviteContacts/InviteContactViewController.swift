//
//  InviteContactViewController.swift
//  PEXit
//
//  Created by Apple on 01/08/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import Material
let HEIGHT_OF_CELL = 60


protocol InviteContactViewControllerInput{
    
    
}

protocol InviteContactViewControllerOutput{
    
}


class InviteContactViewController: UIViewController , InviteContactViewControllerInput , UITableViewDataSource , UITableViewDelegate , UITextFieldDelegate , UITextViewDelegate{
    
    var output: InviteContactViewControllerOutput!
    var router: InviteContactRouter!
    var arrayOfTextFields : [TextfieldInfoStruct]?
    var currentUserEmail : String?
    var message = ""
    
    @IBOutlet weak var inviteContactsTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addBackButton(withTitle: "Invite Friends")
        initialSetup()
        // Do any additional setup after loading the view.
    }
    
    override func awakeFromNib() {
        InviteContactConfigurator.sharedInstance.configure(viewController: self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //        fetchItems()
    }
    
    func initialSetup(){
        arrayOfTextFields = [TextfieldInfoStruct(placeholderText: "Your Name"),TextfieldInfoStruct(placeholderText: "Your E-Mail"),TextfieldInfoStruct( placeholderText: "Subject"),TextfieldInfoStruct(placeholderText: "Friend's E-Mail")]
        //tblHeightConstraint.constant = CGFloat((arrayOfTextFields?.count)! * HEIGHT_OF_CELL)
        inviteContactsTableView.dataSource = self
        inviteContactsTableView.delegate = self
        inviteContactsTableView.register(UINib(nibName: "InviteContactViewCell", bundle: nil), forCellReuseIdentifier: "inviteContactCell")
    }
    
    //MARK: Get all required fields
    func getAllRequiredFields() -> [TJTextField] {
        var textFieldsArray = [TJTextField]()
        let cells = Utils.cellsForTableView(inviteContactsTableView) as! [InviteContactTableViewCell]
        
        for cell in cells {
            if !cell.txtFieldInvite.isHidden{
                textFieldsArray.append(cell.txtFieldInvite)
            }
            
        }
        return textFieldsArray
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (arrayOfTextFields?.count)! + 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = inviteContactsTableView.dequeueReusableCell(withIdentifier: "inviteContactCell") as! InviteContactTableViewCell
        if indexPath.row < arrayOfTextFields?.count{
            cell.configureTextfield(arrayOfTextFields![indexPath.row])
            cell.txtFieldInvite.delegate = self
            cell.lblHeader.isHidden = false
            cell.txtFieldInvite.tag = indexPath.row
            cell.txtViewDesc.isHidden = true
            cell.viewBottom.isHidden = true
            cell.btnSned.isHidden = true
        }
        else if indexPath.row == arrayOfTextFields?.count{
            cell.txtViewDesc.delegate = self
            cell.txtViewDesc.isHidden = false
            cell.lblHeader.isHidden = true
            cell.viewBottom.isHidden = false
            cell.txtFieldInvite.isHidden = true
            cell.btnSned.isHidden = true
            cell.lblDescHeader.text = "Message"
            //cell.txtViewDesc.placeholder = "Message"
            
        }
        else{
            cell.lblHeader.isHidden = true
            cell.btnSned.isHidden = false
            cell.btnSned.addTarget(self, action: #selector(sendInvitation(sender:)), for: .touchUpInside)
            cell.txtViewDesc.isHidden = true
            cell.viewBottom.isHidden = true
            cell.txtFieldInvite.isHidden = true
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch indexPath.row{
        case indexPath.row where indexPath.row < arrayOfTextFields?.count:
            return CGFloat(HEIGHT_OF_CELL)
        case indexPath.row where indexPath.row == arrayOfTextFields?.count:
            return UITableViewAutomaticDimension
        default:
            return 60
        }
    }
    
    
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let inviteContactCell = tableView.dequeueReusableCell(withIdentifier: "inviteContactCell", for: indexPath) as! InviteContactTableViewCell
//        inviteContactCell.configureTextfield(arrayOfTextFields![indexPath.row])
//        inviteContactCell.txtFieldInvite.delegate = self
//        inviteContactCell.txtFieldInvite.tag = indexPath.row
//        return inviteContactCell
//    }
    
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        let selectedIndexPath: IndexPath = IndexPath(row: textField.tag, section: 0)
        arrayOfTextFields![selectedIndexPath.row].textfieldText = textField.text
    }
    
    func textViewDidChange(_ textView: UITextView) {
        message = textView.text
        textView.updateTextViewPlacholder()
        UIView.setAnimationsEnabled(false)
        inviteContactsTableView.beginUpdates()
      //  inviteContactsTableView.layoutIfNeeded()
        inviteContactsTableView.endUpdates()
        UIView.setAnimationsEnabled(true)
    }
   
    
    @objc func sendInvitation(sender:UIButton){
        if(Utils.validateFormAndShowToastWithRequiredFields(getAllRequiredFields())){
            if !(message.isEmpty){
                let keys = ["uname","uemail","subject","friendsEmail","mailContent"]
                var params = [String:String]()
                for (index, element) in self.arrayOfTextFields!.enumerated() {
                    print("Item \(index): \(element)")
                    params[keys[index]] = element.textfieldText
                    
                }
                params["uemail"] = ProfileRLMModel.fetchProfile()?.userEmail
                params["mailContent"] = message
                inviteFriends(params)
            }else{
                displayToast("Please enter message")
                return
            }
           
        }else{
            return
        }
    }
    
    @IBAction func sendInvitationClicked(_ sender: Any) {
        
        if(Utils.validateFormAndShowToastWithRequiredFields(getAllRequiredFields())){
                
            let keys = ["uname","uemail","subject","friendsEmail","mailContent"]
            var params = [String:String]()
            for (index, element) in self.arrayOfTextFields!.enumerated() {
                print("Item \(index): \(element)")
                params[keys[index]] = element.textfieldText
                
            }
            params["uemail"] = ProfileRLMModel.fetchProfile()?.userEmail
            inviteFriends(params)
        }else{
            return
        }
    }
}
