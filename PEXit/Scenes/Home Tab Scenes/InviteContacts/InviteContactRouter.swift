//
//  InviteContactRouter.swift
//  PEXit
//
//  Created by Apple on 01/08/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation
protocol InviteContactRouterInput{
    func navigateToSomewhere()
}

class InviteContactRouter : InviteContactRouterInput{
    weak var controller : InviteContactViewController!
    
    func navigateToSomewhere() {
        
    }
    
}
