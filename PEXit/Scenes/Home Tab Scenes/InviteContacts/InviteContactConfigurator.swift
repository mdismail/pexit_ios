//
//  InviteContactConfigurator.swift
//  PEXit
//
//  Created by Apple on 01/08/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation
extension InviteContactViewController: InviteContactPresenterOutput{
//    func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?){
//
//    }
}

extension InviteContactInteractor: InviteContactViewControllerOutput{
}

extension InviteContactPresenter: InviteContactInteractorOutput{
    
    
}



class InviteContactConfigurator{
  
        // MARK: Object lifecycle
        
        class var sharedInstance: InviteContactConfigurator{
            struct Static {
                static let instance =  InviteContactConfigurator()
            }
            print("RegistrationConfigurator instance created")
            return Static.instance
        }
        
        // MARK: Configuration
        
        func configure(viewController: InviteContactViewController){
            print("configure(viewController: InviteContactViewController) called by using class InviteContactConfigurator for setting the delegates")
            let router = InviteContactRouter()
            router.controller = viewController
            
            let presenter = InviteContactPresenter()
            presenter.output = viewController
            
            let interactor = InviteContactInteractor()
            interactor.output = presenter
            
            viewController.output = interactor
            viewController.router = router
        }
    
}
