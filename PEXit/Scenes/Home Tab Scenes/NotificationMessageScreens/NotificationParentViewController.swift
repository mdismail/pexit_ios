//
//  NotificationParentViewController.swift
//  PEXit
//
//  Created by Apple on 01/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
enum TabIndex : Int {
    case notificationTab = 0
    case messageTab = 1
}

class NotificationParentViewController: UIViewController {
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var btnComposeNewMessage: UIButton!
    
    var currentViewController: UIViewController?
    var isFromNotification = false
    var isFromMessageNotification = false

    lazy var firstChildTabVC: UIViewController? = {
        let firstChildTabVC = self.storyboard?.instantiateViewController(withIdentifier: "notificationViewController")
       return firstChildTabVC
    }()
    lazy var secondChildTabVC : UIViewController? = {
        let secondChildTabVC = self.storyboard?.instantiateViewController(withIdentifier: "messageViewController")
      return secondChildTabVC
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        segmentedControl.layer.cornerRadius = 15.0
        segmentedControl.layer.borderColor = UIColor.white.cgColor
        segmentedControl.layer.borderWidth = 1.0
        segmentedControl.layer.masksToBounds = true
        segmentedControl.selectedSegmentIndex = TabIndex.notificationTab.rawValue
        displayCurrentTab(TabIndex.notificationTab.rawValue)
        hideNavigationBar()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool){
        UIApplication.shared.applicationIconBadgeNumber = 0
        hideNavigationBar()
        if isFromNotification{
            //(firstChildTabVC as! NotificationViewController).fetchNotificationList()
            segmentedControl.selectedSegmentIndex = TabIndex.notificationTab.rawValue
            displayCurrentTab(TabIndex.notificationTab.rawValue)
        }
        if isFromMessageNotification{
         //  (secondChildTabVC as! MessageViewController).fetchMessageList()
            segmentedControl.selectedSegmentIndex = TabIndex.messageTab.rawValue
            displayCurrentTab(TabIndex.messageTab.rawValue)
        }
    }
    
    func updateViewForNotification(){
        if isFromNotification{
            if currentViewController == firstChildTabVC{
                (currentViewController as! NotificationViewController).fetchNotificationList()
            }else{
                (firstChildTabVC as! NotificationViewController).fetchNotificationList()
                segmentedControl.selectedSegmentIndex = TabIndex.notificationTab.rawValue
                displayCurrentTab(TabIndex.notificationTab.rawValue)
               
            }
        }
        if isFromMessageNotification{
            if currentViewController == secondChildTabVC{
                (currentViewController as! MessageViewController).fetchMessageList()
            }else{
                (secondChildTabVC as! MessageViewController).fetchMessageList()
                segmentedControl.selectedSegmentIndex = TabIndex.messageTab.rawValue
                displayCurrentTab(TabIndex.messageTab.rawValue)
            }
        }
    }
    
    @IBAction func backClicked(_ sender: Any) {
        self.backAction()
    }
    
   
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if let currentViewController = currentViewController {
            currentViewController.viewWillDisappear(animated)
        }
        showNavigationBar()
    }
    
    // MARK: - Switching Tabs Functions
    @IBAction func switchTabs(_ sender: UISegmentedControl) {
        self.currentViewController!.view.removeFromSuperview()
        self.currentViewController!.removeFromParentViewController()
        displayCurrentTab(sender.selectedSegmentIndex)
    }
    
    func displayCurrentTab(_ tabIndex: Int){
        if let vc = viewControllerForSelectedSegmentIndex(tabIndex) {
            self.addChildViewController(vc)
            vc.didMove(toParentViewController: self)
            vc.view.frame = self.contentView.bounds
            self.contentView.addSubview(vc.view)
            self.currentViewController = vc
        }
    }
    
    func viewControllerForSelectedSegmentIndex(_ index: Int) -> UIViewController? {
        var vc: UIViewController?
        switch index {
        case TabIndex.notificationTab.rawValue :
            btnComposeNewMessage.isHidden = true
            vc = firstChildTabVC
        case TabIndex.messageTab.rawValue :
            btnComposeNewMessage.isHidden = false
            vc = secondChildTabVC
        default:
            return nil
        }
        
        return vc
    }
    
    
    
}
