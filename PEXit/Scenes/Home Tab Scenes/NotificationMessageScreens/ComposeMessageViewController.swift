//
//  ComposeMessageViewController.swift
//  PEXit
//
//  Created by Apple on 03/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

protocol ComposeMessageViewControllerInput{
  func presentMessageUsers(response:ListOfIndividuals.Response)
  func displayResponse(response:NewMessageResponseModel)
  func displayNewMemberResponse(response:CommonModel.BaseModel)
}

protocol ComposeMessageViewControllerOutput{
  func fetchMessageUsers(searchString : String)
  func sendNewMessage(messageRequestModel : NewMessageModel)
  func addNewGroupMembers(groupId:Int,memberIds:String)
}

let MEMBER_CELL_HEIGHT = 40

class ComposeMessageViewController: UIViewController , ComposeMessageViewControllerInput , UITextFieldDelegate , UITextViewDelegate{
    var output : ComposeMessageViewControllerOutput!
    var selectedUserArray = [ListOfIndividuals.IndividualUserModel]()
    var router : MessageRouter!
    @IBOutlet weak var heightCollectionViewUsers: NSLayoutConstraint!
    @IBOutlet weak var collectionViewUsers: UICollectionView!
    @IBOutlet weak var txtViewMessage: UITextView!
    @IBOutlet weak var txtFieldUsers: UITextField!
    @IBOutlet weak var viewAddPeople: UIView!
    @IBOutlet weak var tblCurrentMembers: UITableView!
    @IBOutlet weak var heightTableViewUsers: NSLayoutConstraint!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblGroupMembers: UILabel!
    var currentGroup : MessageListModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if currentGroup == nil{
           setUIForNewMessage()
        }else{
            setUIForAddPeople()
        }
        collectionViewUsers.register(UINib(nibName: "JobSearchCell", bundle: nil), forCellWithReuseIdentifier: "JobSearchCell")
    }

    func setUIForNewMessage(){
        addBackButton(withTitle: "New Message")
        txtViewMessage.leftAlignTextView()
//        txtViewMessage.placeholder = "Message"
        viewAddPeople.isHidden = true
    }
    
    func setUIForAddPeople(){
        addBackButton(withTitle: "Add People")
        viewAddPeople.isHidden = false
    
        lblTitle.text = "Add people to group"
        tblCurrentMembers.register(UINib(nibName: "UserTableViewCell", bundle: nil), forCellReuseIdentifier: "UserTableViewCell")
        tblCurrentMembers.tableFooterView = UIView()
        lblGroupMembers.text = "\((currentGroup?.pimage?.count)!) people in this conversation"
            heightTableViewUsers.constant = CGFloat((currentGroup?.pimage?.count)! * (MEMBER_CELL_HEIGHT + 10))
        tblCurrentMembers.reloadData()
    }
    
    override func awakeFromNib() {
        MessageConfigurator.sharedInstance.configure(viewController: self)
    }
    override func viewWillAppear(_ animated : Bool){
        showNavigationBar()
    }
    
    override func viewWillDisappear(_ animated : Bool){
        hideNavigationBar()
    }
   
    func presentMessageUsers(response:ListOfIndividuals.Response){
        if response.status == true{
            DispatchQueue.main.async{
                if var responseUsers = response.datas{
                    responseUsers = responseUsers.filter{$0.id != Defaults.getUserID()}
                    self.router.navigateToPopUpContoller(userArray:responseUsers, sender: self.txtFieldUsers, popUpDirection: .up)
                }
                
            }
           
        }else{
            displayToast(response.message ?? UNKNOWN_ERROR_MSG)
        }
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(showListOfUsers), object: nil)
        self.perform(#selector(showListOfUsers), with: nil, afterDelay: 0.3)
        
        return true
    }
    
    @objc func showListOfUsers(){
        output.fetchMessageUsers(searchString: txtFieldUsers.text!)
    }
    
    func textViewDidChange(_ textView: UITextView) {
        textView.updateTextViewPlacholder()
    }
    
    @IBAction func btnSaveClicked(_ sender: Any) {
        if selectedUserArray.count > 0{
            if txtViewMessage.text.count > 0{
                let messageModel = getMessageRequestModel()
                output.sendNewMessage(messageRequestModel: messageModel)
            }else{
                displayToast(ENTER_MESSAGE)
            }
        }else{
            displayToast(SELECT_USER)
        }
    }
    
    @IBAction func btnCancelClicked(_ sender: Any) {
        backAction()
    }
    
    @IBAction func btnAddMembersClicked(_ sender: Any) {
        if selectedUserArray.count > 0{
            let newMemberIds = selectedUserArray.compactMap{$0.id}.map{"\($0)"}.joined(separator: ",")
            var groupId : Int?
            if let range = currentGroup?.toid?.range(of: "g:") {
                groupId = Int((currentGroup?.toid![range.upperBound...].trimmingCharacters(in: .whitespaces))!)
            }
            output.addNewGroupMembers(groupId: groupId!, memberIds: newMemberIds)
        }
            else{
                displayToast(SELECT_USER)
        }
       
    }
    
    func displayNewMemberResponse(response:CommonModel.BaseModel){
        displayToast(response.message ?? UNKNOWN_ERROR_MSG)
        if response.status == true{
            self.backAction()
        }
    }
    
    func displayResponse(response:NewMessageResponseModel){
        displayToast(response.message ?? UNKNOWN_ERROR_MSG)
        if response.status == true{
            self.backAction()
        }
    }
    
    func getMessageRequestModel() -> NewMessageModel{
        let newMessageModel = NewMessageModel()
        newMessageModel.chatterMessage = txtViewMessage.text
        newMessageModel.toIds = "ng"
        newMessageModel.chatterIds = selectedUserArray.compactMap{$0.id}.map{"\($0)"}.joined(separator: ",")
        return newMessageModel
    }
    
}

extension ComposeMessageViewController : ItemSelected , UICollectionViewDataSource , UICollectionViewDelegate , UICollectionViewDelegateFlowLayout , UITableViewDataSource , UITableViewDelegate{
 
    func individualUserSelected(userModel : [ListOfIndividuals.IndividualUserModel]?){
        txtFieldUsers.text = ""
        selectedUserArray = userModel!
        router.popUpController = nil
        print(selectedUserArray)
        heightCollectionViewUsers.constant = 40
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            self.collectionViewUsers.reloadData()
        }
        
    }
    //MARK:- CollectionView DataSource And Delegate
  
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       return selectedUserArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            let searchCell = collectionView.dequeueReusableCell(withReuseIdentifier: "JobSearchCell", for: indexPath) as! JobSearchCell
            searchCell.lblSearchText.text = ""
            searchCell.lblSearchText.text = selectedUserArray[indexPath.row].username
            searchCell.lblSearchText.sizeToFit()
            searchCell.btnSearch.tag = indexPath.row
            searchCell.btnSearch.addTarget(self, action: #selector(deleteCell(sender:)), for: UIControlEvents.touchUpInside)
            return searchCell
   }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout:
        UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            let size = (selectedUserArray[indexPath.row]).username?.size(withAttributes: nil)
            return CGSize(width:size!.width + 50,height:collectionView.frame.size.height)
    }
    
    @objc func deleteCell(sender:UIButton){
        selectedUserArray.remove(at: sender.tag)
        collectionViewUsers.reloadData()
        resizeCollectionViewSize()
    }
    
    
    
    func resizeCollectionViewSize(){
        if selectedUserArray.count > 0{
             heightCollectionViewUsers.constant = 40
        }else{
             heightCollectionViewUsers.constant = 0
        }
    }
    
    //MARK:- TableView DataSource And Delegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currentGroup != nil ? (currentGroup?.pimage?.count)! : 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(MEMBER_CELL_HEIGHT) + 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserTableViewCell", for: indexPath) as! UserTableViewCell
        cell.lblUserName.text = currentGroup?.pimage![indexPath.row].uname
        
        if let imageUrl = URL(string: (currentGroup?.pimage![indexPath.row].pimage)!){
            cell.imgViewUser.makeImageRounded()
            cell.imgViewUser.setImage(from:imageUrl)
        }
        return cell
    }
}
