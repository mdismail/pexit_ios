//
//  NotificationViewController.swift
//  PEXit
//
//  Created by Apple on 01/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class NotificationViewController: UIViewController , ItemSelected{
    @IBOutlet weak var tblNotifications: UITableView!
    var notificationArray = [NotificationListModel]()
   
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(NotificationViewController.handleRefresh(_:)),
                                 for: UIControlEvents.valueChanged)
        refreshControl.tintColor = UIColor.red
        
        return refreshControl
    }()
  
    override func viewDidLoad() {
        super.viewDidLoad()
        tblNotifications.register(UINib(nibName: "MessageListTableViewCell", bundle: nil), forCellReuseIdentifier: "messageListCell")
        tblNotifications.tableFooterView = UIView()
        tblNotifications.rowHeight = UITableViewAutomaticDimension
        tblNotifications.estimatedRowHeight = 107
        tblNotifications.addSubview(refreshControl)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        fetchNotificationList()
    }
    
    func fetchNotificationList(){
        let worker = MessageWorker()
        worker.fetchNotifications{ [weak self](response) in
            DispatchQueue.main.async{
                if response.status == true{
                    self?.notificationArray = response.datas!
                    self?.tblNotifications.reloadData()
                }else{
                    self?.displayToast(response.message!)
                }
            }
        }
        
    }
    
    @objc func handleRefresh(_ refreshControl : UIRefreshControl){
        fetchNotificationList()
        refreshControl.endRefreshing()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            self.tblNotifications.contentOffset = CGPoint.zero
        }
    }
}

extension NotificationViewController : UITableViewDelegate , UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notificationArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let messageCell = tableView.dequeueReusableCell(withIdentifier: "messageListCell", for: indexPath) as! MessageListTableViewCell
        messageCell.delegate = self
        messageCell.btnAcceptRequest.tag = indexPath.row
        messageCell.btnRejectRequest.tag = indexPath.row
        messageCell.setNotificationData(notificationModel: notificationArray[indexPath.row])
        return messageCell
    }
    
    func optionSelected(option: String?) {
        let componentsArray = option?.components(separatedBy: ":")
        switch componentsArray![0]{
        case "GROUP":
            navigateToGroup(groupID: componentsArray?[1])
        case "USERNAME":
            navigateToUser(userID: componentsArray?[1])
        case "REJECT_REQUEST":
            let currentModel = notificationArray[Int((componentsArray?[1])!)!]
            if currentModel.gid != nil{
                rejectRequest(index: componentsArray?[1],apiName: ACCEPT_REJECT_GROUP_REQUEST,option:"rejected")
            }else{
                rejectRequest(index: componentsArray?[1],apiName: ACCEPT_REJECT_FRIEND_REQUEST,option:"rejected")
            }
            
        case "ACCEPT_REQUEST":
            let currentModel = notificationArray[Int((componentsArray?[1])!)!]
            if currentModel.gid != nil{
                rejectRequest(index: componentsArray?[1],apiName: ACCEPT_REJECT_GROUP_REQUEST,option:"accepted")
            }else{
                rejectRequest(index: componentsArray?[1],apiName: ACCEPT_REJECT_FRIEND_REQUEST,option:"accepted")
            }
        default:
            print("INVALID URL")
        }
    }
    
    func rejectRequest(index:String?,apiName:String,option:String?){
        let notificationWorker = MessageWorker()
        var params = [String : Any]()
        switch apiName {
        case ACCEPT_REJECT_FRIEND_REQUEST:
            params = ["option":option,"fid":notificationArray[Int(index!)!].uid] as [String : Any]
        case ACCEPT_REJECT_GROUP_REQUEST:
            params = ["option":option,"gdid":notificationArray[Int(index!)!].gid,"reqid":Int(notificationArray[Int(index!)!].reqid!)] as [String : Any]
            break
        default:
            return
        }
        notificationWorker.acceptRejectRequest(apiName: apiName, params: params) { [weak self](response) in
            if response != nil{
                if response?.status == true{
                    DispatchQueue.main.async{
                        self?.displayToast((response?.message)!)
                      //  if option == "rejected"{
                             self?.updateTblUI(currentIndex: Int(index!)!)
                       /* }
                        else{ // will be done if asked to retain the notification after accepting
                            let currentCell = self?.tblNotifications.cellForRow(at: IndexPath(row:  Int(index!)!, section: 0)) as! MessageListTableViewCell
                            currentCell.btnAcceptRequest.isHidden = true
                            currentCell.btnRejectRequest.isHidden = true
                        } */
                       
                    }
                }else{
                    self?.displayToast((response?.message)!)
                }
            }
        }
    }
    
    func updateTblUI(currentIndex: Int){
        DispatchQueue.main.async {
            self.notificationArray.remove(at: currentIndex)
            self.tblNotifications.reloadData()
//            self.tblNotifications.deleteRows(at: [IndexPath(row: currentIndex, section: 0)], with: .automatic)
        }
    }
    
    func navigateToGroup(groupID : String?){
        if let groupID = groupID{
            var groupModel = GroupListModel()
            groupModel.gid = Int(groupID)
            let groupDetailsController = storyboard?.instantiateViewController(withIdentifier: "groupDetailViewController") as! GroupDetailViewController
            groupDetailsController.currentGroupModel = groupModel
            navigationController?.pushViewController(groupDetailsController, animated: true)
        }
    }
    
    func navigateToUser(userID : String?){
        if let userID = userID{
            let groupDetailsController = storyboard?.instantiateViewController(withIdentifier: "groupDetailViewController") as! GroupDetailViewController
            groupDetailsController.userId = Int(userID)
            navigationController?.pushViewController(groupDetailsController, animated: true)
        }
        
    }
}

