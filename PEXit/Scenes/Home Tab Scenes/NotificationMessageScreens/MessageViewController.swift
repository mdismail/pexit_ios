//
//  MessageViewController.swift
//  PEXit
//
//  Created by Apple on 01/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class MessageViewController: UIViewController , UITableViewDataSource , UITableViewDelegate {
    
    @IBOutlet weak var tblMessages: UITableView!
    var messageArray = [MessageListModel]()
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(MessageViewController.handleRefresh(_:)),
                                 for: UIControlEvents.valueChanged)
        refreshControl.tintColor = UIColor.red
        
        return refreshControl
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblMessages.register(UINib(nibName: "MessageListTableViewCell", bundle: nil), forCellReuseIdentifier: "messageListCell")
        tblMessages.tableFooterView = UIView()
        tblMessages.rowHeight = UITableViewAutomaticDimension
        tblMessages.estimatedRowHeight = 107
        tblMessages.addSubview(refreshControl)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        fetchMessageList()
    }
    @objc func handleRefresh(_ refreshControl : UIRefreshControl){
        fetchMessageList()
        refreshControl.endRefreshing()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            self.tblMessages.contentOffset = CGPoint.zero
        }
        
    }
    
    func fetchMessageList(){
        let worker = MessageWorker()
        worker.fetchMessages{ [weak self](response) in
            DispatchQueue.main.async{
                if response.status == true{
                    self?.messageArray = response.datas!
                    self?.tblMessages.reloadData()
                }else{
                    self?.displayToast(response.message!)
                }
            }
        }
    }
    
    func deleteConversation(indexPath : IndexPath){
        
        let worker = MessageWorker()
        let currentConversationModel = messageArray[indexPath.row]
        var typeOfModel = ""
        var idToBeDeleted = ""
        if currentConversationModel.type == 1{
            typeOfModel = "direct"
            idToBeDeleted = currentConversationModel.toid!
        }else{
            typeOfModel = "group"
            // the group id comes with prefix g:
            if let range = currentConversationModel.toid?.range(of: "g:") {
                idToBeDeleted = currentConversationModel.toid![range.upperBound...].trimmingCharacters(in: .whitespaces)
                
            }
        }
        worker.deleteConversation(type: typeOfModel, idToBeDeleted: idToBeDeleted) { (response) in
            if response != nil{
                if response?.status == true{
                    DispatchQueue.main.async{
                        // remove the item from the data model
                        self.messageArray.remove(at: indexPath.row)
                        
                        // delete the table view row
                        self.tblMessages.deleteRows(at: [indexPath], with: .fade)
                    }
                }
                self.displayToast((response?.message)!)
            }else{
                self.displayToast(UNKNOWN_ERROR_MSG)
            }
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messageArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let messageCell = tableView.dequeueReusableCell(withIdentifier: "messageListCell", for: indexPath) as! MessageListTableViewCell
        messageCell.setMessageData(messageModel: messageArray[indexPath.row])
        return messageCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        let chatViewController = self.storyboard?.instantiateViewController(withIdentifier: "chatViewController") as! ChatViewController
        chatViewController.chatModel = messageArray[indexPath.row]
        self.navigationController?.pushViewController(chatViewController, animated: true)
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
            
            showCustomAlert(CONFIRMATION_ALERT, message: "You want to delete entire conversation history", okButtonTitle: UIAlertActionTitle.DELETE.rawValue,  cancelButtonTitle: UIAlertActionTitle.CANCEL.rawValue) { [unowned self](action) in
                switch action{
                case .DELETE:
                    self.deleteConversation(indexPath: indexPath)
                case .CANCEL:
                    print("Dismiss Alert")
                default:
                    print("Dismiss Alert")
                }
                
                
                
            }
            
        }
    }
}
