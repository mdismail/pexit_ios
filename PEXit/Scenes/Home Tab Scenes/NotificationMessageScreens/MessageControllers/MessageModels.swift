//
//  MessageModels.swift
//  PEXit
//
//  Created by Apple on 03/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation


class MessageListResponseModel : Codable{
    var status : Bool?
    var message : String?
    var datas : [MessageListModel]?
}

class MessageListModel : Codable{
    var lastMessage : String?
    var unreadChatCount : Int?
    var type : Int?
    var toid : String?
    var gimage : String?
    var pimage : [MessageUserModel]?
    
}

class NewMessageModel : Codable{
    var chatterMessage : String?
    var chatterIds : String?
    var toIds : String?
}

class NewMessageResponseModel : Codable{
    var status : Bool?
    var message : String?
}

class ConversationResponseModel : Codable{
    var status : Bool?
    var message : String?
    var datas : ConversationListResponse?
}

class ConversationListResponse : Codable{
    //var adminId : Int?
    var conversation : [ConversationList]?
    var membersDetails : [MessageUserModel]?
}

class ConversationList : Codable{
    var fpname : String?
    var fpimage : String?
    var timeAgo : String?
    var message : String?
    var translatedMessage : String?
    var date : String?
    var fromid : Int?
    var leftStatus : Int?
    var mid : Int?
    var forward : Int?
    var unreadMessage : Int?
}

class MessageUserModel : Codable{
    var pimage : String?
    var uname : String?
    var muid : Int?
}

class NotificationResponseModel : Codable{
    var status : Bool?
    var message : String?
    var datas : [NotificationListModel]?
}

class NotificationListModel : Codable{
    var pimage : String?
    var name : String?
    var message : String?
    var username : String?
    var gname : String?
    var gid : Int?
    var cuid : String?
    var created_by : String?
    var uid : Int?
    var fid : Int?
    var reqid : String?
    var template : Int?
    var type : Int?
}
