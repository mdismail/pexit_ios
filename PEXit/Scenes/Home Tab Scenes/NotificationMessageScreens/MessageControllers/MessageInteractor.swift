//
//  MessageInteractor.swift
//  PEXit
//
//  Created by Apple on 03/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation

protocol MessageInteractorInput{
    func fetchMessageUsers(searchString : String)
    func sendNewMessage(messageRequestModel : NewMessageModel)
}

protocol MessageInteractorOutput{
    func presentMessageUsers(response:ListOfIndividuals.Response)
    func displayResponse(response:NewMessageResponseModel)
    func displayChatResponse(response:ConversationResponseModel)
    func displayNewMemberResponse(response:CommonModel.BaseModel)
}

class MessageInteractor : MessageInteractorInput{
    var messageInteractorOutput : MessageInteractorOutput!
    var worker = MessageWorker()
    
     func fetchMessageUsers(searchString : String){
        worker.fetchListOfChatUsers(searchKey: searchString) { [weak self] (response) in
            if response != nil{
                self?.messageInteractorOutput.presentMessageUsers(response: response!)
            }
            
        }
    }
    
    func sendNewMessage(messageRequestModel : NewMessageModel){
        worker.sendMessage(messageModel: messageRequestModel) { [weak self] (response) in
            if response != nil{
                self?.messageInteractorOutput.displayResponse(response: response!)
            }
        }
    }
    
    func forwardMessage(userIds:String,messageIds:String){
        worker.forwardMessage(userIds: userIds, messageIds: messageIds) {  [weak self] (response) in
            if response != nil{
                self?.messageInteractorOutput.displayNewMemberResponse(response: response!)
            }
        }
    }
    
    func fetchChatOfUser(userID: String) {
        worker.getChatOfUser(userId: userID) {[weak self] (response) in
            if response != nil{
                self?.messageInteractorOutput.displayChatResponse(response: response!)
            }
        }
        
    }
    
    func addNewGroupMembers(groupId:Int,memberIds:String){
        worker.addMembersToGroup(groupId : groupId,chatterIds:memberIds) { [weak self] (response) in
            if response != nil{
                self?.messageInteractorOutput.displayNewMemberResponse(response: response!)
            }
        }
    }
}
