//
//  MessageConfigurator.swift
//  PEXit
//
//  Created by Apple on 03/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation
import UIKit
extension ComposeMessageViewController: MessagePresenterOutput{
    func displayChatResponse(response: ConversationResponseModel) {
//        <#code#>
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
}

extension MessageInteractor: ComposeMessageViewControllerOutput , ChatViewControllerOutput{
    
}

extension MessagePresenter: MessageInteractorOutput  {
    
    
}

extension ChatViewController : ChatPresenterOutput{
    
    
    
}


class MessageConfigurator{
    
    // MARK: Object lifecycle
    
    class var sharedInstance: MessageConfigurator{
        struct Static {
            static let instance =  MessageConfigurator()
        }
        
        return Static.instance
    }
    
    // MARK: Configuration
    
    func configure(viewController: ComposeMessageViewController){
        let router = MessageRouter()
        router.controller = viewController
        
        let presenter = MessagePresenter()
        presenter.output = viewController
        
        let interactor = MessageInteractor()
        interactor.messageInteractorOutput = presenter
        
        viewController.output = interactor
        viewController.router = router
       
    }
    
}


class ChatViewConfigurator{
    
    // MARK: Object lifecycle
    
    class var sharedInstance: ChatViewConfigurator{
        struct Static {
            static let instance =  ChatViewConfigurator()
        }
        
        return Static.instance
    }
    
    // MARK: Configuration
    
    func configure(viewController: ChatViewController){
        
        let router = MessageRouter()
        router.chatController = viewController
       
        let presenter = MessagePresenter()
        presenter.chatoutput = viewController
        
        let interactor = MessageInteractor()
        interactor.messageInteractorOutput = presenter
        viewController.router = router
        viewController.output = interactor
       
        
    }
    
}
