//
//  MessagePresenter.swift
//  PEXit
//
//  Created by Apple on 03/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation
protocol MessagePresenterInput{
    func presentMessageUsers(response:ListOfIndividuals.Response)
    func displayResponse(response:NewMessageResponseModel)
    func displayChatResponse(response:ConversationResponseModel)
    func displayNewMemberResponse(response:CommonModel.BaseModel)
}

protocol  MessagePresenterOutput: class{
    func presentMessageUsers(response:ListOfIndividuals.Response)
    func displayResponse(response:NewMessageResponseModel)
    func displayNewMemberResponse(response:CommonModel.BaseModel)
}

protocol  ChatPresenterOutput: class{
     func displayChatResponse(chatArray : ConversationListResponse?, message : String?)
     func presentMessageUsers(response:ListOfIndividuals.Response)
     func displayNewMemberResponse(response:CommonModel.BaseModel)
}

class MessagePresenter : MessagePresenterInput{
    weak var output: MessagePresenterOutput!
    weak var chatoutput : ChatPresenterOutput!
    
   func presentMessageUsers(response:ListOfIndividuals.Response){
    if let output = output{
        output.presentMessageUsers(response: response)
    }else{
        chatoutput.presentMessageUsers(response: response)
    }
    
   }

    func displayNewMemberResponse(response:CommonModel.BaseModel){
        if let output = output{
            output.displayNewMemberResponse(response: response)
        }else{
            chatoutput.displayNewMemberResponse(response: response)
        }
        
    }
    func displayResponse(response:NewMessageResponseModel){
        output.displayResponse(response: response)
    }
    
    func displayChatResponse(response:ConversationResponseModel){
        if response.status == true{
            chatoutput.displayChatResponse(chatArray : response.datas , message:"")
        }else{
            chatoutput.displayChatResponse(chatArray: nil,message: response.message ?? UNKNOWN_ERROR_MSG)
        }
        
    }
}
