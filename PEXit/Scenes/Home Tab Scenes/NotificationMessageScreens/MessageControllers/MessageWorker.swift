//
//  MessageWorker.swift
//  PEXit
//
//  Created by Apple on 03/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation
class MessageWorker{
    func fetchMessages(completionHandler: @escaping(_ response : MessageListResponseModel) -> Void) {
        START_LOADING_VIEW()
        ServiceManager.methodType(requestType: GET_REQUEST, url: GET_MESSAGE_LIST, params: nil ,paramsData: nil,completion: { (response,_ responseData, statusCode) in
            STOP_LOADING_VIEW()
            let decoder = JSONDecoder()
            do{
//                print("Response issss ---->",response)
                let messageResponse = try decoder.decode(MessageListResponseModel.self, from: responseData!)
                completionHandler(messageResponse)
            }catch{
                DispatchQueue.main.async{
                    if ((response as! [String : Any])["message"] as? String) != nil{
                        if ((response as! [String : Any])["message"] as? String) != "No chat records found"{
                            KEY_WINDOW?.makeToast((response as! [String : Any])["message"] as? String)
                        }
                    }
                }
                
                print("error")
            }
            
        }) { (response, statusCode) in
            STOP_LOADING_VIEW()
        }
    }
    
    func fetchNotifications(completionHandler: @escaping(_ response : NotificationResponseModel) -> Void) {
        START_LOADING_VIEW()
        ServiceManager.methodType(requestType: GET_REQUEST, url: GET_NOTIFICATIONS, params: nil ,paramsData: nil,completion: { (response,_ responseData, statusCode) in
            STOP_LOADING_VIEW()
            let decoder = JSONDecoder()
            do{
//                print("Response issss ---->",response)
                let messageResponse = try decoder.decode(NotificationResponseModel.self, from: responseData!)
                completionHandler(messageResponse)
            }catch{
                DispatchQueue.main.async{
                    if ((response as! [String : Any])["message"] as? String) != nil{
                        if ((response as! [String : Any])["message"] as? String) != "No records found"{
                            KEY_WINDOW?.makeToast((response as! [String : Any])["message"] as? String)
                        }
                        
                    }
                }
                
                print("error")
            }
            
        }) { (response, statusCode) in
            STOP_LOADING_VIEW()
        }
    }
    
    func fetchListOfChatUsers(searchKey : String,completionHandler: @escaping(_ response : ListOfIndividuals.Response?) -> Void){
        
        ServiceManager.methodType(requestType: POST_REQUEST, url: GET_LIST_OF_CHAT_USERS, params: ["nskey":searchKey] ,paramsData: nil,completion: { (response,_ responseData, statusCode) in
            
            let decoder = JSONDecoder()
            do{
                //                print("Response issss ---->",response)
                let usersResponse = try decoder.decode(ListOfIndividuals.Response.self, from: responseData!)
                completionHandler(usersResponse)
            }catch{
                DispatchQueue.main.async{
                    if ((response as! [String : Any])["message"] as? String) != nil{
                        KEY_WINDOW?.makeToast((response as! [String : Any])["message"] as? String)
                    }
                }
                
                print("error")
            }
            
        }) { (response, statusCode) in
            STOP_LOADING_VIEW()
        }
    }

    func sendMessage(messageModel : NewMessageModel,completionHandler: @escaping(_ response : NewMessageResponseModel?) -> Void){
        START_LOADING_VIEW()
        ServiceManager.methodType(requestType: POST_REQUEST, url: SEND_MESSAGE, params: ["chatterIds":messageModel.chatterIds,"toIds":messageModel.toIds,"chatterMessage":messageModel.chatterMessage] ,paramsData: nil,completion: { (response,_ responseData, statusCode) in
            STOP_LOADING_VIEW()
            let decoder = JSONDecoder()
            do{
                //                print("Response issss ---->",response)
                let messageResponse = try decoder.decode(NewMessageResponseModel.self, from: responseData!)
                completionHandler(messageResponse)
            }catch{
                DispatchQueue.main.async{
                    if ((response as! [String : Any])["message"] as? String) != nil{
                        KEY_WINDOW?.makeToast((response as! [String : Any])["message"] as? String)
                    }
                }
                
                print("error")
            }
            
        }) { (response, statusCode) in
            STOP_LOADING_VIEW()
        }
    }
    
    func forwardMessage(userIds : String,messageIds:String,completionHandler: @escaping(_ response : CommonModel.BaseModel?) -> Void){
        START_LOADING_VIEW()
        ServiceManager.methodType(requestType: POST_REQUEST, url: FORWARD_MESSAGE, params: ["chatterIds":userIds,"chatterMIds":messageIds] ,paramsData: nil,completion: { (response,_ responseData, statusCode) in
            STOP_LOADING_VIEW()
            let decoder = JSONDecoder()
            do{
                //                print("Response issss ---->",response)
                let forwardMessageResponse = try decoder.decode(CommonModel.BaseModel.self, from: responseData!)
                completionHandler(forwardMessageResponse)
            }catch{
                DispatchQueue.main.async{
                    if ((response as! [String : Any])["message"] as? String) != nil{
                        KEY_WINDOW?.makeToast((response as! [String : Any])["message"] as? String)
                    }
                }
                
                print("error")
            }
            
        }) { (response, statusCode) in
            STOP_LOADING_VIEW()
        }
    }
    
    
    func addMembersToGroup(groupId : Int,chatterIds:String,completionHandler: @escaping(_ response : CommonModel.BaseModel?) -> Void){
        START_LOADING_VIEW()
        ServiceManager.methodType(requestType: POST_REQUEST, url: ADD_GROUP_MEMBER, params: ["gid":groupId,"nchatterIds":chatterIds] ,paramsData: nil,completion: { (response,_ responseData, statusCode) in
            STOP_LOADING_VIEW()
            let decoder = JSONDecoder()
            do{
                //                print("Response issss ---->",response)
                let addMemberResponse = try decoder.decode(CommonModel.BaseModel.self, from: responseData!)
                completionHandler(addMemberResponse)
            }catch{
                DispatchQueue.main.async{
                    if ((response as! [String : Any])["message"] as? String) != nil{
                        KEY_WINDOW?.makeToast((response as! [String : Any])["message"] as? String)
                    }
                }
                
                print("error")
            }
            
        }) { (response, statusCode) in
            STOP_LOADING_VIEW()
        }
        
    }
    
    func getChatOfUser(userId : String,completionHandler: @escaping(_ response : ConversationResponseModel?) -> Void){
        START_LOADING_VIEW()
        ServiceManager.methodType(requestType: GET_REQUEST, url: GET_USER_CHAT(userId), params: nil ,paramsData: nil,completion: { (response,_ responseData, statusCode) in
            STOP_LOADING_VIEW()
            let decoder = JSONDecoder()
            do{
                print("Response issss ---->",response)
                let conversationResponse = try decoder.decode(ConversationResponseModel.self, from: responseData!)
                completionHandler(conversationResponse)
            }catch{
                DispatchQueue.main.async{
                    if ((response as! [String : Any])["message"] as? String) != nil{
                        KEY_WINDOW?.makeToast((response as! [String : Any])["message"] as? String)
                    }
                }
                
                print("error")
            }
            
        }) { (response, statusCode) in
            STOP_LOADING_VIEW()
        }
    }
    
    func deleteConversation(type:String,idToBeDeleted:String,completionHandler: @escaping(_ response : CommonModel.BaseModel?) -> Void){
        START_LOADING_VIEW()
        ServiceManager.methodType(requestType: DELETE_REQUEST, url: DELETE_CONVERSATION(type, idToBeDeleted: idToBeDeleted), params: nil ,paramsData: nil,completion: { (response,_ responseData, statusCode) in
            STOP_LOADING_VIEW()
            let decoder = JSONDecoder()
            do{
                
                let deleteResponse = try decoder.decode(CommonModel.BaseModel.self, from: responseData!)
                completionHandler(deleteResponse)
            }catch{
                DispatchQueue.main.async{
                    if ((response as! [String : Any])["message"] as? String) != nil{
                        KEY_WINDOW?.makeToast((response as! [String : Any])["message"] as? String)
                    }
                }
                
                print("error")
            }
            
        }) { (response, statusCode) in
            STOP_LOADING_VIEW()
        }
    }
    
    func leaveConversation(groupId:String,completionHandler: @escaping(_ response : CommonModel.BaseModel?) -> Void){
        START_LOADING_VIEW()
        ServiceManager.methodType(requestType: PUT_REQUEST, url: LEAVE_GROUP_CHAT(groupId), params: nil ,paramsData: nil,completion: { (response,_ responseData, statusCode) in
            STOP_LOADING_VIEW()
            let decoder = JSONDecoder()
            do{
                
                let deleteResponse = try decoder.decode(CommonModel.BaseModel.self, from: responseData!)
                completionHandler(deleteResponse)
            }catch{
                DispatchQueue.main.async{
                    if ((response as! [String : Any])["message"] as? String) != nil{
                        KEY_WINDOW?.makeToast((response as! [String : Any])["message"] as? String)
                    }
                }
                
                print("error")
            }
            
        }) { (response, statusCode) in
            STOP_LOADING_VIEW()
        }
    }
    
    func makeConversationUnread(type:String,idToBeUnread:String,completionHandler: @escaping(_ response : CommonModel.BaseModel?) -> Void){
        START_LOADING_VIEW()
        ServiceManager.methodType(requestType: PUT_REQUEST, url: MARK_CONVERSATION_UNREAD(type, idToBeUnread: idToBeUnread), params: nil ,paramsData: nil,completion: { (response,_ responseData, statusCode) in
            STOP_LOADING_VIEW()
            let decoder = JSONDecoder()
            do{
                
                let deleteResponse = try decoder.decode(CommonModel.BaseModel.self, from: responseData!)
                completionHandler(deleteResponse)
            }catch{
                DispatchQueue.main.async{
                    if ((response as! [String : Any])["message"] as? String) != nil{
                        KEY_WINDOW?.makeToast((response as! [String : Any])["message"] as? String)
                    }
                }
                
                print("error")
            }
            
        }) { (response, statusCode) in
            STOP_LOADING_VIEW()
        }
    }
    
   // NOTIFICATION APIS
    
    func acceptRejectRequest(apiName : String,params:[String:Any],completionHandler: @escaping(_ response : CommonModel.BaseModel?) -> Void){
        START_LOADING_VIEW()
        ServiceManager.methodType(requestType: POST_REQUEST, url: apiName, params: params as NSDictionary ,paramsData: nil,completion: { (response,_ responseData, statusCode) in
            STOP_LOADING_VIEW()
            let decoder = JSONDecoder()
            do{
                //                print("Response issss ---->",response)
                let acceptRejectResponse = try decoder.decode(CommonModel.BaseModel.self, from: responseData!)
                completionHandler(acceptRejectResponse)
            }catch{
                DispatchQueue.main.async{
                    if ((response as! [String : Any])["message"] as? String) != nil{
                        KEY_WINDOW?.makeToast((response as! [String : Any])["message"] as? String)
                    }
                }
                
                print("error")
            }
            
        }) { (response, statusCode) in
            STOP_LOADING_VIEW()
        }
        
    }
}



