//
//  MessageRouter.swift
//  PEXit
//
//  Created by Apple on 03/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation
import UIKit
import Material


protocol MessageRouterInput{
    func navigateToPopUpContoller(userArray:[ListOfIndividuals.IndividualUserModel]?,sender:AnyObject, popUpDirection : UIPopoverArrowDirection?)
}

class MessageRouter : NSObject,MessageRouterInput,UIPopoverPresentationControllerDelegate{
    weak var controller : ComposeMessageViewController!
    weak var chatController : ChatViewController!
    var popUpController : CommonViewController?
    func navigateToPopUpContoller(userArray:[ListOfIndividuals.IndividualUserModel]? = nil,sender: AnyObject , popUpDirection : UIPopoverArrowDirection? = .up) {
        var currentController : UIViewController?
        print("Here")
        if let chatController = chatController{
            currentController = chatController
        }else{
            currentController = controller
        }
        
        if popUpController == nil{
            
            popUpController = currentController?.storyboard?.instantiateViewController(withIdentifier: "commonViewController") as? CommonViewController
            popUpController?.modalPresentationStyle = .popover
            DispatchQueue.main.async { [weak self] in
                if #available(iOS 12.0, *) {
                    self?.popUpController?.preferredContentSize = CGSize(width : sender.frame.size.width , height :CGFloat((userArray?.count)! * COMMON_CONTROLLER_ROW_HEIGHT))
                } else {
                    // Fallback on earlier versions
                }
                // self?.popUpController?.individualUsersArray = userArray
                self?.popUpController?.sendUserArray(usersArray: userArray)
                if currentController is ChatViewController{
                    self?.popUpController?.delegate = currentController as! ChatViewController
                    self?.popUpController?.previousSelectedUsers = ((currentController as! ChatViewController).selectedUserArray)
                }
                if currentController is ComposeMessageViewController{
                    self?.popUpController?.delegate = currentController as! ComposeMessageViewController
                    self?.popUpController?.previousSelectedUsers = ((currentController as! ComposeMessageViewController).selectedUserArray)
                }
                self?.popUpController?.sendUserArray(usersArray: userArray)
                if self?.popUpController?.tblListOfOptions != nil{
                    self?.popUpController?.tblListOfOptions.reloadData()
                }
                
            }
            let popOver = popUpController?.popoverPresentationController
            popOver?.delegate = self
            popOver?.permittedArrowDirections = popUpDirection!
            popOver?.sourceView = sender as? UIView
            popOver?.sourceRect = sender.bounds
            // popUpController?.delegate = currentController
            currentController?.present(popUpController!, animated: true, completion: nil)
        }
        else{
            
            DispatchQueue.main.async { [weak self] in
                if #available(iOS 12.0, *) {
                    self?.popUpController?.preferredContentSize = CGSize(width : sender.frame.size.width , height :CGFloat((userArray?.count)! * COMMON_CONTROLLER_ROW_HEIGHT))
                } else {
                    // Fallback on earlier versions
                }
                
                if currentController is ChatViewController{
                    
                    self?.popUpController?.previousSelectedUsers = ((currentController as! ChatViewController).selectedUserArray)
                }
                if currentController is ComposeMessageViewController{
                    
                    self?.popUpController?.previousSelectedUsers = ((currentController as! ComposeMessageViewController).selectedUserArray)
                }
                //self?.popUpController?.previousSelectedUsers = (currentController.selectedUserArray)!
                self?.popUpController?.individualUsersArray = userArray
                if self?.popUpController?.tblListOfOptions != nil{
                    self?.popUpController?.tblListOfOptions.reloadData()
                }
            }
        }
        
        
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return .none
    }
    
    
    
    func popoverPresentationControllerDidDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) {
        popUpController = nil
    }
    
    
}
