//
//  ChatViewController.swift
//  PEXit
//
//  Created by Apple on 04/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import Presentr

protocol ChatViewControllerInput{
    func displayChatResponse(chatArray : ConversationListResponse?, message : String?)
    func presentMessageUsers(response:ListOfIndividuals.Response)
    //    func displayResponse(response:NewMessageResponseModel)
}

protocol ChatViewControllerOutput{
    func fetchChatOfUser(userID : String)
    func fetchMessageUsers(searchString : String)
    func forwardMessage(userIds:String,messageIds:String)
    //    func sendNewMessage(messageRequestModel : NewMessageModel)
}

class ChatViewController: UIViewController , ChatViewControllerInput , ItemSelected {
    @IBOutlet weak var tblMessages: UITableView!
    @IBOutlet weak var txtViewMessage: UITextView!
    @IBOutlet weak var btnSend: UIButton!
    @IBOutlet weak var lblGroupMember: UILabel!
    @IBOutlet weak var txtFieldUserName: UITextField!
    @IBOutlet weak var collectionViewUsers: UICollectionView!
    @IBOutlet weak var viewForwardMessage: UIView!
    @IBOutlet weak var heightConstraintViewForwardMessage: NSLayoutConstraint!
    @IBOutlet weak var heightConstraintCollectionView: NSLayoutConstraint!
    
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    var selectedUserArray = [ListOfIndividuals.IndividualUserModel]()
    var chatModel : MessageListModel?
    var output : ChatViewControllerOutput!
    var userChatArary = [ConversationList]()
    var sectionArray = [String]()
    var messageArray = [[ConversationList]]()
    var selectedMessages = [ConversationList]()
    var router : MessageRouter!
    var startIndex = 0
    var endIndex = 0
    var isForwardMessage = false
    var arrayLangInfo = [AllLangDetails]()
    var keyboardHeight : CGFloat?
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetUp()
//        NotificationCenter.default.addObserver(self, selector: #selector(ChatViewController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
//          NotificationCenter.default.addObserver(self, selector: #selector(ChatViewController.UIKeyboardDidShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        
//        NotificationCenter.default.addObserver(self, selector: #selector(ChatViewController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(ChatViewController.languageUpdateNotification(notification:)), name: Notification.Name("LanguageUpdated"), object: nil)
    }
    
    override func viewWillAppear(_ animated : Bool){
//        IQKeyboardManager.shared().isEnabled = false
        showNavigationBar()
      showNavigationBarWithLeftAndRightBarButtonItems(languageTranslator: "language")
        if chatModel?.type == 1{
            addBackButton(withTitle: "Message")
        }else{
            addBackButton(withTitle: "Group Message")
            addMoreActionBtn()
        }
    }
    
    override func viewWillDisappear(_ animated : Bool){
//        IQKeyboardManager.shared().isEnabled = true
        hideNavigationBar()
        CURRENT_APP_LANGUAGE = "en"
        SELECTED_LANGUAGE = "English"
    }
 
    override func awakeFromNib() {
        ChatViewConfigurator.sharedInstance.configure(viewController: self)
    }

    
    func initialSetUp(){
        heightConstraintViewForwardMessage.constant = 0
        viewForwardMessage.isHidden = true
        txtViewMessage.placeholder = "Write a message"
        tblMessages.register(UINib(nibName: "ChatTableViewCell", bundle: nil), forCellReuseIdentifier: "chatTableViewCell")
        collectionViewUsers.register(UINib(nibName: "JobSearchCell", bundle: nil), forCellWithReuseIdentifier: "JobSearchCell")
        tblMessages.tableFooterView = UIView()
        output.fetchChatOfUser(userID: (chatModel?.toid)!)
       
    }
    
    
    override func showLanguages(){
        getLanguages { (languagesArray,completeLanguageDetails) in
            self.arrayLangInfo = completeLanguageDetails
            self.presentLanguagesView(languageArray: languagesArray)
        }
    }
    
    func presentLanguagesView(languageArray : [String]){
        DispatchQueue.main.async {
            let presenter = Presentr(presentationType: .popup)
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "commonViewController") as! CommonViewController
            // controller.isFromHome = true
            controller.isLanguageUpdation = true
            controller.itemsArray = languageArray
            controller.delegate = self
            self.customPresentViewController(presenter, viewController: controller, animated: true)
        }
    }
    
    func addMoreActionBtn(){
        let moreActionBtn = UIBarButtonItem(image: UIImage(named: "MoreBtn"), style: .plain, target: self, action: #selector(self.actionBtnTapped))
        if self.navigationItem.rightBarButtonItems?.count > 0{
            let rightBarItem = self.navigationItem.rightBarButtonItems![0]
        self.navigationItem.setRightBarButtonItems([rightBarItem,moreActionBtn], animated: true)
        }
       
    }
    
    @objc func actionBtnTapped(_ sender : UIBarButtonItem){
        displayActions()
    }
    
    func displayActions(){
        let presenter: Presentr = {
            let width = ModalSize.custom(size: 240)
            let height = ModalSize.custom(size: 220)
            let center = ModalCenterPosition.customOrigin(origin: CGPoint(x: self.view.frame.size.width/2 - 90, y: ((navigationController?.navigationBar.frame.size.height)! + 10)))
            let customType = PresentationType.custom(width: width, height: height, center: center)
            
            let customPresenter = Presentr(presentationType: customType)
            customPresenter.transitionType = .coverVerticalFromTop
            customPresenter.dismissTransitionType = .crossDissolve
            customPresenter.roundCorners = false
            customPresenter.backgroundColor = .black
            customPresenter.backgroundOpacity = 0.5
            customPresenter.dismissOnSwipe = true
            customPresenter.dismissOnSwipeDirection = .top
            return customPresenter
        }()
        let optionsController = self.storyboard?.instantiateViewController(withIdentifier: "commonViewController") as! CommonViewController
        optionsController.delegate = self
        optionsController.itemsArray = MESSAGE_ACTION_ARRAY
        customPresentViewController(presenter, viewController: optionsController, animated: true, completion: nil)
    }
    
    func optionSelected(option: String?) {
        switch option{
        case MESSAGE_ACTIONS.ADD_PEOPLE.rawValue:
            addNewMembers()
        case MESSAGE_ACTIONS.LEAVE_CONVERSATION.rawValue:
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                self.leaveGroupConfirmation()
            }
        case MESSAGE_ACTIONS.MARK_AS_UNREAD.rawValue:
            markConversationUnread()
        case MESSAGE_ACTIONS.FORWARD_CONVERSTAION.rawValue:
            if !isForwardMessage{
                isForwardMessage = true
                viewForwardMessage.isHidden = false
                heightConstraintViewForwardMessage.constant = 100
                btnSend.setTitle("Forward", for: .normal)
                tblMessages.setEditing(true, animated: true)
            }
           
        case MESSAGE_ACTIONS.DELETE_CONVERSATION.rawValue:
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                self.deleteCoversationConfirmation()
            }
        default:
            print("Selected option is--->",option)
            CURRENT_APP_LANGUAGE = arrayLangInfo.filter{$0.name == option}[0].code
            SELECTED_LANGUAGE = arrayLangInfo.filter{$0.name == option}[0].name
            startIndex = 0
            endIndex = 0
            translateToCurrentSelectedLanguage()
        }
    }
    
    func leaveGroupConfirmation(){
        showCustomAlert(CONFIRMATION_ALERT, message: LEAVE_GROUP_ALERT, okButtonTitle: UIAlertActionTitle.LEAVE.rawValue, cancelButtonTitle: UIAlertActionTitle.CANCEL.rawValue) { [unowned self](action) in
            switch action{
            case .DELETE:
                self.leaveGroup()
            case .CANCEL:
                print("Dismiss Alert")
            default:
                print("Dismiss Alert")
            }
        }
    }
    
    func leaveGroup(){
        let worker = MessageWorker()
        if let range = chatModel?.toid?.range(of: "g:") {
            let groupId = (chatModel?.toid![range.upperBound...].trimmingCharacters(in: .whitespaces))!
            worker.leaveConversation(groupId: groupId) { (response) in
                if response != nil{
                    if response?.status == true{
                        self.backAction()
                    }
                    self.displayToast(response?.message ?? UNKNOWN_ERROR_MSG)
                }
            }
        }
    }
    
    func addNewMembers(){
        let addPeopleController = self.storyboard?.instantiateViewController(withIdentifier: "composeMessageViewController") as! ComposeMessageViewController
        addPeopleController.currentGroup = chatModel
        navigationController?.pushViewController(addPeopleController, animated: true)
    }
    
    func markConversationUnread(){
        let worker = MessageWorker()
        let conversationType = chatModel?.type == 1 ? "direct" : "group"
        if let range = chatModel?.toid?.range(of: "g:") {
            let groupId = (chatModel?.toid![range.upperBound...].trimmingCharacters(in: .whitespaces))!
            worker.makeConversationUnread(type: conversationType, idToBeUnread: groupId) { (response) in
                if response != nil{
                    if response?.status == true{
                        self.backAction()
                    }
                    self.displayToast(response?.message ?? UNKNOWN_ERROR_MSG)
                }
            }
        }
        
    }
    
    func deleteCoversationConfirmation(){
        showCustomAlert(CONFIRMATION_ALERT, message: "You want to delete entire conversation history", okButtonTitle: UIAlertActionTitle.DELETE.rawValue,  cancelButtonTitle: UIAlertActionTitle.CANCEL.rawValue) { [unowned self](action) in
            switch action{
            case .DELETE:
                self.deleteConversation()
            case .CANCEL:
                print("Dismiss Alert")
            default:
                print("Dismiss Alert")
            }
        }
    }
    
    func deleteConversation(){
        let worker = MessageWorker()
        var typeOfModel = ""
        var idToBeDeleted = ""
        if chatModel?.type == 1{
            typeOfModel = "direct"
            idToBeDeleted = (chatModel?.toid!)!
        }else{
            typeOfModel = "group"
            // the group id comes with prefix g:
            if let range = chatModel?.toid?.range(of: "g:") {
                idToBeDeleted = (chatModel?.toid![range.upperBound...].trimmingCharacters(in: .whitespaces))!
            }
        }
        worker.deleteConversation(type: typeOfModel, idToBeDeleted: idToBeDeleted) { (response) in
            if response != nil{
                if response?.status == true{
                    self.backAction()
                }
                self.displayToast((response?.message)!)
            }else{
                self.displayToast(UNKNOWN_ERROR_MSG)
            }
        }
    }
    
    func displayChatResponse(chatArray : ConversationListResponse?, message : String?){
        if chatArray != nil{
            userChatArary = (chatArray?.conversation)!
            DispatchQueue.main.async{ [unowned self] in
                self.getDate()
                if self.chatModel?.type != 1{
                    let members = chatArray?.membersDetails?.filter{$0.muid == Defaults.getUserID()}
                    if members?.count == 0{
                       // self.navigationItem.rightBarButtonItem = nil
                        self.navigationItem.rightBarButtonItems = nil
                        self.lblGroupMember.isHidden = false
                        self.txtViewMessage.isHidden = true
                        self.btnSend.isHidden = true
                    }
                }
                
            }
        }else{
            displayToast(message!)
        }
    }
    
    
    func displayNewMemberResponse(response: CommonModel.BaseModel) {
        
        if response.status == true{
            DispatchQueue.main.async{ [weak self] in
                self?.isForwardMessage = false
                self?.viewForwardMessage.isHidden = true
                self?.heightConstraintViewForwardMessage.constant = 0
                self?.heightConstraintCollectionView.constant = 0
                self?.btnSend.setTitle("SEND", for: .normal)
                self?.tblMessages.setEditing(false, animated: true)
                self?.selectedUserArray.removeAll()
                self?.selectedMessages.removeAll()
                self?.collectionViewUsers.reloadData()
                // self?.resizeCollectionViewSize()
            }
            
        }
        displayToast(response.message ?? UNKNOWN_ERROR_MSG)
    }
    
    
    @IBAction func sendMessageClicked(_ sender: Any) {
        self.view.endEditing(true)
        txtViewMessage.resignFirstResponder()
        isForwardMessage == true ? forwardUserMessage() : sendNewUserMessage()
    }
    
    
    func forwardUserMessage(){
        if selectedMessages.count > 0 {
            if selectedUserArray.count > 0{
                let userIds = selectedUserArray.compactMap{$0.id}.map{"\($0)"}.joined(separator: ",")
                let messageIds = selectedMessages.compactMap{$0.mid}.map{"\($0)"}.joined(separator: ",")
                output.forwardMessage(userIds: userIds, messageIds: messageIds)
                return
            }else{
                displayToast("Please choose a user")
                return
            }
            
        }else{
            displayToast("Please select a message")
        }
    }
    
    func sendNewUserMessage(){
        if txtViewMessage.text.count > 0{
            let messageModel = getMessageRequestModel()
            let worker = MessageWorker()
            worker.sendMessage(messageModel: messageModel) { [weak self] (response) in
                if response != nil{
                    if response?.status == true{
                        DispatchQueue.main.async{
                            self?.txtViewMessage.text = ""
                            self?.messageArray.removeAll()
                            self?.sectionArray.removeAll()
                            self?.output.fetchChatOfUser(userID: (self?.chatModel?.toid)!)
                            
                        }
                    }
                   
                    self?.displayToast(response?.message ?? UNKNOWN_ERROR_MSG)
                }
            }
        }else{
            displayToast(ENTER_MESSAGE)
        }
    }
    
    func getMessageRequestModel() -> NewMessageModel{
        let newMessageModel = NewMessageModel()
        newMessageModel.chatterMessage = txtViewMessage.text
        if chatModel?.type == 1{
            newMessageModel.toIds = ""
            newMessageModel.chatterIds = (chatModel?.toid)!
        }else{
            newMessageModel.toIds = (chatModel?.toid)!
            newMessageModel.chatterIds = ""
            
        }
        
        return newMessageModel
    }
    
    func getDate(){
        let dateFormatter = DateFormatter()
        for chatObject in userChatArary{
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let dateString = chatObject.date
            let dateObj = dateFormatter.date(from: dateString!)
            dateFormatter.dateFormat = "MMM dd,yyyy"
            sectionArray.append("\(dateFormatter.string(from: dateObj!))")
            //print("Dateobj: \(dateFormatter.string(from: dateObj!))")
        }
        sectionArray = sectionArray.removeDuplicates()
        
        sectionArray = sectionArray.sorted(by: { dateFormatter.date(from:$0)?.compare((dateFormatter.date(from:$1))!) == .orderedAscending })
        for date in sectionArray{
            var conversationArray = [ConversationList]()
            for chatObject in userChatArary{
                dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                let dateString = chatObject.date
                let dateObj = dateFormatter.date(from: dateString!)
                dateFormatter.dateFormat = "MMM dd,yyyy"
                let formattedDate = "\(dateFormatter.string(from: dateObj!))"
                if date == formattedDate{
                    conversationArray.append(chatObject)
                }
            }
            messageArray.append(conversationArray)
        }
        self.tblMessages.reloadData()
//        if CURRENT_APP_LANGUAGE != PREVIOUS_APP_LANGUAGE{
//            self.translateToCurrentSelectedLanguage()
//        }
       
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+0.1, execute:{
            self.tblMessages.scrollToBottom(animated: true)
        })
        
    }
    
    func translateToCurrentSelectedLanguage(){
        let translationWorker = LanguageTranslationWoker()
        var stringsToTranslate = [String]()
        endIndex = startIndex
        print("VALUE OF SI",startIndex)
        for i in startIndex..<userChatArary.count{
            print("value of i --->",i)
            let messageModel = userChatArary[i]
            if messageModel.message != nil{
                if (stringsToTranslate.map({$0.count}).reduce(0, +)) < 4500 {
                    //endIndex = i
                    stringsToTranslate.append(messageModel.message?.htmlDecodedText() ?? "")
                    endIndex = endIndex + 1
                }else{
                    
                    //print("VALUE OF EI---->",i,endIndex)
                    break
                }
                
            }
            //i = i + 1;
        }
        //endIndex = i
        
        print("END INDEX IS ---> ",endIndex)
        translationWorker.translateCurrentString(newLanguageCode: CURRENT_APP_LANGUAGE, stringsToTranslate: stringsToTranslate) { (response) in
            if response != nil{
                if let translations = response as? [ReturnedJson]{
                DispatchQueue.main.async {
                    var i = self.startIndex
                    print("VALUE OF I AND SI and EI AFTER RESPONSE--->",i,self.startIndex, self.endIndex)
                    for value in translations{
                        if i < self.userChatArary.count{
                            for translatedStr in value.translations{
                                print("TRANSLATED STRING IS --->",translatedStr)
                                
                                self.userChatArary[i].translatedMessage = translatedStr.text
                                //self.tblMessages.reloadData()
                            }
                            i = i + 1
                        }
                        
                    }
                    self.tblMessages.reloadData()
                    self.startIndex = self.endIndex
                    print("VALUE OF I AND SI and EI AFTER RESPONSE--->",self.startIndex, self.endIndex)
                    if self.endIndex < self.userChatArary.count{
                        self.translateToCurrentSelectedLanguage()
                    }
                }
            }
        }
        }
    }
}

extension ChatViewController : UITableViewDataSource , UITableViewDelegate , UITextViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sectionArray.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sectionArray[section]
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messageArray[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let messageCell = tableView.dequeueReusableCell(withIdentifier: "chatTableViewCell", for: indexPath) as! ChatTableViewCell
        let messageModel = messageArray[indexPath.section]
        messageCell.setData(chatModel : messageModel[indexPath.row])
        return messageCell
    }
    
//    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
//      //  return UITableViewCellEditingStyle(rawValue: 3)!
//    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if viewForwardMessage.isHidden == false{
            let messageModel = messageArray[indexPath.section]
            selectedMessages.append(messageModel[indexPath.row])
        }
        
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if viewForwardMessage.isHidden == false{
            let messageModel = messageArray[indexPath.section]
            selectedMessages.remove(messageModel[indexPath.row])
        }
    }
    
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int)
    {
        let header = view as! UITableViewHeaderFooterView
        header.textLabel?.font = UIFont(name: "Arial-BoldMT", size: 10)!
        header.textLabel?.textColor = UIColor.black
        header.textLabel?.textAlignment = .center
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let messageModel = messageArray[indexPath.section]
        let currentModel = messageModel[indexPath.row]
        if currentModel.leftStatus == 1{
            return 30
        }else{
            return UITableViewAutomaticDimension
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        textView.updateTextViewPlacholder()
       // var tblCurrentFrame = tblMessages.frame
      //  tblCurrentFrame =
//        let height = textView.newHeight(withBaseHeight: 35)
        
     //   tblMessages.contentInset =
//        tblMessages.contentInset = UIEdgeInsetsMake(0, 0, self.view.frame.size.height - (keyboardHeight ?? 0) + height + 30, 0)
//    tblMessages.setContentOffset(CGPoint.init(x: 0, y: self.view.frame.size.height - (keyboardHeight ?? 0) + height + 30),animated: true)
       // bottomConstraint.constant =  (keyboardHeight ?? 0) + height
    }
    
}

extension ChatViewController :   UICollectionViewDataSource , UICollectionViewDelegate , UICollectionViewDelegateFlowLayout , UITextFieldDelegate{
    
    func individualUserSelected(userModel : [ListOfIndividuals.IndividualUserModel]?){
        txtFieldUserName.text = ""
        selectedUserArray = userModel!
        router.popUpController = nil
        print(selectedUserArray)
        heightConstraintCollectionView.constant = 40
        heightConstraintViewForwardMessage.constant = 100 + heightConstraintCollectionView.constant
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            self.collectionViewUsers.reloadData()
        }
        
    }
    
    func presentMessageUsers(response:ListOfIndividuals.Response){
        if response.status == true{
            DispatchQueue.main.async{
                if var responseUsers = response.datas{
                    responseUsers = responseUsers.filter{$0.id != Defaults.getUserID()}
                     self.router.navigateToPopUpContoller(userArray: responseUsers, sender: self.txtFieldUserName, popUpDirection: .up)
                }
//                self.router.navigateToPopUpContoller(userArray: response.datas, sender: self.txtFieldUserName, popUpDirection: .up)
            }
        }else{
            displayToast(response.message ?? UNKNOWN_ERROR_MSG)
        }
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(showListOfUsers), object: nil)
        self.perform(#selector(showListOfUsers), with: nil, afterDelay: 0.3)
        return true
    }
    
    @objc func showListOfUsers(){
        output.fetchMessageUsers(searchString: txtFieldUserName.text!)
    }
    
    //MARK: - COLLECTIONVIEW DATASOURCE AND DELEGATE
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return selectedUserArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let searchCell = collectionView.dequeueReusableCell(withReuseIdentifier: "JobSearchCell", for: indexPath) as! JobSearchCell
        searchCell.lblSearchText.text = ""
        searchCell.lblSearchText.text = selectedUserArray[indexPath.row].username
        searchCell.lblSearchText.sizeToFit()
        searchCell.btnSearch.tag = indexPath.row
        searchCell.btnSearch.addTarget(self, action: #selector(deleteCell(sender:)), for: UIControlEvents.touchUpInside)
        return searchCell
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout:
        UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = (selectedUserArray[indexPath.row]).username?.size(withAttributes: nil)
        return CGSize(width:size!.width + 50,height:collectionView.frame.size.height)
    }
    
    @objc func deleteCell(sender:UIButton){
        selectedUserArray.remove(at: sender.tag)
        collectionViewUsers.reloadData()
        resizeCollectionViewSize()
        
    }
    
    func resizeCollectionViewSize(){
        if selectedUserArray.count > 0{
            heightConstraintCollectionView.constant = 40
        }else{
            heightConstraintCollectionView.constant = 0
        }
        heightConstraintViewForwardMessage.constant = 100 + heightConstraintCollectionView.constant
    }
}

