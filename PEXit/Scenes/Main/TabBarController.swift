//
//  TabBarController.swift
//  PEXit
//
//  Created by Apple on 09/07/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController,UITabBarControllerDelegate {
    
    //    var type = Int()
    //
    //    var selectedString = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.delegate = self
//        configureTabBarsBasedOnLoggedInUser(Utils.getLoggedInUserType())
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        if (selectedViewController is HomeNavigationController) || (selectedViewController is SearchNavigationController) || (selectedViewController is PostProjectNavigationController) || (selectedViewController is NotificationsNavigationController) || (selectedViewController is ProfileNavigationController) {
//            hideNavigationBar()
//        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        print("viewWillDisappear called from TabBarController")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
     //MARK:- For Remove Tab
     func removeTab(at index: Int) {
     self.viewControllers?.remove(at: index)
     }
     
     
     //MARK:- For Add Tab
     func addTab(new vc:UIViewController ,at index: Int) {
     DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
     // your code here
     self.viewControllers?.insert(vc, at: index)
     }
     }
     */
    
    func configureTabBarsBasedOnLoggedInUser(_ userType:LOGGED_IN_USER_TYPE!) {
//        switch userType {
//        case .CUSTOMER_TYPE:
//            configureTabsForCustomerLogin()
//        case .SUPPLIER_TYPE:
//            configureTabsForSupplierLogin()
//        default:
//            break
//        }
    }
    
    func configureTabsForCustomerLogin() {
//        self.viewControllers?.removeAll()
//        let vc0 = self.storyboard?.instantiateViewController(withIdentifier: "HomeNavigationController") as? HomeNavigationController
//        let vc1 = self.storyboard?.instantiateViewController(withIdentifier: "SearchNavigationController") as? SearchNavigationController
//        let vc2 = self.storyboard?.instantiateViewController(withIdentifier: "PostProjectNavigationController") as? PostProjectNavigationController
//        let vc3 = self.storyboard?.instantiateViewController(withIdentifier: "NotificationsNavigationController") as? NotificationsNavigationController
//        let vc4 = self.storyboard?.instantiateViewController(withIdentifier: "ProfileNavigationController") as? ProfileNavigationController
//        self.viewControllers?.append(contentsOf:([vc0,vc1,vc2,vc3,vc4] as? [UINavigationController])!)
    }
    
    func configureTabsForSupplierLogin() {
//        self.viewControllers?.removeAll()
//        let vc0 = self.storyboard?.instantiateViewController(withIdentifier: "HomeNavigationController") as? HomeNavigationController
//        let vc1 = self.storyboard?.instantiateViewController(withIdentifier: "SearchNavigationController") as? SearchNavigationController
//        let vc2 = self.storyboard?.instantiateViewController(withIdentifier: "NotificationsNavigationController") as? NotificationsNavigationController
//        let vc3 = self.storyboard?.instantiateViewController(withIdentifier: "ProfileNavigationController") as? ProfileNavigationController
//        self.viewControllers?.append(contentsOf:([vc0,vc1,vc2,vc3] as? [UINavigationController])!)
    }
    
    
    // MARK: - UITabBarControllerDelegate
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
       
         let vc = tabBarController.viewControllers![tabBarController.selectedIndex]
        IQKeyboardManager.shared().previousNextDisplayMode = .alwaysShow

            if let controller = vc as? UINavigationController{
               
                if controller.viewControllers.count > 0{
                    let rootController = controller.viewControllers[0]
                    rootController.showNavigationBar()
                    for childController in controller.viewControllers{
                        if childController is LocationViewController{
                            (childController as! LocationViewController).searchController.dismiss(animated: false, completion: nil)
                        }
                    }
                    rootController.cleanUpGlobalSearch(previousSearchOption : CURRENT_GLOBAL_SEARCH_OPTION)
                }
                CURRENT_GLOBAL_SEARCH_KEYWORD = ""
                CURRENT_GLOBAL_SEARCH_OPTION = ""
                controller.popToRootViewController(animated: false)
          }
        
        return true
    }
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {

        if let vc = viewController as? UINavigationController {
            vc.popToRootViewController(animated: false)
        }
        
    }
    
    
    
}
