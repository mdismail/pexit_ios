//
//  TermsViewController.swift
//  PEXit
//
//  Created by Apple on 26/09/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import WebKit

class TermsViewController: UIViewController , WKNavigationDelegate , WKUIDelegate{
    @IBOutlet weak var webViewTerms: WKWebView!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var lblProgressBar: UILabel!
    var termsUrl : String = ""{
        didSet{
            print(termsUrl)
            DispatchQueue.main.async {
                if !(self.termsUrl.isEmpty){
                    self.webViewTerms.load(URLRequest(url: URL(string: self.termsUrl)!))

                }
                
//                 self.webViewTerms.addObserver(self, forKeyPath: "estimatedProgress", options: .new, context: nil);
            }
            
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        if termsUrl == ""{
            fetchUrl()
        }
        webViewTerms.navigationDelegate = self
        webViewTerms.uiDelegate = self
        prepareTabItem()
        // Do any additional setup after loading the view.
    }
    
    func fetchUrl(){
        let settingWorker = SettingsWorker()
        settingWorker.getStaticPageUrls { (response) in
            if response != nil{
                print("Response is --->",response)
                DispatchQueue.main.async{
                    if let response = response?.datas, response.terms != nil , !((response.terms?.isEmpty)!){
                        self.webViewTerms.load(URLRequest(url: URL(string: (response.terms)!)!))
                    }
//                    self.webViewTerms.load(URLRequest(url: URL(string: (response?.datas?.terms)!)!))
//                    self.webViewTerms.addObserver(self, forKeyPath: "estimatedProgress", options: .new, context: nil);
                }
                
            }
        }
    }
    
    func webView(_ webView: WKWebView,createWebViewWith configuration: WKWebViewConfiguration, for navigationAction: WKNavigationAction, windowFeatures: WKWindowFeatures) -> WKWebView?{
        
        UIApplication.shared.open(navigationAction.request.url!, options: [:], completionHandler: nil)
        
        return nil
    }


}

extension TermsViewController {
    fileprivate func prepareTabItem() {
        tabItem.title = " Terms  "
    }
}
