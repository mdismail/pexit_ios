//
//  AboutUsParentViewController.swift
//  PEXit
//
//  Created by Apple on 26/09/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class AboutUsParentViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
        // Do any additional setup after loading the view.
    }

    func setUpView(){
        let aboutUsController = storyboard?.instantiateViewController(withIdentifier: "aboutUsViewController") as! AboutUsViewController
        let termsController = storyboard?.instantiateViewController(withIdentifier: "termsViewController") as! TermsViewController
//        let pricingController = storyboard?.instantiateViewController(withIdentifier: "pricingViewController") as! PricingViewController
        let faqController = storyboard?.instantiateViewController(withIdentifier: "faqController") as! FAQViewController
        
//        let appController = AppTabsController(viewControllers:[aboutUsController,termsController,pricingController,faqController])
        let appController = AppTabsController(viewControllers:[aboutUsController,termsController,faqController])
       // self.view.translatesAutoresizingMaskIntoConstraints = false
       // appController.prepareButtons()
        self.add(asChildViewController: appController)
    }
    

    

}
