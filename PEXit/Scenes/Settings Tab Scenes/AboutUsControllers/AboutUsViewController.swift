//
//  AboutUsViewController.swift
//  PEXit
//
//  Created by Apple on 26/09/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import WebKit

class AboutUsViewController: UIViewController , WKNavigationDelegate , WKUIDelegate {

    @IBOutlet weak var webViewAboutUs: WKWebView!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var lblProgressBar: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareTabItem()
        //webViewAboutUs.navigationDelegate = self
        webViewAboutUs.navigationDelegate = self
        webViewAboutUs.uiDelegate = self
        let settingWorker = SettingsWorker()
        settingWorker.getStaticPageUrls { (response) in
            if response != nil{
                var responseLocal = response
               // responseLocal?.datas = nil
//                print("Response is --->",response)
                DispatchQueue.main.async{
                    if let response = response?.datas, response.about != nil , !((response.about?.isEmpty)!){
                        self.webViewAboutUs.load(URLRequest(url: URL(string: (response.about)!)!))
                    }
//                    self.webViewAboutUs.load(URLRequest(url: URL(string: (responseLocal?.datas?.about)!)!))

//                print("CurrentControllers---->",self.navigationController?.viewControllers[0].childViewControllers)
                let settingCurrent = self.navigationController?.viewControllers[0].childViewControllers.filter{$0 is AboutUsParentViewController}
                if settingCurrent != nil{
                        let currentController = settingCurrent![0]
                        let aboutUsParentController = currentController as! AboutUsParentViewController
                        let termsController = (aboutUsParentController.childViewControllers[0] as! AppTabsController).viewControllers[1] as! TermsViewController
                        termsController.termsUrl = responseLocal?.datas?.terms ?? ""
//                     let pricingController = (aboutUsParentController.childViewControllers[0] as! AppTabsController).viewControllers[2] as! PricingViewController
//                    pricingController.pricingUrl = responseLocal?.datas?.pricing ?? ""
                    
                    let faqController = (aboutUsParentController.childViewControllers[0] as! AppTabsController).viewControllers[2] as! FAQViewController
                    faqController.faqUrl = responseLocal?.datas?.faq ?? ""
                    
                }
                
                }
            }
        }
    }
    
    func webView(_ webView: WKWebView,createWebViewWith configuration: WKWebViewConfiguration, for navigationAction: WKNavigationAction, windowFeatures: WKWindowFeatures) -> WKWebView?{
        
        UIApplication.shared.open(navigationAction.request.url!, options: [:], completionHandler: nil)
        
        return nil
    }
 

}

extension AboutUsViewController {
    fileprivate func prepareTabItem() {
        tabItem.title = "About"
    }
}
