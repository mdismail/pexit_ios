//
//  PricingViewController.swift
//  PEXit
//
//  Created by Apple on 26/09/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import WebKit

class PricingViewController: UIViewController , WKNavigationDelegate , WKUIDelegate{
    var pricingUrl : String = ""{
        didSet{
            print(pricingUrl)
            DispatchQueue.main.async{
                if !(self.pricingUrl.isEmpty){
                    self.webViewPricing.load(URLRequest(url: URL(string: self.pricingUrl)!))

                }
//                self.webViewPricing.addObserver(self, forKeyPath: "estimatedProgress", options: .new, context: nil);
            }
            
        }
    }
    @IBOutlet weak var webViewPricing: WKWebView!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var lblProgressBar: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        if pricingUrl == ""{
            fetchUrl()
        }
        webViewPricing.navigationDelegate = self
        webViewPricing.uiDelegate = self
        prepareTabItem()
        // Do any additional setup after loading the view.
    }
    
    
    func fetchUrl(){
        let settingWorker = SettingsWorker()
        settingWorker.getStaticPageUrls { (response) in
            if response != nil{
                print("Response is --->",response)
                DispatchQueue.main.async{
                    if let response = response?.datas, response.pricing != nil , !((response.pricing?.isEmpty)!){
                        self.webViewPricing.load(URLRequest(url: URL(string: (response.pricing)!)!))
                    }
                    //                    self.webViewTerms.load(URLRequest(url: URL(string: (response?.datas?.terms)!)!))
                    //                    self.webViewTerms.addObserver(self, forKeyPath: "estimatedProgress", options: .new, context: nil);
                }
                
            }
        }
    }
    
    func webView(_ webView: WKWebView,createWebViewWith configuration: WKWebViewConfiguration, for navigationAction: WKNavigationAction, windowFeatures: WKWindowFeatures) -> WKWebView?{
        
        UIApplication.shared.open(navigationAction.request.url!, options: [:], completionHandler: nil)
        
        return nil
    }

}

extension PricingViewController {
    fileprivate func prepareTabItem() {
        tabItem.title = "Pricing "
    }
}

