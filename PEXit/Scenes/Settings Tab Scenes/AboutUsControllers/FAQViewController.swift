//
//  FAQViewController.swift
//  PEXit
//
//  Created by ats on 27/12/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import WebKit

class FAQViewController: UIViewController  , WKNavigationDelegate ,WKUIDelegate{
    @IBOutlet weak var webViewFaq: WKWebView!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var lblProgressBar: UILabel!
    var faqUrl : String = ""{
        didSet{
            print(faqUrl)
            DispatchQueue.main.async {
                if !self.faqUrl.isEmpty{
                    self.webViewFaq.load(URLRequest(url: URL(string: self.faqUrl)!))
                }
//                self.webViewFaq.addObserver(self, forKeyPath: "estimatedProgress", options: .new, context: nil);
            }
            
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        if faqUrl == ""{
            fetchUrl()
        }
        
        webViewFaq.navigationDelegate = self
        webViewFaq.uiDelegate = self
        prepareTabItem()
        // Do any additional setup after loading the view.
    }

//    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
//        if let host = navigationAction.request.url?.host {
//            if host.contains("hackingwithswift.com") {
//                decisionHandler(.allow)
//                return
//            }
//        }
//
//        decisionHandler(.cancel)
//    }
    
   
    func fetchUrl(){
        let settingWorker = SettingsWorker()
        settingWorker.getStaticPageUrls { (response) in
            if response != nil{
                print("Response is --->",response)
                DispatchQueue.main.async{
                    if let response = response?.datas, response.faq != nil , !((response.faq?.isEmpty)!){
                        self.webViewFaq.load(URLRequest(url: URL(string: (response.faq)!)!))
                    }

                }
                
            }
        }
    }
    
    
    

    
    // this handles target=_blank links by opening them in the same view
//    func webView(_ webView: WKWebView, createWebViewWith configuration: WKWebViewConfiguration, for navigationAction: WKNavigationAction, windowFeatures: WKWindowFeatures) -> WKWebView? {
//        if navigationAction.targetFrame == nil {
//            webView.load(navigationAction.request)
//        }
//        return nil
//    }

    
    
//    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation) {
//        UIApplication.shared.isNetworkActivityIndicatorVisible = true
//        print("didStartProvisionalNavigation: \(navigation)")
//    }
//
//    func webView(_ webView: WKWebView, didReceiveServerRedirectForProvisionalNavigation navigation: WKNavigation) {
//        print("didReceiveServerRedirectForProvisionalNavigation: \(navigation)")
//    }
//
//    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation, withError error: Error) {
//        print("didFailProvisionalNavigation: \(navigation), error: \(error)")
//    }
//
//    func webView(_ webView: WKWebView, didFinishLoading navigation: WKNavigation) {
//        UIApplication.shared.isNetworkActivityIndicatorVisible = false
//        print("didFinishLoadingNavigation: \(navigation)")
//    }
    
    
//    func _webViewWebProcessDidCrash(_ webView: WKWebView) {
//        print("WebContent process crashed; reloading")
//    }
//
    func webView(_ webView: WKWebView,createWebViewWith configuration: WKWebViewConfiguration, for navigationAction: WKNavigationAction, windowFeatures: WKWindowFeatures) -> WKWebView?{
        //if <a> tag does not contain attribute or has _blank then navigationAction.targetFrame will return nil
//        if let trgFrm = navigationAction.targetFrame {
//
//            if(!trgFrm.isMainFrame){
//                UIApplication.shared.isNetworkActivityIndicatorVisible = true
//                UIApplication.shared.open(navigationAction.request.url!, options: [:], completionHandler: nil)
//             //   webViewFaq.load(navigationAction.request)
//            }
//            else{
//                 UIApplication.shared.open(navigationAction.request.url!, options: [:], completionHandler: nil)
//            }
//        }
        
         UIApplication.shared.open(navigationAction.request.url!, options: [:], completionHandler: nil)
        
        return nil
    }
    
//    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!){
//
//        UIApplication.shared.isNetworkActivityIndicatorVisible = false
////        print("didFinish: \(String(describing: webViewFaq.url)); stillLoading:\(self.webView.isLoading ? "NO" : "YES")")
////
//    }
//
//
//
//    func webView(_ webView: WKWebView, decidePolicyFor navigationResponse: WKNavigationResponse, decisionHandler: @escaping (_: WKNavigationResponsePolicy) -> Void) {
//        print("decidePolicyForNavigationResponse")
//        decisionHandler(.allow)
//    }
//
//    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (_: WKNavigationActionPolicy) -> Void) {
//
//        decisionHandler(.allow)
//    }
//
  
}

extension FAQViewController {
    fileprivate func prepareTabItem() {
        tabItem.title = " Support "
    }
}
