//
//  TransactionHistoryViewController.swift
//  PEXit
//
//  Created by Apple on 28/09/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class TransactionHistoryViewController: UIViewController , UITableViewDataSource , UITableViewDelegate{
    
    @IBOutlet weak var tblTransactionHistory: UITableView!
    @IBOutlet weak var lblNoTransaction: UILabel!
    var transactionsArray = [TransactionListModel]()
    let paymentModel = PaymentUpdationModel()
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(TransactionHistoryViewController.handleRefresh(_:)),
                                 for: UIControlEvents.valueChanged)
        refreshControl.tintColor = UIColor.red
        
        return refreshControl
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        tblTransactionHistory.tableFooterView = UIView()
        tblTransactionHistory.register(UINib(nibName: "TransactionHistoryTableViewCell", bundle: nil), forCellReuseIdentifier: "transactionHistoryCell")
        tblTransactionHistory.addSubview(refreshControl)
        // Do any additional setup after loading the view.
    }
    
    @objc func handleRefresh(_ refreshControl : UIRefreshControl){
        transactionsArray.removeAll()
        tblTransactionHistory.reloadData()
        fetchListOfTransactions()
        refreshControl.endRefreshing()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            self.tblTransactionHistory.contentOffset = CGPoint.zero
        }
    }
    
    override func viewWillAppear(_ animated : Bool){
        transactionsArray.removeAll()
        tblTransactionHistory.reloadData()
        fetchListOfTransactions()
    }

    func fetchListOfTransactions(){
        let worker = SettingsWorker()
        worker.fetchTransactionHistory{ [weak self] (response) in
            DispatchQueue.main.async{
                if response != nil {
                    if response?.status == true{
                        self?.transactionsArray = (response?.datas)!
                        if self?.transactionsArray.count > 0{
                            self?.tblTransactionHistory.isHidden = false
                            self?.tblTransactionHistory.reloadData()
                        }
                        else{
                            self?.tblTransactionHistory.isHidden = true
                            self?.lblNoTransaction.isHidden = false
                        }
                    }else{
                        self?.displayToast(response?.message ?? UNKNOWN_ERROR_MSG)
                    }
                }else{
                    self?.displayToast(UNKNOWN_ERROR_MSG)
                }
            }
        }
    }
//    func goToCCAvenueController(transactionModel : TransactionListModel){
//        let ccWebController = storyboard?.instantiateViewController(withIdentifier: "CCWebViewController") as! CCWebViewController
//        ccWebController.orderId = transactionModel.orderid ?? ""
//        ccWebController.delegate = self
//        ccWebController.amount = transactionModel.amount ?? ""
//        ccWebController.currency = transactionModel.currency ?? ""
//        //ccWebController.paymentModel = paymentModel
//        self.navigationController?.pushViewController(ccWebController, animated: true)
//    }
//    
//    func updatePaymentStatus(paymentStatus:String,paymentType:Int){
//        let checkoutworker = CheckoutWorker()
//        paymentModel.orderStatus = paymentStatus
//        
//        paymentModel.paymentType = paymentType // for paypal 2 is the type
//        checkoutworker.updatePaymentStatus( requestObject: self.paymentModel) { (response, error) in
//            print("response is --->",response)
//            if let response = response{
//                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
//                    self.navigationController?.popToRootViewController(animated: true)
//                }
//            }
//        }
//    }
    
    func navigateToCheckOutController(transactionModel : TransactionListModel){
        let checkoutController = storyboard?.instantiateViewController(withIdentifier: "checkoutViewController") as! CheckoutViewController
        checkoutController.isFromTransactionHistory = true
        checkoutController.transactionHistoryModel = transactionModel
        
        //checkoutController.productRequestModel = requestModel
        checkoutController.serviceType = transactionModel.shortName ?? ""
        navigationController?.pushViewController(checkoutController, animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return transactionsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let transactionCell = tableView.dequeueReusableCell(withIdentifier: "transactionHistoryCell", for: indexPath) as! TransactionHistoryTableViewCell
        transactionCell.setTransactionData(transactionModel: transactionsArray[indexPath.row])
        return transactionCell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 164
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            let transactionModel = transactionsArray[indexPath.row]
            if transactionModel.order_status != "Success"{
             navigateToCheckOutController(transactionModel: transactionModel)
            }
        }
    }

