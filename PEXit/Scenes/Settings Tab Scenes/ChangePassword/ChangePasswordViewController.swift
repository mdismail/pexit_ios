//
//  ChangePasswordViewController.swift
//  PEXit
//
//  Created by Apple on 24/09/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import SDWebImage
import GoogleSignIn


class ChangePasswordViewController: UIViewController {
    
    @IBOutlet weak var txtFieldNewPassword: UITextField!
    
    @IBOutlet weak var txtFieldConfirmPassword: UITextField!
    override func viewDidLoad() {
        txtFieldNewPassword.addPaddingAndBorder()
        txtFieldConfirmPassword.addPaddingAndBorder()
        super.viewDidLoad()
    }
    
    @IBAction func btnSubmitClicked(_ sender: Any) {
        self.dismissKeyboard()
        if  Utils.validateFormAndShowToastWithRequiredTextFields([txtFieldNewPassword,txtFieldConfirmPassword]){
            if txtFieldNewPassword.text == txtFieldConfirmPassword.text{
                let worker = SettingsWorker()
                worker.updatePassword(password: txtFieldNewPassword.text!) { (response) in
                    if response != nil && response?.status == true{
                            self.displayToast("Password Changed,Please login")
//                            KEY_WINDOW?.makeToast("Password Changed Successfully,Please login now")
                      
                            self.clearUserData()
                        
                    }
                }
                
            }else{
                displayToast(PASSWORD_NOT_MATCH_TOAST)
            }
        }
        
    }
    
    func clearUserData(){
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
           
            GIDSignIn.sharedInstance().signOut()
            LinkedInManager.sharedLinkedInManager.linkedInLogout()
            SDImageCache.shared().clearMemory()
            SDImageCache.shared().clearDisk()
            Defaults.setLoginStatus(false)
            RealmDBManager.sharedManager.deleteRealmDb()
            Utils.redirectToLoginScreen()
        }
           
    }
       
//        KEY_WINDOW?.makeToast("Password Changed Successfully,Please login now")
    }

