//
//  SettingsWorker.swift
//  PEXit
//
//  Created by Apple on 26/09/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation

class SettingsWorker{
    func updatePassword(password : String,completionHandler: @escaping(_ passwordResponse : NewPost.Response?) -> Void){
        START_LOADING_VIEW()
        
        ServiceManager.methodType(requestType: PUT_REQUEST, url: CHANGE_PASSWORD, params: ["password":password], paramsData: nil, completion: { (response,_ responseData, statusCode) in
            STOP_LOADING_VIEW()
            if response != nil{
                let decoder = JSONDecoder()
                do{
                    if let data = responseData{
                        let passwordResponse = try decoder.decode(NewPost.Response.self, from: data)
                        completionHandler(passwordResponse)
                    }
                }catch{
                    
                }
            }
            
        }) { (response, statusCode) in
            STOP_LOADING_VIEW()
        }
    }
    
    //STATIC_PAGES
    
    func getStaticPageUrls(completionHandler: @escaping(_ passwordResponse : StaticUrlsModel?) -> Void){
        START_LOADING_VIEW()
        
        ServiceManager.methodType(requestType: GET_REQUEST, url: STATIC_PAGES, params: nil, paramsData: nil, completion: { (response,_ responseData, statusCode) in
            STOP_LOADING_VIEW()
            if response != nil{
                let decoder = JSONDecoder()
                do{
                    if let data = responseData{
                        let passwordResponse = try decoder.decode(StaticUrlsModel.self, from: data)
                        completionHandler(passwordResponse)
                    }
                   
                }catch{
                    
                }
            }
            
        }) { (response, statusCode) in
            STOP_LOADING_VIEW()
        }
    }
    
    // TRANSACTION HISTORY
    
    func fetchTransactionHistory(completionHandler: @escaping(_ transactionHistoryResponse : TransactionHistoryModel?) -> Void){
        START_LOADING_VIEW()
        
        ServiceManager.methodType(requestType: GET_REQUEST, url: TRANSACTION_HISTORY, params: nil, paramsData: nil, completion: { (response,_ responseData, statusCode) in
            STOP_LOADING_VIEW()
            if response != nil{
                let decoder = JSONDecoder()
                do{
                    if let data = responseData{
                        let transactionHistoryResponse = try decoder.decode(TransactionHistoryModel.self, from: data)
                        completionHandler(transactionHistoryResponse)
                    }
                }catch{
                    
                }
            }
            
        }) { (response, statusCode) in
            STOP_LOADING_VIEW()
        }
    }
    
    func updateFCMToken(params:[String:String]){
//        print("CMAE HERE")
//        print("TOKEN ISS__-->", params["fcmToken"])
        ServiceManager.methodType(requestType: PUT_REQUEST, url: UPDATE_FCM, params: params as NSDictionary, paramsData: nil, completion: { (response,_ responseData, statusCode) in
//        print("FCM TOKEN ON SERVER --->",params["fcmToken"])
            if response != nil{
//                 print("RESPONSE OF FCM IS --->",response)
 
            }
            
        }) { (response, statusCode) in
           
        }
    }
}
