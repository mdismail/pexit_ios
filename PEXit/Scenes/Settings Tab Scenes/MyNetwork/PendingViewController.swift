//
//  PendingViewController.swift
//  PEXit
//
//  Created by Apple on 19/09/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import Material

protocol PendingViewControllerInput{
    func displayPendingContacts(response :[Connections.FetchConnections.UserRecord]?,message:String?)
}

protocol PendingViewControllerOutput{
    func fetchPendingContacts()
}

class PendingViewController: UIViewController , PendingViewControllerInput{
    
    @IBOutlet weak var tblPending: UITableView!
    @IBOutlet weak var lblNoPendingInvitation: UILabel!
    var output : PendingViewControllerOutput!
    var records = [Connections.FetchConnections.UserRecord]()
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(PendingViewController.handleRefresh(_:)),
                                 for: UIControlEvents.valueChanged)
        refreshControl.tintColor = UIColor.red
        
        return refreshControl
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareTabItem()
        tblPending.tableFooterView = UIView()
        tblPending.register(UINib(nibName: "PendingTableViewCell", bundle: nil), forCellReuseIdentifier: "pendingTableViewCell")
        tblPending.addSubview(refreshControl)
    }
    
    override func awakeFromNib(){
        super.awakeFromNib()
        MyNetworkConfigurator.sharedInstance.configure(viewController: self)
    }
    
    override func viewWillAppear(_ animated : Bool){
        output.fetchPendingContacts()
    }
   
    @objc func handleRefresh(_ refreshControl : UIRefreshControl){
        output.fetchPendingContacts()
        refreshControl.endRefreshing()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            self.tblPending.contentOffset = CGPoint.zero
        }
    }
    
    func displayPendingContacts(response :[Connections.FetchConnections.UserRecord]?,message:String?){
        if response != nil{
            records = response!
            records = records.sorted{$1.username > $0.username}
            DispatchQueue.main.async {
                if self.records.count == 0{
                    self.lblNoPendingInvitation.isHidden = false
            }else{
                self.tblPending.reloadData()
            }
        }
            
        }else{
            displayToast(message!)
        }
    }
}


extension PendingViewController : UITableViewDataSource , UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return records.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let pendingCell = tableView.dequeueReusableCell(withIdentifier: "pendingTableViewCell", for: indexPath) as! PendingTableViewCell
        pendingCell.setData(userDetails: records[indexPath.row])
        return pendingCell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(CONNECTION_CELL_HEIGHT)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let currentUser = records[indexPath.row]
        let groupDetailsController = storyboard?.instantiateViewController(withIdentifier: "groupDetailViewController") as! GroupDetailViewController
        groupDetailsController.userId = currentUser.id
        navigationController?.pushViewController(groupDetailsController, animated: true)
    }
}


extension PendingViewController {
    fileprivate func prepareTabItem() {
        if DeviceModel.IS_IPHONE_5 || DeviceModel.IS_IPHONE_4_OR_LESS || DeviceModel.IS_IPHONE_6{
            tabItem.title = "Pending"
        }else{
            tabItem.title = "    Pending    "
        }
        
    }
}
