//
//  GroupsViewController.swift
//  PEXit
//
//  Created by Apple on 19/09/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import Material

let GROUP_CELL_HEIGHT = 200

enum GroupSearch : String{
    case All = "all"
    case SpecificSearch = "search"
}

protocol GroupsViewControllerInput{
    func displayResult(response : [GroupListModel]?,message:String?)
    func displayCreateGroupResponse(response : NewPost.Response, type : Int)
    
}

protocol GroupsViewControllerOutput{
    func fetchGroups(request : GroupsRequestModel)
    func createGroup(request : CreateGroupRequestModel , isEdit : Bool)
    func joinGroup(groupModel:GroupListModel)
    func leaveGroup(groupModel:GroupListModel, type : Int)
}



class GroupsViewController: UIViewController , GroupsViewControllerInput , UITextFieldDelegate, UITextViewDelegate , ItemSelected{
    @IBOutlet weak var searchBarGroup: UISearchBar!
    @IBOutlet weak var btnCreateGroup: UIButton!
    @IBOutlet weak var btnShowAllGroups: UIButton!
    @IBOutlet weak var viewCreateGroup: UIView!
    @IBOutlet weak var viewGroupsList: UIView!
    @IBOutlet weak var txtFieldGroupName: TJTextField!
    @IBOutlet weak var txtViewGroupDescription: UITextView!
    @IBOutlet weak var txtFieldAssignTo: TJTextField!
    @IBOutlet weak var txtFieldTypeOfGroup: TJTextField!
    @IBOutlet weak var collectionViewGroups: UICollectionView!
    @IBOutlet weak var parentScrollView: UIScrollView!
    @IBOutlet weak var constraintGroupsViewHeight: NSLayoutConstraint!
    @IBOutlet weak var collectionViewUsers: UICollectionView!
    @IBOutlet weak var usersCollectionViewHeight: NSLayoutConstraint!
    @IBOutlet weak var lblDisclaimerText: UILabel!
    @IBOutlet weak var btnCreateEditGroup: UIButton!
  
    var idToBeDeleted : Int?
    var groupListModel = [GroupListModel]()
    var usersArray : [ListOfIndividuals.IndividualUserModel]?
    var indexPathArray = [IndexPath]()
    var isEditClicked = false
    var output : GroupsViewControllerOutput!
    var pageNumber = 1
    var router : GroupRouter!
    var groupId : Int?
    var isMoreDataAvailable = true
    var selectedUserArray = [ListOfIndividuals.IndividualUserModel]()
    let columnLayout = ColumnFlowLayout(
        cellsPerRow: 2,
        minimumInteritemSpacing: 10,
        minimumLineSpacing: 10,
        sectionInset: UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10),
        cellHeight : CGFloat(GROUP_CELL_HEIGHT)
    )
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(GroupsViewController.handleRefresh(_:)),
                                 for: UIControlEvents.valueChanged)
        refreshControl.tintColor = UIColor.red
        
        return refreshControl
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareTabItem()
        txtFieldTypeOfGroup.text = GROUPTYPES.PUBLIC.rawValue
    //    txtViewGroupDescription.placeholder = textViewPlaceholder
        collectionViewGroups?.collectionViewLayout = columnLayout
        collectionViewGroups?.contentInsetAdjustmentBehavior = .always
        collectionViewUsers.register(UINib(nibName: "JobSearchCell", bundle: nil), forCellWithReuseIdentifier: "JobSearchCell")
        collectionViewGroups.register(UINib(nibName: "GroupCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "groupCell")
        fetchGroupsFromServer(typeOfSearch: "")
        parentScrollView.addSubview(refreshControl)
        // Do any additional setup after loading the view.
    }
    
    override func awakeFromNib(){
        super.awakeFromNib()
        MyNetworkConfigurator.sharedInstance.configureGroupsController(viewController: self)
    }
    
    override func viewWillAppear(_ animated : Bool){
    }
    
    @objc func handleRefresh(_ refreshControl : UIRefreshControl){
        if viewCreateGroup.isHidden{
            pageNumber = 1
            searchBarGroup.text = ""
            groupListModel.removeAll()
            collectionViewGroups.reloadData()
            fetchGroupsFromServer(typeOfSearch: "")
            refreshControl.endRefreshing()
        }else{
            refreshControl.endRefreshing()
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            self.parentScrollView.contentOffset = CGPoint.zero
        }
        
    }
    
    func fetchGroupsFromServer(typeOfSearch : String){
        var request = GroupsRequestModel()
        request.page = pageNumber
        request.numberOfRecords = NUMBER_OF_RECORDS
        request.typeOfSearch = typeOfSearch
        request.key = searchBarGroup.text
        output.fetchGroups(request : request)
    }
    
    func displayResult(response: [GroupListModel]?, message: String?) {
        if response != nil && response?.count > 0{
           
            groupListModel.append(contentsOf: response!)
           // print("RESPONSE---->",response?.count,groupListModel.count)
            isMoreDataAvailable = (response?.count == NUMBER_OF_RECORDS) ? true : false
            DispatchQueue.main.async{
                
                self.constraintGroupsViewHeight.constant = CGFloat((ceilf(Float(Double(self.groupListModel.count) / 2.0))) * Float(GROUP_CELL_HEIGHT + 10)) + 140
                
               // self.parentScrollView.contentSize.height = self.constraintGroupsViewHeight.constant + self.viewGroupsList.frame.origin.y + 10
                
                self.view.layoutIfNeeded()
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                     self.collectionViewGroups.reloadData()
                })
               
            }
        
        }else{
            self.displayToast(message!)
        }
    }
    @IBAction func btnCreateGroupClicked(_ sender: Any) {
        searchBarGroup.text = ""
        btnCreateEditGroup.setTitle("Create", for: .normal)
        searchBarGroup.resignFirstResponder()
        if let requestModel = getGroupRequestModel(){
            output.createGroup(request: requestModel, isEdit: isEditClicked)
        }
        
    }
    
    func getGroupRequestModel() -> CreateGroupRequestModel?{
        if Utils.validateFormAndShowToastWithRequiredFields([txtFieldGroupName,txtFieldTypeOfGroup]){
            var requestModel = CreateGroupRequestModel()
            requestModel.gname = txtFieldGroupName.text
            requestModel.gtype = txtFieldTypeOfGroup.text
            requestModel.gdescription = txtViewGroupDescription.text
            let userEmail = ProfileRLMModel.fetchProfile()?.userEmail
            requestModel.gusers = selectedUserArray.count > 0 ? selectedUserArray.map{$0.email!}.filter{$0 != userEmail} : []
            if isEditClicked == true{
                requestModel.groupId = groupId
            }
            return requestModel
            
        }
        return nil
    }
    @IBAction func showAllGroupsClicked(_ sender: UIButton) {
        searchBarGroup.text = ""
        searchBarGroup.resignFirstResponder()
        sender.isSelected = !sender.isSelected
        groupListModel.removeAll()
        self.collectionViewGroups.reloadData()
        pageNumber = 1
        if sender.isSelected{
            fetchGroupsFromServer(typeOfSearch: GroupSearch.All.rawValue)
        }else{
            fetchGroupsFromServer(typeOfSearch: "")
        }
    }
    
    @IBAction func createNewGroupClicked(_ sender: Any) {
        viewCreateGroup.isHidden = false
        viewGroupsList.isHidden = true
        self.parentScrollView.contentSize.height = self.viewCreateGroup.frame.size.height + self.viewCreateGroup.frame.origin.y
    }
    @IBAction func btnCloseGroupClicked(_ sender: Any) {
        clearData()
        self.parentScrollView.contentSize.height = self.constraintGroupsViewHeight.constant + self.viewGroupsList.frame.origin.y
        viewCreateGroup.isHidden = true
        viewGroupsList.isHidden = false
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        switch textField{
        case txtFieldAssignTo:
            fetchConnections()
        case txtFieldTypeOfGroup:
//        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.view.endEditing(true)
            self.showGroupTypeOptions()
            return false
            
        //}
        default:
            return true
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.dismissKeyboard()
        }
        return true
    }
    
    func textViewDidChange(_ textView: UITextView) {
        txtViewGroupDescription.updateTextViewPlacholder()
        adjustTextViewHeight()
    }
    
    func showGroupTypeOptions(){
        self.router.navigateToPopUpContoller(currentArray:[GROUPTYPES.PUBLIC.rawValue,GROUPTYPES.PRIVATE.rawValue,GROUPTYPES.CLOSED.rawValue],sender:(self.txtFieldTypeOfGroup)!)
    }
    
    func adjustTextViewHeight() {
        let fixedWidth = txtViewGroupDescription.frame.size.width
        let newSize = txtViewGroupDescription.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
        parentScrollView.contentSize.height = parentScrollView.contentSize.height - txtViewGroupDescription.frame.size.height + newSize.height
        //self.textHeightConstraint.constant = newSize.height
        self.view.layoutIfNeeded()
    }
    
    func fetchConnections(){
//        if usersArray != nil{
//            self.router.navigateToPopUpContoller(userArray:self.usersArray,sender:(self.txtFieldAssignTo)!)
//            return
//        }
        let worker = NewPostWorker()
        worker.fetchListOfIndividuals{ [weak self] (message,response) in
            if let response = response{
                if (response.datas?.count)! > 0{
                    self?.usersArray = response.datas!
                self?.router.navigateToPopUpContoller(userArray:self?.usersArray,sender:(self?.txtFieldAssignTo)!)
                }
                else{
                    self?.displayToast(response.message!)
                }
                
            }else{
                self?.displayToast(message ?? UNKNOWN_ERROR_MSG)
            }
            
        }
    }
    
    func optionSelected(option : String?){
        txtFieldTypeOfGroup.text = option
        switch option{
        case GROUPTYPES.PUBLIC.rawValue :
            lblDisclaimerText.text = PUBLIC_GROUP_DISCLAIMER
        case GROUPTYPES.PRIVATE.rawValue :
            lblDisclaimerText.text = PRIVATE_GROUP_DISCLAIMER
        case GROUPTYPES.CLOSED.rawValue :
            lblDisclaimerText.text = CLOSED_GROUP_DISCLAIMER
        default:
            lblDisclaimerText.text = PUBLIC_GROUP_DISCLAIMER
        }
        txtFieldTypeOfGroup.resignFirstResponder()
    }
    
    func individualUserSelected(userModel : [ListOfIndividuals.IndividualUserModel]?){
        selectedUserArray = userModel!
        usersCollectionViewHeight.constant = 40
        self.parentScrollView.contentSize.height = self.parentScrollView.contentSize.height + usersCollectionViewHeight.constant
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.collectionViewUsers.reloadData()
        }
        txtFieldAssignTo.resignFirstResponder()
        
    }
    
    func displayCreateGroupResponse(response : NewPost.Response , type : Int){
        if response.message != nil {
            displayToast(response.message!)
        }
            if response.status == true{
                switch type{
                case TYPE_OF_GROUP_ACTIONS.LeaveGroup.rawValue:
                    DispatchQueue.main.async{ [weak self] in
                        self?.indexPathArray = [IndexPath(item:(self?.idToBeDeleted)!,section:0)]
                        self?.removeGroupFromList()
//                        self?.groupListModel.remove(at: (self?.idToBeDeleted)!)
//                        self?.collectionViewGroups.deleteItems(at: [IndexPath(item: (self?.idToBeDeleted)!, section: 0)])
                    }
                case TYPE_OF_GROUP_ACTIONS.JoinGroup.rawValue:
                     clearData()
                case TYPE_OF_GROUP_ACTIONS.CreateGroup.rawValue:
                     clearData()
                     DispatchQueue.main.async{
                        self.groupListModel.removeAll()
                        self.collectionViewGroups.reloadData()
                        self.pageNumber = 1
                        self.fetchGroupsFromServer(typeOfSearch: "")
                     }
                   
                    
                default:
                    break
            }
                
         }
        
    }
    
    func removeGroupFromList(){

        let indices = indexPathArray.map{ $0.item }
        groupListModel = groupListModel.enumerated().compactMap { indices.contains($0.0) ? nil : $0.1 }
        self.collectionViewGroups.deleteItems(at: indexPathArray)

    }
    
    func clearData(){
        DispatchQueue.main.async{ [weak self] in
            self?.txtFieldTypeOfGroup.text = ""
            self?.txtFieldGroupName.text = ""
            self?.txtFieldAssignTo.text = ""
            self?.txtViewGroupDescription.text = ""
            self?.txtViewGroupDescription.updateTextViewPlacholder()
            self?.groupId = nil
            self?.isEditClicked = false
            self?.selectedUserArray.removeAll()
            self?.collectionViewUsers.reloadData()
            self?.usersCollectionViewHeight.constant = 0
            self?.parentScrollView.contentSize.height = (self?.constraintGroupsViewHeight.constant)! + (self?.viewGroupsList.frame.origin.y)!
            self?.viewCreateGroup.isHidden = true
            self?.viewGroupsList.isHidden = false
        }
       
   }
}


extension GroupsViewController : UICollectionViewDataSource , UICollectionViewDelegate , UISearchBarDelegate , SendGroupAction , UICollectionViewDelegateFlowLayout , UIScrollViewDelegate{
    
    fileprivate func prepareTabItem() {
        tabItem.title = "  Groups  "
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collectionViewUsers{
            return selectedUserArray.count
        }else{
            return groupListModel.count
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == collectionViewUsers{
            let searchCell = collectionView.dequeueReusableCell(withReuseIdentifier: "JobSearchCell", for: indexPath) as! JobSearchCell
            searchCell.lblSearchText.text = ""
            searchCell.lblSearchText.text = selectedUserArray[indexPath.row].username
            searchCell.lblSearchText.sizeToFit()
            searchCell.btnSearch.tag = indexPath.row
            searchCell.btnSearch.addTarget(self, action: #selector(deleteCell(sender:)), for: UIControlEvents.touchUpInside)
            return searchCell
        }else{
           
            let groupListCell = collectionView.dequeueReusableCell(withReuseIdentifier: "groupCell", for: indexPath) as! GroupCollectionViewCell
            // print("CELL IS --->",indexPath.row, groupListCell.frame)
            groupListCell.delegate = self
            groupListCell.btnEdit.tag = groupListModel[indexPath.row].gid!
            groupListCell.btnJoinLeave.tag = groupListModel[indexPath.row].gid!
            groupListCell.btnViewPosts.tag = groupListModel[indexPath.row].gid!
            groupListCell.btnViewGroupDetails.tag = groupListModel[indexPath.row].gid!
            groupListCell.btnViewGroupDetails.addTarget(self, action: #selector(viewGroupDetails(sender:)), for: UIControlEvents.touchUpInside)
            groupListCell.setGroupData(groupModel:groupListModel[indexPath.row])
            return groupListCell
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout:
        UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == collectionViewUsers{
            let size = (selectedUserArray[indexPath.row]).username?.size(withAttributes: nil)
            return CGSize(width:size!.width + 50,height:collectionView.frame.size.height)
        }else{
            return CGSize(width:collectionView.frame.size.width,height:CGFloat(GROUP_CELL_HEIGHT))
        }
        
        
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if viewCreateGroup.isHidden{
            let isReachingEnd = scrollView.contentOffset.y >= 0
                && scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.frame.size.height)
            if isReachingEnd == true && isMoreDataAvailable == true{
                isMoreDataAvailable = false
                pageNumber = pageNumber + 1
                if btnShowAllGroups.isSelected{
                    fetchGroupsFromServer(typeOfSearch: GroupSearch.All.rawValue)
                }else{
                    if searchBarGroup.text == ""{
                        fetchGroupsFromServer(typeOfSearch: "")
                    }else{
                        fetchGroupsFromServer(typeOfSearch: GroupSearch.SpecificSearch.rawValue)
                    }
                    
                }
            }
        }
       
    }
    
    @objc func deleteCell(sender:UIButton){
        selectedUserArray.remove(at: sender.tag)
        collectionViewUsers.reloadData()
        if selectedUserArray.count == 0{
            usersCollectionViewHeight.constant = 0
            self.parentScrollView.contentSize.height = self.parentScrollView.contentSize.height + usersCollectionViewHeight.constant
            
        }
        
    }
    
    @objc func viewGroupDetails(sender:UIButton){
        let currentGroupModel = groupListModel.filter{$0.gid == sender.tag}[0]
        //let currentGroupModel = groupListModel[sender.tag]
        router.pushToGroupDetails(groupListModel: currentGroupModel)
    }
    
    func sendAction(type:Int,sender:UIButton){
        let currentGroupModel = groupListModel.filter{$0.gid == sender.tag}[0]
        switch type{
        case TYPE_OF_GROUP_ACTIONS.JoinGroup.rawValue:
            // groupId = currentGroupModel.gid
            if currentGroupModel.userKey == 1 || currentGroupModel.adminKey == 1{
                leaveGroup(groupModel :currentGroupModel,sender:sender.tag)
            }else{
                connectToGroup(groupModel :currentGroupModel)
            }
        case TYPE_OF_GROUP_ACTIONS.EditGroup.rawValue:
            groupId = currentGroupModel.gid
            editGroup(groupModel: currentGroupModel)
        case TYPE_OF_GROUP_ACTIONS.ViewPosts.rawValue:
            let postsController = self.storyboard?.instantiateViewController(withIdentifier: "HomePageViewController") as! HomePageViewController
            postsController.othersPost = currentGroupModel.gname
            self.navigationController?.pushViewController(postsController, animated: true)
            print("TEst")
        default:
            print("TEst")
        }
    }
    
    func connectToGroup(groupModel : GroupListModel){
        output.joinGroup(groupModel:groupModel)
    }
    
    func editGroup(groupModel : GroupListModel){
        btnCreateEditGroup.setTitle("Save", for: .normal)
        viewCreateGroup.isHidden = false
        viewGroupsList.isHidden = true
       
        txtViewGroupDescription.text = groupModel.gdescription
        txtViewGroupDescription.updateTextViewPlacholder()
        var contentSize = txtViewGroupDescription.sizeThatFits(txtViewGroupDescription.bounds.size)
         self.parentScrollView.contentSize.height = self.viewCreateGroup.frame.size.height + self.viewCreateGroup.frame.origin.y + contentSize.height
        if let groupUsers = groupModel.gusers{
            selectedUserArray = groupUsers
            if groupUsers.count > 0{
                usersCollectionViewHeight.constant = 40
                self.parentScrollView.contentSize.height = self.parentScrollView.contentSize.height + usersCollectionViewHeight.constant
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.collectionViewUsers.reloadData()
            }
            //collectionViewUsers.reloadData()
        }
       
        txtFieldGroupName.text = groupModel.gname
        
        txtFieldTypeOfGroup.text = groupModel.gtype
        isEditClicked = true
      }
    
    func leaveGroup(groupModel : GroupListModel , sender : Int){
        if groupModel.adminKey == 1{
            showCustomAlert(CONFIRMATION_ALERT, message: ADMIN_LEAVE_GROUP, okButtonTitle: UIAlertActionTitle.ADMINLEAVE.rawValue, cancelButtonTitle: UIAlertActionTitle.CANCEL.rawValue) { [unowned self](action) in
                switch action{
                case .DELETE:
                    self.idToBeDeleted = self.groupListModel.index{$0.gid == groupModel.gid}
                    self.output.leaveGroup(groupModel:groupModel,type:0)
                case .CANCEL:
                    print("Dismiss Alert")
                default:
                    print("Test")
                }
            }
        }else{
            showCustomAlert(CONFIRMATION_ALERT, message: LEAVE_GROUP_ALERT, okButtonTitle: UIAlertActionTitle.LEAVE.rawValue, cancelButtonTitle: UIAlertActionTitle.CANCEL.rawValue) { [unowned self](action) in
                switch action{
                case .DELETE:
                    self.idToBeDeleted = self.groupListModel.index{$0.gid == groupModel.gid}
                    self.output.leaveGroup(groupModel:groupModel,type:1)
                case .CANCEL:
                    print("Dismiss Alert")
                default:
                    print("Test")
                }
            }
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        clearData()
        btnShowAllGroups.isSelected = false
        searchBar.resignFirstResponder()
        groupListModel.removeAll()
        collectionViewGroups.reloadData()
      //  self.collectionViewGroups.reloadData()
        pageNumber = 1
        fetchGroupsFromServer(typeOfSearch: GroupSearch.SpecificSearch.rawValue)
    }
    
}
