//
//  MyConnectionsViewController.swift
//  PEXit
//
//  Created by Apple on 19/09/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import Material

let CONNECTION_CELL_HEIGHT = 73
let COLLECTION_VIEW_CELL_HEIGHT = 175

protocol MyConnectionsViewControllerInput{
    func displayResult(response : [Connections.FetchConnections.UserRecord]?,message:String?, typeOfResult : Int)
    func displayFriendRequestResponse(response:CommonModel.BaseModel)
}

protocol MyConnectionsViewControllerOutput{
    func fetchMyConnections()
    func fetchUsersList(pageNumber : Int?)
    func fetchSuggestedContacts(pageNumber : Int?)
    func sendFriendRequest(friendId : Int)
}

class MyConnectionsViewController: UIViewController , MyConnectionsViewControllerInput , UIScrollViewDelegate , UISearchBarDelegate{
    @IBOutlet weak var tblConnections: UITableView!
    @IBOutlet weak var collectionViewUsers: UICollectionView!
    @IBOutlet weak var tblConnectionsHeight: NSLayoutConstraint!
    @IBOutlet weak var collectionViewUsersHeight: NSLayoutConstraint!
    @IBOutlet weak var constraintTopViewHeight: NSLayoutConstraint!
    @IBOutlet weak var constraintMidViewHeight: NSLayoutConstraint!
    @IBOutlet weak var customView: UIView!
    @IBOutlet weak var lblAllConnections: UILabel!
    @IBOutlet weak var lblSuggestedContacts: UILabel!
    @IBOutlet weak var btnShowAllConnections: UIButton!
    @IBOutlet weak var searchBarConnections: UISearchBar!
    @IBOutlet weak var scrollViewParent: UIScrollView!
    var output : MyConnectionsViewControllerOutput!
    var records = [Connections.FetchConnections.UserRecord]()
    var usersArray = [Connections.FetchConnections.UserRecord]()
    var isAllUsers = false
    var isSearch = false
    var isMoreDataAvailable = true
    var pageNumber = 1
    var idToBeDeleted = 0
    var isUserSelected = false
    var local_keywords = ""
    var global_keywords = ""
    let columnLayout = ColumnFlowLayout(
        cellsPerRow: 2,
        minimumInteritemSpacing: 10,
        minimumLineSpacing: 10,
        sectionInset: UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10),
        cellHeight : CGFloat(COLLECTION_VIEW_CELL_HEIGHT)
    )
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(MyConnectionsViewController.handleRefresh(_:)),
                                 for: UIControlEvents.valueChanged)
        refreshControl.tintColor = UIColor.red
        
        return refreshControl
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareTabItem()
        //        let refreshControl = UIRefreshControl()
        //        collectionViewUsers.bottomRefreshControl = refreshControl
        searchBarConnections.showsCancelButton = false
        collectionViewUsers.delegate = self
        scrollViewParent.delegate = self
        searchBarConnections.delegate = self
        tblConnections.tableFooterView = UIView()
        // collectionViewUsers.isScrollEnabled = true
        collectionViewUsers?.collectionViewLayout = columnLayout
        collectionViewUsers?.contentInsetAdjustmentBehavior = .always
        tblConnections.register(UINib(nibName: "MyConnectionTableViewCell", bundle: nil), forCellReuseIdentifier: "myConnectionListCell")
        collectionViewUsers.register(UINib(nibName: "ConnectionCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "connectionCollectionViewCell")
        scrollViewParent.addSubview(refreshControl)
        // Do any additional setup after loading the view.
    }
    
    override func awakeFromNib(){
        super.awakeFromNib()
        MyNetworkConfigurator.sharedInstance.configureConnectionsController(viewController: self)
    }
    
    override func viewWillAppear(_ animated:Bool){
        if !isUserSelected{
            pageNumber = 1
          
            //if isSearch == false{
//                self.usersArray.removeAll()
//                tblConnections.reloadData()
                //pageNumber = 1
                //output.fetchMyConnections()
                
            //}
            //if !global_keywords.isEmpty{
                //pageNumber = 1
                //usersArray.removeAll()
                //searchUsers()
//            }else{
//                if isSearch == false{
//                    loadSuggestedContatcs()
//                }
            //}
            //print("Called with page Number C--->",pageNumber)

            loadMyConnectionView()
        }
        else{
            isUserSelected = false
        }
    }
    
    func loadMyConnectionView(){
        if !isSearch{
            if (pageNumber == 1) {
                output.fetchMyConnections()
                usersArray.removeAll()
                collectionViewUsers.reloadData()
            }
            if isAllUsers{
                isMoreDataAvailable = false
                output.fetchUsersList(pageNumber: pageNumber)
            }else{
                isMoreDataAvailable = false
                btnShowAllConnections.isSelected = false
                output.fetchSuggestedContacts(pageNumber: pageNumber)
            }
        } else {
            if (pageNumber == 1) {
                searchBarClickedSetView()
                usersArray.removeAll()
                records.removeAll()
                tblConnections.reloadData()
                collectionViewUsers.reloadData()
            }
            
            searchUsers()
        }
    }
    
    func myConnectionsGlobalSearch(searchKeyword : String){
        pageNumber = 1
        isSearch = true
        global_keywords = searchKeyword
    //    searchBarClickedSetView()
    }
    
    func myConnectionsCleanGlobalSearch(){
        pageNumber = 1
        isUserSelected = false
        isSearch = false
        global_keywords = ""
        local_keywords = ""
        setUpAfterSearch()
        
        self.view.endEditing(true)
        isAllUsers = false
        searchBarConnections.showsCancelButton = false
        records.removeAll()
        collectionViewUsers.reloadData()
        tblConnections.reloadData()
    }
    
    @objc func handleRefresh(_ refreshControl : UIRefreshControl){
        myConnectionRefreshView()
        refreshControl.endRefreshing()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            self.scrollViewParent.contentOffset = CGPoint.zero
        }
    }
    
    func myConnectionRefreshView(){
        setUpAfterSearch()
        tblConnections.reloadData()
        //output.fetchMyConnections()
        //print("Called with page Number D--->",pageNumber)

        loadMyConnectionView()
    }
    
    
//    func loadSuggestedContatcs(){
//        output.fetchSuggestedContacts(pageNumber: pageNumber)
//    }
    
    @IBAction func showConnectionsClicked(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        setUpAfterSearch()
        if sender.isSelected{
            isAllUsers = true
            //output.fetchUsersList(pageNumber: pageNumber)
        }else{
            isAllUsers = false
            //loadSuggestedContatcs()
        }
        //print("Called with page Number E--->",pageNumber)

        loadMyConnectionView()
    }
    
    func setUpAfterSearch(){
        searchBarConnections.text = ""
        local_keywords = ""
        pageNumber = 1
        isSearch = false
        if (!global_keywords.isEmpty){
            isSearch = true
        }
        isMoreDataAvailable = true
        lblAllConnections.isHidden = false
        btnShowAllConnections.isHidden = false
        lblSuggestedContacts.isHidden = false
        constraintTopViewHeight.constant = 100
        constraintMidViewHeight.constant = 60
        tblConnectionsHeight.constant = CGFloat(records.count * CONNECTION_CELL_HEIGHT)
        self.usersArray.removeAll()
    }
    
    func displayResult(response : [Connections.FetchConnections.UserRecord]?,message:String?, typeOfResult : Int){
        DispatchQueue.main.async {
            if response != nil{
                switch typeOfResult{
                case 1:
                    self.records = response!
                    self.tblConnections.reloadData()
                case 2:
                    
                    if response?.count == NUMBER_OF_RECORDS{
                        self.isMoreDataAvailable = true
                    }else{
                        self.isMoreDataAvailable = false
                    }
                    let tempSuggestedContactsArray = response!.filter{$0.friendStatus == 0}
                    self.usersArray.append(contentsOf: tempSuggestedContactsArray)
                    if tempSuggestedContactsArray.count > 0{
                        self.collectionViewUsers.reloadData()
                    }
                    
                case 3:
                    
                    if response?.count == 30{
                        self.isMoreDataAvailable = true
                    }else{
                        self.isMoreDataAvailable = false
                    }
                    print("Is more data available --->",self.isMoreDataAvailable)
                    let tempArray = response!.filter{$0.friendStatus == 0}
                    self.usersArray.append(contentsOf: tempArray)
                    if tempArray.count > 0{
                        self.collectionViewUsers.reloadData()
                    }
                    
                default:
                    print("Test")
                }
                
            }
            else{
                self.displayToast(message!)
            }
        }
    }
    
    
    
}

extension MyConnectionsViewController {
    fileprivate func prepareTabItem() {
        tabItem.title = "My Connections"
    }
    
    func searchBarClickedSetView(){
        pageNumber = 1
        isSearch = true
        self.usersArray.removeAll()
        btnShowAllConnections.isSelected = false
        lblAllConnections.isHidden = true
        btnShowAllConnections.isHidden = true
        lblSuggestedContacts.isHidden = true
        tblConnectionsHeight.constant = 0
        constraintMidViewHeight.constant = 0
        constraintTopViewHeight.constant = 30
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar){
        searchBarClickedSetView()
        local_keywords = searchBarConnections.text ?? ""
        //searchUsers()
        //print("Called with page Number F--->",pageNumber)

        loadMyConnectionView()
    }
    
    
    
    func searchUsers(){
        searchBarConnections.resignFirstResponder()
        self.view.endEditing(true)
        let worker = HomeWorker()
        var request = SearchResult.FetchPosts.Request()
        request.page = pageNumber
        request.records = NUMBER_OF_RECORDS
        request.type = 1
        var keywords = local_keywords
        if !global_keywords.isEmpty{
            if (!keywords.isEmpty){
                keywords = keywords + "," + global_keywords
            } else {
                keywords = global_keywords
            }
        }

        request.key = keywords
        if !keywords.isEmpty{
            let totalKeywords = keywords.components(separatedBy:",")
            if totalKeywords.count > 1{
                request.flag = 1
            }else{
                request.flag = 0
            }
        }
        worker.fetchPosts(request: request) { (response,totalCount, profileModel) in
            if response != nil{
                if response?.count == 0 && self.pageNumber == 1{
                    self.displayToast("No connections found")
                    return
                }
                let records = response as! [Connections.FetchConnections.UserRecord]
                // let tempArray = records.filter{$0.friendStatus == 0}
                self.usersArray.append(contentsOf: records)
                if records.count == NUMBER_OF_RECORDS{
                    self.isMoreDataAvailable = true
                }else{
                    self.isMoreDataAvailable = false
                }
                self.collectionViewUsers.reloadData()
            }
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        setUpAfterSearch()
        searchBarConnections.resignFirstResponder()
        self.view.endEditing(true)
        isAllUsers = false
        searchBar.showsCancelButton = false
        //loadSuggestedContatcs()
        //local_keywords = ""
        //print("Called with page Number A--->",pageNumber)

        loadMyConnectionView()
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = true
    }
}

extension MyConnectionsViewController : UITableViewDataSource , UITableViewDelegate , UICollectionViewDataSource , UICollectionViewDelegate,SendFriendId{
    
    //MARK:- TableView DataSource & Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        tblConnectionsHeight.constant = CGFloat(records.count * CONNECTION_CELL_HEIGHT)
        scrollViewParent.contentSize.height =  constraintTopViewHeight.constant
            + constraintMidViewHeight.constant + tblConnectionsHeight.constant + collectionViewUsersHeight.constant
        return records.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let myConnectionCell = tableView.dequeueReusableCell(withIdentifier: "myConnectionListCell", for: indexPath) as! MyConnectionTableViewCell
        myConnectionCell.setData(userDetails:records[indexPath.row])
        return myConnectionCell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(CONNECTION_CELL_HEIGHT)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let currentUser = records[indexPath.row]
        let groupDetailsController = storyboard?.instantiateViewController(withIdentifier: "groupDetailViewController") as! GroupDetailViewController
        groupDetailsController.userId = currentUser.id
        isUserSelected = true
        navigationController?.pushViewController(groupDetailsController, animated: true)
    }
    
    //MARK:- CollectionView DataSource & Delegates
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        collectionViewUsersHeight.constant = CGFloat((ceilf(Float(Double(self.usersArray.count) / 2.0)) + 1) * Float(COLLECTION_VIEW_CELL_HEIGHT + 10))
        if isSearch == true{
            scrollViewParent.contentSize.height = tblConnectionsHeight.constant + collectionViewUsersHeight.constant + constraintTopViewHeight.constant - constraintMidViewHeight.constant
        }else{
            scrollViewParent.contentSize.height = tblConnectionsHeight.constant + collectionViewUsersHeight.constant + constraintTopViewHeight.constant + constraintMidViewHeight.constant
        }
        
        print("COunt of users-->",usersArray.count)
        return usersArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let suggestedContactCell = collectionView.dequeueReusableCell(withReuseIdentifier: "connectionCollectionViewCell", for: indexPath) as! ConnectionCollectionViewCell
        
        suggestedContactCell.btnConnect.tag = indexPath.row
        suggestedContactCell.delegate = self
        suggestedContactCell.setData(userDetails:usersArray[indexPath.row])
        return suggestedContactCell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let currentUser = usersArray[indexPath.row]
        let groupDetailsController = storyboard?.instantiateViewController(withIdentifier: "groupDetailViewController") as! GroupDetailViewController
        groupDetailsController.userId = currentUser.id
        isUserSelected = true
        navigationController?.pushViewController(groupDetailsController, animated: true)
    }
    
    func sendFriendId(friendId:Int){
        let currentModel = usersArray[friendId]
        if currentModel.blockStatus == 1{
            showConfirmationOptionToBlock(type:"unblock", completion: { [weak self](index) in
                if index == 0{
                    self?.unblockUser(friendId:currentModel.id!,type: "unblock")
                }
            })
        }else{
            if usersArray.count >= friendId{
                idToBeDeleted = friendId
                print("FRIEND ID IS ---->",friendId)
                output.sendFriendRequest(friendId : currentModel.id!)
            }
        }
    }
    
  
    
   
    func unblockUser(friendId:Int,type:String){
        let worker = HomeWorker()
        worker.blockUser(idOfUser: friendId,type:type) {[weak self] (response) in
            if response.status == true{
                DispatchQueue.main.async{
                    self?.loadMyConnectionView()
                }
            }
            self?.displayToast(response.message!)
        }
    }
    
    func displayFriendRequestResponse(response:CommonModel.BaseModel){
        displayToast(response.message!)
        if response.status == true{
            DispatchQueue.main.async{ [weak self] in
                self?.usersArray.remove(at: (self?.idToBeDeleted)!)
                self?.collectionViewUsers.deleteItems(at: [IndexPath(item: (self?.idToBeDeleted)!, section: 0)])
            }
            
        }
       
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let isReachingEnd = scrollView.contentOffset.y >= 0
            && scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.frame.size.height)
        if isReachingEnd == true && isMoreDataAvailable == true{
            isMoreDataAvailable = false
            pageNumber = pageNumber + 1
//            if isSearch{
//                searchUsers()
//            }else{
//                if isAllUsers{
//                    print("API CaLLed")
//                    output.fetchUsersList(pageNumber: pageNumber)
//                }else{
//                    loadSuggestedContatcs()
//                }
//            }
            //print("Called with page Number B--->",pageNumber)
            loadMyConnectionView()
        }
        
    }
}

