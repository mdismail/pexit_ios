//
//  AppTabsController.swift
//  PEXit
//
//  Created by Apple on 19/09/18.
//  Copyright © 2018 Apple. All rights reserved.
//

//import Foundation
import UIKit
import Material

class AppTabsController: TabsController {
    fileprivate var buttons = [TabItem]()
    open override func prepare() {
        super.prepare()
        tabBarAlignment = .top
        tabBar.lineAlignment = .bottom
        tabBar.setLineColor(Color.grey.base, for: .selected)
        tabBar.setTabItemsColor(Color.grey.base, for: .normal)
        tabBar.setTabItemsColor(Color.black, for: .selected)
        tabBar.setTabItemsColor(Color.black, for: .highlighted)

    }
}



