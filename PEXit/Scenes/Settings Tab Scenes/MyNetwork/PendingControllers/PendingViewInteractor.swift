//
//  PendingViewInteractor.swift
//  PEXit
//
//  Created by Apple on 20/09/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation
protocol PendingViewInteractorInput {
    func fetchPendingContacts()
}

protocol PendingViewInteractorOutput{
    func displayPendingContacts(response : Connections.FetchConnections.Response)
    
}

class PendingViewInteractor :  PendingViewInteractorInput{
    var interactorOutput : PendingViewInteractorOutput!
    var networkWorker = MyNetworkWorker()
    func fetchPendingContacts() {
        networkWorker.fetchPendingListFromServer { (message, response) in
            if response != nil{
                self.interactorOutput.displayPendingContacts(response: response!)
            }
        }
            
    }
            
}
