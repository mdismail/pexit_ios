//
//  PendingViewPresenter.swift
//  PEXit
//
//  Created by Apple on 20/09/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation

protocol PendingViewPresenterInput {
   func displayPendingContacts(response : Connections.FetchConnections.Response)
}
    
protocol PendingViewPresenterOutput : class{
    func displayPendingContacts(response :[Connections.FetchConnections.UserRecord]?,message:String?)
    
}

class PendingViewPresenter :  PendingViewPresenterInput{
    weak var presenterOutput : PendingViewPresenterOutput!
    
    func displayPendingContacts(response : Connections.FetchConnections.Response){
        if response.datas != nil{
            presenterOutput.displayPendingContacts(response: response.datas, message: "")
        }else{
            presenterOutput.displayPendingContacts(response: nil, message: response.message)
        }
    }
}
