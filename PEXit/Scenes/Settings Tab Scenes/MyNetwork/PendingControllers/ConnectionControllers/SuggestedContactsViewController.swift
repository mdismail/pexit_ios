//
//  SuggestedContactsViewController.swift
//  PEXit
//
//  Created by Apple on 21/09/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class SuggestedContactsViewController: UIViewController , StackContainable , UICollectionViewDataSource{
    @IBOutlet weak var collectionViewContacts: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionViewContacts.register(UINib(nibName: "HomeDetailCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "homeDetailsCell")
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionViewContacts.dequeueReusableCell(withReuseIdentifier: "homeDetailsCell", for: indexPath)
        return cell
    }
    
    public func preferredAppearanceInStack() -> ScrollingStackController.ItemAppearance {
        let _ = self.view // force load of the view
        return .scroll(self.collectionViewContacts!, insets: UIEdgeInsetsMake(50, 0, 50, 0))
    }

}
