//
//  ConnectionsListViewController.swift
//  PEXit
//
//  Created by Apple on 21/09/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class ConnectionsListViewController: UIViewController , StackContainable , UITableViewDataSource , UITableViewDelegate{
    @IBOutlet weak var tblConnectionList: UITableView!
    var count = 0
    override func viewDidLoad() {
        super.viewDidLoad()

        tblConnectionList.register(UINib(nibName: "PendingTableViewCell", bundle: nil), forCellReuseIdentifier: "pendingTableViewCell")
    }
    


    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return count + 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let pendingCell = tableView.dequeueReusableCell(withIdentifier: "pendingTableViewCell", for: indexPath)
        return pendingCell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath){
        if  (indexPath.row == 3 || indexPath.row == 6 || indexPath.row == 9) && count < 15{
           count = count + 5
           tblConnectionList.reloadData()
           test()
        }
    }
    
    func test() -> ScrollingStackController.ItemAppearance{
        return .scroll(self.tblConnectionList!, insets: UIEdgeInsetsMake(20, 0, 50, 20))
    }
    
    public func preferredAppearanceInStack() -> ScrollingStackController.ItemAppearance {
        let _ = self.view // force load of the view
        return .scroll(self.tblConnectionList!, insets: UIEdgeInsetsMake(20, 0, 50, 20))
    }
}
