//
//  AllPexitUserViewController.swift
//  PEXit
//
//  Created by Apple on 21/09/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class AllPexitUserViewController: UIViewController , StackContainable{

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    public func preferredAppearanceInStack() -> ScrollingStackController.ItemAppearance {
        let _ = self.view // force load of the view
        return .view(height:100)
    }

}
