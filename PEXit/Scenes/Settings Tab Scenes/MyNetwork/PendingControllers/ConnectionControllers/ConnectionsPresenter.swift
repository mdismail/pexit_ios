//
//  ConnectionsPresenter.swift
//  PEXit
//
//  Created by Apple on 21/09/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation
protocol ConnectionsViewPresenterInput {
//    func displayMyConnections(response : Connections.FetchConnections.Response)
    func displayResult(response : Connections.FetchConnections.Response , typeOfResult : Int)
    func displayFriendResponse(response : CommonModel.BaseModel? , message:String?)
}

protocol ConnectionsViewPresenterOutput : class{
    func displayResult(response : [Connections.FetchConnections.UserRecord]?,message:String?, typeOfResult : Int)
    func displayFriendRequestResponse(response:CommonModel.BaseModel)
}

class ConnectionsViewPresenter :  ConnectionsViewPresenterInput{
   
    
    weak var presenterOutput : ConnectionsViewPresenterOutput!
    
    func displayResult(response : Connections.FetchConnections.Response , typeOfResult : Int){
        if response.datas != nil{
            presenterOutput.displayResult(response: response.datas, message: "",typeOfResult:typeOfResult)
        }else{
            presenterOutput.displayResult(response: nil, message: response.message,typeOfResult:typeOfResult)
        }
    }
    
    func displayFriendResponse(response : CommonModel.BaseModel? , message:String?){
        if response != nil{
            presenterOutput.displayFriendRequestResponse(response:response!)
        }
    }
    

}
