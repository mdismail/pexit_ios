//
//  ConnectionsInteractor.swift
//  PEXit
//
//  Created by Apple on 21/09/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation

protocol ConnectionsViewInteractorInput {
    func fetchMyConnections()
    func fetchSuggestedContacts(pageNumber : Int?)
    func fetchUsersList(pageNumber : Int?)
    func sendFriendRequest(friendId : Int)
}

protocol ConnectionsViewInteractorOutput{
    func displayResult(response : Connections.FetchConnections.Response , typeOfResult : Int)
    func displayFriendResponse(response : CommonModel.BaseModel? , message:String?)
    
}

class ConnectionsViewInteractor :  ConnectionsViewInteractorInput{
    
    
    var interactorOutput : ConnectionsViewInteractorOutput!
    var networkWorker = MyNetworkWorker()
    func fetchMyConnections() {
        self.networkWorker.fetchMyConnectionsListFromServer { (message, response) in
            if response != nil{
                self.interactorOutput.displayResult(response: response!,typeOfResult: 1)
                }
                
            }
        }
        
    func fetchUsersList(pageNumber : Int?){
        self.networkWorker.fetchUsersListFromServer(pageNumber: pageNumber) { (message, response) in
            if response != nil{
                self.interactorOutput.displayResult(response: response!,typeOfResult: 3)
            }
        }
    }
    
    func fetchSuggestedContacts(pageNumber : Int?){
        self.networkWorker.fetchSuggestedContactsFromServer(pageNumber: pageNumber) { (message, response) in
            if response != nil{
                self.interactorOutput.displayResult(response: response!,typeOfResult: 2)
            }
        }
    }

    func sendFriendRequest(friendId : Int){
        self.networkWorker.sendFriendRequest(userId: friendId) { (response) in
            if response != nil{
                self.interactorOutput.displayFriendResponse(response: response!, message: "")
            }else{
                self.interactorOutput.displayFriendResponse(response: nil, message: UNKNOWN_ERROR_MSG)
            }
        }
    }
    
}
