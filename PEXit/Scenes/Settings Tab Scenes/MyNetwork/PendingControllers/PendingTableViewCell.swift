//
//  PendingTableViewCell.swift
//  PEXit
//
//  Created by Apple on 20/09/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class PendingTableViewCell: UITableViewCell {
    @IBOutlet weak var imgViewUser: UIImageView!
    @IBOutlet weak var lblUsername: UILabel!
    @IBOutlet weak var lblUserAddress: UILabel!
    @IBOutlet weak var btnAccept: UIButton!
    @IBOutlet weak var btnReject: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imgViewUser.makeImageRounded()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(userDetails : Connections.FetchConnections.UserRecord){
        lblUsername.text = userDetails.username
        if let profileImage = userDetails.pimage{
            if let imageUrl = URL(string: profileImage){
                imgViewUser.setImage(from:imageUrl)
            }
        }
        let state: String? = userDetails.userstate
        let city: String? = userDetails.location
        let country: String? = userDetails.country
        
        lblUserAddress.text = ([city,state,country].filter{$0 != ""}).compactMap{$0}.joined(separator: ",")
       
    }
    
    @IBAction func btnRejectClicked(_ sender: Any) {
    }
    
    @IBAction func btnAcceptClicked(_ sender: Any) {
    }
}
