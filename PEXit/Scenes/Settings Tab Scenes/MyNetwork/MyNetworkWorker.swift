//
//  MyNetworkWorker.swift
//  PEXit
//
//  Created by Apple on 20/09/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation
import ObjectMapper

class MyNetworkWorker{
    func fetchPendingListFromServer(completionHandler: @escaping(_ message : String? ,_ pendingConnections : Connections.FetchConnections.Response?) -> Void){
        START_LOADING_VIEW()
        ServiceManager.methodType(requestType: GET_REQUEST, url: GET_PENDING_CONNECTIONS, params: nil, paramsData: nil, completion: { (response,_ responseData, statusCode) in
            STOP_LOADING_VIEW()
            if response != nil{
                let decoder = JSONDecoder()
                do{
                    let responseConnections = try decoder.decode(Connections.FetchConnections.Response.self, from: responseData!)
                    completionHandler("", responseConnections)
                }catch{
                    
                }
            }
            
        }) { (response, statusCode) in
            STOP_LOADING_VIEW()
        }
    }
    
    func fetchMyConnectionsListFromServer(completionHandler: @escaping(_ message : String? ,_ pendingConnections : Connections.FetchConnections.Response?) -> Void){
        START_LOADING_VIEW()
        ServiceManager.methodType(requestType: GET_REQUEST, url: GET_MY_CONNECTIONS, params: nil, paramsData: nil, completion: { (response,_ responseData, statusCode) in
            STOP_LOADING_VIEW()
            if response != nil{
                let decoder = JSONDecoder()
                do{
                    let responseConnections = try decoder.decode(Connections.FetchConnections.Response.self, from: responseData!)
                    completionHandler("", responseConnections)
                }catch{
                    
                }
            }
            
        }) { (response, statusCode) in
            STOP_LOADING_VIEW()
        }
    }
    
    func fetchSuggestedContactsFromServer(pageNumber:Int?,completionHandler: @escaping(_ message : String? ,_ suggestedConnections : Connections.FetchConnections.Response?) -> Void){
        START_LOADING_VIEW()
        let params = ["records":NUMBER_OF_RECORDS,"page":pageNumber ?? 1] as! [String:Any]
        ServiceManager.methodType(requestType: POST_REQUEST, url: USERS_SUGGESTIONS, params: params as NSDictionary, paramsData: nil, completion: { (response,_ responseData, statusCode) in
            STOP_LOADING_VIEW()
            if response != nil{
                let decoder = JSONDecoder()
                do{
                    let responseConnections = try decoder.decode(Connections.FetchConnections.Response.self, from: responseData!)
                    completionHandler("", responseConnections)
                }catch{
                    
                }
            }
            
        }) { (response, statusCode) in
            STOP_LOADING_VIEW()
        }
    }
    
    func fetchUsersListFromServer(pageNumber:Int?,completionHandler: @escaping(_ message : String? ,_ suggestedConnections : Connections.FetchConnections.Response?) -> Void){
        START_LOADING_VIEW()
        print("Hit goessss for--->",pageNumber)
        let params = ["records":30,"page":pageNumber ?? 1] as! [String:Any]
        ServiceManager.methodType(requestType: POST_REQUEST, url: USERS_LIST, params: params as NSDictionary, paramsData: nil, completion: { (response,_ responseData, statusCode) in
            STOP_LOADING_VIEW()
            if response != nil{
                let decoder = JSONDecoder()
                do{
                    let responseConnections = try decoder.decode(Connections.FetchConnections.Response.self, from: responseData!)
                    completionHandler("", responseConnections)
                }catch{
                    
                }
            }
            
        }) { (response, statusCode) in
            STOP_LOADING_VIEW()
        }
    }
    
    
    func sendFriendRequest(userId:Int?,completionHandler: @escaping(_ friendResponse : CommonModel.BaseModel?) -> Void){
        START_LOADING_VIEW()
        
        ServiceManager.methodType(requestType: POST_REQUEST, url: FRIEND_REQUEST(userId!), params: nil, paramsData: nil, completion: { (response,_ responseData, statusCode) in
            STOP_LOADING_VIEW()
            if response != nil{
                let decoder = JSONDecoder()
                do{
                    let friendResponse = try decoder.decode(CommonModel.BaseModel.self, from: responseData!)
                    completionHandler(friendResponse)
                }catch{
                    
                }
            }
            
        }) { (response, statusCode) in
            STOP_LOADING_VIEW()
        }
    }
    
    // NOTIFICATION APIS
    
    func removeFriendRequest(userId:Int?,completionHandler: @escaping(_ friendResponse : CommonModel.BaseModel?) -> Void){
        START_LOADING_VIEW()
        
        ServiceManager.methodType(requestType: DELETE_REQUEST, url: REMOVE_FRIEND_REQUEST(userId!), params: nil, paramsData: nil, completion: { (response,_ responseData, statusCode) in
            STOP_LOADING_VIEW()
            if response != nil{
                let decoder = JSONDecoder()
                do{
                    let friendResponse = try decoder.decode(CommonModel.BaseModel.self, from: responseData!)
                    completionHandler(friendResponse)
                }catch{
                    
                }
            }
            
        }) { (response, statusCode) in
            STOP_LOADING_VIEW()
        }
    }

    
    func fetchGroupsList(request:GroupsRequestModel,completionHandler: @escaping(_ groupsResponse : GroupsResponseModel?) -> Void){
        START_LOADING_VIEW()
        
        ServiceManager.methodType(requestType: POST_REQUEST, url: GET_GROUP_LIST(request.typeOfSearch ?? ""), params: ["page":request.page!,"records":request.numberOfRecords ?? 10,"key":request.key ?? ""], paramsData: nil, completion: { (response,_ responseData, statusCode) in
            STOP_LOADING_VIEW()
            if response != nil{
                let decoder = JSONDecoder()
                do{
                    let friendResponse = try decoder.decode(GroupsResponseModel.self, from: responseData!)
                    completionHandler(friendResponse)
                }catch{
                    
                }
            }
            
        }) { (response, statusCode) in
            STOP_LOADING_VIEW()
        }
    }
    
    func createGroup(request:CreateGroupRequestModel,isEdit:Bool? = false ,completionHandler: @escaping(_ groupsResponse : NewPost.Response?) -> Void){
        START_LOADING_VIEW()

        var apiUrl : String!
        if isEdit == true{
            apiUrl = EDIT_GROUP(groupId: request.groupId!)
        }else{
            apiUrl = CREATE_GROUP
        }
        ServiceManager.methodType(requestType: POST_REQUEST, url: apiUrl, params: ["gname":request.gname!,"gdescription":request.gdescription!,"gusers":request.gusers!,"gtype":request.gtype!], paramsData: nil, completion: { (response,_ responseData, statusCode) in
            STOP_LOADING_VIEW()
//            print("Response is ---->",response)
            if response != nil{
                let decoder = JSONDecoder()
                do{
                    let createGroupResponse = try decoder.decode(NewPost.Response.self, from: responseData!)
                    completionHandler(createGroupResponse)
                }catch{
                    
                }
            }
            
        }) { (response, statusCode) in
            STOP_LOADING_VIEW()
        }
    }
    
    func joinGroup(groupId:Int?,friendId:Int?,completionHandler: @escaping(_ groupsResponse : NewPost.Response?) -> Void){
        START_LOADING_VIEW()
        
        ServiceManager.methodType(requestType: POST_REQUEST, url: CONNECT_TO_GROUP, params: ["gid":groupId!,"fid":friendId!], paramsData: nil, completion: { (response,_ responseData, statusCode) in
            STOP_LOADING_VIEW()
            if response != nil{
                let decoder = JSONDecoder()
                do{
                    let createGroupResponse = try decoder.decode(NewPost.Response.self, from: responseData!)
                    completionHandler(createGroupResponse)
                }catch{
                    
                }
            }
            
        }) { (response, statusCode) in
            STOP_LOADING_VIEW()
        }
    }
    
    func leaveGroup(groupId:Int?,type:Int?,completionHandler: @escaping(_ groupsResponse : NewPost.Response?) -> Void){
        START_LOADING_VIEW()
        
        ServiceManager.methodType(requestType: POST_REQUEST, url: LEAVE_GROUP, params: ["gid":groupId!,"type":type!], paramsData: nil, completion: { (response,_ responseData, statusCode) in
            STOP_LOADING_VIEW()
            if response != nil{
                let decoder = JSONDecoder()
                do{
                    let createGroupResponse = try decoder.decode(NewPost.Response.self, from: responseData!)
                    completionHandler(createGroupResponse)
                }catch{
                    
                }
            }
            
        }) { (response, statusCode) in
            STOP_LOADING_VIEW()
        }
    }
    
    func getGroupProfileDetails(typeOfDetails:String?,idForDetails:Int?,completionHandler: @escaping(_ groupsResponse : GroupDetailModel?) -> Void){
        START_LOADING_VIEW()
        
        ServiceManager.methodType(requestType: GET_REQUEST, url: GET_GROUP_PROFILE_DETAILS(typeOfDetails!, id: idForDetails!), params: nil, paramsData: nil, completion: { (response,_ responseData, statusCode) in
            STOP_LOADING_VIEW()
            if response != nil{
            if (response as! [String : AnyObject])["status"] as? Bool == true{
                let decoder = JSONDecoder()
                do{
                    print("Response is --->",response)
                    let groupDetails = try decoder.decode(GroupDetailModel.self, from: responseData!)
                    completionHandler(groupDetails)
                }catch{
                    DispatchQueue.main.async{
                        if ((response as! [String : Any])["message"] as? String) != nil{
                            KEY_WINDOW?.makeToast((response as! [String : Any])["message"] as? String)
                        }
                    }
                }
            }else{
                DispatchQueue.main.async{
                    if ((response as! [String : Any])["message"] as? String) != nil{
                        KEY_WINDOW?.makeToast((response as! [String : Any])["message"] as? String)
                    }
                }
                }
            }
            
        }) { (response, statusCode) in
            STOP_LOADING_VIEW()
        }
    }
    
    func getConnectionsOfOtherUser(typeOfConnections:String,idForDetails:Int?,completionHandler:@escaping(_ connectionsResponse : Connections.FetchConnections.Response?) -> Void){
        START_LOADING_VIEW()
        ServiceManager.methodType(requestType: GET_REQUEST, url: GET_OTHER_CONNECTIONS(typeOfConnections, id: idForDetails!), params: nil, paramsData: nil, completion: { (response,_ responseData, statusCode) in
            STOP_LOADING_VIEW()
            if response != nil{
                if (response as! [String : AnyObject])["status"] as? Bool == true{
                    let decoder = JSONDecoder()
                    do{
                        let connectionsResponse = try decoder.decode(Connections.FetchConnections.Response.self, from: responseData!)
                        completionHandler(connectionsResponse)
                    }catch{
                        
                    }
                }
            }
            
        }) { (response, statusCode) in
            STOP_LOADING_VIEW()
        }
    }
    
    func fetchBlockedUsersList(completionHandler: @escaping(_ usersList : Connections.FetchConnections.Response?) -> Void){
        START_LOADING_VIEW()
        ServiceManager.methodType(requestType: GET_REQUEST, url: BLOCKED_USERS_LIST, params: nil, paramsData: nil, completion: { (response,_ responseData, statusCode) in
            STOP_LOADING_VIEW()
            if response != nil{
                let decoder = JSONDecoder()
                do{
                    let responseConnections = try decoder.decode(Connections.FetchConnections.Response.self, from: responseData!)
                    completionHandler(responseConnections)
                }catch{
                    
                }
            }
            
        }) { (response, statusCode) in
            STOP_LOADING_VIEW()
        }
    }
    
}


