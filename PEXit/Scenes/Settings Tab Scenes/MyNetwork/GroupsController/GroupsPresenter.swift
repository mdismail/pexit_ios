//
//  GroupsPresenter.swift
//  PEXit
//
//  Created by Apple on 26/09/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation
protocol GroupsViewPresenterInput {
    func displayResult(response : GroupsResponseModel)
    func displayCreateGroupResponse(response : NewPost.Response , type : Int)
}

protocol GroupsViewPresenterOutput : class{
    func displayResult(response : [GroupListModel]?,message:String?)
    func displayCreateGroupResponse(response : NewPost.Response , type : Int)
    
}

class GroupsViewPresenter :  GroupsViewPresenterInput{
    weak var presenterOutput : GroupsViewPresenterOutput!
    
    func displayResult(response : GroupsResponseModel){
        if response.datas != nil{
            presenterOutput.displayResult(response: response.datas, message: response.message ?? UNKNOWN_ERROR_MSG)
        }else{
            presenterOutput.displayResult(response: nil, message: response.message ?? UNKNOWN_ERROR_MSG)
        }
    }
    
    func displayCreateGroupResponse(response : NewPost.Response, type : Int){
        presenterOutput.displayCreateGroupResponse(response: response , type:type)
    }
}
