//
//  GroupRouter.swift
//  PEXit
//
//  Created by Apple on 27/09/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation
import UIKit

class GroupRouter : NSObject,UIPopoverPresentationControllerDelegate{
    weak var controller : GroupsViewController!
    func navigateToPopUpContoller(currentArray:[String]?=nil,userArray:[ListOfIndividuals.IndividualUserModel]? = nil,sender: AnyObject , popUpDirection : UIPopoverArrowDirection? = .any) {
        let popUpController = controller.storyboard?.instantiateViewController(withIdentifier: "commonViewController") as! CommonViewController
        popUpController.modalPresentationStyle = .popover
        DispatchQueue.main.async { [weak self] in
            
            
            if userArray != nil{
                if #available(iOS 12.0, *) {
                    popUpController.preferredContentSize = CGSize(width : sender.frame.size.width , height : CGFloat((userArray?.count)! * COMMON_CONTROLLER_ROW_HEIGHT))
                } else {
                    // Fallback on earlier versions
                }
                popUpController.previousSelectedUsers = (self?.controller.selectedUserArray)!
                popUpController.individualUsersArray = userArray
            }else{
                if #available(iOS 12.0, *) {
                    popUpController.preferredContentSize = CGSize(width : sender.frame.size.width , height : CGFloat((currentArray?.count)! * COMMON_CONTROLLER_ROW_HEIGHT))
                } else {
                    // Fallback on earlier versions
                }
                popUpController.itemsArray = currentArray
            }
            
            let popOver = popUpController.popoverPresentationController
            popOver?.delegate = self
            popOver?.permittedArrowDirections = popUpDirection!
            popOver?.sourceView = sender as! UIView
            popOver?.sourceRect = sender.bounds
            popUpController.delegate = self?.controller
            
            self?.controller.present(popUpController, animated: true, completion: nil)
        }
        
    }
    
    func pushToGroupDetails(groupListModel : GroupListModel){
        let groupDetailsController = controller.storyboard?.instantiateViewController(withIdentifier: "groupDetailViewController") as! GroupDetailViewController
        groupDetailsController.currentGroupModel = groupListModel
        controller.navigationController?.pushViewController(groupDetailsController, animated: true)
    
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return .none
    }
    
    
}
