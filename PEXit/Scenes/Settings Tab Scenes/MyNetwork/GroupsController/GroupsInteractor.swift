//
//  GroupsInteractor.swift
//  PEXit
//
//  Created by Apple on 26/09/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation



protocol GroupsViewInteractorInput {
    func fetchGroups(request : GroupsRequestModel)
    func createGroup(request : CreateGroupRequestModel ,  isEdit : Bool)
    func joinGroup(groupModel:GroupListModel)
    func leaveGroup(groupModel:GroupListModel, type : Int)
}

protocol GroupsViewInteractorOutput{
    func displayResult(response : GroupsResponseModel)
    func displayCreateGroupResponse(response : NewPost.Response , type : Int)
    
}

class GroupsViewInteractor :  GroupsViewInteractorInput{
    
    
    var interactorOutput : GroupsViewInteractorOutput!
    var networkWorker = MyNetworkWorker()
    func fetchGroups(request : GroupsRequestModel) {
        self.networkWorker.fetchGroupsList(request: request) { (response) in
            if response != nil{
                self.interactorOutput.displayResult(response: response!)
            }
        }
    }
    
    func createGroup(request : CreateGroupRequestModel ,  isEdit : Bool){
        self.networkWorker.createGroup(request: request , isEdit: isEdit) { (response) in
            if response != nil{
                self.interactorOutput.displayCreateGroupResponse(response: response!, type: TYPE_OF_GROUP_ACTIONS.CreateGroup.rawValue)
            }
        }
    }
    
    func joinGroup(groupModel:GroupListModel){
        self.networkWorker.joinGroup(groupId: groupModel.gid, friendId: groupModel.uid) { (response) in
            if response != nil{
                self.interactorOutput.displayCreateGroupResponse(response: response!,type: TYPE_OF_GROUP_ACTIONS.JoinGroup.rawValue)
            }
        }
    }
    
    func leaveGroup(groupModel:GroupListModel , type : Int){
        self.networkWorker.leaveGroup(groupId: groupModel.gid, type: type) { (response) in
            if response != nil{
                self.interactorOutput.displayCreateGroupResponse(response: response!,type: TYPE_OF_GROUP_ACTIONS.LeaveGroup.rawValue)
            }
        }
    }
}
