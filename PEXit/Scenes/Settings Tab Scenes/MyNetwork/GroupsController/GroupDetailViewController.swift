//
//  GroupDetailViewController.swift
//  PEXit
//
//  Created by Apple on 27/09/18.
//  Copyright © 2018 Apple. All rights reserved.
//


import UIKit
import Presentr
import TTTAttributedLabel

enum TYPE_OF_DETAILS : String{
    case GROUP = "group"
    case PROFILE = "profile"
}

class GroupDetailViewController: UIViewController , UserSelected{
    
    @IBOutlet weak var imgViewGroup: UIImageView!
    @IBOutlet weak var lblGroupName: UILabel!
    @IBOutlet weak var lblConnectionsCount: UILabel!
    @IBOutlet weak var lblGroupSummary: UILabel!
    @IBOutlet weak var lblGroupCreatedBy: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblContactNumber: UILabel!
    @IBOutlet weak var lblProfileSummary: UILabel!
    @IBOutlet weak var lblPosition: UILabel!
    @IBOutlet weak var tblUserDetails: UITableView!
    // @IBOutlet weak var tblUserDetailsHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var btnAddRemoveContact: UIButton!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblStaticText: UILabel!
    
    @IBOutlet weak var trailingConstraintGroupName: NSLayoutConstraint!
    var arrStatus:NSMutableArray = []
    let sectionHeaders =  ["Experience","Education","Honors & Rewards"]
    let orgSectionHeaders = ["Company","Honors & Rewards"]
    var currentGroupModel : GroupListModel?
    var educationModelArray = [EducationModel]()
    var experienceModelArray = [ExperienceModel]()
    var companyModelArray = [CompanyModel]()
    var honorsModelArray = [HonorsModel]()
    var userId : Int?
    var currentDetailsType : TYPE_OF_DETAILS?
    var detailsModel : GroupDetailListModel?
    override func viewDidLoad() {
        super.viewDidLoad()
        clearLabels()
        for _ in 0..<sectionHeaders.count
        {
            self.arrStatus.add("1")
        }
        // self.tblUserDetailsHeightConstraint.constant = (CGFloat(HEADER_HEIGHT) * 3)
        tblUserDetails.register(UINib(nibName: "WorkEducationTableViewCell", bundle: nil), forCellReuseIdentifier: "workEducationCell")
        let headerNib = UINib.init(nibName: "ProfileHeaderView", bundle: Bundle.main)
        tblUserDetails.register(headerNib, forHeaderFooterViewReuseIdentifier: "ProfileHeaderView")
        
        if userId != nil{
            
            trailingConstraintGroupName.constant = 116
            btnAddRemoveContact.isHidden = false
            if userId == Defaults.getUserID(){
                btnAddRemoveContact.isHidden = true
            }else{
                addBlockUserOption()
            }
            currentDetailsType = TYPE_OF_DETAILS.PROFILE
            addBackButton(withTitle: "Profile Details")
            fetchDetails(id:userId!,type:TYPE_OF_DETAILS.PROFILE.rawValue)
        }else{
            trailingConstraintGroupName.constant = 10
            btnAddRemoveContact.isHidden = true
            currentDetailsType = TYPE_OF_DETAILS.GROUP
            addBackButton(withTitle: "Group Details")
            fetchDetails(id:(currentGroupModel?.gid!)!,type:TYPE_OF_DETAILS.GROUP.rawValue)
        }
        imgViewGroup.makeImageRounded()
        
    }
    
    override func showPopOver(){
        if let popUpController = self.storyboard?.instantiateViewController(withIdentifier: "commonViewController") as? CommonViewController {
            popUpController.delegate = self
            if self.detailsModel?.blockStatus == 0{
                popUpController.itemsArray = ["Block User"]
            }else{
                popUpController.itemsArray = ["Unblock User"]
            }
            popUpController.modalPresentationStyle = .popover
            popUpController.preferredContentSize = CGSize(width : self.view.frame.size.width / 1.5 , height : CGFloat(60))
            if let pctrl = popUpController.popoverPresentationController {
                pctrl.barButtonItem = navigationItem.rightBarButtonItem
                pctrl.delegate = self
                self.present(popUpController, animated: true, completion: nil)
            }
        }
    }
    
    
    
    func clearLabels(){
        
        if userId != nil{
            lblGroupSummary.isHidden = true
            lblProfileSummary.text = ""
        }else{
            lblProfileSummary.isHidden = true
            lblGroupSummary.text = ""
        }
        lblGroupName.text = ""
        lblConnectionsCount.text = ""
        
        lblGroupCreatedBy.text = ""
        lblEmail.text = ""
        lblContactNumber.text = ""
        lblPosition.text = ""
        lblStaticText.text = ""
        self.view.layoutIfNeeded()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        showNavigationBar()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        guard let headerView = tblUserDetails.tableHeaderView else {
            return
        }
        let size = headerView.systemLayoutSizeFitting(UILayoutFittingCompressedSize)
        
        if headerView.frame.size.height != size.height {
            headerView.frame.size.height = size.height
            
            tblUserDetails.tableHeaderView = headerView
            tblUserDetails.tableFooterView = UIView()
            tblUserDetails.layoutIfNeeded()
        }
    }
    
    
    func fetchDetails(id:Int,type:String){
        let networkWorker = MyNetworkWorker()
        networkWorker.getGroupProfileDetails(typeOfDetails: type, idForDetails:id) { [weak self](response) in
            
            DispatchQueue.main.async{
                if response != nil{
                    if response?.status == true{
                        self?.detailsModel = (response?.datas)!
                        self?.setTitleBtnAddRemoveFriend()
                        self?.setData(responseData: (response?.datas)!)
                        self?.createModels(responseData: (response?.datas)!)
                    }else{
                        self?.displayToast(response?.message ?? UNKNOWN_ERROR_MSG)
                    }
                }else{
                    self?.displayToast(UNKNOWN_ERROR_MSG)
                }
            }
            
        }
    }
    
    
    func setTitleBtnAddRemoveFriend(){
        if userId != nil{
            switch detailsModel?.friendStatus{
            case 0:
                if detailsModel?.blockStatus == 1{
                    btnAddRemoveContact.setTitle("Unblock", for: .normal)
                }else{
                    btnAddRemoveContact.setTitle("Connect", for: .normal)
                }
            case 1:
                btnAddRemoveContact.setTitle("Remove Friend", for: .normal)
            default:
                btnAddRemoveContact.setTitle("Sent Invite", for: .normal)
            }
        }
    }
    
    @IBAction func btnAddRemoveFriendClicked(_ sender: UIButton) {
        if userId != nil{
            if detailsModel?.blockStatus
                == 1{
                showConfirmationOptionToBlock(type:"unblock", completion: { [weak self](index) in
                    if index == 0{
                        self?.blockUser(type: "unblock")
                    }
                })
            }else{
                switch detailsModel?.friendStatus{
                case 0:
                    sendFriendRequest()
                case 1:
                    removeFriend()
                default:
                    break
                }
            }
           
        }
    }
    @IBAction func fetchConnections(_ sender: Any) {
        
        let networkWorker = MyNetworkWorker()
        let idForConnections = userId != nil ? userId : currentGroupModel?.gid
        networkWorker.getConnectionsOfOtherUser(typeOfConnections: (currentDetailsType?.rawValue)!, idForDetails: idForConnections) { (response) in
            if response != nil && response?.status == true{
                DispatchQueue.main.async {
                    let presenter = Presentr(presentationType: .popup)
                    let controller = self.storyboard?.instantiateViewController(withIdentifier: "connectionsViewController") as! ConnectionsViewController
                    //controller.setUpView(connectionResp: response)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                        controller.connectionResponse = response
                        controller.delegate = self
                        self.customPresentViewController(presenter, viewController: controller, animated: true)
                    })
                    
                }
            }
            
        }
    }
    
    
    
    func userSelected(user:Connections.FetchConnections.UserRecord){
        let groupDetailsController = storyboard?.instantiateViewController(withIdentifier: "groupDetailViewController") as! GroupDetailViewController
        groupDetailsController.userId = user.id
        navigationController?.pushViewController(groupDetailsController, animated: true)
    }
    
    func setData(responseData : GroupDetailListModel){
        if currentGroupModel != nil{
            lblGroupName.text = responseData.gname
            lblConnectionsCount.text = "\(responseData.groupFriendsCount!)" + " Connections"
            if responseData.gdescription != nil && !(responseData.gdescription?.isEmpty)!{
                if let groupSummary = responseData.gdescription?.getHtmlAttributedStr(){
                    lblGroupSummary.attributedText = groupSummary
                }
            }else{
                lblGroupSummary.text = "No summary added"
            }
            lblStaticText.text = "Created By"
            lblGroupCreatedBy.text = responseData.createdBy?.name
            lblPosition.text = responseData.createdBy?.title
            if let profileImage = responseData.gimage{
                if let imageUrl = URL(string: profileImage){
                    imgViewGroup.setImage(from:imageUrl)
                }
            }
            
        }else{
            lblGroupSummary.isHidden = true
            lblGroupName.text = responseData.createdBy?.name
            lblConnectionsCount.text = "\(responseData.friendsCount!)" + " Connections"
            lblStaticText.text = "Basic Information"
            lblPosition.text = responseData.createdBy?.address
            lblGroupCreatedBy.text = responseData.createdBy?.title
            if let profileImage = responseData.profileImage{
                if let imageUrl = URL(string: profileImage){
                    imgViewGroup.setImage(from:imageUrl)
                }
            }
            topConstraint.constant = -16
        }
        
        lblEmail.text = responseData.createdBy?.email
        lblContactNumber.text = responseData.createdBy?.phone
        if responseData.summary != nil && !(responseData.summary?.isEmpty)!{
            if let profileSummary = responseData.summary?.getHtmlAttributedStr(){
                lblProfileSummary.attributedText = profileSummary
            }
        }else{
            lblProfileSummary.text = "No summary added"
        }
        
        
        
        
        //        lblProfileSummary.attributedText = try? NSAttributedString(htmlString: responseData.summary ?? "")
        //        lblProfileSummary.text = (responseData.summary?.stringByStrippingHTMLTags)?.stringByDecodingHTMLEntities ?? "No Summary Added"
        
        
    }
    
    func createModels(responseData : GroupDetailListModel){
        if responseData.education != nil{
            for groupEducationModel in responseData.education!{
                var educationModel = EducationModel()
                educationModel = educationModel.initFromGroupModel(groupEducationModel: groupEducationModel)
                educationModelArray.append(educationModel)
            }
        }
        
        if responseData.experience != nil{
            for groupExperiencModel in responseData.experience!{
                var experienceModel = ExperienceModel()
                experienceModel = experienceModel.initFromGroupModel(groupExperienceModel: groupExperiencModel)
                experienceModelArray.append(experienceModel)
            }
        }
        if responseData.honors != nil{
            for groupHonorsModel in responseData.honors!{
                var honorModel = HonorsModel()
                honorModel = honorModel.initFromGroupModel(groupHonorModel: groupHonorsModel)
                honorsModelArray.append(honorModel)
            }
        }
        if responseData.company != nil{
            //for groupCompany in responseData.company!{
            var companyModel = CompanyModel()
            companyModel = companyModel.initFromCompanyModel(groupCompanyModel: responseData.company!)
            companyModelArray.append(companyModel)
            // }
        }
        
        self.tblUserDetails.reloadData()
    }
    
}

extension GroupDetailViewController : UITableViewDataSource , UITableViewDelegate {
    
    
    @objc func headerCellButtonTapped(_sender: UIButton)
    {
        let str:String = arrStatus[_sender.tag] as! String
        
        
        //        if str == "0"
        //        {
        //            arrStatus[_sender.tag] = "1"
        //            switch _sender.tag{
        //            case 0:
        //                self.tblUserDetailsHeightConstraint.constant = self.tblUserDetailsHeightConstraint.constant + CGFloat(experienceModelArray.count * CELL_HEIGHT)
        //            case 1:
        //                self.tblUserDetailsHeightConstraint.constant = self.tblUserDetailsHeightConstraint.constant + CGFloat(educationModelArray.count * CELL_HEIGHT)
        //            case 2:
        //                self.tblUserDetailsHeightConstraint.constant = self.tblUserDetailsHeightConstraint.constant + CGFloat(honorsModelArray.count * CELL_HEIGHT)
        //            default:
        //                self.tblUserDetailsHeightConstraint.constant = (CGFloat(HEADER_HEIGHT) * 3)
        //            }
        //        }
        //        else
        //        {
        //            arrStatus[_sender.tag] = "0"
        //            switch _sender.tag{
        //            case 0:
        //                self.tblUserDetailsHeightConstraint.constant = self.tblUserDetailsHeightConstraint.constant - CGFloat(experienceModelArray.count * CELL_HEIGHT)
        //            case 1:
        //                self.tblUserDetailsHeightConstraint.constant = self.tblUserDetailsHeightConstraint.constant - CGFloat(educationModelArray.count * CELL_HEIGHT)
        //            case 2:
        //                self.tblUserDetailsHeightConstraint.constant = self.tblUserDetailsHeightConstraint.constant - CGFloat(honorsModelArray.count * CELL_HEIGHT)
        //            default:
        //                self.tblUserDetailsHeightConstraint.constant = (CGFloat(HEADER_HEIGHT) * 3)
        //            }
        //        }
        //
        //        tblUserDetails.reloadData()
        //        tblUserDetails.layoutIfNeeded()
        //
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        //       return CGFloat(HEADER_HEIGHT)
        
        
        if self.detailsModel?.company?.pcomcountry != nil{
            switch section{
            case 0 :
                return companyModelArray.count > 0 ? CGFloat(HEADER_HEIGHT) : 0
            case 1 :
                return honorsModelArray.count > 0 ? CGFloat(HEADER_HEIGHT) : 0
                
            default:
                return CGFloat(HEADER_HEIGHT)
            }
        }else{
            switch section{
            case 0:
                return experienceModelArray.count > 0 ? CGFloat(HEADER_HEIGHT) : 0
            case 1:
                return educationModelArray.count > 0 ? CGFloat(HEADER_HEIGHT) : 0
            case 2:
                return honorsModelArray.count > 0 ? CGFloat(HEADER_HEIGHT) : 0
                
            default:
                return CGFloat(HEADER_HEIGHT)
                
            }
        }
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tblUserDetails.dequeueReusableHeaderFooterView(withIdentifier: "ProfileHeaderView") as! ProfileHeaderView
        headerView.backgroundColor = UIColor.white
        headerView.btnAddRecord.isHidden = true
        if self.detailsModel?.company?.pcomcountry != nil{
            headerView.headerTitle.text = orgSectionHeaders[section]
            
        }else{
            headerView.headerTitle.text = sectionHeaders[section]
            
        }
        headerView.btnAddRecord.tag = section
        headerView.btnAddRecord.addTarget(self, action: #selector(JobApplyViewController.headerCellButtonTapped(_sender:)), for: UIControlEvents.touchUpInside)
        let str:String = arrStatus[section] as! String
        if str == "0"
        {
            UIView.animate(withDuration: 2) { () -> Void in
                headerView.btnAddRecord.setImage(UIImage(named : "drop"), for: .normal)
                let angle =  CGFloat(Double.pi * 2)
                let tr = CGAffineTransform.identity.rotated(by: angle)
                headerView.btnAddRecord.imageView?.transform = tr
                //                self.tblUserDetails.imgArrow.transform = tr
            }
        }
        else
        {
            UIView.animate(withDuration: 2) { () -> Void in
                headerView.btnAddRecord.setImage(UIImage(named : "up"), for: .normal)
                let angle =  CGFloat(Double.pi * 2)
                let tr = CGAffineTransform.identity.rotated(by: angle)
                headerView.btnAddRecord.imageView?.transform = tr
            }
        }
        return headerView
    }
    
    func rotateImage(btnToRotate : UIButton){
        let angle =  CGFloat(Double.pi * 2)
        let tr = CGAffineTransform.identity.rotated(by: angle)
        btnToRotate.imageView?.transform = tr
    }
    
    func removeFriend(){
        let notificationworker = MyNetworkWorker()
        if userId != nil{
            notificationworker.removeFriendRequest(userId: userId) {[weak self] (response) in
                if response != nil{
                    if response?.status == true{
                        DispatchQueue.main.async{
                            self?.detailsModel?.friendStatus = 0
                            self?.btnAddRemoveContact.setTitle("Connect", for: .normal)
                        }
                        
                    }else{
                        self?.displayToast((response?.message)!)
                    }
                }
            }
        }
        
    }
    
    func sendFriendRequest(){
        let networkWorker = MyNetworkWorker()
        networkWorker.sendFriendRequest(userId: userId) {[weak self] (response) in
            if response != nil{
                if response?.status == true{
                    DispatchQueue.main.async{
                        self?.detailsModel?.friendStatus = 2
                        self?.btnAddRemoveContact.setTitle("SENT INVITE", for: .normal)
                    }
                }else{
                    self?.displayToast((response?.message)!)
                }
            }
        }
    }
   
    func blockUser(type:String){
        let worker = HomeWorker()
        guard let userToBeBlocked = userId else{
           return
        }
        worker.blockUser(idOfUser: userToBeBlocked,type:type) {[weak self] (response) in
            if response.status == true{
                
                if self?.detailsModel?.blockStatus == 0{
                    DispatchQueue.main.async {
                        self?.btnAddRemoveContact.setTitle("Unblock", for:.normal )
                    }
                    self?.detailsModel?.blockStatus = 1
                }else{
                    DispatchQueue.main.async {
                        self?.btnAddRemoveContact.setTitle("Connect", for:.normal )
                    }
                   self?.detailsModel?.blockStatus = 0
                }
                
                print("User blocked succesfully")
                
            }
            self?.displayToast(response.message!)
        }
      
    }
    
    // TABLE VIEW DATA SOURCE AND DELEGATES
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        if self.detailsModel?.company?.pcomcountry != nil{
            switch section{
            case 0 :
                return companyModelArray.count
            case 1 :
                return honorsModelArray.count
                
            default:
                return 0
            }
        }else{
            switch section{
            case 0 :
                return experienceModelArray.count
            case 1 :
                return educationModelArray.count
            case 2 :
                return honorsModelArray.count
            default:
                return 0
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let workEducationCell = tableView.dequeueReusableCell(withIdentifier: "workEducationCell", for: indexPath) as! WorkEducationTableCell
        workEducationCell.collectionViewMedia.isHidden = true
        workEducationCell.btnEdit.isHidden = true
        workEducationCell.btnDelete.isHidden = true
        if self.detailsModel?.company?.pcomcountry != nil{
            switch indexPath.section{
            case 0:
                workEducationCell.setCompanyData(companyModel: companyModelArray[indexPath.row])
            case 1:
                workEducationCell.setHonorsData(honorsModel: honorsModelArray[indexPath.row] )
            default:
                print("empty cell")
                
            }
        }else{
            switch indexPath.section{
            case 0:
                workEducationCell.setExperienceData(experienceModel: experienceModelArray[indexPath.row])
            case 1:
                workEducationCell.setEducationData(educationModel: educationModelArray[indexPath.row] )
            case 2:
                workEducationCell.setHonorsData(honorsModel: honorsModelArray[indexPath.row] )
            default:
                print("empty cell")
                
            }
        }
        workEducationCell.heightConstraintCollectionView.constant = 0
        return workEducationCell
        
    }
    func numberOfSections(in tableView: UITableView) -> Int
    {
        if self.detailsModel?.company?.pcomcountry != nil{
            return 2
        }else{
            return 3
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableViewAutomaticDimension
    }
    
}

extension GroupDetailViewController : UIPopoverPresentationControllerDelegate , ItemSelected{
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return .none
    }
    
    func optionSelected(option: String?) {
        var type = ""
        if self.detailsModel?.blockStatus == 0{
            type = "block"
        }else{
            type = "unblock"
        }
        showConfirmationOptionToBlock(type:type, completion: { [weak self](index) in
            if index == 0{
                self?.blockUser(type: type)
            }
        })
        //showConfirmationOptionToBlock()
    }
}

