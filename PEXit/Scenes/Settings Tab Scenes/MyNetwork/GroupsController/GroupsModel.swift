//
//  GroupsModel.swift
//  PEXit
//
//  Created by Apple on 26/09/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation

struct GroupsRequestModel{
    var key : String?
    var typeOfSearch : String?
    var page : Int?
    var numberOfRecords : Int?
}

struct GroupsResponseModel : Codable {
    var status : Bool?
    var message : String?
    var datas : [GroupListModel]?
    
}

struct CreateGroupRequestModel{
    var gname : String?
    var gdescription : String?
    var gusers : [String]?
    var gtype : String?
    var groupId : Int?
}

/*//\"status\": true,\n    \"datas\": [\n        {\n            \"uid\": \"7\",\n            \"gid\": 15,\n            \"gname\": \"PEXit Support - Admin\",\n            \"gdescription\": \"PEXit Support Group\",\n            \"gusers\": \"support@pexit.net,gvr@sstusa.com\",\n            \"gtype\": \"public\",\n            \"createdBy\": \"10\",\n            \"checkDate\": \"2018-06-08 11:06:14\",\n            \"editKey\": 0,\n            \"joinKey\": 1,\n            \"adminKey\": 0,\n            \"userKey\": 0,\n            \"viewKey\": 1\n j       }\n    ],\n    \"message\": \"1 records found\"\n}\n"
 */


struct GroupListModel : Codable{
    var gid : Int?
    var uid : Int?
    var gname : String?
    var gdescription : String?
    var gusers : [ListOfIndividuals.IndividualUserModel]?
    var gtype : String?
    var joinKey : Int?
    var editKey : Int?
    var viewKey : Int?
    var adminKey : Int?
    var userKey : Int?
}

struct GroupDetailModel : Codable{
    var status : Bool?
    var message : String?
    var datas : GroupDetailListModel?
}




struct GroupDetailListModel : Codable{
    var gname : String?
    var gdescription : String?
    var groupFriendsCount : Int?
    var blockStatus : Int?
    var friendsCount : Int?
    var friendStatus : Int?
    var profileImage : String?
    var summary : String?
    var gimage : String?
    var createdBy : GroupCreatedBy?
    var education : [GroupEducationModel]?
    var experience : [GroupExperienceModel]?
    var honors : [GroupHonorsModel]?
    var company : GroupCompanyModel?
    
}
struct GroupCreatedBy : Codable{
    var name : String?
    var email : String?
    var phone : String?
    var title : String?
    var address : String?
    
}

struct GroupEducationModel : Codable{
    var edupid: Int?
    var edupuid: Int?
    var edupschoolname: String?
    var eduschoolfromyear: Int
    var eduschooltoyear: Int
    var eduschoolDegree: String?
    var eduschool_act: String?
    var eduexp_desc:String?
    var eduschoolstudy:String?
    var eduschoolgrade: String?
}

struct GroupCompanyModel : Codable{
   // var pcomuid: Int?
    var pcomcity: String?
    var pcomcountry: String?
    var pcomstates: String?
    //var pcomid: Int?
}

struct GroupExperienceModel : Codable{
    
    var exptitle: String?
    var expcompanyname: String?
    var expfrom_yr: Int?
    var expto_yr: Int?
    var expto_mon: Int?
    var expfrom_mon:Int?
    var expiscurrent:String?
    var expuid: Int?
    var expid: Int?
    var explocation:String?
    var expdes:String?
  
}


struct GroupHonorsModel : Codable{
    var honid: Int?
    var hontitle: String?
    var honuid: Int?
    var hondesc: String?
    var honmonth: Int?
    var honyear: Int?

}


