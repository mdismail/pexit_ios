//
//  MyNetworkConfigurator.swift
//  PEXit
//
//  Created by Apple on 20/09/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation


//
//  JobConfigurator.swift
//  PEXit
//
//  Created by Apple on 02/08/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation
import UIKit
// MARK: Connect View, Interactor, and Presenter

extension PendingViewController: PendingViewPresenterOutput{
    
}

extension PendingViewInteractor: PendingViewControllerOutput {
}

extension PendingViewPresenter: PendingViewInteractorOutput{
    
}

extension MyConnectionsViewController: ConnectionsViewPresenterOutput{
    
}

extension ConnectionsViewInteractor: MyConnectionsViewControllerOutput {
    
    
}

extension ConnectionsViewPresenter: ConnectionsViewInteractorOutput{
    
}

extension GroupsViewController: GroupsViewPresenterOutput{
    
}

extension GroupsViewInteractor: GroupsViewControllerOutput {
}

extension GroupsViewPresenter: GroupsViewInteractorOutput{
    
}

class MyNetworkConfigurator{
    // MARK: Object lifecycle
    
    class var sharedInstance: MyNetworkConfigurator{
        struct Static {
            static let instance =  MyNetworkConfigurator()
        }
        
        return Static.instance
    }
    
    // MARK: Configuration
    
    func configure(viewController: PendingViewController){
        
        let presenter = PendingViewPresenter()
        presenter.presenterOutput = viewController
        
        let interactor = PendingViewInteractor()
        interactor.interactorOutput = presenter
        
        viewController.output = interactor
//      viewController.router = router
    }
    
    func configureConnectionsController(viewController: MyConnectionsViewController){
        
        let presenter = ConnectionsViewPresenter()
        presenter.presenterOutput = viewController
        
        let interactor = ConnectionsViewInteractor()
        interactor.interactorOutput = presenter
        
        viewController.output = interactor
        //        viewController.router = router
    }
    
    func configureGroupsController(viewController: GroupsViewController){
        let router = GroupRouter()
        router.controller = viewController
        
        let presenter = GroupsViewPresenter()
        presenter.presenterOutput = viewController
        
        let interactor = GroupsViewInteractor()
        interactor.interactorOutput = presenter
        
        viewController.output = interactor
        viewController.router = router
    }
}
