//
//  MyNetworkParentViewController.swift
//  PEXit
//
//  Created by Apple on 19/09/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class MyNetworkParentViewController: UIViewController {
    var selectedIndex = 0
    var global_keywords = ""
    var myConnectionsController : MyConnectionsViewController?
    var appController : AppTabsController?
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
        // Do any additional setup after loading the view.
    }

    func setUpView(){
        let pendingController = storyboard?.instantiateViewController(withIdentifier: "pendingViewController") as! PendingViewController
        myConnectionsController = storyboard?.instantiateViewController(withIdentifier: "myConnectionsViewController") as! MyConnectionsViewController
        let groupsController = storyboard?.instantiateViewController(withIdentifier: "groupsViewController") as! GroupsViewController
       // var appController = AppTabsController(viewControllers:[pendingController,myConnectionsController,groupsController])
        let blockedUserController = storyboard?.instantiateViewController(withIdentifier: "blockedUsersViewController") as! BlockedUsersViewController
        //if CURRENT_GLOBAL_SEARCH_OPTION == GLOBAL_SEARCH_TYPE.CONNECTIONS.rawValue{
        appController =  AppTabsController(viewControllers:[pendingController,blockedUserController,myConnectionsController!,groupsController],selectedIndex:selectedIndex)
        if !global_keywords.isEmpty{
            myConnectionsController?.myConnectionsGlobalSearch(searchKeyword: global_keywords)
            global_keywords = ""
        }
//        }else{
//            appController = AppTabsController(viewControllers:[pendingController,myConnectionsController,groupsController])
//        }
        
        self.add(asChildViewController: appController!)
    }
    
    override func searchLocallyUsingGlobal_subview(searchKeyword : String, searchOptions : String){
        selectedIndex = 2
        if let connectionController = myConnectionsController{
            connectionController.myConnectionsGlobalSearch(searchKeyword: searchKeyword)
            appController?.select(at: selectedIndex)
        } else {
            global_keywords = searchKeyword
        }
        
    }
    
    override func cleanUpGlobalSearch_subview() {
        selectedIndex = 0
        global_keywords = ""
        if let connectionController = myConnectionsController{
            connectionController.myConnectionsCleanGlobalSearch()
        }
    }
    
    override func refresh_SubView() {
        if let connectionController = myConnectionsController{
            connectionController.myConnectionRefreshView()
        }
    }
    
}
