//
//  BlockedUsersViewController.swift
//  PEXit
//
//  Created by ats on 27/03/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class BlockedUsersViewController: UIViewController {
    @IBOutlet weak var tblBlockedUsers: UITableView!
    var blockedUserArray = [Connections.FetchConnections.UserRecord]()
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(BlockedUsersViewController.handleRefresh(_:)),
                                 for: UIControlEvents.valueChanged)
        refreshControl.tintColor = UIColor.red
        return refreshControl
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareTabItem()
        tblBlockedUsers.addSubview(refreshControl)
        tblBlockedUsers.tableFooterView = UIView()
        tblBlockedUsers.register(UINib.init(nibName: "BlockedUsersTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "blockedUserCell")
       
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getListOfBlockedUsers()
        tblBlockedUsers.reloadData()
    }
    
    @objc func handleRefresh(_ refreshControl : UIRefreshControl){
        blockedUserArray.removeAll()
        tblBlockedUsers.reloadData()
        getListOfBlockedUsers()
        self.refreshControl.endRefreshing()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            self.tblBlockedUsers.contentOffset = CGPoint.zero
        }
     
    }
    
    func getListOfBlockedUsers(){
        let worker = MyNetworkWorker()
        worker.fetchBlockedUsersList{ [weak self] (response)  in
            DispatchQueue.main.async{
                if response != nil {
                    if response?.status == true{
                        self?.blockedUserArray = response?.datas ?? []
                        self?.tblBlockedUsers.reloadData()
                    }else{
                        self?.displayToast(response?.message ?? UNKNOWN_ERROR_MSG)
                    }
                }else{
                    self?.displayToast(UNKNOWN_ERROR_MSG)
                }
            }
            }
        }
    
        func unblockUser(indexInArray : Int){
            self.showConfirmationOptionToBlock(type: "unblock") { [weak self] (index) in
                if index == 0{
                    let record = self?.blockedUserArray[indexInArray]
                    guard let userId = record!.id else{
                        return
                    }
                    self?.unblockUser(idOfUser:userId,indexInArray:indexInArray)

                }
            }
        }
    
    func unblockUser(idOfUser:Int,indexInArray:Int){
            let worker = HomeWorker()
            worker.blockUser(idOfUser: idOfUser,type:"unblock") {[weak self] (response) in
                if response.status == true{
                    DispatchQueue.main.async{
                        self?.blockedUserArray.remove(at: indexInArray)
                        self?.tblBlockedUsers.reloadData()
                        print("User blocked succesfully")
                    }
                }
                self?.displayToast(response.message!)
            }
        }
    
    }


extension BlockedUsersViewController : UITableViewDataSource , UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return blockedUserArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let blockedUserCell = tableView.dequeueReusableCell(withIdentifier: "blockedUserCell", for: indexPath) as? BlockedUsersTableViewCell else{
            return UITableViewCell()
        }
        blockedUserCell.btnUnblockUser.tag = indexPath.row
        blockedUserCell.unblockClicked = { [weak self] (index) in
            self?.unblockUser(indexInArray : index)
        }
        blockedUserCell.setUserData(userRecord:blockedUserArray[indexPath.row])
        return blockedUserCell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
}


extension BlockedUsersViewController {
    fileprivate func prepareTabItem() {
        tabItem.title = "Blocked Users"
    }
}
