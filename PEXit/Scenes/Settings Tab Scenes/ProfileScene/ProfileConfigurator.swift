//
//  ProfileConfigurator.swift
//  PEXit
//
//  Created by Apple on 29/08/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation

//
//  HomeConfigurator.swift
//  PEXit
//
//  Created by Apple on 27/07/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation
import UIKit

extension ProfileViewController: ProfilePresenterOutput{

}

extension ProfileInteractor : ProfileViewControllerOutput{
    
    
}

extension ProfilePresenter: ProfileInteractorOutput{
    
}

class ProfileConfigurator{
    
    class var sharedInstance : ProfileConfigurator{
        struct Static{
            static let instance = ProfileConfigurator()
        }
        return Static.instance
    }
    
    
    func configure(viewController : ProfileViewController){
        let router = ProfileRouter()
        router.controller = viewController
        
        let presenter = ProfilePresenter()
        presenter.profilePresenterOutput = viewController
        
        let interactor = ProfileInteractor()
        interactor.profileInteractorOutput = presenter
        
        viewController.router = router
        viewController.output = interactor
    }
    
    
}
