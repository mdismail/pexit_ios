//
//  ProfileUpdateRequest.swift
//  PEXit
//
//  Created by Apple on 30/08/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation

struct ProfileRequestModel{
    struct ProfileRequest{
        var uname : String?
        var puserType : String?
        var ptitle : String?
        var pgender : String?
        var pphone : Int?
        var ppostalcode : Int?
        var pdob : String?
        var paddress : String?
        var pcountry : String?
        var puserstate : String?
        var plocation : String?
        var psummary : String?
        var plnk : String?
        var pvdeo : String?
        var pfle: [MediaAttatchment]?
        var pphot: [MediaAttatchment]?
        var pprest : [MediaAttatchment]?
        var pimage : MediaAttatchment?
    }
}
