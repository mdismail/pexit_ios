//
//  ExperienceViewController.swift
//  PEXit
//
//  Created by Apple on 05/09/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

protocol HeightUpdates{
    func updateHeight(height : CGFloat)
}


class ExperienceViewController: UIViewController , UITextFieldDelegate , UITextViewDelegate , ItemSelected{
    @IBOutlet weak var txtFieldTitle: TJTextField!
    @IBOutlet weak var txtFieldCompanyName: TJTextField!
    @IBOutlet weak var txtFieldFromMonth: TJTextField!
    @IBOutlet weak var txtFieldFromYear: TJTextField!
    @IBOutlet weak var txtFieldToMonth: TJTextField!
    @IBOutlet weak var txtFieldToYear: TJTextField!
    @IBOutlet weak var btnCurrentOrganization: UIButton!
    @IBOutlet weak var txtFieldLocation: TJTextField!
    @IBOutlet weak var txtViewDescription: UITextView!
    @IBOutlet weak var heightDescriptionConstraint: NSLayoutConstraint!
    var router = WorkEducationRouter()
    var currentTextField : TJTextField?
    var delegate : HeightUpdates?
    var isCurrent = ""
    var currentExpModel : ExperienceModel?
    var updatedExpModel : ExperienceModel?
    var paramsDict = [String : Any]()
    var experienceModelId : Int?

    override func viewDidLoad() {
        super.viewDidLoad()
        txtFieldCompanyName.addPreviousNextDoneOnKeyboard(withTarget: self, previousAction: #selector(self.previousAction(_:)), nextAction: #selector(self.nextAction(_:)), doneAction: #selector(self.doneAction(_:)), shouldShowPlaceholder: true)
        txtFieldToYear.addPreviousNextDoneOnKeyboard(withTarget: self, previousAction: #selector(self.previousAction(_:)), nextAction: #selector(self.nextAction(_:)), doneAction: #selector(self.doneAction(_:)), shouldShowPlaceholder: true)
        txtViewDescription.delegate = self
//        txtViewDescription.placeholder = textViewPlaceholder
    }
    
//    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
//         let textField = textField as! TJTextField
//         currentTextField = textField
//        if textField == txtFieldFromMonth || textField == txtFieldToMonth || textField == txtFieldFromYear || textField == txtFieldToYear{
//            self.view.endEditing(true)
//        }
//
//            if textField == txtFieldFromMonth || textField == txtFieldToMonth {
//                router.navigateToPopUpController(sender: textField, optionsArray: Utils.getAllMonths(),controller: self)
//                return false
//            }
//            if textField == txtFieldFromYear {
//                router.navigateToPopUpController(sender: textField, optionsArray: Utils.getListOfYears(addCount: FROM_YEAR_CONSTANT),controller: self)
//                return false
//            }
//            if textField == txtFieldToYear {
//                router.navigateToPopUpController(sender: textField, optionsArray: Utils.getListOfYears(addCount: TO_YEAR_CONSTANT),controller: self)
//                return false
//        }
//
//        return true
//    }

    @objc func previousAction(_ sender : UITextField!) {
        if txtFieldCompanyName.isFirstResponder{
            txtFieldTitle.becomeFirstResponder()
        }
        if txtFieldLocation.isFirstResponder{
            txtFieldToYear.becomeFirstResponder()
        }
    }
    
    @objc func nextAction(_ sender : UITextField!) {
        if (txtFieldCompanyName.isFirstResponder){
            txtFieldFromMonth.becomeFirstResponder()
        }
        if txtFieldLocation.isFirstResponder{
            txtViewDescription.becomeFirstResponder()
        }
    }
    
    @objc func doneAction(_ sender : UITextField!) {
        self.view.endEditing(true)
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        let textField = textField as! TJTextField
        currentTextField = textField
        switch textField{
        case txtFieldFromMonth,txtFieldToMonth:
            self.dismissKeyboard()
            router.navigateToPopUpController(sender: textField, optionsArray: Utils.getAllMonths(),controller: self)
        case txtFieldFromYear:
            self.dismissKeyboard()
            router.navigateToPopUpController(sender: textField, optionsArray: Utils.getListOfYears(addCount: FROM_YEAR_CONSTANT),controller: self)
        case txtFieldToYear:
            self.dismissKeyboard()
            router.navigateToPopUpController(sender: textField, optionsArray: Utils.getListOfYears(addCount: FROM_YEAR_CONSTANT),controller: self)
        default:
            return true
        }
//        DispatchQueue.main.asyncAfter(deadline: .now() + 0.08) {
//            self.dismissKeyboard()
//        }
        return false
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField{
        case txtFieldTitle:
            paramsDict["pexptitle"] = textField.text
        case txtFieldCompanyName:
            paramsDict["pexpcompanyname"] = textField.text
//            txtFieldFromMonth.becomeFirstResponder()
        case txtFieldLocation:
            paramsDict["pexplocation"] = textField.text
        default:
            print("Test")
        }
        
    }
    
    func optionSelected(option: String?) {
        currentTextField?.text = option
        switch currentTextField{
        case txtFieldFromYear:
            paramsDict["pexpfrom_yr"] = Int(option!)
        case txtFieldToYear:
            paramsDict["pexpto_yr"] = Int(option!)
        case txtFieldFromMonth:
            let dateFormatter = DateFormatter()
            paramsDict["pexpfrom_mon"] = dateFormatter.getAllMonths().index(of: option!)
        case txtFieldToMonth:
            let dateFormatter = DateFormatter()
            paramsDict["pexpto_mon"] = dateFormatter.getAllMonths().index(of: option!)
        default:
            break
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
//            textView.updateTextViewPlacholder()
            paramsDict["pexpdes"] = textView.text
            caculateTxtHeight()

    }
    
    func caculateTxtHeight(){
        let size = txtViewDescription.sizeThatFits(CGSize(width: txtViewDescription.frame.size.width, height: CGFloat.greatestFiniteMagnitude))
        if size.height != heightDescriptionConstraint.constant {
            heightDescriptionConstraint.constant = size.height
            delegate?.updateHeight(height: size.height)
        }
    }
    

    
    @IBAction func btnCurrentOrganizationClicked(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.isSelected{
            isCurrent = "isCurrent"
            txtFieldToYear.text = ""
            txtFieldToMonth.text = ""
            txtFieldToYear.isUserInteractionEnabled = false
            txtFieldToMonth.isUserInteractionEnabled = false
        }else{
            isCurrent = ""
            txtFieldToYear.isUserInteractionEnabled = true
            txtFieldToMonth.isUserInteractionEnabled = true
        }
        paramsDict["pexpiscurrent"] = isCurrent
    }
    
    
    func setDataForEdit(currentModel : ExperienceModel){
        if currentExpModel == nil || (currentExpModel != updatedExpModel && currentExpModel != nil){
            currentExpModel = currentModel
            paramsDict["pexpcompanyname"] = currentExpModel?.pexpcompanyname
            paramsDict["pexptitle"] = currentExpModel?.pexptitle
            paramsDict["pexpfrom_yr"] = currentExpModel?.pexpfrom_yr
            paramsDict["pexpto_yr"] = currentExpModel?.pexpto_yr
            paramsDict["pexplocation"] = currentExpModel?.pexplocation
            paramsDict["pexpdes"] = currentExpModel?.pexpdes
            paramsDict["pexpiscurrent"] = currentExpModel?.pexpiscurrent
            paramsDict["pexpfrom_mon"] = currentExpModel?.pexpfrom_mon
            paramsDict["pexpto_mon"] = currentExpModel?.pexpto_mon
            
        }
        if !(currentModel.isInvalidated){
            experienceModelId = currentModel.pexpid
        }
        
        txtFieldCompanyName.text = paramsDict["pexpcompanyname"] as? String
        txtFieldTitle.text = paramsDict["pexptitle"] as? String
        txtFieldFromYear.text = (paramsDict["pexpfrom_yr"] as? Int).map(String.init) ?? ""
        if (paramsDict["pexpto_yr"] as? Int) != 0{
             txtFieldToYear.text = (paramsDict["pexpto_yr"] as? Int).map(String.init) ?? ""
        }
       
        txtFieldLocation.text = paramsDict["pexplocation"] as? String
        txtViewDescription.text = paramsDict["pexpdes"] as? String
        caculateTxtHeight()
        isCurrent = (paramsDict["pexpiscurrent"] as? String)!
        if isCurrent == ""{
            btnCurrentOrganization.isSelected = false
        }else{
            btnCurrentOrganization.isSelected = true
        }
        
        let dateformatter = DateFormatter()
        txtFieldFromMonth.text = "\(dateformatter.getMonthName(monthNumber: (paramsDict["pexpfrom_mon"] as? Int)! - 1))"
        if paramsDict["pexpto_mon"] as? Int > 0{
             txtFieldToMonth.text = "\(dateformatter.getMonthName(monthNumber: (paramsDict["pexpto_mon"] as? Int)! - 1))"
        }
       // txtViewDescription.updateTextViewPlacholder()
    }
    
    
    func clearData(){
        if currentExpModel != nil {
            currentExpModel = nil
            addNewClicked()
        }
       
    }
    
    func addNewClicked(){
        if txtFieldCompanyName != nil{
            currentExpModel = nil
            experienceModelId = nil
            txtFieldCompanyName.text = ""
            txtFieldTitle.text = ""
            txtFieldFromYear.text = ""
            txtFieldToYear.text = ""
            txtFieldLocation.text = ""
            txtViewDescription.text = ""
            heightDescriptionConstraint.constant = 30
            txtViewDescription.updateTextViewPlacholder()
            isCurrent = ""
            btnCurrentOrganization.isSelected = false
            txtFieldFromMonth.text = ""
            txtFieldToMonth.text = ""
            txtFieldToYear.isUserInteractionEnabled = true
            txtFieldToMonth.isUserInteractionEnabled = true
        }
       
    }
    
    func fetchExperienceData() -> WorkEducationRequestModel.AddExperienceModel?{
        var textFieldArray = [txtFieldCompanyName,txtFieldTitle,txtFieldFromMonth,txtFieldFromYear]
        if isCurrent == ""{
            textFieldArray.append(txtFieldToMonth)
            textFieldArray.append(txtFieldToYear)
        }
        let dateFormatter = DateFormatter()
        if Utils.validateFormAndShowToastWithRequiredFields(textFieldArray as! [TJTextField]){
            var requestModel = WorkEducationRequestModel.AddExperienceModel()
            if isCurrent == "" {

                if txtFieldFromYear.text > txtFieldToYear.text{
                    displayToast("From year should be less than to year")
                    return nil
                } else if txtFieldFromYear.text == txtFieldToYear.text {
                    let fromMonthNumber = (dateFormatter.getAllMonths().index(of: txtFieldFromMonth.text!))!+1
                    let toMonthNumber = (dateFormatter.getAllMonths().index(of: txtFieldToMonth.text!))!+1
                    
                    if fromMonthNumber > toMonthNumber {
                        displayToast("From month should be less than to month")
                        return nil
                    }
                }
            }
            requestModel.companyname = txtFieldCompanyName.text
            requestModel.exp_title = txtFieldTitle.text
            
            let monthNumber = (dateFormatter.getAllMonths().index(of: txtFieldFromMonth.text!))!+1
            requestModel.startDateMonth = "\(monthNumber)"
            
            
//            requestModel.startDateMonth = txtFieldFromMonth.text
            requestModel.startDateYear = txtFieldFromYear.text
            if !(txtFieldToMonth.text?.isEmpty)!{
                let toMonthNumber = (dateFormatter.getAllMonths().index(of: txtFieldToMonth.text!))!+1
                requestModel.endDateMonth = "\(toMonthNumber)"
            }
            requestModel.endDateYear = txtFieldToYear.text
            //         requestModel.companyname = txtFieldToYear.text
            requestModel.exp_location = txtFieldLocation.text
            requestModel.exp_desc = txtViewDescription.text
            if currentExpModel != nil{
                requestModel.isEdit = true
                requestModel.currentModelId = experienceModelId!
            }
            
            requestModel.iscurrent = isCurrent
            
            return requestModel
        }
        return nil
    }
}
