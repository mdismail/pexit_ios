//
//  WorkEducationRouter.swift
//  PEXit
//
//  Created by Apple on 05/09/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation


import UIKit

//protocol ProfileRouterInput{
//
//}


class WorkEducationRouter:NSObject,UIPopoverPresentationControllerDelegate{
//    weak var controller : ProfileViewController!
    
    func navigateToPopUpController(sender:UITextField,optionsArray:[String]?,controller:UIViewController){
        
        let popUpController = controller.storyboard?.instantiateViewController(withIdentifier: "commonViewController") as! CommonViewController
        popUpController.modalPresentationStyle = .popover
        popUpController.preferredContentSize = CGSize(width : sender.frame.size.width, height : CGFloat((optionsArray?.count)! * COMMON_CONTROLLER_ROW_HEIGHT))
        let popOver = popUpController.popoverPresentationController
        popOver?.delegate = self
        popOver?.permittedArrowDirections = .up
        popOver?.sourceView = sender
        popOver?.sourceRect = sender.bounds
        if controller is EducationViewController{
            popUpController.delegate = controller as! EducationViewController
        }
        if controller is ExperienceViewController{
            popUpController.delegate = controller as! ExperienceViewController
        }
        if controller is AddHonorsController{
            popUpController.delegate = controller as! AddHonorsController
        }
        
        popUpController.itemsArray = optionsArray
        controller.present(popUpController, animated: true, completion: nil)
        
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return .none
    }
    
    
    
    
}
