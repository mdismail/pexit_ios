//
//  ProfileViewController.swift
//  PEXit
//
//  Created by Apple on 06/08/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import Presentr

let DEFAULT_HEIGHT_ATTACHMENT_VIEW = CGFloat(50)

protocol ProfileViewControllerInput{
    func showProfileResult(message:String?, status:Bool)
    func profileUpdateResponse(response : NewPost.Response)
}

protocol ProfileViewControllerOutput{
    func fetchUserProfile()
    func updateUserProfile(paramsDict : [String:Any],profileImage:Data?,request:NewPost.Request)
}

class ProfileViewController: UIViewController , UITableViewDataSource , UITableViewDelegate , ProfileViewControllerInput , PassAttatchmentsToController , SaveUserProfile , ItemSelected{
    @IBOutlet weak var tblProfile: UITableView!
    var arrayOfTextFields : [TextfieldInfoStruct]?
    var router : ProfileRouter!
    //    var profileRequestModel : ProfileRequestModel.ProfileRequest?
    //    var displayedItems = [TextfieldInfoStruct]()
    var output : ProfileViewControllerOutput!
    var countryArray = [String]()
    var stateArray = [String]()
    var cityArray = [String]()
    var countryCodeArray = [String]()
    var attachmentCell : ProfileTableViewCell?
    var updatedHeight : CGFloat?
    var currentUser : ProfileDetailRealmModel?
    var paramsDict = [String : String]()
    
    var userProfileImage : Data?
    var currentTextField : UITextField?
    var mediaArrayCount = 0
    var countryOfUser = ""
    var cityOfUser = ""
    var locationOfUser = ""
    var birthDate = ""
    var countryCode = ""
    var currentCell : ProfileTableViewCell?
    var currentImage : UIImage?
    var userDateOfBirth = ""
    var isImageChanged = false
    var isProfileUpdated = false
    var textUpdated = false
    var arrayLangInfo = [AllLangDetails]()
    @IBOutlet weak var viewAttachment: UIView!
    
    @IBOutlet weak var constraintProfileTblHeight: NSLayoutConstraint!
    private lazy var attachmentController: AttatchmentViewController = {
        var viewController = storyboard?.instantiateViewController(withIdentifier: "attachmentController") as! AttatchmentViewController
        //viewController.delegate = self
        self.add(asChildViewController: viewController)
        return viewController
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblProfile.register(UINib(nibName: "ProfileTableViewCell", bundle: nil), forCellReuseIdentifier: "profileCell")
        print("NAVIGATION CONTROLLER---->",self.navigationController)
        
//        NotificationCenter.default.addObserver(self, selector: #selector(JobDetailViewController.languageUpdateNotification(notification:)), name: Notification.Name("LanguageUpdated"), object: nil)
      
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if !isImageChanged || (currentUser?.isInvalidated)!{
            if let currentProfile = ProfileDetailRealmModel.fetchProfile(){
                currentUser = currentProfile
                getTextFieldArray()
                calculateMediaArray()
                tblProfile.reloadData()
                if (currentUser?.isInvalidated)!{
                    displayToast("Invalid user")
                    return
                }
            }
            output.fetchUserProfile()
        }else{
            isImageChanged = false
        }
        
    }
    
//    deinit {
//        NotificationCenter.default.removeObserver(self, name: Notification.Name("LanguageUpdated"), object: nil)
//    }
//
//    @objc func languageUpdateNotification(notification: NSNotification){
//        // Take Action on Notification
//        translateToCurrentSelectedLanguage()
//    }
    
    func displayLanguages(){
        getLanguages { (languagesArray,completeLanguageDetails) in
            self.arrayLangInfo = completeLanguageDetails
            self.presentLanguagesView(languageArray: languagesArray)
        }
    }
    
    func presentLanguagesView(languageArray : [String]){
        currentTextField = nil
        DispatchQueue.main.async {
            let presenter = Presentr(presentationType: .popup)
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "commonViewController") as! CommonViewController
            // controller.isFromHome = true
            controller.isLanguageUpdation = true
            controller.itemsArray = languageArray
            controller.delegate = self
            self.customPresentViewController(presenter, viewController: controller, animated: true)
        }
    }
    
    func translateToCurrentSelectedLanguage(){
        if currentUser != nil && !((currentUser?.isInvalidated)!){
            if self.paramsDict["psummary"] != nil{
                let translationWorker = LanguageTranslationWoker()
                
//                let profileSummary = (currentUser?.psummary?.stringByStrippingHTMLTags)?.stringByDecodingHTMLEntities
                translationWorker.translateCurrentString(newLanguageCode: CURRENT_APP_LANGUAGE, stringsToTranslate: [self.paramsDict["psummary"] ?? ""]) { (response) in
                    if response != nil{
                        
                    if let translations = response as? [ReturnedJson]{
                        DispatchQueue.main.async {
                            var i = 0
                            for value in translations{
                                
                                for translatedStr in value.translations{
                                    userProfileSummary = translatedStr.text
                                    i = i + 1
                                    
                                    
                                }
                                
                            }
                            self.paramsDict["psummary"] = userProfileSummary
                            //self.tblProfile.reloadData()
                            self.textUpdated = true
                            self.tblProfile.reloadRows(at: [IndexPath.init(row: (self.arrayOfTextFields?.count)! + 1, section: 0)], with: .automatic)
                        }
                        }
                    }
                }
            }
        }else{
            print("INVALIDATED OBJECT, NEED TO CHECK REASON")
        }
        
    }
    
    override func awakeFromNib(){
        super.awakeFromNib()
        ProfileConfigurator.sharedInstance.configure(viewController: self)
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayOfTextFields != nil ? arrayOfTextFields!.count + 5 : 0
    }
    
    func getTextFieldArray(){

        if let dateOfBirth = currentUser?.pdob{
            userDateOfBirth = Utils.generalDateWithString(dateOfBirth,newDateFormat:"dd MMMM YYYY")
        }
        if let country = currentUser?.pcountry{
            countryOfUser = country
        }
        if let city = currentUser?.puserstate{
            cityOfUser = city
        }
        if let location = currentUser?.plocation{
            locationOfUser = location
        }
        
        paramsDict["pphone"] = currentUser?.pphone
        paramsDict["pdob"] = userDateOfBirth
        paramsDict["pgender"] = currentUser?.pgender
        paramsDict["paddress"] = currentUser?.paddress
        paramsDict["pcountry"] = countryOfUser
        paramsDict["puserstate"] = cityOfUser
        paramsDict["plocation"] = locationOfUser
        paramsDict["ppostalcode"] = currentUser?.ppostalcode
        paramsDict["uname"] = currentUser?.uname
        paramsDict["puserType"] = currentUser?.puserType
        paramsDict["ptitle"] = currentUser?.ptitle
        paramsDict["pimage"] = currentUser?.pimage
        paramsDict["psummary"] = currentUser?.psummary
        if userProfileSummary != ""{
            paramsDict["psummary"] = userProfileSummary
        }
        if let phoneNumberCode = currentUser?.pcode{
            countryCode = "+\(phoneNumberCode ??? "")"
            paramsDict["countryCode"] = countryCode
        }
        if currentUser?.pcode == ""{
            countryCode = "+1" // Giving a default country code for United States
            paramsDict["countryCode"] = countryCode
        }
        generateTextFields(typeOfUser: currentUser?.puserType ?? "", dateOfBirth: userDateOfBirth)
    }
    
    @ objc func chooseCountryCode(_ sender:UIButton){
        print("Country Code Clicked")
        fetchCountryCodes(countryCodeBtn: sender)
    }
    
    func generateTextFields(typeOfUser : String , dateOfBirth : String){

        switch typeOfUser{
        case PROFILE_TYPE.ORGANIZATION.rawValue:
            arrayOfTextFields = [
                TextfieldInfoStruct(placeholderText: "Name",textfieldText: paramsDict["uname"]),
                TextfieldInfoStruct(placeholderText: "Job Title",textfieldText: paramsDict["ptitle"]),
                TextfieldInfoStruct(rightImage: UIImage(named: "drop"),placeholderText: "Profession",textfieldText: paramsDict["puserType"]),
                TextfieldInfoStruct(placeholderText: "Email",textfieldText: currentUser?.uemail),TextfieldInfoStruct(placeholderText: "Phone Number",textfieldText: paramsDict["pphone"]) ,TextfieldInfoStruct(placeholderText: "Address",textfieldText: paramsDict["paddress"]),TextfieldInfoStruct(rightImage: UIImage(named: "drop"), placeholderText: "Country",textfieldText: countryOfUser),TextfieldInfoStruct(rightImage: UIImage(named: "drop"),placeholderText: "State",textfieldText: cityOfUser),TextfieldInfoStruct(rightImage: UIImage(named: "drop"),placeholderText: "Location",textfieldText: locationOfUser),TextfieldInfoStruct(placeholderText: "Postal Code",textfieldText: paramsDict["ppostalcode"])]
            
        case PROFILE_TYPE.PROFESSIONAL.rawValue ,  PROFILE_TYPE.STUDENT.rawValue,PROFILE_TYPE.OTHERS.rawValue :
            arrayOfTextFields = [
                TextfieldInfoStruct(placeholderText: "Name",textfieldText: paramsDict["uname"]),
                TextfieldInfoStruct(placeholderText: "Job Title",textfieldText: paramsDict["ptitle"]),
                TextfieldInfoStruct(rightImage: UIImage(named: "drop"),placeholderText: "Profession",textfieldText: paramsDict["puserType"]),
                
                TextfieldInfoStruct(placeholderText: "Email",textfieldText: currentUser?.uemail),
                TextfieldInfoStruct(placeholderText: "Phone Number",textfieldText: paramsDict["pphone"]),
                TextfieldInfoStruct(rightImage: UIImage(named: "drop"),placeholderText: "Date Of Birth",textfieldText:dateOfBirth),TextfieldInfoStruct(rightImage: UIImage(named: "drop"),placeholderText: "Gender",textfieldText: paramsDict["pgender"]),TextfieldInfoStruct(placeholderText: "Address",textfieldText: paramsDict["paddress"]),TextfieldInfoStruct(rightImage: UIImage(named: "drop"), placeholderText: "Country",textfieldText: countryOfUser),TextfieldInfoStruct(rightImage: UIImage(named: "drop"),placeholderText: "State",textfieldText: cityOfUser),TextfieldInfoStruct(rightImage: UIImage(named: "drop"),placeholderText: "Location",textfieldText: locationOfUser),TextfieldInfoStruct(placeholderText: "Postal Code",textfieldText: paramsDict["ppostalcode"])]
        default :
            arrayOfTextFields = [
                TextfieldInfoStruct(placeholderText: "Name",textfieldText: currentUser?.uname),
                TextfieldInfoStruct(placeholderText: "Job Title",textfieldText: currentUser?.ptitle),
                TextfieldInfoStruct(rightImage: UIImage(named: "drop"),placeholderText: "Profession",textfieldText: currentUser?.puserType),
                TextfieldInfoStruct(placeholderText: "Email",textfieldText: currentUser?.uemail),TextfieldInfoStruct(placeholderText: "Phone Number",textfieldText: paramsDict["pphone"]),TextfieldInfoStruct(rightImage: UIImage(named: "drop"),placeholderText: "Date Of Birth",textfieldText:dateOfBirth),TextfieldInfoStruct(rightImage: UIImage(named: "drop"),placeholderText: "Gender",textfieldText: paramsDict["pgender"]),TextfieldInfoStruct(placeholderText: "Address",textfieldText: paramsDict["paddress"]),TextfieldInfoStruct(rightImage: UIImage(named: "drop"), placeholderText: "Country",textfieldText: countryOfUser),TextfieldInfoStruct(rightImage: UIImage(named: "drop"),placeholderText: "State",textfieldText: cityOfUser),TextfieldInfoStruct(rightImage: UIImage(named: "drop"),placeholderText: "Location",textfieldText: locationOfUser),TextfieldInfoStruct(placeholderText: "Postal Code",textfieldText: paramsDict["ppostalcode"])]
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let profileCell = tableView.dequeueReusableCell(withIdentifier: "profileCell", for: indexPath) as! ProfileTableViewCell
        profileCell.delegate = self
        currentCell = profileCell
        profileCell.txtFieldProfile.delegate = self
//        profileCell.txtFieldName.delegate = self
//        profileCell.txtFieldJobTitle.delegate = self
//        profileCell.txtFieldProfession.delegate = self
        profileCell.txtViewProfileSummary.delegate = self
        profileCell.btnChangeImage.addTarget(self, action: #selector(changeProfileImage), for: UIControlEvents.touchUpInside)
        profileCell.btnCodePicker.addTarget(self, action: #selector(chooseCountryCode), for: UIControlEvents.touchUpInside)
        if indexPath.row == 0{
            
            profileCell.txtFieldProfile.isHidden = true
            profileCell.viewProfileSummary.isHidden = true
            profileCell.viewProfileHeader.isHidden = false
            profileCell.viewAttachment.isHidden = true
            profileCell.btnSave.isHidden = true
            profileCell.collectionViewProfileMedia.isHidden = true
            if currentImage != nil{
                profileCell.imgViewUser.image = currentImage
            }
            profileCell.setProfileHeaderData(profileHeaderDict: paramsDict as! [String : String])
        }
        else if indexPath.row > 0 && indexPath.row < (arrayOfTextFields?.count)! + 1{
            profileCell.txtFieldProfile.isHidden = false
            profileCell.txtFieldProfile.tag = indexPath.row - 1
            profileCell.viewProfileSummary.isHidden = true
            profileCell.viewProfileHeader.isHidden = true
            profileCell.viewAttachment.isHidden = true
            profileCell.btnSave.isHidden = true
            profileCell.txtFieldProfile.delegate = self
            profileCell.collectionViewProfileMedia.isHidden = true
            profileCell.configureTextfield(arrayOfTextFields![indexPath.row - 1],countryCode:paramsDict["countryCode"])
        }
        else if indexPath.row == (arrayOfTextFields?.count)! + 1{
            profileCell.txtFieldProfile.isHidden = true
            currentCell = profileCell
            profileCell.viewProfileSummary.isHidden = false
            profileCell.viewProfileHeader.isHidden = true
            profileCell.viewAttachment.isHidden = true
            profileCell.btnSave.isHidden = true
            profileCell.collectionViewProfileMedia.isHidden = true
            profileCell.setProfileHeaderData(profileHeaderDict: paramsDict as! [String : String])
        }
        else if indexPath.row == (arrayOfTextFields?.count)! + 2{
            profileCell.txtFieldProfile.isHidden = true
            profileCell.viewProfileSummary.isHidden = true
            profileCell.viewProfileHeader.isHidden = true
            profileCell.viewAttachment.isHidden = true
            profileCell.btnSave.isHidden = true
            profileCell.collectionViewProfileMedia.register(UINib(nibName: "HomeDetailCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "homeDetailsCell")
            profileCell.collectionViewProfileMedia.isHidden = false
            
            if currentUser != nil && mediaArrayCount > 0{
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                    
                    profileCell.collectionViewProfileMedia.dataSource = self
                    profileCell.collectionViewProfileMedia.delegate = self
//                    if let flowLayout = profileCell.collectionViewProfileMedia.collectionViewLayout as? UICollectionViewFlowLayout {
//                        flowLayout.scrollDirection = .vertical
//                    }
                    profileCell.collectionViewProfileMedia.reloadData()
                })
                
            }else{
                profileCell.collectionViewProfileMedia.dataSource = nil
                profileCell.collectionViewProfileMedia.delegate = nil
            }
            
        }
        else if indexPath.row == (arrayOfTextFields?.count)! + 3{
            attachmentCell = profileCell
            profileCell.txtFieldProfile.isHidden = true
            profileCell.viewProfileSummary.isHidden = true
            profileCell.viewProfileHeader.isHidden = true
            profileCell.viewAttachment.isHidden = false
            profileCell.collectionViewProfileMedia.isHidden = true
            addAttachmentView(currentCell: profileCell)
            profileCell.btnSave.isHidden = true
        }else{
            profileCell.txtFieldProfile.isHidden = true
            profileCell.viewProfileSummary.isHidden = true
            profileCell.viewProfileHeader.isHidden = true
            profileCell.viewAttachment.isHidden = true
            profileCell.btnSave.isHidden = false
            profileCell.collectionViewProfileMedia.isHidden = true
        }
        return profileCell
    }
    
    @objc func changeProfileImage(){
        ImageVideoPicker.imageVideoPicker.showImagePicker(controller: self, mediaType: .Image)
        ImageVideoPicker.imageVideoPicker.imagePickedBlock = { [unowned self](imageData) in
            self.isImageChanged = true
            self.userProfileImage = UIImageJPEGRepresentation(imageData.file as! UIImage, 0.4)
            self.paramsDict["pimage"] = ""
            self.currentImage = imageData.file as? UIImage
            if let tblCell = self.tblProfile.cellForRow(at: IndexPath.init(row:0,section:0)){
                (tblCell as! ProfileTableViewCell).imgViewUser.image = imageData.file as? UIImage
                self.tblProfile.reloadRows(at: [IndexPath.init(row: 0, section: 0)], with: .automatic)
            }
            //  if self.currentCell?.imgViewUser != nil{
            //                self.paramsDict["pimage"] = ""
            //                self.currentImage = imageData.file as? UIImage
            //
            //                self.currentCell?.imgViewUser.image =  imageData.file as? UIImage
            //                self.tblProfile.reloadRows(at: [IndexPath.init(row: 0, section: 0)], with: .automatic)
            // }
        }
    }
    
    func addAttachmentView(currentCell : ProfileTableViewCell){
        if (attachmentCell?.viewAttachment.subviews.count)! < 1{
            addChildViewController(attachmentController)
            currentCell.viewAttachment.addSubview(attachmentController.view)
            attachmentController.view.translatesAutoresizingMaskIntoConstraints = false
            attachmentController.delegate = self
            attachmentController.btnVideoAttatchment.isHidden = true
            attachmentController.btnPost.isHidden = true
            currentCell.viewAttachment.frame = attachmentController.view.bounds
            currentCell.viewAttachment.addConstraint(NSLayoutConstraint(item: attachmentController.view, attribute: NSLayoutAttribute.leading, relatedBy: NSLayoutRelation.equal, toItem: currentCell.viewAttachment, attribute: NSLayoutAttribute.leading, multiplier: 1.0, constant: 0.0))
            currentCell.viewAttachment.addConstraint(NSLayoutConstraint(item: attachmentController.view, attribute: NSLayoutAttribute.trailing, relatedBy: NSLayoutRelation.equal, toItem: currentCell.viewAttachment, attribute: NSLayoutAttribute.trailing, multiplier: 1.0, constant: 0.0))
            currentCell.viewAttachment.addConstraint(NSLayoutConstraint(item: attachmentController.view, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal, toItem: currentCell.viewAttachment, attribute: NSLayoutAttribute.top, multiplier: 1.0, constant: 0.0))
            currentCell.viewAttachment.addConstraint(NSLayoutConstraint(item: attachmentController.view, attribute: NSLayoutAttribute.bottom, relatedBy: NSLayoutRelation.equal, toItem: currentCell.viewAttachment, attribute: NSLayoutAttribute.bottom, multiplier: 1.0, constant: 0.0))
            attachmentController.didMove(toParentViewController: self)
            //        attachmentController.view.setNeedsLayout()
            attachmentController.view.layoutIfNeeded()
            attachmentController.didMove(toParentViewController: self)
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0{
            return 100
        }
        else if indexPath.row > 0 && indexPath.row < (arrayOfTextFields?.count)! + 1{
            return 60
        }
        else if indexPath.row == (arrayOfTextFields?.count)! + 1{
            return UITableViewAutomaticDimension
        }
        else if indexPath.row == (arrayOfTextFields?.count)! + 2{
            return mediaArrayCount > 0 ? 100 : 0
        }
        else if indexPath.row == (arrayOfTextFields?.count)! + 3{
            return (updatedHeight != nil) ? updatedHeight! : DEFAULT_HEIGHT_ATTACHMENT_VIEW
           // return UITableViewAutomaticDimension
        }
        else{
            return 55
        }
    }
    
    func calculateMediaArray(){
        mediaArrayCount = (currentUser?.pprest.count)! + (currentUser?.pfle.count)! + (currentUser?.pphot.count)!
        if !(currentUser?.plnk?.isEmpty)!{
            mediaArrayCount = mediaArrayCount + (currentUser?.plnk?.components(separatedBy: ","))!.count
        }
        if !(currentUser?.pvdeo?.isEmpty)!{
            mediaArrayCount = mediaArrayCount + (currentUser?.pvdeo?.components(separatedBy: ","))!.count
        }
    }
    
    // Fetching Of User Profile
    
    func showProfileResult(message:String?, status:Bool){
        STOP_LOADING_VIEW()
        if status == true{
            if let currentProfile = ProfileDetailRealmModel.fetchProfile(){
                currentUser = currentProfile
                getTextFieldArray()
            }
            calculateMediaArray()
            tblProfile.reloadData()
            tblProfile.scrollToFirstRow()
        }
        else{
            displayToast((message != nil) ? message! : UNKNOWN_ERROR_MSG )
        }
    }
    
    func profileUpdateResponse(response : NewPost.Response){
        displayToast((response.message != nil) ? response.message! : UNKNOWN_ERROR_MSG)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
            if response.status == true{
                self.isProfileUpdated = true
                self.output.fetchUserProfile()
                self.attachmentController.removeAllElements()
            }
        })
    }
    
    // Attachment Controller Delegate Methods
    
    func updateHeightOfParentController(height: CGFloat) {
        isImageChanged = true
        tblProfile.beginUpdates()
        //Taking a constant
        updatedHeight = DEFAULT_HEIGHT_ATTACHMENT_VIEW + height
        tblProfile.layoutIfNeeded()
        tblProfile.endUpdates()
    }
   
    func saveUserProfile(){
       
        var keys = [String]()
        switch paramsDict["puserType"] as! String{
        case PROFILE_TYPE.ORGANIZATION.rawValue :
            keys = ["uname","ptitle","puserType","pphone","paddress","pcountry","puserstate","plocation","ppostalcode","psummary"]
        case PROFILE_TYPE.PROFESSIONAL.rawValue ,              PROFILE_TYPE.STUDENT.rawValue,
            PROFILE_TYPE.OTHERS.rawValue:
            keys = ["uname","ptitle","puserType","pphone","pdob","pgender","paddress","pcountry","puserstate","plocation","ppostalcode","psummary"]
        default:
            keys = ["uname","ptitle","puserType","pphone","pdob","pgender","paddress","pcountry","puserstate","plocation","ppostalcode","psummary"]
        }
        
        var tempDictArray = arrayOfTextFields
        tempDictArray?.remove(at: 3)
        //        tempDictArray = tempDictArray?.remove(at: 0)
        for (index, element) in (tempDictArray?.enumerated())! {
            print("Item \(index): \(element)")
            if element.placeholderText == "Date Of Birth"{
                if !birthDate.isEmpty{
                    paramsDict[keys[index]] = birthDate
                }else{
                    paramsDict[keys[index]] = currentUser?.pdob
                }
                
            }
            else if element.placeholderText == "Phone Number"{
                if !(element.textfieldText?.isEmpty)!{
                    if !countryCode.isEmpty{
                        let codeToSend = countryCode.dropFirst()
                        print("Code to send-->",codeToSend)
                        paramsDict[keys[index]] = "\(codeToSend ??? ""):\(element.textfieldText ??? "")"
                        print("Phone number-->",paramsDict[keys[index]])
                    }else{
                        paramsDict[keys[index]] = element.textfieldText
                    }
                }else{
                    displayToast("Please enter phone number")
                    return
                }
            }else{
                paramsDict[keys[index]] = element.textfieldText
            }
            
        }
        var request = NewPost.Request()
        request.poimage = attachmentController.imageNamesArray
        let linkString = attachmentController.linksArray.map{$0.fileName!}.joined(separator: ",")
        let youTubeString = attachmentController.youTubeLinksArray.map{$0.fileName!}.joined(separator: ",")
        
        paramsDict["plnk"] = linkString
        paramsDict["pvdeo"] = youTubeString
        request.poallvid = attachmentController.videoArray
        request.podocu = attachmentController.fileArray
        request.poppt = attachmentController.pptArray
        output.updateUserProfile(paramsDict: paramsDict,profileImage: userProfileImage,request: request)
    }
}


extension ProfileViewController : UITextFieldDelegate , UITextViewDelegate
{
    
    //MARK: UITextFieldDelegate
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField.accessibilityLabel{
        case "Name":
            paramsDict["uname"] = textField.text
            let selectedIndexPath: IndexPath = IndexPath(row: textField.tag, section: 0)
            arrayOfTextFields![selectedIndexPath.row].textfieldText = textField.text
        case "Job Title":
            paramsDict["ptitle"] = textField.text
            let selectedIndexPath: IndexPath = IndexPath(row: textField.tag, section: 0)
            arrayOfTextFields![selectedIndexPath.row].textfieldText = textField.text
        case "Profession":
            paramsDict["puserType"] = textField.text
            let selectedIndexPath: IndexPath = IndexPath(row: textField.tag, section: 0)
            arrayOfTextFields![selectedIndexPath.row].textfieldText = textField.text
        case "Phone Number":
            paramsDict["pphone"] = textField.text
            let selectedIndexPath: IndexPath = IndexPath(row: textField.tag, section: 0)
            arrayOfTextFields![selectedIndexPath.row].textfieldText = textField.text
        case "Address":
            paramsDict["paddress"] = textField.text
            let selectedIndexPath: IndexPath = IndexPath(row: textField.tag, section: 0)
            arrayOfTextFields![selectedIndexPath.row].textfieldText = textField.text
        case "Postal Code":
            paramsDict["ppostalcode"] = textField.text
            let selectedIndexPath: IndexPath = IndexPath(row: textField.tag, section: 0)
            arrayOfTextFields![selectedIndexPath.row].textfieldText = textField.text
        default:
            let selectedIndexPath: IndexPath = IndexPath(row: textField.tag, section: 0)
            arrayOfTextFields![selectedIndexPath.row].textfieldText = textField.text
            
        }
        
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField.accessibilityLabel == "Country" {
            fetchCountries(textField: textField)
            self.view.endEditing(true)
            return false
        }
        
        else if textField.accessibilityLabel == "State"{
            fetchStates(textField: textField)
            self.view.endEditing(true)
            return false
            
        }
        
        else if textField.accessibilityLabel == "Location"{
            fetchCities(textField: textField)
            self.view.endEditing(true)
            return false
            
        }
        
        else if textField.accessibilityLabel == "Profession" {
            currentTextField = textField
            router.navigateToPopUpController(sender: textField, optionsArray: [PROFILE_TYPE.PROFESSIONAL.rawValue,PROFILE_TYPE.ORGANIZATION.rawValue,PROFILE_TYPE.STUDENT.rawValue,PROFILE_TYPE.OTHERS.rawValue])
            //self.view.endEditing(true)
            //return false
        }
        else if textField.accessibilityLabel == "Gender"{
            currentTextField = textField
            router.navigateToPopUpController(sender: textField, optionsArray: ["Male","Female","Other"])
//            self.view.endEditing(true)
//            return false
        }
        else if textField.accessibilityLabel == "Date Of Birth"{
            currentTextField = textField
            addDateOfBirthAlert()
            self.view.endEditing(true)
            return false
            
            //self.view.endEditing(true)
            //return false
        }
        else{
            return true
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.08) {
            self.dismissKeyboard()
        }
        return true
        
    }
    
    
    
    func addDateOfBirthAlert(){
        let alert = UIAlertController(style: .alert, title: "Select date of birth")
        alert.addDatePicker(mode: .date, date: Date()) { (date) in
            print("The date selected is -->", date)
            if let date = Utils.convertDateToString(dateStr: date){
                //  print(Utils.generalDateWithString(date,newDateFormat:"dd MMMM YYYY"))
                self.currentTextField?.text  = Utils.generalDateWithString(date,newDateFormat:"dd MMMM YYYY")
                
                let selectedIndexPath: IndexPath = IndexPath(row: self.currentTextField!.tag, section: 0)
                self.arrayOfTextFields![selectedIndexPath.row].textfieldText = self.currentTextField?.text
               
                self.userDateOfBirth = self.currentTextField?.text ?? ""
                self.birthDate = Utils.generalDateWithString(date,newDateFormat:"YYYY-MM-dd")
                
            }
            
        }
        
        alert.addAction(title: "OK", style: .cancel)
        alert.show()
    }
    
    func optionSelected(option : String?){
        // print("job type--->", option)
        currentTextField?.text = option
        if currentTextField?.accessibilityLabel == "Gender"{
            let selectedIndexPath: IndexPath = IndexPath(row: currentTextField!.tag, section: 0)
            self.arrayOfTextFields![selectedIndexPath.row].textfieldText = currentTextField?.text
            
        }else if currentTextField?.accessibilityLabel == "Profession"{
           
            var dob = ""
            let selectedIndexPath: IndexPath = IndexPath(row: currentTextField!.tag, section: 0)
            self.arrayOfTextFields![selectedIndexPath.row].textfieldText = currentTextField?.text
            paramsDict["puserType"] = currentTextField?.text
            generateTextFields(typeOfUser: paramsDict["puserType"]!, dateOfBirth: userDateOfBirth)
            tblProfile.reloadData()//MARK: UITextViewDelegate
            
        }else
        {
            if currentTextField == nil{
                CURRENT_APP_LANGUAGE = arrayLangInfo.filter{$0.name == option}[0].code
                SELECTED_LANGUAGE = arrayLangInfo.filter{$0.name == option}[0].name
                translateToCurrentSelectedLanguage()
            }
            
        }
        

    }
    
    func textViewDidChange(_ textView: UITextView) {
        textView.updateTextViewPlacholder()
        textUpdated = true
        paramsDict["psummary"] = textView.text
        tblProfile.beginUpdates()
        tblProfile.layoutIfNeeded()
        tblProfile.endUpdates()
    }
    
    func fetchCountries(textField : UITextField?=nil,countryCodeBtn : UIButton? = nil){
        let worker = RegistrationWorker()
        worker.fetchCountries { [weak self](countryResponse) in
            if countryResponse.datas != nil{
                self?.countryArray.removeAll()
                for country in countryResponse.datas!{
                    self?.countryArray.append(country.name!)
                }
                DispatchQueue.main.async {
                    if let inputTxtField = textField{
                        self?.showCountryPicker(textField: inputTxtField,title:"Country",currentArray: (self?.countryArray)!)
                    }
                    if let inputBtn = countryCodeBtn{
                        self?.showCountryCodePicker(currentArray: (self?.countryArray)!,countryCodeBtn:inputBtn)
                    }
                    
                }
            }
        }
    }
    
    
    func fetchCountryCodes(countryCodeBtn: UIButton?){
        let worker = RegistrationWorker()
        worker.fetchCountryCodes { [weak self](countryResponse) in
            if countryResponse.datas != nil{
                self?.countryCodeArray.removeAll()
                for country in countryResponse.datas!{
//                    self?.countryCodeArray.append(country.name!)
                    self?.countryCodeArray.append("\(country.name ?? "") (\(country.phonecode ?? "") )")
                }
                DispatchQueue.main.async {
                   
                    if let inputBtn = countryCodeBtn{
                        self?.showCountryCodePicker(currentArray: (self?.countryCodeArray)!,countryCodeBtn:inputBtn)
                    }
                    
                }
            }
        }
    }
    
    func fetchStates(textField : UITextField){
        let worker = RegistrationWorker()
        
        for (_, element) in (arrayOfTextFields?.enumerated())! {
            if element.placeholderText == "Country"{
                if let countryName = element.textfieldText ,!countryName.isEmpty{
                    worker.fetchStates(countryName: countryName) {[weak self] (stateResponse) in
                        if stateResponse.datas != nil{
                            self?.stateArray.removeAll()
                            for state in stateResponse.datas!{
                                self?.stateArray.append(state.name!)
                            }
                            DispatchQueue.main.async {
                                self?.showCountryPicker(textField: textField,title:"State",currentArray:(self?.stateArray)!)
                            }
                        }
                        
                    }
                }else{
                    self.displayToast(CHOOSE_COUNTRY_TOAST)
                }
                
            }
        }
    }
    
    func fetchCities(textField : UITextField){
        let worker = RegistrationWorker()
        for (_, element) in (arrayOfTextFields?.enumerated())! {
            if element.placeholderText == "State"{
                
                if let stateName = element.textfieldText , !stateName.isEmpty{
                    worker.fetchCities(stateName: stateName) {[weak self] (cityResponse) in
                        
                        if cityResponse.datas != nil{
                            self?.cityArray.removeAll()
                            for city in cityResponse.datas!{
                                self?.cityArray.append(city.name!)
                            }
                            DispatchQueue.main.async {
                                self?.showCountryPicker(textField: textField,title:"City",currentArray:(self?.cityArray)!)
                            }
                        }
                        
                    }
                }
                else{
                    self.displayToast(CHOOSE_STATE_TOAST)
                }
                
            }
        }
    }
    
    func showCountryCodePicker(currentArray : [String],countryCodeBtn: UIButton)
    {
        ActionSheetStringPicker.show(withTitle: "Country Code", rows: currentArray, initialSelection: 0, doneBlock: {[unowned self] (key, index, value) in
           // textField.text  = value as? String
            if let countryCodeValue = value{
                let code = (countryCodeValue as? String)?.slice(from: "(", to: ")")
                //let currentCode = code
                self.countryCode = "+\(code ?? "")"
                countryCodeBtn.setTitle(self.countryCode, for: .normal)
                
                self.paramsDict["countryCode"] = self.countryCode
            }
            
            self.tblProfile.reloadData()
//            let selectedIndexPath: IndexPath = IndexPath(row: countryCodeBtn.tag, section: 0)
//            self.arrayOfTextFields![selectedIndexPath.row].textfieldText = textField.text
//                self.countryOfUser = textField.text!
//                for (index, element) in (self.arrayOfTextFields?.enumerated())! {
//                    if element.placeholderText == "State" || element.placeholderText == "Location"{
//                        self.cityOfUser = ""
//                        self.locationOfUser = ""
//                        self.arrayOfTextFields![index].textfieldText = ""
//                    }
//                    self.tblProfile.reloadData()
//
//                }
            
            
            
            
            }, cancel: { (picker) in
                
        }, origin: self.view)
    }
    
    
    
    func showCountryPicker(textField : UITextField , title : String , currentArray : [String]){
        ActionSheetStringPicker.show(withTitle: title, rows: currentArray, initialSelection: 0, doneBlock: {[unowned self] (key, index, value) in
            textField.text  = value as? String
            let selectedIndexPath: IndexPath = IndexPath(row: textField.tag, section: 0)
            self.arrayOfTextFields![selectedIndexPath.row].textfieldText = textField.text
            switch title{
            case "Country":
                self.countryOfUser = textField.text!
                for (index, element) in (self.arrayOfTextFields?.enumerated())! {
                    if element.placeholderText == "State" || element.placeholderText == "Location"{
                        self.cityOfUser = ""
                        self.locationOfUser = ""
                        self.arrayOfTextFields![index].textfieldText = ""
                    }
                    self.tblProfile.reloadData()
                    
                }
                
            case "State":
                self.cityOfUser = textField.text!
                for (index, element) in (self.arrayOfTextFields?.enumerated())! {
                    if element.placeholderText == "Location"{
                        self.arrayOfTextFields![index].textfieldText = ""
                    }
                }
                self.tblProfile.reloadData()
            case "City":
                self.locationOfUser = textField.text!
            default:
                print("")
            }
            
            }, cancel: { (picker) in
                
        }, origin: self.view)
    }
}

extension ProfileViewController : UICollectionViewDataSource , UICollectionViewDelegate,UICollectionViewDelegateFlowLayout , DeleteCurrentAttatchment{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // print("CURRENT USER IS ---->",currentUser)
        //print("CURRENT USER PHOTOS IS ---->",currentUser?.pphot)
        
        switch section{
        case 0:
            return (currentUser?.pphot.count)!
        case 1:
            return (currentUser?.pfle.count)!
        case 2:
            return (currentUser?.pprest.count)!
        case 3:
            return (currentUser?.plnk?.isEmpty)! ? 0 : (currentUser?.plnk?.components(separatedBy: ","))!.count
        case 4:
            return (currentUser?.pvdeo?.isEmpty)! ? 0 : (currentUser?.pvdeo?.components(separatedBy: ","))!.count
        default:
            return 0
        }
    }
    
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        if mediaArrayCount > 0{
            return 5
        }else{
            return 0
        }
        
    }
    
   
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "homeDetailsCell", for: indexPath) as! HomeDetailCollectionViewCell
        cell.imgViewMedia.image = nil
        cell.delegate = self
        cell.currentIndexPath = indexPath
        
        
        switch indexPath.section{
        case 0:
            cell.btnDelete.isHidden = false
            cell.setUpViewForImage(url:currentUser?.pphot[indexPath.row])
        case 1:
            cell.btnDelete.isHidden = false
            cell.setUpViewForImage(isFile : true)
        case 2:
            cell.btnDelete.isHidden = false
            cell.setUpViewForImage(isPresentation : true)
        case 3:
            cell.btnDelete.isHidden = true
            cell.setUpViewForLink(link :(currentUser?.plnk?.components(separatedBy: ","))![indexPath.row])
        case 4:
            cell.btnDelete.isHidden = true
            cell.setUpViewForYouTubeVideo(youTubeVideoUrl: (currentUser?.pvdeo?.components(separatedBy: ","))![indexPath.row])
        default:
            print("test")
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        if indexPath.section == 3{
//             return CGSize.init(width: collectionView.frame.size.width , height: collectionView.frame.size.height)
//        }else{
            return CGSize.init(width: collectionView.frame.size.width / 3, height: collectionView.frame.size.height)
     //   }
       
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        isImageChanged = true
        switch indexPath.section{
        case 0:
            let photoController = AppToolbarController(rootViewController: PhotoViewController(imagesArray:(currentUser?.pphot)!,index: indexPath.row))
            present(photoController, animated: true, completion: nil)
        case 1:
            showAttatchmentView(mediaUrl:(currentUser?.pfle[indexPath.row])!)
        case 2:
            showAttatchmentView(mediaUrl:(currentUser?.pprest[indexPath.row])!)
        case 3:
            openLinks(linkUrl:(currentUser?.plnk?.components(separatedBy: ","))![indexPath.row])
        case 4:
            print("You Tube Video")
        default :
            print("Default Statement")
            
        }
    }
    
    func deleteCurrentAttatchment(sender : UIButton , indexPath : IndexPath?)
    {
        
        print("The attachment is -->",sender.tag)
        let currentSection = indexPath?.section
        var fileType = ""
        var fileName = ""
        switch currentSection   {
        case 0:
            fileName = (currentUser?.pphot[(indexPath?.row)!])!
            fileType = "photo"
        case 1:
            fileName = (currentUser?.pfle[(indexPath?.row)!])!
            fileType = "file"
        case 2:
            fileName = (currentUser?.pprest[(indexPath?.row)!])!
            fileType = "presentation"
        default: return
        }
        showCustomAlert(CONFIRMATION_ALERT, message: DELETE_ATTATCHMENT, okButtonTitle: UIAlertActionTitle.DELETE.rawValue, cancelButtonTitle: UIAlertActionTitle.CANCEL.rawValue) { [unowned self](action) in
//            if let currentCell = self.collection.cellForItem(at: indexPath!) as? HomeDetailCollectionViewCell{
//                //currentCell.isSelected = false
//                currentCell.backgroundColor = UIColor.white
//            }
            switch action{
            case .DELETE:
                self.deleteAttatchment(fileType : fileType , sender : indexPath! , fileName : fileName)
            case .CANCEL:
                print("Dismiss Alert")
            default:
                print("Dismiss Alert")
            }
        }
    }
    
    func deleteAttatchment(fileType : String , sender : IndexPath , fileName:String?){
        let worker = ProfileWorker()
        var fileName = URL(string:(fileName?.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!)!)!.lastPathComponent
        fileName =  Data(fileName.utf8).base64EncodedString()
        worker.deleteFile(fileType: fileType, fileTobeDeleted:fileName, completionHandler: { [weak self] (message, responseModel) in
            if let response = responseModel{
                DispatchQueue.main.async {
                    //                    self?.previousFilesArray.remove(at:sender.tag)
                    //                    self?.collectionViewPreviousFiles.reloadData()
                    self?.displayToast((response.message)!)
                    if response.status == true{
                        if self?.currentCell?.collectionViewProfileMedia != nil{
                            DispatchQueue.main.async {
                                let realm = RealmDBManager.sharedManager.realm
                                realm?.beginWrite()
                                switch sender.section{
                                case 0 :
                                    self?.currentUser?.pphot.remove(at: sender.row)
                                case 1:
                                    self?.currentUser?.pfle.remove(at: sender.row)
                                case 2:
                                    self?.currentUser?.pprest.remove(at: sender.row)
                                default:
                                    break
                                }
                                try! realm?.commitWrite()
                                //self?.currentCell?.collectionViewProfileMedia.beginUpdates()
                                // self?.currentCell?.collectionViewProfileMedia.reloadData()
                                self?.mediaArrayCount = (self?.mediaArrayCount)!-1
                                //                                self?.currentCell?.collectionViewProfileMedia.reloadSections(IndexSet(integer: sender.section))
                                self?.currentCell?.collectionViewProfileMedia.reloadData()
                                
                                self?.tblProfile.reloadRows(at: [IndexPath(row: (self?.arrayOfTextFields?.count)! + 2, section: 0)], with: .automatic)
                                //self?.currentCell?.collectionViewProfileMedia.reloadSections(IndexSet(integer: sender.section))
                                //self?.currentCell?.collectionViewProfileMedia.endUpdates()
                            }
                        }
                    }
                    
                }
                
            }else{
                self?.displayToast(message!)
            }
        })
    }
}

