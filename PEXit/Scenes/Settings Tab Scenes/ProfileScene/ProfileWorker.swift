//
//  ProfileWorker.swift
//  PEXit
//
//  Created by Apple on 29/08/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation
import ObjectMapper

class ProfileWorker{
    func fetchUserProfile(completionHandler:@escaping(_ message:String?,_ status : Bool) -> Void){
        
        START_LOADING_VIEW()
        ServiceManager.methodType(requestType: GET_REQUEST, url: GET_PROFILE, params: nil, paramsData: nil,completion: { (response,_ responseData, statusCode) in
            STOP_LOADING_VIEW()
            if response != nil{
                
                do{
                    print("Response issss ---->",response)
                    
                    if (response as! [String : AnyObject])["status"] as? Bool == true{
                        
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                        
                        do{
                            
                            let json = try JSONSerialization.jsonObject(with: responseData!, options: []) as? Dictionary<String , Any>
                            let profileData = json!["datas"] as! [String:Any]
                            ProfileDetailRealmModel.deleteAll()
                            EducationModel.deleteAll()
                            HonorsModel.deleteAll()
                            ExperienceModel.deleteAll()
                            CompanyModel.deleteAll()
                            let profileModel = Mapper<ProfileDetailRealmModel>().map(JSONObject:profileData["profile"] as! [String:Any])
                            RealmDBManager.sharedManager.addOrUpdateObject(object: profileModel!)
                            
                            let experienceList = profileData["experience"] as! [[String:Any]]
                                    for experience in experienceList{
                                            let experienceModel = Mapper<ExperienceModel>().map(JSONObject:experience )
                                            RealmDBManager.sharedManager.addOrUpdateObject(object: experienceModel!)
                            }
                            
                            let educationList = profileData["education"] as! [[String:Any]]
                            for education in educationList{
                                let educationModel = Mapper<EducationModel>().map(JSONObject:education)
                                RealmDBManager.sharedManager.addOrUpdateObject(object: educationModel!)
                            }
                            
                            let honorsList = profileData["honors"] as! [[String:Any]]
                            for honors in honorsList{
                                let honorsModel = Mapper<HonorsModel>().map(JSONObject:honors)
                                RealmDBManager.sharedManager.addOrUpdateObject(object: honorsModel!)
                            }
                            let companyModel = Mapper<CompanyModel>().map(JSONObject:profileData["company"] as! [String:Any])
                            RealmDBManager.sharedManager.addOrUpdateObject(object: companyModel!)

                            
                            completionHandler(nil,true)
                            
                        }//end of do
                        catch{
                            
                            print(error)
                        }
                    })
                    }
                    
                    
                }catch{
                    STOP_LOADING_VIEW()
                    print("error")
                }
            }
            
            
        }) { (response, statusCode) in
            STOP_LOADING_VIEW()
        }
    }
    
    
    func saveUserProfile(params:[String:Any],profileImage:Data? = nil ,request: NewPost.Request,completionHandler:@escaping(_ responseModel : NewPost.Response) -> Void){
            START_LOADING_VIEW()
        AlamofireManager.shared().updateUserProfile(apiUrl: PROFILE_UPDATE,userProfileImage: profileImage,imageData: request.poimage,videoData: request.poallvid,fileData: request.podocu,pptData: request.poppt, parameters: params) { (response, error) in
                STOP_LOADING_VIEW()
                if response != nil{
                    if !(((response!["errors"] as? String)?.isEmpty)!) || !((response!["virus"] as? String)?.isEmpty)!{
                        Utils.showErrorFilesAlert(errorFiles : response!["errors"] as? String,corruptFiles : response!["virus"] as? String)
                    }
                    let decoder = JSONDecoder()
                    do{
                        let jsonData = try JSONSerialization.data(withJSONObject: response, options: .prettyPrinted)
                        let reqJSONStr = String(data: jsonData, encoding: .utf8)
                        let data = reqJSONStr?.data(using: .utf8)
                        if let data = data{
                            let responsePost = try decoder.decode(NewPost.Response.self, from: data)
                            completionHandler(responsePost)
                        }
                       
                    }catch{
                        STOP_LOADING_VIEW()
                        print("error")
                    }
                }else{
                    
                    STOP_LOADING_VIEW()
//                    setToastStatus(flag: false)
                    KEY_WINDOW?.makeToast(error?.localizedDescription)
                }
                
                
            }
    }
    
    func deleteFile(fileType:String,fileTobeDeleted:String,completionHandler:@escaping(_ message:String?,_ responseModel : CommonModel.BaseModel?) -> Void){
        START_LOADING_VIEW()
        let completeURL =  DELETE_PROFILE_ATTATCHMENT(fileType: fileType, fileName: fileTobeDeleted)
        ServiceManager.methodType(requestType: DELETE_REQUEST, url: completeURL, params: nil, paramsData: nil,completion: { (response,_ responseData, statusCode) in
            STOP_LOADING_VIEW()
            let decoder = JSONDecoder()
            do{
                if let data = responseData{
                    let deleteFileResponse = try decoder.decode(CommonModel.BaseModel.self, from: data)
                    completionHandler("",deleteFileResponse)
                }
               
            }catch{
                completionHandler((response as! [String : Any])["message"] as? String, nil)
                print("error")
            }
            
        }) { (response, statusCode) in
            STOP_LOADING_VIEW()
        }
    }
    func deleteOrganization(orgType:String,idToBeDeleted:Int,completionHandler:@escaping(_ message:String?,_ responseModel : CommonModel.BaseModel?) -> Void){
        START_LOADING_VIEW()
        let completeURL =  DELETE_ORGANIZATION(type: orgType, idOfOrg: idToBeDeleted)
        ServiceManager.methodType(requestType: DELETE_REQUEST, url: completeURL, params: nil, paramsData: nil,completion: { (response,_ responseData, statusCode) in
            STOP_LOADING_VIEW()
            let decoder = JSONDecoder()
            do{
                if let data = responseData{
                    let deleteFileResponse = try decoder.decode( CommonModel.BaseModel.self, from: data)
                    completionHandler("",deleteFileResponse)
                }
                
            }catch{
                completionHandler((response as! [String : Any])["message"] as? String, nil)
                print("error")
            }
            
        }) { (response, statusCode) in
            STOP_LOADING_VIEW()
        }
    }
    
    
}




