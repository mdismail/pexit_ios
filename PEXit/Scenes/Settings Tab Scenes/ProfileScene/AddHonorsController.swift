//
//  AddHonorsController.swift
//  PEXit
//
//  Created by Apple on 05/09/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class AddHonorsController: UIViewController , UITextFieldDelegate , UITextViewDelegate,ItemSelected{
    
    @IBOutlet weak var txtViewDescription: UITextView!
    @IBOutlet weak var txtFieldYear: TJTextField!
    @IBOutlet weak var txtFieldMonth: TJTextField!
    @IBOutlet weak var txtFieldStudy: TJTextField!
    @IBOutlet weak var heightDescriptionConstraint: NSLayoutConstraint!
    var router = WorkEducationRouter()
    var currentTextField : TJTextField?
    var delegate : HeightUpdates?
    var currentHonorsModel : HonorsModel?
    var updatedHonorsModel : HonorsModel?
    var paramsDict = [String : Any]()
    var honorModelId : Int?

    override func viewDidLoad() {
        super.viewDidLoad()
        txtFieldStudy.addPreviousNextDoneOnKeyboard(withTarget: self, previousAction: #selector(self.previousAction(_:)), nextAction: #selector(self.nextAction(_:)), doneAction: #selector(self.doneAction(_:)), shouldShowPlaceholder: true)
        txtViewDescription.addPreviousNextDoneOnKeyboard(withTarget: self, previousAction: #selector(self.previousAction(_:)), nextAction: #selector(self.nextAction(_:)), doneAction: #selector(self.doneAction(_:)), shouldShowPlaceholder: true)
        txtViewDescription.leftAlignTextView()
      //  txtViewDescription.placeholder = textViewPlaceholder
        // Do any additional setup after loading the view.
    }
    
    
    
    @objc func previousAction(_ sender : UITextField!) {
        if txtViewDescription.isFirstResponder{
            txtFieldYear.becomeFirstResponder()
        }
       
    }
    
    @objc func nextAction(_ sender : UITextField!) {
        if (txtFieldStudy.isFirstResponder){
            txtFieldMonth.becomeFirstResponder()
        }
        
    }
    
    @objc func doneAction(_ sender : UITextField!) {
        self.view.endEditing(true)
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        let textField = textField as! TJTextField
        currentTextField = textField
        switch textField{
        case txtFieldYear:
            self.dismissKeyboard()
            router.navigateToPopUpController(sender: textField, optionsArray: Utils.getListOfYears(addCount: FROM_YEAR_CONSTANT),controller:self)
        case txtFieldMonth:
            self.dismissKeyboard()
            router.navigateToPopUpController(sender: textField, optionsArray: Utils.getAllMonths(),controller:self)
        default:
            return true
        }
//        DispatchQueue.main.asyncAfter(deadline: .now() + 0.08) {
//            self.dismissKeyboard()
//        }
        return false
        
    }
    
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField{
        case txtFieldStudy:
            paramsDict["phontitle"] = textField.text
            txtFieldMonth.becomeFirstResponder()
        default:
            print("Test")
        }
        
    }
    
    //MARK: - Item Selected Delegate
    
    func optionSelected(option: String?) {
        currentTextField?.text = option
        switch currentTextField{
        case txtFieldYear:
            paramsDict["phonyear"] = Int(option!)
        case txtFieldMonth:
            //          paramsDict["phonmonth"] = option
            let dateFormatter = DateFormatter()
            paramsDict["phonmonth"] = dateFormatter.getAllMonths().index(of: option!)
            
        default:
            break
        }
    }
    
    func calculateTxtHeight(){
        let size = txtViewDescription.sizeThatFits(CGSize(width: txtViewDescription.frame.size.width, height: CGFloat.greatestFiniteMagnitude))
        if size.height != heightDescriptionConstraint.constant {
            heightDescriptionConstraint.constant = size.height
            delegate?.updateHeight(height: size.height)
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
       // textView.updateTextViewPlacholder()
//        let size = textView.sizeThatFits(CGSize(width: textView.frame.size.width, height: CGFloat.greatestFiniteMagnitude))
//        if size.height != heightDescriptionConstraint.constant {
//            heightDescriptionConstraint.constant = size.height
//            delegate?.updateHeight(height: size.height)
//        }
        paramsDict["phondesc"] = textView.text
        calculateTxtHeight()
    }
    
    func setHonorsDataForEdit(currentModel : HonorsModel){
        if currentHonorsModel == nil || (currentHonorsModel != updatedHonorsModel && currentHonorsModel != nil){
            currentHonorsModel = currentModel
            paramsDict["phontitle"] = currentHonorsModel?.phontitle
            paramsDict["phonyear"] = currentHonorsModel?.phonyear
            paramsDict["phondesc"] = currentHonorsModel?.phondesc
            paramsDict["phonmonth"] = currentHonorsModel?.phonmonth
        }
        if !(currentModel.isInvalidated){
            honorModelId = currentModel.phonid
        }
        txtFieldStudy.text = paramsDict["phontitle"] as? String
        txtFieldYear.text = (paramsDict["phonyear"] as? Int).map(String.init) ?? ""
        txtViewDescription.text = paramsDict["phondesc"] as! String
        calculateTxtHeight()
       // txtViewDescription.updateTextViewPlacholder()
        let dateformatter = DateFormatter()
        txtFieldMonth.text = "\(dateformatter.getMonthName(monthNumber: (paramsDict["phonmonth"] as? Int)! - 1))"
    }
    
    func clearData(){
        if currentHonorsModel != nil{
            currentHonorsModel = nil
            addNewClicked()
        }
        
    }
    
    func addNewClicked(){
        if txtFieldStudy != nil{
            txtFieldStudy.text = ""
            txtFieldYear.text = ""
            txtFieldMonth.text = ""
            txtViewDescription.text = ""
            currentHonorsModel = nil
            honorModelId = nil
            heightDescriptionConstraint.constant = 30
            txtViewDescription.updateTextViewPlacholder()
        }
        
    }
    
    func fetchHonorsData() -> WorkEducationRequestModel.AddHonorsModel?{
        if Utils.validateFormAndShowToastWithRequiredFields([txtFieldStudy,txtFieldMonth,txtFieldYear]){
            var requestModel = WorkEducationRequestModel.AddHonorsModel()
            requestModel.hons_title = txtFieldStudy.text
            requestModel.hons_desc = txtViewDescription.text
            let dateFormatter = DateFormatter()
            let monthNumber = (dateFormatter.getAllMonths().index(of: txtFieldMonth.text!))!+1
            requestModel.honrmnth = "\(monthNumber)"
            requestModel.honryr = txtFieldYear.text
            if currentHonorsModel != nil{
                requestModel.isEdit = true
                requestModel.currentModelId = honorModelId!
            }
            return requestModel
        }
        return nil
        
    }
    
}
