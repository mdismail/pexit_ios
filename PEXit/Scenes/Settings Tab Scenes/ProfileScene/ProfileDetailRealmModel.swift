//
//  ProfileDetailRealmModel.swift
//  PEXit
//
//  Created by Apple on 29/08/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation
import Realm
import RealmSwift
import ObjectMapper

class ProfileDetailRealmModel: Object, Mappable {
    
    @objc dynamic var pid: Int = Int()
    @objc dynamic var ptitle: String?
    @objc dynamic var puserType: String?
    @objc dynamic var pphone: String?
    @objc dynamic var pdob: String?
    @objc dynamic var pgender: String?
    @objc dynamic var paddress:String?
    @objc dynamic var pcountry:String?
    @objc dynamic var puserstate: String?
    @objc dynamic var plocation: String?
    @objc dynamic var ppostalcode:String?
    @objc dynamic var psummary:String?
    @objc dynamic var uid:String?
    @objc dynamic var uname: String?
    @objc dynamic var uemail:String?
    @objc dynamic var pimage:String?
    @objc dynamic var plnk:String?
    @objc dynamic var pvdeo:String?
    @objc dynamic var pcode:String?
    
   
    var pprest: [String] {
        get {
            return _pprest.map { $0.stringValue }
        } set {
            _pprest.removeAll()
            _pprest.append(objectsIn: newValue.map({ RealmString(value: [$0]) }))
        }
    }
    
    private let _pprest = List<RealmString>()
    
    var pphot: [String] {
        get {
            return _pphot.map { $0.stringValue }
        } set {
            _pphot.removeAll()
            _pphot.append(objectsIn: newValue.map({ RealmString(value: [$0]) }))
        }
    }
    
    private let _pphot = List<RealmString>()
    
    var pfle: [String] {
        get {
            return _pfle.map { $0.stringValue }
        } set {
            _pfle.removeAll()
            _pfle.append(objectsIn: newValue.map({ RealmString(value: [$0]) }))
        }
    }
    
    private let _pfle = List<RealmString>()
    
    override static func primaryKey() -> String? {
        return "uid"
    }
    convenience required init?(map: Map) {
        self.init()
    }

    func mapping(map: Map) {
        pid <- map["pid"]
        ptitle <- map["ptitle"]
        puserType <- map["puserType"]
        pphone <- map["pphone"]
        pdob <- map["pdob"]
        pgender <- map["pgender"]
        paddress <- map["paddress"]
        pcountry <- map["pcountry"]
        puserstate <- map["puserstate"]
        plocation <- map["plocation"]
        ppostalcode <- map["ppostalcode"]
        psummary <- map["psummary"]
        uid <- map["uid"]
        uname <- map["uname"]
        uemail <- map["uemail"]
        pimage <- map["pimage"]
        pfle <- map["pfle"]
        pphot <- map["pphot"]
        pprest <- map["pprest"]
        plnk <- map["plnk"]
        pvdeo <- map["pvdeo"]
        pcode <- map["pcode"]
    }
    
    
    //    MARK: fetch and delete functions
    
    static func fetchProfile() -> ProfileDetailRealmModel?{
        let  profile = RealmDBManager.sharedManager.fetchObject(object: self)
        if profile != nil{
            return profile as! ProfileDetailRealmModel
        }
        return nil
    }

    static func deleteAll(){
        if let realm = RealmDBManager.sharedManager.getRealm() {
            do {
                try realm.write() {
                    realm.delete(realm.objects(ProfileDetailRealmModel.self))
                    realm.refresh()
                }
            } catch (let error as NSError) {

                print("ERROR: Realm: writing to database, ERROR: \(error.localizedDescription)")
            }
        }
    }
    
    static func deleteAll_withUId(Id:String){
        let pred = NSPredicate(format: "uid == %@",Id)
        let objectsToDelete = RealmDBManager.sharedManager.fetchObjectsWithPredicate(object: ProfileDetailRealmModel.self, pred: pred)
        if objectsToDelete != nil && (objectsToDelete?.count)! > 0{


            if let realm = RealmDBManager.sharedManager.getRealm() {
                do {
                    try realm.write() {
                        realm.delete(objectsToDelete!)
                        realm.refresh()
                    }
                } catch (let error as NSError) {

                    print("ERROR: Realm: writing to database, ERROR: \(error.localizedDescription)")
                }
            }
        }
    }
}



class ExperienceModel : Object, Mappable{
    @objc dynamic var pexpid: Int = Int()
    @objc dynamic var pexptitle: String?
    @objc dynamic var pexpcompanyname: String?
    @objc dynamic var pexplocation: String?
    @objc dynamic var pexpfrom_mon: Int = Int()
    @objc dynamic var pexpfrom_yr: Int = Int()
    @objc dynamic var pexpto_mon: Int = Int()
    @objc dynamic var pexpto_yr: Int = Int()
    @objc dynamic var pexpiscurrent: String?
    @objc dynamic var pexpuid:  Int = Int()
    @objc dynamic var pexpdes:String?
    @objc dynamic var pexplnk:String?
    @objc dynamic var pexpvdeo: String?
   
    
    var pexpfle: [String] {
        get {
            return _pexpfle.map { $0.stringValue }
        } set {
            _pexpfle.removeAll()
            _pexpfle.append(objectsIn: newValue.map({ RealmString(value: [$0]) }))
        }
    }
    
    private let _pexpfle = List<RealmString>()
    
    var pexpphot: [String] {
        get {
            return _pexpphot.map { $0.stringValue }
        } set {
            _pexpphot.removeAll()
            _pexpphot.append(objectsIn: newValue.map({ RealmString(value: [$0]) }))
        }
    }
    
    private let _pexpphot = List<RealmString>()
    
    var pexpprest: [String] {
        get {
            return _pexpprest.map { $0.stringValue }
        } set {
            _pexpprest.removeAll()
            _pexpprest.append(objectsIn: newValue.map({ RealmString(value: [$0]) }))
        }
    }
    
    private let _pexpprest = List<RealmString>()
    
    override static func primaryKey() -> String? {
        return "pexpid"
    }
    convenience required init?(map: Map) {
        self.init()
    }
    
    
    func mapping(map: Map) {
        pexpid <- map["pexpid"]
        pexptitle <- map["pexptitle"]
        pexpcompanyname <- map["pexpcompanyname"]
        pexplocation <- map["pexplocation"]
        pexpfrom_mon <- map["pexpfrom_mon"]
        pexpfrom_yr <- map["pexpfrom_yr"]
        pexpto_mon <- map["pexpto_mon"]
        pexpto_yr <- map["pexpto_yr"]
        pexpiscurrent <- map["pexpiscurrent"]
        pexpdes <- map["pexpdes"]
        pexpuid <- map["pexpuid"]
        pexpfle <- map["pexpfle"]
        pexpphot <- map["pexpphot"]
        pexplnk <- map["pexplnk"]
        pexpvdeo <- map["pexpvdeo"]
        pexpprest <- map["pexpprest"]
    }
    
    
    //    MARK: fetch and delete functions
    
   
    static func fetchExperienceModels() -> [ExperienceModel]{
        var  experienceModels = RealmDBManager.sharedManager.fetchObjects(object: self) as! [ExperienceModel]
        return experienceModels
    }
  
    static func deleteAll(){
        if let realm = RealmDBManager.sharedManager.getRealm() {
            do {
                try realm.write() {
                    realm.delete(realm.objects(ExperienceModel.self))
                    realm.refresh()
                }
            } catch (let error as NSError) {
                
                print("ERROR: Realm: writing to database, ERROR: \(error.localizedDescription)")
            }
        }
    }
    
    static func deleteAll_withUId(Id:Int){
        let pred = NSPredicate(format: "pexpid == %d",Id)
        let objectsToDelete = RealmDBManager.sharedManager.fetchObjectsWithPredicate(object: ExperienceModel.self, pred: pred)
        if objectsToDelete != nil && (objectsToDelete?.count)! > 0{
            
            
            if let realm = RealmDBManager.sharedManager.getRealm() {
                do {
                    try realm.write() {
                        realm.delete(objectsToDelete!)
                        realm.refresh()
                    }
                } catch (let error as NSError) {
                    
                    print("ERROR: Realm: writing to database, ERROR: \(error.localizedDescription)")
                }
            }
        }
    }
    
    func initFromGroupModel(groupExperienceModel : GroupExperienceModel) -> ExperienceModel{
        pexptitle = groupExperienceModel.exptitle
        pexpcompanyname = groupExperienceModel.expcompanyname
        pexplocation = groupExperienceModel.explocation
        pexpfrom_mon = groupExperienceModel.expfrom_mon ?? 0
        pexpfrom_yr = groupExperienceModel.expfrom_yr ?? 0
        pexpto_mon = groupExperienceModel.expto_mon ?? 0
        pexpto_yr = groupExperienceModel.expto_yr ?? 0
        pexpuid = groupExperienceModel.expuid ?? 0
        pexpdes = groupExperienceModel.expdes
        
        return self
        
    }

}



class EducationModel : Object, Mappable{
    @objc dynamic var pedupid: Int = Int()
    @objc dynamic var pedupuid: Int = Int()
    @objc dynamic var pedupschoolname: String?
    @objc dynamic var peduschoolfromyear: Int = Int()
    @objc dynamic var peduschooltoyear: Int = Int()
    @objc dynamic var peduschoolDegree: String?
    @objc dynamic var peduschool_act: String?
    @objc dynamic var peduexp_desc:String?
    @objc dynamic var peduschoolstudy:String?
    @objc dynamic var peduschoolgrade: String?
    @objc dynamic var pedulnk: String?
    @objc dynamic var peduvdeo:String?
    @objc dynamic var peduschoolODegree: Bool = false
    @objc dynamic var peduschoolograde: Bool = false
    
    var pedufle: [String] {
        get {
            return _pedufle.map { $0.stringValue }
        } set {
            _pedufle.removeAll()
            _pedufle.append(objectsIn: newValue.map({ RealmString(value: [$0]) }))
        }
    }
    
    private let _pedufle = List<RealmString>()
    
    var peduphot: [String] {
        get {
            return _peduphot.map { $0.stringValue }
        } set {
            _peduphot.removeAll()
            _peduphot.append(objectsIn: newValue.map({ RealmString(value: [$0]) }))
        }
    }
    
    private let _peduphot = List<RealmString>()
    
    var peduprest: [String] {
        get {
            return _peduprest.map { $0.stringValue }
        } set {
            _peduprest.removeAll()
            _peduprest.append(objectsIn: newValue.map({ RealmString(value: [$0]) }))
        }
    }
    
    private let _peduprest = List<RealmString>()
    
    override static func primaryKey() -> String? {
        return "pedupid"
    }
    convenience required init?(map: Map) {
        self.init()
    }
    
    
    func mapping(map: Map) {
        pedupid <- map["pedupid"]
        pedupuid <- map["pedupuid"]
        pedupschoolname <- map["pedupschoolname"]
        peduschoolfromyear <- map["peduschoolfromyear"]
        peduschooltoyear <- map["peduschooltoyear"]
        peduschoolDegree <- map["peduschoolDegree"]
        peduschool_act <- map["peduschool_act"]
        peduexp_desc <- map["peduexp_desc"]
        peduschoolstudy <- map["peduschoolstudy"]
        peduschoolgrade <- map["peduschoolgrade"]
        pedufle <- map["pedufle"]
        peduphot <- map["peduphot"]
        pedulnk <- map["pedulnk"]
        peduvdeo <- map["peduvdeo"]
        peduprest <- map["peduprest"]
        peduschoolODegree <- map["peduschoolODegree"]
        peduschoolograde <- map["peduschoolograde"]
    }
    
    
    //    MARK: fetch and delete functions
    
    static func fetchEducationModels() -> [EducationModel]{
        var  educationModels = RealmDBManager.sharedManager.fetchObjects(object: self) as! [EducationModel]
        return educationModels
    }
    
    static func deleteAll(){
        if let realm = RealmDBManager.sharedManager.getRealm() {
            do {
                try realm.write() {
                    realm.delete(realm.objects(EducationModel.self))
                    realm.refresh()
                }
            } catch (let error as NSError) {
                
                print("ERROR: Realm: writing to database, ERROR: \(error.localizedDescription)")
            }
        }
    }
    
    static func deleteAll_withUId(Id:Int){
        let pred = NSPredicate(format: "pedupid == %d",Id)
        let objectsToDelete = RealmDBManager.sharedManager.fetchObjectsWithPredicate(object: EducationModel.self, pred: pred)
        if objectsToDelete != nil && (objectsToDelete?.count)! > 0{
            
            
            if let realm = RealmDBManager.sharedManager.getRealm() {
                do {
                    try realm.write() {
                        realm.delete(objectsToDelete!)
                        realm.refresh()
                    }
                } catch (let error as NSError) {
                    
                    print("ERROR: Realm: writing to database, ERROR: \(error.localizedDescription)")
                }
            }
        }
    }
    
    
    func initFromGroupModel(groupEducationModel : GroupEducationModel) -> EducationModel{
        pedupid = groupEducationModel.edupid!
        pedupuid = groupEducationModel.edupuid!
        pedupschoolname = groupEducationModel.edupschoolname
        peduschoolfromyear = groupEducationModel.eduschoolfromyear
        peduschooltoyear = groupEducationModel.eduschooltoyear
        peduschoolDegree = groupEducationModel.eduschoolDegree
        peduschool_act = groupEducationModel.eduschool_act
        peduexp_desc = groupEducationModel.eduexp_desc
        peduschoolstudy = groupEducationModel.eduschoolstudy
        peduschoolgrade = groupEducationModel.eduschoolgrade

        return self
       
    }
}



class HonorsModel : Object, Mappable{
    @objc dynamic var phonid: Int = Int()
    @objc dynamic var phontitle: String?
    @objc dynamic var phonuid: Int = Int()
    @objc dynamic var phondesc: String?
    @objc dynamic var phonmonth: Int = Int()
    @objc dynamic var phonyear: Int = Int()
   // @objc dynamic var phonfle:String?
    @objc dynamic var phonlnk:String?
    @objc dynamic var phonvdeo: String?
   
    
    var phonphot: [String] {
        get {
            return _phonphot.map { $0.stringValue }
        } set {
            _phonphot.removeAll()
            _phonphot.append(objectsIn: newValue.map({ RealmString(value: [$0]) }))
        }
    }
    
    private let _phonphot = List<RealmString>()
    
    var phonfle: [String] {
        get {
            return _phonfle.map { $0.stringValue }
        } set {
            _phonfle.removeAll()
            _phonfle.append(objectsIn: newValue.map({ RealmString(value: [$0]) }))
        }
    }
    
    private let _phonfle = List<RealmString>()
    
    var phonprest: [String] {
        get {
            return _phonprest.map { $0.stringValue }
        } set {
            _phonprest.removeAll()
            _phonprest.append(objectsIn: newValue.map({ RealmString(value: [$0]) }))
        }
    }
    
    private let _phonprest = List<RealmString>()
    
    override static func primaryKey() -> String? {
        return "phonid"
    }
    convenience required init?(map: Map) {
        self.init()
    }
    
    
    func mapping(map: Map) {
        phonid <- map["phonid"]
        phonuid <- map["phonuid"]
        phontitle <- map["phontitle"]
        phondesc <- map["phondesc"]
        phonmonth <- map["phonmonth"]
        phonyear <- map["phonyear"]
        phonlnk <- map["phonlnk"]
        phonvdeo <- map["phonvdeo"]
        phonprest <- map["phonprest"]
        phonphot <- map["phonphot"]
        phonfle <- map["phonfle"]
    }
   
    static func fetchHonorsModels() -> [HonorsModel]{
        var  honorsModels = RealmDBManager.sharedManager.fetchObjects(object: self) as! [HonorsModel]
        return honorsModels
    }
    
    static func deleteAll(){
        if let realm = RealmDBManager.sharedManager.getRealm() {
            do {
                try realm.write() {
                    realm.delete(realm.objects(HonorsModel.self))
                    realm.refresh()
                }
            } catch (let error as NSError) {
                
                print("ERROR: Realm: writing to database, ERROR: \(error.localizedDescription)")
            }
        }
    }
    
    static func deleteAll_withUId(Id:Int){
        let pred = NSPredicate(format: "phonid == %d",Id)
        let objectsToDelete = RealmDBManager.sharedManager.fetchObjectsWithPredicate(object: HonorsModel.self, pred: pred)
        if objectsToDelete != nil && (objectsToDelete?.count)! > 0{
            
            
            if let realm = RealmDBManager.sharedManager.getRealm() {
                do {
                    try realm.write() {
                        realm.delete(objectsToDelete!)
                        realm.refresh()
                    }
                } catch (let error as NSError) {
                    
                    print("ERROR: Realm: writing to database, ERROR: \(error.localizedDescription)")
                }
            }
        }
    }
    
    
   
    func initFromGroupModel(groupHonorModel : GroupHonorsModel) -> HonorsModel{
        phonid = groupHonorModel.honid!
        phonuid = groupHonorModel.honuid!
        phontitle = groupHonorModel.hontitle
        phondesc = groupHonorModel.hondesc
        phonmonth = groupHonorModel.honmonth!
        phonyear = groupHonorModel.honyear!
        return self
        
    }

}


class CompanyModel : Object, Mappable{
    @objc dynamic var pcomuid: Int = Int()
    @objc dynamic var pcomcity: String?
    @objc dynamic var pcomid: Int = Int()
    @objc dynamic var pcomcountry:String?
    @objc dynamic var pcomstates: String?
   
    var pcomprofile: [String] {
        get {
            return _pcomprofile.map { $0.stringValue }
        } set {
            _pcomprofile.removeAll()
            _pcomprofile.append(objectsIn: newValue.map({ RealmString(value: [$0]) }))
        }
    }
    
    private let _pcomprofile = List<RealmString>()
    override static func primaryKey() -> String? {
        return "pcomuid"
    }
    convenience required init?(map: Map) {
        self.init()
    }
    
    
    func mapping(map: Map) {
        pcomuid <- map["pcomuid"]
        pcomcity <- map["pcomcity"]
        pcomcountry <- map["pcomcountry"]
        pcomstates <- map["pcomstates"]
        pcomid <- map["pcomid"]
        pcomprofile <- map["pcomprofile"]
       
    }
    
    static func fetchCompanyModels() -> [CompanyModel]{
        var  companyModels = RealmDBManager.sharedManager.fetchObjects(object: self) as! [CompanyModel]
        return companyModels
    }
    
    static func deleteAll(){
        if let realm = RealmDBManager.sharedManager.getRealm() {
            do {
                try realm.write() {
                    realm.delete(realm.objects(CompanyModel.self))
                    realm.refresh()
                }
            } catch (let error as NSError) {
                
                print("ERROR: Realm: writing to database, ERROR: \(error.localizedDescription)")
            }
        }
    }
    
    static func deleteAll_withUId(Id:Int){
        let pred = NSPredicate(format: "pcomid == %d",Id)
        let objectsToDelete = RealmDBManager.sharedManager.fetchObjectsWithPredicate(object: CompanyModel.self, pred: pred)
        if objectsToDelete != nil && (objectsToDelete?.count)! > 0{
            
            
            if let realm = RealmDBManager.sharedManager.getRealm() {
                do {
                    try realm.write() {
                        realm.delete(objectsToDelete!)
                        realm.refresh()
                    }
                } catch (let error as NSError) {
                    
                    print("ERROR: Realm: writing to database, ERROR: \(error.localizedDescription)")
                }
            }
        }
    }
    
    func initFromCompanyModel(groupCompanyModel : GroupCompanyModel) -> CompanyModel{
      //  pcomuid = groupCompanyModel.pcomuid
        pcomcity = groupCompanyModel.pcomcity
        pcomcountry = groupCompanyModel.pcomcountry
        pcomstates = groupCompanyModel.pcomstates
       // pcomid = groupCompanyModel.pcomid
      
        return self
        
    }
    
}





