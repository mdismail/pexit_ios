//
//  ProfilePresenter.swift
//  PEXit
//
//  Created by Apple on 29/08/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation

protocol ProfilePresenterInput {
    func displayResult(message:String?, status:Bool)
    func profileUpdateResult(response : NewPost.Response)
    
}
protocol ProfilePresenterOutput : class{
     func showProfileResult(message:String?, status:Bool)
     func profileUpdateResponse(response : NewPost.Response)
}


class ProfilePresenter : ProfilePresenterInput{
    
  
    weak var profilePresenterOutput : ProfilePresenterOutput!

    func displayResult(message:String?, status:Bool) {
        profilePresenterOutput.showProfileResult(message: message,status: status)
    }
    
    func profileUpdateResult(response : NewPost.Response){
        profilePresenterOutput.profileUpdateResponse(response : response)
    }
}
