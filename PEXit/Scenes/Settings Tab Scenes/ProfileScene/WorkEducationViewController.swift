//
//  WorkEducationViewController.swift
//  PEXit
//
//  Created by Apple on 06/08/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit



protocol WorkEducationViewControllerInput{
    func displayWorkProfileResult(response: NewPost.Response)
}

protocol WorkEducationViewControllerOutput{
    func addExperience(typeOfWorkProfile:WORK_PROFILE_TYPE,requestModel : WorkEducationRequestModel.AddExperienceModel)
    func addEducation(typeOfWorkProfile:WORK_PROFILE_TYPE,requestModel : WorkEducationRequestModel.AddEducationModel)
    func addHonors(typeOfWorkProfile:WORK_PROFILE_TYPE,requestModel : WorkEducationRequestModel.AddHonorsModel)
    func addCompany(typeOfWorkProfile:WORK_PROFILE_TYPE,requestModel : WorkEducationRequestModel.AddCompanyModel)
}


class WorkEducationViewController: UIViewController , UITableViewDataSource , UITableViewDelegate , DeleteOrgModel , HeaderTapped , PassAttatchmentsToController , HeightUpdates{
    var sectionHeaders = [String]()
    @IBOutlet weak var tblWorkEducation: UITableView!
    var userType = ""
    var educationModelArray = [EducationModel]()
    var experienceModelArray = [ExperienceModel]()
    var honorsModelArray = [HonorsModel]()
    var companyModelArray = [CompanyModel]()
    var currentSection : Int?
    var updatedHeight : CGFloat?
    var isEdit = false
    var output : WorkEducationViewControllerOutput!
    var currentCell : WorkEducationTableCell?
    var currentModel : AnyObject?
   // var currentIndexPath : IndexPath?
    var linksArray = [String]()
    var previousUserType = ""
   
    private lazy var honorsController: AddHonorsController = {
        var viewController = storyboard?.instantiateViewController(withIdentifier: "addHonorsController") as! AddHonorsController
        viewController.delegate = self
//        self.add(asChildViewController: viewController)
        return viewController
    }()
    private lazy var educationController: EducationViewController = {
        var viewController = storyboard?.instantiateViewController(withIdentifier: "educationViewController") as! EducationViewController
        viewController.delegate = self
        return viewController
    }()
    private lazy var experienceController: ExperienceViewController = {
        var viewController = storyboard?.instantiateViewController(withIdentifier: "experienceViewController") as! ExperienceViewController
        viewController.delegate = self
        return viewController
    }()
   
    private lazy var attachmentController: AttatchmentViewController = {
        var viewController = storyboard?.instantiateViewController(withIdentifier: "attachmentController") as! AttatchmentViewController
        viewController.delegate = self
        return viewController
    }()
    
    private lazy var companyController: CompanyViewController = {
        var viewController = storyboard?.instantiateViewController(withIdentifier: "companyController") as! CompanyViewController
        viewController.delegate = self
        return viewController
    }()
    
   
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
    }

    override func awakeFromNib(){
        super.awakeFromNib()
        WorkEducationConfigurator.sharedInstance.configure(viewController: self)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let userProfile = ProfileDetailRealmModel.fetchProfile()
        if userProfile != nil{
            userType = (userProfile?.puserType)!
            if userType != previousUserType{
                userTypeChanged()
            }
            previousUserType = (userProfile?.puserType)!
            switch userProfile?.puserType{
            case PROFILE_TYPE.ORGANIZATION.rawValue  :
                sectionHeaders = ["Company","Honors & Rewards"]
            case PROFILE_TYPE.PROFESSIONAL.rawValue,PROFILE_TYPE.NONE.rawValue ,  PROFILE_TYPE.STUDENT.rawValue,  PROFILE_TYPE.OTHERS.rawValue :
                sectionHeaders = ["Experience","Education","Honors & Rewards"]
            default:
                sectionHeaders = ["Experience","Education","Honors & Rewards"]
            }
            fetchCurrentModels()
            
        }
        if currentSection == nil{
            tblWorkEducation.reloadData()
        }
        
    }
    
     deinit{
        cancelClicked()
    }
    
    func fetchCurrentModels(){
        educationModelArray = EducationModel.fetchEducationModels()
        experienceModelArray = ExperienceModel.fetchExperienceModels()
        companyModelArray = CompanyModel.fetchCompanyModels()
        for companyModel in companyModelArray{
            if companyModel.pcomid == 0{
                companyModelArray.remove(companyModel)
            }
        }
        honorsModelArray = HonorsModel.fetchHonorsModels()
    }
    
    func userTypeChanged(){
        currentModel = nil
        attachmentController.currentProfileModel = nil
        attachmentController.workProfileType = ""
        isEdit = false
        currentSection = nil
        removePreviousControllers()
    }
    
    
    func configureView(){
        tblWorkEducation.estimatedRowHeight = 40
        tblWorkEducation.rowHeight = UITableViewAutomaticDimension
        tblWorkEducation.register(UINib(nibName: "WorkEducationTableViewCell", bundle: nil), forCellReuseIdentifier: "workEducationCell")
        let headerNib = UINib.init(nibName: "ProfileHeaderView", bundle: Bundle.main)
        tblWorkEducation.register(headerNib, forHeaderFooterViewReuseIdentifier: "ProfileHeaderView")
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = tblWorkEducation.dequeueReusableHeaderFooterView(withIdentifier: "ProfileHeaderView") as! ProfileHeaderView
        headerView.headerTitle.text = currentSection != nil ? sectionHeaders[currentSection!] : sectionHeaders[section]
        if userType == PROFILE_TYPE.ORGANIZATION.rawValue{
            if companyModelArray.count > 0 && section == 0{
                headerView.btnAddRecord.isHidden = true
            }else{
               headerView.btnAddRecord.isHidden = false
            }
        }else{
            headerView.btnAddRecord.isHidden = false
        }
        headerView.btnAddRecord.tag = section
        headerView.delegate = self
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
       return currentSection != nil ? 1 : sectionHeaders.count
    }
    
    func updateHeightOfParentController(height: CGFloat) {
        if currentSection != nil{
            tblWorkEducation.beginUpdates()
            //Taking a constant
            updatedHeight = DEFAULT_HEIGHT_ATTACHMENT_VIEW + height
            // tblWorkEducation.reloadData()
            tblWorkEducation.layoutIfNeeded()
            tblWorkEducation.endUpdates()
        }else{
            tblWorkEducation.reloadData()
        }
       
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        switch userType{
        case PROFILE_TYPE.ORGANIZATION.rawValue  :
            if currentSection != nil{
                switch currentSection{
                case 0:
//                    if currentModel == nil{
//                        return 1
//                    }
                    return 2// Number of companies will always be 1 only
                case 1:
                    return 3
                default:
                    return 0
                }
            }
                else{
                    switch section{
                    case 0:
                        return companyModelArray.count // Number of companies will always be 1 only
                    case 1:
                        return honorsModelArray.count
                    default:
                        return 0
                    }
                }
           
        case PROFILE_TYPE.PROFESSIONAL.rawValue,PROFILE_TYPE.NONE.rawValue ,  PROFILE_TYPE.STUDENT.rawValue,  PROFILE_TYPE.OTHERS.rawValue :
            switch currentSection != nil ? currentSection : section{
            case 0:
//                return currentSection != nil ? (experienceModelArray.count) + 3 : experienceModelArray.count
                 return currentSection != nil ? 3 : experienceModelArray.count
            case 1:
                 return currentSection != nil ?  3 : educationModelArray.count
//                return currentSection != nil ? (educationModelArray.count) + 3 : educationModelArray.count
            case 2:
                return currentSection != nil ?  3 : honorsModelArray.count
//                return currentSection != nil ? (honorsModelArray.count) + 3 : honorsModelArray.count
            default:
                return 0
            }
        default:
             return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        switch userType{
        case PROFILE_TYPE.ORGANIZATION.rawValue  :
            if currentSection != nil{
                switch currentSection{
                case 0:
                    switch indexPath.row{
                    case indexPath.row where indexPath.row == 0 :
                        return UITableViewAutomaticDimension
//                    case indexPath.row where indexPath.row ==  1 :
//                        return isEdit == true ?  UITableViewAutomaticDimension :  40
                    case indexPath.row where indexPath.row == 1 :
                        return 40
                    default:
                        return UITableViewAutomaticDimension
                    }
                
                case 1:
                    switch indexPath.row{
                    case indexPath.row where indexPath.row == 0 :
                        return UITableViewAutomaticDimension
                    case indexPath.row where indexPath.row ==  1 :
                        return (updatedHeight != nil) ? updatedHeight! : DEFAULT_HEIGHT_ATTACHMENT_VIEW
                    case indexPath.row where indexPath.row ==  2 :
                        return 40
                    default:
                      
                        return UITableViewAutomaticDimension
                    }
                    
                default:
                    return 0
                }
            }else{
                return UITableViewAutomaticDimension
            }
   
           
        case PROFILE_TYPE.PROFESSIONAL.rawValue,PROFILE_TYPE.NONE.rawValue ,  PROFILE_TYPE.STUDENT.rawValue,  PROFILE_TYPE.OTHERS.rawValue :
            if currentSection != nil{
                switch currentSection{
                case 0,1,2:
                    switch indexPath.row{
//                    case indexPath.row where indexPath.row <= (experienceModelArray.count) :
//                             return UITableViewAutomaticDimension
                    case indexPath.row where indexPath.row == 0 :
                        return UITableViewAutomaticDimension
                    case indexPath.row where indexPath.row ==  1 :
                             return (updatedHeight != nil) ? updatedHeight! : DEFAULT_HEIGHT_ATTACHMENT_VIEW
                    case indexPath.row where indexPath.row ==  2 :
                            return 40
                    default:
                        return UITableViewAutomaticDimension
                    }
//                case 1:
//                    switch indexPath.row{
//                    case indexPath.row where indexPath.row <= (educationModelArray.count) :
//                        return UITableViewAutomaticDimension
//                    case indexPath.row where indexPath.row == (educationModelArray.count) + 1 :
//                        return (updatedHeight != nil) ? updatedHeight! : DEFAULT_HEIGHT_ATTACHMENT_VIEW
//                    case indexPath.row where indexPath.row == (educationModelArray.count) + 2 :
//                        return 40
//                    default:
//                        return UITableViewAutomaticDimension
//                    }
//
//                case 2:
//                    switch indexPath.row{
//                    case indexPath.row where indexPath.row <= (honorsModelArray.count) :
//                        return UITableViewAutomaticDimension
//                    case indexPath.row where indexPath.row == (honorsModelArray.count) + 1 :
//                        return (updatedHeight != nil) ? updatedHeight! : DEFAULT_HEIGHT_ATTACHMENT_VIEW
//                    case indexPath.row where indexPath.row == (honorsModelArray.count) + 2 :
//                        return 40
//                    default:
//                        return UITableViewAutomaticDimension
//                    }
                    
                default:
                    return 0
                }
            }else{
                return UITableViewAutomaticDimension
            }
            
        default:
            return 0
        }
        
        
    }
    
    func updateHeight(height: CGFloat) {
//        UIView.setAnimationsEnabled(false)
//        tblWorkEducation.beginUpdates()
//        currentCell?.contentView.frame.size.height = (currentCell?.contentView.frame.size.height)! + height
//
//        tblWorkEducation.endUpdates()
//        UIView.setAnimationsEnabled(true)
        
        
        UIView.setAnimationsEnabled(false)
        //        /* These will causes table cell heights to be recaluclated,
        //         without reloading the entire cell */
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
            self.tblWorkEducation.beginUpdates()
            //  let indexPath = IndexPath(row: self.expandingIndexRow!, section: 0)
            self.tblWorkEducation.layoutIfNeeded()
            
            self.tblWorkEducation.endUpdates()
            //        // Re-enable animations
            UIView.setAnimationsEnabled(true)
        })
        
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let workEducationCell = tableView.dequeueReusableCell(withIdentifier: "workEducationCell", for: indexPath) as! WorkEducationTableCell
        currentCell = workEducationCell
        workEducationCell.currentIndex = indexPath
        workEducationCell.delegate = self
        workEducationCell.collectionViewMedia.register(UINib(nibName: "HomeDetailCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "homeDetailsCell")
        
        switch userType{
        case PROFILE_TYPE.ORGANIZATION.rawValue  :
           
                if currentSection != nil{
                   
                    switch currentSection{
                    case 0:
                        switch indexPath.row{
//                        case indexPath.row where indexPath.row < companyModelArray.count:
//                            workEducationCell.viewActions.isHidden = true
//                            workEducationCell.viewAddRecord.isHidden = true
//                            workEducationCell.viewAttachment.isHidden = true
//                            workEducationCell.viewData.isHidden = false
//                            workEducationCell.collectionViewMedia.dataSource = self
//                            workEducationCell.collectionViewMedia.delegate = self
//                            workEducationCell.collectionViewMedia.tag = currentSection!
//                            workEducationCell.collectionViewMedia.rowNumber = indexPath.row
////                            workEducationCell.collectionViewMedia.reloadData()
//                            workEducationCell.setCompanyData(companyModel: companyModelArray[indexPath.row])
                            
                        case indexPath.row where indexPath.row == 0 :
                            workEducationCell.viewActions.isHidden = true
                            workEducationCell.viewData.isHidden = true
                            workEducationCell.viewAddRecord.isHidden = false
                            workEducationCell.viewAttachment.isHidden = true
                            isEdit == true ? companyController.setCompanyDataForEdit(currentModel: currentModel as! CompanyModel) : companyController.clearData()
                            addHonorsViewController(controller: companyController, currentCell: workEducationCell,currentView:workEducationCell.viewAddRecord)
                            
                        case indexPath.row where indexPath.row == 1 :
                            workEducationCell.viewData.isHidden = true
                            workEducationCell.viewAddRecord.isHidden = true
                            workEducationCell.viewAttachment.isHidden = true
                            isEdit == true ? workEducationCell.btnAdd.setTitle("Save", for: .normal) : workEducationCell.btnAdd.setTitle("Add", for: .normal)
                            workEducationCell.viewActions.isHidden = false
                            
                        default:
                            print("Test")
                            
                        }
                        return workEducationCell
                
                case 1:
                    switch indexPath.row{
                        
                    case 0 :
                        workEducationCell.viewActions.isHidden = true
                        workEducationCell.viewData.isHidden = true
                        workEducationCell.viewAddRecord.isHidden = false
                        workEducationCell.viewAttachment.isHidden = true
                        addHonorsViewController(controller: honorsController, currentCell: workEducationCell,currentView:workEducationCell.viewAddRecord)
                        if isEdit == true{
                            honorsController.updatedHonorsModel = currentModel as? HonorsModel
                        }
                        isEdit == true ? honorsController.setHonorsDataForEdit(currentModel: currentModel as! HonorsModel) : honorsController.clearData()
                    case  1 :
                        workEducationCell.viewActions.isHidden = true
                        workEducationCell.viewData.isHidden = true
                        workEducationCell.viewAddRecord.isHidden = true
                        workEducationCell.viewAttachment.isHidden = false
                        addHonorsViewController(controller: attachmentController, currentCell: workEducationCell,currentView:workEducationCell.viewAttachment)
                        if isEdit == true{
                            attachmentController.workProfileType = WORK_PROFILE_TYPE.Honors.rawValue
                            attachmentController.currentProfileModel = currentModel as! HonorsModel
                        }else{
                            attachmentController.currentProfileModel = nil
                        }
                        workEducationCell.viewActions.isHidden = true
                    case  2 :
                        workEducationCell.viewData.isHidden = true
                        workEducationCell.viewAddRecord.isHidden = true
                        workEducationCell.viewAttachment.isHidden = true
                        isEdit == true ? workEducationCell.btnAdd.setTitle("Save", for: .normal) : workEducationCell.btnAdd.setTitle("Add", for: .normal)
                        workEducationCell.viewActions.isHidden = false
                        
                    default:
                        print("Test")
                        
                        }
                
                default:
                print("empty cell")
                }
                    return workEducationCell
                }
                
                else{
                    workEducationCell.viewActions.isHidden = true
                    workEducationCell.viewAddRecord.isHidden = true
                    workEducationCell.viewAttachment.isHidden = true
                    workEducationCell.viewData.isHidden = false
                    workEducationCell.bringSubview(toFront: workEducationCell.viewData)
                    workEducationCell.collectionViewMedia.dataSource = self
                    workEducationCell.collectionViewMedia.delegate = self
                    workEducationCell.collectionViewMedia.tag = indexPath.section
                    workEducationCell.collectionViewMedia.rowNumber = indexPath.row
                    switch indexPath.section{
                        case 0:
                            workEducationCell.setCompanyData(companyModel: companyModelArray[indexPath.row])
                        case 1:
                            workEducationCell.setHonorsData(honorsModel: honorsModelArray[indexPath.row])
                        default:
                            print("empty cell")
                
                }
                }
            return workEducationCell
        case PROFILE_TYPE.PROFESSIONAL.rawValue,PROFILE_TYPE.NONE.rawValue ,  PROFILE_TYPE.STUDENT.rawValue,  PROFILE_TYPE.OTHERS.rawValue :
            
            if currentSection != nil{
                switch currentSection{
                case 0:
                    switch indexPath.row{
//                    case indexPath.row where indexPath.row < experienceModelArray.count:
//                        workEducationCell.viewActions.isHidden = true
//                        workEducationCell.viewAddRecord.isHidden = true
//                        workEducationCell.viewAttachment.isHidden = true
//                        workEducationCell.viewData.isHidden = false
//                        workEducationCell.collectionViewMedia.dataSource = self
//                        workEducationCell.collectionViewMedia.delegate = self
//                        workEducationCell.collectionViewMedia.tag = currentSection!
//                        workEducationCell.collectionViewMedia.rowNumber = indexPath.row
//                        workEducationCell.setExperienceData(experienceModel: experienceModelArray[indexPath.row])
                        

          
                    case indexPath.row where indexPath.row == 0 :
                        print("will display")
                        workEducationCell.viewActions.isHidden = true
                        workEducationCell.viewData.isHidden = true
                        workEducationCell.viewAddRecord.isHidden = false
                        workEducationCell.viewAttachment.isHidden = true
                        addHonorsViewController(controller: experienceController, currentCell: workEducationCell,currentView:workEducationCell.viewAddRecord)
                        if isEdit == true{
                            experienceController.updatedExpModel = currentModel as? ExperienceModel
                        }
                        isEdit == true ? experienceController.setDataForEdit(currentModel: currentModel as! ExperienceModel) : experienceController.clearData()
                    case indexPath.row where indexPath.row ==  1 :
                        workEducationCell.viewActions.isHidden = true
                        
                        workEducationCell.viewData.isHidden = true
                        workEducationCell.viewAddRecord.isHidden = true
                        workEducationCell.viewAttachment.isHidden = false
                        addHonorsViewController(controller: attachmentController, currentCell: workEducationCell,currentView:workEducationCell.viewAttachment)
                        if isEdit == true{
                            attachmentController.workProfileType = WORK_PROFILE_TYPE.Experience.rawValue
                            attachmentController.currentProfileModel = currentModel as! ExperienceModel
                        }else{
                            attachmentController.currentProfileModel = nil
                        }
                        workEducationCell.viewActions.isHidden = true
                    case indexPath.row where indexPath.row ==  2 :
                         workEducationCell.viewData.isHidden = true
                        workEducationCell.viewAddRecord.isHidden = true
                        workEducationCell.viewAttachment.isHidden = true
                         isEdit == true ? workEducationCell.btnAdd.setTitle("Save", for: .normal) : workEducationCell.btnAdd.setTitle("Add", for: .normal)
                        workEducationCell.viewActions.isHidden = false
                
                    default:
                       print("Test")
                        
                    }

                case 1:
                    switch indexPath.row{
//                    case let index where index < educationModelArray.count:
//                        workEducationCell.viewActions.isHidden = true
//                        workEducationCell.viewAddRecord.isHidden = true
//                        workEducationCell.viewAttachment.isHidden = true
//                        workEducationCell.viewData.isHidden = false
//
//                        workEducationCell.collectionViewMedia.dataSource = self
//                        workEducationCell.collectionViewMedia.delegate = self
//                        workEducationCell.collectionViewMedia.tag = currentSection!
//                        workEducationCell.collectionViewMedia.rowNumber = indexPath.row
//                       workEducationCell.setEducationData(educationModel: educationModelArray[indexPath.row])
//                        workEducationCell.collectionViewMedia.reloadData()
                    case 0 :
                        workEducationCell.viewActions.isHidden = true
                        workEducationCell.viewData.isHidden = true
                        workEducationCell.viewAddRecord.isHidden = false
                        workEducationCell.viewAttachment.isHidden = true
                        addHonorsViewController(controller: educationController, currentCell: workEducationCell,currentView:workEducationCell.viewAddRecord)
                        if isEdit == true{
                            educationController.updatedEduModel = currentModel as? EducationModel
                        }
                        isEdit == true ? educationController.setEducationEditData(currentModel: currentModel as! EducationModel) : educationController.clearData()
                    case  1 :
                        workEducationCell.viewActions.isHidden = true
                        workEducationCell.viewData.isHidden = true
                        workEducationCell.viewAddRecord.isHidden = true
                        workEducationCell.viewAttachment.isHidden = false
                        addHonorsViewController(controller: attachmentController, currentCell: workEducationCell,currentView:workEducationCell.viewAttachment)
                        if isEdit == true{
                            attachmentController.workProfileType = WORK_PROFILE_TYPE.Education.rawValue
                            attachmentController.currentProfileModel = currentModel as! EducationModel
                        }else{
                            attachmentController.currentProfileModel = nil
                        }
                        workEducationCell.viewActions.isHidden = true
                    case  2 :
                        workEducationCell.viewData.isHidden = true
                        workEducationCell.viewAddRecord.isHidden = true
                        workEducationCell.viewAttachment.isHidden = true
                        isEdit == true ? workEducationCell.btnAdd.setTitle("Save", for: .normal) : workEducationCell.btnAdd.setTitle("Add", for: .normal)
                        workEducationCell.viewActions.isHidden = false
                        
                    default:
                        print("Test")
                        
                    }
                    
                case 2:
             
                    switch indexPath.row{

                    case 0 :
                        workEducationCell.viewActions.isHidden = true
                        workEducationCell.viewData.isHidden = true
                        workEducationCell.viewAddRecord.isHidden = false
                        workEducationCell.viewAttachment.isHidden = true
                        addHonorsViewController(controller: honorsController, currentCell: workEducationCell,currentView:workEducationCell.viewAddRecord)
                        if isEdit == true{
                            honorsController.updatedHonorsModel = currentModel as? HonorsModel
                        }
                        isEdit == true ? honorsController.setHonorsDataForEdit(currentModel: currentModel as! HonorsModel) : honorsController.clearData()
                    case  1 :
                        workEducationCell.viewActions.isHidden = true
                        workEducationCell.viewData.isHidden = true
                        workEducationCell.viewAddRecord.isHidden = true
                        workEducationCell.viewAttachment.isHidden = false
                        addHonorsViewController(controller: attachmentController, currentCell: workEducationCell,currentView:workEducationCell.viewAttachment)
                        if isEdit == true{
                            attachmentController.workProfileType = WORK_PROFILE_TYPE.Honors.rawValue
                            attachmentController.currentProfileModel = currentModel as! HonorsModel
                        }else{
                            attachmentController.currentProfileModel = nil
                        }
                        workEducationCell.viewActions.isHidden = true
                    case  2 :
                        workEducationCell.viewData.isHidden = true
                        workEducationCell.viewAddRecord.isHidden = true
                        workEducationCell.viewAttachment.isHidden = true
                        isEdit == true ? workEducationCell.btnAdd.setTitle("Save", for: .normal) : workEducationCell.btnAdd.setTitle("Add", for: .normal)
                        workEducationCell.viewActions.isHidden = false
                        
                    default:
                        print("Test")
                        
                    }

                default:
                    print("empty cell")
                }
            }
            else{
                workEducationCell.viewActions.isHidden = true
                workEducationCell.viewAddRecord.isHidden = true
                workEducationCell.viewAttachment.isHidden = true
                workEducationCell.viewData.isHidden = false
                workEducationCell.bringSubview(toFront: workEducationCell.viewData)
                workEducationCell.collectionViewMedia.dataSource = self
                workEducationCell.collectionViewMedia.delegate = self
                workEducationCell.collectionViewMedia.tag = indexPath.section
                workEducationCell.collectionViewMedia.rowNumber = indexPath.row
                switch indexPath.section{
                case 0:
                    workEducationCell.setExperienceData(experienceModel: experienceModelArray[indexPath.row])
                case 1:
                    workEducationCell.setEducationData(educationModel: educationModelArray[indexPath.row])
                case 2:
                   workEducationCell.setHonorsData(honorsModel: honorsModelArray[indexPath.row])
                default:
                    print("empty cell")
                }
               
//                workEducationCell.collectionViewMedia.reloadData()

            }
            
            workEducationCell.setNeedsLayout()
            workEducationCell.layoutIfNeeded()
            return workEducationCell
        default:
            return workEducationCell
        
        }
 
    }
    
    


    
    func indexOfHeaderTapped(index: Int) {
        if currentSection == nil{
           currentSection = index
        }else{
            return
        }
        tblWorkEducation.reloadData()
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+0.1, execute:{
            self.tblWorkEducation.scrollToFirstRow()
            // self.tblWorkEducation.scrollToBottom(animated: true)
//            if self.userType == PROFILE_TYPE.ORGANIZATION.rawValue && index == 0{
//                self.tblWorkEducation.scrollToLastCell(animated: true,countOfLasRow:2)
//            }else{
//                self.tblWorkEducation.scrollToFirstRow()
//                self.tblWorkEducation.scrollToLastCell(animated: true,countOfLasRow:1)
//            }
           
        })
  
    }
    
   
    func addClicked(indexPath: IndexPath) {
        let imagesArray = attachmentController.imageNamesArray
        let linkString = attachmentController.linksArray.map{$0.fileName!}.joined(separator: ",")
        let youTubeString = attachmentController.youTubeLinksArray.map{$0.fileName!}.joined(separator: ",")
        let documents = attachmentController.fileArray
        let presentations = attachmentController.pptArray
        
        switch userType{
        case PROFILE_TYPE.ORGANIZATION.rawValue :
           
            switch currentSection{
            case 0:
                let requestModel = companyController.fetchCompanyData()
                if requestModel != nil{
                    output.addCompany(typeOfWorkProfile: WORK_PROFILE_TYPE.Company, requestModel: requestModel!)
                }
            
            case 1:
                var requestModel = honorsController.fetchHonorsData()
                if requestModel != nil{
                    requestModel?.lnk = linkString
                    requestModel?.vde = youTubeString
                    requestModel?.images = imagesArray
                    requestModel?.documents = documents
                    requestModel?.presentations = presentations
                    output.addHonors(typeOfWorkProfile: WORK_PROFILE_TYPE.Honors , requestModel: requestModel!)
                }
                
            default:
                break
            }
            
            
        case PROFILE_TYPE.STUDENT.rawValue , PROFILE_TYPE.PROFESSIONAL.rawValue,PROFILE_TYPE.NONE.rawValue , PROFILE_TYPE.OTHERS.rawValue :
           
            switch currentSection{
            case 0:
                var requestModel = experienceController.fetchExperienceData()
                if requestModel != nil{
                    requestModel?.lnk = linkString
                    requestModel?.vde = youTubeString
                    requestModel?.images = imagesArray
                    requestModel?.documents = documents
                    requestModel?.presentations = presentations
                    output.addExperience(typeOfWorkProfile: WORK_PROFILE_TYPE.Experience, requestModel: requestModel!)
                }
            case 1:
                var requestModel = educationController.fetchEducationData()
                if requestModel != nil{
                    requestModel?.lnk = linkString
                    requestModel?.vde = youTubeString
                    requestModel?.images = imagesArray
                    requestModel?.documents = documents
                    requestModel?.presentations = presentations
                    output.addEducation(typeOfWorkProfile: WORK_PROFILE_TYPE.Education, requestModel: requestModel!)
                }
            case 2:
                var requestModel = honorsController.fetchHonorsData()
                if requestModel != nil{
                    requestModel?.lnk = linkString
                    requestModel?.vde = youTubeString
                    requestModel?.images = imagesArray
                    requestModel?.documents = documents
                    requestModel?.presentations = presentations
                    output.addHonors(typeOfWorkProfile: WORK_PROFILE_TYPE.Honors , requestModel: requestModel!)
                }
                
            default:
                break
            }
        default:
            break
        }
   }
    
    func displayWorkProfileResult(response: NewPost.Response) {
        print("Response is---->",response)
        if response.status == true{
            isEdit = false
            switch userType{
            case PROFILE_TYPE.PROFESSIONAL.rawValue,PROFILE_TYPE.NONE.rawValue ,  PROFILE_TYPE.STUDENT.rawValue,  PROFILE_TYPE.OTHERS.rawValue :
                switch currentSection{
                case 0:
                    experienceModelArray.removeAll()
                    experienceModelArray = ExperienceModel.fetchExperienceModels()
                case 1:
                    educationModelArray.removeAll()
                    educationModelArray = EducationModel.fetchEducationModels()
                case 2:
                    honorsModelArray.removeAll()
                    honorsModelArray = HonorsModel.fetchHonorsModels()
                default:
                    print("Default")
                }
                
            case PROFILE_TYPE.ORGANIZATION.rawValue  :
                switch currentSection{
                case 0:
                    companyModelArray.removeAll()
                    companyModelArray = CompanyModel.fetchCompanyModels()
                case 1:
                    honorsModelArray.removeAll()
                    honorsModelArray = HonorsModel.fetchHonorsModels()
                default:
                    print("Default")
                }
            default:
                break
        }
 
            cancelClicked()
        }
            
        displayToast(response.message ?? UNKNOWN_ERROR_MSG)
    
    }
    
    func cancelClicked() {
        isEdit = false
        removePreviousControllers()
        updatedHeight = DEFAULT_HEIGHT_ATTACHMENT_VIEW
        currentSection = nil
        fetchCurrentModels()
//        tblWorkEducation.reloadData()
        attachmentController.removeAllElements()
//        scrollToFirstRow()
//        tblWorkEducation.setContentOffset(.zero, animated: true)
        
    }
    
    func removePreviousControllers(){
        attachmentController.view.removeFromSuperview()
        attachmentController.removeFromParentViewController()
        experienceController.addNewClicked()
        experienceController.view.removeFromSuperview()
        experienceController.removeFromParentViewController()
        educationController.addNewClicked()
        educationController.view.removeFromSuperview()
        educationController.removeFromParentViewController()
        honorsController.addNewClicked()
        honorsController.view.removeFromSuperview()
        honorsController.removeFromParentViewController()
        companyController.clearData()
        companyController.view.removeFromSuperview()
        companyController.removeFromParentViewController()
    }
    
    func scrollToFirstRow() {
        let indexPath = IndexPath(row: 0, section: 0)
        tblWorkEducation.scrollToRow(at: indexPath, at: .top, animated: true)
    }
    
    
    func addHonorsViewController(controller : UIViewController , currentCell : WorkEducationTableCell ,currentView:UIView){
        
//                currentCell.contentView.isHidden = false
                addChildViewController(controller)
                currentCell.layoutIfNeeded()
                currentView.addSubview(controller.view)
                controller.view.translatesAutoresizingMaskIntoConstraints = false
              
                currentView.frame = controller.view.bounds
                if controller == attachmentController{
                    attachmentController.btnPost.isHidden = true
                    attachmentController.btnVideoAttatchment.isHidden = true
                }
                currentView.addConstraint(NSLayoutConstraint(item: controller.view, attribute: NSLayoutAttribute.leading, relatedBy: NSLayoutRelation.equal, toItem: currentView, attribute: NSLayoutAttribute.leading, multiplier: 1.0, constant: 0.0))
                currentCell.contentView.addConstraint(NSLayoutConstraint(item: controller.view, attribute: NSLayoutAttribute.trailing, relatedBy: NSLayoutRelation.equal, toItem: currentCell.contentView, attribute: NSLayoutAttribute.trailing, multiplier: 1.0, constant: 0.0))
                currentView.addConstraint(NSLayoutConstraint(item: controller.view, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal, toItem: currentView, attribute: NSLayoutAttribute.top, multiplier: 1.0, constant: 0.0))
                currentView.addConstraint(NSLayoutConstraint(item: controller.view, attribute: NSLayoutAttribute.bottom, relatedBy: NSLayoutRelation.equal, toItem: currentView, attribute: NSLayoutAttribute.bottom, multiplier: 1.0, constant: 0.0))
                controller.didMove(toParentViewController: self)
//                controller.view.setNeedsLayout()
                controller.view.layoutIfNeeded()
                controller.didMove(toParentViewController: self)
       
            
    }
    
    func deleteOrganization(indexPath: IndexPath) {
        var orgType = ""
        var orgId : Int!
        switch userType{
            case PROFILE_TYPE.PROFESSIONAL.rawValue,PROFILE_TYPE.NONE.rawValue ,  PROFILE_TYPE.STUDENT.rawValue,  PROFILE_TYPE.OTHERS.rawValue :
                
                switch currentSection != nil ? currentSection : indexPath.section{
                case 0:
                    orgType = "experience"
                    orgId = experienceModelArray[indexPath.row].pexpid
                case 1:
                    orgType = "education"
                    orgId = educationModelArray[indexPath.row].pedupid
                case 2:
                    orgType = "honors"
                    orgId = honorsModelArray[indexPath.row].phonid
                default:
                    print("Default")
                }

            
            case PROFILE_TYPE.ORGANIZATION.rawValue  :
                switch indexPath.section{
                case 0:
                    orgType = "company"
                    orgId = companyModelArray[indexPath.row].pcomid
                
                case 1:
                    orgType = "honors"
                    orgId = honorsModelArray[indexPath.row].phonid
                default:
                    print("Default")
            }
        default :
            print("Default")
        }
        var indexPath = currentSection != nil ? IndexPath(row:indexPath.row,section:currentSection!) : indexPath
       
        showCustomAlert(CONFIRMATION_ALERT, message: DELETE_ATTATCHMENT, okButtonTitle: UIAlertActionTitle.DELETE.rawValue, cancelButtonTitle: UIAlertActionTitle.CANCEL.rawValue) { [unowned self](action) in
                        switch action{
                        case .DELETE:
                            self.deleteMethod(fileType : orgType , sender : indexPath , fileName : orgId)
                        case .CANCEL:
                            print("Dismiss Alert")
                        default:
                            print("Dismiss Alert")
                        }
                    }
    }
    
    
    
    func editClicked(indexPath: IndexPath) {
        isEdit = true
        switch userType{
        case PROFILE_TYPE.PROFESSIONAL.rawValue,PROFILE_TYPE.NONE.rawValue ,  PROFILE_TYPE.STUDENT.rawValue,  PROFILE_TYPE.OTHERS.rawValue :
            switch currentSection != nil ? currentSection : indexPath.section{
            case 0:
               currentModel = experienceModelArray[indexPath.row]
            case 1:
                currentModel = educationModelArray[indexPath.row]
            case 2:
               currentModel = honorsModelArray[indexPath.row]
            default:
                print("Default")
            }
            
        case PROFILE_TYPE.ORGANIZATION.rawValue  :
             switch currentSection != nil ? currentSection : indexPath.section{
            case 0:
                currentModel = companyModelArray[indexPath.row]
            case 1:
                currentModel = honorsModelArray[indexPath.row]
            default:
                print("Default")
            }
        default :
            print("Default")
        }
        
       indexOfHeaderTapped(index: indexPath.section)
   }
    
        func deleteMethod(fileType : String , sender : IndexPath , fileName:Int?){
            let worker = ProfileWorker()
            worker.deleteOrganization(orgType: fileType, idToBeDeleted:fileName!, completionHandler: { [weak self] (message, responseModel) in
                if let response = responseModel{
                    DispatchQueue.main.async {
                       
                        self?.displayToast((response.message)!)
                        if response.status == true{
                            
                                DispatchQueue.main.async {
                                    switch self?.userType{
                                    case PROFILE_TYPE.ORGANIZATION.rawValue  :
                                        switch sender.section{
                                        case 0 :
                                            self?.companyModelArray.remove(at: sender.row)
                                            CompanyModel.deleteAll_withUId(Id: fileName!)
                                        
                                        case 1:
                                            self?.honorsModelArray.remove(at: sender.row)
                                            HonorsModel.deleteAll_withUId(Id: fileName!)
                                        default:
                                            break
                                        }
                                        
                                        
                                    case PROFILE_TYPE.PROFESSIONAL.rawValue,PROFILE_TYPE.NONE.rawValue ,  PROFILE_TYPE.STUDENT.rawValue,  PROFILE_TYPE.OTHERS.rawValue :
                                        switch sender.section{
                                        case 0 :
                                            self?.experienceModelArray.remove(at: sender.row)
                                            ExperienceModel.deleteAll_withUId(Id: fileName!)
                                        case 1:
                                            self?.educationModelArray.remove(at: sender.row)
                                            EducationModel.deleteAll_withUId(Id: fileName!)
                                        case 2:
                                            self?.honorsModelArray.remove(at: sender.row)
                                            HonorsModel.deleteAll_withUId(Id: fileName!)
                                        default:
                                            break
                                        }
                                    default:
                                        break
                                    }
                                   self?.tblWorkEducation.reloadData()
//                                    try! realm?.commitWrite()
//                                    self?.tblWorkEducation.deleteRows(at: [IndexPath(row:sender.row,section:sender.section)], with: .automatic)
                                }
                            }
                        }
    
                    
    
                }else{
                    self?.displayToast(message!)
                }
            })
        }
    
}

extension WorkEducationViewController : UICollectionViewDataSource , UICollectionViewDelegate , UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        

        let collectionView = collectionView as! CustomCollectionView
        switch userType{
        case PROFILE_TYPE.ORGANIZATION.rawValue  :
            switch section{
            case 0:
               switch collectionView.tag{
               case 0:
                return 1
               case 1:
                return honorsModelArray[collectionView.rowNumber].phonphot.count
               default:
                return 0
                }
            case 1:
                switch collectionView.tag{
                
                case 1:
                    return honorsModelArray[collectionView.rowNumber].phonfle.count
                default:
                    return 0
                }
            case 2:
                switch collectionView.tag{
                
                case 1:
                    return honorsModelArray[collectionView.rowNumber].phonprest.count
                default:
                    return 0
                }
            case 3:
                switch collectionView.tag{
               
                case 1:
                    return (honorsModelArray[collectionView.rowNumber].phonlnk?.isEmpty)! ? 0 : (honorsModelArray[collectionView.rowNumber].phonlnk?.components(separatedBy: ","))!.count
                default:
                    return 0
                }
            case 4:
                switch collectionView.tag{
                
                case 1:
                    return (honorsModelArray[collectionView.rowNumber].phonvdeo?.isEmpty)! ? 0 : (honorsModelArray[collectionView.rowNumber].phonvdeo?.components(separatedBy: ","))!.count
                default:
                    return 0
                }
            default:
                return 0
            }
        case PROFILE_TYPE.PROFESSIONAL.rawValue,PROFILE_TYPE.NONE.rawValue ,  PROFILE_TYPE.STUDENT.rawValue,  PROFILE_TYPE.OTHERS.rawValue :
         
               switch section {
                    case 0:
                        switch collectionView.tag{
                        case 0:
                            return experienceModelArray[collectionView.rowNumber].pexpphot.count
                        case 1:
                            return educationModelArray[collectionView.rowNumber].peduphot.count
                        case 2:
                            return honorsModelArray[collectionView.rowNumber].phonphot.count
                        default:
                            return 0
                        }
                    
                    case 1:
                        switch collectionView.tag{
                        case 0:
                            return experienceModelArray[collectionView.rowNumber].pexpfle.count
                        case 1:
                            return educationModelArray[collectionView.rowNumber].pedufle.count
                        case 2:
                            return honorsModelArray[collectionView.rowNumber].phonfle.count
                        default:
                            return 0
                    }
                    case 2:
                        switch collectionView.tag{
                        case 0:
                            return experienceModelArray[collectionView.rowNumber].pexpprest.count
                        case 1:
                            return educationModelArray[collectionView.rowNumber].peduprest.count
                        case 2:
                            return honorsModelArray[collectionView.rowNumber].phonprest.count
                        default:
                            return 0
                    }
                    case 3:
                        switch collectionView.tag{
                        case 0:
                            return (experienceModelArray[collectionView.rowNumber].pexplnk?.isEmpty)! ? 0 : (experienceModelArray[collectionView.rowNumber].pexplnk?.components(separatedBy: ","))!.count
                        case 1:
                            return (educationModelArray[collectionView.rowNumber].pedulnk?.isEmpty)! ? 0 : (educationModelArray[collectionView.rowNumber].pedulnk?.components(separatedBy: ","))!.count
                        case 2:
                            return (honorsModelArray[collectionView.rowNumber].phonlnk?.isEmpty)! ? 0 : (honorsModelArray[collectionView.rowNumber].phonlnk?.components(separatedBy: ","))!.count
                    default:
                        return 0
                    }
                    
                    case 4:
                        switch collectionView.tag{
                        case 0:
                            return (experienceModelArray[collectionView.rowNumber].pexpvdeo?.isEmpty)! ? 0 : (experienceModelArray[collectionView.rowNumber].pexpvdeo?.components(separatedBy: ","))!.count
                        case 1:
                            return (educationModelArray[collectionView.rowNumber].peduvdeo?.isEmpty)! ? 0 : (educationModelArray[collectionView.rowNumber].peduvdeo?.components(separatedBy: ","))!.count
                        case 2:
                            return (honorsModelArray[collectionView.rowNumber].phonvdeo?.isEmpty)! ? 0 : (honorsModelArray[collectionView.rowNumber].phonvdeo?.components(separatedBy: ","))!.count
                        default:
                            return 0
                    }
                    
                    default:
                    return 0
                }
            
            
        default:
            return 0
        }
   }
 
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "homeDetailsCell", for: indexPath) as! HomeDetailCollectionViewCell
        cell.btnDelete.isHidden = true
        let collectionView = collectionView as! CustomCollectionView
        switch userType{
        case PROFILE_TYPE.ORGANIZATION.rawValue  :
                        switch indexPath.section{
                        case 0:
                            switch collectionView.tag{
                            case 0:
                                cell.setUpViewForImage(isFile : true)
                            case 1:
cell.setUpViewForImage(url:honorsModelArray[collectionView.rowNumber].phonphot[indexPath.row])
                            default:
                                return cell
                            }
                        case 1:
                        
                            switch collectionView.tag{
                            case 0:
                                return cell
                            case 1:
                               cell.setUpViewForImage(isFile : true)
                            default:
                                return cell
                            }
                        case 2:
                        
                            switch collectionView.tag{
                            case 0:
                                return cell
                            case 1:
                               cell.setUpViewForImage(isPresentation : true)
                            default:
                                return cell
                            }
                        case 3:
                        
                            switch collectionView.tag{
                            case 0:
                                return cell
                            case 1:
                              cell.setUpViewForLink(link:(honorsModelArray[collectionView.rowNumber].phonlnk?.components(separatedBy: ","))![indexPath.row])
                            default:
                                return cell
                            }
                        case 4:
                        
                            switch collectionView.tag{
                            case 0:
                                return cell
                            case 1:
                               cell.setUpViewForLink(link:(honorsModelArray[collectionView.rowNumber].phonvdeo?.components(separatedBy: ","))![indexPath.row])
                            default:
                                return cell
                            }
                        default:
                            return cell
            }
  
        case PROFILE_TYPE.PROFESSIONAL.rawValue,PROFILE_TYPE.NONE.rawValue ,  PROFILE_TYPE.STUDENT.rawValue,  PROFILE_TYPE.OTHERS.rawValue :
            switch indexPath.section{
            case 0:
                switch collectionView.tag{
                case 0:
                    cell.setUpViewForImage(url:experienceModelArray[collectionView.rowNumber].pexpphot[indexPath.row])
                case 1:
                    cell.setUpViewForImage(url:educationModelArray[collectionView.rowNumber].peduphot[indexPath.row])
                case 2:
                    cell.setUpViewForImage(url:honorsModelArray[collectionView.rowNumber].phonphot[indexPath.row])
                default:
                    return cell
                }
                
            case 1:
                cell.setUpViewForImage(isFile : true)

            case 2:
                 cell.setUpViewForImage(isPresentation : true)
            case 3:
                switch collectionView.tag{
                case 0:
                    
                    cell.setUpViewForLink(link:(experienceModelArray[collectionView.rowNumber].pexplnk?.components(separatedBy: ","))![indexPath.row])
                    
                case 1:
                    
                     cell.setUpViewForLink(link:(educationModelArray[collectionView.rowNumber].pedulnk?.components(separatedBy: ","))![indexPath.row])
                    
                case 2:
                    
                     cell.setUpViewForLink(link:(honorsModelArray[collectionView.rowNumber].phonlnk?.components(separatedBy: ","))![indexPath.row])
                    
                default:
                    return cell
                }
            case 4:
                switch collectionView.tag{
                case 0:
                   
                    cell.setUpViewForYouTubeVideo(youTubeVideoUrl:(experienceModelArray[collectionView.rowNumber].pexpvdeo?.components(separatedBy: ","))![indexPath.row])
                    
                case 1:
                    
                    cell.setUpViewForYouTubeVideo(youTubeVideoUrl:(educationModelArray[collectionView.rowNumber].peduvdeo?.components(separatedBy: ","))![indexPath.row])
                    
                case 2:
                    
                    cell.setUpViewForYouTubeVideo(youTubeVideoUrl:(honorsModelArray[collectionView.rowNumber].phonvdeo?.components(separatedBy: ","))![indexPath.row])
                    
                default:
                    return cell
                }
                
            default:
                return cell
            }

            default:
                return cell
            }

        return cell
        
        }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: collectionView.frame.size.width / 3, height: collectionView.frame.size.height)
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        
        let collectionView = collectionView as! CustomCollectionView
        switch userType{
        case PROFILE_TYPE.ORGANIZATION.rawValue  :
            switch indexPath.section{
            case 0:
                switch collectionView.tag{
                case 0:
                   print("Default")
                    showAttatchmentView(mediaUrl: companyModelArray[collectionView.rowNumber].pcomprofile[indexPath.row])
                case 1:
                    
                    let photoController = AppToolbarController(rootViewController: PhotoViewController(imagesArray:honorsModelArray[collectionView.rowNumber].phonphot,index: indexPath.row))
                    present(photoController, animated: true, completion: nil)
                    
                    
                default:
                    print("Default")
                }
                
            case 1:
                switch collectionView.tag{
                case 1:
                   showAttatchmentView(mediaUrl:(honorsModelArray[collectionView.rowNumber].phonfle[indexPath.row]))
                default:
                    print("Default")
                }
                
                
                
            case 2:
                switch collectionView.tag{
                
                    case 1:
                    
                    showAttatchmentView(mediaUrl:(honorsModelArray[collectionView.rowNumber].phonprest[indexPath.row]))
                default:
                    print("Default")
                }
            case 3:
                
                switch collectionView.tag{
                
                case 1:
                    
                    openLinks(linkUrl:(honorsModelArray[collectionView.rowNumber].phonlnk?.components(separatedBy: ","))![indexPath.row])
                default:
                    print("Default")
                }
            case 4:
                print("You tube video")
                
            default:
                print("Default")
            }
        case PROFILE_TYPE.PROFESSIONAL.rawValue,PROFILE_TYPE.NONE.rawValue ,  PROFILE_TYPE.STUDENT.rawValue,  PROFILE_TYPE.OTHERS.rawValue :
            switch indexPath.section{
            case 0:
                switch collectionView.tag{
                case 0:
                    let photoController = AppToolbarController(rootViewController: PhotoViewController(imagesArray:experienceModelArray[collectionView.rowNumber].pexpphot,index: indexPath.row))
                     present(photoController, animated: true, completion: nil)
               case 1:
                    let photoController = AppToolbarController(rootViewController: PhotoViewController(imagesArray:educationModelArray[collectionView.rowNumber].peduphot,index: indexPath.row))
                    present(photoController, animated: true, completion: nil)
                case 2:
                    let photoController = AppToolbarController(rootViewController: PhotoViewController(imagesArray:honorsModelArray[collectionView.rowNumber].phonphot,index: indexPath.row))
                    present(photoController, animated: true, completion: nil)
                   
                    
                default:
                    print("Default")
                }
                
            case 1:
                switch collectionView.tag{
                case 0:
                    showAttatchmentView(mediaUrl:(experienceModelArray[collectionView.rowNumber].pexpfle[indexPath.row]))
                    
                case 1:
                    showAttatchmentView(mediaUrl:(educationModelArray[collectionView.rowNumber].pedufle[indexPath.row]))
                case 2:
                    showAttatchmentView(mediaUrl:(honorsModelArray[collectionView.rowNumber].phonfle[indexPath.row]))
                default:
                    print("Default")
                }
                
               
                
            case 2:
                switch collectionView.tag{
                case 0:
                    showAttatchmentView(mediaUrl:(experienceModelArray[collectionView.rowNumber].pexpprest[indexPath.row]))
                    
                case 1:
                    showAttatchmentView(mediaUrl:(educationModelArray[collectionView.rowNumber].peduprest[indexPath.row]))
                case 2:
                    showAttatchmentView(mediaUrl:(honorsModelArray[collectionView.rowNumber].phonprest[indexPath.row]))
                default:
                    print("Default")
                }
            case 3:
                
                switch collectionView.tag{
                case 0:
                    openLinks(linkUrl:(experienceModelArray[collectionView.rowNumber].pexplnk?.components(separatedBy: ","))![indexPath.row])
                case 1:
                    openLinks(linkUrl:(educationModelArray[collectionView.rowNumber].pedulnk?.components(separatedBy: ","))![indexPath.row])
                case 2:
                    openLinks(linkUrl:(honorsModelArray[collectionView.rowNumber].phonlnk?.components(separatedBy: ","))![indexPath.row])
                    
                    
                default:
                   print("Default")
                }
            case 4:
               print("You tube video")
                
            default:
                print("Default")
            }
            
        default:
            print("Default")
        }
        
        
        
    }
}

    

