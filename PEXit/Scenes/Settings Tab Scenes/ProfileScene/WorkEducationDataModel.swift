//
//  WorkEducationDataModel.swift
//  PEXit
//
//  Created by Apple on 06/09/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation


struct WorkEducationRequestModel{
    struct AddExperienceModel{
        var companyname : String!
        var exp_title : String!
        var exp_location : String?
        var startDateMonth : String?
        var startDateYear : String?
        var endDateMonth : String?
        var endDateYear : String?
        var iscurrent : String?
        var exp_desc : String?
        var lnk : String?
        var vde : String?
        var documents : [MediaAttatchment]?
        var images : [MediaAttatchment]?
        var presentations : [MediaAttatchment]?
        var isEdit = false
        var currentModelId = 0
    }
    
    struct AddEducationModel{
        var schoolname : String!
        var schoolfromyear : String?
        var schooltoyear : String!
        var schoolDegree : String?
        var odegre : String?
        var schoolMode : String?
        var omod : String?
        var schoolstudy : String?
        var school_act : String?
        var lnk : String?
        var exp_desc : String?
        var vde : String?
        var documents : [MediaAttatchment]?
        var images : [MediaAttatchment]?
        var presentations : [MediaAttatchment]?
        var isEdit = false
        var currentModelId = 0
    }
    
    struct AddCompanyModel{
        var companyCountry : String!
        var companyState : String?
        var companyCity : String!
        var documents : [MediaAttatchment]?
        var isEdit = false
        var currentModelId = 0
       
    }
    
    struct AddHonorsModel{
        var hons_title : String!
        var honrmnth : String?
        var honryr : String!
        var hons_desc : String?
        var lnk : String?
        var vde : String?
        var documents : [MediaAttatchment]?
        var images : [MediaAttatchment]?
        var presentations : [MediaAttatchment]?
        var isEdit = false
        var currentModelId = 0
    }
}
