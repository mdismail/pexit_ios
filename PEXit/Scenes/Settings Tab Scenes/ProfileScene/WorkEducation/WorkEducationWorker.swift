//
//  WorkEducationWorker.swift
//  PEXit
//
//  Created by Apple on 06/09/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation
import ObjectMapper

class WorkEducationWorker{
    func saveUserWorkProfile(workProfileType:String,params:[String:Any],images : [MediaAttatchment]? = nil,documents:[MediaAttatchment]? = nil, presentation:[MediaAttatchment]? = nil,isEdit:Bool = false ,fileId:Int? = 0,completionHandler:@escaping(_ responseModel : NewPost.Response) -> Void){
        START_LOADING_VIEW()
        let apiName = isEdit == false ? WORKPROFILE_INSERT(workProfileType) : WORKPROFILE_UPDATE(workProfileType, idOfProfile: fileId!)
        AlamofireManager.shared().updateUserWorkProfile(apiUrl: apiName,imageData: images,fileData: documents,pptData: presentation, parameters: params) { (response, error) in
            STOP_LOADING_VIEW()
            if response != nil{
                if !(((response!["errors"] as? String)?.isEmpty)!) || !((response!["virus"] as? String)?.isEmpty)!{
                    Utils.showErrorFilesAlert(errorFiles : response!["errors"] as? String,corruptFiles : response!["virus"] as? String)
                }

                let decoder = JSONDecoder()
                do{
                    
                    let jsonData = try JSONSerialization.data(withJSONObject: response, options: .prettyPrinted)
                    let reqJSONStr = String(data: jsonData, encoding: .utf8)
                    let data = reqJSONStr?.data(using: .utf8)
                    
                    let responsePost = try decoder.decode(NewPost.Response.self, from: data!)
                    if responsePost.status == true{
//                        let json = try JSONSerialization.jsonObject(with: response!, options: []) as? Dictionary<String , Any>
                        let json = try JSONSerialization.jsonObject(with: jsonData, options: []) as? Dictionary<String , Any>
                        let profileData = json!["datas"] as! [String:Any]
                        switch workProfileType{
                        case WORK_PROFILE_TYPE.Education.rawValue :
//                            let profileData = jsonData["datas"] as! [String:Any]
                                let educationModel = Mapper<EducationModel>().map(JSONObject:profileData)
                                RealmDBManager.sharedManager.addOrUpdateObject(object: educationModel!)

                        case WORK_PROFILE_TYPE.Experience.rawValue:
//                               let profileData = jsonData["datas"] as! [String:Any]
                               let educationModel = Mapper<ExperienceModel>().map(JSONObject:profileData)
                               RealmDBManager.sharedManager.addOrUpdateObject(object: educationModel!)

                        case WORK_PROFILE_TYPE.Honors.rawValue:
//                            let profileData = jsonData["datas"] as! [String:Any]
                            let educationModel = Mapper<HonorsModel>().map(JSONObject:profileData)
                            RealmDBManager.sharedManager.addOrUpdateObject(object: educationModel!)

                        case WORK_PROFILE_TYPE.Company.rawValue:
//                            let profileData = jsonData["datas"] as! [String:Any]
                            let educationModel = Mapper<CompanyModel>().map(JSONObject:profileData)
                            RealmDBManager.sharedManager.addOrUpdateObject(object: educationModel!)
                            default:
                                    break

                            }
                        
                    }
                    
                    completionHandler(responsePost)
                }catch{
                    STOP_LOADING_VIEW()
                    print("error")
                }
            }else{
                
                STOP_LOADING_VIEW()
                //                    setToastStatus(flag: false)
                KEY_WINDOW?.makeToast(error?.localizedDescription)
            }
            
            
        }
    }
}
