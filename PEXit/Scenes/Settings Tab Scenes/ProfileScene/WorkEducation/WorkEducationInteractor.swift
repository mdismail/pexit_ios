//
//  WorkEducationInteractor.swift
//  PEXit
//
//  Created by Apple on 06/09/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation
protocol WorkEducationInteractorInput {
    
    func addExperience(typeOfWorkProfile:WORK_PROFILE_TYPE,requestModel : WorkEducationRequestModel.AddExperienceModel)
    func addEducation(typeOfWorkProfile:WORK_PROFILE_TYPE,requestModel : WorkEducationRequestModel.AddEducationModel)
    func addHonors(typeOfWorkProfile:WORK_PROFILE_TYPE,requestModel : WorkEducationRequestModel.AddHonorsModel)
    func addCompany(typeOfWorkProfile:WORK_PROFILE_TYPE,requestModel : WorkEducationRequestModel.AddCompanyModel)
    
}

protocol WorkEducationInteractorOutput{
   func displayResult(response: NewPost.Response)
    
}

class WorkEducationInteractor : WorkEducationInteractorInput{
    
    
    
    
    var workEducationInteractorOutput : WorkEducationInteractorOutput!
    var worker = WorkEducationWorker()
    
    func addExperience(typeOfWorkProfile:WORK_PROFILE_TYPE,requestModel : WorkEducationRequestModel.AddExperienceModel){
        let params = ["companyname":requestModel.companyname! ,"exp_title":requestModel.exp_title! ,"exp_location":requestModel.exp_location! ?? "","startDateMonth":requestModel.startDateMonth! ,"startDateYear":requestModel.startDateYear! ?? "","endDateMonth":requestModel.endDateMonth ?? "","endDateYear":requestModel.endDateYear! ?? "","iscurrent":requestModel.iscurrent ?? 0,"exp_desc":requestModel.exp_desc! ?? "","lnk":requestModel.lnk ?? "","vde":requestModel.vde ?? ""] as [String : Any]
        worker.saveUserWorkProfile(workProfileType: WORK_PROFILE_TYPE.Experience.rawValue,params:params,images: requestModel.images,documents: requestModel.documents, presentation : requestModel.presentations,isEdit:requestModel.isEdit,fileId:requestModel.currentModelId) { (response) in
                    self.workEducationInteractorOutput.displayResult(response: response)
        }
        
        
        
    }
    
    func addEducation(typeOfWorkProfile:WORK_PROFILE_TYPE,requestModel : WorkEducationRequestModel.AddEducationModel){
        var params = ["schoolname":requestModel.schoolname! ?? "","schoolfromyear":requestModel.schoolfromyear! ?? "","schooltoyear":requestModel.schooltoyear! ?? "","schoolDegree":requestModel.schoolDegree! ?? ""
            ,"odegre":requestModel.odegre! ?? "","schoolMode":requestModel.schoolMode! ?? "","omod":requestModel.omod! ?? "","schoolstudy":requestModel.schoolstudy! ?? "","school_act":requestModel.school_act! ?? "","exp_desc":requestModel.exp_desc! ?? "",
                      "lnk":requestModel.lnk ?? "","vde":requestModel.vde ?? ""] as [String : Any]
        print("Education params are --->",params)
        worker.saveUserWorkProfile(workProfileType: WORK_PROFILE_TYPE.Education.rawValue,params:params,images: requestModel.images,documents: requestModel.documents, presentation : requestModel.presentations,isEdit:requestModel.isEdit,fileId:requestModel.currentModelId) { (response) in
                        self.workEducationInteractorOutput.displayResult(response: response)
        }
        
        
        
    }
    
    
    func addHonors(typeOfWorkProfile:WORK_PROFILE_TYPE,requestModel : WorkEducationRequestModel.AddHonorsModel){
      var params =   ["hons_title":requestModel.hons_title! ?? "","honrmnth":requestModel.honrmnth! ?? "","honryr":requestModel.honryr! ?? "","hons_desc":requestModel.hons_desc! ?? "","lnk":requestModel.lnk ?? "","vde":requestModel.vde ?? ""] as [String : Any]
        worker.saveUserWorkProfile(workProfileType: WORK_PROFILE_TYPE.Honors.rawValue,params:params,images: requestModel.images,documents: requestModel.documents, presentation : requestModel.presentations,isEdit:requestModel.isEdit,fileId:requestModel.currentModelId) { (response) in
                        self.workEducationInteractorOutput.displayResult(response: response)
        }
        
        
        
    }
    
    func addCompany(typeOfWorkProfile:WORK_PROFILE_TYPE,requestModel : WorkEducationRequestModel.AddCompanyModel){
    var params = ["prfcunt":requestModel.companyCountry!,"prfstat":requestModel.companyState!,"prfcit":requestModel.companyCity!] as [String : Any]
        worker.saveUserWorkProfile(workProfileType: WORK_PROFILE_TYPE.Company.rawValue, params:params,documents:requestModel.documents,isEdit:requestModel.isEdit,fileId:requestModel.currentModelId) { (response) in
                    self.workEducationInteractorOutput.displayResult(response: response)
                }
        
        
        
    }
    
}
