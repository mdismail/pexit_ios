//
//  WorkEducationConfigurator.swift
//  PEXit
//
//  Created by Apple on 06/09/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation
import Foundation
import UIKit

extension WorkEducationViewController: WorkEducationPresenterOutput{
    
}

extension WorkEducationInteractor : WorkEducationViewControllerOutput{
    
    
}

extension WorkEducationPresenter: WorkEducationInteractorOutput{
    
}

class WorkEducationConfigurator{
    
    class var sharedInstance : WorkEducationConfigurator{
        struct Static{
            static let instance = WorkEducationConfigurator()
        }
        return Static.instance
    }
    
    
    func configure(viewController : WorkEducationViewController){
//        let router = ProfileRouter()
//        router.controller = viewController
        
        let presenter = WorkEducationPresenter()
        presenter.workEducationPresenterOutput = viewController
        
        let interactor = WorkEducationInteractor()
        interactor.workEducationInteractorOutput = presenter
        
       // viewController.router = router
        viewController.output = interactor
    }
    
    
}
