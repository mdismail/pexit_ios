//
//  WorkEducationPresenter.swift
//  PEXit
//
//  Created by Apple on 06/09/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation


protocol WorkEducationPresenterInput {
    func displayResult(response: NewPost.Response)
    
    
}
protocol WorkEducationPresenterOutput : class{
    func displayWorkProfileResult(response: NewPost.Response)
}


class WorkEducationPresenter : WorkEducationPresenterInput{
    
    
    weak var workEducationPresenterOutput : WorkEducationPresenterOutput!
    
    func displayResult(response: NewPost.Response) {
        workEducationPresenterOutput.displayWorkProfileResult(response: response)
    }
//
//    func profileUpdateResult(response : NewPost.Response){
//        profilePresenterOutput.profileUpdateResponse(response : response)
//    }
}
