//
//  ProfileRouter.swift
//  PEXit
//
//  Created by Apple on 30/08/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation
import UIKit

protocol ProfileRouterInput{
    
}


class ProfileRouter:NSObject,ProfileRouterInput,UIPopoverPresentationControllerDelegate{
    weak var controller : ProfileViewController!
    
    func navigateToPopUpController(sender:UITextField,optionsArray:[String]?){
        
        let popUpController = controller.storyboard?.instantiateViewController(withIdentifier: "commonViewController") as! CommonViewController
        popUpController.modalPresentationStyle = .popover
        popUpController.preferredContentSize = CGSize(width : controller.view.frame.size.width , height : CGFloat((optionsArray?.count)! * COMMON_CONTROLLER_ROW_HEIGHT))
        let popOver = popUpController.popoverPresentationController
        popOver?.delegate = self
        popOver?.permittedArrowDirections = .up
        popOver?.sourceView = sender
        popOver?.sourceRect = sender.bounds
        popUpController.delegate = controller
        popUpController.itemsArray = optionsArray
        controller.present(popUpController, animated: true, completion: nil)
        
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return .none
    }
    
    

    
}


