//
//  ProfileSuperViewController.swift
//  PEXit
//
//  Created by Apple on 06/08/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import Material

class ProfileSuperViewController: UIViewController ,TabBarDelegate {
    fileprivate var buttons = [TabItem]()
    fileprivate var tabBar: TabBar!
    @IBOutlet weak var parentView: UIView!
    var isFromHome = false
    private lazy var profileViewController: ProfileViewController = {
        var viewController = storyboard?.instantiateViewController(withIdentifier: "profileViewController") as! ProfileViewController
        self.add(asChildViewController: viewController)
        return viewController
    }()
    
    private lazy var workEducationController: WorkEducationViewController = {
        var viewController = storyboard?.instantiateViewController(withIdentifier: "workEductaionController") as! WorkEducationViewController
        self.add(asChildViewController: viewController)
        return viewController
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareButtons()
        prepareTabBar()
        addSubView(asChildViewController: profileViewController)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        if isFromHome{
//            addBackButton(withTitle: "Edit Profile")
//        }
        if tabBar.selectedTabItem?.tag != 1{
            parent?.navigationItem.rightBarButtonItem = nil
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    fileprivate func prepareButtons() {
        let btnBasicInfo = TabItem(title: "Basic Information", titleColor: Color.black)
        btnBasicInfo.pulseAnimation = .none
        btnBasicInfo.tag = 1
        buttons.append(btnBasicInfo)
        
        let btnWorkEducation = TabItem(title: "Work & Education", titleColor: Color.black)
        btnWorkEducation.pulseAnimation = .none
        btnWorkEducation.tag = 2
        buttons.append(btnWorkEducation)
        
    }
    
    fileprivate func prepareTabBar() {
        tabBar = TabBar()
        tabBar.delegate = self
        tabBar.dividerColor = Color.grey.lighten2
        tabBar.dividerAlignment = .bottom
        tabBar.lineColor = Color.darkGray
        tabBar.lineAlignment = .bottom
        tabBar.backgroundColor = Color.grey.lighten5
        tabBar.tabItems = buttons
        view.layout(tabBar).horizontally().top()
    }
    
    func addSubView(asChildViewController viewController: UIViewController) {
        // Add Child View Controller
        addChildViewController(viewController)
        // Add Child View as Subview
        parentView.addSubview(viewController.view)
        // Configure Child View
        viewController.view.frame = parentView.bounds
        viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        // Notify Child View Controller
        viewController.didMove(toParentViewController: self)
    }
    
    func removeSubView(asChildViewController viewController: UIViewController) {
        // Notify Child View Controller
        viewController.willMove(toParentViewController: nil)
        
        // Remove Child View From Superview
        viewController.view.removeFromSuperview()
        
        // Notify Child View Controller
        viewController.removeFromParentViewController()
    }
    
    @objc
    func tabBar(tabBar: TabBar, didSelect tabItem: TabItem) {
        if workEducationController.isEdit == true{
            tabBar.select(at: 1)
            displayToast("Please save/discard the changes")
            return
        }
        if self.childViewControllers.count > 0{
            removeSubView(asChildViewController: self.childViewControllers[0])
        }
        if tabItem.tag == 1{
        parent?.showNavigationBarWithLeftAndRightBarButtonItems(languageTranslator: "language")
            addSubView(asChildViewController: profileViewController)
        }else{
            parent?.navigationItem.rightBarButtonItem = nil
            // parennavigationItem.rightBarButtonItem = nil
            addSubView(asChildViewController: workEducationController)
        }
        
    }
    
    func displayLanguage(){
        profileViewController.displayLanguages()
    }
    
    
}
