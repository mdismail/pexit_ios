//
//  ProfileInteractor.swift
//  PEXit
//
//  Created by Apple on 29/08/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation


protocol ProfileInteractorInput {
    func fetchUserProfile()
    func updateUserProfile(paramsDict : [String:Any],profileImage:Data?,request:NewPost.Request)
    
}

protocol ProfileInteractorOutput{
    func displayResult(message:String?, status:Bool)
    func profileUpdateResult(response : NewPost.Response)
    
}

class ProfileInteractor : ProfileInteractorInput{
   
    
    var profileInteractorOutput : ProfileInteractorOutput!
    var worker = ProfileWorker()
    
    func fetchUserProfile() {
        worker.fetchUserProfile{ [weak self] (message,status) in
            self?.profileInteractorOutput.displayResult(message: message, status: status)
        }

    }
    
    func updateUserProfile(paramsDict: [String : Any],profileImage:Data?, request: NewPost.Request) {
        worker.saveUserProfile(params: paramsDict,profileImage:profileImage, request: request) {[weak self] (response) in
            self?.profileInteractorOutput.profileUpdateResult(response: response)
        }
    }
    
    
}
