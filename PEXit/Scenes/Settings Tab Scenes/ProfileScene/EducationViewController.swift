//
//  EducationViewController.swift
//  PEXit
//
//  Created by Apple on 05/09/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit


class EducationViewController: UIViewController , ItemSelected , UITextFieldDelegate , UITextViewDelegate{
    
    @IBOutlet weak var txtFieldFromYear: TJTextField!
    @IBOutlet weak var txtFieldToYear: TJTextField!
    @IBOutlet weak var txtFieldDegree: TJTextField!
    @IBOutlet weak var txtFieldSchool: TJTextField!
    @IBOutlet weak var txtFieldModeOfStudy: TJTextField!
    @IBOutlet weak var txtFieldStudy: TJTextField!
    @IBOutlet weak var txtViewActivities: UITextView!
    @IBOutlet weak var txtViewDescription: UITextView!
    @IBOutlet weak var heightDescriptionConstraint: NSLayoutConstraint!
    @IBOutlet weak var heightActivitiesConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblDegree: UILabel!
    @IBOutlet weak var txtFieldOtherDegreeHeight: NSLayoutConstraint!
    @IBOutlet weak var txtFieldOther: TJTextField!
    @IBOutlet weak var lblOtherDegreeHeight: NSLayoutConstraint!
    
    @IBOutlet weak var lblOtherModel: UILabel!
    @IBOutlet weak var txtFieldOtherModeHeight: NSLayoutConstraint!
    @IBOutlet weak var txtFieldOtherMode: TJTextField!
    @IBOutlet weak var lblOtherModeHeight: NSLayoutConstraint!
    var router = WorkEducationRouter()
    var currentTextField : TJTextField?
    var delegate : HeightUpdates?
    var currentEducationModel : EducationModel?
    var updatedEduModel : EducationModel?
    var paramsDict = [String : Any]()
    var educationModelId : Int?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        hideView()
        hideOtherModeView()
        txtFieldSchool.addPreviousNextDoneOnKeyboard(withTarget: self, previousAction: #selector(self.previousAction(_:)), nextAction: #selector(self.nextAction(_:)), doneAction: #selector(self.doneAction(_:)), shouldShowPlaceholder: true)
        
        txtViewActivities.addPreviousNextDoneOnKeyboard(withTarget: self, previousAction: #selector(self.previousAction(_:)), nextAction: #selector(self.nextAction(_:)), doneAction: #selector(self.doneAction(_:)), shouldShowPlaceholder: true)

        txtViewDescription.leftAlignTextView()
        txtViewActivities.leftAlignTextView()
//        txtViewDescription.placeholder = textViewPlaceholder
//        txtViewActivities.placeholder = "Activities"
      
    }
    
    func hideView()
    {
        txtFieldOtherDegreeHeight.constant = 0
        lblOtherDegreeHeight.constant = 0
        txtFieldOther.isHidden = true
        lblDegree.isHidden = true
        delegate?.updateHeight(height: 0)

        
    }
    
    func showView(){
        txtFieldOtherDegreeHeight.constant = 30
        lblOtherDegreeHeight.constant = 12
        txtFieldOther.isHidden = false
        lblDegree.isHidden = false
        delegate?.updateHeight(height: 60)
    }
    
    func hideOtherModeView()
    {
        txtFieldOtherModeHeight.constant = 0
        lblOtherModeHeight.constant = 0
        txtFieldOtherMode.isHidden = true
        lblOtherModel.isHidden = true
        delegate?.updateHeight(height: 0)
        
        
    }
    
    func showOtherModeView(){
        txtFieldOtherModeHeight.constant = 30
        lblOtherModeHeight.constant = 12
        txtFieldOtherMode.isHidden = false
        lblOtherModel.isHidden = false
        delegate?.updateHeight(height: 60)
    }
 
    
    @objc func previousAction(_ sender : UITextField!) {
        if txtFieldSchool.isFirstResponder{
            txtFieldStudy.becomeFirstResponder()
        }
        if txtViewActivities.isFirstResponder{
            txtFieldModeOfStudy.becomeFirstResponder()
        }
    }
    
    @objc func nextAction(_ sender : UITextField!) {
        if (txtFieldSchool.isFirstResponder)
        {
            txtFieldDegree.becomeFirstResponder()
        }
        if txtViewActivities.isFirstResponder{
            txtViewDescription.becomeFirstResponder()
        }
    }
    
    @objc func doneAction(_ sender : UITextField!) {
        self.view.endEditing(true)
    }
    
    //MARK: - TextField Delegate
  
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        let textField = textField as! TJTextField
        currentTextField = textField
        switch textField{
        case txtFieldFromYear:
            self.dismissKeyboard()
            router.navigateToPopUpController(sender: textField, optionsArray: Utils.getListOfYears(addCount: FROM_YEAR_CONSTANT),controller:self)
        case txtFieldToYear:
            self.dismissKeyboard()
            router.navigateToPopUpController(sender: textField, optionsArray: Utils.getListOfYears(addCount: TO_YEAR_CONSTANT),controller:self)
        case txtFieldDegree:
            self.dismissKeyboard()
            router.navigateToPopUpController(sender: textField, optionsArray: DEGREE_OPTIONS,controller:self)
        case txtFieldModeOfStudy:
            self.dismissKeyboard()
            router.navigateToPopUpController(sender: textField, optionsArray: MODE_OF_STUDY_OPTIONS,controller:self)
        default:
            return true
        }
//        DispatchQueue.main.asyncAfter(deadline: .now() + 0.08) {
//            self.dismissKeyboard()
//        }
        return false
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField{
        case txtFieldSchool:
            paramsDict["pedupschoolname"] = textField.text
        case txtFieldStudy:
            paramsDict["peduschoolstudy"] = textField.text
        case txtFieldOther:
            paramsDict["potherdegree"] = textField.text
        case txtFieldOtherMode:
            paramsDict["peduschoolograde"] = textField.text
        default:
            print("Test")
        }
    }
    
    //MARK: - Item Selected Delegate
    
    func optionSelected(option: String?) {
        currentTextField?.text = option
        switch currentTextField{
        case txtFieldDegree:
           
            if option == "Other"{
                paramsDict["peduschoolDegree"] = "Other"
                txtFieldOther.becomeFirstResponder()
                showView()
            }else{
                paramsDict["peduschoolDegree"] = option
                txtFieldFromYear.becomeFirstResponder()
                hideView()
            }
        case txtFieldModeOfStudy:
            if option == "Other"{
                paramsDict["peduschoolgrade"] = "Other"
                txtFieldOtherMode.becomeFirstResponder()
                showOtherModeView()
            }else{
                paramsDict["peduschoolgrade"] = option
                hideOtherModeView()
            }
            
        case txtFieldFromYear:
            paramsDict["peduschoolfromyear"] = Int(option!)
        case txtFieldToYear:
            paramsDict["peduschooltoyear"] = Int(option!)
        default:
            break
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
       // textView.updateTextViewPlacholder()
//        let size = textView.sizeThatFits(CGSize(width: textView.frame.size.width, height: CGFloat.greatestFiniteMagnitude))
//        var needUpdate = false
        switch textView{
        case txtViewDescription:
            paramsDict["peduexp_desc"] = textView.text
            caculateTxtHeight(txtView: txtViewDescription, heightConstraint: heightDescriptionConstraint)
//
//            if size.height != heightDescriptionConstraint.constant {
//                heightDescriptionConstraint.constant = size.height
//                needUpdate = true
//            }
        case txtViewActivities:
            paramsDict["peduschool_act"] = textView.text
            caculateTxtHeight(txtView: txtViewActivities, heightConstraint: heightActivitiesConstraint)
//            if size.height != heightActivitiesConstraint.constant {
//                heightActivitiesConstraint.constant = size.height
//                needUpdate = true
//            }
        default:
            break
        }
        
//        if (needUpdate){
//            delegate?.updateHeight(height: size.height)
//        }
    }
    
    
    
    func caculateTxtHeight(txtView:UITextView,heightConstraint:NSLayoutConstraint){
        let size = txtView.sizeThatFits(CGSize(width: txtView.frame.size.width, height: CGFloat.greatestFiniteMagnitude))
        if size.height != heightConstraint.constant {
            heightConstraint.constant = size.height
            delegate?.updateHeight(height: size.height)
        }
    }
    
    func setEducationEditData(currentModel : EducationModel){
        if currentEducationModel == nil || (currentEducationModel != updatedEduModel && currentEducationModel != nil){
            currentEducationModel = currentModel
            paramsDict["pedupschoolname"] = currentEducationModel?.pedupschoolname
            paramsDict["peduschoolstudy"] = currentEducationModel?.peduschoolstudy
            paramsDict["peduschoolfromyear"] = currentEducationModel?.peduschoolfromyear
            paramsDict["peduschooltoyear"] = currentEducationModel?.peduschooltoyear
            paramsDict["peduschoolDegree"] = currentEducationModel?.peduschoolDegree
            paramsDict["peduschoolgrade"] = currentEducationModel?.peduschoolgrade
            paramsDict["peduschool_act"] = currentEducationModel?.peduschool_act
            paramsDict["peduexp_desc"] = currentEducationModel?.peduexp_desc
            paramsDict["potherdegree"] = currentEducationModel?.peduschoolODegree
            paramsDict["peduschoolograde"] = currentEducationModel?.peduschoolograde
        }
        if !(currentModel.isInvalidated){
             educationModelId = currentModel.pedupid
        }
        
        txtFieldSchool.text = paramsDict["pedupschoolname"] as? String
        txtFieldStudy.text = paramsDict["peduschoolstudy"] as? String
        if paramsDict["peduschoolfromyear"] as? Int != 0{
            txtFieldFromYear.text = (paramsDict["peduschoolfromyear"] as? Int).map(String.init) ?? ""
        }else{
            txtFieldFromYear.text = ""
        }
        if paramsDict["peduschooltoyear"] as? Int != 0{
            txtFieldToYear.text = (paramsDict["peduschooltoyear"] as? Int).map(String.init) ?? ""
        }else{
            txtFieldToYear.text = ""
        }
        if paramsDict["potherdegree"] as? Bool == true{
            showView()
            txtFieldDegree.text = "Other"
            txtFieldOther.text = paramsDict["peduschoolDegree"] as? String
        }else{
            hideView()
            txtFieldDegree.text = paramsDict["peduschoolDegree"] as? String
        }
        if paramsDict["peduschoolograde"] as? Bool == true{
            showOtherModeView()
            txtFieldModeOfStudy.text = "Other"
            txtFieldOtherMode.text = paramsDict["peduschoolgrade"] as? String
        }else{
            hideOtherModeView()
            txtFieldModeOfStudy.text = (paramsDict["peduschoolgrade"] as? String)?.capitalized
        }
       
        //txtFieldModeOfStudy.text = paramsDict["peduschoolgrade"] as? String
        txtViewActivities.text = paramsDict["peduschool_act"] as! String
        caculateTxtHeight(txtView: txtViewActivities, heightConstraint: heightActivitiesConstraint)
       // txtViewActivities.updateTextViewPlacholder()
        txtViewDescription.text = paramsDict["peduexp_desc"] as? String
        caculateTxtHeight(txtView: txtViewDescription, heightConstraint: heightDescriptionConstraint)
       // txtViewDescription.updateTextViewPlacholder()
    }
    
    
    
    func clearData(){
        if currentEducationModel != nil{
            currentEducationModel = nil
            addNewClicked()
        }
        
    }
    
    func addNewClicked(){
        if txtFieldSchool != nil{
            txtFieldSchool.text = ""
            txtFieldStudy.text = ""
            txtFieldFromYear.text = ""
            txtFieldToYear.text = ""
            txtFieldDegree.text = ""
            txtFieldModeOfStudy.text = ""
            txtViewActivities.text = ""
            txtFieldOther.text = ""
            txtFieldOtherMode.text = ""
            currentEducationModel = nil
            educationModelId = nil
            txtViewDescription.text = ""
            hideView()
            hideOtherModeView()
            heightDescriptionConstraint.constant = 30
            heightActivitiesConstraint.constant = 30
            txtViewDescription.updateTextViewPlacholder()
            txtViewActivities.updateTextViewPlacholder()
        }
        
    }
    
    func fetchEducationData() -> WorkEducationRequestModel.AddEducationModel?{
        if Utils.validateFormAndShowToastWithRequiredFields([txtFieldSchool]){
            if ((txtFieldFromYear.text?.isEmpty)! && !(txtFieldToYear.text?.isEmpty)!) {
                displayToast("Please enter from year")
                return nil
            }
            else if (!(txtFieldFromYear.text?.isEmpty)! && (txtFieldToYear.text?.isEmpty)!){
                 displayToast("Please enter to year")
                 return nil
            }
            else if txtFieldFromYear.text > txtFieldToYear.text{
                displayToast("From year should be less than to year")
                return nil
            }
            if txtFieldOther.isHidden == false{
                if (txtFieldOther.text?.isEmpty)!{
                    displayToast("Please enter name of the degree")
                    return nil
                }
            }
            if txtFieldOtherMode.isHidden == false{
                if (txtFieldOtherMode.text?.isEmpty)!{
                    displayToast("Please enter mode of study")
                    return nil
                }
            }
            var requestModel = WorkEducationRequestModel.AddEducationModel()
            requestModel.schoolname = txtFieldSchool.text
            requestModel.schoolfromyear = txtFieldFromYear.text
            requestModel.schooltoyear = txtFieldToYear.text
            requestModel.schoolDegree = txtFieldDegree.text
            requestModel.schoolMode = txtFieldModeOfStudy.text?.lowercased()
            requestModel.schoolstudy = txtFieldStudy.text
            requestModel.odegre = txtFieldOther.text ?? ""
            requestModel.omod = txtFieldOtherMode.text ?? ""
            requestModel.school_act = txtViewActivities.text
            requestModel.exp_desc = txtViewDescription.text
            if currentEducationModel != nil {
                requestModel.isEdit = true
                requestModel.currentModelId = educationModelId!
                //requestModel.currentModelId = (currentEducationModel?.pedupid)!
            }
            return requestModel
        }
        return nil
    }
    
    
}
