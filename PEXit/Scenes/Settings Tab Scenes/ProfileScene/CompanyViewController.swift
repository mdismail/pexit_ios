//
//  CompanyViewController.swift
//  PEXit
//
//  Created by Apple on 05/09/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import MobileCoreServices
import UIKit
import ActionSheetPicker_3_0

class CompanyViewController: UIViewController , UITableViewDataSource , UITableViewDelegate , UITextFieldDelegate{
    @IBOutlet weak var tblAddedFiles: UITableView!
    @IBOutlet weak var txtFieldState: TJTextField!
    @IBOutlet weak var txtFieldCity: TJTextField!
    @IBOutlet weak var txtFieldCountry: TJTextField!
    @IBOutlet weak var tblFileArrayHeightConstraint: NSLayoutConstraint!
    var fileArray = [MediaAttatchment]()
//    var delegate : PassAttatchmentsToController?
    var countryArray = [String]()
    var stateArray = [String]()
    var cityArray = [String]()
    var delegate : HeightUpdates?
    var paramsDict = [String : Any]()
    var currentCompanyModel : CompanyModel?
    var companyModelId : Int?

    override func viewDidLoad() {
        super.viewDidLoad()
        tblFileArrayHeightConstraint.constant = 0
        // Do any additional setup after loading the view.
    }

    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        let textField = textField as! TJTextField
//        currentTextField = textField
        if textField == txtFieldCountry {
            fetchCountries(textField: textField)
            self.view.endEditing(true)
            return false
        }
        if textField == txtFieldCity {
            fetchCities(textField: textField)
            self.view.endEditing(true)
            return false
        }
        if textField == txtFieldState {
            fetchStates(textField: textField)
            self.view.endEditing(true)
            return false
        }
        
        return true
    }
    
    
    func setCompanyDataForEdit(currentModel : CompanyModel){
        if currentCompanyModel == nil{
            currentCompanyModel = currentModel
            paramsDict["pcomcity"] = currentCompanyModel?.pcomcity
            paramsDict["pcomcountry"] = currentCompanyModel?.pcomcountry
            paramsDict["pcomstates"] = currentCompanyModel?.pcomstates
           
        }
        if !(currentModel.isInvalidated){
            companyModelId = currentCompanyModel?.pcomid
        }
        txtFieldState.text = paramsDict["pcomstates"] as? String
        txtFieldCity.text = paramsDict["pcomcity"] as? String
        txtFieldCountry.text = paramsDict["pcomcountry"] as? String
        
    }
    
    func clearData(){
        if currentCompanyModel != nil{
            currentCompanyModel = nil
            companyModelId = nil
            txtFieldState.text = ""
            txtFieldCity.text = ""
            txtFieldCountry.text = ""
        }
        if tblAddedFiles != nil{
            fileArray.removeAll()
            tblAddedFiles.reloadData()
            tblFileArrayHeightConstraint.constant = 0
        }
       
        
    }

    
    func fetchCountries(textField : UITextField){
        let worker = RegistrationWorker()
        worker.fetchCountries { [weak self](countryResponse) in
            if countryResponse.datas != nil{
                self?.countryArray.removeAll()
                for country in countryResponse.datas!{
                    self?.countryArray.append(country.name!)
                }
                DispatchQueue.main.async {
                    self?.showCountryPicker(textField: textField,title:"Country",currentArray: (self?.countryArray)!)
                }
            }
            
        }
    }
    
    
    func fetchStates(textField : UITextField){
        let worker = RegistrationWorker()
        if !(txtFieldCountry.text?.isEmpty)!{
            worker.fetchStates(countryName: txtFieldCountry.text!) {[weak self] (stateResponse) in
                        if stateResponse.datas != nil{
                            self?.stateArray.removeAll()
                            for state in stateResponse.datas!{
                                self?.stateArray.append(state.name!)
                            }
                            DispatchQueue.main.async {
                                self?.showCountryPicker(textField: textField,title:"State",currentArray:(self?.stateArray)!)
                            }
                        }
                        
                    }
                }else{
                    self.displayToast(CHOOSE_COUNTRY_TOAST)
                }
                
            }
        
    
    
    func fetchCities(textField : UITextField){
        let worker = RegistrationWorker()
        if !(txtFieldState.text?.isEmpty)!{
                worker.fetchCities(stateName: txtFieldState.text!) {[weak self] (cityResponse) in
                        
                        if cityResponse.datas != nil{
                            self?.cityArray.removeAll()
                            for city in cityResponse.datas!{
                                self?.cityArray.append(city.name!)
                            }
                            DispatchQueue.main.async {
                                self?.showCountryPicker(textField: textField,title:"City",currentArray:(self?.cityArray)!)
                            }
                        }
                        
                    }
                }
                else{
                    self.displayToast(CHOOSE_STATE_TOAST)
                }
                
            }
        
    
    func showCountryPicker(textField : UITextField , title : String , currentArray : [String]){
        ActionSheetStringPicker.show(withTitle: title, rows: currentArray, initialSelection: 0, doneBlock: {[unowned self] (key, index, value) in
            textField.text  = value as? String
            let selectedIndexPath: IndexPath = IndexPath(row: textField.tag, section: 0)
           
            switch title{
            case "Country":
                self.txtFieldState.text = ""
                self.txtFieldCity.text = ""
                self.paramsDict["pcomcountry"] = textField.text
            case "State":
                self.paramsDict["pcomstates"] = textField.text
                self.txtFieldCity.text = ""
            case "City":
                self.paramsDict["pcomcity"] = textField.text
                print("")
                
            default:
                print("")
            }
            
            }, cancel: { (picker) in
                
        }, origin: self.view)
    }
    
    @IBAction func btnAddCompanyProfile(_ sender: Any) {
        // Attatch Files
        if fileArray.count < 1{
            FileAttatchmentHandler.shared.showDocumentPicker(vc: self, files: [String(kUTTypePDF)])
            FileAttatchmentHandler.shared.filePickedBlock = {[unowned self] (url) in
                print(url)
//                self.delegate?.updateHeightOfParentController(height: 100)
                print("File Name is ---->",url.lastPathComponent)
//                let newIndexPath = IndexPath(item: 0, section: 0)
                do {
                    let data = try Data(contentsOf: url, options: .mappedIfSafe)
                    print(data)
                    let media = MediaAttatchment(file: nil, fileName: url.lastPathComponent,fileData:data,fileExtension:url.mimeType())
                    self.addNewFile(currentFile: media)
                    
                } catch  {
                }
            }
        }else{
            displayToast("Only 1 profile can be added")
        }
       
    }
    
    func addNewFile(currentFile : MediaAttatchment)
    {
        tblFileArrayHeightConstraint.constant = 100
        if let delegate = delegate{
            delegate.updateHeight(height: tblFileArrayHeightConstraint.constant)
        }
        let newIndexPath = IndexPath(item: 0, section: 0)
        self.fileArray.insert(currentFile, at: 0)
        tblAddedFiles.insertRows(at: [newIndexPath], with: .automatic)
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fileArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let uploadCell = tableView.dequeueReusableCell(withIdentifier: "addCompanyCell", for: indexPath) as! AttatchmentTableViewCell
        let currentAttactchement = fileArray[indexPath.row]
        uploadCell.btnDelete.tag = indexPath.row
        uploadCell.setData(currentAttatchment : currentAttactchement)
        return uploadCell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 85
    }
    
    func fetchCompanyData() -> WorkEducationRequestModel.AddCompanyModel?{
        let textFieldArray = [txtFieldCountry,txtFieldState,txtFieldCity]
        if fileArray.count == 0 && currentCompanyModel == nil{
            displayToast(ADD_COMPANY_PROFILE)
            return nil
        }
        if Utils.validateFormAndShowToastWithRequiredFields(textFieldArray as! [TJTextField]){
            var requestModel = WorkEducationRequestModel.AddCompanyModel()
            requestModel.companyCountry = txtFieldCountry.text
            requestModel.companyState = txtFieldState.text
            requestModel.companyCity = txtFieldCity.text
            requestModel.documents = fileArray
            
            if currentCompanyModel != nil{
                requestModel.isEdit = true
                requestModel.currentModelId = companyModelId!
            }
            
            
            return requestModel
        }
        return nil
    }
}




