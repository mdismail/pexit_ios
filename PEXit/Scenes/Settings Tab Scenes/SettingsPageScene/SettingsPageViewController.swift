//
//  SettingsPageViewController.swift
//  PEXit
//
//  Created by Apple on 09/07/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import Presentr
import Realm
import FirebaseInstanceID
import Firebase
import SDWebImage
import GoogleSignIn
import LinkedinSwift

class SettingsPageViewController: UIViewController , ItemSelected {
    
    private lazy var profileViewController: ProfileSuperViewController = {
        var viewController = storyboard?.instantiateViewController(withIdentifier: "profileSuperViewController") as! ProfileSuperViewController
        self.add(asChildViewController: viewController)
        return viewController
    }()
    private lazy var networkParentController: MyNetworkParentViewController = {
        var viewController = storyboard?.instantiateViewController(withIdentifier: "networkParentController") as! MyNetworkParentViewController
        return viewController
    }()
    private lazy var aboutUsParentController: AboutUsParentViewController = {
        var viewController = storyboard?.instantiateViewController(withIdentifier: "aboutUsParentViewController") as! AboutUsParentViewController
        return viewController
    }()
    private lazy var changePasswordController: ChangePasswordViewController = {
        var viewController = storyboard?.instantiateViewController(withIdentifier: "changePasswordViewController") as! ChangePasswordViewController
        return viewController
    }()
    private lazy var transactionHistoryViewController: TransactionHistoryViewController = {
        var viewController = storyboard?.instantiateViewController(withIdentifier: "transactionHistoryViewController") as! TransactionHistoryViewController
        return viewController
    }()
    
    private lazy var homePageController: HomePageViewController = {
        var viewController = self.storyboard?.instantiateViewController(withIdentifier: "HomePageViewController") as! HomePageViewController
        return viewController
        
        
    }()
    
    
    var networkControllerType = "Edit Profile"
    var currentController = ""

    
    override func viewDidLoad() {
        super.viewDidLoad()
        showNavigationBar()
        setUpHeaderView()
       // add(asChildViewController: profileViewController)
        showNavigationBarWithLeftAndRightBarButtonItems(languageTranslator: "language")
        
        // Do any additional setup after loading the view.
    }
    
    func setUpHeaderView(option : String? = "Edit Profile"){
        if self.navigationController != nil && (self.navigationController?.viewControllers.count)! >= 1{
            if self.navigationController?.viewControllers[0] is HomePageViewController{
                addBackButton(withTitle: option)
            }else{
                if networkControllerType.isEmpty{
                     addNavigationBarWithTitleAndSubtitle(title: "Settings", subTitle: option,size:CGFloat(120))
                }else{
                     addNavigationBarWithTitleAndSubtitle(title: "Settings", subTitle: networkControllerType,size:CGFloat(120))
                }
               
            }
            
        }
        else{
            if networkControllerType.isEmpty{
                addNavigationBarWithTitleAndSubtitle(title: "Settings", subTitle: option,size:CGFloat(120))
            }else{
                addNavigationBarWithTitleAndSubtitle(title: "Settings", subTitle: networkControllerType,size:CGFloat(120))
            }
        }
        
    }
    
//    override func viewWillAppear(_ animated: Bool) {
//        if CURRENT_GLOBAL_SEARCH_OPTION == GLOBAL_SEARCH_TYPE.CONNECTIONS.rawValue {
//            optionSelected(option: "My Network")
//        }
//        
//    }
    
    override func viewWillAppear(_ animated: Bool) {
        if (!networkControllerType.isEmpty) {
            optionSelected(option: networkControllerType)
            networkControllerType = ""
        }
    }
    
    override func searchLocallyUsingGlobal(searchKeyword : String, searchOptions : String){
        
        networkControllerType = "My Circle"
        networkParentController.searchLocallyUsingGlobal_subview(searchKeyword: searchKeyword, searchOptions : searchOptions)
        addGlobalSearchRefreshBtn()
        
    }
    
    override func cleanUpGlobalSearch(previousSearchOption : String?){
        networkControllerType = ""
        networkParentController.cleanUpGlobalSearch_subview()
        removeGlobalSearchRefreshBtn()
    }
    
    override func refreshView(previousSearchOption: String?) {
         networkParentController.refresh_SubView()
    }
    
    override func clickOnSubtitleButton(_ sender: UIButton) {
        let presenter = Presentr(presentationType: .popup)
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "commonViewController") as! CommonViewController
//        controller.itemsArray = ["Edit Profile","My Posts","My Circle","Change Password","Transaction History","About","Logout"]
        controller.itemsArray = ["Edit Profile","My Posts","My Circle","Change Password","About","Logout"]
        controller.delegate = self
        customPresentViewController(presenter, viewController: controller, animated: true)
    }
    
    func optionSelected(option: String?) {
        
//        if self.childViewControllers.count > 0 && option != "Logout"{
//            remove(asChildViewController: self.childViewControllers[0])
//            addNavigationBarWithTitleAndSubtitle(title: "Settings", subTitle: option)
//        }
        
        if self.childViewControllers.count > 0 && option != "Logout"{
            
            if option == currentController{
                print("CONTROLLER DID NOT CHANGE----->",currentController)
                return
            }
            navigationItem.rightBarButtonItem = nil
            removeGlobalSearchRefreshBtn()
            self.childViewControllers[0].cleanUpGlobalSearch_subview()
            remove(asChildViewController: self.childViewControllers[0])
           
        }
        currentController = option!
//        addNavigationBarWithTitleAndSubtitle(title: "Settings", subTitle: option,size:CGFloat(120))
    
        setUpHeaderView(option: option)
        
        switch option {
        case "Edit Profile":
            showNavigationBarWithLeftAndRightBarButtonItems(languageTranslator: "language")
            add(asChildViewController: profileViewController)
        case "My Posts":
            homePageController.othersPost = "myposts"
            add(asChildViewController: homePageController)
        case "My Circle":
            showNavigationBarWithLeftAndRightBarButtonItems(searchBarButton: "Search",showRefresh:true)
            add(asChildViewController: networkParentController)
        
        case "Change Password":
            add(asChildViewController: changePasswordController)
        case "About":
            add(asChildViewController: aboutUsParentController)
        case "Transaction History":
            add(asChildViewController: transactionHistoryViewController)
        case "Logout":
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                self.showCustomAlert(CONFIRMATION_ALERT, message: LOGOUT_ALERT, okButtonTitle: UIAlertActionTitle.LOGOUT.rawValue, cancelButtonTitle: UIAlertActionTitle.CANCEL.rawValue) { [unowned self](action) in
                    switch action{
                    case .DELETE:
                        //                        let instance = FIRInstanceID.instanceID()
                        //                        instance.delete(handler: { (error) in
                        //                            if error != nil{
                        //                                print(error.debugDescription);
                        //                            } else {
                        //                                print("Token Deleted");
                        //                            }
                        //                        })
                      
                        GIDSignIn.sharedInstance().signOut()
                        LinkedInManager.sharedLinkedInManager.linkedInLogout()
                        SDImageCache.shared().clearMemory()
                        SDImageCache.shared().clearDisk()
                        Defaults.setLoginStatus(false)
                        RealmDBManager.sharedManager.deleteRealmDb()
                        Utils.redirectToLoginScreen()
                        
                        
                    case .CANCEL:
                        print("Dismiss Alert")
                    default:
                        print("Test")
                    }
                }
            }
            
            
        default:
            add(asChildViewController: profileViewController)
        }
        
    }
    
    override func showLanguages() {
        profileViewController.displayLanguage()
    }
    
}
