//
//  SponsorContentDetailViewController.swift
//  PEXit
//
//  Created by ats on 23/11/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class SponsorContentDetailViewController: UIViewController {

    @IBOutlet weak var tblMedia: UITableView!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    var currentSponsorModel : SponsorModel?
    lazy var dataController: MediaDataSourceAndDelegate = {
        let dataController = MediaDataSourceAndDelegate()
        return dataController
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        tblMedia.register(UINib(nibName: "HomeDetailTableViewCell", bundle: nil), forCellReuseIdentifier: "homeDetailCell")
        tblMedia.register(UINib(nibName: "MediaTableViewCell", bundle: nil), forCellReuseIdentifier: "mediaLinkCell")
        tblMedia.dataSource = dataController
        tblMedia.delegate = dataController
        tblMedia.tableFooterView = UIView()
        addBackButton(withTitle : "Sponsored Content")
        lblTitle.text = currentSponsorModel?.title
        lblDescription.text = currentSponsorModel?.post?.htmlDecodedText()
        self.dataController.configureMediaTbl(responseModel: currentSponsorModel!, controller: self)
        self.tblMedia.reloadData()
        // Do any additional setup after loading the view.
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        guard let headerView = tblMedia.tableHeaderView else {
            return
        }
        let size = headerView.systemLayoutSizeFitting(UILayoutFittingCompressedSize)
        
        if headerView.frame.size.height != size.height {
            headerView.frame.size.height = size.height
            tblMedia.tableHeaderView = headerView
            tblMedia.layoutIfNeeded()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    

}
