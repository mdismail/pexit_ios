//
//  CheckoutPresenter.swift
//  PEXit
//
//  Created by ats on 30/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation


protocol CheckoutInput{
   func presentCheckoutResponse(response: CheckOutResponseModel?)
   func displayResponse(response : ServicePostResponseModel?,message : String?)
   func displayPriceResponse(response : PriceValueModel?,message : String?)
 
}

protocol CheckoutOutput: class{
    func presentCheckoutResponse(response:CheckOutDataModel?,message: String?)
    func displayResponse(response :ServicePostResponseModel?,message: String?)
    func displayPriceResponse(value : Int?,tax:Int?,message : String?)
  
}

class CheckoutPresenter : CheckoutInput{
    weak var output: CheckoutOutput!
    
   func presentCheckoutResponse(response: CheckOutResponseModel?){
        if response != nil && response?.status == true{
            output.presentCheckoutResponse(response: response?.datas, message: "")
        }else{
            response != nil ? output.presentCheckoutResponse(response: nil, message: response?.message) :
                output.presentCheckoutResponse(response: nil, message: UNKNOWN_ERROR_MSG)
        }
    }
    
    func displayResponse(response : ServicePostResponseModel?,message : String?){
        if response != nil && response?.status == true{
            output.displayResponse(response: response,message:response?.message)
        }else{
            response != nil ? output.displayResponse(response: response,message: response?.message) :
                output.displayResponse(response:nil,message: UNKNOWN_ERROR_MSG)
        }
    }
    
    func displayPriceResponse(response : PriceValueModel?,message : String?){
        if response != nil && response?.status == true{
            output.displayPriceResponse(value:response?.datas,tax:response?.tax,message: "")
        }else{
            response != nil ? output.displayPriceResponse(value:nil, tax: nil,message: response?.message) :
                output.displayPriceResponse(value:nil, tax: nil,message: UNKNOWN_ERROR_MSG)
        }
    }
}
