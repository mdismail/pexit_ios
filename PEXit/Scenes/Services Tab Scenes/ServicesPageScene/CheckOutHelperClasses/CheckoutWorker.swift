//
//  CheckoutWorker.swift
//  PEXit
//
//  Created by ats on 30/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation

class CheckoutWorker{
    func fetchCheckoutOptions(requestObject:CheckoutRequestModel?,completionHandler: @escaping(_ response : AnyObject?,_ error:Error? ) -> Void) {
        START_LOADING_VIEW()
        ServiceManager.methodType(requestType: POST_REQUEST, url: CHECKOUT, params: ["service":requestObject?.service,"countryCode":requestObject?.countryCode,"stype":requestObject?.serviceType]  ,paramsData: nil,completion: { (response,_ responseData, statusCode) in
//            print("REsponse checkout options ---->",response)
            STOP_LOADING_VIEW()
            let decoder = JSONDecoder()
            do{
                let checkoutDetails = try decoder.decode(CheckOutResponseModel.self, from: responseData!)
                completionHandler(checkoutDetails,nil)
                
            }catch{
                DispatchQueue.main.async{
                    if ((response as! [String : Any])["message"] as? String) != nil{
                        KEY_WINDOW?.makeToast((response as! [String : Any])["message"] as? String)
                    }
                }
                
                print("error")
            }
            
        }) { (response, statusCode) in
            STOP_LOADING_VIEW()
        }
    }
    
    
    func fetchPriceList(params:[String:Any]?,completionHandler: @escaping(_ response : PriceValueModel?,_ error:Error? ) -> Void) {
        START_LOADING_VIEW()
        ServiceManager.methodType(requestType: POST_REQUEST, url: GET_PRICES, params: params as! NSDictionary  ,paramsData: nil,completion: { (response,_ responseData, statusCode) in
            STOP_LOADING_VIEW()
            let decoder = JSONDecoder()
            do{
                let priceModel = try decoder.decode(PriceValueModel.self, from: responseData!)
                completionHandler(priceModel,nil)
                
            }catch{
                DispatchQueue.main.async{
                    if ((response as! [String : Any])["message"] as? String) != nil{
                        KEY_WINDOW?.makeToast((response as! [String : Any])["message"] as? String)
                    }
                }
                
                print("error")
            }
            
        }) { (response, statusCode) in
            STOP_LOADING_VIEW()
        }
    }
    
    
    func applyPromoCode(params:[String:Any]?,completionHandler: @escaping(_ response : PriceValueModel?,_ error:Error? ) -> Void) {
        START_LOADING_VIEW()
        ServiceManager.methodType(requestType: POST_REQUEST, url: APPLY_PROMO_CODE, params: params as? NSDictionary  ,paramsData: nil,completion: { (response,_ responseData, statusCode) in
            STOP_LOADING_VIEW()
            let decoder = JSONDecoder()
            do{
                let priceModel = try decoder.decode(PriceValueModel.self, from: responseData!)
                completionHandler(priceModel,nil)
                
            }catch{
                DispatchQueue.main.async{
                    if ((response as! [String : Any])["message"] as? String) != nil{
                        KEY_WINDOW?.makeToast((response as! [String : Any])["message"] as? String)
                    }
                }
                
                print("error")
            }
            
        }) { (response, statusCode) in
            STOP_LOADING_VIEW()
        }
    }
    
    
    
    func postSponsorContent(apiName:String,requestObject:SponsoredContentRequestModel?,completionHandler: @escaping(_ response : ServicePostResponseModel?,_ error:Error? ) -> Void) {
        START_LOADING_VIEW()
        var params = ["stype":requestObject?.stype ?? "","sctitle":(requestObject?.title)!,"scdescription":(requestObject?.description)!,"sclink":requestObject?.links ??? "","scylink":requestObject?.youTubeLink ??? "","scorderid":requestObject?.orderId ?? "","scduration":requestObject?.duration ?? nil] as [String : Any]
        params["scpaid"] = requestObject?.paid
        params["scpromocode"] = requestObject?.promoCode
        params["sccurrency"] = requestObject?.currency
        params["sctax"] = requestObject?.tax
        print("PARAMS ARE ---->",params)
        AlamofireManager.shared().uploadSponsorContent(apiUrl: apiName,imageData: requestObject?.images,videoData: requestObject?.videos,fileData: requestObject?.files,pptData: requestObject?.ppt,parameters: params) { (response, error) in
            STOP_LOADING_VIEW()
            if response != nil{
                if !(((response!["errors"] as? String)?.isEmpty)!) || !((response!["virus"] as? String)?.isEmpty)!{
                    Utils.showErrorFilesAlert(errorFiles : response!["errors"] as? String,corruptFiles : response!["virus"] as? String)
                }
                let decoder = JSONDecoder()
                do{
                    let jsonData = try JSONSerialization.data(withJSONObject: response, options: .prettyPrinted)
                    let reqJSONStr = String(data: jsonData, encoding: .utf8)
                    let data = reqJSONStr?.data(using: .utf8)
                    let response = try decoder.decode(ServicePostResponseModel.self, from: data!)
                    completionHandler(response, nil)
                }catch{
                    print("error")
                }
            }else{
                STOP_LOADING_VIEW()
                KEY_WINDOW?.makeToast(error?.localizedDescription)
            }
            
        }
    }
    
    func postNewProduct(apiName:String,requestObject:NewProductRequestModel,completionHandler: @escaping(_ response : ServicePostResponseModel?,_ error:Error?) -> Void){
        
        var params = ["stype":requestObject.stype ?? "",
                      "pname":(requestObject.name)!,
                      "plocation":(requestObject.location)!,
                      "pcategory":(requestObject.category)!,
                      "pdescription":(requestObject.description)!,
                      "pcondition":(requestObject.condition)!,
                      "pgyoutube":requestObject.youTubeLink ?? "",
                      "pduration":requestObject.duration ?? nil,
                      "ppaid":requestObject.paid ?? "",
                      "ppromocode":requestObject.promoCode ?? "",
                      "ptax":requestObject.tax ?? "",
                      "porderid":requestObject.orderId ?? ""] as [String:Any]
        
        params["ppaid"] = requestObject.paid
        params["ppromocode"] = requestObject.promoCode
        params["pcurrency"] = requestObject.currency
       // params["ptax"] = requestObject.tax
        
        var mediaArray = ["pgimage[]":requestObject.images,
                          "pgvideo[]":requestObject.videos] as! [String : [MediaAttatchment]?]
        if requestObject.transaction != nil{
            mediaArray["ptransaction"] = [requestObject.transaction] as! [MediaAttatchment]
        }else{
             mediaArray["ptransaction"] = nil
        }
        if requestObject.logo != nil{
            mediaArray["plogo"] = [requestObject.logo] as! [MediaAttatchment]
        }else{
            mediaArray["plogo"] = nil
        }
        if requestObject.details != nil{
            mediaArray["pdetails"] = [requestObject.details] as! [MediaAttatchment]
        }else{
            mediaArray["pdetails"] = nil
        }
        
        print("PARAMeters are---->",params)
        AlamofireManager.shared().uploadServicesContent(apiUrl: apiName,attachmentArray: mediaArray,parameters: params) { (response, error) in
            STOP_LOADING_VIEW()
            if response != nil{
                if !(((response!["errors"] as? String)?.isEmpty)!) || !((response!["virus"] as? String)?.isEmpty)!{
                    Utils.showErrorFilesAlert(errorFiles : response!["errors"] as? String,corruptFiles : response!["virus"] as? String)
                }
                let decoder = JSONDecoder()
                do{
                    let jsonData = try JSONSerialization.data(withJSONObject: response, options: .prettyPrinted)
                    let reqJSONStr = String(data: jsonData, encoding: .utf8)
                    let data = reqJSONStr?.data(using: .utf8)
                    let response = try decoder.decode(ServicePostResponseModel.self, from: data!)
                    completionHandler(response, nil)
                }catch{
                    print("error")
                }
            }else{
                STOP_LOADING_VIEW()
                KEY_WINDOW?.makeToast(error?.localizedDescription)
            }
        }
        
    }
    
    
    func postNewService(apiName:String,requestObject:NewServiceRequestModel,completionHandler: @escaping(_ response : ServicePostResponseModel?,_ error:Error?) -> Void){
        var params = ["stype":requestObject.stype ?? "",
                      "scompany":requestObject.company ?? "",
                      "sindustry":(requestObject.industry)!,
                      "scountry":(requestObject.country)!,
                      "sstate":requestObject.state!,
                      "scity":requestObject.city!,
                      "spostalcode":requestObject.postalCode!,
                      "scemail":requestObject.email!,
                      "sduration":requestObject.duration ?? nil,
                      "sdescription":requestObject.description!] as [String:Any]
        
        params["slink"] = requestObject.link
        params["sylink"] = requestObject.youTubeLink
      //  params["sduration"] = requestObject.duration
        params["spaid"] = requestObject.paid
        params["scurrency"] = requestObject.currency
        params["spromocode"] = requestObject.promoCode
        params["stax"] = requestObject.tax
        params["sorderid"] = requestObject.orderId ?? ""
        
        print("PARAMS ARE--->",params)
        
      /*  var mediaArray = ["sfile[]":requestObject.file,
                          "simage[]":requestObject.images,
                          "svideo":requestObject.videos,
                          "sppt":requestObject.ppt,
                          "slogo":[requestObject.logo]
            ] as! [String : [MediaAttatchment]] */
        
        
        var mediaArray = ["sfile[]":requestObject.file,
                          "simage[]":requestObject.images,
                          "svideo[]":requestObject.videos,
                          "sppt[]":requestObject.ppt] as! [String : [MediaAttatchment]?]
        if requestObject.logo != nil{
            mediaArray["slogo"] = [requestObject.logo] as! [MediaAttatchment]
        }else{
            mediaArray["slogo"] = nil
        }
        
        
        
        AlamofireManager.shared().uploadServicesContent(apiUrl: apiName,attachmentArray: mediaArray,parameters: params) { (response, error) in
            STOP_LOADING_VIEW()
            if response != nil{
                if !(((response!["errors"] as? String)?.isEmpty)!) || !((response!["virus"] as? String)?.isEmpty)!{
                    Utils.showErrorFilesAlert(errorFiles : response!["errors"] as? String,corruptFiles : response!["virus"] as? String)
                }
                let decoder = JSONDecoder()
                do{
                    let jsonData = try JSONSerialization.data(withJSONObject: response, options: .prettyPrinted)
                    let reqJSONStr = String(data: jsonData, encoding: .utf8)
                    let data = reqJSONStr?.data(using: .utf8)
                    let response = try decoder.decode(ServicePostResponseModel.self, from: data!)
                    completionHandler(response, nil)
                }catch{
                    print("error")
                }
            }else{
                STOP_LOADING_VIEW()
                KEY_WINDOW?.makeToast(error?.localizedDescription)
            }
        }
        
    }
    
    func postNewJob(apiName:String,requestObject:NewJobRequestModel,completionHandler: @escaping(_ response : ServicePostResponseModel?,_ error:Error?) -> Void){
        var params = ["stype":requestObject.stype ?? "",
                      "jtitle":(requestObject.jobTitle)!,
                      "jcompany":(requestObject.jobCompany)!,
                      "jcountry":(requestObject.country)!,
                      "jstate":(requestObject.state)!,
                      "jcity":(requestObject.city)!,
                      "jpostalcode":(requestObject.postalCode)!,
                      "jfunction":requestObject.jobFunction ?? "",
                      "jemptype":requestObject.empType ?? "",
                      "ppromocode":requestObject.promoCode ?? "",
                      "jindustry":requestObject.industry ?? "",
                      "jduration":requestObject.duration ?? nil,
                      "jlevel":requestObject.level ?? ""]  as [String:Any]
        
        params["jdescription"] = (requestObject.jobDesc)!
        params["jcdescription"] = (requestObject.companyDesc)!
        params["japplytype"] = requestObject.applyType
        params["jemail"] = requestObject.email ?? ""
        params["japplysite"] = requestObject.jobApplySite ?? ""
        params["jview"] = requestObject.jobView ?? ""
     //   params["jduration"] = requestObject.duration ?? ""
        params["jpaid"] = requestObject.paid ?? ""
        params["jpromocode"] = requestObject.promoCode ?? ""
        params["jtax"] = requestObject.tax ?? ""
        params["jorderid"] = requestObject.orderId ?? ""
        params["jcurrency"] = requestObject.currency
       // params["japplysite"] = ""
       
        var mediaArray = [String : [MediaAttatchment]?]()
        
        if requestObject.jobLogo != nil{
            mediaArray["jlogo"] = [requestObject.jobLogo] as! [MediaAttatchment]
        }else{
            mediaArray["jlogo"] = nil
        }
        print("PARAMeters are---->",params)
        AlamofireManager.shared().uploadServicesContent(apiUrl: apiName,attachmentArray: mediaArray,parameters: params) { (response, error) in
            STOP_LOADING_VIEW()
            if response != nil{
                if !(((response!["errors"] as? String)?.isEmpty)!) || !((response!["virus"] as? String)?.isEmpty)!{
                    Utils.showErrorFilesAlert(errorFiles : response!["errors"] as? String,corruptFiles : response!["virus"] as? String)
                }
                let decoder = JSONDecoder()
                do{
                    let jsonData = try JSONSerialization.data(withJSONObject: response, options: .prettyPrinted)
                    let reqJSONStr = String(data: jsonData, encoding: .utf8)
                    let data = reqJSONStr?.data(using: .utf8)
                    let response = try decoder.decode(ServicePostResponseModel.self, from: data!)
                    completionHandler(response, nil)
                }catch{
                    print("error")
                }
            }else{
                STOP_LOADING_VIEW()
                KEY_WINDOW?.makeToast(error?.localizedDescription)
            }
        }
    }
    func postNewAd(apiName:String,requestObject:NewAdsRequestModel,completionHandler: @escaping(_ response : ServicePostResponseModel?,_ error:Error?) -> Void){
        if requestObject.adLayout != nil{
        if requestObject.adLayout?.components(separatedBy:"_").count == 2{
                 requestObject.adLayout =  requestObject.adLayout?.components(separatedBy:"_")[1]
            }
           
        }
        var params = [
                      "adlayout":(requestObject.adLayout)!,
                      "adtitle":requestObject.adTitle ?? "",
                      "adlink":requestObject.adLink ?? "",
                      "addescription":requestObject.adDescription ?? "",
                      "adage":requestObject.adAge ?? "",
                      "adduration":requestObject.duration ?? nil,
                      "adgender":requestObject.adGender ?? "",
                      "adcounty":requestObject.adCountry ?? "",
                      "adstate":requestObject.adState ?? ""
                      ] as [String:Any]
        params["stype"] = requestObject.stype ?? ""
        params["adcompany"] = requestObject.adCompany ?? ""
        params["adpaid"] = requestObject.paid ?? ""
        params["adcurrency"] = requestObject.currency ?? ""
        params["adpromocode"] = requestObject.promoCode ?? ""
        params["adtax"] = requestObject.tax ?? ""
        params["adorderid"] = requestObject.orderId ?? ""
        params["adylink"] = requestObject.adYouTubeLink ?? ""
        
        let mediaArray = ["admedia":[requestObject.adMedia]] as? [String : [MediaAttatchment]]
        print("PARAMeters are---->",params)
        
        AlamofireManager.shared().uploadServicesContent(apiUrl: apiName,attachmentArray: mediaArray,parameters: params) { (response, error) in
            STOP_LOADING_VIEW()
            if response != nil{
                if !(((response!["errors"] as? String)?.isEmpty)!) || !((response!["virus"] as? String)?.isEmpty)!{
                    Utils.showErrorFilesAlert(errorFiles : response!["errors"] as? String,corruptFiles : response!["virus"] as? String)
                }
                let decoder = JSONDecoder()
                do{
                    let jsonData = try JSONSerialization.data(withJSONObject: response, options: .prettyPrinted)
                    let reqJSONStr = String(data: jsonData, encoding: .utf8)
                    let data = reqJSONStr?.data(using: .utf8)
                    let response = try decoder.decode(ServicePostResponseModel.self, from: data!)
                    completionHandler(response, nil)
                }catch{
                    print("error")
                }
            }else{
                STOP_LOADING_VIEW()
                KEY_WINDOW?.makeToast(error?.localizedDescription)
            }
        }
    }
    func postNewSoftware(apiName:String,requestObject:NewSoftwareRequestModel,completionHandler: @escaping(_ response : ServicePostResponseModel?,_ error:Error?) -> Void){
        var params = ["stype":requestObject.stype ?? "",
                      "stname":(requestObject.softwareName)!,
                      "sturl": requestObject.softwareUrl ?? "",
                      "stdescription":(requestObject.softwareDesc)!,
                      "stcurrency":requestObject.currency ?? "",
                      "stpaid":requestObject.paid ?? "",
                      "stpromocode":requestObject.promoCode ?? "",
                      "sttax":requestObject.tax ?? "",
                      "stduration":requestObject.duration ?? nil
                      ] as [String:Any]
        params["storderid"] = requestObject.orderId ?? ""
        var mediaArray = [String : [MediaAttatchment]?]()
     
        if requestObject.logo != nil{
            mediaArray["stlogo"] = [requestObject.logo] as! [MediaAttatchment]
        }else{
            mediaArray["slogo"] = nil
        }
        if requestObject.software != nil{
            mediaArray["stsoftware"] = [requestObject.software] as! [MediaAttatchment]
        }else{
            mediaArray["stsoftware"] = nil
        }
        print("PARAMeters are---->",params)
        
        AlamofireManager.shared().uploadServicesContent(apiUrl: apiName,attachmentArray: mediaArray,parameters: params) { (response, error) in
            STOP_LOADING_VIEW()
            if response != nil{
                if !(((response!["errors"] as? String)?.isEmpty)!) || !((response!["virus"] as? String)?.isEmpty)!{
                    Utils.showErrorFilesAlert(errorFiles : response!["errors"] as? String,corruptFiles : response!["virus"] as? String)
                }
                let decoder = JSONDecoder()
                do{
                    let jsonData = try JSONSerialization.data(withJSONObject: response, options: .prettyPrinted)
                    let reqJSONStr = String(data: jsonData, encoding: .utf8)
                    let data = reqJSONStr?.data(using: .utf8)
                    let response = try decoder.decode(ServicePostResponseModel.self, from: data!)
                    completionHandler(response, nil)
                }catch{
                    print("error")
                }
            }else{
                STOP_LOADING_VIEW()
                KEY_WINDOW?.makeToast(error?.localizedDescription)
            }
        }
    }
    
    
    func updatePaymentStatus(requestObject:PaymentUpdationModel,completionHandler: @escaping(_ response : CommonModel.BaseModel?,_ error:Error?) -> Void){
        let params = ["orderId":requestObject.orderId ?? "",
                      "orderStatus":requestObject.orderStatus ?? "",
                      "tax": requestObject.tax ?? "",
                      "amount":requestObject.amount ?? "0",
                      "renewType":requestObject.renewType ?? "new",
                      "promoCode":requestObject.promoCode ?? "",
                      "serId":requestObject.serId!,
                      "productType":requestObject.productType!,
                      "paymentType":requestObject.paymentType!,
                      "nonce":requestObject.nonce ?? "",
                      "currency":requestObject.currency ?? "INR"] as [String:Any]
        START_LOADING_VIEW()
        print("PARAMS ARE---->",params)
        ServiceManager.methodType(requestType: POST_REQUEST, url: PAYMENT_UPDATE, params: params as NSDictionary,paramsData: nil,completion: { (response,_ responseData, statusCode) in
            print("REsponse payment updation ---->",response)
            STOP_LOADING_VIEW()
            let decoder = JSONDecoder()
            do{
                let checkoutDetails = try decoder.decode(CommonModel.BaseModel.self, from: responseData!)
                completionHandler(checkoutDetails,nil)
                
            }catch{
                DispatchQueue.main.async{
                    if ((response as! [String : Any])["message"] as? String) != nil{
                        KEY_WINDOW?.makeToast((response as! [String : Any])["message"] as? String)
                    }
                }
                
                print("error")
            }
            
        }) { (response, statusCode) in
            STOP_LOADING_VIEW()
        }
        

        
    }
    
    
}
