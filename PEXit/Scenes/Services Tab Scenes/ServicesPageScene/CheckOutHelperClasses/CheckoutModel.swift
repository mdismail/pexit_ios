//
//  CheckoutModel.swift
//  PEXit
//
//  Created by ats on 30/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation

class CheckoutRequestModel{
    var service : String?
    var countryCode : String?
    var serviceType : String?
}

class CheckOutResponseModel : Codable{
    var status : Bool?
    var datas : CheckOutDataModel?
    var message : String?
}

class CheckOutDataModel : Codable{
    var orderid : String?
    var currency : String?
    var priceRange : [PriceModel]?
    var freeCount : Int?
    var freeCountLimit : Int?
    var payPalToken : String?
}

class PriceModel : Codable{
   // var prid : Int?
    var duration : Int?
    var duration_in : String?
}

class PriceValueModel : Codable{
    var status : Bool?
    var datas : Int?
    var tax : Int?
    var message : String?
}

class PaymentUpdationModel : Codable{
     var orderId : String?
     var orderStatus : String?
     var tax : String?
     var amount : String?
     var renewType : String?
     var promoCode : String?
     var serId : String?
     var productType : String?
     var paymentType : Int?
     var nonce : String?
     var currency : String?
}
