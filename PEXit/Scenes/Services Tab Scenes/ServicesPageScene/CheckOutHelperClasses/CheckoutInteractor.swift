//
//  CheckoutInteractor.swift
//  PEXit
//
//  Created by ats on 30/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation

protocol CheckoutInteractorInput{
    func fetchCheckoutOptions(checkoutRequestModel : CheckoutRequestModel?)
    func uploadSponsoredContent(requestObject : SponsoredContentRequestModel?)
    func uploadNewProduct(requestObject : NewProductRequestModel?)
    func uploadNewSoftware(requestObject : NewSoftwareRequestModel?)
    func uploadNewJob(requestObject : NewJobRequestModel?)
    func uploadNewAd(requestObject : NewAdsRequestModel?)
    func uploadNewService(requestObject : NewServiceRequestModel?)
    func fetchPriceList(paramsDict : [String:Any]?)
    func applyPromoCode(paramsDict : [String:Any]?)
}


protocol CheckoutInteractorOutput{
    func presentCheckoutResponse(response: CheckOutResponseModel?)
    func displayResponse(response : ServicePostResponseModel?,message : String?)
    func displayPriceResponse(response : PriceValueModel?,message : String?)
}

class CheckoutInteractor : CheckoutInteractorInput{
    var worker = CheckoutWorker()
    var output : CheckoutInteractorOutput!
    
    func fetchCheckoutOptions(checkoutRequestModel : CheckoutRequestModel?){
        worker.fetchCheckoutOptions(requestObject: checkoutRequestModel) { (response, error) in
            self.output.presentCheckoutResponse(response: response as? CheckOutResponseModel)
        }
    }
    
    func uploadSponsoredContent(requestObject : SponsoredContentRequestModel?){
        var apiName = ""
        if requestObject?.idOfContent != nil{
            apiName = UPDATE_SPONSOR_CONTENT(sponsorID: (requestObject?.idOfContent!)!)
        }else{
            apiName = POST_SPONSOR_CONTENT
        }
        worker.postSponsorContent(apiName: apiName, requestObject: requestObject!) { (response, error) in
            if error == nil{
                self.output.displayResponse(response: response, message: nil)
            }else{
                self.output.displayResponse(response: nil, message: error?.localizedDescription ?? UNKNOWN_ERROR_MSG)
            }
        }
    }
    
    func uploadNewProduct(requestObject : NewProductRequestModel?){
        var apiName = ""
        if requestObject?.idOfProduct != nil{
            apiName = UPDATE_PRODUCT(productId: (requestObject?.idOfProduct!)!)
        }else{
            apiName = POST_PRODUCT
        }
        worker.postNewProduct(apiName:apiName,requestObject: requestObject!) { (response, error) in
            if error == nil{
                self.output.displayResponse(response: response, message: nil)
            }else{
                self.output.displayResponse(response: nil, message: error?.localizedDescription ?? UNKNOWN_ERROR_MSG)
            }
        }
    }
    
    func uploadNewSoftware(requestObject : NewSoftwareRequestModel?){
        var apiName = ""
        if requestObject?.idOfSoftware != nil{
            apiName = UPDATE_SOFTWARE(softwareId: (requestObject?.idOfSoftware!)!)
        }else{
            apiName = UPLOAD_NEW_SOFTWARE
        }
        worker.postNewSoftware(apiName: apiName, requestObject: requestObject!) { (response, error) in
            if error == nil{
                self.output.displayResponse(response: response, message: nil)
            }else{
                self.output.displayResponse(response: nil, message: error?.localizedDescription ?? UNKNOWN_ERROR_MSG)
            }
        }
        
    }
    func uploadNewJob(requestObject : NewJobRequestModel?){
        var apiName = ""
        if requestObject?.idOfJob != nil{
            apiName = UPDATE_JOB(jobId: (requestObject?.idOfJob!)!)
        }else{
            apiName = POST_NEW_JOB
        }
        worker.postNewJob(apiName:apiName,requestObject: requestObject!) { (response, error) in
            if error == nil{
                self.output.displayResponse(response: response, message: nil)
            }else{
                self.output.displayResponse(response: nil, message: error?.localizedDescription ?? UNKNOWN_ERROR_MSG)
            }
        }
        
    }
    func uploadNewAd(requestObject : NewAdsRequestModel?){
        var apiName = ""
        if requestObject?.idOfAd != nil{
            apiName = UPDATE_AD(adID: (requestObject?.idOfAd!)!)
        }else{
            apiName = POST_NEW_AD
        }
        worker.postNewAd(apiName:apiName,requestObject: requestObject!) { (response, error) in
            if error == nil{
                self.output.displayResponse(response: response, message: nil)
            }else{
                self.output.displayResponse(response: nil, message: error?.localizedDescription ?? UNKNOWN_ERROR_MSG)
            }
        }
        
    }
    func uploadNewService(requestObject : NewServiceRequestModel?){
        var apiName = ""
        if requestObject?.idOfService != nil{
            apiName = UPDATE_SERVICE(serviceID: (requestObject?.idOfService!)!)
        }else{
            apiName = POST_SERVICE
        }
        worker.postNewService(apiName:apiName,requestObject: requestObject!) { (response, error) in
            if error == nil{
                self.output.displayResponse(response: response, message: nil)
            }else{
                self.output.displayResponse(response: nil, message: error?.localizedDescription ?? UNKNOWN_ERROR_MSG)
            }
        }
        
    }
    
    func fetchPriceList(paramsDict : [String:Any]?){
        worker.fetchPriceList(params: paramsDict) { (response, error) in
            if error == nil{
                self.output.displayPriceResponse(response: response, message: nil)
            }else{
                self.output.displayPriceResponse(response: nil, message: error?.localizedDescription ?? UNKNOWN_ERROR_MSG)
            }
        }
    }
    
    func applyPromoCode(paramsDict : [String:Any]?){
        worker.applyPromoCode(params: paramsDict) { (response, error) in
            if error == nil{
                self.output.displayPriceResponse(response: response, message: nil)
            }else{
                self.output.displayPriceResponse(response: nil, message: error?.localizedDescription ?? UNKNOWN_ERROR_MSG)
            }
        }
    }
}
