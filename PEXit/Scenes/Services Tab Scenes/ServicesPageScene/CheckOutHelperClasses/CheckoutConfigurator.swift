//
//  CheckoutConfigurator.swift
//  PEXit
//
//  Created by ats on 30/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation



extension CheckoutViewController: CheckoutOutput{
  
}

extension CheckoutInteractor: CheckoutViewControllerOutput{

}

extension CheckoutPresenter: CheckoutInteractorOutput{
    
}


class CheckoutConfigurator{
    
    // MARK: Object lifecycle
    
    class var sharedInstance: CheckoutConfigurator{
        struct Static {
            static let instance =  CheckoutConfigurator()
        }
     //   print("RegistrationConfigurator instance created")
        return Static.instance
    }
    
    // MARK: Configuration
    
    func configure(viewController: CheckoutViewController){
        
        let presenter = CheckoutPresenter()
        presenter.output = viewController
        let interactor = CheckoutInteractor()
        interactor.output = presenter
        viewController.output = interactor
        
    }
    
    
    
}
