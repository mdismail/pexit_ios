//
//  ServicesPageViewController.swift
//  PEXit
//
//  Created by Apple on 09/07/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import Presentr

protocol NavigationItemTapped{
    func navigationItemSelected()
}

let titleNavigation = "Free Services"

class ServicesPageViewController: UIViewController , ItemSelected{
    var delegate : NavigationItemTapped?
    private lazy var productsServicesViewController: ProductsServicesViewController = {
        var editProductController = storyboard?.instantiateViewController(withIdentifier: "productsServicesViewController") as! ProductsServicesViewController
        editProductController.controllerType = ControllerType.Product.rawValue
        return editProductController
    }()
    
    private lazy var editSoftwareViewController: ProductsServicesViewController = {
        // Instantiate View Controller
        var editProductController = storyboard?.instantiateViewController(withIdentifier: "productsServicesViewController") as! ProductsServicesViewController
        editProductController.controllerType = ControllerType.Software.rawValue
        return editProductController
    }()
    private lazy var editJobViewController: ProductsServicesViewController = {
        var viewController = storyboard?.instantiateViewController(withIdentifier: "productsServicesViewController") as! ProductsServicesViewController
        viewController.controllerType = ControllerType.Jobs.rawValue
        return viewController
    }()
    
    private lazy var sponsoredContentViewController: ProductsServicesViewController = {
        var viewController = storyboard?.instantiateViewController(withIdentifier: "productsServicesViewController") as! ProductsServicesViewController
        viewController.controllerType = ControllerType.Sponsor.rawValue
        return viewController
    }()
    private lazy var adsViewController: ProductsServicesViewController = {
        var viewController = storyboard?.instantiateViewController(withIdentifier: "productsServicesViewController") as! ProductsServicesViewController
        viewController.controllerType = ControllerType.Ads.rawValue
        return viewController
    }()
    
    private lazy var editServicesViewController: ProductsServicesViewController = {
        // Instantiate View Controller
        var editProductController = storyboard?.instantiateViewController(withIdentifier: "productsServicesViewController") as! ProductsServicesViewController
        editProductController.controllerType = ControllerType.Services.rawValue
        return editProductController
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        showNavigationBar()
        addNavigationRightBarButton()
        addNavigationBarWithTitleAndSubtitle(title: titleNavigation, subTitle: "Products")
     //  addNavigationTitleView()
        add(asChildViewController: productsServicesViewController)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    override func clickOnSubtitleButton(_ sender: UIButton) {
        let presenter = Presentr(presentationType: .popup)
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "commonViewController") as! CommonViewController
        controller.itemsArray = ["Products","Services","Software","Post A Job","Post An Ad","Sponsored Content"]
        controller.delegate = self
        customPresentViewController(presenter, viewController: controller, animated: true)
    }
    
    override func addNewData(){
        if let delegate = delegate{
            delegate.navigationItemSelected()
        }
//        let sponsoredContentController = self.storyboard?.instantiateViewController(withIdentifier: "sponsoredContentController") as! SponsoredContentViewController
//        self.navigationController?.pushViewController(sponsoredContentController, animated: true)
    }
    
    func optionSelected(option: String?) {
        
        print("CHILD CONTROLLERS ------>",self.childViewControllers)
        if self.childViewControllers.count > 0{
            remove(asChildViewController: self.childViewControllers[0])
        }
        addNavigationRightBarButton()
        addNavigationBarWithTitleAndSubtitle(title: titleNavigation, subTitle: option)
        switch option{
        case "Products":
            add(asChildViewController: productsServicesViewController)
        case "Services":
            add(asChildViewController: editServicesViewController)
        case "Software":
            add(asChildViewController: editSoftwareViewController)
        case "Post A Job":
            add(asChildViewController: editJobViewController)
        case "Post An Ad":
            add(asChildViewController: adsViewController)
        case "Sponsored Content":
            add(asChildViewController: sponsoredContentViewController)
        default:
            print("No COntroller")
        }
    }

}
