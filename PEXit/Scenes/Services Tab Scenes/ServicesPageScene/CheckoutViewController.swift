//
//  CheckoutViewController.swift
//  PEXit
//
//  Created by ats on 29/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
//import BraintreeDropIn
//import Braintree


protocol CheckoutViewControllerInput{
    func presentCheckoutResponse(response:CheckOutDataModel?,message: String?)
    func displayResponse(response :ServicePostResponseModel?,message: String?)
    func displayPriceResponse(value : Int?,tax:Int?,message : String?)
}

protocol CheckoutViewControllerOutput{
    func fetchPriceList(paramsDict : [String:Any]?)
    func fetchCheckoutOptions(checkoutRequestModel : CheckoutRequestModel?)
    func uploadSponsoredContent(requestObject : SponsoredContentRequestModel?)
    func uploadNewProduct(requestObject : NewProductRequestModel?)
    func uploadNewSoftware(requestObject : NewSoftwareRequestModel?)
    func uploadNewJob(requestObject : NewJobRequestModel?)
    func uploadNewAd(requestObject : NewAdsRequestModel?)
    func uploadNewService(requestObject : NewServiceRequestModel?)
    func applyPromoCode(paramsDict : [String:Any]?)
}



class CheckoutViewController: UIViewController, CheckoutViewControllerInput {
    
    
  
    @IBOutlet weak var paypalRedirectionView: UIView!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var txtFieldPromoCode: TJTextField!
    @IBOutlet weak var btnDuration: UIButton!
    @IBOutlet weak var btnFree: UIButton!
    @IBOutlet weak var btnPaid: UIButton!
    @IBOutlet weak var txtPromoCode: UILabel!
    @IBOutlet weak var lblDuration: UILabel!
    @IBOutlet weak var viewPaidType: UIView!
    @IBOutlet weak var btnPromoCode: UIButton!
    @IBOutlet weak var constraintPaidViewHeight: NSLayoutConstraint!
    @IBOutlet weak var btnCheckout: UIButton!
    
    @IBOutlet weak var lblDisclaimer: UILabel!
    var sponsoredContentRequestModel : SponsoredContentRequestModel?
    var productRequestModel : NewProductRequestModel?
    var serviceRequestModel : NewServiceRequestModel?
    var softwareRequestModel : NewSoftwareRequestModel?
    var jobRequestModel : NewJobRequestModel?
    var adsRequestModel : NewAdsRequestModel?
    var serviceType : String?
    var output : CheckoutViewControllerOutput!
    var orderId : String?
    var currency : String?
    var currentDuration : String?
    var currentPrice : Int?
    var isFromEdit = false
    var idToEdit : Int?
    var durationArray = [String]()
    var serviceTypeChoosen : String?
    let checkOutRequestModel = CheckoutRequestModel()
    let paymentModel = PaymentUpdationModel()
    var clientToken : String?
    var currentId : Int?
    var isFromTransactionHistory = false
    var transactionHistoryModel : TransactionListModel?
    var tax = 0
    var taxPercentage = 0
    var transactionHistoryTax = 0
    var isFreeLimitExceeded = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewPaidType.isHidden = true
        constraintPaidViewHeight.constant = 0
        lblDisclaimer.text = ""
        addBackButton(withTitle: "Checkout")
        btnPaid.isHidden = true
        if isFromEdit{
            //setUpViewForPaidServices()
            btnFree.isEnabled = false
            btnFree.isUserInteractionEnabled = false
          //  btnPaid.isSelected = true
        }
        if isFromTransactionHistory{
            setDataFromTransactionHistory()
        }
        //  print(sponsoredContentRequestModel?.title)
        // print(Utils.getUserCountry())
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated : Bool){
        showNavigationBar()
    }
    override func awakeFromNib() {
        CheckoutConfigurator.sharedInstance.configure(viewController: self)
    }
    
    func setDataFromTransactionHistory(){
        if transactionHistoryModel != nil{
            currentPrice = Int(transactionHistoryModel?.amount ?? "")
            transactionHistoryTax = Int(transactionHistoryModel?.tax ?? "") ?? 0
            btnPaid.isSelected = true
            btnFree.isUserInteractionEnabled = false
            btnPaid.isUserInteractionEnabled = false
            //btnDuration.isHidden = true
            btnDuration.isUserInteractionEnabled = false
            btnCheckout.setTitle("Checkout",for:.normal)
            
            self.setUpViewForPaidServices()
            orderId = transactionHistoryModel?.orderid
            currency = transactionHistoryModel?.currency
            lblPrice.text = "\(transactionHistoryModel?.amount ?? "") \(self.currency!)"
            serviceTypeChoosen = "paid"
            
            checkOutRequestModel.service = transactionHistoryModel?.shortName
            checkOutRequestModel.countryCode = Utils.getUserCountry() ?? "US"
           
            getPrices(requestModel: checkOutRequestModel)
            // }
            
            paymentModel.serId = String((transactionHistoryModel?.serid)!)
            paymentModel.orderId = transactionHistoryModel?.orderid
            setDisclaimerText()
            paymentModel.currency = currency
            paymentModel.promoCode = txtFieldPromoCode.text ?? ""
            paymentModel.amount = String(currentPrice!)
            paymentModel.productType = transactionHistoryModel?.shortName
        }
    }
    
    func setDisclaimerText(){
        lblDisclaimer.text = "Note : \n Please note down the reference number \(self.orderId ?? "") \n If the payment process is unsuccessful,then check the status of the payment through Transaction History available under Profile menu and then retry with checkout again \n Price shown above is exclusive of applicable Taxes and Levies."
        
    }
    
    @IBAction func btnServiceTypeClicked(_ sender: UIButton) {
        btnFree.isSelected = false
        btnPaid.isSelected = false
        sender.isSelected = !sender.isSelected
        
        if sender == btnPaid{
            setUpViewForPaidServices()
            btnDuration.setTitle("Choose Duration", for: .normal)
          
        }else{
            btnDuration.isUserInteractionEnabled = false
            setBtnDurationTitle()
            viewPaidType.isHidden = true
            constraintPaidViewHeight.constant = 0
            checkOutRequestModel.serviceType = "free"
        }
        
        serviceTypeChoosen = checkOutRequestModel.serviceType
        checkOutRequestModel.service = serviceType
        checkOutRequestModel.countryCode = Utils.getUserCountry() ?? "US"
        getPrices(requestModel: checkOutRequestModel)
    }
    
    
    func setBtnDurationTitle(){
        switch  serviceType {
        case  SERVICE_TYPE_API.PRODUCTS.rawValue,
              SERVICE_TYPE_API.SERVICES.rawValue,
              SERVICE_TYPE_API.SOFTWARES.rawValue:
                currentDuration = "3 Months"
                btnDuration.setTitle("3 Months",for:.normal)
        case SERVICE_TYPE_API.SPONSORS.rawValue:
                currentDuration = "3 Months"
                btnDuration.setTitle("3 Months",for:.normal)
        case SERVICE_TYPE_API.AD.rawValue ,
             SERVICE_TYPE_API.JOBS.rawValue:
                currentDuration = "15 Days"
                btnDuration.setTitle("15 Days",for:.normal)
        default:
            return
        }
    }
    
    func getPrices(requestModel : CheckoutRequestModel?){
        output.fetchCheckoutOptions(checkoutRequestModel: requestModel)
    }
    
    func presentCheckoutResponse(response: CheckOutDataModel?, message: String?) {
        if response != nil{
            if checkOutRequestModel.serviceType == "free"{
                if (response?.freeCount)! >= (response?.freeCountLimit)!{
//                    DispatchQueue.main.async{
//                        self.currentDuration = nil
//                        self.btnPaid.isSelected = true
//                        self.btnFree.isSelected = false
//                        self.btnFree.isUserInteractionEnabled = false
//                        self.btnDuration.setTitle("Choose Duration", for: .normal)
//                        self.setUpViewForPaidServices()
//                    }
//                    displayToast("You have exceeded the free limit. And paid services option is not available with ios App  at this time")
                    isFreeLimitExceeded = true
                }
            }
            
            DispatchQueue.main.async{
                self.clientToken = response?.payPalToken
                self.orderId = response?.orderid
                self.setDisclaimerText()
                self.currency = response?.currency
                self.durationArray.removeAll()
                for prices in (response?.priceRange)!{
                    self.durationArray.append("\(prices.duration!) \(prices.duration_in!)")
                }
              
            }
        }else{
            displayToast(message ?? UNKNOWN_ERROR_MSG)
        }
    }
    
    func setUpViewForPaidServices(){
        btnDuration.isUserInteractionEnabled = true
        constraintPaidViewHeight.constant = 150
        viewPaidType.isHidden = false
        currentDuration = nil
        checkOutRequestModel.serviceType = "paid"
        serviceTypeChoosen = "paid"
    }
    
    func fetchPrices(){
        let params = ["duration":currentDuration,"currency":currency,"service":serviceType]
        output.fetchPriceList(paramsDict: params)
    }
    
    func displayResponse(response :ServicePostResponseModel?,message: String?){
        displayToast(message ?? UNKNOWN_ERROR_MSG)
        // if !isFromTransactionHistory{
        
        if let resp = response,resp.status == true{
            if checkOutRequestModel.serviceType == "paid"{
                paymentModel.serId = resp.datas
                paymentModel.orderId = orderId
                paymentModel.currency = currency
                paymentModel.promoCode = txtFieldPromoCode.text ?? ""
                
                paymentModel.productType = serviceType
                if isFromEdit{
                    paymentModel.renewType = "renew"
                    if let idToEdit = idToEdit{
                        paymentModel.serId = String(idToEdit)
                    }
                    
                }else{
                    paymentModel.renewType = "new"
                }
                
            
                if Utils.getUserCountry() == "IN"{
                    
                    tax = (taxPercentage * Int(currentPrice!))/100  // needs to be 18% of amount
                    print("VALUE OF TAX IS ---->",tax)
                    paymentModel.tax = String(tax)
                    paymentModel.amount = String(currentPrice! + tax)
                    goToCCAvenueController()
                }else{
                    paymentModel.tax = String(tax)
                    paymentModel.amount = String(currentPrice! + tax)
                    navigateToPayPal()
                }
                
            }else{
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    self.performSegue(withIdentifier: "navigateToList", sender: self)
                }
            }
            
        }
        //  }
        
    }
    
  
    func displayPriceResponse(value : Int?,tax:Int?,message : String?){
        DispatchQueue.main.async {
            if value != nil{
                self.currentPrice = value!
                self.taxPercentage = tax ?? 0
                self.lblPrice.text = "\(value!) \(self.currency!)"
            }else{
                self.displayToast(message ?? UNKNOWN_ERROR_MSG)
            }
        }
        
    }
    
    @IBAction func btnDurationClicked(_ sender: UIButton) {
        navigateToPopUpController(sender: sender, optionsArray: durationArray)
    }
    
    @IBAction func btnApplyClicked(_ sender: Any) {
        self.view.endEditing(true)
        if Utils.validateFormAndShowToastWithRequiredFields([txtFieldPromoCode]){
            if currentPrice != nil{
                let params = ["service":serviceType,"amount":currentPrice!,"promocode":txtFieldPromoCode.text!] as [String : Any]
                output.applyPromoCode(paramsDict: params)
            }else{
                displayToast("Please choose duration")
            }
            
        }
        
    }
    
    @IBAction func btnPublishClicked(_ sender: Any) {
        if serviceTypeChoosen == nil{
            displayToast(CHOOSE_CHECKOUT_OPTION)
            return
        }
        if Utils.getUserCountry() == "IN" {
            if currentPrice != nil{
                tax = (taxPercentage * Int(currentPrice!))/100  // needs to be 18% of amount
                if isFromTransactionHistory && taxPercentage == 0{
                   tax = transactionHistoryTax
                }
                print("VALUE OF TAX IS ---->",tax)

            }
        }
      
        if isFromTransactionHistory{
            proceedToPayment()
        }else{
            if checkOutRequestModel.serviceType == "paid" && currentDuration == nil{
                displayToast("Please choose duration")
                return
            }
            if isFreeLimitExceeded == true{
                displayToast("You have reached your \"Free Services\" limit. Contact support@pexit.net.")
                return
            }
            switch  serviceType {
            case SERVICE_TYPE_API.SPONSORS.rawValue:
                publishSponsorContent()
            case SERVICE_TYPE_API.PRODUCTS.rawValue:
                publishNewProduct()
            case SERVICE_TYPE_API.AD.rawValue:
                publishNewAd()
            case SERVICE_TYPE_API.SOFTWARES.rawValue:
                publishNewSoftware()
            case SERVICE_TYPE_API.SERVICES.rawValue:
                publishNewService()
            case SERVICE_TYPE_API.JOBS.rawValue:
                publishNewJob()
            default:
                return
            }
        }
        
    }
    
    func proceedToPayment(){
        if Utils.getUserCountry() == "IN"{
           
            paymentModel.tax = transactionHistoryModel?.tax
            goToCCAvenueController()
        }else{
            paymentModel.tax = transactionHistoryModel?.tax
            navigateToPayPal()
        }
    }
    
    func publishSponsorContent(){
        sponsoredContentRequestModel?.orderId = orderId
        sponsoredContentRequestModel?.duration = currentDuration
        sponsoredContentRequestModel?.stype = serviceTypeChoosen
        if checkOutRequestModel.serviceType == "paid"{
            sponsoredContentRequestModel?.promoCode = txtFieldPromoCode.text
            sponsoredContentRequestModel?.currency = currency
            sponsoredContentRequestModel?.paid = String(currentPrice!)
            sponsoredContentRequestModel?.tax = String(tax)
        }
        if isFromEdit == true{
            sponsoredContentRequestModel?.idOfContent = idToEdit
        }
        output.uploadSponsoredContent(requestObject: sponsoredContentRequestModel)
    }
    
    func publishNewProduct(){
        productRequestModel?.orderId = orderId
        productRequestModel?.duration = currentDuration
        productRequestModel?.stype = serviceTypeChoosen
        if checkOutRequestModel.serviceType == "paid"{
            productRequestModel?.promoCode = txtFieldPromoCode.text
            productRequestModel?.currency = currency
            productRequestModel?.paid = String(currentPrice!)
            productRequestModel?.tax = String(tax)
        }
        if isFromEdit == true{
            productRequestModel?.idOfProduct = idToEdit
        }
        output.uploadNewProduct(requestObject: productRequestModel)
    }
    
    func publishNewAd(){
        adsRequestModel?.orderId = orderId
        adsRequestModel?.duration = currentDuration
        adsRequestModel?.stype = serviceTypeChoosen
        if checkOutRequestModel.serviceType == "paid"{
            adsRequestModel?.promoCode = txtFieldPromoCode.text
            adsRequestModel?.currency = currency
            adsRequestModel?.paid = String(currentPrice!)
            adsRequestModel?.tax = String(tax)
        }
        if isFromEdit == true{
            adsRequestModel?.idOfAd = idToEdit
        }
        output.uploadNewAd(requestObject: adsRequestModel)
    }
    
    func publishNewSoftware(){
        softwareRequestModel?.orderId = orderId
        softwareRequestModel?.duration = currentDuration
        softwareRequestModel?.stype = serviceTypeChoosen
        if checkOutRequestModel.serviceType == "paid"{
            softwareRequestModel?.promoCode = txtFieldPromoCode.text
            softwareRequestModel?.currency = currency
            softwareRequestModel?.paid = String(currentPrice!)
            softwareRequestModel?.tax = String(tax)
        }
        if isFromEdit == true{
            softwareRequestModel?.idOfSoftware = idToEdit
        }
        output.uploadNewSoftware(requestObject: softwareRequestModel)
    }
    
    func publishNewService(){
        serviceRequestModel?.orderId = orderId
        serviceRequestModel?.duration = currentDuration
        serviceRequestModel?.stype = serviceTypeChoosen
        if checkOutRequestModel.serviceType == "paid"{
            serviceRequestModel?.promoCode = txtFieldPromoCode.text
            serviceRequestModel?.currency = currency
            serviceRequestModel?.paid = String(currentPrice!)
            serviceRequestModel?.tax = String(tax)
        }
        if isFromEdit == true{
            serviceRequestModel?.idOfService = idToEdit
        }
        output.uploadNewService(requestObject: serviceRequestModel)
    }
    
    func publishNewJob(){
        jobRequestModel?.orderId = orderId
        jobRequestModel?.duration = currentDuration
        jobRequestModel?.stype = serviceTypeChoosen
        if checkOutRequestModel.serviceType == "paid"{
            jobRequestModel?.promoCode = txtFieldPromoCode.text
            jobRequestModel?.currency = currency
            jobRequestModel?.paid = String(currentPrice!)
            jobRequestModel?.tax = String(tax)
        }
        if isFromEdit == true{
            jobRequestModel?.idOfJob = idToEdit
        }
        output.uploadNewJob(requestObject: jobRequestModel)
        
    }
}


extension CheckoutViewController : UIPopoverPresentationControllerDelegate , ItemSelected{
    
    func navigateToPopUpController(sender:AnyObject?,optionsArray:[String]?){
        let popUpController = storyboard?.instantiateViewController(withIdentifier: "commonViewController") as! CommonViewController
        popUpController.modalPresentationStyle = .popover
        if sender is UIButton{
             popUpController.preferredContentSize = CGSize(width : (sender as! UIButton).frame.size.width , height : CGFloat((optionsArray?.count)! * COMMON_CONTROLLER_ROW_HEIGHT))
        }else if sender is UITextField{
            popUpController.preferredContentSize = CGSize(width : (sender as! UITextField).frame.size.width , height : CGFloat((optionsArray?.count)! * COMMON_CONTROLLER_ROW_HEIGHT))
        }
       
        let popOver = popUpController.popoverPresentationController
        popOver?.delegate = self
        popOver?.permittedArrowDirections = .up
        popOver?.sourceView = sender as? UIView
        popOver?.sourceRect = (sender?.bounds)!
        popUpController.delegate = self
        popUpController.itemsArray = optionsArray
        present(popUpController, animated: true, completion: nil)
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle{
        return .none
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return .none
    }
    
    func optionSelected(option: String?) {
        currentDuration = option
        btnDuration?.setTitle(option, for: .normal)
        self.fetchPrices()
    }
    
    func goToCCAvenueController(){
        btnFree.isUserInteractionEnabled = false
        let ccWebController = storyboard?.instantiateViewController(withIdentifier: "CCWebViewController") as! CCWebViewController
        ccWebController.orderId = orderId!
        ccWebController.delegate = self
        ccWebController.amount = String(currentPrice!+(tax ?? 0))
        ccWebController.currency = currency!
        ccWebController.paymentModel = paymentModel
        self.navigationController?.pushViewController(ccWebController, animated: true)
    }
}

extension CheckoutViewController : PaymentStatusUpdation {
//extension CheckoutViewController : BTAppSwitchDelegate , BTViewControllerPresentingDelegate,PaymentStatusUpdation {
//    func appSwitcherWillPerformAppSwitch(_ appSwitcher: Any) {
//        print("SWITCH PERFORMED")
//    }
//
//    func appSwitcher(_ appSwitcher: Any, didPerformSwitchTo target: BTAppSwitchTarget) {
//        print("SWITCH PERFORMED")
//    }
//
//    func appSwitcherWillProcessPaymentInfo(_ appSwitcher: Any) {
//        print("PAYMENT PROCESSED")
//    }
//
//    func paymentDriver(_ driver: Any, requestsPresentationOf viewController: UIViewController) {
//        self.present(viewController, animated: true, completion: nil)
//    }
//
//    func paymentDriver(_ driver: Any, requestsDismissalOf viewController: UIViewController) {
//        self.dismiss(animated: true, completion: nil)
//    }
    
    func showAlert() {
        let alert = UIAlertController(title: "Alert", message: "Wait Please!", preferredStyle: .alert)
        self.present(alert, animated: true, completion: nil)
        Timer.scheduledTimer(withTimeInterval: 3.0, repeats: false, block: { _ in alert.dismiss(animated: true, completion: nil)} )
    }
    
    func navigateToPayPal(){
     /*   paypalRedirectionView.isHidden = false
        btnFree.isUserInteractionEnabled = false
        let btApiClient = BTAPIClient.init(authorization: clientToken ?? "")
        let payPalDriver = BTPayPalDriver.init(apiClient: btApiClient!)
        payPalDriver.viewControllerPresentingDelegate = self;
        payPalDriver.appSwitchDelegate = self
        let paymentRequest = BTPayPalRequest.init(amount: String(currentPrice!))
        paymentRequest.currencyCode = currency ?? "USD"
       
        payPalDriver.requestOneTimePayment(paymentRequest) { (tokenizedPayPalAccount, error) in
            self.paypalRedirectionView.isHidden = true

            if ((tokenizedPayPalAccount) != nil) {
                
                self.paymentModel.nonce = tokenizedPayPalAccount?.nonce
                self.updatePaymentStatus(paymentStatus: "Success",paymentType: 2)
            } else if ((error) != nil) {
                self.updatePaymentStatus(paymentStatus: "Failure",paymentType: 2)
                // Handle error here...
            } else {
                self.updatePaymentStatus(paymentStatus: "Aborted",paymentType: 2)
                // Buyer canceled payment approval
            }
        } */
    }
    
    func updatePaymentStatus(paymentStatus:String,paymentType:Int){
        let checkoutworker = CheckoutWorker()
        paymentModel.orderStatus = paymentStatus
        paymentModel.paymentType = paymentType // for paypal 2 is the type
        checkoutworker.updatePaymentStatus( requestObject: self.paymentModel) { (response, error) in
           // print("response is --->",response)
            if let response = response{
                self.displayToast(response.message ?? "")
                if response.status == true{
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                        if self.isFromTransactionHistory{
                            self.navigationController?.popToRootViewController(animated: true)
                        }else{
                            self.performSegue(withIdentifier: "navigateToList", sender: self)
                        }
                        
                    }
                }
                
            }
        }
    }
    
    func updatePaymentStatus(status: String?) {
        self.updatePaymentStatus(paymentStatus: status!,paymentType: 1)
    }
    
}



