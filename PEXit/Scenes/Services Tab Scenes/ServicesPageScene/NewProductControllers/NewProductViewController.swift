//
//  NewProductViewController.swift
//  PEXit
//
//  Created by ats on 23/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import MobileCoreServices

class NewProductViewController: UIViewController , UIPopoverPresentationControllerDelegate, ItemSelected{
    @IBOutlet weak var tblNewProduct: UITableView!
    var currentTextField : UITextField?
    var currentProductModel : ServiceProductModel?
    
    lazy var dataController: ProductDataSourceDelegate = {
        let dataController = ProductDataSourceDelegate()
        dataController.parentController = self
        dataController.generateTextFields()
        return dataController
    }()
    
    lazy var attachmentController: AttatchmentViewController = {
        var viewController = storyboard?.instantiateViewController(withIdentifier: "attachmentController") as! AttatchmentViewController
        viewController.serviceType = "Product"
        return viewController
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addBackButton(withTitle: "Post Product")
        tblNewProduct.register(UINib(nibName: "PostNewJobTableViewCell", bundle: nil), forCellReuseIdentifier: "postNewJobTableViewCell")
        tblNewProduct.dataSource = dataController
        tblNewProduct.delegate = dataController
        if currentProductModel != nil{
            setUpDataForEdit()
        }
        // Do any additional setup after loading the view.
    }
    
    
    
    func setUpDataForEdit(){
        dataController.paramsDict["Name"] = currentProductModel?.name
        dataController.paramsDict["Category"] = currentProductModel?.category
        dataController.paramsDict["Product Description"] = currentProductModel?.descrip
        dataController.paramsDict["Location"] = currentProductModel?.location
        if let prodDetails = currentProductModel?.pdetails{
            if let pathUrl = URL(string:prodDetails.urlEncodedString()){
                 dataController.paramsDict["Product Details"] = pathUrl.lastPathComponent
            }
        }
        if let transactionHistory = currentProductModel?.thistory{
            if let pathUrl = URL(string:transactionHistory.urlEncodedString()){
                dataController.paramsDict["Transaction History"] = pathUrl.lastPathComponent
            }
        }
        if let logo = currentProductModel?.logo{
            if let pathUrl = URL(string:logo){
                dataController.paramsDict["Upload Image"] = pathUrl.lastPathComponent
            }
        }
//        dataController.paramsDict["Transaction History"] = currentProductModel?.thistory
//        dataController.paramsDict["Upload Image"] = currentProductModel?.logo
        dataController.paramsDict["Product Condition"] = currentProductModel?.pcondition?.capitalized
        let mediaModel = PostRLMModel()
        mediaModel.id = (currentProductModel?.id)!
        mediaModel.video = currentProductModel?.pgyoutube
        mediaModel.allvid = (currentProductModel?.pgvideo)!
        mediaModel.image = (currentProductModel?.pgimage)!
        self.attachmentController.currentProductModel = currentProductModel
        self.attachmentController.isFromEdit = true
        self.attachmentController.currentPostModel = mediaModel
        dataController.generateTextFields()
    }
    
    
    func addSubView(currentView:UIView) {
        // Add Child View Controller
        addChildViewController(attachmentController)
        // Add Child View as Subview
        currentView.addSubview(attachmentController.view)
        attachmentController.btnPost.isHidden = true
        attachmentController.btnPptAttatchment.isHidden = true
        attachmentController.btnLinkAttatchment.isHidden = true
        attachmentController.btnFileAttatchment.isHidden = true
        // Configure Child View
        attachmentController.view.frame = currentView.bounds
        attachmentController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        // Notify Child View Controller
        attachmentController.didMove(toParentViewController: self)
    }
    
    @IBAction func uploadCompanyProfileClicked(_ sender: Any) {
        FileAttatchmentHandler.shared.showDocumentPicker(vc: self, files: [String(kUTTypePDF)])
        FileAttatchmentHandler.shared.filePickedBlock = {[unowned self] (url) in
            do{
                let worker = NewServiceWorker()
                let data = try Data(contentsOf: url, options: .mappedIfSafe)
                print(data)
                let file = MediaAttatchment(file: nil, fileName: url.lastPathComponent,fileData:data,fileExtension:url.mimeType())
                worker.uploadCompanyProfile(fileData: file, completionHandler: { (response) in
                    if response != nil{
                        self.displayToast(response.message!)
                    }
                })
            }
            catch{
                
            }
        }
    }
    
    func getAllRequiredFields() -> [TJTextField]{
        var textFieldsArray = [TJTextField]()
        for txtFieldDetails in dataController.arrayOfTextFields{
            let txtField = TJTextField()
            txtField.placeholder = txtFieldDetails.placeholderText
            txtField.text = txtFieldDetails.textfieldText
            if currentProductModel != nil{
                if txtFieldDetails.placeholderText == "Product Details" || txtFieldDetails.placeholderText == "Transaction History"  || txtFieldDetails.placeholderText == "Upload Image"{
                    continue
                }
            }
            textFieldsArray.append(txtField)
        }
        
        return textFieldsArray
    }
    
    //MARK: Get all required fields
  /*  func getAllRequiredFields() -> [TJTextField] {
        var textFieldsArray = [TJTextField]()
        let cells = Utils.cellsForTableView(tblNewProduct) as! [PostNewJobTableViewCell]
        for cell in cells {
            if !cell.txtFieldInput.isHidden{
                textFieldsArray.append(cell.txtFieldInput)
            }
        }
        return textFieldsArray
    } */
    
    @objc func uploadProduct(){
        
        if Utils.validateFormAndShowToastWithRequiredFields(getAllRequiredFields()){
            if !(dataController.paramsDict["Product Description"]?.isEmpty)!{
                let newProductModel = NewProductRequestModel()
                newProductModel.images = attachmentController.imageNamesArray
                newProductModel.videos = attachmentController.videoArray
                newProductModel.logo = dataController.productLogo
                newProductModel.transaction = dataController.transactionHistory
                newProductModel.details = dataController.productDetails
                newProductModel.name = dataController.paramsDict["Name"]
                newProductModel.location = dataController.paramsDict["Location"]
                newProductModel.category = dataController.paramsDict["Category"]
                newProductModel.description = dataController.paramsDict["Product Description"]
                newProductModel.condition = dataController.paramsDict["Product Condition"]
//                let youTubeString = attachmentController.youTubeLinksArray.map{$0.fileName!}.joined(separator: ",")
                var youTubeString = attachmentController.youTubeLinksArray.map{$0.fileName!}.joined(separator: ",")
                if !youTubeString.isEmpty{
                    youTubeString = [youTubeString,currentProductModel?.pgyoutube].compactMap{$0}.joined(separator: ",")
                }else{
                    youTubeString = currentProductModel?.pgyoutube ?? ""
                }
                newProductModel.youTubeLink = youTubeString
                if (currentProductModel?.stype == "free" || currentProductModel?.stype == "paid" && currentProductModel?.renew == 0){
                    editProductDetails(requestModel: newProductModel)
                }else{
                    navigateToCheckOutController(requestModel: newProductModel)
                }
                
            }else{
                displayToast("Please enter description")
            }
           
        }
    }
    
    func navigateToCheckOutController(requestModel : NewProductRequestModel){
        let checkoutController = storyboard?.instantiateViewController(withIdentifier: "checkoutViewController") as! CheckoutViewController
        if currentProductModel != nil{
            checkoutController.isFromEdit = true
            checkoutController.idToEdit = currentProductModel?.id
        }
        checkoutController.productRequestModel = requestModel
        checkoutController.serviceType = SERVICE_TYPE_API.PRODUCTS.rawValue
        navigationController?.pushViewController(checkoutController, animated: true)
    }
    
    
    func editProductDetails(requestModel : NewProductRequestModel){
       
        let apiName = UPDATE_PRODUCT(productId: (currentProductModel?.id)!)
        
            let worker = CheckoutWorker()
            requestModel.stype = currentProductModel?.stype
            worker.postNewProduct(apiName:apiName,requestObject: requestModel) { (response, error) in
                if response != nil {
                    self.displayToast((response?.message) ?? "Please try again")
                  //  self.navigationController?.popToRootViewController(animated: true)
                    self.performSegue(withIdentifier: "navigateToNewProduct", sender: self)
                    STOP_LOADING_VIEW()
                }else{
                    self.displayToast(error?.localizedDescription ?? UNKNOWN_ERROR_MSG)
                }
            }
        
    }
    
    func navigateToPopUpController(sender:UITextField,optionsArray:[String]?){
        currentTextField = sender
        let popUpController = storyboard?.instantiateViewController(withIdentifier: "commonViewController") as! CommonViewController
        popUpController.modalPresentationStyle = .popover
        popUpController.preferredContentSize = CGSize(width : sender.frame.size.width , height : CGFloat((optionsArray?.count)! * COMMON_CONTROLLER_ROW_HEIGHT))
        let popOver = popUpController.popoverPresentationController
        popOver?.delegate = self
        popOver?.permittedArrowDirections = .up
        popOver?.sourceView = sender
        popOver?.sourceRect = sender.bounds
        popUpController.delegate = self
        popUpController.itemsArray = optionsArray
        present(popUpController, animated: true, completion: nil)
        
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return .none
    }
    func optionSelected(option: String?) {
        let selectedIndexPath: IndexPath = IndexPath(row: currentTextField!.tag, section: 0)
        dataController.arrayOfTextFields[selectedIndexPath.row].textfieldText = option
        currentTextField?.text = option ?? ""
        if currentTextField?.accessibilityLabel == "Product Condition"{
            switch option{
            case "New":
                dataController.paramsDict["Product Condition"] = "new"
            case "Used-Not Refurbished":
                dataController.paramsDict["Product Condition"] = "used"
            case "Used-Refurbished":
                dataController.paramsDict["Product Condition"] = "refurbished"
            default:
                break
              
            }
            
        }
    }
    
    
}

