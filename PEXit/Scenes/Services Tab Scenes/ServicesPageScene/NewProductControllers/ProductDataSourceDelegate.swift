//
//  ProductDataSourceDelegate.swift
//  PEXit
//
//  Created by ats on 23/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation
import UIKit
import MobileCoreServices

let UPLOAD_LOGO_IMAGE = "uploadlogo"

class ProductDataSourceDelegate: NSObject, UITableViewDataSource , UITableViewDelegate{
    
    var arrayOfTextFields = [TextfieldInfoStruct]()
    var parentController : NewProductViewController?
    var currentTblView : UITableView?
    var expandingCellHeight: CGFloat = 50
    var expandingIndexRow : Int?
    var updatedHeight: CGFloat?
    var paramsDict = [String : String]()
    var keys = [String]()
    var productLogo : MediaAttatchment?
    var productDetails : MediaAttatchment?
    var transactionHistory : MediaAttatchment?
    func generateTextFields(){
        keys = ["Name","Location","Category","Product Condition","Product Description"]
        currentTblView = parentController?.tblNewProduct
        arrayOfTextFields = [TextfieldInfoStruct(placeholderText: "Name",textfieldText:paramsDict["Name"]),
                             TextfieldInfoStruct(placeholderText: "Location",textfieldText:paramsDict["Location"]) ,
                             TextfieldInfoStruct(placeholderText: "Category",textfieldText:paramsDict["Category"]),
                             TextfieldInfoStruct(rightImage: UIImage(named: "uploaddocs"), placeholderText: "Product Details",textfieldText:paramsDict["Product Details"]),
                             TextfieldInfoStruct(rightImage: UIImage(named: UPLOAD_LOGO_IMAGE), placeholderText: "Upload Image",textfieldText:paramsDict["Upload Image"]),
                             TextfieldInfoStruct(rightImage: UIImage(named: "drop"), placeholderText: "Product Condition",textfieldText:paramsDict["Product Condition"]),
                             TextfieldInfoStruct(rightImage: UIImage(named: "uploaddocs"), placeholderText: "Transaction History",textfieldText:paramsDict["Transaction History"])]
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayOfTextFields.count + 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let postJobCell = tableView.dequeueReusableCell(withIdentifier: "postNewJobTableViewCell", for: indexPath) as! PostNewJobTableViewCell
        postJobCell.hideViews()
        if indexPath.row < arrayOfTextFields.count{
            postJobCell.txtFieldInput.delegate = self
            postJobCell.txtFieldInput.tag = indexPath.row
            postJobCell.txtFieldInput.isHidden = false
            postJobCell.configureTextfield(arrayOfTextFields[indexPath.row])
        }
        else if indexPath.row == arrayOfTextFields.count{
            postJobCell.viewTxtView.isHidden = false
            postJobCell.delegate = self
            postJobCell.configureTxtView(placeholderStr: "Description",currentText:paramsDict["Product Description"])
        }
        else if indexPath.row == (arrayOfTextFields.count) + 1{
           // postJobCell.viewAttachmentContainer.isHidden = false
            postJobCell.viewAttachment.isHidden = false
            parentController?.attachmentController.delegate = self
            parentController?.addSubView(currentView: postJobCell.viewAttachment)
        }
        else if indexPath.row == (arrayOfTextFields.count) + 2{
            postJobCell.btnSubmit.isHidden = false
            if parentController?.currentProductModel != nil{
                postJobCell.btnSubmit.setTitle("Save", for: .normal)
            }
            postJobCell.btnSubmit.addTarget(parentController, action: #selector(NewProductViewController.uploadProduct), for: .touchUpInside)
        }
        return postJobCell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row{
        case indexPath.row where indexPath.row < arrayOfTextFields.count:
            return 60
        case indexPath.row where indexPath.row == arrayOfTextFields.count:
           // return expandingCellHeight + 25
            return UITableViewAutomaticDimension
        case indexPath.row where indexPath.row == arrayOfTextFields.count + 1:
            return updatedHeight != nil ? updatedHeight! : DEFAULT_HEIGHT_ATTACHMENT_VIEW
        case indexPath.row where indexPath.row == arrayOfTextFields.count + 2:
            return 60
        default:
            return 60
            
        }
    }
}

extension ProductDataSourceDelegate : PassAttatchmentsToController , ExpandingCellDelegate , UITextFieldDelegate{
    
    
    func updateHeightOfParentController(height: CGFloat) {
        //Taking a constant
        updatedHeight = DEFAULT_HEIGHT_ATTACHMENT_VIEW + height
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
            self.currentTblView!.beginUpdates()
            self.currentTblView!.layoutIfNeeded()
            self.currentTblView!.endUpdates()
        })
        
    }
    
    func updated(height: CGFloat,currentTextView: UITextView) {
        paramsDict["Product Description"] = currentTextView.text
       // expandingCellHeight = height + 20
        UIView.setAnimationsEnabled(false)
        //        /* These will causes table cell heights to be recaluclated,
        //         without reloading the entire cell */
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
            self.currentTblView!.beginUpdates()
            //  let indexPath = IndexPath(row: self.expandingIndexRow!, section: 0)
            self.currentTblView!.layoutIfNeeded()
            
            self.currentTblView!.endUpdates()
            //        // Re-enable animations
            UIView.setAnimationsEnabled(true)
        })
        
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField.accessibilityLabel == "Product Condition" {
            parentController?.navigateToPopUpController(sender: textField, optionsArray: ["New","Used-Not Refurbished","Used-Refurbished"])
            parentController?.view.endEditing(true)
            return false
        }
        if textField.accessibilityLabel == "Upload Image" {
            uploadLogo(textField: textField)
            return false
        }
        if textField.accessibilityLabel == "Product Details" || textField.accessibilityLabel == "Transaction History"{
            uploadFile(textField: textField)
            return false
        }
        return true
    }
    
    //MARK: UITextFieldDelegate
    func textFieldDidEndEditing(_ textField: UITextField) {
        let selectedIndexPath: IndexPath = IndexPath(row: textField.tag, section: 0)
        arrayOfTextFields[selectedIndexPath.row].textfieldText = textField.text
        paramsDict[textField.accessibilityLabel!] = textField.text
        /*switch textField.placeholder{
        case "Name":
            paramsDict["Name"] = textField.text
        case "Location":
            paramsDict["Location"] = textField.text
        case "Category":
             paramsDict["Category"] = textField.text
        case "Product Condition":
             paramsDict["Product Condition"] = textField.text
        default:
            break
            
        } */
    }
    
    func uploadLogo(textField : UITextField){
       
        ImageVideoPicker.imageVideoPicker.showImagePicker(controller: parentController!, mediaType: .Image)
        ImageVideoPicker.imageVideoPicker.imagePickedBlock = { [unowned self](imageData) in
            //self.productLogo = imageData
            
            self.productLogo = MediaAttatchment(file: imageData.file, fileName: imageData.fileName,fileData:UIImageJPEGRepresentation(imageData.file as! UIImage, 0.4),fileExtension:imageData.fileExtension)
            
            textField.text = imageData.fileName
            let selectedIndexPath: IndexPath = IndexPath(row: textField.tag, section: 0)
            self.arrayOfTextFields[selectedIndexPath.row].textfieldText = textField.text
        }
    }
    
    func uploadFile(textField : UITextField){
        FileAttatchmentHandler.shared.showDocumentPicker(vc: parentController!, files: [String(kUTTypePDF)])
        FileAttatchmentHandler.shared.filePickedBlock = {[unowned self] (url) in
            do{
                let data = try Data(contentsOf: url, options: .mappedIfSafe)
                print(data)
                let mediaFile = MediaAttatchment(file: nil, fileName: url.lastPathComponent,fileData:data,fileExtension:url.mimeType())
                if textField.accessibilityLabel == "Transaction History"{
                    self.transactionHistory = mediaFile
                }else{
                    self.productDetails = mediaFile
                }
            }catch{
            }
            textField.text = url.lastPathComponent
            let selectedIndexPath: IndexPath = IndexPath(row: textField.tag, section: 0)
            self.arrayOfTextFields[selectedIndexPath.row].textfieldText = textField.text
        }
    }
}
