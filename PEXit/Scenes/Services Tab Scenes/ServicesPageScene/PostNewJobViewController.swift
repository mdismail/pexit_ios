//
//  PostNewJobViewController.swift
//  PEXit
//
//  Created by ats on 20/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0


class PostNewJobViewController: UIViewController , UIPopoverPresentationControllerDelegate , ItemSelected{
    @IBOutlet weak var tblPostjob: UITableView!
    var countryArray = [String]()
    var stateArray = [String]()
    var cityArray = [String]()
    let newJobRequestModel = NewJobRequestModel()
    var currentTextField : UITextField?
    var currentJobModel : JobDetailModel?
    
    lazy var dataController: PostJobDataSourceDelegate = {
        let dataController = PostJobDataSourceDelegate()
        dataController.parentController = self
        dataController.generateTextFields()
        return dataController
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addBackButton(withTitle:"Post Job")
        tblPostjob.register(UINib(nibName: "PostNewJobTableViewCell", bundle: nil), forCellReuseIdentifier: "postNewJobTableViewCell")
        tblPostjob.dataSource = dataController
        tblPostjob.delegate = dataController
        if currentJobModel != nil{
            setUpEditData()
        }
        // Do any additional setup after loading the view.
    }
    
    func setUpEditData(){
        
        //dataController.paramsDict["Upload Logo"] = currentJobModel?.logo
        if let logo = currentJobModel?.logo{
            if let pathUrl = URL(string:logo.urlEncodedString()){
                dataController.paramsDict["Upload Logo"] = pathUrl.lastPathComponent
            }
        }
        dataController.paramsDict["Job Title"] = currentJobModel?.title
        dataController.paramsDict["Company"] = currentJobModel?.cmpny
        dataController.paramsDict["Job Function"] = currentJobModel?.funct
        dataController.paramsDict["Employee Type"] = currentJobModel?.emptyp
        dataController.paramsDict["Seniority Level"] = currentJobModel?.level
        dataController.paramsDict["Country"] = currentJobModel?.country
        dataController.paramsDict["State"] = currentJobModel?.states
        dataController.paramsDict["City"] = currentJobModel?.addr
        dataController.paramsDict["Choose An Industry"] = currentJobModel?.indus
        dataController.paramsDict["Postal Code"] = currentJobModel?.pincod
        if currentJobModel?.prsnl == "show" || currentJobModel?.prsnl == "yes"{
            dataController.showMyProfile = true
        }else{
            dataController.showMyProfile = false
        }
        if currentJobModel?.apply == "website"{
            dataController.isExternalLink = true
        }else{
            dataController.isExternalLink = false
            
        }
        dataController.companyDesc = currentJobModel?.cmpnydesc
        dataController.jobDesc = currentJobModel?.jdesc
        dataController.externalSiteLink = currentJobModel?.website
        self.newJobRequestModel.city = currentJobModel?.addr
        self.newJobRequestModel.country = currentJobModel?.country
        self.newJobRequestModel.state = currentJobModel?.states
        dataController.generateTextFields()
    }
    
    
    func getAllRequiredFields() -> [TJTextField]{
        var textFieldsArray = [TJTextField]()
        for txtFieldDetails in dataController.arrayOfTextFields{
            let txtField = TJTextField()
            txtField.placeholder = txtFieldDetails.placeholderText
            txtField.text = txtFieldDetails.textfieldText
            if currentJobModel != nil{
                if txtFieldDetails.placeholderText == "Upload Logo"{
                    continue
                }
            }
            textFieldsArray.append(txtField)
        }
        return textFieldsArray
    }
    
    @objc func uploadJob(){
        if Utils.validateFormAndShowToastWithRequiredFields(getAllRequiredFields()){
        if validateLink(){
        if dataController.jobDesc != nil && !(dataController.jobDesc?.isEmpty)!{
            newJobRequestModel.jobLogo = dataController.logo
            newJobRequestModel.jobTitle = dataController.paramsDict["Job Title"]
            newJobRequestModel.jobCompany = dataController.paramsDict["Company"]
            newJobRequestModel.jobFunction = dataController.paramsDict["Job Function"]
            newJobRequestModel.empType = dataController.paramsDict["Employee Type"]
            newJobRequestModel.level = dataController.paramsDict["Seniority Level"]
            newJobRequestModel.industry = dataController.paramsDict["Choose An Industry"]
            newJobRequestModel.postalCode = dataController.paramsDict["Postal Code"]
            newJobRequestModel.companyDesc = dataController.companyDesc
            newJobRequestModel.jobDesc = dataController.jobDesc
            newJobRequestModel.applyType = dataController.isExternalLink == true ? "website" : "profile"
            newJobRequestModel.jobApplySite = dataController.externalSiteLink
            newJobRequestModel.jobView = dataController.showMyProfile == true ? "yes" : "no"
            newJobRequestModel.email = ProfileRLMModel.fetchProfile()?.userEmail
            if (currentJobModel?.stype == "free" || currentJobModel?.stype == "paid" && currentJobModel?.renew == 0){
                editJobDetails(requestModel: newJobRequestModel)
            }else{
                navigateToCheckOutController(requestModel: newJobRequestModel)
                }
            
            }else{
                displayToast("Please enter job description")
            }
            }
        }
        
    }
    
    func validateLink() -> Bool{
        if dataController.isExternalLink{
            if dataController.externalSiteLink != nil &&
                !(dataController.externalSiteLink?.isEmpty)!{
                if !(dataController.externalSiteLink?.trimmingCharacters(in: .whitespacesAndNewlines).isValidURL)!{
                    displayToast(ENTER_VALID_LINK_TOAST)
                    return false
                }else{
                    return true
                }
            }else{
                displayToast("Please enter link")
                return false
            }
        }
        else{
            return true
        }
    }
    
    func navigateToCheckOutController(requestModel : NewJobRequestModel){
        let checkoutController = storyboard?.instantiateViewController(withIdentifier: "checkoutViewController") as! CheckoutViewController
        if currentJobModel != nil{
            checkoutController.isFromEdit = true
            checkoutController.idToEdit = currentJobModel?.id
        }
        checkoutController.jobRequestModel = requestModel
        checkoutController.serviceType = SERVICE_TYPE_API.JOBS.rawValue
        navigationController?.pushViewController(checkoutController, animated: true)
    }
    
    func editJobDetails(requestModel : NewJobRequestModel){
        let apiName = UPDATE_JOB(jobId: (currentJobModel?.id!)!)
        let worker = CheckoutWorker()
        worker.postNewJob(apiName: apiName, requestObject: requestModel) { (response, error) in
            if response != nil {
                self.displayToast((response?.message) ?? "Please try again")
                // self.navigationController?.popToRootViewController(animated: true)
                self.performSegue(withIdentifier: "navigateToNewJob", sender: self)
                STOP_LOADING_VIEW()
            }else{
                self.displayToast(error?.localizedDescription ?? UNKNOWN_ERROR_MSG)
            }
        }
        
    }
    
    func fetchCountries(textField : UITextField){
        let worker = RegistrationWorker()
        worker.fetchCountries { [weak self](countryResponse) in
            if countryResponse.datas != nil{
                self?.countryArray.removeAll()
                for country in countryResponse.datas!{
                    self?.countryArray.append(country.name!)
                }
                DispatchQueue.main.async {
                    self?.showCountryPicker(textField: textField,title:"Country",currentArray: (self?.countryArray)!)
                }
            }
        }
    }
    
    
    func fetchStates(textField : UITextField){
        let worker = RegistrationWorker()
        
        for (_, element) in (dataController.arrayOfTextFields.enumerated()) {
            if element.placeholderText == "Country"{
                if let countryName = element.textfieldText ,!countryName.isEmpty{
                    worker.fetchStates(countryName: countryName) {[weak self] (stateResponse) in
                        if stateResponse.datas != nil{
                            self?.stateArray.removeAll()
                            for state in stateResponse.datas!{
                                self?.stateArray.append(state.name!)
                            }
                            DispatchQueue.main.async {
                                self?.showCountryPicker(textField: textField,title:"State",currentArray:(self?.stateArray)!)
                            }
                        }
                        
                    }
                }else{
                    self.displayToast(CHOOSE_COUNTRY_TOAST)
                }
                
            }
        }
    }
    
    func fetchCities(textField : UITextField){
        let worker = RegistrationWorker()
        for (_, element) in (dataController.arrayOfTextFields.enumerated()) {
            if element.placeholderText == "State"{
                
                if let stateName = element.textfieldText , !stateName.isEmpty{
                    worker.fetchCities(stateName: stateName) {[weak self] (cityResponse) in
                        
                        if cityResponse.datas != nil{
                            self?.cityArray.removeAll()
                            for city in cityResponse.datas!{
                                self?.cityArray.append(city.name!)
                            }
                            DispatchQueue.main.async {
                                self?.showCountryPicker(textField: textField,title:"City",currentArray:(self?.cityArray)!)
                            }
                        }
                        
                    }
                }
                else{
                    self.displayToast(CHOOSE_STATE_TOAST)
                }
                
            }
        }
    }
    
    func showCountryPicker(textField : UITextField , title : String , currentArray : [String]){
        ActionSheetStringPicker.show(withTitle: title, rows: currentArray, initialSelection: 0, doneBlock: {[unowned self] (key, index, value) in
            textField.text  = value as? String
            let selectedIndexPath: IndexPath = IndexPath(row: textField.tag, section: 0)
            self.dataController.arrayOfTextFields[selectedIndexPath.row].textfieldText = textField.text
            switch title{
            case "Country":
                self.newJobRequestModel.country = value as? String
                for (index, element) in (self.dataController.arrayOfTextFields.enumerated()) {
                    if element.placeholderText == "State" || element.placeholderText == "City"{
                        
                        self.dataController.arrayOfTextFields[index].textfieldText = ""
                    }
                    self.tblPostjob.reloadData()
                }
                
            case "State":
                self.newJobRequestModel.state = value as? String
                for (index, element) in (self.dataController.arrayOfTextFields.enumerated()) {
                    if element.placeholderText == "City"{
                        self.dataController.arrayOfTextFields[index].textfieldText = ""
                    }
                }
                self.tblPostjob.reloadData()
            case "City":
                self.newJobRequestModel.city = value as? String
            default:
                print("")
            }
            
            }, cancel: { (picker) in
                
        }, origin: self.view)
    }
    
    
    func navigateToPopUpController(sender:UITextField,optionsArray:[String]?){
        currentTextField = sender
        let popUpController = storyboard?.instantiateViewController(withIdentifier: "commonViewController") as! CommonViewController
        popUpController.modalPresentationStyle = .popover
        popUpController.preferredContentSize = CGSize(width : sender.frame.size.width , height : CGFloat((optionsArray?.count)! * COMMON_CONTROLLER_ROW_HEIGHT))
        let popOver = popUpController.popoverPresentationController
        popOver?.delegate = self
        popOver?.permittedArrowDirections = .up
        popOver?.sourceView = sender
        popOver?.sourceRect = sender.bounds
        popUpController.delegate = self
        popUpController.itemsArray = optionsArray
        present(popUpController, animated: true, completion: nil)
        
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return .none
    }
    
    func optionSelected(option: String?) {
        currentTextField?.text = option ?? ""
        let selectedIndexPath: IndexPath = IndexPath(row: currentTextField!.tag, section: 0)
        dataController.paramsDict[(currentTextField?.accessibilityLabel)!] = option
        dataController.arrayOfTextFields[selectedIndexPath.row].textfieldText = option
    }
}

