//
//  ServiceDataSourceDelegate.swift
//  PEXit
//
//  Created by Apple on 18/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation
import UIKit
import ExpandableLabel



class ServiceDataSourceDelegate: NSObject, UITableViewDataSource , UITableViewDelegate, ExpandingDelegate{
    var productArray = [ServiceProductModel]()
    var jobsArray = [JobDetailModel]()
    var sponsorArray = [SponsorModel]()
    var adsArray = [AdsModel]()
    var controllerType : ControllerType.RawValue?
    var currentTblView : UITableView?
    var states : Array<Bool>!
    var parentController : ProductsServicesViewController?
    //func configureTableView(controllerType)
    var pageNumber = 1
    var isMoreDataAvailable = true
   
    func setUpTableView(pageNum : Int? = 1,moreData : Bool? = true){
        pageNumber = pageNum!
        isMoreDataAvailable = moreData!
        parentController?.tblResult.reloadData()
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        currentTblView = tableView
       
        switch ControllerType(rawValue: controllerType!)!{
        case .Product ,
             .Services ,
             .Software:
            return productArray.count
        case .Jobs:
            //states = [Bool](repeating: true, count: (jobsArray?.count) ?? 0)
            //print("Arrat --->",states)
            return jobsArray.count
        case .Sponsor:
            return sponsorArray.count
        case .Ads:
            //states = [Bool](repeating: true, count: (adsArray?.count) ?? 0)
            return adsArray.count
        default:
           return 0
        }
        
       
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch ControllerType(rawValue: controllerType!)!{
        case .Product:
            let cell = tableView.dequeueReusableCell(withIdentifier: "editServiceTableViewCell", for: indexPath) as! EditSeviceTableViewCell
            cell.btnEdit.tag = indexPath.row
            cell.btnEdit.removeTarget(nil, action: nil, for: .allEvents)
            cell.btnEdit.addTarget(ProductsServicesViewController(), action: #selector(ProductsServicesViewController.btnEditClicked(_:)), for: .touchUpInside)
            cell.setProductData(productModel:productArray[indexPath.row])
            return cell
        case .Services:
            let cell = tableView.dequeueReusableCell(withIdentifier: "editServiceTableViewCell", for: indexPath) as! EditSeviceTableViewCell
            cell.btnEdit.tag = indexPath.row
             cell.btnEdit.removeTarget(nil, action: nil, for: .allEvents)
            cell.btnEdit.addTarget(ProductsServicesViewController(), action: #selector(ProductsServicesViewController.btnEditClicked(_:)), for: .touchUpInside)
            cell.setServiceData(productModel:productArray[indexPath.row])
            return cell
        case .Software:
            let cell = tableView.dequeueReusableCell(withIdentifier: "editServiceTableViewCell", for: indexPath) as! EditSeviceTableViewCell
            cell.delegate = self
            cell.softwareArray = productArray
            cell.btnEdit.tag = indexPath.row
        //    cell.btnReadMore.tag = indexPath.row
            cell.btnEdit.removeTarget(nil, action: nil, for: .allEvents)
            cell.btnEdit.addTarget(ProductsServicesViewController(), action: #selector(ProductsServicesViewController.btnEditClicked(_:)), for: .touchUpInside)
            cell.setSoftwareData(productModel:productArray[indexPath.row])
            return cell
        case .Jobs:
            let cell = tableView.dequeueReusableCell(withIdentifier: "editJobTableViewCell", for: indexPath) as! EditJobTableViewCell
           // setUpExpandableLabel(cell: cell, currentLbl: cell.lblJobDesc)
            cell.btnEdit.tag = indexPath.row
            cell.btnEdit.removeTarget(nil, action: nil, for: .allEvents)
            cell.btnEdit.addTarget(ProductsServicesViewController(), action: #selector(ProductsServicesViewController.btnEditClicked(_:)), for: .touchUpInside)
           cell.setJobData(jobModel: jobsArray[indexPath.row])
          // cell.lblJobDesc.collapsed = states[indexPath.row]
            return cell
        case .Sponsor:
            let cell = tableView.dequeueReusableCell(withIdentifier: "editServiceTableViewCell", for: indexPath) as! EditSeviceTableViewCell
            cell.btnEdit.removeTarget(nil, action: nil, for: .allEvents)
            cell.btnEdit.tag = indexPath.row
            cell.btnEdit.addTarget(ProductsServicesViewController(), action: #selector(ProductsServicesViewController.btnEditClicked(_:)), for: .touchUpInside)
            cell.delegate = self
            cell.sponsorArray = self.sponsorArray
            cell.setSponosrData(sponsorModel:sponsorArray[indexPath.row])
            return cell
        case .Ads:
            let cell = tableView.dequeueReusableCell(withIdentifier: "adsListTableViewCell", for: indexPath) as! AdsListTableViewCell
            cell.btnMore.tag  = indexPath.row
            cell.adsArray = self.adsArray
            cell.btnEdit.removeTarget(nil, action: nil, for: .allEvents)
            cell.btnEdit.tag = indexPath.row
            cell.btnEdit.addTarget(ProductsServicesViewController(), action: #selector(ProductsServicesViewController.btnEditClicked(_:)), for: .touchUpInside)
          //  setUpExpandableLabel(cell: cell, currentLbl: cell.lblAdDesc)
            cell.setAdsData(adsModel:self.adsArray[indexPath.row])
            cell.delegate = self
           // cell.lblAdDesc.collapsed = self.states[indexPath.row]
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "editServiceTableViewCell", for: indexPath)
            return cell
        }
        
    }
    
    func tableView(_ tableView : UITableView,heightForRowAt indexPath: IndexPath) -> CGFloat{
        return UITableViewAutomaticDimension
    }
    
  
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       // tableView.tableFooterView?.isHidden = true
        switch ControllerType(rawValue: controllerType!)!{
        case .Product:
            let currentProductModel = productArray[indexPath.row]
            if currentProductModel.active == 1{
                 navigateToProductDetailScreen(productId: currentProductModel.id!)
            }
           
        case .Services:
            let currentServiceModel = productArray[indexPath.row]
            if currentServiceModel.active == 1{
                navigateToServiceDetailsScreen(serviceId: currentServiceModel.id!)
            }
            
        case .Software:
            let currentSoftwareModel = productArray[indexPath.row]
            if currentSoftwareModel.active == 1{
                navigateToSoftwareDetailsScreen(softwareModel: currentSoftwareModel)
            }
        case .Jobs:
          //  let jobModel = jobsArray![indexPath.row]
            let currentJobModel = JobModel()
            currentJobModel.id = jobsArray[indexPath.row].id!
            if jobsArray[indexPath.row].active == 1{
                 navigateToJobDetailsScreen(jobModel: currentJobModel)
            }
           
        case .Sponsor:
            let currentSponsorModel = sponsorArray[indexPath.row]
            if currentSponsorModel.active == 1{
               navigateToSponsorDetailsScreen(sponsorModel:currentSponsorModel)
            }
            
        default:
        break
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
       
        if isMoreDataAvailable == true && indexPath.row == (NUMBER_OF_RECORDS * pageNumber) - 1{
            isMoreDataAvailable = false
            pageNumber = pageNumber + 1
            tableView.tableFooterView = LoadingIndicator.init(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44)))
            tableView.tableFooterView?.isHidden = false
            
            parentController?.loadMoreData(currentPageNumber : pageNumber)
          //  fetchPosts(pageNumber: pageNumber, numberOfRecords: NUMBER_OF_RECORDS)
        }
    }
    
    func navigateToProductDetailScreen(productId : Int){
        let nextController = parentController?.storyboard?.instantiateViewController(withIdentifier: "productDetailController") as! ProductDetailViewController
        nextController.currentProductModel = productId
        nextController.userId = Defaults.getUserID()
        parentController?.navigationController?.pushViewController(nextController, animated: true)
    }
    
    func navigateToServiceDetailsScreen(serviceId : Int){
        let detailsController = parentController?.storyboard?.instantiateViewController(withIdentifier: "serviceDetailViewController") as! ServiceDetailViewController
        detailsController.idOfService = serviceId
        parentController?.navigationController?.pushViewController(detailsController, animated: true)
    }
    
    func navigateToSoftwareDetailsScreen(softwareModel : ServiceProductModel){
      let detailsController = parentController?.storyboard?.instantiateViewController(withIdentifier: "softwareDetailViewController") as! SoftwareDetailViewController
        let currentSoftwareModel = SoftwareModel()
        currentSoftwareModel.name = softwareModel.name
        currentSoftwareModel.descrip = softwareModel.descrip
        currentSoftwareModel.logo = softwareModel.logo
        currentSoftwareModel.url = softwareModel.url
        currentSoftwareModel.software = softwareModel.software
        detailsController.softwareModel = currentSoftwareModel
        parentController?.navigationController?.pushViewController(detailsController, animated: true)
        
    }
    
    func navigateToJobDetailsScreen(jobModel: JobModel){
        let destinationController = parentController?.storyboard?.instantiateViewController(withIdentifier: "jobDetailViewController") as! JobDetailViewController
         destinationController.currentJobModel = jobModel
    parentController?.navigationController?.pushViewController(destinationController, animated: true)
    }
    
    func navigateToSponsorDetailsScreen(sponsorModel: SponsorModel){
        let destinationController = parentController?.storyboard?.instantiateViewController(withIdentifier: "sponsorContentDetailViewController") as! SponsorContentDetailViewController
        destinationController.currentSponsorModel = sponsorModel
       parentController?.navigationController?.pushViewController(destinationController, animated: true)
    }
    
    

    // MARK: - my cell delegate
    func moreTapped(cell: Any) {
        // this will "refresh" the row heights, without reloading
        currentTblView?.beginUpdates()
        currentTblView?.endUpdates()
        
        // do anything else you want because the switch was changed
        
    }
    
    
  /*  func setUpExpandableLabel(cell:UITableViewCell,currentLbl : ExpandableLabel){
        currentLbl.delegate = self
        cell.layoutIfNeeded()
        currentLbl.shouldCollapse = true
        currentLbl.numberOfLines = 3
        currentLbl.setLessLinkWith(lessLink: "Close", attributes: [.foregroundColor:UIColor.red], position: .right)
        //currentLbl.shouldCollapse = true
    }
    
    func willExpandLabel(_ label: ExpandableLabel) {
        currentTblView?.beginUpdates()
    }

    func didExpandLabel(_ label: ExpandableLabel) {
        let point = label.convert(CGPoint.zero, to: currentTblView)
        if let indexPath = currentTblView?.indexPathForRow(at: point) as IndexPath? {
            states[indexPath.row] = false
            DispatchQueue.main.async { [weak self] in
                self?.currentTblView?.scrollToRow(at: indexPath, at: .top, animated: true)
            }
        }
        currentTblView?.endUpdates()
    }

    func willCollapseLabel(_ label: ExpandableLabel) {
        currentTblView?.beginUpdates()
    }

    func didCollapseLabel(_ label: ExpandableLabel) {
        let point = label.convert(CGPoint.zero, to: currentTblView)
        if let indexPath = currentTblView?.indexPathForRow(at: point) as IndexPath? {
            states[indexPath.row] = true
            DispatchQueue.main.async { [weak self] in
                self?.currentTblView?.scrollToRow(at: indexPath, at: .top, animated: true)
            }
        }
        currentTblView?.endUpdates()
    } */
}
    

