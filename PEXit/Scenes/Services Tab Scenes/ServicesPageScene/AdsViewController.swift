//
//  AdsViewController.swift
//  PEXit
//
//  Created by Apple on 18/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class AdsViewController: UIViewController , ItemSelected , AdsDetailDelegate{

    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var stepSlider: StepSlider!
    var selectedLayout : String?
    var newAdRequestModel = NewAdsRequestModel()
    var currentAdModel : AdsModel?
    var previousIndex : UInt = 0
    
    private lazy var layoutViewController: AdsLayoutViewController = {
        // Instantiate View Controller
        var viewController = storyboard?.instantiateViewController(withIdentifier: "adsLayoutViewController") as! AdsLayoutViewController
        viewController.currentAdModel = currentAdModel
        viewController.delegate = self
        return viewController
    }()
    
    private lazy var detailsController : AdsDetailViewController = {
       var detailController = storyboard?.instantiateViewController(withIdentifier: "adsDetailViewController") as! AdsDetailViewController
        if currentAdModel != nil{
            detailController.currentAdModel = currentAdModel
        }
        detailController.delegate = self
        return detailController
    }()
    private lazy var audienceController : AdsAudienceViewController = {
       var audienceController = storyboard?.instantiateViewController(withIdentifier: "adsAudienceViewController") as! AdsAudienceViewController
        if currentAdModel != nil{
            audienceController.currentAdModel = currentAdModel
        }
        audienceController.delegate = self
        return audienceController
    }()
    private lazy var previewController : AdsPreviewViewController = {
        var adsPreviewViewController = storyboard?.instantiateViewController(withIdentifier: "adsPreviewViewController") as! AdsPreviewViewController
        if currentAdModel != nil{
            adsPreviewViewController.currentAdModel = currentAdModel
        }
        adsPreviewViewController.delegate = self
        return adsPreviewViewController
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        stepSlider.labels = ["Layout","Details","Target Audience","Preview"]
        stepSlider.isUserInteractionEnabled = true
        add(childViewController: layoutViewController)
        
        if currentAdModel != nil{
            selectedLayout = "Layout_\((currentAdModel?.layout)!)"
            newAdRequestModel.adYouTubeLink = currentAdModel?.utub
            newAdRequestModel.adDescription = currentAdModel?.post
            newAdRequestModel.adTitle = currentAdModel?.title
            newAdRequestModel.adAge = currentAdModel?.age
            newAdRequestModel.adGender = currentAdModel?.gender
            newAdRequestModel.adLink = currentAdModel?.url
            newAdRequestModel.adLayout = "Layout_\((currentAdModel?.layout)!)"
        }
        addBackButton(withTitle: "Post Ad")
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func sliderIndexChanged(_ sender: StepSlider) {
       if self.childViewControllers.count > 0{
                remove(childViewController: self.childViewControllers[0])
        }
        switch sender.index{
        case 0 :
            add(childViewController: layoutViewController)
        case 1:
            add(childViewController: detailsController)
        case 2:
            add(childViewController: audienceController)
        case 3:
            if selectedLayout != nil{
                 previewController.adsRequestModel = newAdRequestModel
                 add(childViewController: previewController)
            }else{
                displayToast(CHOOSE_LAYOUT)
            }
            
        default:
            break
        }
    }
    
    
     func add(childViewController viewController: UIViewController) {
        // Add Child View Controller
        addChildViewController(viewController)
        
        // Add Child View as Subview
        viewContainer.addSubview(viewController.view)
        if viewController is AdsDetailViewController{
             detailsController.layoutType = selectedLayout
        }
        // Configure Child View
        viewController.view.frame = viewContainer.bounds
        viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        // Notify Child View Controller
        viewController.didMove(toParentViewController: self)
    }
    
     func remove(childViewController viewController: UIViewController) {
        // Notify Child View Controller
        viewController.willMove(toParentViewController: nil)
        
        // Remove Child View From Superview
        viewController.view.removeFromSuperview()
        
        // Notify Child View Controller
        viewController.removeFromParentViewController()
    }
    
    func optionSelected(option: String?) {
        selectedLayout = option
        newAdRequestModel.adLayout = selectedLayout
        detailsController.layoutType = option
        stepSlider.setIndex(1, animated: true)
    }
    
    func navigateToCheckOutController(adsRequestModel : NewAdsRequestModel){
        let checkoutController = storyboard?.instantiateViewController(withIdentifier: "checkoutViewController") as! CheckoutViewController
        checkoutController.adsRequestModel = adsRequestModel
        if currentAdModel != nil{
            checkoutController.isFromEdit = true
            checkoutController.idToEdit = currentAdModel?.id
        }
        checkoutController.serviceType = SERVICE_TYPE_API.AD.rawValue
        navigationController?.pushViewController(checkoutController, animated: true)
    }
    
    func editAdDetails(requestModel : NewAdsRequestModel){
        let apiName = UPDATE_AD(adID: (currentAdModel?.id)!)
        let worker = CheckoutWorker()
        worker.postNewAd(apiName: apiName, requestObject: requestModel) { (response, error) in
            if response != nil {
                self.displayToast((response?.message) ?? "Please try again")
               // self.navigationController?.popToRootViewController(animated: true)
                self.performSegue(withIdentifier: "navigateToNewAd", sender: self)
                STOP_LOADING_VIEW()
            }else{
                self.displayToast(error?.localizedDescription ?? UNKNOWN_ERROR_MSG)
            }
        }
        
    }
    
    func passAdDetails(adRequestModel : NewAdsRequestModel){
        newAdRequestModel = adRequestModel
        audienceController.adsRequestModel = adRequestModel
        previewController.adsRequestModel = adRequestModel
        switch stepSlider.index{
        case 1:
            stepSlider.setIndex(2, animated: true)
            break
        case 2:
            stepSlider.setIndex(3, animated: true)
            break
        case 3:
            if (currentAdModel?.stype == "free" || currentAdModel?.stype == "paid" && currentAdModel?.renew == 0){
                editAdDetails(requestModel: adRequestModel)
            }else{
                navigateToCheckOutController(adsRequestModel: adRequestModel)
            }
            
            break
        default:
            break
        }
    }
}


