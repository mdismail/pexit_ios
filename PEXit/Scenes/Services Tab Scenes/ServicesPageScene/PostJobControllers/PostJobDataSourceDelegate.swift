//
//  PostJobDataSourceDelegate.swift
//  PEXit
//
//  Created by ats on 20/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation
import UIKit

class PostJobDataSourceDelegate: NSObject, UITableViewDataSource , UITableViewDelegate{

    var arrayOfTextFields = [TextfieldInfoStruct]()
    var parentController : PostNewJobViewController?
    var currentTblView : UITableView?
    var expandingCellHeight: CGFloat = 50
    var expandingDescHeight: CGFloat = 50
    var expandingIndexRow : Int?
    var logo : MediaAttatchment?
    var paramsDict = [String : String]()
    var jobDesc : String?
    var companyDesc : String?
    var showMyProfile = false
    var isExternalLink = false
    var currentCell : PostNewJobTableViewCell?
    var externalSiteLink : String?
   
    func generateTextFields(){
        currentTblView = parentController?.tblPostjob
        arrayOfTextFields = [TextfieldInfoStruct(rightImage: UIImage(named: UPLOAD_LOGO_IMAGE),placeholderText: "Upload Logo",textfieldText:paramsDict["Upload Logo"]),
                             TextfieldInfoStruct(placeholderText: "Job Title",textfieldText:paramsDict["Job Title"]) ,
                             TextfieldInfoStruct(placeholderText: "Company",textfieldText:paramsDict["Company"]),
                             TextfieldInfoStruct(rightImage: UIImage(named: "drop"), placeholderText: "Country",textfieldText:paramsDict["Country"]),
                             TextfieldInfoStruct(rightImage: UIImage(named: "drop"), placeholderText: "State",textfieldText:paramsDict["State"]),
                             TextfieldInfoStruct(rightImage: UIImage(named: "drop"), placeholderText: "City",textfieldText: paramsDict["City"]),
                              TextfieldInfoStruct(placeholderText: "Postal Code",textfieldText: paramsDict["Postal Code"]),
                             TextfieldInfoStruct(placeholderText: "Job Function",textfieldText:paramsDict["Job Function"]),
                             TextfieldInfoStruct(rightImage: UIImage(named: "drop"),placeholderText: "Employee Type",textfieldText:paramsDict["Employee Type"]),
                             TextfieldInfoStruct(rightImage: UIImage(named: "drop"),placeholderText: "Seniority Level",textfieldText:paramsDict["Seniority Level"]),
                             TextfieldInfoStruct(placeholderText: "Choose An Industry",textfieldText:paramsDict["Choose An Industry"])]
        
     }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return arrayOfTextFields.count + 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let postJobCell = tableView.dequeueReusableCell(withIdentifier: "postNewJobTableViewCell", for: indexPath) as! PostNewJobTableViewCell
        postJobCell.hideViews()
        if indexPath.row < arrayOfTextFields.count{
            postJobCell.txtFieldInput.delegate = self
            postJobCell.txtFieldInput.tag = indexPath.row
            postJobCell.txtFieldInput.isHidden = false
            postJobCell.configureTextfield(arrayOfTextFields[indexPath.row])
        }
        else if indexPath.row == arrayOfTextFields.count{
           postJobCell.viewTxtView.isHidden = false
           postJobCell.delegate = self
           postJobCell.configureTxtView(placeholderStr: "Job Description",currentText: jobDesc)
        }
        else if indexPath.row == (arrayOfTextFields.count) + 1{
            postJobCell.viewTxtView.isHidden = false
            postJobCell.delegate = self
            postJobCell.configureTxtView(placeholderStr: "Company Description",currentText: companyDesc)
        }
        else if indexPath.row == (arrayOfTextFields.count) + 2{
           currentCell = postJobCell
            postJobCell.txtFieldLink.delegate = self
            postJobCell.txtFieldEmail.delegate = self
            postJobCell.viewJobOptions.isHidden = false
            postJobCell.btnShowMyProfile.addTarget(self, action: #selector(PostJobDataSourceDelegate.btnShowMyProfileClicked(_:)), for: .touchUpInside)
             postJobCell.btnExternalSite.addTarget(self, action: #selector(PostJobDataSourceDelegate.btnApplyTypeClicked(_:)), for: .touchUpInside)
             postJobCell.btnApplyWithProfile.addTarget(self, action: #selector(PostJobDataSourceDelegate.btnApplyTypeClicked(_:)), for: .touchUpInside)
             postJobCell.setUpJobOptionsView(showMyProfile: showMyProfile, isExternalLink: isExternalLink,linkstr: externalSiteLink)
        }
        else if indexPath.row == (arrayOfTextFields.count) + 3{
           postJobCell.btnSubmit.isHidden = false
           if parentController?.currentJobModel != nil{
                postJobCell.btnSubmit.setTitle("Save", for: .normal)
            }
           postJobCell.btnSubmit.addTarget(parentController, action: #selector(PostNewJobViewController.uploadJob), for: .touchUpInside)
        }
       return postJobCell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row{
        case indexPath.row where indexPath.row < arrayOfTextFields.count:
            return 65
        case indexPath.row where indexPath.row == arrayOfTextFields.count:
           // return expandingCellHeight + 25
            return UITableViewAutomaticDimension
        case indexPath.row where indexPath.row == arrayOfTextFields.count + 1:
           // return expandingDescHeight + 25
            return UITableViewAutomaticDimension
        case indexPath.row where indexPath.row == arrayOfTextFields.count + 2:
            return 310
        case indexPath.row where indexPath.row == arrayOfTextFields.count + 3:
            return 100
        default:
            return 60
        
        }
    }
    
     @objc func btnShowMyProfileClicked(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.isSelected{
            showMyProfile = true
        }else{
            showMyProfile = false
        }
    }
    
    @objc func btnApplyTypeClicked(_ sender: UIButton) {
        currentCell?.btnApplyWithProfile.isSelected = false
        currentCell?.btnExternalSite.isSelected = false
        sender.isSelected = !sender.isSelected
        if sender.tag == 1{
            isExternalLink = false
        }else{
            isExternalLink = true
        }
    }
}

extension PostJobDataSourceDelegate : UITextFieldDelegate , ExpandingCellDelegate{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
       switch textField.accessibilityLabel{
        case "Employee Type":
            parentController?.navigateToPopUpController(sender: textField, optionsArray: ["Part-Time","Full-Time","Contract","Temporary","Volunteer","Other"])
//            parentController?.view.endEditing(true)
//            return false
        case "Seniority Level":
            parentController?.navigateToPopUpController(sender: textField, optionsArray: ["Not Applicable","Internship","Entry level","Associate","Mid-Senior Level","Director","Executive"])
//            parentController?.view.endEditing(true)
//            return false
        case "Upload Logo":
            uploadLogo(textField: textField)
            return false
        case "Country":
            parentController?.fetchCountries(textField: textField)
            return false
        case "State":
            parentController?.fetchStates(textField: textField)
            return false
        case "City":
            parentController?.fetchCities(textField: textField)
            return false
        default:
            return true
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.08) {
            self.parentController?.dismissKeyboard()
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == currentCell?.txtFieldLink{
            externalSiteLink = textField.text
            return
        }
        let selectedIndexPath: IndexPath = IndexPath(row: textField.tag, section: 0)
        arrayOfTextFields[selectedIndexPath.row].textfieldText = textField.text
        paramsDict[textField.accessibilityLabel!] = textField.text
    }
    
    
    func updated(height: CGFloat,currentTextView: UITextView) {
        if currentTextView.accessibilityLabel == "Job Description"{
          //  expandingCellHeight = height + 20
            jobDesc = currentTextView.text
        }else{
         //   expandingDescHeight = height + 20
            companyDesc = currentTextView.text
        }
        // Disabling animations gives us our desired behaviour
        UIView.setAnimationsEnabled(false)
//        /* These will causes table cell heights to be recaluclated,
//         without reloading the entire cell */
         DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
            self.currentTblView!.beginUpdates()
          //  let indexPath = IndexPath(row: self.expandingIndexRow!, section: 0)
            self.currentTblView!.layoutIfNeeded()
            
            self.currentTblView!.endUpdates()
            //        // Re-enable animations
            UIView.setAnimationsEnabled(true)
         })
      
    }
    
   
    func uploadLogo(textField : UITextField){
        ImageVideoPicker.imageVideoPicker.showImagePicker(controller: parentController!, mediaType: .Image)
        ImageVideoPicker.imageVideoPicker.imagePickedBlock = { [unowned self](imageData) in
                textField.text = imageData.fileName
                
                self.logo = MediaAttatchment(file: imageData.file, fileName: imageData.fileName,fileData:UIImageJPEGRepresentation(imageData.file as! UIImage, 0.4),fileExtension:imageData.fileExtension)
           
            let selectedIndexPath: IndexPath = IndexPath(row: textField.tag, section: 0)
            self.arrayOfTextFields[selectedIndexPath.row].textfieldText = textField.text
            
        }
    }
}
