//
//  ProductsServicesViewController.swift
//  PEXit
//
//  Created by Apple on 18/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

protocol NewServiceViewControllerInput{
  func presntProductsResponse(response:[ServiceProductModel]?,message: String?)
  func presntJobsResponse(response:[JobDetailModel]?,message: String?)
  func presentSponsorResponse(response:[SponsorModel]?,message: String?)
  func presentAdsResponse(response:[AdsModel]?,message: String?)
}

protocol NewServiceViewControllerOutput{
     func fetchServicesList(typeOfService:String,pageNumber:Int?)
}

class ProductsServicesViewController: UIViewController , NavigationItemTapped , NewServiceViewControllerInput{
    
    @IBOutlet weak var tblResult: UITableView!
    @IBOutlet weak var lblTitle: UILabel!
    var controllerType : ControllerType.RawValue?
    var output : NewServiceViewControllerOutput!
    lazy var dataController: ServiceDataSourceDelegate = {
        let dataController = ServiceDataSourceDelegate()
        dataController.parentController = self
        return dataController
    }()
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(ProductsServicesViewController.handleRefresh(_:)),
                                 for: UIControlEvents.valueChanged)
        refreshControl.tintColor = UIColor.red
        
        return refreshControl
    }()
    var isMoreDataAvailable = true
    var pageNumber = 1
   
    override func viewDidLoad() {
        super.viewDidLoad()
        dataController.controllerType = controllerType
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.addNavigationRightBarButton()
        }
        tblResult.addSubview(refreshControl)
        tblResult.dataSource = dataController
        tblResult.delegate = dataController
        tblResult.tableFooterView = UIView()
        setUpUI()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        showNavigationBar()
      //  tblResult.tableFooterView?.isHidden = true
        print("Loading INDICATOR HIDDEN APPEAR")
        if self.parent is ServicesPageViewController{
            (self.parent as! ServicesPageViewController).delegate = self
        }
    }
    override func viewWillDisappear(_ animated: Bool){
        if tblResult.tableFooterView is LoadingIndicator{
            (tblResult.tableFooterView as! LoadingIndicator).stopAnimating()
        }
    }
    
   
    override func awakeFromNib() {
        NewServiceConfigurator.sharedInstance.configure(viewController: self)
    }
    
    @objc func handleRefresh(_ refreshControl : UIRefreshControl){
        getData()
        refreshControl.endRefreshing()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            self.tblResult.contentOffset = CGPoint.zero
        }
    }
    
    func getData(){
        switch ControllerType(rawValue: controllerType!)!{
        case .Product:
            dataController.productArray.removeAll()
        case .Services:
            dataController.productArray.removeAll()
        case .Software:
            dataController.productArray.removeAll()
        case .Jobs:
            dataController.jobsArray.removeAll()
        case .Ads:
            dataController.adsArray.removeAll()
        case .Sponsor:
            dataController.sponsorArray.removeAll()
        default:
            return
        }
        tblResult.reloadData()
        loadMoreData(currentPageNumber: 1)
    }
    
    @objc func btnEditClicked(_ sender :UIButton){
        switch ControllerType(rawValue: controllerType!)!{
        case .Product:
            let currentProductModel = dataController.productArray[sender.tag]
            navigateToNewProductController(currentProductModel : currentProductModel)
        case .Services:
            let currentServiceModel = dataController.productArray[sender.tag]
            navigateToNewServicesController(currentServiceModel : currentServiceModel)
        case .Software:
            let currentSoftwareModel = dataController.productArray[sender.tag]
            navigateToNewSoftwareController(currentSoftwareModel: currentSoftwareModel)
        case .Jobs:
            let currentJobModel = dataController.jobsArray[sender.tag]
            navigateToNewJobController(jobModel: currentJobModel)
        case .Ads:
          //  navigateToNewAdsController()
            let currentAdModel = dataController.adsArray[sender.tag]
            navigateToNewAdsController(adsModel: currentAdModel)
        case .Sponsor:
            let currentSponsorModel = dataController.sponsorArray[sender.tag]
            navigateToSponsoredController(sponsorModel: currentSponsorModel)
            
        default:
            return
        }
    }
    
    func navigationItemSelected(){
        switch ControllerType(rawValue: controllerType!)!{
        case .Product:
            navigateToNewProductController()
        case .Services:
            navigateToNewServicesController()
        case .Software:
           navigateToNewSoftwareController()
        case .Jobs:
            navigateToNewJobController()
        case .Ads:
            navigateToNewAdsController()
        case .Sponsor:
            navigateToSponsoredController()
        
        default:
            return
        }
    }
    
    func navigateToNewProductController(currentProductModel : ServiceProductModel? = nil){
        let newProductController = self.storyboard?.instantiateViewController(withIdentifier: "newProductViewController") as! NewProductViewController
        newProductController.currentProductModel = currentProductModel
        self.navigationController?.pushViewController(newProductController, animated: true)
    }
    
    func navigateToNewJobController(jobModel : JobDetailModel? = nil){
        let postNewJobViewController = self.storyboard?.instantiateViewController(withIdentifier: "postNewJobViewController") as! PostNewJobViewController
        if jobModel != nil{
            postNewJobViewController.currentJobModel = jobModel
        }
        self.navigationController?.pushViewController(postNewJobViewController, animated: true)
    }
    
    func navigateToSponsoredController(sponsorModel : SponsorModel? = nil){
        let sponsoredContentController = self.storyboard?.instantiateViewController(withIdentifier: "sponsoredContentController") as! SponsoredContentViewController
        sponsoredContentController.currentSponsorModel = sponsorModel
        self.navigationController?.pushViewController(sponsoredContentController, animated: true)
    }
    
    func navigateToNewSoftwareController(currentSoftwareModel : ServiceProductModel? = nil){
        let newSoftwareViewController = self.storyboard?.instantiateViewController(withIdentifier: "newSoftwareViewController") as! NewSoftwareViewController
        newSoftwareViewController.currentSoftwareModel = currentSoftwareModel
        self.navigationController?.pushViewController(newSoftwareViewController, animated: true)
        
    }

    func navigateToNewServicesController(currentServiceModel : ServiceProductModel? = nil){
        let addNewServiceViewController = self.storyboard?.instantiateViewController(withIdentifier: "addNewServiceViewController") as! AddNewServiceViewController
        addNewServiceViewController.currentServiceModel = currentServiceModel
        self.navigationController?.pushViewController(addNewServiceViewController, animated: true)
        
    }
    
    func navigateToNewAdsController(adsModel : AdsModel? = nil){
        let adsViewController = self.storyboard?.instantiateViewController(withIdentifier: "adsViewController") as! AdsViewController
        adsViewController.currentAdModel = adsModel
        self.navigationController?.pushViewController(adsViewController, animated: true)
        
    }
    
    func presntProductsResponse(response:[ServiceProductModel]?,message: String?) {
        DispatchQueue.main.async { [weak self] in
        if response != nil{
           
                
                if response?.count < 10{
                    self?.isMoreDataAvailable = false
                }else{
                    self?.isMoreDataAvailable = true
                }
                self?.dataController.productArray.append(contentsOf: response!)
                for productModel in (self?.dataController.productArray)!{
                    productModel.isExpanded = false
                    
                }
                self?.dataController.setUpTableView(pageNum: self?.pageNumber, moreData: self?.isMoreDataAvailable)
                self?.tblResult.tableFooterView?.isHidden = true
                //self?.tblResult.reloadData()
            }else{
                self?.displayToast(message ?? UNKNOWN_ERROR_MSG)
                self?.tblResult.tableFooterView?.isHidden = true
        }
    }
}
    
    func presntJobsResponse(response:[JobDetailModel]?,message: String?){

         DispatchQueue.main.async { [weak self] in
        if response != nil{
           
                if response?.count == 0{
                    self?.displayToast("No records found")
                    self?.tblResult.tableFooterView?.isHidden = true
                    return
                }
                if response?.count < 10{
                    self?.isMoreDataAvailable = false
                }else{
                    self?.isMoreDataAvailable = true
                }
                self?.dataController.jobsArray.append(contentsOf: response!)
                 self?.dataController.setUpTableView(pageNum: self?.pageNumber, moreData: self?.isMoreDataAvailable)
                self?.tblResult.tableFooterView?.isHidden = true
               // self?.tblResult.reloadData()
            }else{
            self?.displayToast(message ?? UNKNOWN_ERROR_MSG)
            self?.tblResult.tableFooterView?.isHidden = true
        }
        }
        
    }
    
    func presentSponsorResponse(response:[SponsorModel]?,message: String?){

        DispatchQueue.main.async { [weak self] in
        if response != nil{
            
                if response?.count == 0{
                    self?.displayToast("No records found")
                    self?.tblResult.tableFooterView?.isHidden = true
                    return
                }
                if response?.count < 10{
                    self?.isMoreDataAvailable = false
                }else{
                    self?.isMoreDataAvailable = true
                }
                self?.dataController.sponsorArray.append(contentsOf: response!)
                for sponsorModel in (self?.dataController.sponsorArray)!{
                    sponsorModel.isExpanded = false
                }
                self?.dataController.setUpTableView(pageNum: self?.pageNumber, moreData: self?.isMoreDataAvailable)
                self?.tblResult.tableFooterView?.isHidden = true
               
            
            
        }else{
            self?.displayToast(message ?? UNKNOWN_ERROR_MSG)
            self?.tblResult.tableFooterView?.isHidden = true

        }
        }
        
    }
    
    func presentAdsResponse(response:[AdsModel]?,message: String?){

        DispatchQueue.main.async { [weak self] in

        if response != nil{
                if response?.count == 0{
                    self?.displayToast("No records found")
                    self?.tblResult.tableFooterView?.isHidden = true
                    return
                }
                if response?.count < 10{
                    self?.isMoreDataAvailable = false
                }else{
                    self?.isMoreDataAvailable = true
                }
                self?.dataController.adsArray.append(contentsOf: response!)
                for adsModel in (self?.dataController.adsArray)!{
                    adsModel.isExpanded = false
                }
               // (self?.dataController.adsArray)!.map{$0}.isExpanded = false
                self?.dataController.setUpTableView(pageNum: self?.pageNumber, moreData: self?.isMoreDataAvailable)
                self?.tblResult.tableFooterView?.isHidden = true
               
            
            
        }else{
            self?.displayToast(message ?? UNKNOWN_ERROR_MSG)
            self?.tblResult.tableFooterView?.isHidden = true
            
        }
        }
    }
    
    func setUpUI(){
       
        switch ControllerType(rawValue: controllerType!)!{
        case .Product:
            tblResult.register(UINib(nibName: "EditServiceXib", bundle: nil),forCellReuseIdentifier: "editServiceTableViewCell")
            output.fetchServicesList(typeOfService: SERVICE_TYPE.PRODUCT.rawValue, pageNumber: 1)
            lblTitle.text = "Active/Expired Products"
        case .Services:
            tblResult.register(UINib(nibName: "EditServiceXib", bundle: nil),forCellReuseIdentifier: "editServiceTableViewCell")
            
            output.fetchServicesList(typeOfService: SERVICE_TYPE.SERVICE.rawValue, pageNumber: 1)
            lblTitle.text = "Active/Expired Services"
        case .Software:
            tblResult.register(UINib(nibName: "EditServiceXib", bundle: nil),forCellReuseIdentifier: "editServiceTableViewCell")
            tblResult.estimatedRowHeight = 200
            output.fetchServicesList(typeOfService: SERVICE_TYPE.SOFTWARE.rawValue, pageNumber: 1)
            lblTitle.text = "Active/Expired Softwares"
        case .Jobs:
            tblResult.register(UINib(nibName: "EditJobTableViewCell", bundle: nil),forCellReuseIdentifier: "editJobTableViewCell")
            tblResult.estimatedRowHeight = 200
            output.fetchServicesList(typeOfService: SERVICE_TYPE.JOB.rawValue, pageNumber: 1)
            lblTitle.text = "Active/Expired Jobs"
        case .Sponsor:
            tblResult.register(UINib(nibName: "EditServiceXib", bundle: nil),forCellReuseIdentifier: "editServiceTableViewCell")
            output.fetchServicesList(typeOfService: SERVICE_TYPE.SPONSOR.rawValue, pageNumber: 1)
            lblTitle.text = "Active/Expired Sponsored Content"
        case .Ads:
            tblResult.register(UINib(nibName: "AdsListTableViewCell", bundle: nil),forCellReuseIdentifier: "adsListTableViewCell")
            tblResult.estimatedRowHeight = 200
            output.fetchServicesList(typeOfService: SERVICE_TYPE.ADS.rawValue, pageNumber: 1)
            lblTitle.text = "Active/Expired Ad"
            
        default:
            return
        }
      
    }
    
    func loadMoreData(currentPageNumber : Int){
        pageNumber = currentPageNumber
        switch ControllerType(rawValue: controllerType!)!{
        case .Product:
           output.fetchServicesList(typeOfService: SERVICE_TYPE.PRODUCT.rawValue, pageNumber: currentPageNumber)
        case .Services:
         output.fetchServicesList(typeOfService: SERVICE_TYPE.SERVICE.rawValue, pageNumber: currentPageNumber)
        case .Software:
          output.fetchServicesList(typeOfService: SERVICE_TYPE.SOFTWARE.rawValue, pageNumber: currentPageNumber)
        case .Jobs:
           output.fetchServicesList(typeOfService: SERVICE_TYPE.JOB.rawValue, pageNumber: currentPageNumber)
        case .Sponsor:
            output.fetchServicesList(typeOfService: SERVICE_TYPE.SPONSOR.rawValue, pageNumber: currentPageNumber)
        case .Ads:
            output.fetchServicesList(typeOfService: SERVICE_TYPE.ADS.rawValue, pageNumber: currentPageNumber)
        default:
            return
        }
    }
   
    @IBAction func unwindToProductsServicesList(segue:UIStoryboardSegue) {
        STOP_LOADING_VIEW()
        getData()
        
    }
}
