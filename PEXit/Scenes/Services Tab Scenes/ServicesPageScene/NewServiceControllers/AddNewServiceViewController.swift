//
//  AddNewServiceViewController.swift
//  PEXit
//
//  Created by ats on 22/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

class AddNewServiceViewController: UIViewController {
    @IBOutlet weak var tblNewServices: UITableView!
    var currentTextField : UITextField?
    var countryArray = [String]()
    var stateArray = [String]()
    var cityArray = [String]()
    var currentServiceModel : ServiceProductModel?
    var txtFieldArray = [TJTextField]()
    let newServiceModel = NewServiceRequestModel()
    var currentDesc = ""
    lazy var dataController: NewServiceDataSourceDelegate = {
        let dataController = NewServiceDataSourceDelegate()
        dataController.parentController = self
        dataController.generateTextFields()
        return dataController
    }()
    lazy var attachmentController: AttatchmentViewController = {
        var viewController = storyboard?.instantiateViewController(withIdentifier: "attachmentController") as! AttatchmentViewController
        viewController.serviceType = "Service"
        //viewController.delegate = self
        // self.add(asChildViewController: viewController)
        return viewController
    }()
    var cellsArray = [PostNewJobTableViewCell]()
    override func viewDidLoad() {
        super.viewDidLoad()
//        print("Test")
        addBackButton(withTitle: "Post Service")
        tblNewServices.register(UINib(nibName: "PostNewJobTableViewCell", bundle: nil), forCellReuseIdentifier: "postNewJobTableViewCell")
        tblNewServices.dataSource = dataController
        tblNewServices.delegate = dataController
        if currentServiceModel != nil{
            setEditData()
        }
        // Do any additional setup after loading the view.
    }
    
    func setEditData(){
        dataController.paramsDict["Company Name"] = currentServiceModel?.company
        //   dataController.paramsDict["Upload Logo"] = currentServiceModel?.logo
        dataController.paramsDict["Industry"] = currentServiceModel?.industry
        //    dataController.paramsDict["Location"] = currentServiceModel?.states
        if let logo = currentServiceModel?.logo{
            if let pathUrl = URL(string:logo){
                dataController.paramsDict["Upload Logo"] = pathUrl.lastPathComponent
            }
        }
        dataController.paramsDict["Country"] = currentServiceModel?.country
        dataController.paramsDict["State"] = currentServiceModel?.states
        dataController.paramsDict["City"] = currentServiceModel?.city
        dataController.paramsDict["Postal Code"] = currentServiceModel?.pincode
        
        dataController.paramsDict["Email Address"] = currentServiceModel?.email
        dataController.paramsDict["Description"] = currentServiceModel?.descrip
        dataController.paramsDict["Category"] = currentServiceModel?.category
        self.newServiceModel.country = currentServiceModel?.country
        self.newServiceModel.state = currentServiceModel?.states
        self.newServiceModel.city = currentServiceModel?.city
        let mediaModel = PostRLMModel()
        mediaModel.link = currentServiceModel?.links
        mediaModel.video = currentServiceModel?.utube
        mediaModel.allvid = (currentServiceModel?.videos)!
        mediaModel.docu = (currentServiceModel?.files)!
        mediaModel.image = (currentServiceModel?.photos)!
        mediaModel.ppt = (currentServiceModel?.presentation)!
        mediaModel.id = (currentServiceModel?.id)!
        self.attachmentController.currentProductModel = currentServiceModel
        self.attachmentController.isFromEdit = true
        self.attachmentController.currentPostModel = mediaModel
        
        
        //  mediaModel.docu = currentServiceModel.fil
        dataController.generateTextFields()
    }
    
    func addSubView(currentView:UIView) {
        // Add Child View Controller
        addChildViewController(attachmentController)
        // Add Child View as Subview
        currentView.addSubview(attachmentController.view)
        attachmentController.btnPost.isHidden = true
        // Configure Child View
        attachmentController.view.frame = currentView.bounds
        attachmentController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        // Notify Child View Controller
        attachmentController.didMove(toParentViewController: self)
    }
    
    func getAllRequiredFields() -> [TJTextField]{
        var textFieldsArray = [TJTextField]()
        for txtFieldDetails in dataController.arrayOfTextFields{
            let txtField = TJTextField()
            txtField.placeholder = txtFieldDetails.placeholderText
            txtField.text = txtFieldDetails.textfieldText
            if currentServiceModel != nil{
                if txtFieldDetails.placeholderText == "Upload Logo"{
                    continue
                }
            }
            textFieldsArray.append(txtField)
        }
        return textFieldsArray
    }
    
    
    @objc func uploadService(){
        if Utils.validateFormAndShowToastWithRequiredFields(getAllRequiredFields()){
            if !currentDesc.isEmpty{
                newServiceModel.images = attachmentController.imageNamesArray
                newServiceModel.videos = attachmentController.videoArray
                newServiceModel.logo = dataController.productLogo
                newServiceModel.ppt = attachmentController.pptArray
                newServiceModel.file = attachmentController.fileArray
                newServiceModel.company = dataController.paramsDict["Company Name"]!
                newServiceModel.industry = dataController.paramsDict["Industry"]
                newServiceModel.postalCode = dataController.paramsDict["Postal Code"]
                newServiceModel.email = dataController.paramsDict["Email Address"]
                newServiceModel.description = dataController.paramsDict["Description"]
//                let youTubeString = attachmentController.youTubeLinksArray.map{$0.fileName!}.joined(separator: ",")
//                newServiceModel.youTubeLink = youTubeString
//                let linksString = attachmentController.linksArray.map{$0.fileName!}.joined(separator: ",")
//                newServiceModel.link = linksString
                
                var linkString = attachmentController.linksArray.map{$0.fileName!}.joined(separator: ",")
                
                
                if !linkString.isEmpty{
                    linkString = [linkString,currentServiceModel?.links].compactMap{$0}.joined(separator: ",")
                }else{
                    linkString = currentServiceModel?.links ?? ""
                }
                
                var youTubeString = attachmentController.youTubeLinksArray.map{$0.fileName!}.joined(separator: ",")
                if !youTubeString.isEmpty{
                    youTubeString = [youTubeString,currentServiceModel?.utube].compactMap{$0}.joined(separator: ",")
                }else{
                    youTubeString = currentServiceModel?.utube ?? ""
                }
                
                print("LINKSSSS:->",youTubeString)
                newServiceModel.link = linkString
                newServiceModel.youTubeLink = youTubeString
                
                
                
                
                
                
                if (currentServiceModel?.stype == "free" || currentServiceModel?.stype == "paid" && currentServiceModel?.renew == 0){
                    editServiceDetails(requestModel: newServiceModel)
                }else{
                    navigateToCheckOutController(requestModel: newServiceModel)
                }
            }
            else{
                displayToast("Please Enter Description")
                
            }
        }
        
    }
    
    func navigateToCheckOutController(requestModel : NewServiceRequestModel){
        let checkoutController = storyboard?.instantiateViewController(withIdentifier: "checkoutViewController") as! CheckoutViewController
        if currentServiceModel != nil{
            checkoutController.isFromEdit = true
            checkoutController.idToEdit = currentServiceModel?.id
        }
        checkoutController.serviceRequestModel = requestModel
        checkoutController.serviceType = SERVICE_TYPE_API.SERVICES.rawValue
        navigationController?.pushViewController(checkoutController, animated: true)
    }
    
    func editServiceDetails(requestModel : NewServiceRequestModel){
        let apiName = UPDATE_SERVICE(serviceID: (currentServiceModel?.id)!)
        let worker = CheckoutWorker()
        requestModel.stype = currentServiceModel?.stype
        worker.postNewService(apiName:apiName,requestObject: requestModel) { (response, error) in
            if response != nil {
                self.displayToast((response?.message) ?? "Please try again")
                 self.performSegue(withIdentifier: "navigateToNewService", sender: self)
                STOP_LOADING_VIEW()
              //  self.navigationController?.popToRootViewController(animated: true)
               
            }else{
                self.displayToast(error?.localizedDescription ?? UNKNOWN_ERROR_MSG)
            }
        }
        
    }
    
    
    func fetchCountries(textField : UITextField){
        let worker = RegistrationWorker()
        worker.fetchCountries { [weak self](countryResponse) in
            if countryResponse.datas != nil{
                self?.countryArray.removeAll()
                for country in countryResponse.datas!{
                    self?.countryArray.append(country.name!)
                }
                DispatchQueue.main.async {
                    self?.showCountryPicker(textField: textField,title:"Country",currentArray: (self?.countryArray)!)
                }
            }
        }
    }
    
    
    func fetchStates(textField : UITextField){
        let worker = RegistrationWorker()
        
        for (_, element) in (dataController.arrayOfTextFields.enumerated()) {
            if element.placeholderText == "Country"{
                if let countryName = element.textfieldText ,!countryName.isEmpty{
                    worker.fetchStates(countryName: countryName) {[weak self] (stateResponse) in
                        if stateResponse.datas != nil{
                            self?.stateArray.removeAll()
                            for state in stateResponse.datas!{
                                self?.stateArray.append(state.name!)
                            }
                            DispatchQueue.main.async {
                                self?.showCountryPicker(textField: textField,title:"State",currentArray:(self?.stateArray)!)
                            }
                        }
                        
                    }
                }else{
                    self.displayToast(CHOOSE_COUNTRY_TOAST)
                }
                
            }
        }
    }
    
    func fetchCities(textField : UITextField){
        let worker = RegistrationWorker()
        for (_, element) in (dataController.arrayOfTextFields.enumerated()) {
            if element.placeholderText == "State"{
                
                if let stateName = element.textfieldText , !stateName.isEmpty{
                    worker.fetchCities(stateName: stateName) {[weak self] (cityResponse) in
                        
                        if cityResponse.datas != nil{
                            self?.cityArray.removeAll()
                            for city in cityResponse.datas!{
                                self?.cityArray.append(city.name!)
                            }
                            DispatchQueue.main.async {
                                self?.showCountryPicker(textField: textField,title:"City",currentArray:(self?.cityArray)!)
                            }
                        }
                        
                    }
                }
                else{
                    self.displayToast(CHOOSE_STATE_TOAST)
                }
                
            }
        }
    }
    
    func showCountryPicker(textField : UITextField , title : String , currentArray : [String]){
        ActionSheetStringPicker.show(withTitle: title, rows: currentArray, initialSelection: 0, doneBlock: {[unowned self] (key, index, value) in
            textField.text  = value as? String
            let selectedIndexPath: IndexPath = IndexPath(row: textField.tag, section: 0)
            self.dataController.arrayOfTextFields[selectedIndexPath.row].textfieldText = textField.text
            switch title{
            case "Country":
                self.newServiceModel.country = value as? String
                for (index, element) in (self.dataController.arrayOfTextFields.enumerated()) {
                    if element.placeholderText == "State" || element.placeholderText == "City"{
                        
                        self.dataController.arrayOfTextFields[index].textfieldText = ""
                    }
                    self.tblNewServices.reloadData()
                    
                }
                
            case "State":
                self.newServiceModel.state = value as? String
                for (index, element) in (self.dataController.arrayOfTextFields.enumerated()) {
                    if element.placeholderText == "City"{
                        self.dataController.arrayOfTextFields[index].textfieldText = ""
                    }
                }
                self.tblNewServices.reloadData()
            case "City":
                self.newServiceModel.city = value as? String
            default:
                print("")
            }
            
            }, cancel: { (picker) in
                
        }, origin: self.view)
    }
    
}
