//
//  NewServiceDataSourceDelegate.swift
//  PEXit
//
//  Created by ats on 22/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation
import UIKit
let INFINITE_TAG = 99999

class NewServiceDataSourceDelegate: NSObject, UITableViewDataSource , UITableViewDelegate{
    
    var arrayOfTextFields = [TextfieldInfoStruct]()
    var parentController : AddNewServiceViewController?
    var currentTblView : UITableView?
    var expandingCellHeight: CGFloat = 50
    var expandingIndexRow : Int?
    var updatedHeight: CGFloat?
    var paramsDict = [String : String]()
   
    var productLogo : MediaAttatchment?
    
    func generateTextFields(){
        currentTblView = parentController?.tblNewServices
        arrayOfTextFields = [TextfieldInfoStruct(rightImage: UIImage(named: UPLOAD_LOGO_IMAGE),placeholderText: "Upload Logo",textfieldText:paramsDict["Upload Logo"]),
                             TextfieldInfoStruct(placeholderText: "Company Name" ,textfieldText:paramsDict["Company Name"]),
                             TextfieldInfoStruct(placeholderText: "Industry",textfieldText:paramsDict["Industry"]),
                             TextfieldInfoStruct(rightImage: UIImage(named: "drop"), placeholderText: "Country",textfieldText:paramsDict["Country"]),
                             TextfieldInfoStruct(rightImage: UIImage(named: "drop"), placeholderText: "State",textfieldText:paramsDict["State"]),
                             TextfieldInfoStruct(rightImage: UIImage(named: "drop"), placeholderText: "City",textfieldText:paramsDict["City"]),
                             TextfieldInfoStruct(placeholderText: "Postal Code",textfieldText:paramsDict["Postal Code"]) ,
                             TextfieldInfoStruct(placeholderText: "Email Address",textfieldText:paramsDict["Email Address"]),]
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayOfTextFields.count + 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let postJobCell = tableView.dequeueReusableCell(withIdentifier: "postNewJobTableViewCell", for: indexPath) as! PostNewJobTableViewCell
        
        postJobCell.hideViews()
        if indexPath.row < arrayOfTextFields.count{
            postJobCell.txtFieldInput.delegate = self
            postJobCell.txtFieldInput.tag = indexPath.row 
            postJobCell.txtFieldInput.isHidden = false
            postJobCell.viewTxtView.isHidden = true
            postJobCell.btnSubmit.isHidden = true
            postJobCell.viewAttachment.isHidden = true
            postJobCell.configureTextfield(arrayOfTextFields[indexPath.row])
        }
        else if indexPath.row == arrayOfTextFields.count{
            postJobCell.viewTxtView.isHidden = false
            postJobCell.txtFieldInput.isHidden = true
            postJobCell.btnSubmit.isHidden = true
            postJobCell.viewAttachment.isHidden = true
            postJobCell.txtFieldInput.tag = INFINITE_TAG
            postJobCell.delegate = self
            postJobCell.configureTxtView(placeholderStr: "Description",currentText:paramsDict["Description"])
        }
        else if indexPath.row == (arrayOfTextFields.count) + 1{
            postJobCell.viewAttachment.isHidden = false
            postJobCell.viewTxtView.isHidden = true
            postJobCell.txtFieldInput.isHidden = true
            postJobCell.btnSubmit.isHidden = true
            postJobCell.txtFieldInput.tag = INFINITE_TAG
            parentController?.attachmentController.delegate = self
            parentController?.addSubView(currentView: postJobCell.viewAttachment)
        }
        else if indexPath.row == (arrayOfTextFields.count) + 2{
            postJobCell.btnSubmit.isHidden = false
            if parentController?.currentServiceModel != nil{
                postJobCell.btnSubmit.setTitle("Save", for: .normal)
            }
            postJobCell.viewAttachment.isHidden = true
            postJobCell.viewTxtView.isHidden = true
            postJobCell.txtFieldInput.isHidden = true
            postJobCell.txtFieldInput.tag = INFINITE_TAG
            postJobCell.btnSubmit.addTarget(parentController, action: #selector(AddNewServiceViewController.uploadService), for: .touchUpInside)
        }
        parentController?.cellsArray.append(postJobCell)
        return postJobCell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row{
        case indexPath.row where indexPath.row < arrayOfTextFields.count:
            return 65
        case indexPath.row where indexPath.row == arrayOfTextFields.count:
           // return expandingCellHeight + 25
            return UITableViewAutomaticDimension
        case indexPath.row where indexPath.row == arrayOfTextFields.count + 1:
            return updatedHeight != nil ? updatedHeight! : DEFAULT_HEIGHT_ATTACHMENT_VIEW
        case indexPath.row where indexPath.row == arrayOfTextFields.count + 2:
           return 60
        default:
            return 60
            
        }
    }
}

extension NewServiceDataSourceDelegate : PassAttatchmentsToController , ExpandingCellDelegate , UITextFieldDelegate , UITextViewDelegate{
   
    func updateHeightOfParentController(height: CGFloat) {
         //Taking a constant
         updatedHeight = DEFAULT_HEIGHT_ATTACHMENT_VIEW + height
         DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
              self.currentTblView!.beginUpdates()
              self.currentTblView!.layoutIfNeeded()
              self.currentTblView!.endUpdates()
         })
    }
    
    //MARK: UITextFieldDelegate
    func textFieldDidEndEditing(_ textField: UITextField) {
        let selectedIndexPath: IndexPath = IndexPath(row: textField.tag, section: 0)
        arrayOfTextFields[selectedIndexPath.row].textfieldText = textField.text
        paramsDict[textField.accessibilityLabel!] = textField.text
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField.accessibilityLabel == "Country"{
            parentController?.fetchCountries(textField: textField)
            return false
        }
        else if textField.accessibilityLabel == "State"{
            parentController?.fetchStates(textField: textField)
            return false
        }
        else if textField.accessibilityLabel == "City"{
            parentController?.fetchCities(textField: textField)
            return false
        }
        else if textField.accessibilityLabel == "Upload Logo"{
            uploadLogo(textField: textField)
            return false
        }
        return true
    }
    
    func uploadLogo(textField : UITextField){
        
        ImageVideoPicker.imageVideoPicker.showImagePicker(controller: parentController!, mediaType: .Image)
        ImageVideoPicker.imageVideoPicker.imagePickedBlock = { [unowned self](imageData) in
            //self.productLogo = imageData
             self.productLogo = MediaAttatchment(file: imageData.file, fileName: imageData.fileName,fileData:UIImageJPEGRepresentation(imageData.file as! UIImage, 0.4),fileExtension:imageData.fileExtension)
            textField.text = imageData.fileName
            let selectedIndexPath: IndexPath = IndexPath(row: textField.tag, section: 0)
            self.arrayOfTextFields[selectedIndexPath.row].textfieldText = textField.text
        }
    }
    
    
    
    func updated(height: CGFloat,currentTextView:  UITextView) {
       
       paramsDict["Description"] = currentTextView.text
      // expandingCellHeight = height + 20
       parentController?.currentDesc = currentTextView.text
       UIView.setAnimationsEnabled(false)
        //        /* These will causes table cell heights to be recaluclated,
        //         without reloading the entire cell */
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
            self.currentTblView!.beginUpdates()
       
            self.currentTblView!.endUpdates()
            //        // Re-enable animations
            UIView.setAnimationsEnabled(true)
            
        })
        
    }
    
    
}
