//
//  SponsoredContentViewController.swift
//  PEXit
//
//  Created by Apple on 18/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import Material

class SponsoredContentViewController: UIViewController , UITextViewDelegate , PassAttatchmentsToController{
    @IBOutlet weak var txtFieldTitle: TextField!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var viewMedia: UIView!
    @IBOutlet weak var txtViewDescription: UITextView!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    var currentSponsorModel : SponsorModel?
    
    private lazy var attachmentController: AttatchmentViewController = {
        var viewController = storyboard?.instantiateViewController(withIdentifier: "attachmentController") as! AttatchmentViewController
       viewController.isFromEdit = false
       viewController.serviceType = "Sponsor"
       viewController.delegate = self
       return viewController
    }()
  
    override func viewDidLoad() {
        super.viewDidLoad()
        addBackButton(withTitle: "Post Sponsored Content")
        txtViewDescription.delegate = self
       // txtViewDescription.placeholder = "Description"
        if currentSponsorModel != nil{
            setDataForEdit()
            btnSubmit.setTitle("Save", for: .normal)
            
        }
        self.addChildViewController(attachmentController)
        addSubView(asChildViewController: attachmentController)
        
        
        // Do any additional setup after loading the view.
    }

    func setDataForEdit(){
        txtFieldTitle.text = currentSponsorModel?.title
        txtViewDescription.text = currentSponsorModel?.post?.htmlDecodedText()
        txtViewDescription.updateTextViewPlacholder()
        let mediaModel = PostRLMModel()
        mediaModel.link = currentSponsorModel?.link
        mediaModel.video = currentSponsorModel?.video
        mediaModel.allvid = (currentSponsorModel?.allvid)!
        mediaModel.docu = (currentSponsorModel?.docu)!
        mediaModel.image = (currentSponsorModel?.image)!
        mediaModel.ppt = (currentSponsorModel?.ppt)!
        mediaModel.id = (currentSponsorModel?.id)!
        attachmentController.isFromEdit = true
        self.attachmentController.currentSponsorModel = currentSponsorModel
        self.attachmentController.currentPostModel = mediaModel
       
    }
    
    func addSubView(asChildViewController viewController: UIViewController) {
        // Add Child View Controller
        addChildViewController(viewController)
        
        // Add Child View as Subview
        viewMedia.addSubview(viewController.view)
        if viewController is AttatchmentViewController{
            (viewController as! AttatchmentViewController).btnPost.isHidden = true
        }
        // Configure Child View
        viewController.view.frame = viewMedia.bounds
        viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        // Notify Child View Controller
        viewController.didMove(toParentViewController: self)
    }
    
    @IBAction func btnAddClicked(_ sender: Any) {
        if !((txtFieldTitle.text?.isEmpty)!){
            if !txtViewDescription.text.isEmpty{
                let sponsoredContentModel = SponsoredContentRequestModel()
                sponsoredContentModel.title = txtFieldTitle.text
                sponsoredContentModel.description = txtViewDescription.text
                sponsoredContentModel.files = attachmentController.fileArray
                sponsoredContentModel.images = attachmentController.imageNamesArray
                sponsoredContentModel.ppt = attachmentController.pptArray
                sponsoredContentModel.videos = attachmentController.videoArray
                var linkString = attachmentController.linksArray.map{$0.fileName!}.joined(separator: ",")
                
                
                if !linkString.isEmpty{
                    linkString = [linkString,currentSponsorModel?.link].compactMap{$0}.joined(separator: ",")
                }else{
                    linkString = currentSponsorModel?.link ?? ""
                }
               
                var youTubeString = attachmentController.youTubeLinksArray.map{$0.fileName!}.joined(separator: ",")
                if !youTubeString.isEmpty{
                    youTubeString = [youTubeString,currentSponsorModel?.video].compactMap{$0}.joined(separator: ",")
                }else{
                    youTubeString = currentSponsorModel?.video ?? ""
                }
               
                print("LINKSSSS:->",youTubeString)
                sponsoredContentModel.links = linkString
                sponsoredContentModel.youTubeLink = youTubeString
                if (currentSponsorModel?.stype == "free" || currentSponsorModel?.stype == "paid" && currentSponsorModel?.renew == 0){
//                    sponsoredContentModel.stype = currentSponsorModel?.stype

                    editSponsoredContentDetails(requestModel: sponsoredContentModel)
                }else{
                    navigateToCheckOutController(requestModel: sponsoredContentModel)
                }
                
            }else{
                displayToast("Please enter Description")
            }
        }else{
            displayToast("Please enter Title")

        }
    }
    
    func navigateToCheckOutController(requestModel : SponsoredContentRequestModel){
        let checkoutController = storyboard?.instantiateViewController(withIdentifier: "checkoutViewController") as! CheckoutViewController
        if currentSponsorModel != nil{
            checkoutController.isFromEdit = true
            checkoutController.idToEdit = currentSponsorModel?.id
        }
        checkoutController.sponsoredContentRequestModel = requestModel
        checkoutController.serviceType = SERVICE_TYPE_API.SPONSORS.rawValue
        navigationController?.pushViewController(checkoutController, animated: true)
    }
    
    func editSponsoredContentDetails(requestModel : SponsoredContentRequestModel){
       let apiName = UPDATE_SPONSOR_CONTENT(sponsorID: (currentSponsorModel?.id!)!)
       let worker = CheckoutWorker()
       worker.postSponsorContent(apiName: apiName, requestObject: requestModel) { (response, error) in
        if response != nil {
            self.displayToast((response?.message) ?? "Please try again")
            //self.navigationController?.popToRootViewController(animated: true)
            self.performSegue(withIdentifier: "navigateToSponsorList", sender: self)
            STOP_LOADING_VIEW()
        }else{
            self.displayToast(error?.localizedDescription ?? UNKNOWN_ERROR_MSG)
        }
        }
        
    }
    
    // TextView Delegate
    
    func textViewDidChange(_ textView: UITextView) {
        textView.updateTextViewPlacholder()
    }
    
    func updateHeightOfParentController(height: CGFloat) {
        //Taking a constant
        heightConstraint.constant = DEFAULT_HEIGHT_ATTACHMENT_VIEW + height
        self.view.layoutIfNeeded()
    }
    

}
