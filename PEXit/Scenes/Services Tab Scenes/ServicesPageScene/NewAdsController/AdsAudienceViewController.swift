//
//  AdsAudienceViewController.swift
//  PEXit
//
//  Created by ats on 29/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class AdsAudienceViewController: UIViewController , UITextFieldDelegate {
    @IBOutlet weak var txtFieldGender: TJTextField!
    @IBOutlet weak var txtFieldAgeGroup: TJTextField!
    @IBOutlet weak var btnCountry: UIButton!
    @IBOutlet weak var btnState: UIButton!
    @IBOutlet weak var btnCompany: UIButton!
    @IBOutlet weak var collectionViewCountry: UICollectionView!
    @IBOutlet weak var collectionViewCompany: UICollectionView!
    @IBOutlet weak var collectionViewStates: UICollectionView!
    @IBOutlet weak var constraintCountryViewHeight: NSLayoutConstraint!
    @IBOutlet weak var constraintStateViewHeight: NSLayoutConstraint!
    @IBOutlet weak var constraintCompanyViewHeight: NSLayoutConstraint!
    
    var currentTextField : UITextField?
    var currentSender : UIButton?
    var countryArray = [String]()
    var statesArray = [String]()
    var companyArray = [String]()
    var delegate : AdsDetailDelegate?
    var selectedCountryArray = [String]()
    var selectedStatesArray = [String]()
    var selectedCompanyArray = [String]()
    var adsRequestModel : NewAdsRequestModel?
    var countryModelArray = [CountryResponse.Fetchcounties.CountryViewModel]()
    var stateModelArray = [CountryResponse.Fetchcounties.CountryViewModel]()
    var selectedCountryIds : String?
    var previousCountrySelected = [String]()
    var selectedStateIds : String?
    var selectedCompany : String?
    var currentAdModel : AdsModel?
    
    lazy var dataController: SearchResultController = {
        let dataController = SearchResultController()
        dataController.isFromAds = true
        dataController.parentController = self
        return dataController
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        collectionViewStates.register(UINib(nibName: "JobSearchCell", bundle: nil), forCellWithReuseIdentifier: "JobSearchCell")
        collectionViewCompany.register(UINib(nibName: "JobSearchCell", bundle: nil), forCellWithReuseIdentifier: "JobSearchCell")
        collectionViewCountry.register(UINib(nibName: "JobSearchCell", bundle: nil), forCellWithReuseIdentifier: "JobSearchCell")
        collectionViewStates.dataSource = dataController
        collectionViewStates.delegate = dataController
        
        collectionViewCompany.dataSource = dataController
        collectionViewCompany.delegate = dataController
        
        collectionViewCountry.dataSource = dataController
        collectionViewCountry.delegate = dataController
        setUpDataForEdit()
    }
  
   
    override func viewWillAppear(_ animated: Bool) {
        
        if adsRequestModel == nil && currentAdModel != nil{
            adsRequestModel = NewAdsRequestModel()
            adsRequestModel?.adLayout = "Layout_\((currentAdModel?.layout)!)"
            adsRequestModel?.adTitle = currentAdModel?.title
            adsRequestModel?.adLink = currentAdModel?.url
            adsRequestModel?.adDescription = currentAdModel?.post
            adsRequestModel?.adYouTubeLink = currentAdModel?.utub
            //adsRequestModel.adYouTubeLink = currentAdModel?.utub
            adsRequestModel?.adAge = currentAdModel?.age
            adsRequestModel?.adGender = currentAdModel?.gender
        }
        else if adsRequestModel == nil{
            adsRequestModel = NewAdsRequestModel()
        }
        else if adsRequestModel != nil && currentAdModel != nil{
            print("Came here")
            adsRequestModel?.adAge = currentAdModel?.age
            adsRequestModel?.adGender = currentAdModel?.gender
           // adsRequestModel = adsRequestModel
//            adsRequestModel?.adLayout = "Layout_\((currentAdModel?.layout)!)"
//            adsRequestModel?.adTitle = currentAdModel?.title
//            adsRequestModel?.adLink = currentAdModel?.url
//            adsRequestModel?.adDescription = currentAdModel?.post
//            adsRequestModel?.adYouTubeLink = currentAdModel?.utub
//            //adsRequestModel.adYouTubeLink = currentAdModel?.utub
//            adsRequestModel?.adAge = currentAdModel?.age
//            adsRequestModel?.adGender = currentAdModel?.gender
        }
        
    }
    
    func setUpDataForEdit(){
        if currentAdModel != nil{
            txtFieldGender.text = currentAdModel?.gender
            txtFieldAgeGroup.text = currentAdModel?.age
            self.selectedCompanyArray = (currentAdModel?.compny?.components(separatedBy: ","))!
           // dataController.searchArray = selectedCompanyArray.removeDuplicates()
            selectedCompany = dataController.searchArray.joined(separator: ",")
         //   adsRequestModel?.adCompany = selectedCompany
          //  constraintCompanyViewHeight.constant = 40
//            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
//                self.collectionViewCompany.reloadData()
//            }
            dataController.setUpSearchArray(self.collectionViewCompany)
            
           // self.selectedCountryArray = ["India","Afghanistan"]
          //  dataController.searchArray = selectedCountryArray.removeDuplicates()
            //self.selectedC = dataController.searchArray.joined(separator: ",")
            adsRequestModel?.adCountry = selectedCompany
           // constraintCountryViewHeight.constant = 40
           
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                 self.dataController.setUpSearchArray(self.collectionViewCountry)
            }
        }
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        switch textField.accessibilityLabel{
        case "Age Group":
            navigateToPopUpController(sender: textField, optionsArray: ["All","18+","25+","35+","50+"])
            return false
        case "Gender":
            navigateToPopUpController(sender: textField, optionsArray: ["All","Male","Female","Other"])
            return false
        default:
            break
        }
        return true
    }
    
    @IBAction func chooseCountryClicked(_ sender: UIButton) {
        currentSender = btnCountry
        if countryArray.count <= 0{
            let worker = RegistrationWorker()
            worker.fetchCountries { [weak self](countryResponse) in
                if countryResponse.datas != nil{
                    self?.countryArray.removeAll()
                    self?.countryModelArray = countryResponse.datas!
                    for country in countryResponse.datas!{
                        self?.countryArray.append(country.name!)
                    }
                    DispatchQueue.main.async {
                        self?.navigateToPopUpController(sender: sender, optionsArray: self?.countryArray)
                    }
                }
            }
        }else{
            self.navigateToPopUpController(sender: sender, optionsArray: self.countryArray)
        }
    }
    
    
    @IBAction func chooseStateClicked(_ sender: UIButton) {
        if selectedCountryIds != nil && !((selectedCountryIds?.isEmpty)!){
            currentSender = btnState
            let worker = RegistrationWorker()
            worker.fetchAdsStates(countries: selectedCountryIds) {[weak self] (statesResponse) in
                self?.statesArray.removeAll()
                self?.stateModelArray = statesResponse.datas!
                for state in statesResponse.datas!{
                    self?.statesArray.append(state.name!)
                }
                DispatchQueue.main.async {
                    self?.navigateToPopUpController(sender: sender, optionsArray: self?.statesArray)
                    
                }
            }
        }else{
            displayToast("Please choose a country")
        }
   }
    
    @IBAction func chooseCompanyClicked(_ sender: UIButton) {
        currentSender = btnCompany
        if companyArray.count <= 0{
            let worker = NewAdWorker()
            worker.fetchCompanies {[weak self] (response) in
                self?.companyArray.removeAll()
                for company in response.datas!{
                    self?.companyArray.append(company.companyname!)
                }
                DispatchQueue.main.async {
                    self?.navigateToPopUpController(sender: sender, optionsArray: self?.companyArray)
                }
            }
        }else{
            self.navigateToPopUpController(sender: sender, optionsArray: self.companyArray)
        }
    }

    @IBAction func btnSaveClicked(_ sender: Any) {
        if let delegate = delegate{
            delegate.passAdDetails(adRequestModel: adsRequestModel!)
        }
    }
}

extension AdsAudienceViewController : UIPopoverPresentationControllerDelegate , ItemSelected{
   
    func navigateToPopUpController(sender:AnyObject?,optionsArray:[String]?){
         let popUpController = storyboard?.instantiateViewController(withIdentifier: "commonViewController") as! CommonViewController
        if sender is UITextField{
            currentTextField = sender as? UITextField
            popUpController.preferredContentSize = CGSize(width : (sender as! UITextField).frame.size.width , height : CGFloat((optionsArray?.count)! * COMMON_CONTROLLER_ROW_HEIGHT))
        }
        if sender is UIButton{
            popUpController.isMultiSelectionAllowed = true
            popUpController.preferredContentSize = CGSize(width : ((sender as! UIButton).frame.size.width) , height : CGFloat((optionsArray?.count)! * COMMON_CONTROLLER_ROW_HEIGHT))
        }
        switch currentSender {
        case btnCountry:
            popUpController.selectedItemsArray = selectedCountryArray
        case btnState:
            popUpController.selectedItemsArray = selectedStatesArray
        case btnCompany:
            popUpController.selectedItemsArray = selectedCompanyArray
        default:
            break
        }
        popUpController.modalPresentationStyle = .popover
        
        
        let popOver = popUpController.popoverPresentationController
        popOver?.delegate = self
        popOver?.permittedArrowDirections = .up
        popOver?.sourceView = sender as? UIView
        popOver?.sourceRect = (sender?.bounds)!
        popUpController.delegate = self
        popUpController.itemsArray = optionsArray
        present(popUpController, animated: true, completion: nil)
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return .none
    }
   
    func optionSelected(option: String?) {
        currentTextField?.text = option ?? ""
        switch currentTextField{
        case txtFieldAgeGroup:
            adsRequestModel?.adAge = option
        case txtFieldGender:
            adsRequestModel?.adGender = option
        default:
            break
        }
    }
   
    func arrayOfSelectedOptions(option: [String]?) {
        switch currentSender{
        case btnCountry:
            selectedCountryArray = option!
            if option != nil && option?.count > 0{
                constraintCountryViewHeight.constant = 40
            }
            selectedCountryArray = selectedCountryArray.removeDuplicates()
            selectedCountryIds =  countryModelArray.filter{(selectedCountryArray.contains($0.name!))}.map{$0.id}.map{String($0!)}.joined(separator: ",")
            adsRequestModel?.adCountry = selectedCountryIds
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.dataController.setUpSearchArray(self.collectionViewCountry)
//                    self.collectionViewCountry.reloadData()
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                        self.selectedStatesArray.removeAll()
                        self.dataController.setUpSearchArray(self.collectionViewStates)
//                        self.dataController.searchArray = self.selectedStatesArray
//                        self.collectionViewStates.reloadData()
                }
            }
        case btnState:
            selectedStatesArray = option!
            if option != nil && option?.count > 0{
                constraintStateViewHeight.constant = 40
            }
            selectedCountryArray = selectedStatesArray.removeDuplicates()
            selectedStateIds =  stateModelArray.filter{(dataController.stateArray.contains($0.name!))}.map{$0.key}.map{String($0!)}.joined(separator: ",")
            adsRequestModel?.adState = selectedStateIds
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.dataController.setUpSearchArray(self.collectionViewStates)
            }
        case btnCompany:
            selectedCompanyArray = option!
            if option != nil && option?.count > 0{
                constraintCompanyViewHeight.constant = 40
            }
            selectedCompanyArray = selectedCompanyArray.removeDuplicates()
            selectedCompany = dataController.companyArray.joined(separator: ",")
            adsRequestModel?.adCompany = selectedCompany
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
               // self.collectionViewCompany.reloadData()
                 self.dataController.setUpSearchArray(self.collectionViewCompany)
            }
        default:
            break
        }
    }
}

