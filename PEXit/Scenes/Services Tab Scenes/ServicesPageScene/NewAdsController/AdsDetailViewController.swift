//
//  AdsDetailViewController.swift
//  PEXit
//
//  Created by ats on 29/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

protocol AdsDetailDelegate{
    func passAdDetails(adRequestModel : NewAdsRequestModel)
}

class AdsDetailViewController: UIViewController {
    lazy var dataController: NewAdDataSourceDelegate = {
        let dataController = NewAdDataSourceDelegate()
        dataController.parentController = self
        return dataController
    }()
   
    lazy var attachmentController: AttatchmentViewController = {
        var viewController = storyboard?.instantiateViewController(withIdentifier: "attachmentController") as! AttatchmentViewController
        viewController.isFromAds = true
        viewController.serviceType = "Ads"
        //viewController.delegate = self
        // self.add(asChildViewController: viewController)
        return viewController
    }()
    @IBOutlet weak var tblAdDetails: UITableView!
    @IBOutlet weak var lblChooseLayout: UILabel!
    var layoutType : String?
    var previousLayout : String?
    let adRequestModel = NewAdsRequestModel()
    var delegate : AdsDetailDelegate?
    var currentAdModel : AdsModel?
   
    override func viewDidLoad() {
        super.viewDidLoad()
        tblAdDetails.register(UINib(nibName: "PostNewJobTableViewCell", bundle: nil), forCellReuseIdentifier: "postNewJobTableViewCell")
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated : Bool){
        NotificationCenter.default.addObserver(self, selector: #selector(self.adImageRemoved(notification:)), name: Notification.Name("AdImageDeleted"), object: nil)

       
    }
    
    @objc func adImageRemoved(notification:Notification){
        currentAdModel?.image = nil
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: Notification.Name("AdImageDeleted"), object: nil)
    }
    
    override func viewDidAppear(_ animated : Bool){
        if layoutType == nil && currentAdModel == nil{
            hideDetailView()
        }else{
            if layoutType == nil && currentAdModel?.layout != nil{
                adRequestModel.adLayout = "Layout_\((currentAdModel?.layout)!)"
                layoutType = adRequestModel.adLayout!
                dataController.currentDescTxt = currentAdModel?.post
                showDetailView()
            }else{
                adRequestModel.adLayout = layoutType
                showDetailView()
            }
            
        }
    }
    
    func hideDetailView(){
        lblChooseLayout.isHidden = false
        tblAdDetails.isHidden = true
    }
    
    func setUpDataForEdit(){
        let mediaModel = PostRLMModel()
        dataController.paramsDict["Title"] = currentAdModel?.title
        dataController.paramsDict["Link"] = currentAdModel?.url
        dataController.currentDescTxt = currentAdModel?.post
        dataController.currentYouTubeLink = currentAdModel?.utub
        dataController.paramsDict["YouTube Link"] = currentAdModel?.utub
        if currentAdModel?.image != nil{
             mediaModel.image = ([currentAdModel?.image]) as! [String]
        }
        mediaModel.id = (currentAdModel?.id)!
        self.attachmentController.adsRequestModel = adRequestModel
        self.attachmentController.isFromEdit = true
        self.attachmentController.currentPostModel = mediaModel
    }
    
    func showDetailView(){
        lblChooseLayout.isHidden = true
        tblAdDetails.isHidden = false
        tblAdDetails.dataSource = dataController
        tblAdDetails.delegate = dataController
        //            previousLayout = layoutType
        if dataController.layoutType != layoutType{
            dataController.layoutType = layoutType!
            if currentAdModel != nil{
               setUpDataForEdit()
            }
            dataController.generateTextFields()
        }
        
        tblAdDetails.reloadData()
    }
    
    @objc func sendAdDetails(){
       
        if layoutType == LAYOUT_TYPES.Layout_1.rawValue || layoutType == LAYOUT_TYPES.Layout_2.rawValue{
                if dataController.paramsDict["Title"] == nil || (dataController.paramsDict["Title"] != nil && (dataController.paramsDict["Title"]?.isEmpty)!){
                    displayToast("Please enter a title")
                    return
                }
                
        }
        if layoutType == LAYOUT_TYPES.Layout_3.rawValue {
            if  dataController.currentDescTxt == nil ||
                (dataController.currentDescTxt != nil && (dataController.currentDescTxt?.isEmpty)!){
                displayToast("Please enter description")
                return
            }
        }
        if currentAdModel != nil{
            if currentAdModel?.image == nil{
                if (dataController.paramsDict["YouTube Link"] == nil || dataController.paramsDict["YouTube Link"] == "") && attachmentController.imageNamesArray.count == 0{
                    displayToast("Please enter youtube link or upload media file")
                    return
                }
            }
        }else{
            if  (dataController.paramsDict["YouTube Link"] == nil || dataController.paramsDict["YouTube Link"] == "") &&
                attachmentController.imageNamesArray.count == 0{
                displayToast("Please enter youtube link or upload media file")
                return
            }
        }
            
       
        if dataController.paramsDict["Link"] != nil  && !((dataController.paramsDict["Link"]?.isEmpty)!) && !((dataController.paramsDict["Link"]?.trimmingCharacters(in: .whitespacesAndNewlines).isValidURL)!){
            displayToast(ENTER_VALID_LINK_TOAST)
            return
        }
        if dataController.paramsDict["YouTube Link"] != nil  && !((dataController.paramsDict["YouTube Link"]?.isEmpty)!) && !((dataController.paramsDict["YouTube Link"]?.trimmingCharacters(in: .whitespacesAndNewlines).isValidYouTubeUrl)!){
            displayToast(ENTER_VALID_YOUTUBELINK_TOAST)
            return
        }
        adRequestModel.adTitle = dataController.paramsDict["Title"]
        adRequestModel.adLink = dataController.paramsDict["Link"]
        adRequestModel.adYouTubeLink = dataController.paramsDict["YouTube Link"]
        adRequestModel.adDescription = dataController.currentDescTxt
        adRequestModel.adLayout = layoutType
        adRequestModel.adMedia = attachmentController.imageNamesArray.count > 0 ?attachmentController.imageNamesArray[0] : nil
        switch layoutType{
        case LAYOUT_TYPES.Layout_2.rawValue:
            adRequestModel.adDescription = ""
        case LAYOUT_TYPES.Layout_3.rawValue:
            adRequestModel.adTitle = ""
        default:
            break
        }
        if let delegate = delegate{
            delegate.passAdDetails(adRequestModel: adRequestModel)
        }
    }

    func addSubView(currentView:UIView) {
        // Add Child View Controller
        addChildViewController(attachmentController)
        // Add Child View as Subview
        currentView.addSubview(attachmentController.view)
        attachmentController.btnPost.isHidden = true
        attachmentController.btnPptAttatchment.isHidden = true
        attachmentController.btnFileAttatchment.isHidden = true
        attachmentController.btnYouTubeLinkAttatchment.isHidden = true
        attachmentController.btnLinkAttatchment.isHidden = true
        attachmentController.btnVideoAttatchment.isHidden = true
        // Configure Child View
        attachmentController.view.frame = currentView.bounds
        attachmentController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        // Notify Child View Controller
        attachmentController.didMove(toParentViewController: self)
    }
}
