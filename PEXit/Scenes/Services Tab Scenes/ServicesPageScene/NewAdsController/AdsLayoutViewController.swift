//
//  AdsLayoutViewController.swift
//  PEXit
//
//  Created by ats on 29/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class AdsLayoutViewController: UIViewController {
    var layoutImgArray = [LAYOUT_TYPES.Layout_1.rawValue,LAYOUT_TYPES.Layout_2.rawValue,LAYOUT_TYPES.Layout_3.rawValue]
    var delegate : ItemSelected?
    var selectedIndex : IndexPath?
    var currentAdModel : AdsModel?
    //var index : Int?
    @IBOutlet weak var collectionViewLayout: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if currentAdModel != nil{
            selectedIndex = IndexPath.init(item:(currentAdModel?.layout)! - 1, section: 0)
        }
        collectionViewLayout.reloadData()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func btnSelectClicked(_ sender : UIButton){
        let index = sender.tag
        sender.isSelected = !sender.isSelected
        selectedIndex = IndexPath.init(item: index, section: 0)
        if let delegate = delegate , selectedIndex != nil{
            delegate.optionSelected(option: layoutImgArray[index])
            collectionViewLayout.reloadData()
        }
       
    }
    
}

extension AdsLayoutViewController : UICollectionViewDataSource , UICollectionViewDelegate , UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
       let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "adsLayoutCell", for: indexPath) as! AdsLayoutCollectionViewCell
        cell.imgAdLayout.image = UIImage(named:layoutImgArray[indexPath.row])
        cell.btnSelect.removeTarget(nil, action: nil, for: .allEvents)
        cell.btnSelect.tag = indexPath.row
        cell.btnSelect.addTarget(self, action: #selector(AdsLayoutViewController.btnSelectClicked(_:)), for: .touchUpInside)
        if indexPath == selectedIndex{
            cell.btnSelect.isSelected = true
            cell.btnSelect.setImage(UIImage.init(named: "select"), for: .normal)
        }else{
            cell.btnSelect.isSelected = false
            cell.btnSelect.setImage(UIImage.init(named: "circle"), for: .normal)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.row == 0 || indexPath.row == 1{
             return CGSize.init(width: collectionView.frame.size.width/2 - 10 , height: collectionView.frame.size.width/2 + 20)
        }else{
             return CGSize.init(width: collectionView.frame.size.width - 20, height: collectionView.frame.size.width/2 + 40)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        if let delegate = delegate{
//            delegate.optionSelected(option: layoutImgArray[indexPath.row])
//        }
    }
}
