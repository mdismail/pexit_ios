//
//  AdsPreviewViewController.swift
//  PEXit
//
//  Created by ats on 29/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import YouTubePlayer

class AdsPreviewViewController: UIViewController {
    var adsRequestModel : NewAdsRequestModel?
    var delegate : AdsDetailDelegate?
    
    @IBOutlet weak var viewPreview: UIView!
    @IBOutlet weak var lblChooseLayout: UILabel!
    @IBOutlet weak var youTubeView: UIView!
    @IBOutlet weak var viewImage: UIImageView!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var lblLink: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnSave: UIButton!
    var currentAdModel : AdsModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if (currentAdModel?.stype == "free" || currentAdModel?.stype == "paid" && currentAdModel?.renew == 0){
            btnSave.setTitle("Save",for:.normal)
        }
        viewPreview.isHidden = true
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated : Bool){
        lblChooseLayout.isHidden = false
        viewPreview.isHidden = true
        if adsRequestModel?.adLayout == nil{
            lblChooseLayout.isHidden = false
            viewPreview.isHidden = true
        }else{
            switch adsRequestModel?.adLayout{
            case LAYOUT_TYPES.Layout_1.rawValue , LAYOUT_TYPES.Layout_2.rawValue :
                if adsRequestModel?.adTitle == nil || (adsRequestModel?.adTitle != nil && (adsRequestModel?.adTitle?.isEmpty)!){
                    lblChooseLayout.isHidden = false
                    lblChooseLayout.text = "Title cannot be blank,Please update and save the details"
                    return
                }
                if adsRequestModel?.adYouTubeLink == nil && adsRequestModel?.adMedia == nil{
                    lblChooseLayout.isHidden = false
                    lblChooseLayout.text = "Please add a media file or youtube link,Please update and save the details"
                    return
                }
            case LAYOUT_TYPES.Layout_3.rawValue:
                if adsRequestModel?.adDescription == nil || (adsRequestModel?.adDescription != nil && (adsRequestModel?.adDescription?.isEmpty)!){
                    lblChooseLayout.isHidden = false
                    lblChooseLayout.text = "Description cannot be blank,Please update and save the details"
                    return
                }
                if adsRequestModel?.adYouTubeLink == nil && adsRequestModel?.adMedia == nil{
                    lblChooseLayout.isHidden = false
                    lblChooseLayout.text = "Please add a media file or youtube link,Please update and save the details"
                    return
                }
            case "1","2","3":
                print("Backward")
            default:
                return
            }
            lblChooseLayout.isHidden = true
            setPreviewData()
            viewPreview.isHidden = false
        }
    }
    
    func setPreviewData(){
        youTubeView.isHidden = true
        if currentAdModel?.image != nil && adsRequestModel?.adMedia?.file == nil{
            if let imageUrl = URL(string:(currentAdModel?.image)!){
                 viewImage.sd_setImage(with: imageUrl , placeholderImage: nil)
            }
        }else{
             viewImage.image = adsRequestModel?.adMedia?.file as? UIImage
        }
       
        lblLink.text = adsRequestModel?.adLink ?? ""
        lblTitle.text = adsRequestModel?.adTitle ?? ""
        lblDesc.text = adsRequestModel?.adDescription ?? ""
        if adsRequestModel?.adYouTubeLink != nil && !((adsRequestModel?.adYouTubeLink?.isEmpty)!) && (adsRequestModel?.adYouTubeLink?.isValidYouTubeUrl)!{
            setUpVideoView(videoUrl: adsRequestModel?.adYouTubeLink)
        }
    }
    
    func setUpVideoView(videoUrl : String?){
        youTubeView.isHidden = false
        let youTubePlayer = YouTubePlayerView()
        youTubePlayer.frame = youTubeView.bounds
        youTubePlayer.backgroundColor = UIColor.red
        self.view.bringSubview(toFront: youTubePlayer)
        youTubeView.addSubview(youTubePlayer)
        youTubeView.clipsToBounds = true
        youTubeView.layer.masksToBounds = true
        if (videoUrl?.isValidYouTubeUrl)!{
            if let myVideoURL = videoUrl{
                let youTubeUrl = URL(string:myVideoURL)
                youTubeUrl?.expandURLWithCompletionHandler(completionHandler: {
                    expandedURL in
                    
                    var urlString = expandedURL?.absoluteString
                    urlString = urlString?.replacingOccurrences(of: "&feature=youtu.be", with: "", options: NSString.CompareOptions.literal, range:nil)
                    
                    DispatchQueue.main.async {
                        youTubePlayer.loadVideoURL(URL(string:urlString!)!)
                    }
                    
                    
                })
                
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnPostAdClicked(_ sender: Any) {
       
        if let delegate = delegate{
            delegate.passAdDetails(adRequestModel: adsRequestModel!)
        }
    }
    
}
