//
//  NewAdWorker.swift
//  PEXit
//
//  Created by ats on 29/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation

class NewAdWorker{
    func fetchCompanies(completionHandler: @escaping(_ countries : CompanyResponse) -> Void) {
        START_LOADING_VIEW()
        ServiceManager.methodType(requestType: GET_REQUEST, url: GET_LIST_OF_COMPANIES, params: nil, paramsData: nil, completion: { (response,_ responseData, statusCode) in
            STOP_LOADING_VIEW()
            let decoder = JSONDecoder()
            do{
                let responseCompanies = try decoder.decode(CompanyResponse.self, from: responseData!)
                completionHandler(responseCompanies)
            }catch{
                print("error")
            }
            
        }) { (response, statusCode) in
            
        }
    }
}
