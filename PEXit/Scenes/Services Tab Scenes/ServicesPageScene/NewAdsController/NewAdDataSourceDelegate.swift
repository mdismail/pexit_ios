//
//  NewAdDataSourceDelegate.swift
//  PEXit
//
//  Created by ats on 29/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation
import UIKit
import MobileCoreServices

class NewAdDataSourceDelegate: NSObject, UITableViewDataSource , UITableViewDelegate{
    
    var arrayOfTextFields = [TextfieldInfoStruct]()
    var parentController : AdsDetailViewController?
    var currentTblView : UITableView?
    var expandingCellHeight: CGFloat = 50
    var expandingIndexRow : Int?
    var updatedHeight: CGFloat?
    var layoutType : String?
    var paramsDict = [String:String]()
    var currentDescTxt : String?
    var uploadedLogo : MediaAttatchment?
    var currentYouTubeLink : String?
    
    func generateTextFields(){
        currentTblView = parentController?.tblAdDetails
        switch layoutType{
        case LAYOUT_TYPES.Layout_1.rawValue:
            arrayOfTextFields = [TextfieldInfoStruct(placeholderText: "Title",textfieldText:paramsDict["Title"]),TextfieldInfoStruct(placeholderText: "Link",textfieldText:paramsDict["Link"])]
        case LAYOUT_TYPES.Layout_2.rawValue:
             arrayOfTextFields = [TextfieldInfoStruct(placeholderText: "Title",textfieldText:paramsDict["Title"]),TextfieldInfoStruct(placeholderText: "Link",textfieldText:paramsDict["Link"])]
        case LAYOUT_TYPES.Layout_3.rawValue:
             arrayOfTextFields = [TextfieldInfoStruct(placeholderText: "Link",textfieldText:paramsDict["Link"])]
        default:
            break
        
        }
       
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  layoutType == LAYOUT_TYPES.Layout_2.rawValue ? arrayOfTextFields.count + 3 : arrayOfTextFields.count + 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let postJobCell = tableView.dequeueReusableCell(withIdentifier: "postNewJobTableViewCell", for: indexPath) as! PostNewJobTableViewCell
        postJobCell.hideViews()
        if indexPath.row < arrayOfTextFields.count{
            postJobCell.txtFieldInput.delegate = self
            postJobCell.txtFieldInput.tag = indexPath.row
            postJobCell.txtFieldInput.isHidden = false
            postJobCell.configureTextfield(arrayOfTextFields[indexPath.row])
        }
        else if indexPath.row == arrayOfTextFields.count && (layoutType !=  LAYOUT_TYPES.Layout_2.rawValue) {
            postJobCell.viewTxtView.isHidden = false
            postJobCell.delegate = self
            postJobCell.configureTxtView(placeholderStr: "Description",currentText: currentDescTxt)
        }
        else if (indexPath.row == arrayOfTextFields.count + 1 && layoutType != LAYOUT_TYPES.Layout_2.rawValue) || (layoutType ==  LAYOUT_TYPES.Layout_2.rawValue && indexPath.row == arrayOfTextFields.count){
            postJobCell.txtFieldInput.isHidden = false
            postJobCell.txtFieldInput.tag = indexPath.row
            postJobCell.txtFieldInput.delegate = self
            postJobCell.configureTextfield(TextfieldInfoStruct(placeholderText: "YouTube Link",textfieldText:currentYouTubeLink))
        }
        else if (indexPath.row == (arrayOfTextFields.count) + 2 && layoutType != LAYOUT_TYPES.Layout_2.rawValue) || (layoutType ==  LAYOUT_TYPES.Layout_2.rawValue && indexPath.row == arrayOfTextFields.count + 1){
                postJobCell.viewAttachment.isHidden = false
                parentController?.attachmentController.delegate = self
                parentController?.addSubView(currentView: postJobCell.viewAttachment)
        }
        else if (indexPath.row == (arrayOfTextFields.count) + 3 && layoutType != LAYOUT_TYPES.Layout_2.rawValue) || (layoutType ==  LAYOUT_TYPES.Layout_2.rawValue && indexPath.row == arrayOfTextFields.count + 2){
            postJobCell.btnSubmit.isHidden = false
            postJobCell.btnSubmit.setTitle("Save",for:.normal)
            postJobCell.btnSubmit.addTarget(parentController, action: #selector(AdsDetailViewController.sendAdDetails), for: .touchUpInside)
        }
       
        return postJobCell
    }
    

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row < arrayOfTextFields.count{
            return 65
        }
        else if indexPath.row == arrayOfTextFields.count && (layoutType !=  LAYOUT_TYPES.Layout_2.rawValue) {
          // return expandingCellHeight + 25
            return UITableViewAutomaticDimension
        }
        else if (indexPath.row == arrayOfTextFields.count + 1 && layoutType != LAYOUT_TYPES.Layout_2.rawValue) || (layoutType ==  LAYOUT_TYPES.Layout_2.rawValue && indexPath.row == arrayOfTextFields.count){
            return 65
        }
        else if (indexPath.row == (arrayOfTextFields.count) + 2 && layoutType != LAYOUT_TYPES.Layout_2.rawValue) || (layoutType ==  LAYOUT_TYPES.Layout_2.rawValue && indexPath.row == arrayOfTextFields.count + 1){
            return updatedHeight != nil ? updatedHeight! : 50
        }
        else if (indexPath.row == (arrayOfTextFields.count) + 3 && layoutType != LAYOUT_TYPES.Layout_2.rawValue) || (layoutType ==  LAYOUT_TYPES.Layout_2.rawValue && indexPath.row == arrayOfTextFields.count + 2){
            return 60
        }
        return 60
    }
    
}

extension NewAdDataSourceDelegate : PassAttatchmentsToController , ExpandingCellDelegate , UITextFieldDelegate{
    
    
    //MARK: UITextFieldDelegate
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.accessibilityLabel == "YouTube Link"{
           currentYouTubeLink = textField.text
        }else{
            let selectedIndexPath: IndexPath = IndexPath(row: textField.tag, section: 0)
            arrayOfTextFields[selectedIndexPath.row].textfieldText = textField.text
        }
       
        paramsDict[textField.accessibilityLabel!] = textField.text
    }
    
    func updateHeightOfParentController(height: CGFloat) {
        //Taking a constant
        updatedHeight = DEFAULT_HEIGHT_ATTACHMENT_VIEW + height
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
            self.currentTblView!.beginUpdates()
            self.currentTblView!.layoutIfNeeded()
            self.currentTblView!.endUpdates()
        })
        
    }
    
    func updated(height: CGFloat,currentTextView: UITextView) {
        currentDescTxt = currentTextView.text
       // expandingCellHeight = height + 20
        UIView.setAnimationsEnabled(false)
        //        /* These will causes table cell heights to be recaluclated,
        //         without reloading the entire cell */
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
            self.currentTblView!.beginUpdates()
            //  let indexPath = IndexPath(row: self.expandingIndexRow!, section: 0)
            self.currentTblView!.layoutIfNeeded()
            
            self.currentTblView!.endUpdates()
            //        // Re-enable animations
            UIView.setAnimationsEnabled(true)
        })
        
    }
 
}
