//
//  NewServiceConfigurator.swift
//  PEXit
//
//  Created by Apple on 03/08/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation


extension ProductsServicesViewController: NewServicePresenterOutput{
   
}

extension ServiceInteractor: NewServiceViewControllerOutput{
}

extension NewServicePresenter: ServiceInteractorOutput{
   
    
   
    
}



class NewServiceConfigurator{
    
    // MARK: Object lifecycle
    
    class var sharedInstance: NewServiceConfigurator{
        struct Static {
            static let instance =  NewServiceConfigurator()
        }
        print("RegistrationConfigurator instance created")
        return Static.instance
    }
    
    // MARK: Configuration
    
    func configure(viewController: ProductsServicesViewController){
        
        let presenter = NewServicePresenter()
        presenter.output = viewController
        let interactor = ServiceInteractor()
        interactor.output = presenter
        viewController.output = interactor
        
        
    }
    
}
