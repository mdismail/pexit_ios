//
//  NewServiceInteractor.swift
//  PEXit
//
//  Created by ats on 22/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation

protocol ServiceInteractorInput{
    func fetchServicesList(typeOfService:String,pageNumber:Int?)
}


protocol ServiceInteractorOutput{
    func presentProductsList(response: NewServiceResponseModel?)
    func presentJobsList(response: JobServiceModel?)
    func presentSponorsList(response: SponsorResponseModel?)
    func presentAdsList(response: AdsResponseModel?)
    
}

class ServiceInteractor : ServiceInteractorInput{
    var output: ServiceInteractorOutput!
    var serviceWorker = NewServiceWorker()
    func fetchServicesList(typeOfService:String,pageNumber:Int?){
        serviceWorker.fetchServiceList(serviceType: typeOfService, pageNumber: pageNumber) {[weak self] (responseObject, error) in
            if error == nil{
                switch typeOfService{
                case SERVICE_TYPE.PRODUCT.rawValue , SERVICE_TYPE.SERVICE.rawValue , SERVICE_TYPE.SOFTWARE.rawValue:
                        self?.output.presentProductsList(response: responseObject as? NewServiceResponseModel)
                case SERVICE_TYPE.JOB.rawValue:
                        self?.output.presentJobsList(response: responseObject as? JobServiceModel)
                case SERVICE_TYPE.SPONSOR.rawValue :
                        self?.output.presentSponorsList(response: responseObject as? SponsorResponseModel)
                case SERVICE_TYPE.ADS.rawValue:
                        self?.output.presentAdsList(response: responseObject as? AdsResponseModel)
                default :
                return
                }
                
            }
        }
    }
}
