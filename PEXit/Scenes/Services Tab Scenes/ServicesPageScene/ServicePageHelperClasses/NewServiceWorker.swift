//
//  NewServiceWorker.swift
//  PEXit
//
//  Created by ats on 22/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation

class NewServiceWorker{
    func fetchServiceList(serviceType : String,pageNumber:Int?,completionHandler: @escaping(_ response : AnyObject?,_ error:Error? ) -> Void) {
        if pageNumber == 1{
            START_LOADING_VIEW()
        }
        //START_LOADING_VIEW()
        ServiceManager.methodType(requestType: POST_REQUEST, url: LIST_OF_FREE_PAID_SERVICES(serviceType: serviceType), params: ["records":NUMBER_OF_RECORDS,"page":pageNumber ?? 1] ,paramsData: nil,completion: { (response,_ responseData, statusCode) in
            STOP_LOADING_VIEW()
//            print("Response of services is --->",response)
            let decoder = JSONDecoder()
            do{
                switch serviceType{
                 case SERVICE_TYPE.PRODUCT.rawValue , SERVICE_TYPE.SERVICE.rawValue , SERVICE_TYPE.SOFTWARE.rawValue:
                    let serviceDetails = try decoder.decode(NewServiceResponseModel.self, from: responseData!)
                    completionHandler(serviceDetails,nil)
                case SERVICE_TYPE.JOB.rawValue:
                    let jobDetails = try decoder.decode(JobServiceModel.self, from: responseData!)
                    completionHandler(jobDetails,nil)
                case SERVICE_TYPE.SPONSOR.rawValue:
                    let sponsorDetails = try decoder.decode(SponsorResponseModel.self, from: responseData!)
                    completionHandler(sponsorDetails,nil)
                case SERVICE_TYPE.ADS.rawValue:
                    let adsDetails = try decoder.decode(AdsResponseModel.self, from: responseData!)
                    completionHandler(adsDetails,nil)
                default :
                    return
                }
                
            }catch{
                DispatchQueue.main.async{
                    if ((response as! [String : Any])["message"] as? String) != nil{
                        KEY_WINDOW?.makeToast((response as! [String : Any])["message"] as? String)
                    }
                }
                
                print("error")
            }
            
        }) { (response, statusCode) in
            STOP_LOADING_VIEW()
        }
    }

    func uploadCompanyProfile(fileData:MediaAttatchment?,completionHandler:@escaping(_ responseModel : CommonModel.BaseModel) -> Void){
        START_LOADING_VIEW()
        
        AlamofireManager.shared().updateCompanyProfile(apiUrl: UPLOAD_COMPANY_PROFILE,fileData:fileData) { (response, error) in
            STOP_LOADING_VIEW()
            if response != nil{
                if !(((response!["errors"] as? String)?.isEmpty)!) || !((response!["virus"] as? String)?.isEmpty)!{
                    Utils.showErrorFilesAlert(errorFiles : response!["errors"] as? String,corruptFiles : response!["virus"] as? String)
                }
                let decoder = JSONDecoder()
                do{
                    let jsonData = try JSONSerialization.data(withJSONObject: response, options: .prettyPrinted)
                    let reqJSONStr = String(data: jsonData, encoding: .utf8)
                    let data = reqJSONStr?.data(using: .utf8)
                    let responsePost = try decoder.decode(CommonModel.BaseModel.self, from: data!)
                    completionHandler(responsePost)
                }catch{
                    print("error")
                }
            }else{
                STOP_LOADING_VIEW()
                KEY_WINDOW?.makeToast(error?.localizedDescription)
            }
            
        }
    }

}


