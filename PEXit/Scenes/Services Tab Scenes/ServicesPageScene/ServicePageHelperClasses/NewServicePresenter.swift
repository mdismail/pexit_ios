//
//  NewServicePresenter.swift
//  PEXit
//
//  Created by ats on 22/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation

protocol NewServicePresenterInput{
    func presentProductsList(response: NewServiceResponseModel?)
    func presentJobsList(response: JobServiceModel?)
    func presentSponorsList(response: SponsorResponseModel?)
    func presentAdsList(response: AdsResponseModel?)
    
}

protocol NewServicePresenterOutput: class{
    func presntProductsResponse(response:[ServiceProductModel]?,message: String?)
    func presntJobsResponse(response:[JobDetailModel]?,message: String?)
    func presentSponsorResponse(response:[SponsorModel]?,message: String?)
    func presentAdsResponse(response:[AdsModel]?,message: String?)
}

class NewServicePresenter : NewServicePresenterInput{
    weak var output: NewServicePresenterOutput!
    
    func presentProductsList(response: NewServiceResponseModel?){
        if response != nil && response?.status == true{
            output.presntProductsResponse(response: response?.datas, message: "")
        }else{
            response != nil ? output.presntProductsResponse(response: nil, message: response?.message) :
                output.presntProductsResponse(response: nil, message: UNKNOWN_ERROR_MSG)
        }
    }
    
    func presentJobsList(response: JobServiceModel?){
        if response != nil && response?.status == true{
            output.presntJobsResponse(response: response?.datas, message: "")
        }else{
            response != nil ? output.presntJobsResponse(response: nil, message: response?.message) :
                output.presntJobsResponse(response: nil, message: UNKNOWN_ERROR_MSG)
        }
    }
    
    func presentSponorsList(response: SponsorResponseModel?){
        if response != nil && response?.status == true{
            output.presentSponsorResponse(response: response?.datas, message: "")
        }else{
            response != nil ? output.presentSponsorResponse(response: nil, message: response?.message) :
                output.presentSponsorResponse(response: nil, message: UNKNOWN_ERROR_MSG)
        }
    }
    
    func presentAdsList(response: AdsResponseModel?){
        if response != nil && response?.status == true{
            output.presentAdsResponse(response: response?.datas, message: "")
        }else{
            response != nil ? output.presentAdsResponse(response: nil, message: response?.message) :
                output.presentAdsResponse(response: nil, message: UNKNOWN_ERROR_MSG)
        }
    }
}
