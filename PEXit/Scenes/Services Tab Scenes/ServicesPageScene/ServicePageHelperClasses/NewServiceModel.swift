//
//  NewServiceModel.swift
//  PEXit
//
//  Created by ats on 22/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation

class JobServiceModel : Codable{
    var message : String?
    var status : Bool?
    var datas : [JobDetailModel]?
}

class NewServiceResponseModel : Codable{
    var message : String?
    var status : Bool?
    var datas : [ServiceProductModel]?
}

class ServiceProductModel : Codable{
    var id : Int?
    var logo : String?
    var name : String?
    var descrip : String?
    var category : String?
    var stype : String?
    var active : Int?
    var expired : Int?
    var edit : Int?
    var state : Int?
    var renew : Int?
    var company : String?
    var industry : String?
    var country : String?
    var states : String?
    var city : String?
    var links : String?
    var utube : String?
    var url : String?
    var software : String?
    var pincode : String?
    var files : [String]?
    var email : String?
    var photos : [String]?
    var presentation : [String]?
    var videos : [String]?
    var orderId : String?
    var location : String?
    var pdetails : String?
    var thistory : String?
    var pcondition : String?
    var pgyoutube : String?
    var pgimage : [String]?
    var pgvideo : [String]?
    var isExpanded : Bool?
   
    
}

class SponsorResponseModel : Codable{
    var message : String?
    var status : Bool?
    var datas : [SponsorModel]?
}

class SponsorModel : Codable{
    var id : Int?
    var title : String?
    var post : String?
//    var month : String?
//    var year : String?
    var state : Int?
    var active : Int?
    var expired : Int?
    var edit : Int?
    var renew : Int?
    var link : String?
    var stype : String?
  //  var links : String?
    var video : String?
    var docu : [String]?
    var image : [String]?
    var ppt : [String]?
    var allvid : [String]?
    var isExpanded : Bool?
 //   var amount : Int?
}

class AdsResponseModel : Codable{
    var message : String?
    var status : Bool?
    var datas : [AdsModel]?
}

class AdsModel : Codable{

    var id : Int?
    var title : String?
    var post : String?
    var utub : String?
    var url : String?
    var image : String?
    var expired : Int?
    var edit : Int?
    var renew : Int?
    var active : Int?
    var isExpanded : Bool?
    var stype : String?
    var layout : Int?
    var gender : String?
    var age : String?
    var compny : String?
    
//    required init(from decoder: Decoder) throws {
//        let container = try decoder.container(keyedBy: CodingKeys.self)
//        if let isExpanded = try container.decodeIfPresent(Bool.self, forKey: .isExpanded) {
//            self.isExpanded = isExpanded
//        }
//    }
  
}

class SponsoredContentRequestModel{
    var title : String?
    var description : String?
    var files : [MediaAttatchment]?
    var images : [MediaAttatchment]?
    var videos : [MediaAttatchment]?
    var ppt : [MediaAttatchment]?
    var youTubeLink : String?
    var links  : String?
    var orderId : String?
    var stype : String?
    var paid : String?
    var promoCode : String?
    var currency : String?
    var tax : String?
    var duration : String?
    var idOfContent : Int?
}


class NewProductRequestModel{
    var name : String?
    var location : String?
    var logo : MediaAttatchment?
    var details : MediaAttatchment?
    var videos : [MediaAttatchment]?
    var images : [MediaAttatchment]?
    var transaction : MediaAttatchment?
    var youTubeLink : String?
    var category : String?
    var description  : String?
    var condition : String?
    var orderId : String?
    var stype : String?
    var paid : String?
    var promoCode : String?
    var currency : String?
    var tax : String?
    var duration : String?
    var country : String?
    var state : String?
    var city : String?
    var idOfProduct : Int?
}


class NewSoftwareRequestModel{
    var softwareName : String?
    var softwareUrl : String?
    var logo : MediaAttatchment?
    var software : MediaAttatchment?
    var softwareDesc  : String?
    var orderId : String?
    var stype : String?
    var paid : String?
    var promoCode : String?
    var currency : String?
    var tax : String?
    var duration : String?
    var idOfSoftware : Int?
}


class NewServiceRequestModel{
    var serviceType : String?
    var country : String?
    var logo : MediaAttatchment?
    var file : [MediaAttatchment]?
    var videos : [MediaAttatchment]?
    var images : [MediaAttatchment]?
    var ppt : [MediaAttatchment]?
    var youTubeLink : String?
    var link : String?
    var company : String?
    var industry  : String?
    var state : String?
    var city : String?
    var postalCode : String?
    var email : String?
    var description : String?
    var duration : String?
    var stype : String?
    var paid : String?
    var promoCode : String?
    var currency : String?
    var tax : String?
    var orderId : String?
    var idOfService : Int?
}


class NewJobRequestModel{
    var jobCompany : String?
    var jobTitle : String?
    var jobLogo : MediaAttatchment?
    var country : String?
    var jobFunction  : String?
    var state : String?
    var city : String?
    var empType : String?
    var industry : String?
    var level  : String?
    var jobDesc : String?
    var companyDesc : String?
    var applyType : String?
    var jobApplySite : String?
    var jobView : String?
    var email : String?
    var desc : String?
    var duration : String?
    var stype : String?
    var paid : String?
    var postalCode : String?
    var promoCode : String?
    var currency : String?
    var tax : String?
    var orderId : String?
    var idOfJob : Int?
}


class NewAdsRequestModel{
    var adLayout : String?
    var adTitle : String?
    var adMedia : MediaAttatchment?
    var adLink : String?
    var adDescription  : String?
    var adYouTubeLink :String?
    var adAge : String?
    var adGender : String?
    var adCountry : String?
    var adState  : String?
    var adCompany : String?
    var orderId : String?
    var duration : String?
    var stype : String?
    var paid : String?
    var promoCode : String?
    var currency : String?
    var tax : String?
    var idOfAd : Int?
}

struct ServicePostResponseModel : Codable{
    var status : Bool?
    var message : String?
    var datas : String?
}
