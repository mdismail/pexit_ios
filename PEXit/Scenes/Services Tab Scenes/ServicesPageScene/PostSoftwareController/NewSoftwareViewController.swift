//
//  NewSoftwareViewController.swift
//  PEXit
//
//  Created by ats on 22/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class NewSoftwareViewController: UIViewController {
    @IBOutlet weak var tblPostSoftware: UITableView!
    var currentSoftwareModel : ServiceProductModel?
    var currentTextField : UITextField?
    lazy var dataController: NewSoftwareDataSourceDelegate = {
        let dataController = NewSoftwareDataSourceDelegate()
        dataController.parentController = self
        dataController.generateTextFields()
        return dataController
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("Test")
        addBackButton(withTitle: "Upload Software")
        tblPostSoftware.register(UINib(nibName: "PostNewJobTableViewCell", bundle: nil), forCellReuseIdentifier: "postNewJobTableViewCell")
        tblPostSoftware.dataSource = dataController
        tblPostSoftware.delegate = dataController
        if currentSoftwareModel != nil{
            setEditData()
        }
        // Do any additional setup after loading the view.
    }

    func setEditData(){
        dataController.paramsDict["Software Name"] = currentSoftwareModel?.name
        if let logo = currentSoftwareModel?.logo{
            if let pathUrl = URL(string:logo){
                dataController.paramsDict["Upload Logo"] = pathUrl.lastPathComponent
            }
        }
       // dataController.paramsDict["Upload Logo"] = currentSoftwareModel?.logo
        dataController.paramsDict["Download Link"] = currentSoftwareModel?.url
        dataController.paramsDict["Description"] = currentSoftwareModel?.descrip
        dataController.generateTextFields()
    }
    
    func getAllRequiredFields() -> [TJTextField]{
        var textFieldsArray = [TJTextField]()
        for txtFieldDetails in dataController.arrayOfTextFields{
            let txtField = TJTextField()
            txtField.placeholder = txtFieldDetails.placeholderText
            txtField.text = txtFieldDetails.textfieldText
            if currentSoftwareModel != nil{
                if txtFieldDetails.placeholderText == "Upload Logo"{
                    continue
                }
            }
            textFieldsArray.append(txtField)
        }
        return textFieldsArray
    }
    
    //MARK: Get all required fields
 /*   func getAllRequiredFields() -> [TJTextField] {
        var textFieldsArray = [TJTextField]()
        let cells = Utils.cellsForTableView(tblPostSoftware) as! [PostNewJobTableViewCell]
        for cell in cells {
            if !cell.txtFieldInput.isHidden{
                textFieldsArray.append(cell.txtFieldInput)
            }
        }
        return textFieldsArray
    } */
    
    func uplaodSoftware(){
        if dataController.paramsDict["Download Link"] != nil &&  !((dataController.paramsDict["Download Link"]?.isEmpty)!) && (dataController.paramsDict["Download Link"]?.trimmingCharacters(in: .whitespacesAndNewlines).isValidURL)!{
            let newSoftwareModel = NewSoftwareRequestModel()
            newSoftwareModel.software = dataController.uploadedSoftware
            newSoftwareModel.logo = dataController.productLogo
            newSoftwareModel.softwareName = dataController.paramsDict["Software Name"]
            newSoftwareModel.softwareDesc = dataController.paramsDict["Description"]
            newSoftwareModel.softwareUrl = dataController.paramsDict["Download Link"]
            if (currentSoftwareModel?.stype == "free" || currentSoftwareModel?.stype == "paid" && currentSoftwareModel?.renew == 0){
                editSoftwareDetails(requestModel: newSoftwareModel)
            }else{
                navigateToCheckOutController(requestModel: newSoftwareModel)
            }
        }else{
            displayToast(ENTER_VALID_LINK_TOAST)
        }
    }
    
    @objc func uploadSoftware(){
        self.dismissKeyboard()
        if Utils.validateFormAndShowToastWithRequiredFields(getAllRequiredFields()){
            if dataController.paramsDict["Description"] != nil && !(dataController.paramsDict["Description"]?.isEmpty)!{
        if currentSoftwareModel != nil {
            uplaodSoftware()
//            if dataController.paramsDict["Download Link"] != nil &&  !((dataController.paramsDict["Download Link"]?.isEmpty)!) && (dataController.paramsDict["Download Link"]?.trimmingCharacters(in: .whitespacesAndNewlines).isValidURL)!{
//            let newSoftwareModel = NewSoftwareRequestModel()
//            newSoftwareModel.software = dataController.uploadedSoftware
//            newSoftwareModel.logo = dataController.productLogo
//            newSoftwareModel.softwareName = dataController.paramsDict["Software Name"]
//            newSoftwareModel.softwareDesc = dataController.paramsDict["Description"]
//            newSoftwareModel.softwareUrl = dataController.paramsDict["Download Link"]
//            if (currentSoftwareModel?.stype == "free" || currentSoftwareModel?.stype == "paid" && currentSoftwareModel?.renew == 0){
//                editSoftwareDetails(requestModel: newSoftwareModel)
//            }else{
//                navigateToCheckOutController(requestModel: newSoftwareModel)
//            }
//            }else{
//                displayToast(ENTER_VALID_LINK_TOAST)
//            }
        }else{
            if (dataController.uploadedSoftware == nil){
                displayToast("Please upload software")
            }else{
                uplaodSoftware()
                }
            }
            }
        else{
                displayToast("Please enter description")
            }
        }
    }
    
    
   
    func navigateToCheckOutController(requestModel : NewSoftwareRequestModel){
        let checkoutController = storyboard?.instantiateViewController(withIdentifier: "checkoutViewController") as! CheckoutViewController
        if currentSoftwareModel != nil{
            checkoutController.isFromEdit = true
            checkoutController.idToEdit = currentSoftwareModel?.id
        }
        checkoutController.softwareRequestModel = requestModel
        checkoutController.serviceType = SERVICE_TYPE_API.SOFTWARES.rawValue
        navigationController?.pushViewController(checkoutController, animated: true)
    }
    
    func editSoftwareDetails(requestModel : NewSoftwareRequestModel){
        
        let apiName = UPDATE_SOFTWARE(softwareId: (currentSoftwareModel?.id)!)
        let worker = CheckoutWorker()
        worker.postNewSoftware(apiName:apiName,requestObject: requestModel) { (response, error) in
            if response != nil {
                self.displayToast((response?.message) ?? "Please try again")
               // self.navigationController?.popToRootViewController(animated: true)
                self.performSegue(withIdentifier: "navigateToSoftware", sender: self)
                STOP_LOADING_VIEW()
            }else{
                self.displayToast(error?.localizedDescription ?? UNKNOWN_ERROR_MSG)
            }
        }
        
    }

}
