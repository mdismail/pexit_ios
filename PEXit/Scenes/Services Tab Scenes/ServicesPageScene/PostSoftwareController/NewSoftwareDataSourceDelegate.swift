//
//  NewSoftwareDataSourceDelegate.swift
//  PEXit
//
//  Created by ats on 22/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation
import UIKit
import MobileCoreServices

class NewSoftwareDataSourceDelegate: NSObject, UITableViewDataSource , UITableViewDelegate{
    
    var arrayOfTextFields = [TextfieldInfoStruct]()
    var parentController : NewSoftwareViewController?
    var currentTblView : UITableView?
    var expandingCellHeight: CGFloat = 50
    var expandingIndexRow : Int?
    var paramsDict = [String : String]()
    var productLogo : MediaAttatchment?
    var uploadedSoftware : MediaAttatchment?
    var uploadedsoftwareName : String?
    func generateTextFields(){
        currentTblView = parentController?.tblPostSoftware
        arrayOfTextFields = [TextfieldInfoStruct(rightImage: UIImage(named: UPLOAD_LOGO_IMAGE),placeholderText: "Upload Logo",textfieldText:paramsDict["Upload Logo"]),
                             TextfieldInfoStruct(placeholderText: "Software Name",textfieldText:paramsDict["Software Name"]) ,
                             TextfieldInfoStruct(placeholderText: "Download Link",textfieldText:paramsDict["Download Link"])]
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayOfTextFields.count + 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let postJobCell = tableView.dequeueReusableCell(withIdentifier: "postNewJobTableViewCell", for: indexPath) as! PostNewJobTableViewCell
        postJobCell.hideViews()
        if indexPath.row < arrayOfTextFields.count{
            postJobCell.txtFieldInput.delegate = self
            postJobCell.txtFieldInput.tag = indexPath.row
            postJobCell.txtFieldInput.isHidden = false
            postJobCell.configureTextfield(arrayOfTextFields[indexPath.row])
        }
        else if indexPath.row == arrayOfTextFields.count{
            postJobCell.viewTxtView.isHidden = false
            postJobCell.delegate = self
            postJobCell.configureTxtView(placeholderStr: "Description",currentText: paramsDict["Description"])
        }
        else if indexPath.row == (arrayOfTextFields.count) + 1{
            postJobCell.txtFieldInput.isHidden = false
            postJobCell.txtFieldInput.delegate = self
            postJobCell.txtFieldInput.tag = indexPath.row
            postJobCell.configureTextfield(TextfieldInfoStruct(rightImage: UIImage(named: "uploaddocs"),placeholderText: "Upload Software",textfieldText: uploadedsoftwareName))
        }
        else if indexPath.row == (arrayOfTextFields.count) + 2{
            postJobCell.btnSubmit.isHidden = false
            if parentController?.currentSoftwareModel != nil{
                postJobCell.btnSubmit.setTitle("Save", for: .normal)
            }
             postJobCell.btnSubmit.addTarget(parentController, action: #selector(NewSoftwareViewController.uploadSoftware), for: .touchUpInside)
        }
        return postJobCell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row{
        case indexPath.row where indexPath.row < arrayOfTextFields.count:
            return 65
        case indexPath.row where indexPath.row == arrayOfTextFields.count:
            return UITableViewAutomaticDimension
        case indexPath.row where indexPath.row == arrayOfTextFields.count + 1:
            return 65
        case indexPath.row where indexPath.row == arrayOfTextFields.count + 2:
            return 120
        default:
            return 60
            
        }
    }
    
}

extension NewSoftwareDataSourceDelegate : ExpandingCellDelegate , UITextFieldDelegate{
    func updated(height: CGFloat, currentTextView: UITextView) {
        
        
        
      //  expandingCellHeight = height + 20
        paramsDict["Description"] = currentTextView.text
        UIView.setAnimationsEnabled(false)
        //        /* These will causes table cell heights to be recaluclated,
        //         without reloading the entire cell */
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
            self.currentTblView!.beginUpdates()
            //  let indexPath = IndexPath(row: self.expandingIndexRow!, section: 0)
            self.currentTblView!.layoutIfNeeded()
            
            self.currentTblView!.endUpdates()
            //        // Re-enable animations
            UIView.setAnimationsEnabled(true)
        })
    }
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField.accessibilityLabel == "Upload Logo"{
            uploadLogo(textField: textField)
            return false
        }
        if textField.accessibilityLabel == "Upload Software"{
            uploadFile(textField: textField)
            return false
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        let selectedIndexPath: IndexPath = IndexPath(row: textField.tag, section: 0)
        arrayOfTextFields[selectedIndexPath.row].textfieldText = textField.text
        paramsDict[textField.accessibilityLabel!] = textField.text
    }
    
    func uploadLogo(textField : UITextField){
        ImageVideoPicker.imageVideoPicker.showImagePicker(controller: parentController!, mediaType: .Image)
        ImageVideoPicker.imageVideoPicker.imagePickedBlock = { [unowned self](imageData) in
            textField.text = imageData.fileName
             self.productLogo = MediaAttatchment(file: imageData.file, fileName: imageData.fileName,fileData:UIImageJPEGRepresentation(imageData.file as! UIImage, 0.4),fileExtension:imageData.fileExtension)
            let selectedIndexPath: IndexPath = IndexPath(row: textField.tag, section: 0)
            self.arrayOfTextFields[selectedIndexPath.row].textfieldText = textField.text
          }
    }

    func uploadFile(textField : UITextField){
        FileAttatchmentHandler.shared.showDocumentPicker(vc: parentController!, files: [String(kUTTypeZipArchive),String(kUTTypeApplication),String(kUTTypeApplicationFile),String(kUTTypeUnixExecutable),String(kUTTypeWindowsExecutable)])
        FileAttatchmentHandler.shared.filePickedBlock = {[unowned self] (url) in
            do{
                let data = try Data(contentsOf: url, options: .mappedIfSafe)
                print(data)
                let mediaFile = MediaAttatchment(file: nil, fileName: url.lastPathComponent,fileData:data,fileExtension:url.mimeType())
                self.uploadedSoftware = mediaFile
                
            }catch{
            }
            textField.text = url.lastPathComponent
            self.uploadedsoftwareName = textField.text
        }
    }
}
