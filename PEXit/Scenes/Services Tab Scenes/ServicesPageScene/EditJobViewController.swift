//
//  EditJobViewController.swift
//  PEXit
//
//  Created by Apple on 18/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class EditJobViewController: UIViewController , NavigationItemTapped{
     var controllerType : ControllerType.RawValue?
    override func viewDidLoad() {
        super.viewDidLoad()
        if self.parent is ServicesPageViewController{
            (self.parent as! ServicesPageViewController).delegate = self
        }
        // Do any additional setup after loading the view.
    }

    func navigationItemSelected(){
        switch ControllerType(rawValue: controllerType!)!{
        case .Jobs:
            navigateToNewJobController()
        
        default:
            return
        }
    }
    
    func navigateToNewJobController(){
        let postNewJobViewController = self.storyboard?.instantiateViewController(withIdentifier: "postNewJobViewController") as! PostNewJobViewController
        self.navigationController?.pushViewController(postNewJobViewController, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

   

}
