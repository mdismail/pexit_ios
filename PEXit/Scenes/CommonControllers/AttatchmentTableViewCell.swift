//
//  AttatchmentTableViewCell.swift
//  PEXit
//
//  Created by Apple on 08/08/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

protocol AttachmentDeleteTapped{
    func deleteTapped(sender : UIButton)
}

class AttatchmentTableViewCell: UITableViewCell {
    @IBOutlet weak var lblFileName: UILabel!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var imageViewFile: UIImageView!
    @IBOutlet weak var constraintlblLeading: NSLayoutConstraint!
    var delegate : AttachmentDeleteTapped?
//    var currentAttatchment : MediaAttatchment?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(currentAttatchment : MediaAttatchment){
        imageViewFile.image = nil
        lblFileName.text = currentAttatchment.fileName
        if currentAttatchment.fileData != nil{
            thumbnailView(currentAttatchment : currentAttatchment)
        }
       if imageViewFile.image == nil{
            constraintlblLeading.constant = 10
        }else{
           constraintlblLeading.constant = 66
        }
    }

    @IBAction func deleteAttatchmentClicked(_ sender: UIButton) {
        if let delegate = self.delegate{
            delegate.deleteTapped(sender: sender)
        }
    }
    
    func thumbnailView(currentAttatchment : MediaAttatchment){
        
        switch currentAttatchment.fileExtension{
        case "image/jpeg","image/png","image/jpg":
            imageViewFile.image = UIImage(data: currentAttatchment.fileData!)
        case "video/3gpp","video/3gpp","video/mp2t","video/mp4","video/mpeg","video/mpeg","video/quicktime","video/webm","video/x-flv",
             "video/x-m4v","video/x-mng","video/x-ms-asf","video/x-ms-asf","video/x-ms-wmv","video/x-msvideo" :
            imageViewFile.image = currentAttatchment.file as? UIImage
        case "application/vnd.openxmlformats-officedocument.presentationml.presentation" , "application/vnd.ms-powerpoint" :
            imageViewFile.image = UIImage(named:"pptPreview")
        case "application/pdf" :
            imageViewFile.image = UIImage(named:"pdf")
        case "text/plain","application/msword","application/vnd.openxmlformats-officedocument.wordprocessingml.document":
            imageViewFile.image = UIImage(named:"textPreview")
        default:
            imageViewFile.image = UIImage(named:"docsPreview")
            
        }
        
    }
}
