//
//  AlertController.swift
//  PEXit
//
//  Created by Apple on 08/08/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import Material

enum TypeOfLink {
    case Link
    case YouTubeLink
}

protocol SendLinkDelegate{
    func sendLinkText(link:String,type:TypeOfLink)
}

class AlertController: UIViewController {
    
    @IBOutlet weak var txtFieldLink: TextField!
    var delegate : SendLinkDelegate?
    var typeOfLink : TypeOfLink?
    var currentPostModel : PostRLMModel?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpTextField(typeOfField: typeOfLink!)
        if let currentPostModel = currentPostModel{
            setUpViewForEdit(typeOfField: typeOfLink!)
        }
        // Do any additional setup after loading the view.
    }
    
    func setUpViewForEdit(typeOfField : TypeOfLink){
        switch typeOfField{
        case .Link:
            txtFieldLink.text = currentPostModel?.link
        case .YouTubeLink:
            txtFieldLink.text = currentPostModel?.video
            
        }
    }
    
    func setUpTextField(typeOfField : TypeOfLink){
        txtFieldLink.detail = "You can add multiple links separated by comma(,)"
        switch typeOfField{
        case .Link:
            txtFieldLink.placeholder = "Link"
        case .YouTubeLink:
            txtFieldLink.placeholder = "Youtube Link"
        
        }
    }

    @IBAction func btnDoneClicked(_ sender: Any) {
        var result = false
        result = Utils.validateTextAndShowToastWithRequiredFields([txtFieldLink])
        if result == true{
            passLinkToAttachment()
        }
        
        
    }
    
    func passLinkToAttachment(){
        if let delegate = delegate{
            delegate.sendLinkText(link: txtFieldLink.text!,type:typeOfLink!)
        }
        self.dismissViewController()
    }
}
