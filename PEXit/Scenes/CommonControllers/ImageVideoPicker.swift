//
//  ImageVideoPicker.swift
//  SampleCleanSwift
//
//  Created by Apple on 27/07/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import MobileCoreServices
import Photos
import AVFoundation

enum MediaType{
    case Video
    case Image
}



class ImageVideoPicker: NSObject {
    static let imageVideoPicker = ImageVideoPicker()
    fileprivate var currentVC : UIViewController!
    var imagePickedBlock : ((MediaAttatchment) -> Void)?
    var videoPickedBlock : ((MediaAttatchment) -> Void)?
    func showImagePicker(controller : UIViewController ,mediaType : MediaType){

        currentVC = controller
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        if mediaType == .Video{
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
                imagePickerController.sourceType = .photoLibrary
                imagePickerController.mediaTypes = [kUTTypeMovie as String, kUTTypeVideo as String]
                
            }
            currentVC.present(imagePickerController,animated: true,completion: nil)
        }else{
            let actionsheet = UIAlertController(title: "Photo Source", message: "Choose A Sourece", preferredStyle: .actionSheet)
            actionsheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action:UIAlertAction)in
                if UIImagePickerController.isSourceTypeAvailable(.camera){
                    imagePickerController.sourceType = .camera
                    self.currentVC.present(imagePickerController, animated: true, completion: nil)
                }else
                {
                    print("Camera is Not Available")
                }
                
                
                
            }))
            actionsheet.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: { (action:UIAlertAction)in
                imagePickerController.sourceType = .photoLibrary
                self.currentVC.present(imagePickerController, animated: true, completion: nil)
            }))
            actionsheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
             currentVC.present(actionsheet,animated: true, completion: nil)
        }

    }
    
}

extension ImageVideoPicker : UIImagePickerControllerDelegate , UINavigationControllerDelegate {
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        var filename = ""
        var imageMimeType = "image/jpeg"
        if let asset = info["UIImagePickerControllerPHAsset"] as? PHAsset{
            if let fileName = asset.value(forKey: "filename") as? String{
                filename = fileName
            }
        }
        
        
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage{
            if filename.isEmpty{
                if let imageUrl = info[UIImagePickerControllerImageURL] as? URL{
                    filename = imageUrl.lastPathComponent
                    imageMimeType = imageUrl.mimeType()
                }
            }
            if filename == ""{
                filename = String.uniqueFilename(withPrefix: "Image")
            }
            let fixedOrientationImage = image.fixedOrientation()
            let mediaAttachment = MediaAttatchment(file: fixedOrientationImage, fileName: filename,fileData:nil,fileExtension:imageMimeType)
            self.imagePickedBlock?(mediaAttachment)
        }
        else{
            if let videoUrl = info[UIImagePickerControllerMediaURL] as? NSURL{
                print("videourl: ", videoUrl)
                //trying compression of video
                if filename.isEmpty{
                    if let videoUrl = info[UIImagePickerControllerMediaURL] as? URL{
                        filename = videoUrl.lastPathComponent
                    }
                }
                let video = MediaAttatchment(file: generateThumbnail(url:videoUrl as URL), fileName: filename,fileData:NSData(contentsOf: videoUrl as URL) as Data?,fileExtension:(videoUrl as URL).mimeType())
                
//                self.imagePickedBlock?(image)
                self.videoPickedBlock?(video)
//                let data = NSData(contentsOf: videoUrl as URL)!
//                print("File size before compression: \(Double(data.length / 1048576)) mb")
                //compressWithSessionStatusFunc(videoUrl)
            }
        }
        
        picker.dismiss(animated: true, completion: nil)
    }
    
    
    func generateThumbnail(url: URL) -> UIImage?{
        do {
            let videoUrl = url
            
            let asset = AVURLAsset(url: videoUrl, options: nil)
            let imgGenerator = AVAssetImageGenerator(asset: asset)
            
            let cgImage = try imgGenerator.copyCGImage(at: CMTimeMake(0, 1), actualTime: nil)
            
            // Rotate image before proceeding
            let uiImage = UIImage(cgImage: cgImage, scale: CGFloat(1.0), orientation: .right)
            
            
            
            return uiImage
            
        } catch let error as NSError {
            
            print("Error occurred: \(error)")
        }
        return nil
    }
   
    
    
}



