//
//  LocationViewController.swift
//  PEXit
//
//  Created by Apple on 17/09/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class LocationViewController: UIViewController , UITableViewDataSource, UITableViewDelegate{
    @IBOutlet var tblLocations: UITableView!
    //    @IBOutlet var searchFooter: SearchFooter!
    
    var locationArray = [JobLocationModel]()
    let searchController = UISearchController(searchResultsController: nil)
    var currentSearchText = ""
    var delegate : ItemSelected?
    var sender : UIViewController?
    override func viewDidLoad() {
        super.viewDidLoad()
        tblLocations.tableFooterView = UIView()
        // Setup the Search Controller
        searchController.searchResultsUpdater = self
        //        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search Location"
        //        navigationItem.searchController = searchController
        //         definesPresentationContext = true
        searchController.searchBar.scopeButtonTitles = []
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchBar.delegate = self
        //  tblLocations.layout
        tblLocations.tableHeaderView = searchController.searchBar
    }
    
    @ objc func fetchLocations(){
        let worker = JobWorker()
        worker.fetchJobLocations(jobStr: searchController.searchBar.text!) { (response, error) in
            if response != nil && response?.status == true{
                self.locationArray = (response?.datas)!
                DispatchQueue.main.async{
                    self.tblLocations.reloadData()
                }
            }
        }
    }
    
    @ objc func fetchProductLocations(){
        let worker = EMarketProductWorker()
        worker.fetchProductLocations(jobStr: searchController.searchBar.text!) { (response, error) in
            if response != nil && response?.status == true{
                self.locationArray = (response?.datas)!
                DispatchQueue.main.async{
                    self.tblLocations.reloadData()
                }
            }
        }
    }
    
    @objc func fetchServiceLocations(){
        let worker = EMarketSoftwareWorker()
        worker.fetchServiceLocations(softwareStr: searchController.searchBar.text!) { (response, error) in
            if response != nil && response?.status == true{
                self.locationArray = (response?.datas)!
                DispatchQueue.main.async{
                    self.tblLocations.reloadData()
                }
            }
        }
    }
    
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        print("Did not come here")
        searchController.isActive = false
        searchController.dismiss(animated: false, completion: nil)
    }
    deinit {
        updateView()
    }
    
    func updateView(){
        //        self.searchController.loadViewIfNeeded()    // iOS 9
        //        let _ = self.searchController.view
    }
    
    //    override func viewWillDisappear(_ animated: Bool) {
    //        dismiss(animated: searchController, completion: nil)
    //    }
    
    // MARK: - Table View
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return locationArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "locationCell", for: indexPath)
        cell.textLabel!.text = locationArray[indexPath.row].name
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedLocation = locationArray[indexPath.row].name
        if let delegate = delegate{
            self.navigationController?.popViewController(animated: true)
            delegate.optionSelected(option: selectedLocation ?? "")
            
        }
    }
}

extension LocationViewController: UISearchBarDelegate {
    // MARK: - UISearchBar Delegate
    //    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
    //        filterContentForSearchText(searchBar.text!, scope: searchBar.scopeButtonTitles![selectedScope])
    //    }
}

extension LocationViewController: UISearchResultsUpdating {
    // MARK: - UISearchResultsUpdating Delegate
    func updateSearchResults(for searchController: UISearchController) {
        currentSearchText = searchController.searchBar.text!
        if sender is EMarketProductViewController{
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(fetchProductLocations), object: nil)
            self.perform(#selector(fetchProductLocations), with: nil, afterDelay: 0.5)
        }
        else if sender is EMarketSSTSoftwareViewController{
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(fetchServiceLocations), object: nil)
            self.perform(#selector(fetchServiceLocations), with: nil, afterDelay: 0.5)
        }
        else{
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(fetchLocations), object: nil)
            self.perform(#selector(fetchLocations), with: nil, afterDelay: 0.5)
        }
        
    }
}
