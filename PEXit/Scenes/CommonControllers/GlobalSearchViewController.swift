//
//  GlobalSearchViewController.swift
//  PEXit
//
//  Created by ats on 15/11/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
let GLOBAL_SEARCH_CELL = 50

class GlobalSearchViewController: UIViewController , UITableViewDataSource , UITableViewDelegate, UISearchBarDelegate{
    @IBOutlet var tblSearchOptions: UITableView!
    @IBOutlet weak var globalSearchBar: UISearchBar!
    
    @IBOutlet weak var lblPreviousSearch: UILabel!
    @IBOutlet weak var btnGlobalSearch: UIButton!
    @IBOutlet weak var constraintTblViewHeight: NSLayoutConstraint!
    var selectedIndex : IndexPath?
    var searchOptionArray = ["Posts","Connections","Jobs","Products","Services","Softwares","Product Suppliers"]
    var searchWithinResult = false
    var searchOption = ""
   
    override func viewDidLoad() {
        super.viewDidLoad()
        constraintTblViewHeight.constant = CGFloat(GLOBAL_SEARCH_CELL * searchOptionArray.count)
        globalSearchBar.delegate = self
      
//        globalSearchBar.showsSear
        tblSearchOptions.reloadData()
       // Do any additional setup after loading the view.
    }

    override func viewDidAppear(_ animated : Bool){
//         globalSearchBar.customizeSearchBar(placeholderStr:"Search Here | Yellow Pages",rightImageStr:"Search",color: .white,placeholderColor:.white)
         globalSearchBar.delegate = self
    }
    
    override func viewWillAppear(_ animated : Bool){
        if CURRENT_GLOBAL_SEARCH_OPTION != ""  && CURRENT_GLOBAL_SEARCH_KEYWORD != ""{
            lblPreviousSearch.isHidden = false
            lblPreviousSearch.text = "Searched Items: [" + CURRENT_GLOBAL_SEARCH_OPTION + "] -> " + CURRENT_GLOBAL_SEARCH_KEYWORD
        }else{
            lblPreviousSearch.isHidden = true
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchOptionArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "globalSearchCell", for: indexPath) as! GlobalSearchTableViewCell
        cell.lblSearchOption.text = searchOptionArray[indexPath.row]
        cell.btnSelect.tag = indexPath.row
        cell.btnSelect.isSelected = false
        cell.btnSelect.addTarget(self,action: #selector(searchOptionSelected(sender:)),for:.touchUpInside)
        cell.btnSelect.setImage(UIImage.init(named: "circle"), for: .normal)
        if indexPath == selectedIndex{
            cell.btnSelect.isSelected = true
            cell.btnSelect.setImage(UIImage.init(named: "select"), for: .normal)
        }else{
            cell.btnSelect.isSelected = false
            cell.btnSelect.setImage(UIImage.init(named: "circle"), for: .normal)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(GLOBAL_SEARCH_CELL)
    }
    
    @objc func searchOptionSelected(sender : UIButton){
        sender.isSelected = !sender.isSelected
        selectedIndex = IndexPath.init(item: sender.tag, section: 0)
        
        let currentCell = tblSearchOptions.cellForRow(at: selectedIndex!) as! GlobalSearchTableViewCell
        if CURRENT_GLOBAL_SEARCH_OPTION != ""  && CURRENT_GLOBAL_SEARCH_KEYWORD != "" {
            if currentCell.lblSearchOption.text! == CURRENT_GLOBAL_SEARCH_OPTION{
                btnGlobalSearch.isHidden = false
               // btnGlobalSearch.blink()
            } else {
                btnGlobalSearch.isHidden = true
                //CURRENT_GLOBAL_SEARCH_KEYWORD = ""
            }
        }
        searchOption = currentCell.lblSearchOption.text!
    //    optionChoosen(option: currentCell.lblSearchOption.text!)
        tblSearchOptions.reloadData()
        
    }
//    func optionChoosen(option : String){
//        if !(globalSearchBar.text?.isEmpty)!{
//            passSelectedValues()
//        }
//        else{
//            displayToast(ENTER_SEARCH_KEYWORDS)
//            return
//        }
//    }
    
    @IBAction func searchWIthinResultClicked(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.isSelected{
             searchWithinResult = true
        }else{
             searchWithinResult = false
        }
       
    }
    @IBAction func btnCloseClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func passSelectedValues(){
        var searchText = globalSearchBar.text
        if searchWithinResult == true{
            CURRENT_GLOBAL_SEARCH_KEYWORD = "\(CURRENT_GLOBAL_SEARCH_KEYWORD),\(searchText!)"
        }else{
            CURRENT_GLOBAL_SEARCH_KEYWORD = searchText ?? ""
        }
        print("Current search text is --->", searchText!)
        globalSearchBarHandle()
        dismiss(animated: true, completion: nil)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
        if (globalSearchBar.text?.isEmpty)!{
            displayToast(ENTER_SEARCH_KEYWORDS)
            return
        }
        if searchOption == ""{
            displayToast(CHOOSE_OPTION)
            return
        }
        passSelectedValues()
    }
    func globalSearchBarHandle(){
        let tabBarController = UIApplication.shared.keyWindow?.rootViewController as! UITabBarController
        
        if let controller = getBaseControllerByIndex(tabBarController: tabBarController){
            controller.cleanUpGlobalSearch(previousSearchOption : CURRENT_GLOBAL_SEARCH_OPTION)
        }
        CURRENT_GLOBAL_SEARCH_OPTION = searchOption
        
        switch CURRENT_GLOBAL_SEARCH_OPTION{
        case "Posts":
            tabBarController.selectedIndex = 0
        case "Connections":
            tabBarController.selectedIndex = 4
        case "Jobs":
            tabBarController.selectedIndex = 1
        case "Products":
            tabBarController.selectedIndex = 2
        case "Services":
            tabBarController.selectedIndex = 2
        case "Softwares":
            tabBarController.selectedIndex = 2
        case "Product Suppliers":
            tabBarController.selectedIndex = 2
        default:
            break
        }

        if let controller = getBaseControllerByIndex(tabBarController: tabBarController){
            controller.searchLocallyUsingGlobal(searchKeyword: CURRENT_GLOBAL_SEARCH_KEYWORD, searchOptions : CURRENT_GLOBAL_SEARCH_OPTION)
        }
        
    }
    
    func getBaseControllerByIndex(tabBarController : UITabBarController) -> UIViewController?{
        let vc = tabBarController.viewControllers![tabBarController.selectedIndex]
        if let controller = vc as? UINavigationController{
            if controller.viewControllers.count > 0{
                let rootController = controller.viewControllers[0]
                return(rootController)
            }
        }
        return nil
    }
}
