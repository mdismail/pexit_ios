//
//  AttatchmentViewController.swift
//  PEXit
//
//  Created by Apple on 07/08/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import MobileCoreServices
import Presentr

let ATTATCHMENT_CELL_HEIGHT = 85

protocol PassAttatchmentsToController{
    func sendAttatchments(imageArray : [MediaAttatchment]?,videoArray:[MediaAttatchment]?,fileArray:[MediaAttatchment]?,pptArray:[MediaAttatchment]?,linksString:String?,youTubeLinkString:String?)
    func updateHeightOfParentController(height : CGFloat)
}

extension PassAttatchmentsToController{
    func updateHeightOfParentController(height : CGFloat){
        
    }
    func sendAttatchments(imageArray: [MediaAttatchment]?, videoArray: [MediaAttatchment]?, fileArray: [MediaAttatchment]?, pptArray: [MediaAttatchment]?, linksString: String?, youTubeLinkString: String?) {
        
    }
}

struct MediaAttatchment{
    var file : Any?
    var fileName : String?
    var fileData : Data?
    var fileExtension : String?
    
}


class AttatchmentViewController: UIViewController , SendLinkDelegate , AttachmentDeleteTapped , DeleteCurrentAttatchment{
    
    
    @IBOutlet weak var btnImageAttatchment: UIButton!
    @IBOutlet weak var btnFileAttatchment: UIButton!
    @IBOutlet weak var btnLinkAttatchment: UIButton!
    @IBOutlet weak var btnYouTubeLinkAttatchment: UIButton!
    @IBOutlet weak var btnVideoAttatchment: UIButton!
    @IBOutlet weak var btnPost: UIButton!
    @IBOutlet weak var btnPptAttatchment: UIButton!
    @IBOutlet weak var tblAttatchment: UITableView!
    @IBOutlet weak var btnAddNew: UIButton!
    @IBOutlet weak var collectionViewPreviousFiles: UICollectionView!
    @IBOutlet weak var heightCollectionViewPreviousFiles: NSLayoutConstraint!
    @IBOutlet weak var heightAttatchmentTblConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var stackViewButtons: UIStackView!
    var delegate : PassAttatchmentsToController?
    var linksArray = [MediaAttatchment]()
    var youTubeLinksArray = [MediaAttatchment]()
    var imageNamesArray = [MediaAttatchment]()
    var imageDataArray = [Data]()
    var videoArray = [MediaAttatchment]()
    var currentArray = [MediaAttatchment]()
    var pptArray = [MediaAttatchment]()
    var fileArray = [MediaAttatchment]()
    var previousFilesArray = [String]()
    var currentButton : UIButton?
    var isFromEdit = false
    
    var isFromShare = false
    var currentPostModel : PostRLMModel?
    var currentProfileModel : AnyObject?
    var workProfileType : String?
    var isFromAds = false
    var adsRequestModel : NewAdsRequestModel?
    var serviceType : String?
    
    var currentProductModel : ServiceProductModel?
    var currentSponsorModel : SponsorModel?
//    var currentProductModel : ServiceProductModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblAttatchment.tableFooterView = UIView()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        initialSetUp()
    }
    
    func initialSetUp(){
        btnImageAttatchment.tag = BUTTON_TAGS.IMAGEATTATCHMENT.rawValue
        btnFileAttatchment.tag = BUTTON_TAGS.FILEATTATCHMENT.rawValue
        btnLinkAttatchment.tag = BUTTON_TAGS.LINKATTATCHMENT.rawValue
        btnYouTubeLinkAttatchment.tag = BUTTON_TAGS.YOUTUBELINKATTATCHMENT.rawValue
        btnVideoAttatchment.tag = BUTTON_TAGS.VIDEOATTATCHMENT.rawValue
        btnPptAttatchment.tag = BUTTON_TAGS.PPTATTATCHMENT.rawValue
        collectionViewPreviousFiles.register(UINib(nibName: "HomeDetailCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "homeDetailsCell")
        if isFromEdit{
            btnPost.setTitle("Save", for: .normal)
            //            collectionViewPreviousFiles.register(UINib(nibName: "HomeDetailCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "homeDetailsCell")
        }
        if currentProfileModel != nil{
            //            collectionViewPreviousFiles.register(UINib(nibName: "HomeDetailCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "homeDetailsCell")
        }
        if isFromShare{
            btnPost.setTitle("Share", for: .normal)
        }
    }
    
    
    func removeAllElements(){
        imageNamesArray.removeAll()
        youTubeLinksArray.removeAll()
        pptArray.removeAll()
        fileArray.removeAll()
        currentArray.removeAll()
        linksArray.removeAll()
        btnAddNew.isHidden = true
        for attachmentBtn in stackViewButtons.subviews{
            attachmentBtn.backgroundColor = .clear
        }
        previousFilesArray.removeAll()
        collectionViewPreviousFiles.reloadData()
        tblAttatchment.reloadData()
        self.delegate?.updateHeightOfParentController(height: self.heightCollectionViewPreviousFiles.constant + CGFloat(ATTATCHMENT_CELL_HEIGHT * self.currentArray.count))
    }
    
    
    @IBAction func btnAttatchmentClicked(_ sender: UIButton) {
        for attachmentBtn in stackViewButtons.subviews{
            attachmentBtn.backgroundColor = .clear
        }
        sender.backgroundColor = SELECTION_BLUE_COLOR
        currentArray.removeAll()
        previousFilesArray.removeAll()
        collectionViewPreviousFiles.reloadData()
        btnAddNew.isHidden = true
        currentButton = sender
        
        showPicker(currentButton: sender)
        if currentArray.count > 0 || previousFilesArray.count > 0{
            if !isFromAds{
                btnAddNew.isHidden = false
            }
            
        }
        if previousFilesArray.count > 0{
            if sender.tag == BUTTON_TAGS.LINKATTATCHMENT.rawValue || sender.tag == BUTTON_TAGS.YOUTUBELINKATTATCHMENT.rawValue{
                if let layout = collectionViewPreviousFiles.collectionViewLayout as? UICollectionViewFlowLayout {
                    layout.scrollDirection = .vertical
                }

                heightCollectionViewPreviousFiles.constant = CGFloat(previousFilesArray.count * 30)
               
                print("Height --->",heightCollectionViewPreviousFiles.constant)

            }else{
                if let layout = collectionViewPreviousFiles.collectionViewLayout as? UICollectionViewFlowLayout {
                    layout.scrollDirection = .horizontal
                }

                heightCollectionViewPreviousFiles.constant = 100

            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                self.collectionViewPreviousFiles.reloadData()
            })
        }else{
            print("In else")
            heightCollectionViewPreviousFiles.constant = 0
        }
        self.delegate?.updateHeightOfParentController(height: self.heightCollectionViewPreviousFiles.constant + CGFloat(ATTATCHMENT_CELL_HEIGHT * self.currentArray.count) + 50)
        tblAttatchment.reloadData()
    }
    
    
    func showPicker(currentButton : UIButton ,isAddNew : Bool = false){
        switch BUTTON_TAGS(rawValue: currentButton.tag)!   {
        case .IMAGEATTATCHMENT:
            currentArray = imageNamesArray
            updateHeightConstraints()
            if isFromEdit{
                previousFilesArray = (currentPostModel?.image)!
            }
            if currentProfileModel != nil{
                switch workProfileType{
                case WORK_PROFILE_TYPE.Experience.rawValue:
                    previousFilesArray = ((currentProfileModel as? ExperienceModel)?.pexpphot)!
                case WORK_PROFILE_TYPE.Education.rawValue:
                    previousFilesArray = ((currentProfileModel as? EducationModel)?.peduphot)!
                case WORK_PROFILE_TYPE.Honors.rawValue:
                    previousFilesArray = ((currentProfileModel as? HonorsModel)?.phonphot)!
                default:
                    break
                }
            }
            if previousFilesArray.count > 0 && !isAddNew{
                return
            }
            if currentArray.count == 0 || isAddNew{
                attatchImage()
            }
        case .VIDEOATTATCHMENT:
            currentArray = videoArray
            updateHeightConstraints()
            if isFromEdit{
                previousFilesArray = (currentPostModel?.allvid)!
            }
            if previousFilesArray.count > 0 && !isAddNew{
                return
            }
            if currentArray.count == 0 || isAddNew {
                attatchVideo()
            }
        case .FILEATTATCHMENT:
            currentArray = fileArray
            updateHeightConstraints()
            if isFromEdit{
                previousFilesArray = (currentPostModel?.docu)!
            }
            if currentProfileModel != nil{
                switch workProfileType{
                case WORK_PROFILE_TYPE.Experience.rawValue:
                    previousFilesArray = ((currentProfileModel as? ExperienceModel)?.pexpfle)!
                case WORK_PROFILE_TYPE.Education.rawValue:
                    previousFilesArray = ((currentProfileModel as? EducationModel)?.pedufle)!
                case WORK_PROFILE_TYPE.Honors.rawValue:
                    previousFilesArray = ((currentProfileModel as? HonorsModel)?.phonfle)!
                default:
                    break
                }
            }
            if previousFilesArray.count > 0 && !isAddNew{
                return
            }
            if currentArray.count == 0 || isAddNew{
                attatchFile()
            }
        case .PPTATTATCHMENT:
            currentArray = pptArray
            updateHeightConstraints()
            if isFromEdit{
                previousFilesArray = (currentPostModel?.ppt)!
            }
            if currentProfileModel != nil{
                switch workProfileType{
                case WORK_PROFILE_TYPE.Experience.rawValue:
                    previousFilesArray = ((currentProfileModel as? ExperienceModel)?.pexpprest)!
                case WORK_PROFILE_TYPE.Education.rawValue:
                    previousFilesArray = ((currentProfileModel as? EducationModel)?.peduprest)!
                case WORK_PROFILE_TYPE.Honors.rawValue:
                    previousFilesArray = ((currentProfileModel as? HonorsModel)?.phonprest)!
                default:
                    break
                }
            }
            if previousFilesArray.count > 0 && !isAddNew{
                return
            }
            if currentArray.count == 0 || isAddNew {
                attatchPpt()
            }
        case .YOUTUBELINKATTATCHMENT:
            currentArray = youTubeLinksArray
            updateHeightConstraints()
            if isFromEdit{
//                if currentPostModel?.youtube != nil && !(currentPostModel?.youtube?.isEmpty)!{
//                    previousFilesArray = ((currentPostModel?.youtube)?.components(separatedBy: ","))!
//
//                }
//                else
                    if currentPostModel?.video != nil && !(currentPostModel?.video?.isEmpty)!{
                    previousFilesArray = ((currentPostModel?.video)?.components(separatedBy: ","))!
                }
            }
            if currentProfileModel != nil{
                switch workProfileType{
                case WORK_PROFILE_TYPE.Experience.rawValue:
                    if !((currentProfileModel as? ExperienceModel)?.pexpvdeo?.isEmpty)!{
                        previousFilesArray = (((currentProfileModel as? ExperienceModel)?.pexpvdeo)?.components(separatedBy: ","))!
                        if previousFilesArray.count > 0 && !isAddNew{
                            return
                        }
                    }
                case WORK_PROFILE_TYPE.Education.rawValue:
                    if !((currentProfileModel as? EducationModel)?.peduvdeo?.isEmpty)!{
                        previousFilesArray = (((currentProfileModel as? EducationModel)?.peduvdeo)?.components(separatedBy: ","))!
                        if previousFilesArray.count > 0 && !isAddNew{
                            return
                        }
                    }
                case WORK_PROFILE_TYPE.Honors.rawValue:
                    if !((currentProfileModel as? HonorsModel)?.phonvdeo?.isEmpty)!{
                        previousFilesArray = (((currentProfileModel as? HonorsModel)?.phonvdeo)?.components(separatedBy: ","))!
                        if previousFilesArray.count > 0 && !isAddNew{
                            return
                        }
                    }
                default:
                    break
                }
            }
            if previousFilesArray.count > 0 && !isAddNew{
                return
            }
            
            if currentArray.count == 0 || isAddNew{
                showAlertController(type:.YouTubeLink)
            }
        case .LINKATTATCHMENT:
            currentArray = linksArray
            updateHeightConstraints()
            if isFromEdit{
                if currentPostModel?.link != nil && !(currentPostModel?.link?.isEmpty)!{
                    previousFilesArray = ((currentPostModel?.link)?.components(separatedBy: ","))!
                    if previousFilesArray.count > 0 && !isAddNew{
                        return
                    }
                }
            }
            
            if currentProfileModel != nil{
                switch workProfileType{
                case WORK_PROFILE_TYPE.Experience.rawValue:
                    if !((currentProfileModel as? ExperienceModel)?.pexplnk?.isEmpty)!{
                        previousFilesArray = (((currentProfileModel as? ExperienceModel)?.pexplnk)?.components(separatedBy: ","))!
                        if previousFilesArray.count > 0 && !isAddNew{
                            return
                        }
                    }
                case WORK_PROFILE_TYPE.Education.rawValue:
                    if !((currentProfileModel as? EducationModel)?.pedulnk?.isEmpty)!{
                        previousFilesArray = (((currentProfileModel as? EducationModel)?.pedulnk)?.components(separatedBy: ","))!
                        if previousFilesArray.count > 0 && !isAddNew{
                            return
                        }
                    }
                case WORK_PROFILE_TYPE.Honors.rawValue:
                    if !((currentProfileModel as? HonorsModel)?.phonlnk?.isEmpty)!{
                        previousFilesArray = (((currentProfileModel as? HonorsModel)?.phonlnk)?.components(separatedBy: ","))!
                        if previousFilesArray.count > 0 && !isAddNew{
                            return
                        }
                    }
                default:
                    break
                }
            }
            
            if currentArray.count == 0 || isAddNew{
                showAlertController(type:.Link)
            }
            
        default: break
        }
        
    }
    
    @IBAction func btnAddNewClicked(_ sender: UIButton) {
        showPicker(currentButton: currentButton!,isAddNew : true)
        
    }
    
    @IBAction func postAttatchmentClicked(_ sender: Any) {
        var linkString = linksArray.map{$0.fileName!}.joined(separator: ",")
        if !linkString.isEmpty{
             linkString = [linkString,currentPostModel?.link].compactMap{$0}.joined(separator: ",")
        }else{
            linkString = currentPostModel?.link ?? ""
        }
       
        print("NEW links string --->",linkString)
        var youTubeString = youTubeLinksArray.map{$0.fileName!}.joined(separator: ",")
//        if !youTubeString.isEmpty && currentPostModel?.youtube != nil{
//             youTubeString = [youTubeString,currentPostModel?.youtube].compactMap{$0}.joined(separator: ",")
//        }else{
//            youTubeString = currentPostModel?.youtube ?? ""
//        }
        if !youTubeString.isEmpty {
            youTubeString = [youTubeString,currentPostModel?.video].compactMap{$0}.joined(separator: ",")
        }else{
            youTubeString = currentPostModel?.video ?? ""
        }
       
        print("NEW links string --->",youTubeString)
        delegate?.sendAttatchments(imageArray: imageNamesArray, videoArray: videoArray, fileArray: fileArray, pptArray: pptArray, linksString: linkString, youTubeLinkString: youTubeString)
    }
    
    func attatchVideo(){
        ImageVideoPicker.imageVideoPicker.showImagePicker(controller: self, mediaType: .Video)
        ImageVideoPicker.imageVideoPicker.videoPickedBlock = {[unowned self] (videoFile) in
            self.btnAddNew.isHidden = false
            let newIndexPath = IndexPath(item: 0, section: 0)
            self.videoArray.insert(MediaAttatchment(file: videoFile.file, fileName: videoFile.fileName,fileData:videoFile.fileData,fileExtension:videoFile.fileExtension), at: 0)
            self.currentArray = self.videoArray
            self.tblAttatchment.insertRows(at: [newIndexPath], with: .automatic)
            self.updateHeightConstraints()
        }
    }
    
    func attatchFile() {
        // Attatch Files
        FileAttatchmentHandler.shared.showDocumentPicker(vc: self, files: [String(kUTTypePDF),String(kUTTypeRTF),String(kUTTypePlainText)])
        FileAttatchmentHandler.shared.filePickedBlock = {[unowned self] (url) in
//            print(url)
            self.btnAddNew.isHidden = false
//            print("File Name is ---->",url.lastPathComponent)
            let newIndexPath = IndexPath(item: 0, section: 0)
            do {
                let data = try Data(contentsOf: url, options: .mappedIfSafe)
                print(data)
                self.fileArray.insert((MediaAttatchment(file: nil, fileName: url.lastPathComponent,fileData:data,fileExtension:url.mimeType())), at: 0)
                self.currentArray = self.fileArray
                self.tblAttatchment.insertRows(at: [newIndexPath], with: .automatic)
                self.updateHeightConstraints()
                //  here you can see data bytes of selected video, this data object is upload to server by multipartFormData upload
            } catch  {
            }
            //            self.fileArray.insert(url.lastPathComponent, at: 0)
            //            self.currentArray = self.fileArray
            
        }
    }
    
    func attatchPpt() {
        FileAttatchmentHandler.shared.showDocumentPicker(vc: self, files: ["org.openxmlformats.presentationml.presentation","public.presentation"])
        FileAttatchmentHandler.shared.filePickedBlock = {[unowned self] (url) in
            print(url)
            self.btnAddNew.isHidden = false
            print("File Name is ---->",url.lastPathComponent)
            let newIndexPath = IndexPath(item: 0, section: 0)
            do {
                let data = try Data(contentsOf: url, options: .mappedIfSafe)
                self.pptArray.insert((MediaAttatchment(file: nil, fileName: url.lastPathComponent,fileData:data,fileExtension:url.mimeType())), at: 0)
                self.currentArray = self.pptArray
                self.tblAttatchment.insertRows(at: [newIndexPath], with: .automatic)
                self.updateHeightConstraints()
            }
            catch  {
            }
        }
    }
    
    func attatchImage() {
        ImageVideoPicker.imageVideoPicker.showImagePicker(controller: self, mediaType: .Image)
        ImageVideoPicker.imageVideoPicker.imagePickedBlock = { [unowned self](imageData) in
            if !self.isFromAds{
                self.btnAddNew.isHidden = false
            }
            
            let newIndexPath = IndexPath(item: 0, section: 0)
            self.imageNamesArray.insert(MediaAttatchment(file: imageData.file, fileName: imageData.fileName,fileData:UIImageJPEGRepresentation(imageData.file as! UIImage, 0.4),fileExtension:imageData.fileExtension), at: 0)
            var count = 0
            //            for media in self.imageNamesArray{
            //                media.fileName = media.fileName?.append(\count)
            //            }
            // self.imageNamesArray = self.imageNamesArray {$0.fileName}.append("test")
            self.currentArray = self.imageNamesArray
            self.tblAttatchment.insertRows(at: [newIndexPath], with: .automatic)
            self.updateHeightConstraints()
        }
        
    }
    
    
    func updateHeightConstraints(){
        self.heightAttatchmentTblConstraint.constant = CGFloat(ATTATCHMENT_CELL_HEIGHT * self.currentArray.count)
        self.delegate?.updateHeightOfParentController(height: self.heightCollectionViewPreviousFiles.constant + CGFloat(ATTATCHMENT_CELL_HEIGHT * self.currentArray.count) + 50)
    }
    
    func showAlertController(type:TypeOfLink){
        DispatchQueue.main.async { [weak self] in
            let presenter = Presentr(presentationType: .alert)
            let controller = self?.storyboard?.instantiateViewController(withIdentifier: "alertController") as! AlertController
            controller.delegate = self
            controller.typeOfLink = type
            //                controller.currentPostModel = self?.currentPostModel
            self?.customPresentViewController(presenter, viewController: controller, animated: true)
        }
    }
    
    func sendLinkText(link: String,type:TypeOfLink) {
        btnAddNew.isHidden = false
        var indexPathArray = [IndexPath]()
        let linksList = link.components(separatedBy: ",")
        
        
        if type == .Link{
            for i in 0..<linksList.count{
                indexPathArray.append(IndexPath(item: i, section: 0))
                linksArray.insert(MediaAttatchment(file: nil, fileName: linksList[i],fileData:nil,fileExtension:"link"),at: 0)
                
            }
            
            currentArray = linksArray
            
        }
        else{
            for i in 0..<linksList.count{
                indexPathArray.append(IndexPath(item: i, section: 0))
                youTubeLinksArray.insert(MediaAttatchment(file: nil, fileName: linksList[i],fileData:nil,fileExtension:"youtube"), at: 0)
            }
            currentArray = youTubeLinksArray
        }
        tblAttatchment.insertRows(at: indexPathArray, with: .automatic)
        self.updateHeightConstraints()
        
    }
}


extension AttatchmentViewController : UITableViewDataSource , UITableViewDelegate , UICollectionViewDataSource , UICollectionViewDelegate , UICollectionViewDelegateFlowLayout{
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let listCell = tableView.dequeueReusableCell(withIdentifier: "attatchmentListCell", for: indexPath) as! AttatchmentTableViewCell
        let currentAttactchement = currentArray[indexPath.row]
        listCell.btnDelete.tag = indexPath.row
        listCell.delegate = self
        listCell.setData(currentAttatchment : currentAttactchement)
        return listCell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currentArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 85
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return previousFilesArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "homeDetailsCell", for: indexPath) as! HomeDetailCollectionViewCell
        cell.btnDelete.isHidden = false
        cell.btnDelete.tag = indexPath.row
        cell.delegate = self
        cell.lblLink.textAlignment = .left
        switch BUTTON_TAGS(rawValue: (currentButton?.tag)!)!   {
        case .IMAGEATTATCHMENT:
            if isFromEdit{
                cell.setUpViewForImage(url:currentPostModel?.image[indexPath.row])
            }
            if currentProfileModel != nil{
                switch workProfileType{
                case WORK_PROFILE_TYPE.Experience.rawValue:
                    cell.setUpViewForImage(url:(currentProfileModel as? ExperienceModel)?.pexpphot[indexPath.row])
                case WORK_PROFILE_TYPE.Education.rawValue:
                    cell.setUpViewForImage(url:(currentProfileModel as? EducationModel)?.peduphot[indexPath.row])
                case WORK_PROFILE_TYPE.Honors.rawValue:
                    cell.setUpViewForImage(url:(currentProfileModel as? HonorsModel)?.phonphot[indexPath.row])
                default:
                    break
                }
            }
            
        case .FILEATTATCHMENT:
            cell.setUpViewForImage(isFile : true)
        case .VIDEOATTATCHMENT:
            cell.setUpViewForVideo(videoUrl: currentPostModel?.allvid[indexPath.row])
        case .PPTATTATCHMENT:
            cell.setUpViewForImage(isPresentation: true)
        case .LINKATTATCHMENT , .YOUTUBELINKATTATCHMENT:
           
            cell.btnDelete.isHidden = true
            cell.setUpViewForLink(link: previousFilesArray[indexPath.row])
        default:
            print("Default")
        }
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == collectionViewPreviousFiles{
            
            
            switch BUTTON_TAGS(rawValue: (currentButton?.tag)!)!   {
            case .IMAGEATTATCHMENT:
                if isFromEdit{
                    let photoController = AppToolbarController(rootViewController: PhotoViewController(imagesArray:(currentPostModel?.image)!,index: indexPath.row))
                    present(photoController, animated: true, completion: nil)
                }
                if currentProfileModel != nil{
                    switch workProfileType{
                    case WORK_PROFILE_TYPE.Experience.rawValue:
                        let photoController = AppToolbarController(rootViewController: PhotoViewController(imagesArray:((currentProfileModel as? ExperienceModel)?.pexpphot)!,index: indexPath.row))
                        present(photoController, animated: true, completion: nil)
                    case WORK_PROFILE_TYPE.Education.rawValue:
                        let photoController = AppToolbarController(rootViewController: PhotoViewController(imagesArray:((currentProfileModel as? EducationModel)?.peduphot)!,index: indexPath.row))
                        present(photoController, animated: true, completion: nil)
                    case WORK_PROFILE_TYPE.Honors.rawValue:
                        let photoController = AppToolbarController(rootViewController: PhotoViewController(imagesArray:((currentProfileModel as? HonorsModel)?.phonphot)!,index: indexPath.row))
                        present(photoController, animated: true, completion: nil)
                    default:
                        break
                    }
                }
                
            case .FILEATTATCHMENT:
                if isFromEdit{
                    showAttatchmentView(mediaUrl:(currentPostModel?.docu[indexPath.row])!)
                }
                if currentProfileModel != nil{
                    switch workProfileType{
                    case WORK_PROFILE_TYPE.Experience.rawValue:
                        showAttatchmentView(mediaUrl: (((currentProfileModel as? ExperienceModel)?.pexpfle)![indexPath.row]))
                    case WORK_PROFILE_TYPE.Education.rawValue:
                        showAttatchmentView(mediaUrl: (((currentProfileModel as? EducationModel)?.pedufle)![indexPath.row]))
                    case WORK_PROFILE_TYPE.Honors.rawValue:
                        showAttatchmentView(mediaUrl: (((currentProfileModel as? HonorsModel)?.phonfle)![indexPath.row]))
                    default:
                        break
                    }
                }
                
            case .VIDEOATTATCHMENT:
                openVideoPlayer(videoUrl:(currentPostModel?.allvid[indexPath.row])!)
            case .PPTATTATCHMENT:
                if isFromEdit{
                    showAttatchmentView(mediaUrl:(currentPostModel?.ppt[indexPath.row])!)
                }
                if currentProfileModel != nil{
                    switch workProfileType{
                    case WORK_PROFILE_TYPE.Experience.rawValue:
                        showAttatchmentView(mediaUrl: (((currentProfileModel as? ExperienceModel)?.pexpprest)![indexPath.row]))
                    case WORK_PROFILE_TYPE.Education.rawValue:
                        showAttatchmentView(mediaUrl: (((currentProfileModel as? EducationModel)?.peduprest)![indexPath.row]))
                    case WORK_PROFILE_TYPE.Honors.rawValue:
                        showAttatchmentView(mediaUrl: (((currentProfileModel as? HonorsModel)?.phonprest)![indexPath.row]))
                    default:
                        break
                    }
                }
                
            case .LINKATTATCHMENT , .YOUTUBELINKATTATCHMENT:
                openLinks(linkUrl:previousFilesArray[indexPath.row])
            default:
                print("Default")
            }
            
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch BUTTON_TAGS(rawValue: (currentButton?.tag)!)!   {
        case .IMAGEATTATCHMENT , .FILEATTATCHMENT , .VIDEOATTATCHMENT ,.PPTATTATCHMENT:
            return CGSize.init(width: collectionView.frame.size.width / 3, height: collectionView.frame.size.height)
            
        case .LINKATTATCHMENT , .YOUTUBELINKATTATCHMENT:
//            let size = (previousFilesArray[indexPath.row]).size(withAttributes: nil)
            return CGSize(width:collectionView.frame.size.width,height:30)
//        case .YOUTUBELINKATTATCHMENT:
//            let size = (previousFilesArray[indexPath.row]).size(withAttributes: nil)
//            return CGSize(width:size.width + 50,height:collectionView.frame.size.height)
        default:
            return CGSize.init(width: collectionView.frame.size.width / 3, height: collectionView.frame.size.height)
        }
        
    }
    
    func deleteTapped(sender : UIButton){
        currentArray.remove(at: sender.tag)
        if currentArray.count == 0{
            btnAddNew.isHidden = true
        }
        if previousFilesArray.count > 0{
            btnAddNew.isHidden = false
        }
        switch BUTTON_TAGS(rawValue: currentButton!.tag)!   {
        case .IMAGEATTATCHMENT:
            imageNamesArray.remove(at: sender.tag)
        case .VIDEOATTATCHMENT:
            videoArray.remove(at: sender.tag)
        case .FILEATTATCHMENT:
            fileArray.remove(at: sender.tag)
        case .PPTATTATCHMENT:
            pptArray.remove(at: sender.tag)
        case .YOUTUBELINKATTATCHMENT:
            youTubeLinksArray.remove(at: sender.tag)
        case .LINKATTATCHMENT:
            linksArray.remove(at: sender.tag)
        default: break
        }
        updateHeightConstraints()
        tblAttatchment.reloadData()
    }
    
    func deleteCurrentAttatchment(sender: UIButton, indexPath: IndexPath?) {
        print("The attachment is -->",sender.tag)
        var fileType = ""
        switch BUTTON_TAGS(rawValue: currentButton!.tag)!   {
        case .IMAGEATTATCHMENT:
            fileType = currentProfileModel != nil ? "photo" : "photos"
        case .VIDEOATTATCHMENT:
            fileType = "video"
        case .FILEATTATCHMENT:
            fileType = currentProfileModel != nil ? "file" : "documents"
        case .PPTATTATCHMENT:
            fileType = currentProfileModel != nil ? "presentation" : "presentations"
            
        default: break
        }
        showCustomAlert(CONFIRMATION_ALERT, message: DELETE_ATTATCHMENT, okButtonTitle: UIAlertActionTitle.DELETE.rawValue, cancelButtonTitle: UIAlertActionTitle.CANCEL.rawValue) { [unowned self](action) in
            switch action{
            case .DELETE:
                self.deleteAttatchment(fileType : fileType , sender : sender)
            case .CANCEL:
                print("Dismiss Alert")
            default:
                print("Dismiss Alert")
            }
        }
    }
    
    func deleteAttatchment(fileType : String , sender : UIButton){
        let worker = NewPostWorker()
        var idToBeDeleted = 0
        let fileType = fileType
        var fileFolder = ""
        var fileName = (URL(string:(self.previousFilesArray[sender.tag].addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!))!.lastPathComponent)
        print("Original file name : ",fileName)
        fileName =  Data(fileName.utf8).base64EncodedString()
        print("After encoding file name : ",fileName)
        var completeUrl = ""
        if serviceType == nil{
            if currentProfileModel != nil{
                switch workProfileType{
                case WORK_PROFILE_TYPE.Experience.rawValue:
                    idToBeDeleted = ((currentProfileModel as? ExperienceModel)?.pexpid)!
                    fileFolder = WORK_PROFILE_TYPE.Experience.rawValue
                case WORK_PROFILE_TYPE.Education.rawValue:
                    idToBeDeleted = ((currentProfileModel as? EducationModel)?.pedupid)!
                    fileFolder = WORK_PROFILE_TYPE.Education.rawValue
                case WORK_PROFILE_TYPE.Honors.rawValue:
                    idToBeDeleted = ((currentProfileModel as? HonorsModel)?.phonid)!
                    fileFolder = WORK_PROFILE_TYPE.Honors.rawValue
                default:
                    return
                }
                completeUrl = DELETE_ATTATCHMENT_FILE(idToBeDeleted, fileFolder: fileFolder, fileType: fileType, fileName: fileName)
                
                //            DELETE_ATTATCHMENT_FILE
            }else{
                idToBeDeleted = (self.currentPostModel?.id)!
                fileName = (URL(string:(self.previousFilesArray[sender.tag].addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!))!.lastPathComponent)
                fileName =  Data(fileName.utf8).base64EncodedString()
                completeUrl = DELETE_ATTATCHMENT(idToBeDeleted, fileType: fileType, fileName: fileName)
            }
            worker.deleteFile(completeURL:completeUrl,workProfileType:currentProfileModel != nil ? workProfileType : "", completionHandler: { [weak self] (message, responseModel) in
                if let response = responseModel , response.status == true{
                    DispatchQueue.main.async {
                        self?.previousFilesArray.remove(at:sender.tag)
                        if self?.currentProfileModel == nil{
                            let realm = RealmDBManager.sharedManager.realm
                            realm?.beginWrite()
                            self?.updateCurrentModel(fileType: fileType, sender: sender)
                            try! realm?.commitWrite()
                        }
                        self?.collectionViewPreviousFiles.reloadData()
                        self?.displayToast((response.message)!)
                    }
                    
                }else{
                    self?.displayToast(message!)
                }
            })
        }else{
            deleteServiceMedia(fileName: fileName, sender: sender, fileType: fileType)
        }
    }
    
    func updateCurrentModel(fileType:String,sender:UIButton){
        if fileType == "photos"{
            self.currentPostModel?.image.remove(at:sender.tag)
            if serviceType != nil{
                if currentProductModel != nil{
                    currentProductModel?.pgimage?.remove(at: sender.tag)
                }
                else if currentSponsorModel != nil{
                    currentSponsorModel?.image?.remove(at: sender.tag)
                }
            }
        }
        else if fileType == "video"{
            self.currentPostModel?.allvid.remove(at:sender.tag)
            if serviceType != nil {
                if currentProductModel != nil{
                    currentProductModel?.pgvideo?.remove(at: sender.tag)
                }
                else if currentSponsorModel != nil{
                    currentSponsorModel?.allvid?.remove(at: sender.tag)
                }
            }
        }
        else if fileType == "documents"{
            self.currentPostModel?.docu.remove(at:sender.tag)
            if serviceType != nil {
                if currentProductModel != nil{
                    currentProductModel?.files?.remove(at: sender.tag)
                }
                else if currentSponsorModel != nil{
                    currentSponsorModel?.docu?.remove(at: sender.tag)
                }
            }
        }
        else if fileType == "presentations"{
            self.currentPostModel?.ppt.remove(at:sender.tag)
            if serviceType != nil {
                if currentProductModel != nil{
                    currentProductModel?.presentation?.remove(at: sender.tag)
                }
                else if currentSponsorModel != nil{
                    currentSponsorModel?.ppt?.remove(at: sender.tag)
                }
            }
        }
        
    }
    
    func deleteServiceMedia(fileName:String,sender : UIButton,fileType:String){
        var serviceFolder = ""
        var folderType = ""
        
        if let serviceMediaModel = currentPostModel{
            let idToBeDeleted = serviceMediaModel.id
            switch serviceType{
            case "Product":
                serviceFolder = "gallery"
                folderType = "product"
            case "Service":
                serviceFolder = "service"
                folderType = fileType
                if folderType == "video"{
                    folderType = "videos"
                }else if folderType == "documents"{
                    folderType = "files"
                }
            case "Sponsor":
                serviceFolder = "sponsor"
                folderType = fileType
                if folderType == "video"{
                    folderType = "videos"
                }else if folderType == "documents"{
                    folderType = "files"
                }
            case "Ads":
                serviceFolder = "ads"
                folderType = fileType
                
            default:
                return
            }
            let worker = NewPostWorker()
            worker.deleteServiceMedia(completeURL: DELETE_SERVICE_MEDIA(serviceId: idToBeDeleted, serviceFolder: serviceFolder, serviceType: folderType, serviceName: fileName)) { [weak self] (message, responseModel) in
                if let response = responseModel , response.status == true{
                    DispatchQueue.main.async {
                        self?.previousFilesArray.remove(at:sender.tag)
                        self?.updateCurrentModel(fileType: fileType, sender: sender)
                        if (self?.isFromAds)!{
                            NotificationCenter.default.post(name: Notification.Name("AdImageDeleted"), object: nil)
                            self?.adsRequestModel?.adMedia = nil
                        }
                       
                        self?.collectionViewPreviousFiles.reloadData()
                        self?.displayToast((response.message)!)
                    }
                    
                }else{
                    self?.displayToast(message!)
                }
            }
            
        }
    }
    
}
