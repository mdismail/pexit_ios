//
//  FileAttatchmentHandler.swift
//  SampleCleanSwift
//
//  Created by Apple on 27/07/18.
//  Copyright © 2018 Apple. All rights reserved.
//


import Foundation
import UIKit
import MobileCoreServices
import AVFoundation
import Photos

class FileAttatchmentHandler: NSObject {
    static let shared = FileAttatchmentHandler()
    fileprivate var currentVC: UIViewController?
    var filePickedBlock : ((URL) -> Void)?
    
    func showDocumentPicker(vc:UIViewController,files:[String]?){
        let importMenu = UIDocumentPickerViewController(documentTypes: files!, in: .import)
        currentVC = vc
       // let importMenu = UIDocumentMenuViewController(documentTypes: typeOfFile!, in: .import)
        importMenu.delegate = self
        importMenu.modalPresentationStyle = .formSheet
        currentVC?.present(importMenu, animated: true, completion: nil)
    }
    
    
}
extension FileAttatchmentHandler:  UIDocumentPickerDelegate{
    func documentMenu(_ documentMenu: UIDocumentPickerViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        documentPicker.delegate = self
        currentVC?.present(documentPicker, animated: true, completion: nil)
    }
    
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        print("url", url)
        self.filePickedBlock?(url)
    }
    
    //    Method to handle cancel action.
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        currentVC?.dismiss(animated: true, completion: nil)
    }
    
}




