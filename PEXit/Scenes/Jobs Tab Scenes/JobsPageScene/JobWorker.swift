//
//  JobWorker.swift
//  PEXit
//
//  Created by Apple on 02/08/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation

class JobWorker{
    func fetchJobDetails(jobId : Int,completionHandler: @escaping(_ response : JobResponseModel?,_ error:Error? ) -> Void) {
        START_LOADING_VIEW()
        ServiceManager.methodType(requestType: GET_REQUEST, url: GET_JOB_DETAILS(jobId), params: nil ,paramsData: nil,completion: { (response,_ responseData, statusCode) in
            STOP_LOADING_VIEW()
            let decoder = JSONDecoder()
            do{
//                print("Response issss ---->",response)
                let jobDetails = try decoder.decode(JobResponseModel.self, from: responseData!)
                completionHandler(jobDetails,nil)
            }catch{
                DispatchQueue.main.async{
                    if ((response as! [String : Any])["message"] as? String) != nil{
                        KEY_WINDOW?.makeToast((response as! [String : Any])["message"] as? String)
                    }
                }
                
                print("error")
            }
            
        }) { (response, statusCode) in
            STOP_LOADING_VIEW()
        }
    }
    
    func fetchJobLocations(jobStr : String,completionHandler: @escaping(_ response : JobLocationResponseModel?,_ error:Error? ) -> Void) {
        //   START_LOADING_VIEW()
        ServiceManager.methodType(requestType: POST_REQUEST, url: GET_LOCATIONS, params: ["jlkey":jobStr] ,paramsData: nil,completion: { (response,_ responseData, statusCode) in
            STOP_LOADING_VIEW()
            let decoder = JSONDecoder()
            do{
//                print("Response issss ---->",response)
                let jobDetails = try decoder.decode(JobLocationResponseModel.self, from: responseData!)
                completionHandler(jobDetails,nil)
            }catch{
                DispatchQueue.main.async{
                    if ((response as! [String : Any])["message"] as? String) != nil{
                        KEY_WINDOW?.makeToast((response as! [String : Any])["message"] as? String)
                    }
                }
                
                print("error")
            }
            
        }) { (response, statusCode) in
            STOP_LOADING_VIEW()
        }
    }
    
    
    func applyForJob(jobId : Int,attachments:[MediaAttatchment]? = nil ,completionHandler: @escaping(_ response : CommonModel.BaseModel) -> Void) {
        START_LOADING_VIEW()
        
        AlamofireManager.shared().applyForJob(jobId: jobId, parameters: nil) { (response, error) in
            STOP_LOADING_VIEW()
            if response != nil{
                if !(((response!["errors"] as? String)?.isEmpty)!) || !((response!["virus"] as? String)?.isEmpty)!{
                    Utils.showErrorFilesAlert(errorFiles : response!["errors"] as? String,corruptFiles : response!["virus"] as? String)
                }
                let decoder = JSONDecoder()
                do{
                    
                    let jsonData = try JSONSerialization.data(withJSONObject: response, options: .prettyPrinted)
                    let reqJSONStr = String(data: jsonData, encoding: .utf8)
                    let data = reqJSONStr?.data(using: .utf8)
                    
                    let responseComment = try decoder.decode(CommonModel.BaseModel.self, from: data!)
                    completionHandler(responseComment)
                    
                }catch{
                    print("error")
                }
            }
            
        }
        
        
    }

    
}

