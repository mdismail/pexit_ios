//
//  JobPresenter.swift
//  PEXit
//
//  Created by Apple on 02/08/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation

import UIKit

protocol JobPresenterInput {
     func presentFetchResults(response: [JobModel]?,totalCount:Int?)
    func presentjobDetails(jobDetailModel: JobResponseModel?, error: Error?)
}
protocol JobPresenterOutput : class{
    func displaySearchedResults(response: [JobModel]?,totalCount:Int?)
    func displayJobDetails(response:JobResponseModel?)
}

extension JobPresenterOutput{
    func displayJobDetails(response:JobResponseModel?){
        
    }
}


class JobPresenter : JobPresenterInput{
    
    
    weak var jobPresenterOutput : JobPresenterOutput!
    
    func presentFetchResults(response:[JobModel]?,totalCount:Int?) {
        //    print("Fetched records :------>",response.viewModel?.datas_  let jobs = response
       
        jobPresenterOutput.displaySearchedResults(response: response,totalCount: totalCount)
        
    }
    
    func presentjobDetails(jobDetailModel: JobResponseModel?, error: Error?){
        if jobDetailModel != nil{
            jobPresenterOutput.displayJobDetails(response: jobDetailModel)
        }
//        let jobDetails = response
//
//        jobPresenterOutput.displaySearchedResults(response: response)
        
    }
}
