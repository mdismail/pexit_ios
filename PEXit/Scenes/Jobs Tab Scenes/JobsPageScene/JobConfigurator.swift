//
//  JobConfigurator.swift
//  PEXit
//
//  Created by Apple on 02/08/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation
import UIKit
// MARK: Connect View, Interactor, and Presenter

extension JobsPageViewController: JobPresenterOutput{
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        router.passDataToNextScene(segue: segue, sender: sender)
        
    }
}

extension JobDetailViewController: JobPresenterOutput{
    func displaySearchedResults(response: [JobModel]?, totalCount: Int?) {
        
    }
    
//    func displaySearchedResults(response: [JobModel]?) {
//
//    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if !(self.detailJobModel?.website?.isEmpty)!{
            openLinks(linkUrl:(self.detailJobModel?.website)!)
        }else{
            router.passDataToNextScene(segue: segue, sender: sender)
        }
     }
    
    
}

extension JobInteractor: JobViewControllerOutput , JobDetailViewControllerOutput{
}

extension JobPresenter: JobInteractorOutput{
    
}

class JobConfigurator{
    // MARK: Object lifecycle
    
    class var sharedInstance: JobConfigurator{
        struct Static {
            static let instance =  JobConfigurator()
        }
        print("RegistrationConfigurator instance created")
        return Static.instance
    }
    
    // MARK: Configuration
    
    func configure(viewController: JobsPageViewController){
        print("configure(viewController: RegistrationViewController) called by using class RegistrationConfigurator for setting the delegates")
        let router = JobRouter()
        router.controller = viewController
        
        let presenter = JobPresenter()
        presenter.jobPresenterOutput = viewController
        
        let interactor = JobInteractor()
        interactor.jobInteractorOutput = presenter
        
        viewController.output = interactor
        viewController.router = router
    }
    
    func configureJobDetails(viewController: JobDetailViewController){
        print("configure(viewController: RegistrationViewController) called by using class RegistrationConfigurator for setting the delegates")
            let router = JobRouter()
            router.detailController = viewController
        
        let presenter = JobPresenter()
        presenter.jobPresenterOutput = viewController
        
        let interactor = JobInteractor()
        interactor.jobInteractorOutput = presenter
        
        viewController.output = interactor
        viewController.router = router
    }
    
}
