//
//  JobsPageViewController.swift
//  PEXit
//
//  Created by Apple on 09/07/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

protocol JobViewControllerInput{
    func displaySearchedResults(response: [JobModel]?,totalCount:Int?)
}

protocol JobViewControllerOutput{
    func fetchJobs(request : SearchResult.FetchPosts.Request)
}


class JobsPageViewController: UIViewController , UICollectionViewDataSource , UICollectionViewDelegate, UISearchBarDelegate ,   UICollectionViewDelegateFlowLayout , UITableViewDelegate,UITableViewDataSource , JobViewControllerInput  {
    
    @IBOutlet weak var collectionViewSearchItems: UICollectionView!
    @IBOutlet weak var lblResultCount: UILabel!
    @IBOutlet weak var tblViewJobResults: UITableView!
    @IBOutlet weak var constraintCollectionViewHeight: NSLayoutConstraint!
    @IBOutlet weak var searchBarTitle: UISearchBar!
    @IBOutlet weak var searchByLocation: UISearchBar!
    var searchArray = [String]()
    var router : JobRouter!
    var output : JobViewControllerOutput!
    var isMoreDataAvailable = true
    var records : [JobModel] = []
    var pageNumber = 1
    var local_keywords = ""
    var global_keywords = ""
    var locationStr = ""
    
  
    var isJobSelected = false
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(JobsPageViewController.handleRefresh(_:)),
                                 for: UIControlEvents.valueChanged)
        refreshControl.tintColor = UIColor.red
        
        return refreshControl
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        doInitialSetup()
        // Do any additional setup after loading the view.
    }
    
    override func awakeFromNib(){
        super.awakeFromNib()
        JobConfigurator.sharedInstance.configure(viewController: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if isJobSelected == false{
            pageNumber = 1
            fetchJobsFromServer()
        }
        isJobSelected = false
    }
    
    func removeAbuseReportedJob(id : Int){
        DispatchQueue.main.async{
            print("Previous records count--->",self.records.count)
            self.records = self.records.filter{$0.id != id}
            print("Next records count--->",self.records.count)
            JobModel.deleteAll_withPId(Id:id)
            self.tblViewJobResults.reloadData()
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool){
        if tblViewJobResults.tableFooterView is LoadingIndicator{
            (tblViewJobResults.tableFooterView as! LoadingIndicator).stopAnimating()
        }
    }
    
    override func viewDidAppear(_ animated: Bool){
        super.viewDidAppear(animated)
        searchBarTitle.customizeSearchBar(placeholderStr:"Search By Title,Keyword Or Company",rightImageStr:"Search_gray",color:.black,placeholderColor: .gray)
        searchByLocation.customizeSearchBar(placeholderStr:"Location",rightImageStr:"loc_gray",color:.black,placeholderColor: .gray)
    }
    
    @objc func handleRefresh(_ refreshControl : UIRefreshControl){
        refreshView(previousSearchOption : "")
        refreshControl.endRefreshing()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            self.tblViewJobResults.contentOffset = CGPoint.zero
        }
    }
    
    override func refreshView(previousSearchOption : String?) {
        self.records.removeAll()
        isMoreDataAvailable = true
        pageNumber = 1
        fetchJobsFromServer()
        tblViewJobResults.reloadData()
    }
    
    
    
    func fetchJobsFromServer(pageNumber : Int? = 1,numberOfRecords : Int? = 10){
        DispatchQueue.main.async {
            self.records = JobModel.fetchJobModels()
            self.tblViewJobResults.reloadData()
        }
        //Generate request to fetch jobs from Server
        var request = SearchResult.FetchPosts.Request()
        var keywords = local_keywords
        if !global_keywords.isEmpty{
            if (!keywords.isEmpty){
                keywords = keywords + "," + global_keywords
            } else {
                keywords = global_keywords
            }
        }
        
        request.page = pageNumber
        request.records = numberOfRecords
        request.type = 2
        request.key = keywords
        request.location = locationStr
        if !keywords.isEmpty{
            let totalKeywords = keywords.components(separatedBy:",")
            if totalKeywords.count > 1{
                request.flag = 1
            }else{
                request.flag = 0
            }
        }
        output.fetchJobs(request: request)
    }
    
    func displaySearchedResults(response: [JobModel]?,totalCount:Int?) {
        DispatchQueue.main.async { [weak self] in
            if response?.count < (self?.pageNumber)! * NUMBER_OF_RECORDS{
                self?.isMoreDataAvailable = false
            }else{
                self?.isMoreDataAvailable = true
            }
            self?.records.removeAll()
            self?.records.append(contentsOf: response!)
            self?.lblResultCount.text = "\(self?.records.count ?? 0) of \(totalCount ?? 0) result"
            self?.tblViewJobResults.tableFooterView?.isHidden = true
            self?.tblViewJobResults.reloadData()
        }
    }
    
    
    func doInitialSetup(){
        showNavigationBar()
        showNavigationBarWithLeftAndRightBarButtonItems(searchBarButton: "Search",showRefresh:true)
        tblViewJobResults.tableFooterView = UIView()
        tblViewJobResults.addSubview(refreshControl)
      //  tblViewJobResults.rowHeight = UITableViewAutomaticDimension
        tblViewJobResults.estimatedRowHeight = 85
        constraintCollectionViewHeight.constant = 0
        lblResultCount.text = ""
        searchBarTitle.delegate = self
        searchByLocation.delegate = self
        collectionViewSearchItems.register(UINib(nibName: "JobSearchCell", bundle: nil), forCellWithReuseIdentifier: "JobSearchCell")
        tblViewJobResults.register(UINib(nibName: "JobResultTableViewCell", bundle: nil), forCellReuseIdentifier: "JobCell")
    }
    
    
    override func searchLocallyUsingGlobal(searchKeyword : String, searchOptions : String){
        addGlobalSearchRefreshBtn()
        global_keywords = searchKeyword
        isJobSelected = false
        
    }
    
    override func cleanUpGlobalSearch(previousSearchOption : String?){
        removeGlobalSearchRefreshBtn()
        pageNumber = 1
        print("Job selection value is-->",isJobSelected)
        isJobSelected = false
        locationStr = ""
        local_keywords = ""
        global_keywords = ""
        searchArray.removeAll()
        collectionViewSearchItems.reloadData()
        resizeCollectionViewSize()
    }
    
    func insertSearchItems(itemStr : String){
        let newIndexPath = IndexPath(item: searchArray.count, section: 0)
        searchArray.insert(itemStr, at: searchArray.count)
        collectionViewSearchItems.reloadData()
        //collectionViewSearchItems.insertItems(at: [newIndexPath])
        resizeCollectionViewSize()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        /* if let index = searchArray.index(where: { $0 == keywords}) {
         searchArray.remove(at: index)
         
         } */
        if let searchText = searchBar.text{
            //            if searchText.isEmpty{
            //                self.view.endEditing(true)
            //                displayToast(ENTER_SEARCH_KEYWORDS)
            //                return
            //            }
            if local_keywords != ""{
                local_keywords = local_keywords + "," + searchText
            }else{
                local_keywords = searchText
            }
            
            if !(searchText.isEmpty){
                insertSearchItems(itemStr: searchText)
            }
        }
        pageNumber = 1
        fetchJobsFromServer(pageNumber: pageNumber, numberOfRecords: NUMBER_OF_RECORDS)
        searchBar.text = ""
        self.view.endEditing(true)
    }
    
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        if searchBar == searchByLocation{
            router.navigateToLocationSearchScreen()
            searchByLocation.resignFirstResponder()
            return false
        }
        return true
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return searchArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        let searchCell = collectionView.dequeueReusableCell(withReuseIdentifier: "JobSearchCell", for: indexPath) as! JobSearchCell
        searchCell.lblSearchText.text = ""
        searchCell.lblSearchText.text = searchArray[indexPath.row]
        searchCell.lblSearchText.sizeToFit()
        searchCell.btnSearch.tag = indexPath.row
        searchCell.btnSearch.addTarget(self, action: #selector(deleteCell(sender:)), for: UIControlEvents.touchUpInside)
        return searchCell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let size = (searchArray[indexPath.row]).size(withAttributes: nil)
        return CGSize(width:size.width + 50,height:40)
        
    }
    
    func resizeCollectionViewSize(){
        if searchArray.count > 0{
            constraintCollectionViewHeight.constant = 40
        }else{
            constraintCollectionViewHeight.constant = 0
        }
    }
    
    @objc func deleteCell(sender:UIButton){
        if locationStr == searchArray[sender.tag]{
            locationStr = ""
            // searchArray.remove(at: sender.tag)
        }else{
            
            var strArray = local_keywords.components(separatedBy: ",")
            print(local_keywords)
            print(strArray)
            strArray = strArray.filter{$0 != searchArray[sender.tag]}
            print(strArray)
            local_keywords = strArray.joined(separator: ",")
            print(local_keywords)
            // keywords = searchArray.joined(separator: ",")
        }
        
        
        searchArray.remove(at: sender.tag)
        collectionViewSearchItems.reloadData()
        resizeCollectionViewSize()
        pageNumber = 1
        fetchJobsFromServer(pageNumber: pageNumber, numberOfRecords: NUMBER_OF_RECORDS)
        // collectionViewSearchItems.deleteItems(at: [IndexPath(row: sender.tag, section: 0)])
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return records.count
    }
    
     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return (UITableViewAutomaticDimension > 80) ? UITableViewAutomaticDimension : 80
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let jobResultCell = tableView.dequeueReusableCell(withIdentifier: "JobCell", for: indexPath) as! JobResultTableViewCell
        if !records[indexPath.row].isInvalidated{
            jobResultCell.setUpData(jobModel: records[indexPath.row])

        }else{
            print("INVALID JOBBBB")
        }
        return jobResultCell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath){
        
        if isMoreDataAvailable == true && indexPath.row == (records.count) - 1{
            isMoreDataAvailable = false
            pageNumber = pageNumber + 1
            tableView.tableFooterView = LoadingIndicator.init(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44)))
            tableView.tableFooterView?.isHidden = false
            fetchJobsFromServer(pageNumber: pageNumber, numberOfRecords: NUMBER_OF_RECORDS)
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if !records[indexPath.row].isInvalidated{
             router.navigateToNextScreen(indexPath: indexPath)
        }else{
            print("INVALIIIIIIDDDDD")
        }
       
    }
    
}

extension JobsPageViewController : ItemSelected{
    func optionSelected(option: String?) {
        //        searchArray.remove(element:locationStr)
        if let index = searchArray.index(where: { $0 == locationStr}) {
            searchArray.remove(at: index)
            //            collectionViewSearchItems.reloadData()
        }
      //  isFromLocation = true
        locationStr = option!
        // searchByLocation.text = locationStr
        insertSearchItems(itemStr: locationStr)
        fetchJobsFromServer(pageNumber: 1, numberOfRecords: NUMBER_OF_RECORDS)
    }
}
