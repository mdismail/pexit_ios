//
//  JobApplyViewController.swift
//  PEXit
//
//  Created by Apple on 17/09/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import MobileCoreServices
let HEADER_HEIGHT = 40.0
let CELL_HEIGHT = 105

class JobApplyViewController: UIViewController {
    @IBOutlet weak var lblJobTitle: UILabel!
    @IBOutlet weak var lblJobLocation: UILabel!
    @IBOutlet weak var lblJobName: UILabel!
    @IBOutlet weak var lblEmploymentType: UILabel!
    @IBOutlet weak var lblExperience: UILabel!
    @IBOutlet weak var lblJobDescription: UILabel!
    @IBOutlet weak var lblCompanyDescription: UILabel!
    @IBOutlet weak var tblProfile: UITableView!
    @IBOutlet weak var imageViewCompanyLogo: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblContactNumber: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblDob: UILabel!
    @IBOutlet weak var lblGender: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
   
    @IBOutlet weak var btnJobApply: UIButton!
    @IBOutlet weak var btnUploadResume: UIButton!
    @IBOutlet weak var tblResume: UITableView!
    @IBOutlet weak var constraintResumeHeight: NSLayoutConstraint!
    @IBOutlet weak var lblCompany: UILabel!
    @IBOutlet weak var lblCountry: UILabel!
    @IBOutlet weak var lblState: UILabel!
    @IBOutlet weak var resumeUploadView: UIView!
    @IBOutlet weak var scrollViewJobs: UIScrollView!
    var currentJobModel : JobDetailModel?
    var arrStatus:NSMutableArray = []
    let sectionHeaders =  ["Experience","Education","Honors & Rewards"]
    var educationModelArray = [EducationModel]()
    var experienceModelArray = [ExperienceModel]()
    var honorsModelArray = [HonorsModel]()
    var attachmentArray = [MediaAttatchment]()
    @IBOutlet weak var tblHeightConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addBackButton(withTitle: "Apply For Job")
        tblProfile.estimatedRowHeight = CGFloat(CELL_HEIGHT)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        initialSetUp()
    }
    
    func clearLabels(){
        lblContactNumber.text = ""
        lblName.text = ""
        lblAddress.text = ""
        lblDob.text = ""
        lblGender.text = ""
        lblLocation.text = ""
        lblState.text = ""
        lblCountry.text = ""
    }
    
    func initialSetUp(){
        fetchUserProfile()
        for _ in 0..<sectionHeaders.count
        {
            self.arrStatus.add("0")
        }
        clearLabels()
        self.tblHeightConstraint.constant = (CGFloat(HEADER_HEIGHT) * 3)
        tblProfile.register(UINib(nibName: "WorkEducationTableViewCell", bundle: nil), forCellReuseIdentifier: "workEducationCell")
        let headerNib = UINib.init(nibName: "ProfileHeaderView", bundle: Bundle.main)
        tblProfile.register(headerNib, forHeaderFooterViewReuseIdentifier: "ProfileHeaderView")
        setData(response:currentJobModel!)
    }
    
    func fetchUserProfile(){
        let worker = ProfileWorker()
        worker.fetchUserProfile { [weak self](message, status) in
            if status == true{
                self?.educationModelArray = EducationModel.fetchEducationModels()
                self?.experienceModelArray = ExperienceModel.fetchExperienceModels()
                self?.honorsModelArray = HonorsModel.fetchHonorsModels()
                let currentProfile = ProfileDetailRealmModel.fetchProfile()
                self?.setUserData(userProfile: currentProfile!)
            }
            else{
                self?.displayToast((message != nil) ? message! : UNKNOWN_ERROR_MSG )
            }
        }
        //fetchUserProfile
    }
    
    func setUserData(userProfile : ProfileDetailRealmModel){
        //        DispatchQueue.main.async{
        lblName.text = userProfile.uname
        lblContactNumber.text = (userProfile.pcode ?? "") + ":" + (userProfile.pphone ?? "")
        lblAddress.text = userProfile.paddress
        lblGender.text = userProfile.pgender
        lblDob.text = userProfile.pdob
        lblState.text = userProfile.puserstate
        lblCountry.text = userProfile.pcountry
        lblLocation.text = userProfile.plocation
        
        //        }
    }
    
    
    func setData(response : JobDetailModel){
        
        DispatchQueue.main.async{
            self.lblJobTitle.text = response.title
            let address: String? = response.addr
            let city: String? = response.states
            let country: String? = response.country
            self.lblJobLocation.text = [address,city,country].compactMap{$0}.joined(separator: ",")
            self.lblJobDescription.text = response.jdesc?.htmlDecodedText()
            self.lblCompanyDescription.text = response.cmpnydesc?.htmlDecodedText()
            if (response.cmpnydesc?.isEmpty)!{
                self.lblCompanyDescription.text = "No company description added"
            }
            self.lblEmploymentType.text = response.emptyp
            self.lblCompany.text = response.cmpny
            self.lblJobName.text = response.indus
            self.lblExperience.text = response.level
            if let imageUrl = response.logo{
                let imageURL = URL(string:(imageUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!))
                self.imageViewCompanyLogo.setImage(from: imageURL!)
                
            }
            
            
            self.tblProfile.reloadData()
            
            self.tblProfile.layoutIfNeeded()
        }
        
    }
    
    @IBAction func btnJobApplyClicked(_ sender: Any) {
        
        
        let worker = JobWorker()
        worker.applyForJob(jobId: (currentJobModel?.id)!, attachments: attachmentArray) { (response) in
            self.displayToast(response.message!)
            if response.status == true{
                self.navigationController?.popToRootViewController(animated: true)
            }
        }
        
    }
    
    @IBAction func btnUploadResumeClicked(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.isSelected{
            
            if attachmentArray.count < 1{
                FileAttatchmentHandler.shared.showDocumentPicker(vc: self, files: [String(kUTTypePDF),String(kUTTypeRTF),String(kUTTypePlainText)])
                FileAttatchmentHandler.shared.filePickedBlock = {[unowned self] (url) in
                    print(url)
                    print("File Name is ---->",url.lastPathComponent)
                    
                    do {
                        let data = try Data(contentsOf: url, options: .mappedIfSafe)
                        print(data)
                        let media = MediaAttatchment(file: nil, fileName: url.lastPathComponent,fileData:data,fileExtension:url.mimeType())
                        self.constraintResumeHeight.constant = 80
                        self.resumeUploadView.isHidden = false
                        self.attachmentArray.append(media)
                        self.tblResume.reloadData()
                        
                        
                    } catch  {
                    }
                }
            }
           
        }else{
            constraintResumeHeight.constant = 0
            resumeUploadView.isHidden = true
        }
        
    }
    
    
}

extension JobApplyViewController : UITableViewDataSource , UITableViewDelegate ,AttachmentDeleteTapped{
    
    
    @objc func headerCellButtonTapped(_sender: UIButton)
    {
        let str:String = arrStatus[_sender.tag] as! String
//        var countOfOpenSections = 0
//        var countOfClosedSections = 0
//        for element in arrStatus{
//            if (element as! String) == "0"{
//                countOfClosedSections = countOfClosedSections+1
//            }else{
//                countOfOpenSections = countOfOpenSections+1
//            }
//        }
        
        if str == "0"
        {
            arrStatus[_sender.tag] = "1"
            switch _sender.tag{
            case 0:
                self.tblHeightConstraint.constant = self.tblHeightConstraint.constant + CGFloat(experienceModelArray.count * CELL_HEIGHT)
            case 1:
                self.tblHeightConstraint.constant = self.tblHeightConstraint.constant + CGFloat(educationModelArray.count * CELL_HEIGHT)
            case 2:
                self.tblHeightConstraint.constant = self.tblHeightConstraint.constant + CGFloat(honorsModelArray.count * CELL_HEIGHT)
            default:
                self.tblHeightConstraint.constant = (CGFloat(HEADER_HEIGHT) * 3)
            }
        }
        else
        {
            arrStatus[_sender.tag] = "0"
            switch _sender.tag{
            case 0:
                self.tblHeightConstraint.constant = self.tblHeightConstraint.constant - CGFloat(experienceModelArray.count * CELL_HEIGHT)
            case 1:
                self.tblHeightConstraint.constant = self.tblHeightConstraint.constant - CGFloat(educationModelArray.count * CELL_HEIGHT)
            case 2:
                self.tblHeightConstraint.constant = self.tblHeightConstraint.constant - CGFloat(honorsModelArray.count * CELL_HEIGHT)
            default:
                self.tblHeightConstraint.constant = (CGFloat(HEADER_HEIGHT) * 3)
            }
        }
        
        tblProfile.reloadData()
        tblProfile.layoutIfNeeded()
        
    }
    

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        switch tableView{
        case tblProfile:
            return CGFloat(HEADER_HEIGHT)
        case tblResume:
            return 0
        default:
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tblProfile.dequeueReusableHeaderFooterView(withIdentifier: "ProfileHeaderView") as! ProfileHeaderView
        headerView.headerTitle.text = sectionHeaders[section]
        headerView.btnAddRecord.tag = section
        headerView.btnAddRecord.addTarget(self, action: #selector(JobApplyViewController.headerCellButtonTapped(_sender:)), for: UIControlEvents.touchUpInside)
        let str:String = arrStatus[section] as! String
        if str == "0"
        {
            UIView.animate(withDuration: 2) { () -> Void in
                headerView.btnAddRecord.setImage(UIImage(named : "drop"), for: .normal)
                
//                self.tblProfile.imgArrow.image = UIImage(named :"drop")
                let angle =  CGFloat(Double.pi * 2)
                let tr = CGAffineTransform.identity.rotated(by: angle)
                headerView.btnAddRecord.imageView?.transform = tr
//                self.tblProfile.imgArrow.transform = tr
            }
        }
        else
        {
            UIView.animate(withDuration: 2) { () -> Void in
                headerView.btnAddRecord.setImage(UIImage(named : "up"), for: .normal)
//                self.tblProfile.imgArrow.image = UIImage(named :"up")
                let angle =  CGFloat(M_PI * 2)
                let tr = CGAffineTransform.identity.rotated(by: angle)
                headerView.btnAddRecord.imageView?.transform = tr
            }
        }
        return headerView
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        switch tableView{
        case tblProfile:
            let str:String = arrStatus[section] as! String
            switch section{
            case 0 :
                return str == "0" ? 0 : experienceModelArray.count
            case 1 :
                return str == "0" ? 0 : educationModelArray.count
            case 2 :
                return str == "0" ? 0 : honorsModelArray.count
            default:
                return 0
            }
        case tblResume:
            return attachmentArray.count
        default:
            return 0
        }
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
       
//        switch tableView{
        if tableView ==  tblProfile{
             let workEducationCell = tableView.dequeueReusableCell(withIdentifier: "workEducationCell", for: indexPath) as! WorkEducationTableCell
            workEducationCell.trailingConstraintHeader.constant = 10
            workEducationCell.collectionViewMedia.isHidden = true
            workEducationCell.btnEdit.isHidden = true
            workEducationCell.btnDelete.isHidden = true
            switch indexPath.section{
            case 0:
                workEducationCell.setExperienceData(experienceModel: experienceModelArray[indexPath.row])
            case 1:
                workEducationCell.setEducationData(educationModel: educationModelArray[indexPath.row])
            case 2:
                workEducationCell.setHonorsData(honorsModel: honorsModelArray[indexPath.row])
            default:
                print("empty cell")
                
            }
            workEducationCell.lblLocation.text = ""
            workEducationCell.lblJobDescription.text = ""
            workEducationCell.heightConstraintCollectionView.constant = 0
            return workEducationCell
        }
        else{
            let uploadCell = tableView.dequeueReusableCell(withIdentifier: "resumeCell", for: indexPath) as! AttatchmentTableViewCell
            let currentAttactchement = attachmentArray[indexPath.row]
            uploadCell.btnDelete.tag = indexPath.row
            uploadCell.delegate = self
            uploadCell.setData(currentAttatchment : currentAttactchement)
            return uploadCell
        }

        
        
    }
    func numberOfSections(in tableView: UITableView) -> Int
    {
        switch tableView{
        case tblProfile:
            return 3
        case tblResume:
            return 1
        default:
            return 0
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        switch tableView{
        case tblProfile:
            return CGFloat(CELL_HEIGHT)
        case tblResume:
            return 80
        default:
            return 0
        
        }
    }
    
    func deleteTapped(sender : UIButton){
        attachmentArray.remove(at: sender.tag)
        tblResume.reloadData()
        btnUploadResume.isSelected = !btnUploadResume.isSelected
        if attachmentArray.count == 0{
            constraintResumeHeight.constant = 0
        }
    }
}
