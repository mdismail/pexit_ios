//
//  JobRouter.swift
//  PEXit
//
//  Created by Apple on 02/08/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation
import UIKit

protocol JobRouterInput{
    func navigateToNextScreen(indexPath:IndexPath)
}

class JobRouter : JobRouterInput{
    weak var controller : JobsPageViewController?
    weak var detailController : JobDetailViewController?
    func navigateToNextScreen(indexPath:IndexPath) {
        controller?.isJobSelected = true
        controller?.performSegue(withIdentifier: "jobToDetail", sender: indexPath)
    }
 
    func navigateToLocationSearchScreen(){
        let locationController = controller?.storyboard?.instantiateViewController(withIdentifier: "locationController") as! LocationViewController
        locationController.delegate = controller
        controller?.navigationController?.pushViewController(locationController, animated: true)
    }
    
    
    func passDataToNextScene(segue: UIStoryboardSegue,sender:Any?){
        if segue.identifier == "jobToDetail"{
            let destinationController = segue.destination as! JobDetailViewController
            let currentIndex = sender as! IndexPath
            destinationController.jobReportedAbuse = { [weak self] (jobIndex) in
                self?.controller?.removeAbuseReportedJob(id:jobIndex)
            }
            destinationController.currentJobModel = controller?.records[currentIndex.row]
        }
        
        if segue.identifier == "detailsToApply"{
                let destinationController = segue.destination as! JobApplyViewController
                destinationController.currentJobModel = detailController?.detailJobModel
           
            
        }
    }
}
