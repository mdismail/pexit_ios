//
//  JobDetailViewController.swift
//  PEXit
//
//  Created by Apple on 02/08/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import Presentr

protocol JobDetailViewControllerInput{
    func displayJobDetails(response:JobResponseModel?)
}

protocol JobDetailViewControllerOutput{
    func fetchJobDetails(jobId : Int)
}

class JobDetailViewController: UIViewController , JobDetailViewControllerInput , ItemSelected {
    
    
    @IBOutlet weak var lblJobTitle: UILabel!
    @IBOutlet weak var lblJobLocation: UILabel!
    @IBOutlet weak var lblJobName: UILabel!
    @IBOutlet weak var lblEmploymentType: UILabel!
    @IBOutlet weak var lblExperience: UILabel!
    @IBOutlet weak var lblJobDescription: UILabel!
    @IBOutlet weak var lblCompanyDescription: UILabel!
    @IBOutlet weak var btnApply: UIButton!
    @IBOutlet weak var imgLogo: UIImageView!
    @IBOutlet weak var lblCompanyName: UILabel!
    @IBOutlet weak var btnViewPoster: UIButton!
    @IBOutlet weak var btnViewPosterHeight: NSLayoutConstraint!
    //    @IBOutlet weak var viewResume: UIView!
    //    @IBOutlet weak var constraintResumeViewHeight: NSLayoutConstraint!
    //    @IBOutlet weak var btnResumeUpload: UIButton!
    //    @IBOutlet weak var lblResumeFile: UILabel!
    //    @IBOutlet weak var viewUploadedResume: UIView!
    //    @IBOutlet weak var contsraintUploadResumeHeight: NSLayoutConstraint!
    var currentJobModel : JobModel?
    var output : JobDetailViewControllerOutput!
    var router : JobRouter!
    var detailJobModel : JobDetailModel?
    var currentBtnTag = 1
    var arrayLangInfo = [AllLangDetails]()
    
    var jobReportedAbuse : ((Int) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        clearLabels()
        addBackButton(withTitle: "Jobs")
        addBlockUserOption()
        //        showNavigationBarWithLeftAndRightBarButtonItems(languageTranslator: "language",showRefresh:false)
        if !(currentJobModel?.isInvalidated)!{
            output.fetchJobDetails(jobId: (currentJobModel?.id)!)
            
        }else{
            displayToast("Please try again")
        }
        //        NotificationCenter.default.addObserver(self, selector: #selector(JobDetailViewController.languageUpdateNotification(notification:)), name: Notification.Name("LanguageUpdated"), object: nil)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        CURRENT_APP_LANGUAGE = "en"
        SELECTED_LANGUAGE = "English"
    }
    
    func clearLabels(){
        lblJobTitle.text = ""
        lblJobLocation.text = ""
        lblJobName.text = ""
        lblEmploymentType.text = ""
        lblExperience.text = ""
        lblJobDescription.text = ""
        lblCompanyDescription.text = ""
        lblCompanyName.text = ""
    }
    
    override func awakeFromNib(){
        super.awakeFromNib()
        JobConfigurator.sharedInstance.configureJobDetails(viewController:self)
    }
    
    func displayJobDetails(response:JobResponseModel?){
        if response != nil && response?.status == true{
            setData(response: (response?.datas)!)
        }
    }
    
    
    
    func showLanguagesList(){
        getLanguages { (languagesArray,completeLanguageDetails) in
            self.arrayLangInfo = completeLanguageDetails
            self.presentLanguagesView(languageArray: languagesArray)
        }
    }
    
    func presentLanguagesView(languageArray : [String]){
        DispatchQueue.main.async {
            let presenter = Presentr(presentationType: .popup)
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "commonViewController") as! CommonViewController
            // controller.isFromHome = true
            controller.isLanguageUpdation = true
            controller.itemsArray = languageArray
            controller.delegate = self
            self.customPresentViewController(presenter, viewController: controller, animated: true)
        }
    }
    
    override func showPopOver(){
            if let popUpController = self.storyboard?.instantiateViewController(withIdentifier: "commonViewController") as? CommonViewController {
                popUpController.delegate = self
                popUpController.itemsArray = [REPORT_ABUSE]
                popUpController.modalPresentationStyle = .popover
                popUpController.preferredContentSize = CGSize(width : self.view.frame.size.width / 1.5 , height : CGFloat(60))
                if let pctrl = popUpController.popoverPresentationController {
                    pctrl.barButtonItem = navigationItem.rightBarButtonItem
                    pctrl.delegate = self
                    self.present(popUpController, animated: true, completion: nil)
                }
            }
    }
    
    func optionSelected(option: String?) {
        if option == REPORT_ABUSE{
            showAbuseCategoryOptions { [weak self] (option) in
                self?.showConfirmationOptionToBlock(itemToMarkAbuse: "Job", completion: { (index) in
                    if index == 0{
                        self?.reportAbuse(category: option)
                    }
                })
            }
        }else{
            CURRENT_APP_LANGUAGE = arrayLangInfo.filter{$0.name == option}[0].code
            SELECTED_LANGUAGE = arrayLangInfo.filter{$0.name == option}[0].name
            translateToCurrentSelectedLanguage()
        }
    }
    
    
    func reportAbuse(category : String){
        let worker = HomeWorker()
        let reportAbuseModel = ReportAbuseModel()
        reportAbuseModel.rcategory = category
        reportAbuseModel.moid = detailJobModel?.uid
        reportAbuseModel.mid = detailJobModel?.id
        reportAbuseModel.moduleType = "job"
        worker.reportAbuse(model: reportAbuseModel) { [weak self] (response) in
            if response.status == true{
                self?.jobReportedAbuse?((self?.detailJobModel?.id)!)
                self?.backAction()
            }
            self?.displayToast(response.message!)
        }
    }
    
    func setData(response : JobDetailModel){
        
        detailJobModel = response
        DispatchQueue.main.async{
            if !(self.detailJobModel?.website?.isEmpty)!{
                self.btnApply.setTitle("Apply on link", for: .normal)
            }
            if !((self.currentJobModel?.isInvalidated)!){
                if self.currentJobModel?.applyStatus != 0{
                    //  self.btnApply.isUserInteractionEnabled = false
                    self.btnApply.setTitle("Reapply", for: .normal)
                }
            }
            
            if response.prsnl == "show" || response.prsnl == "yes"{
                self.btnViewPoster.isHidden = false
                self.btnViewPosterHeight.constant = 20
                self.view.layoutIfNeeded()
                
            }else{
                self.btnViewPoster.isHidden = true
                self.btnViewPosterHeight.constant = 0
                self.view.layoutIfNeeded()
                
            }
            self.lblJobTitle.text = response.title
            var address: String? = response.addr
            var city: String? = response.states
            var country: String? = response.country
            address = (address?.isEmpty)! ? nil : address
            city = (city?.isEmpty)! ? nil : city
            country = (country?.isEmpty)! ? nil : country
            self.lblJobLocation.text = [address,city,country].compactMap{$0}.joined(separator: ",")
            
            self.lblJobDescription.text = (response.jdesc?.stringByStrippingHTMLTags)?.stringByDecodingHTMLEntities
            
            self.lblCompanyDescription.text = (response.cmpnydesc?.stringByStrippingHTMLTags)?.stringByDecodingHTMLEntities
            if (response.cmpnydesc?.isEmpty)!{
                self.lblCompanyDescription.text = "No company description added"
            }
            self.lblEmploymentType.text = response.emptyp
            self.lblJobName.text = response.indus
            self.lblCompanyName.text = response.cmpny
            self.lblExperience.text = response.level
            if response.userType == PROFILE_TYPE.ORGANIZATION.rawValue{
                self.btnApply.isHidden = true
            }
            else{
                self.btnApply.isHidden = false
            }
            
            if let imageUrl = response.logo{
                let imageURL = URL(string:(imageUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!))
                self.imgLogo.setImage(from: imageURL!)
                
            }
        }
        //        if CURRENT_APP_LANGUAGE != PREVIOUS_APP_LANGUAGE{
        //            translateToCurrentSelectedLanguage()
        //        }
        
        
    }
    
    @IBAction func btnTranslateClicked(_ sender: UIButton) {
        currentBtnTag = sender.tag
        showLanguagesList()
    }
    
    func translateToCurrentSelectedLanguage(){
        let translationWorker = LanguageTranslationWoker()
        var lblArray = [UILabel]()
        var stringToTranslate : String?
        if currentBtnTag == 1{
            lblArray = [lblJobDescription]
            stringToTranslate = (detailJobModel?.jdesc?.stringByStrippingHTMLTags)?.stringByDecodingHTMLEntities
        }else{
            lblArray = [lblCompanyDescription]
            stringToTranslate = (detailJobModel?.cmpnydesc?.stringByStrippingHTMLTags)?.stringByDecodingHTMLEntities
        }
        //  var lblArray = [lblJobDescription,lblCompanyDescription]
        //        let jobDesc = (detailJobModel?.jdesc?.stringByStrippingHTMLTags)?.stringByDecodingHTMLEntities
        //        let companyDesc =
        //    (detailJobModel?.cmpnydesc?.stringByStrippingHTMLTags)?.stringByDecodingHTMLEntities
        translationWorker.translateCurrentString(newLanguageCode: CURRENT_APP_LANGUAGE, stringsToTranslate: [stringToTranslate ?? ""]) { (response) in
            if response != nil{
                if let translations = response as? [ReturnedJson]{
                    DispatchQueue.main.async {
                        var i = 0
                        for value in translations{
                            
                            for translatedStr in value.translations{
                                lblArray[i].text = translatedStr.text
                                print("Translated string is--->",lblArray[i].text)
                                i = i + 1
                                
                                
                            }
                            
                        }
                    }
                }
            }
        }
    }
    
    
    @IBAction func btnViewPosterClicked(_ sender: Any) {
        let groupDetailsController = storyboard?.instantiateViewController(withIdentifier: "groupDetailViewController") as! GroupDetailViewController
        groupDetailsController.userId = detailJobModel?.uid
        navigationController?.pushViewController(groupDetailsController, animated: true)
    }
    
    @IBAction func btnApplyClicked(_ sender: Any) {
        if !((self.currentJobModel?.isInvalidated)!){
            if self.currentJobModel?.applyStatus != 0{
                showCustomAlert(CONFIRMATION_ALERT, message: "You have already applied,Do you want to reapply?", okButtonTitle: UIAlertActionTitle.APPLY.rawValue, cancelButtonTitle: UIAlertActionTitle.CANCEL.rawValue) { [unowned self](action) in
                    switch action{
                    case .DELETE:
                        self.applyForJob()
                        return
                    case .CANCEL:
                        return
                    default:
                        print("Dismiss Alert")
                    }
                }
            }else{
                applyForJob()
            }
        }else{
            applyForJob()
        }
        
    }
    
    func applyForJob(){
        if (self.detailJobModel?.website?.isEmpty)!{
            self.performSegue(withIdentifier:"detailsToApply", sender: self)
        }else{
            openLinks(linkUrl:(self.detailJobModel?.website)!)
        }
    }
    
    
}

extension JobDetailViewController : UIPopoverPresentationControllerDelegate{
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return .none
    }
}
