//
//  JobModel.swift
//  PEXit
//
//  Created by Apple on 02/08/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation
import Realm
import RealmSwift
import ObjectMapper



struct JobResponseModel : Codable{
    var status: Bool?
    var message : String?
    var datas : JobDetailModel?
}

class JobDetailModel : Codable{
    var id: Int?
    var uid : Int?
    var logo : String?
    var cmpny : String?
    var addr : String?
    var states: String?
    var country : String?
    var jdesc : String?
    var indus : String?
    var emptyp : String?
    var level : String?
    var funct : String?
    var title : String?
    var cmpnydesc : String?
    var userType : String?
    var website : String?
    var active : Int?
    var expired : Int?
    var edit : Int?
    var pincod : String?
    var renew : Int?
    var stype : String?
    var prsnl : String?
    var apply : String?
    
}


struct JobLocationResponseModel : Codable{
    var status: Bool?
    var message : String?
    var datas : [JobLocationModel]?
}

class JobLocationModel : Codable{
    var name : String?
}

class JobModel: Object, Mappable {
    @objc dynamic var id: Int = Int()
    @objc dynamic var uid: Int = Int()
    @objc dynamic var logo: String?
    @objc dynamic var title: String?
    @objc dynamic var cmpny: String?
    @objc dynamic var addr: String?
    @objc dynamic var states: String?
    @objc dynamic var country: String?
    @objc dynamic var applyStatus: Int = Int()
   
    
    convenience required init?(map: Map) {
        self.init()
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    
    func mapping(map: Map) {
        id <- map["id"]
        uid <- map["uid"]
        logo <- map["logo"]
        title <- map["title"]
        cmpny <- map["cmpny"]
        addr <- map["addr"]
        states <- map["states"]
        country <- map["country"]
        applyStatus <- map["applyStatus"]
       
    }
    
    
    //    MARK: fetch and delete functions
    static func deleteAll(){
        if let realm = RealmDBManager.sharedManager.getRealm() {
            do {
                try realm.write() {
                    realm.delete(realm.objects(JobModel.self))
                    realm.refresh()
                }
            } catch (let error as NSError) {
                
                print("ERROR: Realm: writing to database, ERROR: \(error.localizedDescription)")
            }
        }
    }
    
    static func deleteAll_withPId(Id:Int){
        let pred = NSPredicate(format: "id == %d",Id)
        let objectsToDelete = RealmDBManager.sharedManager.fetchObjectsWithPredicate(object: JobModel.self, pred: pred)
        if objectsToDelete != nil && (objectsToDelete?.count)! > 0{
            
            
            if let realm = RealmDBManager.sharedManager.getRealm() {
                do {
                    try realm.write() {
                        realm.delete(objectsToDelete!)
                        realm.refresh()
                    }
                } catch (let error as NSError) {
                    
                    print("ERROR: Realm: writing to database, ERROR: \(error.localizedDescription)")
                }
            }
        }
    }
    
    static func fetchJobModels() -> [JobModel]{
        var  jobModels = RealmDBManager.sharedManager.fetchObjects(object: self) as! [JobModel]
        return jobModels
    }
}
