//
//  JobInteractor.swift
//  PEXit
//
//  Created by Apple on 02/08/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation

protocol JobInteractorInput {
   func fetchJobs(request : SearchResult.FetchPosts.Request)
   func fetchJobDetails(jobId : Int)
}

protocol JobInteractorOutput{
    func presentFetchResults(response: [JobModel]?,totalCount:Int?)
    func presentjobDetails(jobDetailModel: JobResponseModel?, error: Error?)
}

class JobInteractor : JobInteractorInput{
    var jobInteractorOutput : JobInteractorOutput!
    var worker = HomeWorker()
    var jobWorker = JobWorker()
    func fetchJobs(request: SearchResult.FetchPosts.Request) {
        worker.fetchPosts(request: request) { [weak self](response,totalCount,profileResponse) in
           
                (self?.jobInteractorOutput.presentFetchResults(response: response as? [JobModel],totalCount: totalCount))!
            
            
        }
    }
    func fetchJobDetails(jobId: Int) {
        jobWorker.fetchJobDetails(jobId: jobId) { (response, error) in
            self.jobInteractorOutput.presentjobDetails(jobDetailModel: response as? JobResponseModel, error: error as? Error)
        }
    }
}
