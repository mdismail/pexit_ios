//
//  EMarketPageViewController.swift
//  PEXit
//
//  Created by Apple on 09/07/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import Presentr


class EMarketPageViewController: UIViewController , ItemSelected  {

    private var marketControllerType = "Products"
    var currentController = ""
//    var currentSearchOption = ""
    private lazy var marketProductViewContoller: EMarketProductViewController = {
        var viewController = storyboard?.instantiateViewController(withIdentifier: "productMarketController") as! EMarketProductViewController
        viewController.parentController = self
        return viewController
    }()
    
    private lazy var servicesViewController: EMarketSSTSoftwareViewController = {
       // Instantiate View Controller
        var viewController = storyboard?.instantiateViewController(withIdentifier: "sstSoftwareController") as! EMarketSSTSoftwareViewController
        viewController.controllerType = ControllerType.Services.rawValue
        viewController.parentController = self
        return viewController
    }()
    private lazy var sstSoftwareViewController: EMarketSSTSoftwareViewController = {
        var viewController = storyboard?.instantiateViewController(withIdentifier: "sstSoftwareController") as! EMarketSSTSoftwareViewController
        viewController.controllerType = ControllerType.SSTSoftware.rawValue
        viewController.parentController = self
        return viewController
    }()
    
    private lazy var otherSoftwareViewController: EMarketSSTSoftwareViewController = {
       var viewController = storyboard?.instantiateViewController(withIdentifier: "sstSoftwareController") as! EMarketSSTSoftwareViewController
        viewController.controllerType = ControllerType.OtherSoftware.rawValue
        return viewController
    }()
   
    override func viewDidLoad() {
        super.viewDidLoad()
        showNavigationBar()
        addNavigationBarWithTitleAndSubtitle(title: "E-Market", subTitle: "Products",size:CGFloat(120))
        showNavigationBarWithLeftAndRightBarButtonItems(searchBarButton: "Search",showRefresh:true)
      //  marketControllerType = "Products"
        //add(asChildViewController: marketProductViewContoller)
     //   optionSelected(option: "Products")
      
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if (!marketControllerType.isEmpty) {
            optionSelected(option: marketControllerType)
            //marketControllerType = ""
        }
    }

    override func searchLocallyUsingGlobal(searchKeyword : String, searchOptions : String){
        var uiViewController : UIViewController?
//        currentSearchOption = searchOptions
        //marketControllerType = ""
        if searchOptions == GLOBAL_SEARCH_TYPE.PRODUCTS.rawValue ||
            searchOptions == GLOBAL_SEARCH_TYPE.PRODUCTSUPPLIERS.rawValue{
            marketControllerType = "Products"
            uiViewController = marketProductViewContoller;
        }
        else if searchOptions == GLOBAL_SEARCH_TYPE.SOFTWARES.rawValue{
            marketControllerType = "SST Software"
            uiViewController = sstSoftwareViewController
        } else if searchOptions == GLOBAL_SEARCH_TYPE.SERVICES.rawValue {
            marketControllerType = "Services"
            uiViewController = servicesViewController
        }
        if !marketControllerType.isEmpty {
            if self.childViewControllers.count > 0{
                if marketControllerType != currentController {
                    self.childViewControllers[0].cleanUpGlobalSearch_subview()
                    remove(asChildViewController: self.childViewControllers[0])
                }
            }

            uiViewController?.searchLocallyUsingGlobal_subview(searchKeyword: searchKeyword, searchOptions : searchOptions)
        }
    }
    
    override func cleanUpGlobalSearch(previousSearchOption : String?){
        var uiViewController : UIViewController?
        var searchOptionControllerType = previousSearchOption
        if searchOptionControllerType == nil || (searchOptionControllerType?.isEmpty)!{
            searchOptionControllerType = marketControllerType
        }

        if searchOptionControllerType != nil && !(searchOptionControllerType?.isEmpty)!{
            if searchOptionControllerType == GLOBAL_SEARCH_TYPE.PRODUCTS.rawValue ||
                searchOptionControllerType == GLOBAL_SEARCH_TYPE.PRODUCTSUPPLIERS.rawValue{
                uiViewController = marketProductViewContoller;
            } else if searchOptionControllerType == GLOBAL_SEARCH_TYPE.SOFTWARES.rawValue || searchOptionControllerType == "Softwares"{
                uiViewController = sstSoftwareViewController
            } else if searchOptionControllerType == GLOBAL_SEARCH_TYPE.SERVICES.rawValue {
                uiViewController = servicesViewController
            }
            uiViewController?.cleanUpGlobalSearch_subview()
        }
        //marketProductViewContoller.cleanUpGlobalSearch_subview()
        //sstSoftwareViewController.cleanUpGlobalSearch_subview()
        //servicesViewController.cleanUpGlobalSearch_subview()
//        marketControllerType = "Products"
    }
    
    override func refreshView(previousSearchOption : String?) {
        var uiViewController : UIViewController?
        
        if ((previousSearchOption == nil) || (previousSearchOption?.isEmpty)!){
            print("**** previousSearchOption is nil/empty ****")
        }
        
        if previousSearchOption != nil && !(previousSearchOption?.isEmpty)!{
            if previousSearchOption == GLOBAL_SEARCH_TYPE.PRODUCTS.rawValue ||
                previousSearchOption == GLOBAL_SEARCH_TYPE.PRODUCTSUPPLIERS.rawValue{
                uiViewController = marketProductViewContoller;
            } else if previousSearchOption == GLOBAL_SEARCH_TYPE.SOFTWARES.rawValue{
                uiViewController = sstSoftwareViewController
            } else if previousSearchOption == GLOBAL_SEARCH_TYPE.SERVICES.rawValue {
                uiViewController = servicesViewController
            }
            uiViewController?.refresh_SubView()
        }
        //marketProductViewContoller.refresh_SubView()
        //sstSoftwareViewController.refresh_SubView()
        //servicesViewController.refresh_SubView()
    }
    
    func optionSelected(option: String?) {
        print("Option Choosen----->", option ?? "")
        print("CHILD CONTROLLERS ------>",self.childViewControllers)
        
        if self.childViewControllers.count > 0{
          
            if option == currentController{
                print("CONTROLLER DID NOT CHANGE----->",currentController)
                return
            }
            CURRENT_GLOBAL_SEARCH_KEYWORD = ""
            CURRENT_GLOBAL_SEARCH_OPTION = ""
        self.childViewControllers[0].cleanUpGlobalSearch_subview()
            remove(asChildViewController: self.childViewControllers[0])
        }
        addNavigationBarWithTitleAndSubtitle(title: "E-Market", subTitle: option,size:CGFloat(120))
//        if !currentSearchOption.isEmpty{
//            addGlobalSearchRefreshBtn()
//            currentSearchOption = ""
//        }
        currentController = option!
        switch option{
        case "Products":
            marketControllerType = "Products"
            add(asChildViewController: marketProductViewContoller)
        case "Services":
            marketControllerType = "Services"
            add(asChildViewController: servicesViewController)
        case "SST Software":
            marketControllerType = "SST Software"
            add(asChildViewController: sstSoftwareViewController)
        case "Other Software":
            marketControllerType = "Other Software"
            add(asChildViewController: otherSoftwareViewController)
        default:
            print("No COntroller")
        }
    }
    
    override func clickOnSubtitleButton(_ sender: UIButton) {
        let presenter = Presentr(presentationType: .popup)
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "commonViewController") as! CommonViewController
        controller.itemsArray = ["Products","Services","SST Software","Other Software"]
        controller.delegate = self
        customPresentViewController(presenter, viewController: controller, animated: true)
    }
 
}
