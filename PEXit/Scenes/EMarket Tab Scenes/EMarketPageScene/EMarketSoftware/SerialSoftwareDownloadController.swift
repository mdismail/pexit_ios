//
//  SerialSoftwareDownloadController.swift
//  PEXit
//
//  Created by Apple on 16/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import Material


class SerialSoftwareDownloadController: UIViewController, UITableViewDelegate , UITableViewDataSource , SoftwareCellDelegate{
    
    
    @IBOutlet weak var tblSoftwares: UITableView!
    @IBOutlet weak var txtFieldSerialKey: TJTextField!
    @IBOutlet weak var txtFieldEmail: TJTextField!
    @IBOutlet weak var txtFieldName: TJTextField!
    var responseSoftwares : SoftwaresLinkModel?
    var softwareArray = [SoftwareModel]()
    lazy var downloadsSession: URLSession = {
        let configuration = URLSessionConfiguration.default
        return URLSession(configuration: configuration, delegate: self, delegateQueue: nil)
    }()
    // Get local file path: download task stores tune here; AV player plays it.
    let documentsPath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
    func localFilePath(for url: URL) -> URL {
        return documentsPath.appendingPathComponent(url.lastPathComponent)
    }
    let downloadService = DownloadService()
    override func viewDidLoad() {
        super.viewDidLoad()
        setData()
        tblSoftwares.register(UINib(nibName: "EMarketSoftwareCell", bundle: nil), forCellReuseIdentifier: "marketSoftwareCell")
        downloadService.downloadsSession = downloadsSession
        // Do any additional setup after loading the view.
    }
    
    func setData(){
        txtFieldEmail.isUserInteractionEnabled = false
        txtFieldEmail.text = ProfileRLMModel.fetchProfile()?.userEmail
        txtFieldName.text = ProfileRLMModel.fetchProfile()?.userName
    }
    
    @IBAction func btnDownloadClicked(_ sender: Any) {
        self.view.endEditing(true)
//        Utils.validateTextAndShowToastWithRequiredFields([txtFieldName,txtFieldEmail,txtFieldSerialKey]){
        if Utils.validateFormAndShowToastWithRequiredFields([txtFieldName,txtFieldEmail,txtFieldSerialKey]){
            let worker = EMarketSoftwareWorker()
            worker.fetchSoftwareLinks(serialKey: txtFieldSerialKey.text!) { [weak self] (response, error) in
                if response != nil{
                    self?.displayToast((response?.message)!)
                    if response?.status == true{
                        self?.responseSoftwares = response?.datas
                        self?.organizeData()
                        
                    }
                }else{
                    self?.displayToast(error?.localizedDescription ?? UNKNOWN_ERROR_MSG)
                }
            }
        }
    }
    
    
    func organizeData(){
        DispatchQueue.main.async{
            for (key,value) in try! self.responseSoftwares.asDictionary(){
                let softwareModel = SoftwareModel()
                softwareModel.name = key.capitalized
                softwareModel.url = value as? String
                self.softwareArray.append(softwareModel)
            }
            self.tblSoftwares.reloadData()
            self.tblSoftwares.isHidden = false
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let productCell = tableView.dequeueReusableCell(withIdentifier: "marketSoftwareCell", for: indexPath) as! EMarketSoftwareCell
        let softwareModel = softwareArray[indexPath.row]
        softwareModel.index = indexPath.row
        productCell.delegate = self
        productCell.configure(softwareModel: softwareModel, downloaded: softwareModel.downloaded ?? false , download: downloadService.activeDownloads[URL(string:softwareModel.url!)!])
        return productCell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return softwareArray.count
    }
    
    func downloadTapped(_ cell: EMarketSoftwareCell) {
        if let indexPath = tblSoftwares.indexPath(for: cell) {
            let software = softwareArray[indexPath.row]
            downloadService.startDownload(software)
            reload(indexPath.row)
            }
        }
    
    
    func cancelTapped(_ cell: EMarketSoftwareCell) {
        if let indexPath = tblSoftwares.indexPath(for: cell) {
            let track = softwareArray[indexPath.row]
            downloadService.cancelDownload(track)
            reload(indexPath.row)
        }
    }
    
    // Update software cell's buttons
    func reload(_ row: Int) {
        tblSoftwares.reloadRows(at: [IndexPath(row: row, section: 0)], with: .none)
    }
}

extension SerialSoftwareDownloadController: URLSessionDownloadDelegate {
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask,
                    didFinishDownloadingTo location: URL) {
        // 1
        guard let sourceURL = downloadTask.originalRequest?.url else { return }
        let download = downloadService.activeDownloads[sourceURL]
        downloadService.activeDownloads[sourceURL] = nil
        // 2
        let destinationURL = localFilePath(for: sourceURL)
        print(destinationURL)
        // 3
        let fileManager = FileManager.default
        try? fileManager.removeItem(at: destinationURL)
        do {
            try fileManager.copyItem(at: location, to: destinationURL)
            download?.softwareModel.downloaded = true
            
        } catch let error {
            print("Could not copy file to disk: \(error.localizedDescription)")
        }
        // 4
                if let index = download?.softwareModel.index {
                    DispatchQueue.main.async {
                        self.tblSoftwares.reloadRows(at: [IndexPath(row: index, section: 0)], with: .none)
                    }
                }
    }
    
    
    // Updates progress info
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask,
                    didWriteData bytesWritten: Int64, totalBytesWritten: Int64,
                    totalBytesExpectedToWrite: Int64) {
        // 1
        guard let url = downloadTask.originalRequest?.url,
            let download = downloadService.activeDownloads[url]  else { return }
        // 2
        download.progress = Float(totalBytesWritten) / Float(totalBytesExpectedToWrite)
        // 3
        let totalSize = ByteCountFormatter.string(fromByteCount: totalBytesExpectedToWrite,
                                                  countStyle: .file)
        // 4
        DispatchQueue.main.async {
            if let trackCell = self.tblSoftwares.cellForRow(at: IndexPath(row: download.softwareModel.index!,
                                                                          section: 0)) as? EMarketSoftwareCell {
                trackCell.updateDisplay(progress: download.progress, totalSize: totalSize)
            }
        }
    }
    
}


