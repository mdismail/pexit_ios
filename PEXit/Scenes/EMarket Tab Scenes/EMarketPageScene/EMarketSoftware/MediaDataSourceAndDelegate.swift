//
//  MediaDataSourceAndDelegate.swift
//  PEXit
//
//  Created by Apple on 17/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation

import Foundation
import UIKit

class MediaDataSourceAndDelegate: NSObject, UITableViewDataSource , UITableViewDelegate , UICollectionViewDataSource, UICollectionViewDelegate , UICollectionViewDelegateFlowLayout{
    var videoArray = [String]()
    var imageArray = [String]()
    var pptArray = [String]()
    var docuArray = [String]()
    var youTubeArray = [String]()
    var linksArray = [String]()
    
    var parentController : UIViewController?
    
    func configureTblWithDataSource(responseModel : ServiceModel,controller:UIViewController){
        pptArray = responseModel.presentation!
        imageArray = responseModel.photos!
        videoArray = responseModel.videos!
        docuArray = responseModel.files!
        if responseModel.links != nil && !(responseModel.links?.isEmpty)!{
            linksArray = (responseModel.links?.components(separatedBy: ","))!
        }
        if responseModel.utube != nil && !(responseModel.utube?.isEmpty)!{
            youTubeArray = (responseModel.utube?.components(separatedBy: ","))!
        }
        parentController = controller
    }
    
  
    
    func configureMediaTbl(responseModel : SponsorModel,controller:UIViewController){
        pptArray = responseModel.ppt!
        imageArray = responseModel.image!
        videoArray = responseModel.allvid!
        docuArray = responseModel.docu!
        if responseModel.link != nil && !(responseModel.link?.isEmpty)!{
            linksArray = (responseModel.link?.components(separatedBy: ","))!
        }
        if responseModel.video != nil && !(responseModel.video?.isEmpty)!{
            youTubeArray = (responseModel.video?.components(separatedBy: ","))!
        }
        parentController = controller
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 6
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0{
            return 30
        }
        return 100
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int){
        view.tintColor = UIColor.clear
        let header = view as! UITableViewHeaderFooterView
        header.textLabel?.textColor = THEME_RED_COLOR
        header.textLabel?.text =  header.textLabel?.text?.capitalized
   //     header.textLabel?.textAlignment = .left

    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section{
        case 0:
            return linksArray.count > 0 ? "Links" : ""
        case 1:
            return imageArray.count > 0 ? "Images" : ""
        case 2:
            return videoArray.count > 0 ?"Videos" : ""
        case 3:
            return docuArray.count > 0 ?"Documents" : ""
        case 4:
            return pptArray.count > 0 ?"Presentations" : ""
        case 5:
            return youTubeArray.count > 0 ?"Youtube Videos" : ""
        default:
            return "Media"
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section{
        case 0:
            return linksArray.count > 0 ? linksArray.count : 0
        case 1:
            return imageArray.count > 0 ? 1 : 0
        case 2:
            return videoArray.count > 0 ? 1 : 0
        case 3:
            return docuArray.count > 0 ? 1 : 0
        case 4:
            return pptArray.count > 0 ? 1 : 0
        case 5:
            return youTubeArray.count > 0 ? 1 : 0
        default:
            return 0
            
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section{
        case 0:
            return linksArray.count > 0 ? 30 : 0
        case 1:
            return imageArray.count > 0 ? 30 : 0
        case 2:
            return videoArray.count > 0 ? 30 : 0
        case 3:
            return docuArray.count > 0 ? 30 : 0
        case 4:
            return pptArray.count > 0 ? 30 : 0
        case 5:
            return youTubeArray.count > 0 ? 30 : 0
        default:
            return 0
            
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "mediaLinkCell", for: indexPath)
            cell.textLabel?.text = linksArray[indexPath.row]
            //cell.content
            cell.textLabel?.textAlignment = .left
            cell.textLabel?.setTextAsLink()
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "homeDetailCell", for: indexPath) as! HomeDetailTableViewCell
            cell.mediaCollectionView.register(UINib(nibName: "HomeDetailCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "homeDetailsCell")
            cell.mediaCollectionView.dataSource = self
            cell.mediaCollectionView.delegate = self
            cell.mediaCollectionView.tag = indexPath.section
            cell.mediaCollectionView.reloadData()
            return cell
        }
    }
    
    
    

    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0{
            parentController?.openLinks(linkUrl:linksArray[indexPath.row])
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        switch collectionView.tag{
        case 0:
            return linksArray.count
        case 1:
            return imageArray.count
        case 2:
            return videoArray.count
        case 3:
            return docuArray.count
        case 4:
            return pptArray.count
        case 5:
            return youTubeArray.count
        default:
            return 0
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "homeDetailsCell", for: indexPath) as! HomeDetailCollectionViewCell
        switch collectionView.tag{
        case 0:
            cell.setUpViewForLink(link: linksArray[indexPath.row])
        case 1:
            cell.setUpViewForImage(url:imageArray[indexPath.row])
        case 2:
            cell.setUpViewForVideo(videoUrl: videoArray[indexPath.row])
        case 3:
            cell.setUpViewForImage(isFile : true)
        case 4:
            cell.setUpViewForImage(isPresentation: true)
        case 5:
            cell.setUpViewForYouTubeVideo(youTubeVideoUrl: youTubeArray[indexPath.row])
        default :
            print("Default Statement")
        }
        
        return cell
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch collectionView.tag{
        case 0 :
            let size = (linksArray[indexPath.row]).size(withAttributes: nil)
            return CGSize(width:size.width + 50,height:30)
        case 1,2,3,4:
            return CGSize.init(width: collectionView.frame.size.width / 3, height: collectionView.frame.size.height)
        case 5:
            return CGSize.init(width: collectionView.frame.size.width / 2, height: collectionView.frame.size.height)
            
        default:
            return CGSize.init(width: collectionView.frame.size.width / 3, height: collectionView.frame.size.height)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch collectionView.tag{
        case 0:
            parentController?.openLinks(linkUrl:linksArray[indexPath.row])
            
        case 1:
            let photoController = AppToolbarController(rootViewController: PhotoViewController(imagesArray:imageArray,index: indexPath.row))
            parentController?.present(photoController, animated: true, completion: nil)
            
        // cell.setUpViewForImage(url:postModel.image[indexPath.row])
        case 2:
            parentController?.openVideoPlayer(videoUrl: videoArray[indexPath.row])
            
        case 3:
            parentController?.showAttatchmentView(mediaUrl: docuArray[indexPath.row])
        case 4:
            parentController?.showAttatchmentView(mediaUrl: pptArray[indexPath.row])
        case 5:
            print("You Tube Video")
            
        default :
            print("Default Statement")
            
        }
    }
    
    
    
    
    
    //    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    //        return 5
    //    }
    //
    //    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
    //        let cell = tableView.dequeueReusableCell(withIdentifier: "homeDetailCell", for: indexPath) as! HomeDetailTableViewCell
    //        // downloadCell.textLabel?.text = fileURLs[indexPath.row].lastPathComponent
    //        return cell
    //    }
    //
    
    
    
    
    
    
    //    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    //        return searchArray.count
    //    }
    //
    //    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
    //        let searchCell = collectionView.dequeueReusableCell(withReuseIdentifier: "JobSearchCell", for: indexPath) as! JobSearchCell
    //        searchCell.lblSearchText.text = ""
    //        searchCell.lblSearchText.text = searchArray[indexPath.row]
    //        searchCell.lblSearchText.sizeToFit()
    //        searchCell.btnSearch.tag = indexPath.row
    //        searchCell.btnSearch.addTarget(self, action: #selector(EMarketSSTSoftwareViewController.deleteCell(sender:)), for: UIControlEvents.touchUpInside)
    //        return searchCell
    //    }
    //
    //    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    //        let size = (searchArray[indexPath.row]).size(withAttributes: nil)
    //        return CGSize(width:size.width + 50,height:40)
    //
    //    }
    
}
