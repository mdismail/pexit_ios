//
//  EMarketSoftwareWorker.swift
//  PEXit
//
//  Created by Apple on 15/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation
class EMarketSoftwareWorker{
    func fetchServiceLocations(softwareStr : String,completionHandler: @escaping(_ response : JobLocationResponseModel?,_ error:Error? ) -> Void) {
        //   START_LOADING_VIEW()
        ServiceManager.methodType(requestType: POST_REQUEST, url: SERVICE_LOCATIONS, params: ["serlkey":softwareStr] ,paramsData: nil,completion: { (response,_ responseData, statusCode) in
            STOP_LOADING_VIEW()
            let decoder = JSONDecoder()
            do{
//                print("Response issss ---->",response)
                let jobDetails = try decoder.decode(JobLocationResponseModel.self, from: responseData!)
                completionHandler(jobDetails,nil)
            }catch{
                DispatchQueue.main.async{
                    if ((response as! [String : Any])["message"] as? String) != nil{
                        KEY_WINDOW?.makeToast((response as! [String : Any])["message"] as? String)
                    }
                }
                
                print("error")
            }
            
        }) { (response, statusCode) in
            STOP_LOADING_VIEW()
        }
    }
    
    func fetchSoftwareLinks(serialKey : String,completionHandler: @escaping(_ response : SoftwaresLinkResponseModel?,_ error:Error? ) -> Void) {
        START_LOADING_VIEW()
        ServiceManager.methodType(requestType: POST_REQUEST, url: GET_SOFTWARE_LINKS, params: ["serial":serialKey] ,paramsData: nil,completion: { (response,_ responseData, statusCode) in
            STOP_LOADING_VIEW()
            let decoder = JSONDecoder()
            do{
                //print("Response issss ---->",response)
                let softwareLinksModel = try decoder.decode(SoftwaresLinkResponseModel.self, from: responseData!)
                completionHandler(softwareLinksModel,nil)
            }catch{
                DispatchQueue.main.async{
                    if ((response as! [String : Any])["message"] as? String) != nil{
                        KEY_WINDOW?.makeToast((response as! [String : Any])["message"] as? String)
                    }
                }
                
                print("error")
            }
            
        }) { (response, statusCode) in
            STOP_LOADING_VIEW()
        }
    }
    
    func fetchServiceDetails(serviceId : Int,completionHandler: @escaping(_ response : ServiceDetailResponseModel?,_ error:Error? ) -> Void) {
        START_LOADING_VIEW()
        ServiceManager.methodType(requestType: GET_REQUEST, url: GET_SERVICE_DETAILS(serviceID: serviceId), params: nil ,paramsData: nil,completion: { (response,_ responseData, statusCode) in
            STOP_LOADING_VIEW()
            let decoder = JSONDecoder()
            do{
//                print("Response issss ---->",response)
                let serviceDetailModel = try decoder.decode(ServiceDetailResponseModel.self, from: responseData!)
                completionHandler(serviceDetailModel,nil)
            }catch{
                DispatchQueue.main.async{
                    if ((response as! [String : Any])["message"] as? String) != nil{
                        KEY_WINDOW?.makeToast((response as! [String : Any])["message"] as? String)
                    }
                }
                
                print("error")
            }
            
        }) { (response, statusCode) in
            STOP_LOADING_VIEW()
        }
    }
}
