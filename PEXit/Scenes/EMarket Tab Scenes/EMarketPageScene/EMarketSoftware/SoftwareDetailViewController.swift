//
//  SoftwareDetailViewController.swift
//  PEXit
//
//  Created by ats on 15/12/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import Presentr

class SoftwareDetailViewController: UIViewController , ItemSelected{
    
    @IBOutlet weak var btnDownload: UIButton!
    @IBOutlet weak var imgViewLogo: UIImageView!
    @IBOutlet weak var lblSoftwareName: UILabel!
    
    @IBOutlet weak var lblProgressIndicator: UILabel!
    @IBOutlet weak var downloadProgressBar: UIProgressView!
    @IBOutlet weak var txtViewSoftwareDesc: UITextView!
    //    @IBOutlet weak var lblSoftwareLink: UITextView!
    @IBOutlet weak var btnViewDownloads: UIButton!
    @IBOutlet weak var parentScrollView: UIScrollView!
    @IBOutlet weak var lblSoftwareDesc: UILabel!
    lazy var downloadsSession: URLSession = {
        let configuration = URLSessionConfiguration.default
        return URLSession(configuration: configuration, delegate: self, delegateQueue: nil)
    }()
    // Get local file path: download task stores tune here; AV player plays it.
    let documentsPath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
    func localFilePath(for url: URL) -> URL {
        return documentsPath.appendingPathComponent(url.lastPathComponent)
    }
    var arrayLangInfo = [AllLangDetails]()
    let downloadService = DownloadService()
    var softwareModel : SoftwareModel?
    var softwareReportedAbuse : ((Int) -> Void)?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("scroll width",parentScrollView.contentSize.width)
        parentScrollView.contentSize.width = self.view.frame.size.width
        print("scroll width",self.view.frame.size.width)
        txtViewSoftwareDesc.leftAlignTextView()
        btnViewDownloads.isHidden = true
        txtViewSoftwareDesc.dataDetectorTypes = [.link]
        //        lblSoftwareLink.leftAlignTextView()
        addBackButton(withTitle: "Software Details")
        addBlockUserOption()
        setSoftwareData()
        downloadService.downloadsSession = downloadsSession
        //        NotificationCenter.default.addObserver(self, selector: #selector(JobDetailViewController.languageUpdateNotification(notification:)), name: Notification.Name(LANG_UPDATED_NOTIFICATION), object: nil)
        //        showNavigationBarWithLeftAndRightBarButtonItems(languageTranslator: "language")
        // Do any additional setup after loading the view.
    }
    
    //    deinit {
    //        NotificationCenter.default.removeObserver(self, name: Notification.Name(LANG_UPDATED_NOTIFICATION), object: nil)
    //    }
    
    override func viewWillDisappear(_ animated: Bool) {
        CURRENT_APP_LANGUAGE = "en"
        SELECTED_LANGUAGE = "English"
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setSoftwareData(){
        lblSoftwareName.text = softwareModel?.name
        // lblSoftwareDesc.text = softwareModel?.descrip?.htmlDecodedText()
        
        //        let attributedStr =   try? NSAttributedString(htmlString: softwareModel?.descrip ?? "")
        //        let mutableAttributedStr = NSMutableAttributedString.init(attributedString: attributedStr ?? NSAttributedString.init(string: ""))
        //
        //        let softwareAttributedStr = mutableAttributedStr.trimmedAttributedString(set: CharacterSet.whitespacesAndNewlines)
        //         let currentFont = UIFont().Arial_Regular(n:14)
        //        softwareAttributedStr.setFontFace(font: currentFont, color: .black)
        if let softwareDesc = softwareModel?.descrip?.getHtmlAttributedStr(){
            txtViewSoftwareDesc.attributedText = softwareDesc
            
        }
        //
        //        let currentFont = UIFont().Arial_Regular(n:12)
        //        let softwareDesc = try? NSMutableAttributedString(HTMLString: softwareModel?.descrip ?? "" , font: currentFont)
        //        if let softwareDescription = softwareDesc{
        //            txtViewSoftwareDesc.attributedText = softwareDescription
        //        }
        
        
        
        //        let mutableStr = NSMutableAttributedString.init(string: softwareModel?.descrip ?? "")
        //        txtViewSoftwareDesc.attributedText = mutableStr.htmlAttributed(family: "Arial", size: 12, color: .black)
        adjustUITextViewHeight(arg: txtViewSoftwareDesc)
        //        lblSoftwareLink.text = softwareModel?.url
        if let imageUrl = softwareModel?.logo{
            let imageURL = URL(string:(imageUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!))
            imgViewLogo.setImage(from: imageURL!)
        }
        //        if CURRENT_APP_LANGUAGE != PREVIOUS_APP_LANGUAGE{
        //            self.translateToCurrentSelectedLanguage()
        //        }
        showSoftwareDownloadBtn()
        
    }
    
    
    @IBAction func btnTranslateClicked(_ sender: Any) {
        showListOfLanguages()
    }
    
    
    func adjustUITextViewHeight(arg : UITextView)
    {
        //        txtViewSoftwareDesc.translatesAutoresizingMaskIntoConstraints = true
        txtViewSoftwareDesc.sizeToFit()
        txtViewSoftwareDesc.isScrollEnabled = false
    }
    
    @IBAction func downloadSoftwareClicked(_ sender: Any) {
        if softwareModel?.dwnld == "serial"{
            navigateToSoftwareDownloadScreen()
        }else{
            btnDownload.setTitle("Downloading", for: .normal)
            btnDownload.isUserInteractionEnabled = false
            downloadService.startDownload(softwareModel!)
            //reload(indexPath.row)
        }
        
    }
    
    func showSoftwareDownloadBtn(){
        if softwareModel?.dwnld == "serial"{
            btnDownload.isHidden = false
        }else{
            if softwareModel?.software != nil , let softwareURL = URL(string:(softwareModel?.software!)!){
                btnDownload.isHidden = false
            }else{
                if softwareModel?.url != nil && !(softwareModel?.url?.isEmpty)! && !((URL(string:(softwareModel?.url!)!)?.lastPathComponent.isEmpty)!){
                    if (softwareModel?.url?.isValidURL)!{
                        softwareModel?.software = softwareModel?.url
                        btnDownload.isHidden = false
                        
                    }else{
                        btnDownload.isHidden = true
                        
                    }
                }else{
                    btnDownload.isHidden = true
                    
                }
                
            }
        }
        
    }
    
    func navigateToSoftwareDownloadScreen(){
        DispatchQueue.main.async {
            let presenter = Presentr(presentationType: .popup)
            let softwareController = self.storyboard?.instantiateViewController(withIdentifier: "serialSoftwareDownloadController") as! SerialSoftwareDownloadController
            self.customPresentViewController(presenter, viewController: softwareController, animated: true)
        }
    }
    
    //MARK: - LANGUAGE TRANSLATION
    
    func showListOfLanguages(){
        getLanguages { (languagesArray,completeLanguageDetails) in
            self.arrayLangInfo = completeLanguageDetails
            self.presentLanguagesView(languageArray: languagesArray)
        }
    }
    
    //    override func showLanguages(){
    //        getLanguages { (languagesArray,completeLanguageDetails) in
    //            self.arrayLangInfo = completeLanguageDetails
    //            self.presentLanguagesView(languageArray: languagesArray)
    //        }
    //    }
    
    func presentLanguagesView(languageArray : [String]){
        DispatchQueue.main.async {
            let presenter = Presentr(presentationType: .popup)
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "commonViewController") as! CommonViewController
            // controller.isFromHome = true
            controller.isLanguageUpdation = true
            controller.itemsArray = languageArray
            controller.delegate = self
            self.customPresentViewController(presenter, viewController: controller, animated: true)
        }
    }
    
    
    override func showPopOver(){
        if let popUpController = self.storyboard?.instantiateViewController(withIdentifier: "commonViewController") as? CommonViewController {
            popUpController.delegate = self
            popUpController.itemsArray = [REPORT_ABUSE]
            popUpController.modalPresentationStyle = .popover
            popUpController.preferredContentSize = CGSize(width : self.view.frame.size.width / 1.5 , height : CGFloat(60))
            if let pctrl = popUpController.popoverPresentationController {
                pctrl.barButtonItem = navigationItem.rightBarButtonItem
                pctrl.delegate = self
                self.present(popUpController, animated: true, completion: nil)
            }
        }
    }
    
    
    func optionSelected(option: String?) {
        if option == REPORT_ABUSE{
            showAbuseCategoryOptions { [weak self] (option) in
                self?.showConfirmationOptionToBlock(itemToMarkAbuse: "Software", completion: { (index) in
                    if index == 0{
                        self?.reportAbuse(category: option)
                    }
                })
            }
        }else{
            CURRENT_APP_LANGUAGE = arrayLangInfo.filter{$0.name == option}[0].code
            SELECTED_LANGUAGE = arrayLangInfo.filter{$0.name == option}[0].name
            translateToCurrentSelectedLanguage()
        }
    }
    
    func reportAbuse(category : String){
        let worker = HomeWorker()
        let reportAbuseModel = ReportAbuseModel()
        reportAbuseModel.rcategory = category
        reportAbuseModel.moid = self.softwareModel?.uid
        reportAbuseModel.mid = self.softwareModel?.id
        reportAbuseModel.moduleType = "software"
        worker.reportAbuse(model: reportAbuseModel) { [weak self] (response) in
            if response.status == true{
                self?.softwareReportedAbuse?((self?.softwareModel?.id)!)
                self?.backAction()
            }
            self?.displayToast(response.message!)
        }
    }
    
    
    
    func translateToCurrentSelectedLanguage(){
        let translationWorker = LanguageTranslationWoker()
        var lblArray = [txtViewSoftwareDesc]
        // let serviceDesc = softwareModel?.descrip?.htmlDecodedText()
        let serviceDesc = softwareModel?.descrip
        translationWorker.translateCurrentString(newLanguageCode: CURRENT_APP_LANGUAGE, stringsToTranslate: [serviceDesc ?? ""],textType:"html") { (response) in
            if response != nil{
                if let translations = response as? [ReturnedJson]{
                    DispatchQueue.main.async {
                        var i = 0
                        for value in translations{
                            
                            for translatedStr in value.translations{
                                if let softwareDesc = translatedStr.text.getHtmlAttributedStr(){
                                    lblArray[i]?.attributedText = softwareDesc
                                    
                                }
                                // lblArray[i]?.text = translatedStr.text
                                i = i + 1
                                
                                
                            }
                            
                        }
                    }
                }
            }
        }
    }
    
    @IBAction func btnViewDownloadsClicked(_ sender: Any) {
        let downloadedFilesController = self.storyboard?.instantiateViewController(withIdentifier: "downloadedFilesViewController")
        self.navigationController?.pushViewController(downloadedFilesController!, animated: true)
    }
    
    
}

extension SoftwareDetailViewController: URLSessionDownloadDelegate {
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask,
                    didFinishDownloadingTo location: URL) {
        // 1
        guard let sourceURL = downloadTask.originalRequest?.url else { return }
        let download = downloadService.activeDownloads[sourceURL]
        /*    if downloadService.activeDownloads[sourceURL] == nil{
         print("FILE NAME EMPTY")
         return
         } */
        downloadService.activeDownloads[sourceURL] = nil
        // 2
        /* if sourceURL.lastPathComponent.isEmpty{
         print("FILE NAME EMPTY AGAIN")
         return
         } */
        let destinationURL = localFilePath(for: sourceURL)
        print(destinationURL)
        
        // 3
        let fileManager = FileManager.default
        try? fileManager.removeItem(at: destinationURL)
        do {
            try fileManager.copyItem(at: location, to: destinationURL)
            download?.softwareModel.downloaded = true
            
            //            download?.track.downloaded = true
        } catch let error {
            print("Could not copy file to disk: \(error.localizedDescription)")
        }
        // 4
        if let index = download?.softwareModel.index {
            DispatchQueue.main.async {
                print("DOWNLOADED>>>>>>")
                self.btnDownload.setTitle("Downloaded", for: .normal)
                self.downloadProgressBar.isHidden = true
                self.btnViewDownloads.isHidden = false
                self.lblProgressIndicator.isHidden = true
                // self.copySoftware(softwareModel : download?.softwareModel)
                //                self.tblResults.reloadRows(at: [IndexPath(row: index, section: 0)], with: .none)
            }
        }
    }
    
    
    // Updates progress info
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask,
                    didWriteData bytesWritten: Int64, totalBytesWritten: Int64,
                    totalBytesExpectedToWrite: Int64) {
        // 1
        guard let url = downloadTask.originalRequest?.url,
            let download = downloadService.activeDownloads[url]  else { return }
        // 2
        download.progress = Float(totalBytesWritten) / Float(totalBytesExpectedToWrite)
        // 3
        let totalSize = ByteCountFormatter.string(fromByteCount: totalBytesExpectedToWrite,
                                                  countStyle: .file)
        /*     DispatchQueue.main.async{
         if totalSize == "-1 byte"{
         if let trackCell = self.tblResults.cellForRow(at: IndexPath(row: download.softwareModel.index!,section: 0)) as? EMarketSoftwareCell {
         self.displayToast("Cannot downlaod")
         self.downloadService.cancelDownload(self.softwareArray[download.softwareModel.index!])
         self.reload(download.softwareModel.index!)
         return
         }
         }
         } */
        
        // 4
        DispatchQueue.main.async {
            var progress = String(format: "%.1f%% of %@", download.progress * 100, totalSize)
            self.downloadProgressBar.isHidden = false
            self.lblProgressIndicator.isHidden = false
            self.downloadProgressBar.progress = download.progress
            self.lblProgressIndicator.text = progress
            
        }
    }
    
}

extension SoftwareDetailViewController : UIPopoverPresentationControllerDelegate{
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return .none
    }
}
