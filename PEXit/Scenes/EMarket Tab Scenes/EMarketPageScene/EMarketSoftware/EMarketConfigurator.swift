//
//  EMarketConfigurator.swift
//  PEXit
//
//  Created by Apple on 15/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation

extension EMarketSSTSoftwareViewController: EMarketSoftwarePresenterOutput{
    
    
    
}

extension EMarketSoftwareInteractor: EMarketSoftwareViewControllerOutput{
}

extension EMarketSoftwarePresenter: EMarketSoftwareInteractorOutput{
    
    
    
    
}



class EMarketSoftwareConfigurator{
    
    // MARK: Object lifecycle
    
    class var sharedInstance: EMarketSoftwareConfigurator{
        struct Static {
            static let instance =  EMarketSoftwareConfigurator()
        }
        print("RegistrationConfigurator instance created")
        return Static.instance
    }
    
    // MARK: Configuration
    
    func configure(viewController: EMarketSSTSoftwareViewController){
        print("configure(viewController: InviteContactViewController) called by using class InviteContactConfigurator for setting the delegates")
        let router = EMarketSoftwareRouter()
        router.controller = viewController
        
        let presenter = EMarketSoftwarePresenter()
        presenter.output = viewController

        let interactor = EMarketSoftwareInteractor()
        interactor.output = presenter

        viewController.output = interactor
        viewController.router = router
        
    }
    
}
