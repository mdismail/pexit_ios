//
//  SearchResultDataSource.swift
//  PEXit
//
//  Created by Apple on 15/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation
import UIKit
class SearchResultController: NSObject, UICollectionViewDataSource, UICollectionViewDelegate , UICollectionViewDelegateFlowLayout{
    var searchArray = [String]()
    var countryArray = [String]()
    var stateArray = [String]()
    var companyArray = [String]()
    var parentController : UIViewController?
    var isFromAds = false
    
    func setUpSearchArray(_ collectionView: UICollectionView){
        if parentController != nil && parentController is AdsAudienceViewController{
            if collectionView == (parentController as! AdsAudienceViewController).collectionViewCountry{
               countryArray = (parentController as! AdsAudienceViewController).selectedCountryArray
            }
            if collectionView == (parentController as! AdsAudienceViewController).collectionViewCompany{
               companyArray = (parentController as! AdsAudienceViewController).selectedCompanyArray
            }
            if collectionView == (parentController as! AdsAudienceViewController).collectionViewStates{
               stateArray = (parentController as! AdsAudienceViewController).selectedStatesArray
            }
        }
        collectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if parentController != nil && parentController is AdsAudienceViewController{
            if collectionView == (parentController as! AdsAudienceViewController).collectionViewCountry{
                return countryArray.count
            }
            if collectionView == (parentController as! AdsAudienceViewController).collectionViewCompany{
               return companyArray.count
            }
            if collectionView == (parentController as! AdsAudienceViewController).collectionViewStates{
                return stateArray.count
            }
        }
        return searchArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        let searchCell = collectionView.dequeueReusableCell(withReuseIdentifier: "JobSearchCell", for: indexPath) as! JobSearchCell
        searchCell.lblSearchText.text = ""
        
        searchCell.btnSearch.tag = indexPath.row
        searchCell.btnSearch.removeTarget(nil, action: nil, for: .allEvents)
       
        if parentController != nil && parentController is AdsAudienceViewController{
            if collectionView == (parentController as! AdsAudienceViewController).collectionViewCountry{
                searchCell.lblSearchText.text = countryArray[indexPath.row]
            }
            if collectionView == (parentController as! AdsAudienceViewController).collectionViewCompany{
               searchCell.lblSearchText.text = companyArray[indexPath.row]
            }
            if collectionView == (parentController as! AdsAudienceViewController).collectionViewStates{
               searchCell.lblSearchText.text = stateArray[indexPath.row]
            }
             searchCell.btnSearch.isHidden = true
             searchCell.lblSearchText.sizeToFit()
        }
        if parentController != nil && parentController is EMarketSSTSoftwareViewController{
            searchCell.lblSearchText.text = searchArray[indexPath.row]
            searchCell.lblSearchText.sizeToFit()
            searchCell.btnSearch.addTarget(EMarketSSTSoftwareViewController(), action: #selector(EMarketSSTSoftwareViewController.deleteCell(sender:)), for: UIControlEvents.touchUpInside)
        }
        
        return searchCell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var size : CGSize?
        if parentController != nil && parentController is AdsAudienceViewController{
            if collectionView == (parentController as! AdsAudienceViewController).collectionViewCountry{
               size = (countryArray[indexPath.row]).size(withAttributes: nil)
               return CGSize(width:size!.width + 50,height:40)
            }
            if collectionView == (parentController as! AdsAudienceViewController).collectionViewCompany{
                size = (companyArray[indexPath.row]).size(withAttributes: nil)
                return CGSize(width:size!.width + 50,height:40)
            }
            if collectionView == (parentController as! AdsAudienceViewController).collectionViewStates{
                size = (stateArray[indexPath.row]).size(withAttributes: nil)
                return CGSize(width:size!.width + 50,height:40)
            }
           
        }
       // else{
            let sizeSearch = (searchArray[indexPath.row]).size(withAttributes: nil)
            return CGSize(width:sizeSearch.width + 50,height:40)
     //   }
       
        
    }
    
}



