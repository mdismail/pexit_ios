//
//  EMarketSoftwareInteractor.swift
//  PEXit
//
//  Created by Apple on 15/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation
//
//  EMarketInteractor.swift
//  PEXit
//
//  Created by Apple on 03/08/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation
protocol EMarketSoftwareInteractorInput{
    
    func fetchSoftwareList(request: SearchResult.FetchPosts.Request)
}


protocol EMarketSoftwareInteractorOutput{
    
    func presentFetchResults(response: [SoftwareModel]?)
    func presentServiceResults(response: [ServiceModel]?,totalCount:Int?)
    
}

class EMarketSoftwareInteractor : EMarketSoftwareInteractorInput{
    var output: EMarketSoftwareInteractorOutput!
   
    var homeWorker = HomeWorker()
    
    
    func fetchSoftwareList(request: SearchResult.FetchPosts.Request){
        homeWorker.fetchPosts(request: request) { [weak self](response,totalCount,profileResponse)  in
            if request.type == 5{
                (self?.output.presentFetchResults(response: response as? [SoftwareModel]))!
            }
            if request.type == 6{
                (self?.output.presentServiceResults(response: response as? [ServiceModel],totalCount: totalCount))!
            }
        }
    }
 }
