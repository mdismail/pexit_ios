//
//  EmarketSoftwareModel.swift
//  PEXit
//
//  Created by Apple on 15/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//


extension Encodable {
    func asDictionary() throws -> [String: Any] {
        let data = try JSONEncoder().encode(self)
        guard let dictionary = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any] else {
            throw NSError()
        }
        return dictionary
    }
}

import Foundation
class SoftwareResponseModel : Codable{
    var message : String?
    var status : Bool?
    var datas : [SoftwareModel]?
}

class SoftwareModel : Codable{
    var id : Int?
    var uid : Int?
    var logo : String?
    var name : String?
    var descrip : String?
    var url : String?
    var software : String?
    var dwnld : String?
    var downloaded : Bool? = false
    var index : Int? = 0
    var isExpanded : Bool? = false
    var translatedDescrip : String?
}

class ServiceResponseModel : Codable{
    var message : String?
    var status : Bool?
    var datas : [ServiceModel]?
}

class ServiceDetailResponseModel : Codable{
    var message : String?
    var status : Bool?
    var datas : ServiceModel?
    var totalRecords : Int?
}

class ServiceModel : Codable{
    var id : Int?
    var uid : Int?
    var logo : String?
    var industry : String?
    var company : String?
    var city : String?
    var states : String?
    var country : String?
    var descrip : String?
    var links : String?
    var photos : [String]?
    var videos : [String]?
    var presentation : [String]?
    var files : [String]?
    var utube : String?
    var pincode : String?
    
}

class SoftwaresLinkResponseModel : Codable{
    var message : String?
    var status : Bool?
    var datas : SoftwaresLinkModel?
}

class SoftwaresLinkModel : Codable{
    var licenseManager : String?
    var sstSoftwareExe : String?
    var sstSoftwareZip : String?
}
