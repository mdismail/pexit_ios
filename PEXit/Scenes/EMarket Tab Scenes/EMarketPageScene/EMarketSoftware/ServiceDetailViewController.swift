//
//  ServiceDetailViewController.swift
//  PEXit
//
//  Created by Apple on 16/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import SDWebImage
import Presentr

class ServiceDetailViewController: UIViewController , ItemSelected {
    @IBOutlet weak var lblServiceTitle: UILabel!
    @IBOutlet weak var lblIndustry: UILabel!
    @IBOutlet weak var lblCountry: UILabel!
    @IBOutlet weak var lblState: UILabel!
    @IBOutlet weak var lblCity: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var tblServiceDetails: UITableView!
    @IBOutlet weak var imageService: UIImageView!
    @IBOutlet weak var btnContact : UIButton!
    var idOfService : Int?
    var currentServiceModel : ServiceModel?
    var arrayLangInfo = [AllLangDetails]()
    lazy var dataController: MediaDataSourceAndDelegate = {
        let dataController = MediaDataSourceAndDelegate()
        return dataController
    }()
    var serviceReportedAbuse : ((Int) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblServiceDetails.register(UINib(nibName: "HomeDetailTableViewCell", bundle: nil), forCellReuseIdentifier: "homeDetailCell")
        tblServiceDetails.register(UINib(nibName: "MediaTableViewCell", bundle: nil), forCellReuseIdentifier: "mediaLinkCell")
        tblServiceDetails.dataSource = dataController
        tblServiceDetails.delegate = dataController
        tblServiceDetails.tableFooterView = UIView()
        addBackButton(withTitle : "Service Details")
        addBlockUserOption()
//        showNavigationBarWithLeftAndRightBarButtonItems(languageTranslator: "language")
        fetchDetailsOfService()
//         NotificationCenter.default.addObserver(self, selector: #selector(JobDetailViewController.languageUpdateNotification(notification:)), name: Notification.Name("LanguageUpdated"), object: nil)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        guard let headerView = tblServiceDetails.tableHeaderView else {
            return
        }
        let size = headerView.systemLayoutSizeFitting(UILayoutFittingCompressedSize)
        
        if headerView.frame.size.height != size.height {
            headerView.frame.size.height = size.height
            tblServiceDetails.tableHeaderView = headerView
            tblServiceDetails.layoutIfNeeded()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        CURRENT_APP_LANGUAGE = "en"
        SELECTED_LANGUAGE = "English"
    }
    
//    deinit {
//        NotificationCenter.default.removeObserver(self, name: Notification.Name("LanguageUpdated"), object: nil)
//    }

    func fetchDetailsOfService(){
        let worker = EMarketSoftwareWorker()
        worker.fetchServiceDetails(serviceId: idOfService!) { [weak self] (response, error) in
            if response != nil{
                if response?.status == true{
                    self?.setData(responseModel: (response?.datas)!)
                }else{
                    self?.displayToast((response?.message)!)
                }
            }else{
                self?.displayToast(error?.localizedDescription ?? UNKNOWN_ERROR_MSG)
            }
        }
    }
    
    func showListOfLanguages(){
        getLanguages { (languagesArray,completeLanguageDetails) in
            self.arrayLangInfo = completeLanguageDetails
            self.presentLanguagesView(languageArray: languagesArray)
        }
    }
    
    func presentLanguagesView(languageArray : [String]){
        DispatchQueue.main.async {
            let presenter = Presentr(presentationType: .popup)
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "commonViewController") as! CommonViewController
           // controller.isFromHome = true
            controller.isLanguageUpdation = true
            controller.itemsArray = languageArray
            controller.delegate = self
            self.customPresentViewController(presenter, viewController: controller, animated: true)
        }
    }
    
    override func showPopOver(){
        if let popUpController = self.storyboard?.instantiateViewController(withIdentifier: "commonViewController") as? CommonViewController {
            popUpController.delegate = self
            popUpController.itemsArray = [REPORT_ABUSE]
            popUpController.modalPresentationStyle = .popover
            popUpController.preferredContentSize = CGSize(width : self.view.frame.size.width / 1.5 , height : CGFloat(60))
            if let pctrl = popUpController.popoverPresentationController {
                pctrl.barButtonItem = navigationItem.rightBarButtonItem
                pctrl.delegate = self
                self.present(popUpController, animated: true, completion: nil)
            }
        }
    }
    
    
    
    func optionSelected(option: String?) {
        if option == REPORT_ABUSE{
            
            showAbuseCategoryOptions { [weak self] (option) in
                self?.showConfirmationOptionToBlock(itemToMarkAbuse: "Service", completion: { (index) in
                    if index == 0{
                        self?.reportAbuse(category: option)
                    }
                })
            }
        }else{
            CURRENT_APP_LANGUAGE = arrayLangInfo.filter{$0.name == option}[0].code
            SELECTED_LANGUAGE = arrayLangInfo.filter{$0.name == option}[0].name
            translateToCurrentSelectedLanguage()
        }
    }
    
    func reportAbuse(category : String){
        let worker = HomeWorker()
        let reportAbuseModel = ReportAbuseModel()
        reportAbuseModel.rcategory = category
        reportAbuseModel.moid = self.currentServiceModel?.uid
        reportAbuseModel.mid = self.currentServiceModel?.id
        reportAbuseModel.moduleType = "service"
        worker.reportAbuse(model: reportAbuseModel) { [weak self] (response) in
            if response.status == true{
                self?.serviceReportedAbuse?((self?.currentServiceModel?.id)!)
                self?.backAction()
            }
            self?.displayToast(response.message!)
        }
    }
    
    func setData(responseModel : ServiceModel){
        currentServiceModel = responseModel
        DispatchQueue.main.async{ [unowned self] in
            if responseModel.uid == Defaults.getUserID(){
                self.btnContact.isHidden = true
            }
            self.lblServiceTitle.text = responseModel.company
            self.lblIndustry.text = responseModel.industry
            let city: String? = responseModel.city
            let state: String? = responseModel.states
            let country: String? = responseModel.country
            self.lblCountry.text = [city,state,country].compactMap{$0}.joined(separator: ",")
            self.lblState.text = responseModel.pincode
            
            self.lblDescription.text = responseModel.descrip?.htmlDecodedText()
            if let imageUrl = responseModel.logo{
               // self.imageService.sd_setImage(with: imageUrl)
                
                let imageURL = URL(string:(imageUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!))
                
                self.imageService.sd_setImage(with: imageURL, placeholderImage: nil)
            }
            self.dataController.configureTblWithDataSource(responseModel: responseModel, controller: self)
            self.tblServiceDetails.reloadData()
//            if CURRENT_APP_LANGUAGE != PREVIOUS_APP_LANGUAGE{
//                self.translateToCurrentSelectedLanguage()
//            }
        }
    }
    
//    @objc func languageUpdateNotification(notification: NSNotification){
//        if currentServiceModel != nil{
//            translateToCurrentSelectedLanguage()
//        }
//        // Take Action on Notification
//    }
    
    
    @IBAction func btnTranslateClicked(_ sender: Any) {
        showListOfLanguages()
    }
    func translateToCurrentSelectedLanguage(){
        let translationWorker = LanguageTranslationWoker()
        var lblArray = [lblDescription]
        let serviceDesc = currentServiceModel?.descrip?.htmlDecodedText()

        translationWorker.translateCurrentString(newLanguageCode: CURRENT_APP_LANGUAGE, stringsToTranslate: [serviceDesc ?? ""]) { (response) in
            if response != nil{
                if let translations = response as? [ReturnedJson]{
                DispatchQueue.main.async {
                    var i = 0
                    for value in translations{

                        for translatedStr in value.translations{
                            lblArray[i]?.text = translatedStr.text
                            i = i + 1


                        }

                    }
                }
                }
            }
        }
    }
    
    @IBAction func contactServiceClicked(_ sender: Any) {
        let nextController = storyboard?.instantiateViewController(withIdentifier: "contactSupplier") as! ContactSupplierViewController
        nextController.idOfUser = currentServiceModel?.id
        nextController.nameOfRecipient = currentServiceModel?.company ?? ""
        nextController.emarketType = EMarketType.Services.rawValue
        navigationController?.pushViewController(nextController, animated: true)
    }
    
    
}

extension ServiceDetailViewController : UIPopoverPresentationControllerDelegate{
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return .none
    }
}
