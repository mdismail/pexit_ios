//
//  EMarketSoftwarePresenter.swift
//  PEXit
//
//  Created by Apple on 15/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation


protocol EMarketSoftwarePresenterInput{
   
    func presentFetchResults(response:[SoftwareModel]?)
    func presentServiceResults(response: [ServiceModel]?,totalCount:Int?)
   
}

protocol EMarketSoftwarePresenterOutput: class{
   
    func displaySearchedResults(response:[SoftwareModel]?)
    func displayServiceResults(response: [ServiceModel]?,totalCount:Int?)
}

class EMarketSoftwarePresenter : EMarketSoftwarePresenterInput{
    weak var output: EMarketSoftwarePresenterOutput!
    func presentFetchResults(response:[SoftwareModel]?) {
        //    print("Fetched records :------>",response.viewModel?.datas_  let jobs = response
        
        output.displaySearchedResults(response: response)
        
    }
    
    func presentServiceResults(response: [ServiceModel]?,totalCount:Int?){
        output.displayServiceResults(response: response,totalCount:totalCount)
    }
  
}
