//
//  DownloadedFilesViewController.swift
//  PEXit
//
//  Created by Apple on 16/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class DownloadedFilesViewController: UIViewController, UITableViewDataSource , UITableViewDelegate{
    @IBOutlet weak var tblDownloadList: UITableView!
    var fileURLs = [URL]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblDownloadList.tableFooterView = UIView()
        addBackButton(withTitle: "Downloaded Softwares")
        fetchListOfDownloadedFiles()
        // Do any additional setup after loading the view.
    }
    
    func fetchListOfDownloadedFiles(){
        let fileManager = FileManager.default
        let documentsURL = fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0]
        do {
            fileURLs = try fileManager.contentsOfDirectory(at: documentsURL, includingPropertiesForKeys: nil)
            fileURLs = fileURLs.filter{!$0.hasDirectoryPath}
            tblDownloadList.reloadData()
        } catch {
            print("Error while enumerating files \(documentsURL.path): \(error.localizedDescription)")
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fileURLs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let downloadCell = tableView.dequeueReusableCell(withIdentifier: "downloadCell", for: indexPath)
        downloadCell.textLabel?.text = fileURLs[indexPath.row].lastPathComponent
        return downloadCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        let url = fileURLs[indexPath.row]
        print(url)
        guard let result = NSData(contentsOf: url) else {
            displayToast("Error opening file.")
            // No data in your fileURL. So no data is received. Do your task if you got no data
            // Keep in mind that you don't have access to your result here.
            // You can return from here.
            return
        }
        if FileManager.default.fileExists(atPath: url.path){
            let activityViewController = UIActivityViewController(activityItems: [url], applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view
            present(activityViewController, animated: true, completion: nil)
        }
        else{
            print("INVALID URL")
        }
        
    }
}
