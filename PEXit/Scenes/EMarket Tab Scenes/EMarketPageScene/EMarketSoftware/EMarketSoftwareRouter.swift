//
//  EMarketSoftwareRouter.swift
//  PEXit
//
//  Created by Apple on 15/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation
import UIKit
import Presentr

protocol EMarketSoftwareRouterInput{
//    func navigateToPopUpController(sender:UIButton)
}

class EMarketSoftwareRouter : NSObject, EMarketSoftwareRouterInput{
   
    weak var controller : EMarketSSTSoftwareViewController!
     func navigateToLocationSearchScreen(){
        let locationController = controller?.storyboard?.instantiateViewController(withIdentifier: "locationController") as! LocationViewController
        locationController.delegate = controller
        locationController.sender = controller
        controller?.navigationController?.pushViewController(locationController, animated: true)
    }
    
    func navigateToDetailsScreen(serviceId : Int){
        let detailsController = controller?.storyboard?.instantiateViewController(withIdentifier: "serviceDetailViewController") as! ServiceDetailViewController
        detailsController.serviceReportedAbuse = { [weak self] (index) in
            self?.controller.removeAbuseReportedService(id:index)
        }
        detailsController.idOfService = serviceId
        controller?.navigationController?.pushViewController(detailsController, animated: true)
    }
    
    func navigateToSoftwareDetailsScreen(softwareModel : SoftwareModel){
        let detailsController = controller?.storyboard?.instantiateViewController(withIdentifier: "softwareDetailViewController") as! SoftwareDetailViewController
        detailsController.softwareModel = softwareModel
        detailsController.softwareReportedAbuse = { [weak self] (index) in
            self?.controller.removeAbuseReportedSoftware(id: index)
        }
        controller?.navigationController?.pushViewController(detailsController, animated: true)
    }
    
    func navigateToSoftwareDownloadScreen(){
        DispatchQueue.main.async {
            let presenter = Presentr(presentationType: .popup)
            let softwareController = self.controller.storyboard?.instantiateViewController(withIdentifier: "serialSoftwareDownloadController") as! SerialSoftwareDownloadController
           self.controller.customPresentViewController(presenter, viewController: softwareController, animated: true)
        }
    }
    
}




