//
//  EMarketProductViewController.swift
//  PEXit
//
//  Created by Apple on 02/08/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
protocol EMarketProductViewControllerInput{
    func presentFetchedCategories(response:[CategoryResponseModel]?,message: String?)
 
    func displaySearchedResults(response:[ProductSupplierModel]?,totalCount:Int?)
    func presentSupplierResult(response: [SupplierResponseModel]?,totalCount:Int?)
}

protocol EMarketProductViewControllerOutput{
    func fetchCategories()
    func fetchProductList(request: SearchResult.FetchPosts.Request)
}

class EMarketProductViewController: UIViewController , UITableViewDataSource , UITableViewDelegate , EMarketProductViewControllerInput , ItemSelected {
    
    @IBOutlet weak var tblProducts: UITableView!
    @IBOutlet weak var btnSortBy: UIButton!
    @IBOutlet weak var btnProductCondition: UIButton!
    @IBOutlet weak var btnCategory: UIButton!
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var widthConstraint: NSLayoutConstraint!
    @IBOutlet weak var productConditionConstraint: NSLayoutConstraint!
    @IBOutlet weak var categoryWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var constraintCollectionViewHeight: NSLayoutConstraint!
    @IBOutlet weak var btnCategoryHeight: NSLayoutConstraint!
    @IBOutlet weak var collectionViewSearchItems: UICollectionView!
    @IBOutlet weak var searchBarTitle: UISearchBar!
    @IBOutlet weak var searchBarLocation: UISearchBar!
    @IBOutlet weak var lblNumberOfRecords: UILabel!
    @IBOutlet weak var viewHeader: UIView!
    
    @IBOutlet weak var sortByTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    var isItemSelected = false
    var currentSender : UIButton?
    var output : EMarketProductViewControllerOutput!
    var router: EMarketProductRouter!
    var optionsArray : [String]?
    var categoryListArray : [String]?
    var searchArray = [String]()
    var local_keywords = ""
    var global_keywords = ""
    var locationStr = ""
    var productCondition = ""
    var productCategory = ""
    var typeOfRequest = TYPE_OF_REQUEST_PRODUCT.PRODUCT.rawValue
    var isSearchBarClicked = false
    var records = [ProductSupplierModel]()
    var supplierArray = [SupplierResponseModel]()
    var pageNumber = 1
    var isMoreDataAvailable = false
    var marketProductControllerType = "Product"
    var parentController : UIViewController?
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(EMarketProductViewController.handleRefresh(_:)),
                                 for: UIControlEvents.valueChanged)
        refreshControl.tintColor = UIColor.red
        
        return refreshControl
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
    }
    
    func initialSetup(){
        constraintCollectionViewHeight.constant = 0
        collectionViewSearchItems.register(UINib(nibName: "JobSearchCell", bundle: nil), forCellWithReuseIdentifier: "JobSearchCell")
        tblProducts.tableFooterView = UIView()
        tblProducts.rowHeight = UITableViewAutomaticDimension
        tblProducts.estimatedRowHeight = 150
        tblProducts.register(UINib(nibName: "EMarketProductCell", bundle: nil), forCellReuseIdentifier: "marketProductCell")
        tblProducts.register(UINib(nibName: "SupplierTableViewCell", bundle: nil), forCellReuseIdentifier: "supplierCell")
        btnSortBy.setTitle("Product", for: .normal)
        btnProductCondition.setTitle("All Types", for: .normal)
        btnCategory.setTitle("All Categories", for: .normal)
        btnSortBy.tag = BUTTON_TAGS.SORTBY.rawValue
        btnProductCondition.tag = BUTTON_TAGS.PRODUCTCONDITION.rawValue
        btnCategory.tag = BUTTON_TAGS.PRODUCTCATEGORY.rawValue
        tblProducts.addSubview(refreshControl)
        //marketProductControllerType = "Product"
    }
    
    func removeAbuseReportedProduct(id : Int){
        DispatchQueue.main.async{
            print("Previous records count--->",self.records.count)
            self.records = self.records.filter{$0.id != id}
            print("Next records count--->",self.records.count)
            ProductSupplierModel.deleteAll_withPId(Id:id)
            self.tblProducts.reloadData()
        }
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        isSearchBarClicked = false

        if !isItemSelected{
            pageNumber = 1
            if (!marketProductControllerType.isEmpty){
                TYPE_OF_REQUEST_PRODUCT.PRODUCT.rawValue == marketProductControllerType ? UISetUpForProducts() : UISetUpForSuppliers()
                //marketProductControllerType = ""
            }
            fetchProductsFromServer(pageNumber: pageNumber)
        
        }
        isItemSelected = false
    }
    
   override func searchLocallyUsingGlobal_subview(searchKeyword : String, searchOptions : String){
    
        parentController?.addGlobalSearchRefreshBtn()
        global_keywords = searchKeyword
        isItemSelected = false
        currentSender = btnSortBy
        //marketProductControllerType = ""
        if searchOptions == GLOBAL_SEARCH_TYPE.PRODUCTS.rawValue{
            marketProductControllerType = "Product"
            typeOfRequest = TYPE_OF_REQUEST_PRODUCT.PRODUCT.rawValue
        }
        else if searchOptions == GLOBAL_SEARCH_TYPE.PRODUCTSUPPLIERS.rawValue{
            marketProductControllerType = "Supplier"
            typeOfRequest = TYPE_OF_REQUEST_PRODUCT.SUPPLIER.rawValue

        }
    }
    
    override func cleanUpGlobalSearch_subview(){
        parentController?.removeGlobalSearchRefreshBtn()
        pageNumber = 1
        productCategory = ""
        productCondition = ""
        btnCategory.setTitle("All Categories", for: .normal)
        btnProductCondition.setTitle("All Types", for: .normal)
        locationStr = ""
        local_keywords = ""
        global_keywords = ""
        isItemSelected = false
        searchArray.removeAll()
        if let collection = collectionViewSearchItems{
            collection.reloadData()
            resizeCollectionViewSize()
        }
        //marketProductControllerType = ""
    }
    
    override func viewDidAppear(_ animated: Bool){
        super.viewDidAppear(animated)
        searchBarTitle.customizeSearchBar(placeholderStr:"Search By Title,Keyword Or Company",rightImageStr:"Search_gray")
       searchBarLocation.customizeSearchBar(placeholderStr:"Location",rightImageStr:"loc_gray")
    }
    
    override func awakeFromNib() {
        EMarketProductConfigurator.sharedInstance.configure(viewController: self)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        guard let headerView = tblProducts.tableHeaderView else {
            return
        }
        
        // The table view header is created with the frame size set in
        // the Storyboard. Calculate the new size and reset the header
        // view to trigger the layout.
        
        // Calculate the minimum height of the header view that allows
        // the text label to fit its preferred width.
        
        let size = headerView.systemLayoutSizeFitting(UILayoutFittingCompressedSize)
        
        if headerView.frame.size.height != size.height {
            headerView.frame.size.height = size.height
            
            // Need to set the header view property of the table view
            // to trigger the new layout. Be careful to only do this
            // once when the height changes or we get stuck in a layout loop.
            
            tblProducts.tableHeaderView = headerView
            
            // Now that the table view header is sized correctly have
            // the table view redo its layout so that the cells are
            // correcly positioned for the new header size.
            
            // This only seems to be necessary on iOS 9.
            
            tblProducts.layoutIfNeeded()
        }
    }
    
    func insertSearchItems(itemStr : String){
        searchArray.insert(itemStr, at: searchArray.count)
        resizeCollectionViewSize()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.collectionViewSearchItems.reloadData()
        }
    }
    
    @IBAction func btnDropDownClicked(_ sender: UIButton) {
        self.view.endEditing(true)
        currentSender = sender
        switch BUTTON_TAGS(rawValue: sender.tag)!   {
        case .SORTBY:
            optionsArray = ["Product","Suppliers"]
        case .PRODUCTCONDITION:
            optionsArray = PRODUCT_CONDITION_ARRAY
        case .PRODUCTCATEGORY:
            if categoryListArray?.count > 0{
                optionsArray = categoryListArray
            }else{
                fetchCategories()
                return
            }
            
        default :
            print("No option choosen")
        }
        router.navigateToPopUpController(sender: sender)
    }
    
    func fetchCategories(){
        output.fetchCategories()
    }
    
    func presentFetchedCategories(response:[CategoryResponseModel]?,message: String?){
        if response != nil{
            DispatchQueue.main.async{
                self.optionsArray = response!.map{$0.category!}
                self.optionsArray?.insert("All Categories", at: 0)
                self.categoryListArray = self.optionsArray
                self.router.navigateToPopUpController(sender: self.currentSender!)
            }
            
        }else{
            displayToast(message!)
        }
    }
    
    
    
    @objc func handleRefresh(_ refreshControl : UIRefreshControl){
        refresh_SubView()
        refreshControl.endRefreshing()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            self.tblProducts.contentOffset = CGPoint.zero
        }
    }
    
    override func refresh_SubView() {
        if tblProducts != nil{
            self.records.removeAll()
            isMoreDataAvailable = true
            pageNumber = 1
            fetchProductsFromServer(pageNumber: pageNumber)
            tblProducts.reloadData()
        }
    }
    
    
    func fetchProductsFromServer(pageNumber:Int?){
        DispatchQueue.main.async {
            self.records = ProductSupplierModel.fetchProductModels()
            self.tblProducts.reloadData()
        }
        //Generate request to fetch jobs from Server
        var request = SearchResult.FetchPosts.Request()
        request.page = pageNumber
        request.records = NUMBER_OF_RECORDS
        request.type = typeOfRequest == TYPE_OF_REQUEST_PRODUCT.PRODUCT.rawValue ? 3 : 4
        var keywords = local_keywords
        if !global_keywords.isEmpty{
            if (!keywords.isEmpty){
                keywords = keywords + "," + global_keywords
            } else {
                keywords = global_keywords
            }
        }
        request.key = keywords
        request.location = locationStr
        request.pctype = productCondition
        request.category = productCategory
        if !keywords.isEmpty{
            let totalKeywords = keywords.components(separatedBy:",")
            if totalKeywords.count > 1{
                request.flag = 1
            }else{
                request.flag = 0
            }
        }
        output.fetchProductList(request: request)
    }
    
     func displaySearchedResults(response:[ProductSupplierModel]?,totalCount:Int?) {
        DispatchQueue.main.async { [weak self] in
            if response?.count < 10{
                self?.isMoreDataAvailable = false
            }else{
                self?.isMoreDataAvailable = true
            }
            self?.records.removeAll()
            self?.records.append(contentsOf: response!)
            if self?.records.count > 0{
                self?.lblNumberOfRecords.text = "\(self?.records.count ?? 0)  of \(totalCount ?? 0) result"
            }else{
               self?.lblNumberOfRecords.text = NO_PRODUCT_FOUND
            }
            
            self?.tblProducts.tableFooterView?.isHidden = true
            self?.tblProducts.reloadData()
        }
    }
    
     func presentSupplierResult(response: [SupplierResponseModel]?,totalCount:Int?){
        DispatchQueue.main.async { [weak self] in
            if response?.count < 10{
                self?.isMoreDataAvailable = false
            }else{
                self?.isMoreDataAvailable = true
            }
            
            if self?.pageNumber == 1{
                self?.supplierArray.removeAll()
            }
            //supplierArray = response
            self?.supplierArray.append(response!)
            //self?.lblNumberOfRecords.text = "\(self?.supplierArray.count ?? 0) result"
            if self?.supplierArray.count > 0{
                self?.lblNumberOfRecords.text = "\(self?.supplierArray.count ?? 0)  of \(totalCount ?? 0) result"
            }else{
                self?.lblNumberOfRecords.text = NO_PRODUCT_FOUND
            }
            self?.tblProducts.tableFooterView?.isHidden = true
            self?.tblProducts.reloadData()
        }
    }
    
    
    func UISetUpForProducts(){
        btnSortBy.setTitle("Products",for:.normal)
        lblCategory.isHidden = false
        btnCategory.isHidden = false
        btnCategoryHeight.constant = 30
        viewDidLayoutSubviews()
    }
    
    func UISetUpForSuppliers(){
        btnSortBy.setTitle("Suppliers",for:.normal)
        lblCategory.isHidden = true
        btnCategory.isHidden = true
        btnCategoryHeight.constant = 0
        tblProducts.reloadData()
        viewDidLayoutSubviews()
        tblProducts.layoutIfNeeded()
    }
    
    //MARK:- TABLE VIEW METHODS
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return typeOfRequest == TYPE_OF_REQUEST_PRODUCT.PRODUCT.rawValue ? records.count : supplierArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if typeOfRequest == TYPE_OF_REQUEST_PRODUCT.PRODUCT.rawValue{
            let productCell = tableView.dequeueReusableCell(withIdentifier: "marketProductCell", for: indexPath) as! EMarketProductCell
//            productCell.btnContactSupplier.tag = indexPath.row
            productCell.setProductData(productModel: records[indexPath.row])
//            productCell.btnContactSupplier.addTarget(self, action: #selector(contactSupplierClicked(sender:)), for: .touchUpInside)
            return productCell
        }else{
            let supplierCell = tableView.dequeueReusableCell(withIdentifier: "supplierCell", for: indexPath) as! SupplierTableViewCell
//            supplierCell.btnContactSupplier.tag = indexPath.row
            supplierCell.setSupplierData(supplierResponse: supplierArray[indexPath.row])
//            supplierCell.btnContactSupplier.addTarget(self, action: #selector(contactSupplierClicked(sender:)), for: .touchUpInside)
            return supplierCell
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath){
        if isMoreDataAvailable == true && indexPath.row == (typeOfRequest == TYPE_OF_REQUEST_PRODUCT.PRODUCT.rawValue ? records.count - 1 : supplierArray.count - 1){
            isMoreDataAvailable = false
            pageNumber = pageNumber + 1
            tableView.tableFooterView = LoadingIndicator.init(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44)))
            tableView.tableFooterView?.isHidden = false
            fetchProductsFromServer(pageNumber: pageNumber)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        isItemSelected = true
        if typeOfRequest == TYPE_OF_REQUEST_PRODUCT.PRODUCT.rawValue{
            router.navigateToProductDetailScreen(productModel:records[indexPath.row])
        }else{
            router.navigateToProductProfileScreen(productModel: supplierArray[indexPath.row])
        }
        
    }
    
    @objc func contactSupplierClicked(sender:UIButton){
        if typeOfRequest == TYPE_OF_REQUEST_PRODUCT.PRODUCT.rawValue{
            let productModel = records[sender.tag]
            router.navigateToContactSupplierController(supplierId: productModel.uid, supplierName: productModel.sname)
        }else{
            let supplierModel = supplierArray[sender.tag]
            router.navigateToContactSupplierController(supplierId: supplierModel.uid!, supplierName: supplierModel.name)
        }
        
    }
    
    
    func optionSelected(option: String?) {
        pageNumber = 1
        isMoreDataAvailable = true
        
        if isSearchBarClicked == true {
            isSearchBarClicked = false
            if let index = searchArray.index(where: { $0 == locationStr}) {
                searchArray.remove(at: index)
            }
            locationStr = option!
            insertSearchItems(itemStr: locationStr)
            supplierArray.removeAll()
            tblProducts.reloadData()
           // fetchProductsFromServer(pageNumber: pageNumber)
            return
        }
        currentSender?.setTitle(option!, for: .normal)
        switch currentSender{
        
        case btnSortBy:
  //          currentSender?.setTitle(option!, for: .normal)
//            currentSender?.textChanged(title: option!, iconName: "downArrow", widthConstraints: widthConstraint)
            typeOfRequest = (btnSortBy.titleLabel?.text)!
            typeOfRequest == TYPE_OF_REQUEST_PRODUCT.PRODUCT.rawValue ? UISetUpForProducts() : UISetUpForSuppliers()
            marketProductControllerType = typeOfRequest
            cleanUpGlobalSearch_subview()
            fetchProductsFromServer(pageNumber: pageNumber)
        case btnCategory:
 //           currentSender?.textChanged(title: option!, iconName: "downArrow", widthConstraints: categoryWidthConstraint)
            if option == "All Categories"{
                productCategory = ""
            }else{
                productCategory = option!

            }
            fetchProductsFromServer(pageNumber: pageNumber)
        case btnProductCondition:
//            currentSender?.textChanged(title: option!, iconName: "downArrow", widthConstraints: productConditionConstraint)
            
            switch option{
            case PRODUCT_CONDITION.AllTypes.rawValue:
                productCondition = ""
            case PRODUCT_CONDITION.New.rawValue:
                productCondition = "new"
            case PRODUCT_CONDITION.Used.rawValue:
                productCondition = "refurbished"
            case PRODUCT_CONDITION.NotUsed.rawValue:
                productCondition = "used"
            default:
                productCondition = ""
            }
            supplierArray.removeAll()
            tblProducts.reloadData()
            fetchProductsFromServer(pageNumber: pageNumber)
        default:
            break
            
        }
    }
    
}


extension EMarketProductViewController : UISearchBarDelegate , UICollectionViewDataSource , UICollectionViewDelegate , UICollectionViewDelegateFlowLayout{
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        if searchBar == searchBarLocation{
            isSearchBarClicked = true
            router.navigateToLocationSearchScreen()
            searchBarLocation.resignFirstResponder()
            return false
        }
        return true
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        /* if let index = searchArray.index(where: { $0 == keywords}) {
            searchArray.remove(at: index)
        } */
        if let searchText = searchBar.text{
            if searchText.isEmpty{
                displayToast(ENTER_SEARCH_KEYWORDS)
                return
            }
            if local_keywords != ""{
                local_keywords = local_keywords + "," + searchText
            }else{
                local_keywords = searchText
            }
            
            if !(searchText.isEmpty){
                insertSearchItems(itemStr: searchText)
            }
        }
        pageNumber = 1
        supplierArray.removeAll()
        tblProducts.reloadData()
        fetchProductsFromServer(pageNumber: pageNumber)
        searchBar.text = ""
        self.view.endEditing(true)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return searchArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        let searchCell = collectionView.dequeueReusableCell(withReuseIdentifier: "JobSearchCell", for: indexPath) as! JobSearchCell
        searchCell.lblSearchText.text = ""
        searchCell.lblSearchText.text = searchArray[indexPath.row]
        searchCell.lblSearchText.sizeToFit()
        searchCell.btnSearch.tag = indexPath.row
        searchCell.btnSearch.addTarget(self, action: #selector(deleteCell(sender:)), for: UIControlEvents.touchUpInside)
        return searchCell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = (searchArray[indexPath.row]).size(withAttributes: nil)
        return CGSize(width:size.width + 50,height:40)
        
    }
    
    func resizeCollectionViewSize(){
        if searchArray.count > 0{
            constraintCollectionViewHeight.constant = 40
            sortByTopConstraint.constant = constraintCollectionViewHeight.constant + 20
        }else{
            constraintCollectionViewHeight.constant = 0
            sortByTopConstraint.constant = 20
        }
       
    }
    
    @objc func deleteCell(sender:UIButton){
        if locationStr == searchArray[sender.tag]{
            locationStr = ""
        }else{
            var strArray = local_keywords.components(separatedBy: ",")
            print(local_keywords)
            print(strArray)
            strArray = strArray.filter{$0 != searchArray[sender.tag]}
            print(strArray)
            local_keywords = strArray.joined(separator: ",")
            print(local_keywords)
            // keywords = searchArray.joined(separator: ",")
        }
        
        searchArray.remove(at: sender.tag)
        collectionViewSearchItems.reloadData()
        resizeCollectionViewSize()
        pageNumber = 1
        supplierArray.removeAll()
        tblProducts.reloadData()
        fetchProductsFromServer(pageNumber: pageNumber)
    }
    
}
