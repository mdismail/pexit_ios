//
//  EMarketProductRouter.swift
//  PEXit
//
//  Created by Apple on 03/08/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation
import UIKit

protocol EMarketProductRouterInput{
    func navigateToPopUpController(sender:UIButton)
}

class EMarketProductRouter : NSObject, EMarketProductRouterInput , UIPopoverPresentationControllerDelegate {
    
    weak var controller : EMarketProductViewController!
    
    func navigateToPopUpController(sender:UIButton) {
        let popUpController = controller.storyboard?.instantiateViewController(withIdentifier: "commonViewController") as! CommonViewController
        popUpController.modalPresentationStyle = .popover
        popUpController.preferredContentSize = CGSize(width : controller.view.frame.size.height - sender.frame.origin.x - 10 , height : CGFloat((controller.optionsArray?.count)! * COMMON_CONTROLLER_ROW_HEIGHT) + 10.0)
        let popOver = popUpController.popoverPresentationController
        popOver?.delegate = self
        popOver?.permittedArrowDirections = .up
        popOver?.sourceView = sender
        popOver?.sourceRect = sender.bounds
        popUpController.delegate = controller
        popUpController.itemsArray = controller.optionsArray
        controller.present(popUpController, animated: true, completion: nil)
    }
    
    func navigateToContactSupplierController(supplierId : Int, supplierName : String?){
        let nextController = controller.storyboard?.instantiateViewController(withIdentifier: "contactSupplier") as! ContactSupplierViewController
        nextController.idOfUser = supplierId
        nextController.nameOfRecipient = supplierName ?? ""
        nextController.emarketType = EMarketType.Supplier.rawValue
        controller.navigationController?.pushViewController(nextController, animated: true)
    }
    
    func navigateToProductDetailScreen(productModel : ProductSupplierModel){
        let nextController = controller.storyboard?.instantiateViewController(withIdentifier: "productDetailController") as! ProductDetailViewController
        nextController.currentProductModel = productModel.id
        nextController.userId = productModel.uid
        nextController.productReportedAbuse = { [weak self] (productIndex) in
            self?.controller?.removeAbuseReportedProduct(id:productIndex)
        }
        controller.navigationController?.pushViewController(nextController, animated: true)
    }
    
    func navigateToProductProfileScreen(productModel : SupplierResponseModel){
        let nextController = controller.storyboard?.instantiateViewController(withIdentifier: "eMarketFullProfileParentController") as! EMarketFullProfileParentController
        nextController.idOfUser = productModel.uid
        controller.navigationController?.pushViewController(nextController, animated: true)
    }
    
    func navigateToLocationSearchScreen(){
        let locationController = controller?.storyboard?.instantiateViewController(withIdentifier: "locationController") as! LocationViewController
        locationController.delegate = controller
        locationController.sender = controller
        controller?.navigationController?.pushViewController(locationController, animated: true)
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return .none
    }
    
    
    
}




