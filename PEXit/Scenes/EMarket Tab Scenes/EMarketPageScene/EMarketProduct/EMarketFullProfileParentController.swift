//
//  EMarketFullProfileParentController.swift
//  PEXit
//
//  Created by Apple on 11/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class EMarketFullProfileParentController: UIViewController {
    var idOfUser : Int!
    var aboutUsController : EMarketAboutUsController!
    override func viewDidLoad() {
        super.viewDidLoad()
        addBackButton(withTitle: "Full Profile")
//        showNavigationBarWithLeftAndRightBarButtonItems(languageTranslator: "language")
        fetchSupplierDetails()
        //setUpView()
        // Do any additional setup after loading the view.
    }

    func fetchSupplierDetails(){
        let worker = EMarketProductWorker()
        worker.getSupplierDetails(supplierId: idOfUser) { [weak self](response, error) in
            if response != nil{
                if response?.status == true{
                    self?.setUpView(response: response?.datas)
                }else{
                    self?.displayToast(response?.message ?? UNKNOWN_ERROR_MSG)
                }
            }
            else{
                self?.displayToast(error?.localizedDescription ?? UNKNOWN_ERROR_MSG)
            }
        }
    }
    
    func setUpView(response : FullProfileDetailModel?){
        aboutUsController = storyboard?.instantiateViewController(withIdentifier: "eMarketAboutUsController") as! EMarketAboutUsController
        aboutUsController.supplierResponseModel = response
        aboutUsController.parentController = self
        let productsController = storyboard?.instantiateViewController(withIdentifier: "eMarketProductsViewController") as! EMarketProductsViewController
        productsController.parentController = self
        productsController.supplierResponseModel = response
        let contactUsController = storyboard?.instantiateViewController(withIdentifier: "eMarketContactUsViewController") as! EMarketContactUsViewController
        contactUsController.parentController = self
        contactUsController.supplierResponseModel = response
        DispatchQueue.main.async{
            let appController = AppTabsController(viewControllers:[self.aboutUsController,productsController,contactUsController])
            self.add(asChildViewController: appController)
        }
        
    }
    
    @IBAction func unwindToProductProfile(segue:UIStoryboardSegue) {
        fetchSupplierDetails()
    }
   
//    override func showLanguages(){
//        aboutUsController.displayLanguages()
//    }
    
    

}
