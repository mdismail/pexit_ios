//
//  EMarketProductsViewController.swift
//  PEXit
//
//  Created by Apple on 11/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class EMarketProductsViewController: UIViewController , UITableViewDataSource , UITableViewDelegate{
    var supplierResponseModel : FullProfileDetailModel?
    @IBOutlet weak var tblProducts: UITableView!
    @IBOutlet weak var btnViewAll: UIButton!
    var currentArray : [ProductListModel]?
    var selectedIndexPath : IndexPath?
    var parentController : UIViewController?
    override func viewDidLoad() {
        super.viewDidLoad()
        tblProducts.register(UINib(nibName: "ProductDetailCell", bundle: nil), forCellReuseIdentifier: "productDetailCell")
        currentArray = supplierResponseModel?.productList?.filter{$0.cName != "All"}
        prepareTabItem()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        parentController?.navigationItem.rightBarButtonItem = nil
    }

    
    // Custom Actions
    
    @IBAction func btnViewAllClicked(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.isSelected{
            btnViewAll.setTitle("By Category", for: .normal)
            currentArray = supplierResponseModel?.productList?.filter{$0.cName == "All"}
        }else{
            btnViewAll.setTitle("View All", for: .normal)
            currentArray = supplierResponseModel?.productList?.filter{$0.cName != "All"}
        }
        tblProducts.reloadData()
        
    }
    
    //MARK: - TableView DataSource & Delegate Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return currentArray?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 160
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int){
        view.tintColor = UIColor.clear
        let header = view as! UITableViewHeaderFooterView
        header.textLabel?.textColor = THEME_LIGHT_GRAY_COLOR
        header.textLabel?.text =  header.textLabel?.text?.capitalized
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return currentArray?[section].cName
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currentArray?[section].pList?.count > 0 ? 1 : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let productDetailCell = tableView.dequeueReusableCell(withIdentifier: "productDetailCell", for: indexPath) as! ProductDetailCell
    productDetailCell.collectionViewGallery.register(UINib(nibName: "ProductProfileCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "productProfileCell")
        productDetailCell.collectionViewGallery.tag = indexPath.section
        productDetailCell.btnViewDetail.isHidden = true
        productDetailCell.imageViewPdfThumbnail.isHidden = true
        //productDetailCell.pdfActivityIndicator.isHidden = true
        productDetailCell.collectionViewGallery.dataSource = self
        productDetailCell.collectionViewGallery.delegate = self
        productDetailCell.collectionViewGallery.reloadData()
        return productDetailCell
    }
    
    
}

extension EMarketProductsViewController : UICollectionViewDataSource, UICollectionViewDelegate , UICollectionViewDelegateFlowLayout {
    fileprivate func prepareTabItem() {
        tabItem.title = "Products"
    }
    
    //MARK: - CollectionView DataSource & Delegate Methods
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return currentArray?[collectionView.tag].pList?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
         let productProfileCell = collectionView.dequeueReusableCell(withReuseIdentifier: "productProfileCell", for: indexPath) as! ProductProfileCollectionViewCell
        productProfileCell.setProductData(productModel: (currentArray?[collectionView.tag].pList![indexPath.row])!)
        
//        let supplierProductCell = collectionView.dequeueReusableCell(withReuseIdentifier: "supplierCollectionViewCell", for: indexPath) as! SupplierCollectionViewCell
//        supplierProductCell.setSupplierProductData(productModel: (currentArray?[collectionView.tag].pList![indexPath.row])!)
//        
//        
        
        return productProfileCell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            return CGSize.init(width: collectionView.frame.size.width / 2.5, height: collectionView.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if self.navigationController?.viewControllers.filter({$0 is ProductDetailViewController}).count > 0{
            selectedIndexPath = indexPath
            performSegue(withIdentifier: "unwindToProductDetails", sender: collectionView.tag)
        }
        else{
            let productDetailController = self.storyboard?.instantiateViewController(withIdentifier: "productDetailController") as! ProductDetailViewController
            productDetailController.userId = (currentArray?[collectionView.tag].pList![(indexPath.row)])!.uid
            productDetailController.currentProductModel = (currentArray?[collectionView.tag].pList![(indexPath.row)])!.id
            navigationController?.pushViewController(productDetailController, animated: true)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "unwindToProductDetails"{
            let destination = segue.destination as! ProductDetailViewController
            destination.userId = (currentArray?[sender as! Int].pList![(selectedIndexPath?.row)!])!.uid
            destination.currentProductModel = (currentArray?[sender as! Int].pList![(selectedIndexPath?.row)!])!.id
        }
    }
}
