//
//  EMarketProductWorker.swift
//  PEXit
//
//  Created by Apple on 03/08/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation

class EMarketProductWorker{
    func fetchProductCategories(completionHandler: @escaping(_ response : ProductCategoryResponse?) -> Void){
        START_LOADING_VIEW()
        ServiceManager.methodType(requestType: GET_REQUEST, url: GET_CATEGORIES, params: nil ,paramsData: nil,completion: { (response,_ responseData, statusCode) in
            STOP_LOADING_VIEW()
            let decoder = JSONDecoder()
            do{
                //                print("Response issss ---->",response)
                let forwardMessageResponse = try decoder.decode(ProductCategoryResponse.self, from: responseData!)
                completionHandler(forwardMessageResponse)
            }catch{
                DispatchQueue.main.async{
                    if ((response as! [String : Any])["message"] as? String) != nil{
                        KEY_WINDOW?.makeToast((response as! [String : Any])["message"] as? String)
                    }
                }
                
                print("error")
            }
            
        }) { (response, statusCode) in
            STOP_LOADING_VIEW()
        }
    }
    
    func fetchProductLocations(jobStr : String,completionHandler: @escaping(_ response : JobLocationResponseModel?,_ error:Error? ) -> Void) {
        //   START_LOADING_VIEW()
        ServiceManager.methodType(requestType: POST_REQUEST, url: PRODUCT_LOCATIONS, params: ["plkey":jobStr] ,paramsData: nil,completion: { (response,_ responseData, statusCode) in
            STOP_LOADING_VIEW()
            let decoder = JSONDecoder()
            do{
//                print("Response issss ---->",response)
                let jobDetails = try decoder.decode(JobLocationResponseModel.self, from: responseData!)
                completionHandler(jobDetails,nil)
            }catch{
                DispatchQueue.main.async{
                    if ((response as! [String : Any])["message"] as? String) != nil{
                        KEY_WINDOW?.makeToast((response as! [String : Any])["message"] as? String)
                    }
                }
                
                print("error")
            }
            
        }) { (response, statusCode) in
            STOP_LOADING_VIEW()
        }
    }
    
    
    func contactEMarket(params : [String : Any],apiName:String,completionHandler: @escaping(_ response : CommonModel.BaseModel?,_ error:Error? ) -> Void) {
         START_LOADING_VIEW()
        ServiceManager.methodType(requestType: POST_REQUEST, url: apiName, params: params as NSDictionary ,paramsData: nil,completion: { (response,_ responseData, statusCode) in
            STOP_LOADING_VIEW()
            let decoder = JSONDecoder()
            do{
//                print("Response issss ---->",response)
                let contactResponse = try decoder.decode(CommonModel.BaseModel.self, from: responseData!)
                completionHandler(contactResponse,nil)
            }catch{
                DispatchQueue.main.async{
                    if ((response as! [String : Any])["message"] as? String) != nil{
                        KEY_WINDOW?.makeToast((response as! [String : Any])["message"] as? String)
                    }
                }
                
                print("error")
            }
            
        }) { (response, statusCode) in
            STOP_LOADING_VIEW()
        }
    }
    
    func getProductDetails(productID : Int,completionHandler: @escaping(_ response : ProductDetailResponseModel?,_ error:Error? ) -> Void) {
        START_LOADING_VIEW()
        ServiceManager.methodType(requestType: GET_REQUEST, url: GET_PRODUCT_DETAILS(productId: productID), params: nil ,paramsData: nil,completion: { (response,_ responseData, statusCode) in
            STOP_LOADING_VIEW()
            let decoder = JSONDecoder()
            do{
//                print("Response issss ---->",response)
                let contactResponse = try decoder.decode(ProductDetailResponseModel.self, from: responseData!)
                completionHandler(contactResponse,nil)
            }catch{
                DispatchQueue.main.async{
                    if ((response as! [String : Any])["message"] as? String) != nil{
                        KEY_WINDOW?.makeToast((response as! [String : Any])["message"] as? String)
                    }
                }
                
                print("error")
            }
            
        }) { (response, statusCode) in
            STOP_LOADING_VIEW()
        }
    }
    
    
    func getSupplierDetails(supplierId : Int,completionHandler: @escaping(_ response : FullProfileResponseModel?,_ error:Error? ) -> Void) {
        START_LOADING_VIEW()
        ServiceManager.methodType(requestType: GET_REQUEST, url: GET_FULL_PROFILE(supplierID: supplierId), params: nil ,paramsData: nil,completion: { (response,_ responseData, statusCode) in
            STOP_LOADING_VIEW()
            let decoder = JSONDecoder()
            do{
//                print("Response issss ---->",response)
                let contactResponse = try decoder.decode(FullProfileResponseModel.self, from: responseData!)
                completionHandler(contactResponse,nil)
            }catch{
                DispatchQueue.main.async{
                    if ((response as! [String : Any])["message"] as? String) != nil{
                        KEY_WINDOW?.makeToast((response as! [String : Any])["message"] as? String)
                    }
                }
                
                print("error")
            }
            
        }) { (response, statusCode) in
            STOP_LOADING_VIEW()
        }
    }
}


