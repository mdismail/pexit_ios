//
//  ProductModel.swift
//  PEXit
//
//  Created by Apple on 09/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation
import Realm
import RealmSwift
import ObjectMapper

struct ProductCategoryResponse : Codable{
    var message : String?
    var status : Bool?
    var datas : [CategoryResponseModel]?
}

struct CategoryResponseModel : Codable{
    var  category : String?
}

class SupplierModel : Codable{
    var message : String?
    var status : Bool?
    var datas : [SupplierResponseModel]?
}

class SupplierResponseModel : Codable{
    var name : String?
    var location : String?
    var products : String?
    var uid : Int?
    var productList : [SupplierObjectModel]?
}

class ProductDetailResponseModel : Codable{
    var message : String?
    var status : Bool?
    var datas : SupplierObjectModel?
}

class FullProfileResponseModel : Codable{
    var message : String?
    var status : Bool?
    var datas : FullProfileDetailModel?
}

class FullProfileDetailModel : Codable{
    var summary : String?
    var id : Int?
    var address : String?
    var location : String?
    var userstate : String?
    var country : String?
    var phone : String?
    var productList : [ProductListModel]?
    var companyProfile : String?
}

class ProductListModel : Codable{
    var cName : String?
    var pList: [SupplierObjectModel]?
}

class SupplierObjectModel : Codable{
     var id: Int?
     var uid: Int?
     var image: String?
     var name: String?
     var location: String?
     var category: String?
     var descrip: String?
     var prodprf: String?
     var transhis: String?
     var companyProfile: String?
     var sname : String?
     var gallery : GalleryObjectModel?
}

class GalleryObjectModel : Codable{
    var link: [String]?
    var image: [String]?
    var video: [String]?
}

class ProductSupplierModel: Object, Mappable {
    @objc dynamic var id: Int = Int()
    @objc dynamic var uid: Int = Int()
    @objc dynamic var image: String?
    @objc dynamic var name: String?
    @objc dynamic var location: String?
    @objc dynamic var category: String?
    @objc dynamic var descrip: String?
    @objc dynamic var sname : String?
    convenience required init?(map: Map) {
        self.init()
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    
    func mapping(map: Map) {
        id <- map["id"]
        uid <- map["uid"]
        image <- map["image"]
        name <- map["name"]
        location <- map["location"]
        category <- map["category"]
        descrip <- map["descrip"]
        sname <- map["sname"]
    }
    
    
    //    MARK: fetch and delete functions
    static func deleteAll(){
        if let realm = RealmDBManager.sharedManager.getRealm() {
            do {
                try realm.write() {
                    realm.delete(realm.objects(ProductSupplierModel.self))
                    realm.refresh()
                }
            } catch (let error as NSError) {

                print("ERROR: Realm: writing to database, ERROR: \(error.localizedDescription)")
            }
        }
    }

    static func deleteAll_withPId(Id:Int){
        let pred = NSPredicate(format: "id == %d",Id)
        let objectsToDelete = RealmDBManager.sharedManager.fetchObjectsWithPredicate(object: ProductSupplierModel.self, pred: pred)
        if objectsToDelete != nil && (objectsToDelete?.count)! > 0{


            if let realm = RealmDBManager.sharedManager.getRealm() {
                do {
                    try realm.write() {
                        realm.delete(objectsToDelete!)
                        realm.refresh()
                    }
                } catch (let error as NSError) {

                    print("ERROR: Realm: writing to database, ERROR: \(error.localizedDescription)")
                }
            }
        }
    }

    static func fetchProductModels() -> [ProductSupplierModel]{
        let  productModels = RealmDBManager.sharedManager.fetchObjects(object: self) as! [ProductSupplierModel]
        return productModels
    }
}
