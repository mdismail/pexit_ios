//
//  EMarketProductPresenter.swift
//  PEXit
//
//  Created by Apple on 03/08/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation

protocol EMarketProductPresenterInput{
    func presentFetchedCategories(response:ProductCategoryResponse?,message: String?)
    func presentFetchResults(response: [ProductSupplierModel]?,totalCount:Int?)
    func presentSupplierResult(response: [SupplierResponseModel]?,totalCount:Int?)
}

protocol EMarketProductPresenterOutput: class{
    func presentFetchedCategories(response:[CategoryResponseModel]?,message: String?)
    func displaySearchedResults(response:[ProductSupplierModel]?,totalCount:Int?)
    func presentSupplierResult(response: [SupplierResponseModel]?,totalCount:Int?)
}

class EMarketProductPresenter : EMarketProductPresenterInput{
    weak var output: EMarketProductPresenterOutput!
    
    func presentFetchedCategories(response:ProductCategoryResponse?,message: String?){
        if response != nil && response?.status == true{
            output.presentFetchedCategories(response: response?.datas, message: "")
        }else{
            response != nil ? output.presentFetchedCategories(response: nil, message: response?.message) :
                output.presentFetchedCategories(response: nil, message: message)
        }
    }
    
   func presentFetchResults(response: [ProductSupplierModel]?,totalCount:Int?) {
        //    print("Fetched records :------>",response.viewModel?.datas_  let jobs = response
        
        output.displaySearchedResults(response: response,totalCount: totalCount)
        
    }
    
    func presentSupplierResult(response: [SupplierResponseModel]?,totalCount:Int?){
        output.presentSupplierResult(response: response,totalCount: totalCount)
    }
}
