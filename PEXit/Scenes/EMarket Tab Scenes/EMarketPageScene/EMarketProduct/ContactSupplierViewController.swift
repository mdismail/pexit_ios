//
//  ContactSupplierViewController.swift
//  PEXit
//
//  Created by Apple on 03/08/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import Material

enum EMarketType : String{
    case Supplier = "supplier"
    case Services = "service"
}


class ContactSupplierViewController: UIViewController , UITableViewDataSource ,UITableViewDelegate ,  UITextFieldDelegate , UITextViewDelegate{
    @IBOutlet weak var tblList: UITableView!
    var arrayOfTextFields : [TextfieldInfoStruct]?
    var idOfUser : Int?
    var emarketType : String?
    var nameOfRecipient = ""
    var message = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpController()
        tblList.tableFooterView = UIView()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func setUpController(){
        showNavigationBar()
        addBackButton(withTitle: "Contact Supplier")
        tblList.register(UINib(nibName: "InviteContactViewCell", bundle: nil), forCellReuseIdentifier: "inviteContactCell")
        arrayOfTextFields = [TextfieldInfoStruct (placeholderText: "Recipient"),TextfieldInfoStruct(placeholderText: "Sender Name"),TextfieldInfoStruct(placeholderText: "Sender Email"),TextfieldInfoStruct(placeholderText: "Sender Phone")]
    }
    
    //MARK: Get all required fields
    func getAllRequiredFields() -> [TJTextField] {
        var textFieldsArray = [TJTextField]()
        let cells = Utils.cellsForTableView(tblList) as! [InviteContactTableViewCell]
        for cell in cells {
            if !cell.txtFieldInvite.isHidden{
                textFieldsArray.append(cell.txtFieldInvite)
            }
            
        }
        return textFieldsArray
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (arrayOfTextFields?.count)! + 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblList.dequeueReusableCell(withIdentifier: "inviteContactCell") as! InviteContactTableViewCell
        if indexPath.row < arrayOfTextFields?.count{
           
            cell.txtFieldInvite.delegate = self
            cell.txtFieldInvite.tag = indexPath.row
            cell.lblHeader.isHidden = false
            cell.configureTextfield(arrayOfTextFields![indexPath.row])
            cell.txtViewDesc.isHidden = true
            cell.viewBottom.isHidden = true
            cell.btnSned.isHidden = true
        }
        else if indexPath.row == arrayOfTextFields?.count{
            cell.txtViewDesc.delegate = self
            cell.lblHeader.isHidden = true
            cell.txtViewDesc.isHidden = false
            cell.viewBottom.isHidden = false
            cell.txtFieldInvite.isHidden = true
            cell.btnSned.isHidden = true
            cell.lblDescHeader.text = "Message"
           // cell.txtViewDesc.placeholder = "Message"
            
        }
        else{
            cell.btnSned.isHidden = false
            cell.lblHeader.isHidden = true
            cell.btnSned.addTarget(self, action: #selector(contactSupplier(sender:)), for: .touchUpInside)
            cell.txtViewDesc.isHidden = true
            cell.viewBottom.isHidden = true
            cell.txtFieldInvite.isHidden = true
        }
        
        if indexPath.row == 0 && !((nameOfRecipient.isEmpty)){
            cell.txtFieldInvite.text = nameOfRecipient
            cell.txtFieldInvite.isUserInteractionEnabled = false
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch indexPath.row{
        case indexPath.row where indexPath.row < arrayOfTextFields?.count:
            return CGFloat(HEIGHT_OF_CELL)
        case indexPath.row where indexPath.row == arrayOfTextFields?.count:
            return UITableViewAutomaticDimension
        default:
            return 60
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        let selectedIndexPath: IndexPath = IndexPath(row: textField.tag, section: 0)
        arrayOfTextFields![selectedIndexPath.row].textfieldText = textField.text
    }
    
    func textViewDidChange(_ textView: UITextView) {
        message = textView.text
        textView.updateTextViewPlacholder()
        UIView.setAnimationsEnabled(false)
        tblList?.beginUpdates()
        tblList?.endUpdates()
        UIView.setAnimationsEnabled(true)
    }
    
    
    
    @objc func contactSupplier(sender:UIButton){
        if(Utils.validateFormAndShowToastWithRequiredFields(getAllRequiredFields())){
            if !(message.isEmpty){
                contactSupplier(message:message)
            }else{
                displayToast("Please enter message")
                return
            }
            
        }else{
            return
        }
    }
    
    func contactSupplier(message : String?){
        let worker = EMarketProductWorker()
        var params : [String:Any]?
        var apiName : String?
        
        switch emarketType{
        case EMarketType.Supplier.rawValue:
            params = ["sid":idOfUser ?? 0,"emessage":message ?? ""]
            apiName = CONTACT_SUPPLIER
        case EMarketType.Services.rawValue:
            params = ["serid":idOfUser ?? 0,"emessage":message ?? ""]
            apiName = CONTACT_SERVICE
        default:
            return
        }
        
        worker.contactEMarket(params:params!,apiName:apiName!) { [weak self](response, error) in
            if response != nil{
                self?.displayToast(response?.message ?? UNKNOWN_ERROR_MSG)
                if response?.status == true{
                    self?.backAction()
                }
            }else{
                self?.displayToast(UNKNOWN_ERROR_MSG)
            }
        }
    }
    
    
    
}
