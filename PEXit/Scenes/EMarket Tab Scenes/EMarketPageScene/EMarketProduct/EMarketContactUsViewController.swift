//
//  EMarketContactUsViewController.swift
//  PEXit
//
//  Created by Apple on 11/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class EMarketContactUsViewController: UIViewController {
    
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblCity: UILabel!
    @IBOutlet weak var lblState: UILabel!
    @IBOutlet weak var lblCountry: UILabel!
    @IBOutlet weak var lblContact: UILabel!
    var supplierResponseModel : FullProfileDetailModel?
    var parentController : UIViewController?
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareTabItem()
        setData()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        parentController?.navigationItem.rightBarButtonItem = nil
    }

//    func setData(){
//        lblAddress.text =  "Address: \(supplierResponseModel?.address ??? "")"
//        lblCity.text =  "City: \(supplierResponseModel?.location ??? "")"
//        lblState.text =  "State: \(supplierResponseModel?.userstate ??? "")"
//        lblCountry.text =  "Country: \(supplierResponseModel?.country ??? "")"
//        lblContact.text =  "Contact: \(supplierResponseModel?.phone ??? "")"
//    }
    func setData(){
        lblAddress.text =  (supplierResponseModel?.address ??? "")
        lblCity.text =     (supplierResponseModel?.location ??? "")
        lblState.text =    (supplierResponseModel?.userstate ??? "")
        lblCountry.text =  (supplierResponseModel?.country ??? "")
        lblContact.text =  (supplierResponseModel?.phone ??? "")
    }

}
extension EMarketContactUsViewController {
    fileprivate func prepareTabItem() {
        tabItem.title = "Contact Us"
    }
}
