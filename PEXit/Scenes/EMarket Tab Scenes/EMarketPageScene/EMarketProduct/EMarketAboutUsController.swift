//
//  EMarketAboutUsController.swift
//  PEXit
//
//  Created by Apple on 11/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import Presentr

class EMarketAboutUsController: UIViewController , ItemSelected {
    var supplierResponseModel : FullProfileDetailModel?
    @IBOutlet weak var txtViewAboutUs: UITextView!
    @IBOutlet weak var btnCompanyProfile: UIButton!
    var arrayLangInfo = [AllLangDetails]()
    var parentController : UIViewController?
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareTabItem()
        txtViewAboutUs.text = supplierResponseModel?.summary?.htmlDecodedText()
        
        //        if CURRENT_APP_LANGUAGE != PREVIOUS_APP_LANGUAGE{
//            self.translateToCurrentSelectedLanguage()
//        }
//         NotificationCenter.default.addObserver(self, selector: #selector(ProductDetailViewController.languageUpdateNotification(notification:)), name: Notification.Name("LanguageUpdated"), object: nil)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        CURRENT_APP_LANGUAGE = "en"
        SELECTED_LANGUAGE = "English"
    }

    override func viewWillAppear(_ animated: Bool) {
// parentController?.showNavigationBarWithLeftAndRightBarButtonItems(languageTranslator: "language")
    }
    @IBAction func btnTranslateClicked(_ sender: Any) {
        showLanguagesList()
    }
    
    func showLanguagesList(){
        getLanguages { (languagesArray,completeLanguageDetails) in
            self.arrayLangInfo = completeLanguageDetails
            self.presentLanguagesView(languageArray: languagesArray)
        }
    }
    
//    func displayLanguages(){
//        getLanguages { (languagesArray,completeLanguageDetails) in
//            self.arrayLangInfo = completeLanguageDetails
//            self.presentLanguagesView(languageArray: languagesArray)
//        }
//    }
    
    func presentLanguagesView(languageArray : [String]){
        DispatchQueue.main.async {
            let presenter = Presentr(presentationType: .popup)
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "commonViewController") as! CommonViewController
            // controller.isFromHome = true
            controller.isLanguageUpdation = true
            controller.itemsArray = languageArray
            controller.delegate = self
            self.customPresentViewController(presenter, viewController: controller, animated: true)
        }
    }
    
    func optionSelected(option: String?) {
        //        PREVIOUS_APP_LANGUAGE = CURRENT_APP_LANGUAGE
        CURRENT_APP_LANGUAGE = arrayLangInfo.filter{$0.name == option}[0].code
        SELECTED_LANGUAGE = arrayLangInfo.filter{$0.name == option}[0].name
        translateToCurrentSelectedLanguage()
        //        NotificationCenter.default.post(name: Notification.Name("LanguageUpdated"), object: nil)
    }
    
    
//    deinit {
//        NotificationCenter.default.removeObserver(self, name: Notification.Name("LanguageUpdated"), object: nil)
//    }
    
//    @objc func languageUpdateNotification(notification: NSNotification){
//        if supplierResponseModel?.summary?.htmlDecodedText() != nil{
//            translateToCurrentSelectedLanguage()
//        }
//
//        // Take Action on Notification
//
//    }
    
    func translateToCurrentSelectedLanguage(){
        let translationWorker = LanguageTranslationWoker()
        var lblArray = [txtViewAboutUs]
        let productDesc = supplierResponseModel?.summary?.htmlDecodedText()

        translationWorker.translateCurrentString(newLanguageCode: CURRENT_APP_LANGUAGE, stringsToTranslate: [productDesc ?? ""]) { (response) in
            if response != nil{
                if let translations = response as? [ReturnedJson]{
                DispatchQueue.main.async {
                    var i = 0
                    for value in translations{

                        for translatedStr in value.translations{
                            lblArray[i]?.text = translatedStr.text
                            i = i + 1


                        }

                    }
                }
                }
            }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
  
    @IBAction func companyProfileClicked(_ sender: Any) {
        if let companyProfile = supplierResponseModel?.companyProfile , !companyProfile.isEmpty{
            showAttatchmentView(mediaUrl: companyProfile)
        }else{
            displayToast("Company Profile Not Found")
        }
    }
    
}
extension EMarketAboutUsController {
    fileprivate func prepareTabItem() {
        tabItem.title = "About Us"
    }
}
