//
//  ProductDetailViewController.swift
//  PEXit
//
//  Created by Apple on 11/10/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import Presentr

class ProductDetailViewController: UIViewController , UITableViewDataSource , UITableViewDelegate , ProductDetailTapped , ItemSelected {
    
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var lblProductDescription: UILabel!
    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var tblProductDetails: UITableView!
    var arrStatus:NSMutableArray = []
    var arrayLangInfo = [AllLangDetails]()
    var sectionHeaders = ["Product Details","Company Profile","Transaction History","Gallery"]
    var currentProductModel : Int?
    var userId : Int?
    var response : SupplierObjectModel?
    var productReportedAbuse : ((Int) -> Void)?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        clearLabels()
        addBackButton(withTitle: "Product Details")
        addBlockUserOption()
        tblProductDetails.register(UINib(nibName: "ProductDetailCell", bundle: nil), forCellReuseIdentifier: "productDetailCell")
        let headerNib = UINib.init(nibName: "ProfileHeaderView", bundle: Bundle.main)
        tblProductDetails.register(headerNib, forHeaderFooterViewReuseIdentifier: "ProfileHeaderView")
        //
        fetchDetailsOfProduct()
        //         NotificationCenter.default.addObserver(self, selector: #selector(ProductDetailViewController.languageUpdateNotification(notification:)), name: Notification.Name("LanguageUpdated"), object: nil)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        initialSetUp()
    }
    
    func initialSetUp(){
        
        for _ in 0..<sectionHeaders.count
        {
            self.arrStatus.add("0")
        }
        // Do any additional setup after loading the view.
    }
    
    func clearLabels(){
        lblCategory.text = ""
        lblLocation.text = ""
        lblProductName.text = ""
        lblProductDescription.text = ""
    }
    
    
    
    override func viewWillDisappear(_ animated: Bool) {
        CURRENT_APP_LANGUAGE = "en"
        SELECTED_LANGUAGE = "English"
    }
    
    func showLanguagesList(){
        getLanguages { (languagesArray,completeLanguageDetails) in
            self.arrayLangInfo = completeLanguageDetails
            self.presentLanguagesView(languageArray: languagesArray)
        }
    }
    
    
    
    func presentLanguagesView(languageArray : [String]){
        DispatchQueue.main.async {
            let presenter = Presentr(presentationType: .popup)
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "commonViewController") as! CommonViewController
            // controller.isFromHome = true
            controller.isLanguageUpdation = true
            controller.itemsArray = languageArray
            controller.delegate = self
            self.customPresentViewController(presenter, viewController: controller, animated: true)
        }
    }
    
    override func showPopOver(){
        if let popUpController = self.storyboard?.instantiateViewController(withIdentifier: "commonViewController") as? CommonViewController {
            popUpController.delegate = self
            popUpController.itemsArray = [REPORT_ABUSE]
            popUpController.modalPresentationStyle = .popover
            popUpController.preferredContentSize = CGSize(width : self.view.frame.size.width / 1.5 , height : CGFloat(60))
            if let pctrl = popUpController.popoverPresentationController {
                pctrl.barButtonItem = navigationItem.rightBarButtonItem
                pctrl.delegate = self
                self.present(popUpController, animated: true, completion: nil)
            }
        }
    }
    
    func optionSelected(option: String?) {
        if option == REPORT_ABUSE{
            showAbuseCategoryOptions { [weak self] (option) in
                self?.showConfirmationOptionToBlock(itemToMarkAbuse: "Product", completion: { (index) in
                    if index == 0{
                        self?.reportAbuse(category: option)
                    }
                })
            }
        }else{
            CURRENT_APP_LANGUAGE = arrayLangInfo.filter{$0.name == option}[0].code
            SELECTED_LANGUAGE = arrayLangInfo.filter{$0.name == option}[0].name
            translateToCurrentSelectedLanguage()
        }
        
    }

    func reportAbuse(category : String){
        let worker = HomeWorker()
        let reportAbuseModel = ReportAbuseModel()
        reportAbuseModel.rcategory = category
        reportAbuseModel.moid = self.response?.uid
        reportAbuseModel.mid = self.response?.id
        reportAbuseModel.moduleType = "product"
        worker.reportAbuse(model: reportAbuseModel) { [weak self] (response) in
            if response.status == true{
                self?.productReportedAbuse?((self?.response?.id)!)
                self?.backAction()
            }
            self?.displayToast(response.message!)
        }
    }
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        guard let headerView = tblProductDetails.tableHeaderView else {
            return
        }
        let size = headerView.systemLayoutSizeFitting(UILayoutFittingCompressedSize)
        
        if headerView.frame.size.height != size.height {
            headerView.frame.size.height = size.height
            
            tblProductDetails.tableHeaderView = headerView
            tblProductDetails.tableFooterView = UIView()
            tblProductDetails.layoutIfNeeded()
        }
    }
    
    func fetchDetailsOfProduct(){
        let worker = EMarketProductWorker()
        worker.getProductDetails(productID: (currentProductModel)!) { [weak self] (response, error) in
            if response != nil{
                if response?.status == true{
                    self?.setDataOnView(productDetailModel: (response?.datas)!)
                }else{
                    self?.displayToast((response?.message)!)
                }
            }else{
                self?.displayToast(error?.localizedDescription ?? UNKNOWN_ERROR_MSG)
            }
        }
    }
    
    func setDataOnView(productDetailModel : SupplierObjectModel){
        DispatchQueue.main.async{ [unowned self] in
            self.response = productDetailModel
            self.lblProductName.text = productDetailModel.name
            self.lblLocation.text =  "Location: \(productDetailModel.location ??? "")"
            self.lblCategory.text = "Category: \(productDetailModel.category ??? "")"
            self.lblProductDescription.text = productDetailModel.descrip?.htmlDecodedText()
            if let imageUrl = productDetailModel.image{
                let imageURL = URL(string:(imageUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!))
                self.imgProduct.setImage(from: imageURL!)
                
            }
            //            if CURRENT_APP_LANGUAGE != PREVIOUS_APP_LANGUAGE{
            //                self.translateToCurrentSelectedLanguage()
            //            }
            
        }
        
    }
    
    @IBAction func btnTranslateClicked(_ sender: Any) {
        showLanguagesList()
    }
    
    func translateToCurrentSelectedLanguage(){
        let translationWorker = LanguageTranslationWoker()
        var lblArray = [lblProductDescription]
        let productDesc = self.response?.descrip?.htmlDecodedText()
        
        translationWorker.translateCurrentString(newLanguageCode: CURRENT_APP_LANGUAGE, stringsToTranslate: [productDesc ?? ""]) { (response) in
            if response != nil{
                if let translations = response as? [ReturnedJson]{
                    DispatchQueue.main.async {
                        var i = 0
                        for value in translations{
                            
                            for translatedStr in value.translations{
                                lblArray[i]?.text = translatedStr.text
                                i = i + 1
                                
                                
                            }
                            
                        }
                    }
                }
                
            }
        }
    }
    
    @IBAction func contactSupplierClicked(_ sender: UIButton) {
        let nextController = storyboard?.instantiateViewController(withIdentifier: "contactSupplier") as! ContactSupplierViewController
        nextController.nameOfRecipient = response?.sname ?? ""
        nextController.idOfUser = response?.uid
        nextController.emarketType = EMarketType.Supplier.rawValue
        navigationController?.pushViewController(nextController, animated: true)
    }
    
    @objc func headerCellButtonTapped(_sender: UIButton)
    {
        let str:String = arrStatus[_sender.tag] as! String
        if str == "0"{
            arrStatus[_sender.tag] = "1"
        }else{
            arrStatus[_sender.tag] = "0"
        }
        tblProductDetails.reloadData()
        tblProductDetails.layoutIfNeeded()
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "detailToFullProfile" || segue.identifier ==  "unwindToProductDetails"{
            let destinationController = segue.destination as! EMarketFullProfileParentController
            //  destinationController.idOfUser = currentProductModel?.uid
            destinationController.idOfUser = userId
        }
    }
    @IBAction func btnViewFullProfileClicked(_ sender: Any) {
        if self.navigationController?.viewControllers.filter({$0 is EMarketFullProfileParentController}).count > 0{
            self.navigationController?.popViewController(animated: true)
            //performSegue(withIdentifier: "unwindToProductProfile", sender: self)
        }else{
            performSegue(withIdentifier: "detailToFullProfile", sender: self)
        }
    }
    
    @IBAction func unwindToProductDetails(segue:UIStoryboardSegue) {
        fetchDetailsOfProduct()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return CGFloat(HEADER_HEIGHT)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tblProductDetails.dequeueReusableHeaderFooterView(withIdentifier: "ProfileHeaderView") as! ProfileHeaderView
        headerView.headerTitle.text = sectionHeaders[section]
        headerView.btnAddRecord.tag = section
        headerView.btnAddRecord.addTarget(self, action: #selector(ProductDetailViewController.headerCellButtonTapped(_sender:)), for: UIControlEvents.touchUpInside)
        let str:String = arrStatus[section] as! String
        if str == "0"
        {
            UIView.animate(withDuration: 2) { () -> Void in
                headerView.btnAddRecord.setImage(UIImage(named : "drop"), for: .normal)
                let angle =  CGFloat(Double.pi * 2)
                let tr = CGAffineTransform.identity.rotated(by: angle)
                headerView.btnAddRecord.imageView?.transform = tr
                
            }
        }
        else
        {
            UIView.animate(withDuration: 2) { () -> Void in
                headerView.btnAddRecord.setImage(UIImage(named : "up"), for: .normal)
                let angle =  CGFloat(Double.pi * 2)
                let tr = CGAffineTransform.identity.rotated(by: angle)
                headerView.btnAddRecord.imageView?.transform = tr
            }
        }
        return headerView
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        let str:String = arrStatus[section] as! String
        switch section{
        case 0 :
            return str == "0" ? 0 : (response?.prodprf != nil && !((response?.prodprf!.isEmpty)!) ? 1 : 0)
        case 1 :
            return str == "0" ? 0 : (response?.companyProfile != nil && !((response?.companyProfile!.isEmpty)!) ? 1 : 0)
        case 2 :
            return str == "0" ? 0 :(response?.transhis != nil && !((response?.transhis!.isEmpty)!) ? 1 : 0)
        case 3:
            return str == "0" ? 0 :1
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let productDetailCell = tableView.dequeueReusableCell(withIdentifier: "productDetailCell", for: indexPath) as! ProductDetailCell
        productDetailCell.delegate = self
        productDetailCell.btnViewDetail.tag = indexPath.section
        switch indexPath.section{
        case 0:
            hideShowViews(cell: productDetailCell, hide: true)
            productDetailCell.setData(urlString: response?.prodprf)
        case 1:
            hideShowViews(cell: productDetailCell, hide: true)
            productDetailCell.setData(urlString: response?.companyProfile)
        case 2:
            hideShowViews(cell: productDetailCell, hide: true)
            productDetailCell.setData(urlString: response?.transhis)
        case 3:
            productDetailCell.collectionViewGallery.register(UINib(nibName: "HomeDetailCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "homeDetailsCell")
            hideShowViews(cell: productDetailCell, hide: false)
            productDetailCell.collectionViewGallery.dataSource = self
            productDetailCell.collectionViewGallery.delegate = self
            productDetailCell.collectionViewGallery.reloadData()
        default:
            productDetailCell.setData(urlString: response?.transhis)
        }
        return productDetailCell
    }
    
    func hideShowViews(cell:ProductDetailCell,hide:Bool){
        cell.collectionViewGallery.isHidden = hide
        cell.btnViewDetail.isHidden = !hide
        cell.imageViewPdfThumbnail.isHidden = !hide
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return sectionHeaders.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        switch indexPath.section{
        case 0, 1, 2:
            return self.view.frame.size.width
        case 3:
            if response?.gallery != nil{
                return ((response?.gallery?.link?.count)! + (response?.gallery?.image?.count)! + (response?.gallery?.video?.count)! > 0) ? 130 : 0
            }else{
                return 0
            }
            
        default:
            return 140
        }
    }
    
    func btnTapped(section: Int) {
        switch section{
        case 0:
            response?.prodprf != nil && !((response?.prodprf?.isEmpty)!) ? showAttatchmentView(mediaUrl:(response?.prodprf)!) : displayToast("Product Profile Not Found")
            
        case 1:
            response?.companyProfile != nil && !((response?.companyProfile?.isEmpty)!) ? showAttatchmentView(mediaUrl:(response?.companyProfile)!) : displayToast("Company Profile Not Found")
            
        case 2:
            response?.transhis != nil && !((response?.transhis?.isEmpty)!) ? showAttatchmentView(mediaUrl:(response?.transhis)!) : displayToast("Transaction History Not Found")
            
        default:
            response?.prodprf != nil && !((response?.prodprf?.isEmpty)!) ? showAttatchmentView(mediaUrl:(response?.prodprf)!) : displayToast("Product Profile Not Found")
            
        }
    }
    
}

extension ProductDetailViewController : UICollectionViewDataSource , UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        switch section{
        case 0:
            return response?.gallery?.image?.count ?? 0
        case 1:
            return response?.gallery?.video?.count ?? 0
        case 2:
            return response?.gallery?.link?.count ?? 0
        default:
            return 0
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "homeDetailsCell", for: indexPath) as! HomeDetailCollectionViewCell
        cell.imgViewMedia.image = nil
        cell.btnDelete.isHidden = true
        cell.currentIndexPath = indexPath
        switch indexPath.section{
        case 0:
            cell.setUpViewForImage(url:response?.gallery?.image![indexPath.row])
        case 1:
            cell.setUpViewForVideo(videoUrl: response?.gallery?.video![indexPath.row])
        case 2:
            cell.setUpViewForYouTubeVideo(youTubeVideoUrl:response?.gallery?.link![indexPath.row])
        default:
            print("test")
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: collectionView.frame.size.width / 3, height: collectionView.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch indexPath.section{
        case 0:
            let photoController = AppToolbarController(rootViewController: PhotoViewController(imagesArray:(response?.gallery?.image)!,index: indexPath.row))
            present(photoController, animated: true, completion: nil)
        case 1:
            openVideoPlayer(videoUrl: (response?.gallery?.video![indexPath.row])!)
        case 2:
            print("Default Statement")
        default :
            print("Default Statement")
            
        }
    }
}

extension ProductDetailViewController : UIPopoverPresentationControllerDelegate{
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return .none
    }
}
