//
//  EMarketInteractor.swift
//  PEXit
//
//  Created by Apple on 03/08/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation
protocol EMarketProductInteractorInput{
    func fetchCategories()
    func fetchProductList(request: SearchResult.FetchPosts.Request)
}


protocol EMarketProductInteractorOutput{
    func presentFetchedCategories(response:ProductCategoryResponse?,message: String?)
    func presentFetchResults(response: [ProductSupplierModel]?,totalCount:Int?)
    func presentSupplierResult(response: [SupplierResponseModel]?,totalCount:Int?)
}

class EMarketProductInteractor : EMarketProductInteractorInput{
    var output: EMarketProductInteractorOutput!
    var worker = EMarketProductWorker()
    var homeWorker = HomeWorker()
    func fetchCategories(){
        worker.fetchProductCategories { [weak self](response) in
            if response != nil{
                self?.output.presentFetchedCategories(response: response, message: nil)
            }else{
                self?.output.presentFetchedCategories(response: nil, message: UNKNOWN_ERROR_MSG)
            }
        }
    }
    
    func fetchProductList(request: SearchResult.FetchPosts.Request){
        homeWorker.fetchPosts(request: request) { [weak self](response,totalCount,profileResponse) in
            if request.type == 3{
                (self?.output.presentFetchResults(response: response as? [ProductSupplierModel], totalCount: totalCount))
            }else{
                (self?.output.presentSupplierResult(response: response as? [SupplierResponseModel], totalCount: totalCount))
            }
            
            
        }
        
    }
    
    //    func fetchSupplierList(request: SearchResult.FetchPosts.Request){
    //        homeWorker.fetchPosts(request: request) { [weak self](response,profileResponse) in
    //            (self?.output.presentSupplierResult(response: response as? [SupplierModel]))!
    //        }
    //
    //    }
    
    
}
