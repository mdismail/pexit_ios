//
//  EMarketProductConfigurator.swift
//  PEXit
//
//  Created by Apple on 03/08/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation


extension EMarketProductViewController: EMarketProductPresenterOutput{
    //        func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?){
    //
    //        }
}

extension EMarketProductInteractor: EMarketProductViewControllerOutput{
}

extension EMarketProductPresenter: EMarketProductInteractorOutput{
    
    
}



class EMarketProductConfigurator{
    
    // MARK: Object lifecycle
    
    class var sharedInstance: EMarketProductConfigurator{
        struct Static {
            static let instance =  EMarketProductConfigurator()
        }
        print("RegistrationConfigurator instance created")
        return Static.instance
    }
    
    // MARK: Configuration
    
    func configure(viewController: EMarketProductViewController){
        print("configure(viewController: InviteContactViewController) called by using class InviteContactConfigurator for setting the delegates")
        let router = EMarketProductRouter()
        router.controller = viewController
        
        let presenter = EMarketProductPresenter()
        presenter.output = viewController
        
        let interactor = EMarketProductInteractor()
        interactor.output = presenter
        
        viewController.output = interactor
        viewController.router = router
        
    }
    
}
