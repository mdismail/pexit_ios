//
//  EMarketSSTSoftwareViewController.swift
//  PEXit
//
//  Created by Apple on 02/08/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

protocol EMarketSoftwareViewControllerInput{
    
    func displaySearchedResults(response:[SoftwareModel]?)
    func displayServiceResults(response: [ServiceModel]?,totalCount:Int?)
}

protocol EMarketSoftwareViewControllerOutput{
    
    func fetchSoftwareList(request: SearchResult.FetchPosts.Request)
}

class EMarketSSTSoftwareViewController: UIViewController , UITableViewDataSource , UITableViewDelegate , EMarketSoftwareViewControllerInput , UISearchBarDelegate , ItemSelected , ExpandingDelegate{
    
    @IBOutlet weak var searchBarKeywords: UISearchBar!
    @IBOutlet weak var searchBarLocation: UISearchBar!
    @IBOutlet weak var lblResultCount: UILabel!
    @IBOutlet weak var tblResults: UITableView!
    
    @IBOutlet weak var btnViewDownloads: UIButton!
    var controllerType : ControllerType.RawValue?
    var output : EMarketSoftwareViewControllerOutput!
    var router: EMarketSoftwareRouter!
    var local_keywords = ""
    var global_keywords = ""
    var locationStr = ""
    var softwareArray  = [SoftwareModel]()
    var isSearchBarClicked = false
    var searchArray = [String]()
    var serviceArray = [ServiceModel]()
    var currentRequestType : Int?
    var pageNumber = 1
    var isMoreDataAvailable = false
    var startIndex = 0
    var endIndex = 0
    var isValueSelected = false
    @IBOutlet weak var searchView: UIView!
    var parentController : UIViewController?

   // var marketServiceControllerType = ""

    @IBOutlet weak var heightConstraintSearchView: NSLayoutConstraint!
    lazy var dataController: SearchResultController = {
        let dataController = SearchResultController()
        dataController.parentController = self
        return dataController
    }()
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(EMarketSSTSoftwareViewController.handleRefresh(_:)),
                                 for: UIControlEvents.valueChanged)
        refreshControl.tintColor = UIColor.red
        
        return refreshControl
    }()
    
    lazy var downloadsSession: URLSession = {
        let configuration = URLSessionConfiguration.default
        return URLSession(configuration: configuration, delegate: self, delegateQueue: nil)
    }()
    // Get local file path: download task stores tune here; AV player plays it.
    let documentsPath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
    func localFilePath(for url: URL) -> URL {
        return documentsPath.appendingPathComponent(url.lastPathComponent)
    }
    @IBOutlet weak var heightCollectionView: NSLayoutConstraint!
    @IBOutlet weak var collectionViewSearch: UICollectionView!
    let downloadService = DownloadService()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblResults.register(UINib(nibName: "EMarketSoftwareCell", bundle: nil), forCellReuseIdentifier: "marketSoftwareCell")
        searchBarKeywords.delegate = self
        searchBarLocation.delegate = self
        collectionViewSearch.dataSource = dataController
        collectionViewSearch.delegate = dataController
        tblResults.tableFooterView = UIView()
        tblResults.rowHeight = UITableViewAutomaticDimension
        tblResults.estimatedRowHeight = 100
        tblResults.addSubview(refreshControl)
        //   fetchDataFromServer(pageNumber: pageNumber, requestType: currentRequestType)
        downloadService.downloadsSession = downloadsSession
    }
    
    override func viewDidAppear(_ animated: Bool){
        super.viewDidAppear(animated)
        searchBarKeywords.customizeSearchBar(placeholderStr:"Search By Title,Keyword Or Company",rightImageStr:"Search_gray")
        searchBarLocation.customizeSearchBar(placeholderStr:"Location",rightImageStr:"loc_gray")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        switch ControllerType(rawValue: controllerType!)!{
        case  .SSTSoftware , .OtherSoftware:
            collectionViewSearch.isHidden = true
            searchView.isHidden = true
            heightConstraintSearchView.constant = 0
            currentRequestType = 5
            btnViewDownloads.isHidden = false
        case .Services:
            collectionViewSearch.register(UINib(nibName: "JobSearchCell", bundle: nil), forCellWithReuseIdentifier: "JobSearchCell")
            searchView.isHidden = false
            currentRequestType = 6
        default:
            return
        }
        if !isValueSelected{
            softwareArray.removeAll()
            serviceArray.removeAll()
            tblResults.reloadData()
            pageNumber = 1
            startIndex = 0
            endIndex = 0
            fetchDataFromServer(pageNumber: pageNumber, requestType: currentRequestType)
        }
        isValueSelected = false
    }
    
    
    func removeAbuseReportedService(id : Int){
        DispatchQueue.main.async{
            print("Previous records count--->",self.serviceArray.count)
            self.serviceArray = self.serviceArray.filter{$0.id != id}
            print("Next records count--->",self.serviceArray.count)
            self.tblResults.reloadData()
        }
    }
    
    
    func removeAbuseReportedSoftware(id : Int){
        DispatchQueue.main.async{
            print("Previous records count--->",self.softwareArray.count)
            self.softwareArray = self.softwareArray.filter{$0.id != id}
            print("Next records count--->",self.softwareArray.count)
            self.tblResults.reloadData()
        }
    }
    
    
    override func searchLocallyUsingGlobal_subview(searchKeyword : String, searchOptions : String){
        parentController?.addGlobalSearchRefreshBtn()
        global_keywords = searchKeyword
        isValueSelected = false
        if searchOptions == GLOBAL_SEARCH_TYPE.SERVICES.rawValue{
            currentRequestType = 6
        }
        else if searchOptions == GLOBAL_SEARCH_TYPE.SOFTWARES.rawValue{
            currentRequestType = 5
        }
    }
    
    override func cleanUpGlobalSearch_subview(){
        parentController?.removeGlobalSearchRefreshBtn()
        pageNumber = 1
        //currentRequestType = 5
        locationStr = ""
        local_keywords = ""
        global_keywords = ""
        isValueSelected = false
        if let collectionView = collectionViewSearch{
            searchArray.removeAll()
            collectionView.reloadData()
            resizeCollectionViewSize()
        }
    }

    override func viewWillDisappear(_ animated: Bool){
        if tblResults.tableFooterView is LoadingIndicator{
            (tblResults.tableFooterView as! LoadingIndicator).stopAnimating()
        }
    }
    
    override func awakeFromNib() {
        EMarketSoftwareConfigurator.sharedInstance.configure(viewController: self)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        guard let headerView = tblResults.tableHeaderView else {
            return
        }
        let size = headerView.systemLayoutSizeFitting(UILayoutFittingCompressedSize)
        if headerView.frame.size.height != size.height {
            headerView.frame.size.height = size.height
            tblResults.tableHeaderView = headerView
            tblResults.layoutIfNeeded()
        }
    }
    
    
    func fetchDataFromServer(pageNumber:Int? = 1,requestType:Int? = 5){
        //Generate request to fetch jobs from Server
        var request = SearchResult.FetchPosts.Request()
        request.page = pageNumber
        request.records = NUMBER_OF_RECORDS
        request.type = requestType
        var keywords = local_keywords
        if !global_keywords.isEmpty{
            if (!keywords.isEmpty){
                keywords = keywords + "," + global_keywords
            } else {
                keywords = global_keywords
            }
        }
        request.key = keywords
        request.location = locationStr
        switch ControllerType(rawValue: controllerType!)!{
        case  .SSTSoftware:
            searchBarLocation.isHidden = true
            request.softwareType = "mine"
        case .OtherSoftware:
            request.softwareType = "others"
        default:
            print("test")
        }
        if !keywords.isEmpty{
            let totalKeywords = keywords.components(separatedBy:",")
            if totalKeywords.count > 1{
                request.flag = 1
            }else{
                request.flag = 0
            }
        }
        output.fetchSoftwareList(request: request)
        
    }
    
    
    func displaySearchedResults(response: [SoftwareModel]?) {
        if response != nil{
            softwareArray.append(response!)
            for softwareModel in softwareArray{
                softwareModel.isExpanded = false
            }
            if response?.count == NUMBER_OF_RECORDS{
                isMoreDataAvailable = true
            }else{
                isMoreDataAvailable = false
            }
            DispatchQueue.main.async{

                self.tblResults.tableFooterView?.isHidden = true
                self.tblResults.reloadData()
                self.lblResultCount.text  = "\(self.softwareArray.count) result"
            }
            
        }
    }
    
    func displayServiceResults(response: [ServiceModel]?,totalCount:Int?){
        if response != nil{
            serviceArray.append(response!)
            if response?.count == NUMBER_OF_RECORDS{
                isMoreDataAvailable = true
            }else{
                isMoreDataAvailable = false
            }
            DispatchQueue.main.async{
                self.tblResults.tableFooterView?.isHidden = true
                self.tblResults.reloadData()
                //self.lblResultCount.text  (self.serviceArray.count) result"
                self.lblResultCount.text = "\(self.serviceArray.count) of \(totalCount ?? 0) result"
            }
            
        }
    }
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        if searchBar == searchBarLocation{
            isSearchBarClicked = true
            router.navigateToLocationSearchScreen()
            searchBarLocation.resignFirstResponder()
            return false
        }
        return true
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        
        if let searchText = searchBar.text{
            if searchText.isEmpty{
                displayToast(ENTER_SEARCH_KEYWORDS)
                return
            }
            if local_keywords != ""{
                local_keywords = local_keywords + "," + searchText
            }else{
                local_keywords = searchText
            }
            
            if !(searchText.isEmpty){
                insertSearchItems(itemStr: searchText)
            }
        }
        pageNumber = 1
        serviceArray.removeAll()
        tblResults.reloadData()
        fetchDataFromServer(pageNumber: pageNumber,requestType:currentRequestType)
        searchBar.text = ""
        self.view.endEditing(true)
    }
    
    
    
    func optionSelected(option: String?) {
        pageNumber = 1
        isMoreDataAvailable = true
        if isSearchBarClicked == true {
            isSearchBarClicked = false
            if let index = searchArray.index(where: { $0 == locationStr}) {
                searchArray.remove(at: index)
            }
            locationStr = option!
            insertSearchItems(itemStr: locationStr)
            serviceArray.removeAll()
            tblResults.reloadData()
            //fetchDataFromServer(pageNumber: pageNumber,requestType:currentRequestType)
            return
        }
    }
    
    func insertSearchItems(itemStr : String){
        searchArray.insert(itemStr, at: searchArray.count)
        dataController.searchArray = searchArray
        resizeCollectionViewSize()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.collectionViewSearch.reloadData()
        }
    }
    
    func resizeCollectionViewSize(){
        if searchArray.count > 0{
            heightCollectionView.constant = 40
        }else{
            heightCollectionView.constant = 0
        }
    }
    
    @objc func deleteCell(sender : UIButton){
        
        if locationStr == searchArray[sender.tag]{
            locationStr = ""
        }else{
            var strArray = local_keywords.components(separatedBy: ",")
            print(local_keywords)
            print(strArray)
            strArray = strArray.filter{$0 != searchArray[sender.tag]}
            print(strArray)
            local_keywords = strArray.joined(separator: ",")
            print(local_keywords)
            // keywords = searchArray.joined(separator: ",")
        }
        
        searchArray.remove(at: sender.tag)
        dataController.searchArray = searchArray
        resizeCollectionViewSize()
        //DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) { commented because of crash
        self.collectionViewSearch.reloadData()
        //}
        pageNumber = 1
        serviceArray.removeAll()
        tblResults.reloadData()
        fetchDataFromServer(pageNumber: pageNumber,requestType:currentRequestType)
    }
    
    @objc func handleRefresh(_ refreshControl : UIRefreshControl){
        refresh_SubView()
        refreshControl.endRefreshing()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            self.tblResults.contentOffset = CGPoint.zero
        }
    }
 
    override func refresh_SubView() {
        if currentRequestType != nil{
            startIndex = 0
            if controllerType == ControllerType.Services.rawValue{
                serviceArray.removeAll()
            }else{
                softwareArray.removeAll()
            }
            isMoreDataAvailable = true
            pageNumber = 1
            fetchDataFromServer(pageNumber: pageNumber, requestType: currentRequestType)
            if let tblresult = tblResults{
                tblresult.reloadData()
            }
        }
        //return
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return controllerType == ControllerType.Services.rawValue ? serviceArray.count : softwareArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let productCell = tableView.dequeueReusableCell(withIdentifier: "marketSoftwareCell", for: indexPath) as! EMarketSoftwareCell
        if controllerType == ControllerType.Services.rawValue{
//            productCell.btnDownload.isHidden = true
            productCell.setServicesData(serviceModel: serviceArray[indexPath.row])
        }else{
            let softwareModel = softwareArray[indexPath.row]
            softwareModel.index = indexPath.row
            productCell.delegate = self
            productCell.softwareArray = softwareArray
            productCell.expandingDelegate = self
          //  productCell.btnReadMore.tag = indexPath.row
            if let softwareURL = URL(string:softwareModel.software!){
                productCell.configure(softwareModel: softwareModel, downloaded: softwareModel.downloaded ?? false , download: downloadService.activeDownloads[softwareURL])
//                productCell.btnDownload.isHidden = false
            }else{
                if softwareModel.url != nil && !(softwareModel.url?.isEmpty)! && !((URL(string:softwareModel.url!)?.lastPathComponent.isEmpty)!){
                    if (softwareModel.url?.isValidURL)!{
                        softwareModel.software = softwareModel.url
                        productCell.configure(softwareModel: softwareModel, downloaded: softwareModel.downloaded ?? false , download: downloadService.activeDownloads[URL(string:softwareModel.software!)!])
//                        productCell.btnDownload.isHidden = false
                    }else{
                        productCell.configure(softwareModel: softwareModel, downloaded: softwareModel.downloaded ?? false , download: nil)
//                        productCell.btnDownload.isHidden = true
                    }
                }else{
                    productCell.configure(softwareModel: softwareModel, downloaded: softwareModel.downloaded ?? false , download: nil)
//                    productCell.btnDownload.isHidden = true
                }
                
            }
//            if softwareModel.dwnld == "serial"{
//                productCell.btnDownload.isHidden = false
//            }
        }
        return productCell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath){
        
        if isMoreDataAvailable == true && indexPath.row == (controllerType == ControllerType.Services.rawValue ? serviceArray.count - 1 : softwareArray.count - 1){
            isMoreDataAvailable = false
            pageNumber = pageNumber + 1
            tableView.tableFooterView = LoadingIndicator.init(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44)))
            tableView.tableFooterView?.isHidden = false
            fetchDataFromServer(pageNumber: pageNumber,requestType:currentRequestType)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        isValueSelected = true
        if controllerType == ControllerType.Services.rawValue{
            router.navigateToDetailsScreen(serviceId: serviceArray[indexPath.row].id!)
        }
        else if controllerType == ControllerType.SSTSoftware.rawValue || controllerType == ControllerType.OtherSoftware.rawValue{
            router.navigateToSoftwareDetailsScreen(softwareModel: softwareArray[indexPath.row])
        }
    }
    
    func moreTapped(cell: Any) {
        // this will "refresh" the row heights, without reloading
        tblResults?.beginUpdates()
        tblResults?.endUpdates()
        
        // do anything else you want because the switch was changed
        
    }
}


extension EMarketSSTSoftwareViewController: SoftwareCellDelegate {
    
    func downloadTapped(_ cell: EMarketSoftwareCell) {
        if let indexPath = tblResults.indexPath(for: cell) {
            let software = softwareArray[indexPath.row]
            if software.dwnld == "serial"{
                router.navigateToSoftwareDownloadScreen()
            }else{
                downloadService.startDownload(software)
                reload(indexPath.row)
            }
            
        }
    }
    
    func cancelTapped(_ cell: EMarketSoftwareCell) {
        if let indexPath = tblResults.indexPath(for: cell) {
            let track = softwareArray[indexPath.row]
            downloadService.cancelDownload(track)
            reload(indexPath.row)
        }
    }
    
    // Update software cell's buttons
    func reload(_ row: Int) {
        tblResults.reloadRows(at: [IndexPath(row: row, section: 0)], with: .none)
    }
    
    
}


extension EMarketSSTSoftwareViewController: URLSessionDownloadDelegate {
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask,
                    didFinishDownloadingTo location: URL) {
        // 1
        guard let sourceURL = downloadTask.originalRequest?.url else { return }
        let download = downloadService.activeDownloads[sourceURL]
        /*    if downloadService.activeDownloads[sourceURL] == nil{
         print("FILE NAME EMPTY")
         return
         } */
        downloadService.activeDownloads[sourceURL] = nil
        // 2
        /* if sourceURL.lastPathComponent.isEmpty{
         print("FILE NAME EMPTY AGAIN")
         return
         } */
        let destinationURL = localFilePath(for: sourceURL)
        print(destinationURL)
        
        // 3
        let fileManager = FileManager.default
        try? fileManager.removeItem(at: destinationURL)
        do {
            try fileManager.copyItem(at: location, to: destinationURL)
            download?.softwareModel.downloaded = true
            
            //            download?.track.downloaded = true
        } catch let error {
            print("Could not copy file to disk: \(error.localizedDescription)")
        }
        // 4
        if let index = download?.softwareModel.index {
            DispatchQueue.main.async {
                // self.copySoftware(softwareModel : download?.softwareModel)
                self.tblResults.reloadRows(at: [IndexPath(row: index, section: 0)], with: .none)
            }
        }
    }
    
    
    // Updates progress info
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask,
                    didWriteData bytesWritten: Int64, totalBytesWritten: Int64,
                    totalBytesExpectedToWrite: Int64) {
        // 1
        guard let url = downloadTask.originalRequest?.url,
            let download = downloadService.activeDownloads[url]  else { return }
        // 2
        download.progress = Float(totalBytesWritten) / Float(totalBytesExpectedToWrite)
        // 3
        let totalSize = ByteCountFormatter.string(fromByteCount: totalBytesExpectedToWrite,
                                                  countStyle: .file)
        /*     DispatchQueue.main.async{
         if totalSize == "-1 byte"{
         if let trackCell = self.tblResults.cellForRow(at: IndexPath(row: download.softwareModel.index!,section: 0)) as? EMarketSoftwareCell {
         self.displayToast("Cannot downlaod")
         self.downloadService.cancelDownload(self.softwareArray[download.softwareModel.index!])
         self.reload(download.softwareModel.index!)
         return
         }
         }
         } */
        
        // 4
        DispatchQueue.main.async {
            if let trackCell = self.tblResults.cellForRow(at: IndexPath(row: download.softwareModel.index!,section: 0)) as? EMarketSoftwareCell {
                
                trackCell.updateDisplay(progress: download.progress, totalSize: totalSize)
            }
        }
    }
    
}


