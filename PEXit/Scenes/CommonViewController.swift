//
//  CommonViewController.swift
//  PEXit
//
//  Created by Apple on 02/08/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

protocol ItemSelected{
    func optionSelected(option : String?)
    func arrayOfSelectedOptions(option : [String]?)
    func shareOptionSelected(shareModel : ListOfUsersToShare.ShareUserModel?)
    func individualUserSelected(userModel : [ListOfIndividuals.IndividualUserModel]?)
}

extension ItemSelected{
    func optionSelected(option : String?){
        
    }
    
    func arrayOfSelectedOptions(option : [String]?){
        
    }
    
    func shareOptionSelected(shareModel : ListOfUsersToShare.ShareUserModel?){
        
    }
    func individualUserSelected(userModel : [ListOfIndividuals.IndividualUserModel]?){
        
    }
}

class CommonViewController: UIViewController , UITableViewDataSource , UITableViewDelegate{
    @IBOutlet weak var tblListOfOptions: UITableView!
    var delegate : ItemSelected?
    var itemsArray : [String]?
    var shareOptionsArray : [ListOfUsersToShare.ShareUserModel]?
    var individualUsersArray : [ListOfIndividuals.IndividualUserModel]?
    var previousSelectedUsers = [ListOfIndividuals.IndividualUserModel]()
    var isFromHome = false
    var isReport = false
    var isLanguageUpdation = false
    var selectedItemsArray = [String]()
    var isMultiSelectionAllowed = false
    @IBOutlet weak var btnDone: UIButton!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    override func viewDidLoad() {
        tblListOfOptions.tableFooterView = UIView()
        tblListOfOptions.allowsMultipleSelection = isMultiSelectionAllowed
        if isMultiSelectionAllowed{
            topConstraint.constant = 40
            btnDone.isHidden = false
        }else{
            topConstraint.constant = 8
        }
        super.viewDidLoad()
    }
    
    func sendUserArray(usersArray : [ListOfIndividuals.IndividualUserModel]?){
        if usersArray != nil{
            individualUsersArray = usersArray
            if tblListOfOptions != nil{
                tblListOfOptions.reloadData()
            }
        }
    }
    
    
    @IBAction func btnDoneClicked(_ sender: Any) {
        selectedItemsArray = selectedItemsArray.filter{$0 != ""}
        delegate?.arrayOfSelectedOptions(option: selectedItemsArray)
        self.dismissViewController()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let shareOptionsArray = shareOptionsArray{
            return shareOptionsArray.count
        }
        if let individualUsersArray = individualUsersArray{
            return individualUsersArray.count
        }
        return itemsArray?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let listCell = tableView.dequeueReusableCell(withIdentifier: "listCell", for: indexPath)
        if let shareOptionsArray = shareOptionsArray{
            listCell.textLabel?.text = shareOptionsArray[indexPath.row].svalue
        }
        if let individualUsersArray = individualUsersArray{
            listCell.textLabel?.text = individualUsersArray[indexPath.row].username
            
            let contains = previousSelectedUsers.contains(where: { $0.id == individualUsersArray[indexPath.row].id })
            if contains == true{
                listCell.accessoryType = .checkmark
            }else{
                listCell.accessoryType = .none
            }
        }
        if itemsArray != nil{
            if selectedItemsArray.contains(itemsArray![indexPath.row]){
                listCell.accessoryType = .checkmark
            }else{
                listCell.accessoryType = .none
            }
        }
        if let itemsArray = itemsArray{
            listCell.textLabel?.text = itemsArray[indexPath.row]
            if isFromHome && !isReport{
                if indexPath.row == 0{
                    listCell.imageView?.image = UIImage(named:"edit")
                }
                else{
                    listCell.imageView?.image = UIImage(named:"delete")
                }
                
            }
          
        }
      
        return listCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isMultiSelectionAllowed == false{
            if shareOptionsArray != nil{
                delegate?.shareOptionSelected(shareModel: shareOptionsArray?[indexPath.row])
            }
            if individualUsersArray != nil{
                let cell = tableView.cellForRow(at: indexPath)
                if !(previousSelectedUsers.contains(where: { $0.id == individualUsersArray![indexPath.row].id })){
                    cell?.accessoryType = .checkmark
                    previousSelectedUsers.append((individualUsersArray?[indexPath.row])!)
                }
                delegate?.individualUserSelected(userModel: previousSelectedUsers)
            }
            if itemsArray != nil{
                delegate?.optionSelected(option: itemsArray?[indexPath.row])
            }
            
            self.dismissViewController()
        }else{
            let selectedCell = tableView.cellForRow(at: indexPath)
            selectedItemsArray.append((itemsArray?[indexPath.row])!)
            selectedCell?.accessoryType = .checkmark
        }
        
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let deselectedCell = tableView.cellForRow(at: indexPath)
        // selectedItemsArray.remove(element:itemsArray?[indexPath.row])
        //   selectedItemsArray.remove(at: indexPath.row)
        // selectedItemsArray.remove((itemsArray?[indexPath.row])!)
        selectedItemsArray = selectedItemsArray.filter{$0 != itemsArray?[indexPath.row]}
        
        deselectedCell?.accessoryType = .none
        tableView.reloadRows(at: [indexPath], with: .automatic)
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(COMMON_CONTROLLER_ROW_HEIGHT)
    }
    
}
