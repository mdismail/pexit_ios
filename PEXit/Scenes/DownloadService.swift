/**
 * Copyright (c) 2017 Razeware LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * Notwithstanding the foregoing, you may not use, copy, modify, merge, publish,
 * distribute, sublicense, create a derivative work, and/or sell copies of the
 * Software in any work that is designed, intended, or marketed for pedagogical or
 * instructional purposes related to programming, coding, application development,
 * or information technology.  Permission for such use, copying, modification,
 * merger, publication, distribution, sublicensing, creation of derivative works,
 * or sale is expressly withheld.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import Foundation
class Download {
    
    var softwareModel: SoftwareModel
    init(softwareModel: SoftwareModel) {
        self.softwareModel = softwareModel
    }
    
    // Download service sets these values:
    var task: URLSessionDownloadTask?
    var isDownloading = false
    var resumeData: Data?
    
    // Download delegate sets this value:
    var progress: Float = 0
    
}
// Downloads song snippets, and stores in local file.
// Allows cancel, pause, resume download.
class DownloadService {

  // SearchViewController creates downloadsSession
  var downloadsSession: URLSession!
  var activeDownloads: [URL: Download] = [:]
  // MARK: - Download methods called by TrackCell delegate methods

  func startDownload(_ softwareModel: SoftwareModel) {
    // 1
    let download = Download(softwareModel: softwareModel)
    // 2
    download.task = downloadsSession.downloadTask(with: URL(string:softwareModel.software!)!)
    
    
    // 3
    download.task!.resume()
    // 4
    download.isDownloading = true
    // 5
    activeDownloads[URL(string:softwareModel.software!)!] = download
  }
  
//    func pauseDownload(_ softwareModel: SoftwareModel) {
//        guard let download = activeDownloads[URL(string:softwareModel.url!)!] else { return }
//        if download.isDownloading {
//            download.task?.cancel(byProducingResumeData: { data in
//                download.resumeData = data
//            })
//            download.isDownloading = false
//        }
//    }
    
    func cancelDownload(_ softwareModel: SoftwareModel) {
        if let download = activeDownloads[URL(string:softwareModel.software!)!] {
            download.task?.cancel()
            activeDownloads[URL(string:softwareModel.software!)!] = nil
        }
    }
    
//    func resumeDownload(_ softwareModel: SoftwareModel) {
//        guard let download = activeDownloads[URL(string:softwareModel.url!)!] else { return }
//        if let resumeData = download.resumeData {
//            download.task = downloadsSession.downloadTask(withResumeData: resumeData)
//        } else {
//            download.task = downloadsSession.downloadTask(with: URL(string:download.softwareModel.url!)!)
//        }
//        download.task!.resume()
//        download.isDownloading = true
//    }

}
