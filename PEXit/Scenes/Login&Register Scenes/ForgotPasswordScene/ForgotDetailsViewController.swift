//
//  ForgotDetailsViewController.swift
//  PEXit
//
//  Created by Apple on 09/07/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

protocol SendEmail{
    func sendEmailId(email : String)
}

//extension UILabel {
//    func halfTextColorChange (fullText : String , changeText : String ) {
//        let strNumber: NSString = fullText as NSString
//        let range = (strNumber).range(of: changeText)
//        let attribute = NSMutableAttributedString.init(string: fullText)
//        let paragraphStyle = NSMutableParagraphStyle()
//        paragraphStyle.lineSpacing = 2
//        self.numberOfLines = 2
//        self.lineBreakMode = .byWordWrapping
//        attribute.addAttribute(.paragraphStyle, value: paragraphStyle, range: NSMakeRange(0, attribute.length))
//        //        attribute.addAttribute(NSForegroundColorAttributeName, value: UIColor.red , range: range)
//                self.attributedText = attribute
//    }
//}

class ForgotDetailsViewController: UIViewController {

    @IBOutlet weak var emailTextfield: TJTextField!
    @IBOutlet weak var headingLabel: UILabel!
    var delegate : SendEmail?
    var taskExecutionName: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        showNavigationBar()
        addBackButton()
        headingLabel.text = taskExecutionName
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Button Actions
    @IBAction func submitButtonAction(_ sender: UIButton) {
        
        if  Utils.validateFormAndShowToastWithRequiredFields([emailTextfield]) {
            if taskExecutionName == "Facebook Email"{
                self.backAction()
                delegate?.sendEmailId(email: emailTextfield.text!)
                
            }
            else{
                let dict = NSMutableDictionary()
                dict.setValue(taskExecutionName, forKey: "taskExecutionName")
                dict.setValue(emailTextfield.text, forKey: "email")
                forgotPasswordOrUsername(params: dict) { (response) in
                    unowned let me = self
                    //                print("response is \(response ?? "Nothing")")
                    if let message = ((response as! [String : Any])["message"] as? String){
                        me.displayToast(message)
                    }
                    me.backAction()
                }
            }
            
        }
    }
    
    
    
}
