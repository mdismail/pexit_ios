//
//  RegistrationViewController.swift
//  PEXit
//
//  Created by Apple on 09/07/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

protocol RegistrationViewControllerInput{
    func displayFetchedItems(viewModel:RegistrationList.FetchItems.ViewModel)
    func displayFetchedCountries(viewModel:CountryResponse.Fetchcounties.Response)
}

protocol RegistrationViewControllerOutput{
    func fetchItems(request:RegistrationList.FetchItems.Request)
    func fetchCountries(request:CountryResponse.Fetchcounties.Request)
     
}


class RegistrationViewController: UIViewController,RegistrationViewControllerInput, UITableViewDelegate, UITableViewDataSource {
    
    var output: RegistrationViewControllerOutput!
    var router: RegistrationRouter!
    
    var displayedItems = [TextfieldInfoStruct]()
    var arrayOfTextFields = [TextfieldInfoStruct]()
    var requiredFields: [TJTextField]?
    var countryArray = [String]()
    // MARK: UI OUTLETS
    @IBOutlet weak var formTableView: UITableView!
    @IBOutlet weak var signUpButtonOutlet: UIButton!
    @IBOutlet weak var agreeButtonOutlet: UIButton!
    
    // MARK: Object lifecycle
    
    override func awakeFromNib(){
        super.awakeFromNib()
        RegistrationConfigurator.sharedInstance.configure(viewController: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        showNavigationBar()
        addBackButton()
        doSomeInitialSetups()
      
    }
    
    func doSomeInitialSetups() {
        ItemsSingleton.sharedInstance.items.removeAll()
        ItemsSingleton.sharedInstance.items.append(contentsOf: getFormItemsBasedOn(displayedItems))
        
        agreeButtonOutlet.titleLabel?.numberOfLines = 2
        agreeButtonOutlet.titleLabel?.lineBreakMode = .byWordWrapping
        agreeButtonOutlet.titleLabel?.textAlignment = .center
        agreeButtonOutlet.setTitle("By Clicking Join Now, you agree to\nPEXit's Terms & Conditions.", for: .normal)
        
        formTableView.dataSource = self
        formTableView.delegate = self
        formTableView.register(UINib(nibName: "TextfieldTableViewCell", bundle: nil), forCellReuseIdentifier: "TextfieldTableViewCell")
        view.backgroundColor = .black
        formTableView.backgroundColor = .clear

    }

    func getFormItemsBasedOn(_ enteredDetails:[TextfieldInfoStruct]) -> [TextfieldInfoStruct] {
        if enteredDetails.count > 0 {
            return []
        } else {
            arrayOfTextFields = [TextfieldInfoStruct(leftImage: #imageLiteral(resourceName: "displayName"), placeholderText: "Display Name"),TextfieldInfoStruct(leftImage: #imageLiteral(resourceName: "userWhite"), placeholderText: "Username"),TextfieldInfoStruct(leftImage: #imageLiteral(resourceName: "lock"), placeholderText: "Password"),TextfieldInfoStruct(leftImage: #imageLiteral(resourceName: "envelope"), placeholderText: "Email"),TextfieldInfoStruct(leftImage: #imageLiteral(resourceName: "globe"), rightImage: #imageLiteral(resourceName: "downArrow"), placeholderText: "Select Country"),TextfieldInfoStruct(leftImage: #imageLiteral(resourceName: "phone"), placeholderText: "Phone Number"),TextfieldInfoStruct(leftImage: #imageLiteral(resourceName: "company"), placeholderText: "Company Name")]
            return arrayOfTextFields
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fetchItems()
        fetchCountries()
    }
    
    // MARK: Event handling
    
    func fetchItems(){
        // NOTE: Ask the Interactor to do some work
        let request = RegistrationList.FetchItems.Request()
        output.fetchItems(request: request)
    }
    
    func fetchCountries(){
        let request = CountryResponse.Fetchcounties.Request()
        output.fetchCountries(request: request)
        
    }
    
    // MARK: Display logic
    
    func displayFetchedItems(viewModel:RegistrationList.FetchItems.ViewModel){
        print("displayFetchedItems(viewModel:RegistrationList.FetchItems.ViewModel) called by using RegistrationViewControllerInput")
        displayedItems = viewModel.displayedItems
        formTableView.reloadData()
        requiredFields = getAllRequiredFields()
    }
    
    func displayFetchedCountries(viewModel: CountryResponse.Fetchcounties.Response) {
        for country in viewModel.datas!{
            countryArray.append(country.name!)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: TABLE VIEW METHODS
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return displayedItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TextfieldTableViewCell") as? TextfieldTableViewCell
        cell?.configureTextfield(displayedItems[indexPath.row])
        cell?.textfield.delegate = self
        cell?.textfield.tag = indexPath.row
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func getValidationFields() -> [TJTextField]{
       
            var textFieldsArray = [TJTextField]()
            for txtFieldDetails in displayedItems{
                let txtField = TJTextField()
                txtField.placeholder = txtFieldDetails.placeholderText
                txtField.text = txtFieldDetails.textfieldText
                textFieldsArray.append(txtField)
            }
            return textFieldsArray
        
       
    }
    
    //MARK: Get all required fields
    func getAllRequiredFields() -> [TJTextField] {
        var textFieldsArray = [TJTextField]()
        let cells = Utils.cellsForTableView(formTableView) as! [TextfieldTableViewCell]
        
        for cell in cells {
            textFieldsArray.append(cell.textfield)
        }
        return textFieldsArray
    }
    
//MARK: ButtonActions
    @IBAction func signUpButtonAction(_ sender: UIButton) {
        if  Utils.validateFormAndShowToastWithRequiredFields(getValidationFields()) {
            print("U can proceed")
            apiSignUp { (response) in
                
            }
        } else {
            return
        }
    }
    @IBAction func agreeButtonAction(_ sender: UIButton) {
        let termsController = storyboard?.instantiateViewController(withIdentifier: "termsViewController") as! TermsViewController
        self.navigationController?.pushViewController(termsController, animated: true)
    }
    

    
}


extension RegistrationViewController : UITextFieldDelegate
{
    //MARK: UITextFieldDelegate
     func textFieldDidEndEditing(_ textField: UITextField) {
         let selectedIndexPath: IndexPath = IndexPath(row: textField.tag, section: 0)
         displayedItems[selectedIndexPath.row].textfieldText = textField.text
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
         if textField.placeholder == "Select Country" {
           self.view.endEditing(true)
           ActionSheetStringPicker.show(withTitle: "Select Country", rows: countryArray, initialSelection: 0, doneBlock: {[unowned self] (key, index, value) in
                textField.text  = value as? String
            let selectedIndexPath: IndexPath = IndexPath(row: textField.tag, section: 0)
            self.displayedItems[selectedIndexPath.row].textfieldText = textField.text
            }, cancel: { (picker) in
                
            }, origin: self.view)
            return false
        }
        return true
    }
    

    
}
