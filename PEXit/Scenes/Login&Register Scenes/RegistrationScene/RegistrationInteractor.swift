//
//  RegistrationInteractor.swift
//  PEXit
//
//  Created by Apple on 12/07/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation
import UIKit

protocol RegistrationInteractorInput{
    func fetchItems(request:RegistrationList.FetchItems.Request)
    func fetchCountries(request: CountryResponse.Fetchcounties.Request)
    }


protocol RegistrationInteractorOutput{
    func presentFetchedItems(response:RegistrationList.FetchItems.Response)
    func presentFetchResults(response: CountryResponse.Fetchcounties.Response)
}

class RegistrationInteractor: RegistrationInteractorInput{
    var output: RegistrationInteractorOutput!
    var worker = ItemWorker(itemStore: ItemMemoryStore())
    var itemsArray: [TextfieldInfoStruct] = []
    var workerCountries = RegistrationWorker()
    
    // MARK: Business logic
    
    func fetchItems(request:RegistrationList.FetchItems.Request){
        print("fetchItems(request:RegistrationList.FetchItems.Request) called by using RegistrationInteractorInput")
        worker.fetchAllItems(completionHandler: {(items) in
            let response = RegistrationList.FetchItems.Response(items: items)
            itemsArray = items
            output.presentFetchedItems(response: response)
        })
    }
    
    func fetchCountries(request: CountryResponse.Fetchcounties.Request){
        workerCountries.fetchCountries(completionHandler: {(country) in
            
            self.output.presentFetchResults(response: country)
        })
    }
    
    /*
    func requestDeleteItem(request: PurchaseList.Delete.Request){
        worker.deleteItem(id: itemsArray[request.index].id!, completionHandler: {(error) in
            let successfully = error == nil ? true : false
            if successfully{
                itemsArray.remove(at: request.index)
            }
            let response = PurchaseList.Delete.Response(index: request.index, successfully: successfully)
            output.presentDeleteItem(response: response)
        })
    }  */
}
