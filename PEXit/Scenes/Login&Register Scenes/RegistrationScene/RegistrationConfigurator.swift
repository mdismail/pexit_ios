//
//  RegistrationConfigurator.swift
//  PEXit
//
//  Created by Apple on 12/07/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation
import UIKit

// MARK: Connect View, Interactor, and Presenter

extension RegistrationViewController: RegistrationPresenterOutput{
    func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?){
//        router.passDataToNextScene(segue: segue)
    }
}

extension RegistrationInteractor: RegistrationViewControllerOutput{
}

extension RegistrationPresenter: RegistrationInteractorOutput{
    
    
}

class RegistrationConfigurator{
    // MARK: Object lifecycle
    
    class var sharedInstance: RegistrationConfigurator{
        struct Static {
            static let instance =  RegistrationConfigurator()
        }
        print("RegistrationConfigurator instance created")
        return Static.instance
    }
    
    // MARK: Configuration
    
    func configure(viewController: RegistrationViewController){
        print("configure(viewController: RegistrationViewController) called by using class RegistrationConfigurator for setting the delegates")
        let router = RegistrationRouter()
        router.viewController = viewController
        
        let presenter = RegistrationPresenter()
        presenter.output = viewController
        
        let interactor = RegistrationInteractor()
        interactor.output = presenter
        
        viewController.output = interactor
        viewController.router = router
    }
}
