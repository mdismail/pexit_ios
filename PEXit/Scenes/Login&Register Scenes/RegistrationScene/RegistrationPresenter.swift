//
//  RegistrationPresenter.swift
//  PEXit
//
//  Created by Apple on 12/07/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation
import UIKit

protocol RegistrationPresenterInput{
    func presentFetchedItems(response:RegistrationList.FetchItems.Response)
    func presentFetchResults(response:CountryResponse.Fetchcounties.Response)
}

protocol RegistrationPresenterOutput: class{
    func displayFetchedItems(viewModel:RegistrationList.FetchItems.ViewModel)
    func displayFetchedCountries(viewModel:CountryResponse.Fetchcounties.Response)
   
}

class RegistrationPresenter: RegistrationPresenterInput{
    weak var output: RegistrationPresenterOutput!
    
    // MARK: Presentation logic
    
    func presentFetchedItems(response:RegistrationList.FetchItems.Response){
        print("presentFetchedItems(response:RegistrationList.FetchItems.Response) called by using RegistrationPresenterInput")
        let viewModel = RegistrationList.FetchItems.ViewModel(displayedItems: response.items)
        output.displayFetchedItems(viewModel: viewModel)
    }
    

    func presentFetchResults(response: CountryResponse.Fetchcounties.Response) {
        //print("Total countriesssssss :------>",response.datas?.count)
        output.displayFetchedCountries(viewModel: response)
    }
}
