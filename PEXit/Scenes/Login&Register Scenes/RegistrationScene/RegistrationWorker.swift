//
//  RegistrationWorker.swift
//  PEXit
//
//  Created by Apple on 25/07/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation


class RegistrationWorker{
    func fetchCountries(completionHandler: @escaping(_ countries : CountryResponse.Fetchcounties.Response) -> Void) {
        START_LOADING_VIEW()
        ServiceManager.methodType(requestType: GET_REQUEST, url: GET_COUNTRIES, params: nil, paramsData: nil, completion: { (response,_ responseData, statusCode) in
            STOP_LOADING_VIEW()
            let decoder = JSONDecoder()
            do{
                if let _ = responseData{
                    let responseCountries = try decoder.decode(CountryResponse.Fetchcounties.Response.self, from: responseData!)
                    completionHandler(responseCountries)
                }
            }catch{
                print("error")
            }
            
        }) { (response, statusCode) in
            
        }
    }
    
    func fetchCountryCodes(completionHandler: @escaping(_ countries : CountryResponse.Fetchcounties.Response) -> Void) {
        START_LOADING_VIEW()
        ServiceManager.methodType(requestType: GET_REQUEST, url: GET_COUNTRY_CODES, params: nil, paramsData: nil, completion: { (response,_ responseData, statusCode) in
            STOP_LOADING_VIEW()
            let decoder = JSONDecoder()
            do{
                if let _ = responseData{
                    let responseCountries = try decoder.decode(CountryResponse.Fetchcounties.Response.self, from: responseData!)
                    completionHandler(responseCountries)
                }
               
            }catch{
                
                print("error")
            }
            
        }) { (response, statusCode) in
            
        }
    }
    
    func fetchStates(countryName : String,completionHandler: @escaping(_ countries : CountryResponse.Fetchcounties.Response) -> Void) {
        START_LOADING_VIEW()
        ServiceManager.methodType(requestType: GET_REQUEST, url: GET_STATES(countryName: countryName), params: nil, paramsData: nil, completion: { (response,_ responseData, statusCode) in
            STOP_LOADING_VIEW()
            let decoder = JSONDecoder()
            do{
                if let _ = responseData{
                    let responseCountries = try decoder.decode(CountryResponse.Fetchcounties.Response.self, from: responseData!)
                    completionHandler(responseCountries)
                }
            }catch{
                
                print("error")
            }
            
        }) { (response, statusCode) in
            
        }
    }
    
    func fetchCities(stateName : String,completionHandler: @escaping(_ countries : CountryResponse.Fetchcounties.Response) -> Void) {
        START_LOADING_VIEW()
        ServiceManager.methodType(requestType: GET_REQUEST, url: GET_CITIES(stateName: stateName), params: nil, paramsData: nil, completion: { (response,_ responseData, statusCode) in
            STOP_LOADING_VIEW()
            let decoder = JSONDecoder()
            do{
                if let _ = responseData{
                    let responseCountries = try decoder.decode(CountryResponse.Fetchcounties.Response.self, from: responseData!)
                    completionHandler(responseCountries)
                }
            }catch{
                
                print("error")
            }
            
        }) { (response, statusCode) in
            
        }
    }
    
    func fetchAdsStates(countries:String?,completionHandler: @escaping(_ countries : CountryResponse.Fetchcounties.Response) -> Void) {
        START_LOADING_VIEW()
        ServiceManager.methodType(requestType: POST_REQUEST, url: GET_AD_STATES, params: ["countries":countries], paramsData: nil, completion: { (response,_ responseData, statusCode) in
            STOP_LOADING_VIEW()
            let decoder = JSONDecoder()
            do{
                if let _ = responseData{
                    let responseCountries = try decoder.decode(CountryResponse.Fetchcounties.Response.self, from: responseData!)
                    completionHandler(responseCountries)
                }
            }catch{
                
                print("error")
            }
            
        }) { (response, statusCode) in
            
        }
    }
    
    
    
}


