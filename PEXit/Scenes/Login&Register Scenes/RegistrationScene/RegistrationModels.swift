//
//  RegistrationModels.swift
//  PEXit
//
//  Created by Apple on 12/07/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation
import UIKit

struct RegistrationList{
    
    struct FetchItems {
        struct Request{
        }
        
        struct Response{
            var items: [TextfieldInfoStruct]
        }
        
        struct ViewModel{
            var displayedItems: [TextfieldInfoStruct]
        }
    }
    
}

struct CountryResponse{
    struct Fetchcounties{
        struct Request{
            
        }
        struct Response : Codable{
            var status: Bool?
            var datas : [CountryViewModel]?
        }
        
        struct CountryViewModel : Codable{
            var name : String?
            var id : Int?
            var key : String?
            var phonecode : String?
        }
    }
}


struct CompanyResponse : Codable{
    var status: Bool?
    var datas : [CompanyListModel]?
}

struct CompanyListModel : Codable{
    var companyname : String?
}
