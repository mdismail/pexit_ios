//
//  GoogleManager.swift
//  PEXit
//
//  Created by Apple on 24/07/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation
import GoogleSignIn
import FBSDKLoginKit
import LinkedinSwift

protocol LoginServiceDelegate: class {
    func loginSuccess(userDetails : [String : String],profileImageData:Data?,socialLoginType:LoginType)
    func loginFailed(message: String)
    func googleSignInLaunch(_ viewController: UIViewController)
    func googleSignInDismiss(_ viewController: UIViewController)
}

class GoogleManager : NSObject , GIDSignInDelegate , GIDSignInUIDelegate{
    weak var delegate : LoginServiceDelegate?
    static let sharedGoogleManager = GoogleManager()
    
    func setup(){
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().signIn()
    }
    //MARK:- Google Delegate
    func sign(inWillDispatch signIn: GIDSignIn!, error: Error!) {
        
    }
    
    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
        delegate?.googleSignInLaunch(viewController)
    }
    
    func sign(_ signIn: GIDSignIn!,
              dismiss viewController: UIViewController!) {
        delegate?.googleSignInDismiss(viewController)
    }
    
    public func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
                     withError error: Error!) {
        if (error == nil) {
            // Perform any operations on signed in user here.
            let userId = user.userID                  // For client-side use only!
            //            let idToken = user.authentication.idToken // Safe to send to the server
            //            let fullName = user.profile.name
            let email : String? = user.profile.email
            
            var imageData : Data? = nil
            if user.profile.hasImage{
                
                if let theProfileImageUrl =  user.profile.imageURL(withDimension : 200) {
                    do {
                        imageData = try Data(contentsOf: theProfileImageUrl as URL)
                        
                    } catch {
                        print("Unable to load data: \(error)")
                    }
                }
                
                // imageUrl = user.profile.imageURL(withDimension : 200)
                
            }
            var name = ""
            if user.profile.name != nil{
                name = user.profile.name
            }
            delegate?.loginSuccess(userDetails:["name":name,"username" : email!,"password" : "","email" : email!,"slogin_id": userId!], profileImageData: imageData,socialLoginType: LoginType.google)
            // ...
        } else {
            delegate?.loginFailed(message: error.localizedDescription)
        }
    }
}

class FacebookManager : NSObject{
    static let sharedFacebookManager = FacebookManager()
    weak var parentController : UIViewController?
    weak var delegate : LoginServiceDelegate?
    
    func getFacebookUserInfo(){
        let loginManager = FBSDKLoginManager()
        loginManager.logIn(withReadPermissions: ["email","public_profile"], from: parentController) { (result, error) in
            if (error == nil){
                let fbloginresult : FBSDKLoginManagerLoginResult = result!
                if fbloginresult.grantedPermissions != nil {
                    if(fbloginresult.grantedPermissions.contains("email"))
                    {
                        self.getFBUserData()
                        loginManager.logOut()
                    }
                }
            }
        }
        
    }
    
    func getFBUserData(){
        if((FBSDKAccessToken.current()) != nil){
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name,email,picture.width(480).height(480)"]).start(completionHandler: {[unowned self] (connection, result, error) -> Void in
                if (error == nil){
                    let dict = result as! [String : Any]
                    print(dict)
                    var imageData : Data? = nil
                    if let imageURL = ((dict["picture"] as? [String: Any])?["data"] as? [String: Any])?["url"] as? String {
                        print(imageURL)
                        do {
                            imageData = try Data(contentsOf: URL(string:imageURL) as! URL)
                            print("Image data is--->",imageData)
                        } catch {
                            print("Unable to load data: \(error)")
                        }
                    }
                    var name = ""
                    if dict["name"] != nil{
                        name = dict["name"] as! String
                    }
                    self.delegate?.loginSuccess(userDetails:["name":name,"username" : dict["email"] as! String  ,"password" : "","email" : dict["email"] as! String  ,"slogin_id": dict["id"] as! String], profileImageData: imageData ,socialLoginType: LoginType.facebook)
                    
                }
            })
        }
    }
}

class LinkedInManager : NSObject{
    static let sharedLinkedInManager = LinkedInManager()
    weak var delegate : LoginServiceDelegate?
    
    let linkedinHelper = LinkedinSwiftHelper(configuration: LinkedinSwiftConfiguration(clientId: LINKED_IN_CLIENT_ID, clientSecret: LINKED_IN_CLIENT_SECRET, state: LINKED_IN_STATE, permissions: ["r_liteprofile", "r_emailaddress"], redirectUrl: "http://localhost/"))
    
    
    func linkedInLogout(){
        //        self.linkedinHelper.requestURL("https://api.linkedin.com/uas/oauth2/invalidateToken", requestType: LinkedinSwiftRequestGet, success: { (response) in
        //            print("Logged Out")
        //        }) { (error) in
        //             print("error occured")
        //        }
        linkedinHelper.logout()
        removeCookies()
        
    }
    func removeCookies(){
        let cookieJar = HTTPCookieStorage.shared
        for cookie in cookieJar.cookies! {
            cookieJar.deleteCookie(cookie)
        }
    }
    // Using v2 apis
    
    func linkedInLogin(){
        var imageData : Data? = nil
        var name = ""
        var emailAddress = ""
        var userId = ""
        linkedinHelper.authorizeSuccess({ [unowned self]  (token) in
            
            print(token)
            self.linkedinHelper.requestURL("https://api.linkedin.com/v2/me?projection=(id,firstName,lastName,profilePicture(displayImage~:playableStreams))", requestType: LinkedinSwiftRequestGet, success: { (profileResponse) -> Void in
                print("Full Profile of user ---->",profileResponse)
                self.linkedinHelper.requestURL("https://api.linkedin.com/v2/emailAddress?q=members&projection=(elements*(handle~))",requestType: LinkedinSwiftRequestGet,success: { (response) -> Void in
                    print(response)
                    if let response = response.jsonObject as NSDictionary?{
                        if let emailResponse = (response.value(forKey: "elements") as? NSArray) , emailResponse.count >= 0{
                            if let email = emailResponse.value(forKeyPath: "handle~.emailAddress") as? NSArray,email.count >= 0{
                                emailAddress = email[0] as? String ?? ""
                                print("Mail id of user--->",email[0])
                                if let userDetail = profileResponse.jsonObject as NSDictionary? {
                                    let locale_language = (userDetail.value(forKeyPath: "firstName.preferredLocale.language") as? String ?? "") + "_" + (userDetail.value(forKeyPath: "firstName.preferredLocale.country") as? String ?? "")
                                    let firstName = (userDetail.value(forKeyPath: "firstName.localized.\(locale_language)"))
                                    let lastName = (userDetail.value(forKeyPath: "lastName.localized.\(locale_language)"))
                                    name = (firstName as? String ?? "") + " " + (lastName as? String ?? "")
                                    userId = (userDetail.value(forKey: "id") as? String ?? "")
                                    print("User Details are ---->",firstName,lastName,userId)
                                    if let profileImage = userDetail.value(forKey: "profilePicture") as? NSDictionary{
                                        if let displayImage = profileImage.value(forKey:"displayImage~") as? NSDictionary{
                                            print("Display Image is---->",displayImage)
                                            if let displayImageUrl = displayImage.value(forKey:"elements") as? NSArray,displayImageUrl.count >= 0{
                                                if let profilePicUrlArray = (displayImageUrl[0] as? NSDictionary)?.value(forKeyPath: "identifiers") as? NSArray,profilePicUrlArray.count >= 0{
                                                    if let profilPicThumbUrl = (profilePicUrlArray[0] as? NSDictionary)?.value(forKey: "identifier"){
                                                        print("PROFILE PIC URL IS ---->",profilPicThumbUrl)
                                                        do {
                                                            imageData = try Data(contentsOf: (URL(string:profilPicThumbUrl as! String))!)
                                                        } catch {
                                                            print("Unable to load data: \(error)")
                                                        }
                                                    }
                                                    
                                                    
                                                }
                                            }
                                        }
                                    }
                                }
                                self.delegate?.loginSuccess(userDetails:["name":name,"username" :  emailAddress ,"password" : "","email" :  emailAddress ,"slogin_id":  userId], profileImageData: imageData ,socialLoginType: LoginType.linkedin)
                                
                            }
                           
                        }
                    }
                })
               
            }) {(error) -> Void in
                
                self.delegate?.loginFailed(message: error.localizedDescription)
                //handle the error
            }
            //This token is useful for fetching profile info from LinkedIn server
            }, error: { (error) in
                self.delegate?.loginFailed(message: error.localizedDescription)
                
                //show respective error
        }) {
            self.delegate?.loginFailed(message: "Login Cancelled")
            //show sign in cancelled event
        }
    }
    
    //Using V1 apis
    /*   func linkedInLogin(){
     //            let linkedinHelper = LinkedinSwiftHelper(configuration: LinkedinSwiftConfiguration(clientId: LINKED_IN_CLIENT_ID, clientSecret: LINKED_IN_CLIENT_SECRET, state: LINKED_IN_STATE, permissions: ["r_basicprofile", "r_emailaddress"], redirectUrl: "http://localhost/"))
     linkedinHelper.authorizeSuccess({ [unowned self]  (token) in
     print(token)
     self.linkedinHelper.requestURL("https://api.linkedin.com/v1/people/~:(id,first-name,last-name,picture-url,email-address)?format=json", requestType: LinkedinSwiftRequestGet, success: { (response) -> Void in
     print(response)
     var imageData : Data? = nil
     
     
     if let theProfileImageUrl =  response.jsonObject["pictureUrl"] {
     do {
     imageData = try Data(contentsOf: URL(string:theProfileImageUrl as! String) as! URL)
     
     } catch {
     print("Unable to load data: \(error)")
     }
     }
     //                pictureUrl
     var name = ""
     if response.jsonObject["firstName"] != nil{
     name = response.jsonObject["firstName"] as! String
     }
     self.delegate?.loginSuccess(userDetails:["name":name,"username" :  response.jsonObject["emailAddress"]! as! String ,"password" : "","email" :  response.jsonObject["emailAddress"]!  as! String ,"slogin_id":  response.jsonObject["id"]!  as! String], profileImageData: imageData ,socialLoginType: LoginType.linkedin)
     //parse this response which is in the JSON format
     }) {(error) -> Void in
     
     self.delegate?.loginFailed(message: error.localizedDescription)
     //handle the error
     }
     //This token is useful for fetching profile info from LinkedIn server
     }, error: { (error) in
     self.delegate?.loginFailed(message: error.localizedDescription)
     
     //show respective error
     }) {
     self.delegate?.loginFailed(message: "Login Cancelled")
     //show sign in cancelled event
     }
     } */
    
}


