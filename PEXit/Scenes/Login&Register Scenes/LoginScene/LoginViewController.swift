//
//  LoginViewController.swift
//  PEXit
//
//  Created by Apple on 09/07/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import GoogleSignIn
import LinkedinSwift
import Firebase
import FirebaseInstanceID

enum LoginType{
    case  facebook
    case  google
    case  linkedin
}

class LoginViewController: UIViewController {

    @IBOutlet weak var pexitImageView: UIImageView!
    @IBOutlet weak var pexitImageViewTopPadding: NSLayoutConstraint!
    @IBOutlet weak var pexitImageViewBottomPadding: NSLayoutConstraint!
    
    @IBOutlet weak var userNameTextfield: TJTextField!
    @IBOutlet weak var passwordTextfield: TJTextField!
    @IBOutlet weak var signInButtonOutlet: UIButton!
    @IBOutlet weak var fbButtonOutlet: UIButton!
    @IBOutlet weak var linkedInButtonOutlet: UIButton!
    @IBOutlet weak var googleButtonOutlet: UIButton!
    @IBOutlet weak var registerNowButtonOutlet: UIButton!
    @IBOutlet weak var forgotUsernameButtonOutlet: UIButton!
    @IBOutlet weak var forgotPasswordButtonOutlet: UIButton!
   
   // @IBOutlet weak var registerNowBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var scrollView: UIScrollView!
    
    var details : [String : String?]?
    var facebookProfilePicData : Data? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if DeviceModel.IS_IPHONE_5 || DeviceModel.IS_IPHONE_4_OR_LESS
        {
            pexitImageViewTopPadding.constant = 35
            pexitImageViewBottomPadding.constant = 30
        }
            else if DeviceModel.IS_IPHONE_6
        {
            pexitImageViewTopPadding.constant = 50
            pexitImageViewBottomPadding.constant = 40
        }
        else
        {
            pexitImageViewTopPadding.constant = 100
            pexitImageViewBottomPadding.constant = 90
        }
        fetchToken()
//        registerNowBottomConstraint.constant = 10
//        forgotPasswordButtonOutlet.titleLabel?.adjustsFontSizeToFitWidth = true
//        forgotPasswordButtonOutlet.titleLabel?.minimumScaleFactor = 0.5;
//        forgotUsernameButtonOutlet.titleLabel?.adjustsFontSizeToFitWidth = true
//        forgotUsernameButtonOutlet.titleLabel?.minimumScaleFactor = 0.5;

//        registerNowTopConstraint.constant = self.view.frame.size.height - (forgotPasswordButtonOutlet.frame.size.height + forgotPasswordButtonOutlet.frame.origin.y) - (23 + registerNowButtonOutlet.frame.size.height)
        doSomeInitialSetups()
    }
    
    func fetchToken(){
        if Defaults.getFcmToken() == nil{
            if let token = FIRInstanceID.instanceID().token() {
                print("Token \(token) fetched");
            } else {
                print("Unable to fetch token");
            }
            let delegate = AppDelegate.sharedDelegate()
            delegate.connectToFCM() 
        }
      

    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.hideNavigationBar()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    //    scrollView.contentSize = CGSize(width:self.view.frame.size.width, height:self.view.frame.size.height)
    }
    
    //MARK: Do some initial setups
    
    func doSomeInitialSetups() {
        signInButtonOutlet.tag = BUTTON_TAGS.SIGN_IN_TAG.rawValue
        fbButtonOutlet.tag = BUTTON_TAGS.FB_TAG.rawValue
        linkedInButtonOutlet.tag = BUTTON_TAGS.LINKED_IN_TAG.rawValue
        googleButtonOutlet.tag = BUTTON_TAGS.GOOGLE_TAG.rawValue
        registerNowButtonOutlet.tag = BUTTON_TAGS.REGISTER_NOW_TAG.rawValue
        forgotUsernameButtonOutlet.tag = BUTTON_TAGS.FORGOT_USERNAME_TAG.rawValue
        forgotPasswordButtonOutlet.tag = BUTTON_TAGS.FORGOT_PASSWORD_TAG.rawValue
    }
    
    
    //MARK: ButtonActions
    @IBAction func buttonActions(_ sender: UIButton) {
        self.view.endEditing(true)
        switch BUTTON_TAGS(rawValue: sender.tag)!   {
        case .SIGN_IN_TAG:
            goForSignIn()
        case .FB_TAG:
            goForFacebookLogin()
        case .LINKED_IN_TAG:
            linkedInLogin()
        case .GOOGLE_TAG:
            goForGoogleLogIn()
        case .REGISTER_NOW_TAG:
            print("EDIT_PROJECT_REQUEST")
        case .FORGOT_USERNAME_TAG,.FORGOT_PASSWORD_TAG:
            goForForgotDetails(sender.tag)
        default: break
        }
    }
    
    func goForSignIn() {
        
        if  Utils.validateFormAndShowToastWithRequiredFields([userNameTextfield,passwordTextfield]) {
            apiLogin { (response) in
                
                self.pushToTabBarController()
            }
        }
    }
    

    
    func goForForgotDetails(_ buttonTag:Int)  {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ForgotDetailsViewController") as! ForgotDetailsViewController
        if buttonTag == BUTTON_TAGS.FORGOT_USERNAME_TAG.rawValue{
            vc.taskExecutionName = "Forgot username"
        }else if buttonTag == BUTTON_TAGS.FORGOT_PASSWORD_TAG.rawValue{
            vc.taskExecutionName = "Forgot password"
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }

}

extension LoginViewController : LoginServiceDelegate , SendEmail{
   
    
    
   
    //Google Login In Delegate Methods
    func goForGoogleLogIn()
    {
        GoogleManager.sharedGoogleManager.delegate = self
        GoogleManager.sharedGoogleManager.setup()
    }
    
    func googleSignInLaunch(_ viewController: UIViewController) {
        self.present(viewController, animated: true, completion: nil)
    }
    
    func googleSignInDismiss(_ viewController: UIViewController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func connectToFacebook(params : [String : String],profileImageData:Data? = nil){
        apiSocialLoginIn(params, socialLoginType: "facebook",profileImageData:profileImageData) { [weak self] (response) in
            if((response as! [String : Any])["status"] as? Int) == 0{
                self?.fetchEmailId(userDetails : params,profileImageData : profileImageData)
            }else{
                self?.pushToTabBarController()
            }
            
            
        }
    }
    
    func loginSuccess(userDetails:[String:String],profileImageData : Data? = nil,socialLoginType:LoginType) {
        
        switch socialLoginType{
        case .google:
            apiSocialLoginIn(userDetails, socialLoginType: "google",profileImageData:profileImageData){ (response) in
                 self.pushToTabBarController()
            }
            

        case .facebook:
            connectToFacebook(params: userDetails,profileImageData:profileImageData)
//            apiSocialLoginIn(userDetails, socialLoginType: "facebook") { (response) in
//
//                if((response as! [String : Any])["status"] as? Int) == 0{
//                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "ForgotDetailsViewController") as! ForgotDetailsViewController
//                    vc.taskExecutionName = "Facebook Email"
//                    vc.fetchEmailId(completion: { (email) in
//                        details["email"] = email
//
//                    })
////                    vc.delegate = self
//                    self.navigationController?.pushViewController(vc, animated: true)
//                }else{
//                     self.pushToTabBarController()
//                }
//
//            }
        case .linkedin:
           apiSocialLoginIn(userDetails, socialLoginType: "linkedin",profileImageData:profileImageData){ (response) in
                 self.pushToTabBarController()
                
            }
            
    }
       
    }
        
        func fetchEmailId(userDetails : [String : String?],profileImageData:Data? = nil){
            details = userDetails
            facebookProfilePicData = profileImageData
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ForgotDetailsViewController") as! ForgotDetailsViewController
            vc.taskExecutionName = "Facebook Email"
            vc.delegate = self
            DispatchQueue.main.async {
                 self.navigationController?.pushViewController(vc, animated: true)
            }
           
        }
        
        func sendEmailId(email : String){
            if var details = details{
                details["email"] = email
                connectToFacebook(params: details as! [String : String],profileImageData:facebookProfilePicData)
            }
        }
        
        func loginFailed(message: String) {
            //Toast.showToasts(message: message, controller: LoginViewController())
            self.displayToast(message)
        }
        
        //FaecbookLoginMethods
        
        func goForFacebookLogin(){
            FacebookManager.sharedFacebookManager.delegate = self
            FacebookManager.sharedFacebookManager.parentController = self
            FacebookManager.sharedFacebookManager.getFacebookUserInfo()
            
        }
        
        
        func linkedInLogin(){
            LinkedInManager.sharedLinkedInManager.delegate = self
            LinkedInManager.sharedLinkedInManager.linkedInLogin()
        }
        
    }



