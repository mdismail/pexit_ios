//
//  AlamofireManager.swift
//  PEXit
//
//  Created by Apple on 09/08/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import Alamofire


private var manager: AlamofireManager? = nil

class AlamofireManager: NSObject {
    
    class func shared() -> AlamofireManager {
        //    Singleton creation
        if manager == nil {
            manager = AlamofireManager()
        }
        return manager ?? AlamofireManager()
    }
    
    var isInternetAvailable: Bool {
        if currentReachabilityStatus == .notReachable {
            KEY_WINDOW?.makeToast(NO_INTERNET_CONNECTION)
            return false;
        } else {
            return true
        }
    }
    
    
    func requestWith(apiUrl: String,userProfileImage:Data? = nil, imageData: [MediaAttatchment]? = nil,videoData:[MediaAttatchment]? = nil ,fileData: [MediaAttatchment]? = nil ,pptData:[MediaAttatchment]? = nil ,attatchments:[MediaAttatchment]? = nil,parameters: [String : Any],isPost:Bool = false ,completion:@escaping((_ response:[String : Any]?,_ error:Error?) -> Void)){
        
        if isInternetAvailable{
            let completeUrl = "\(BASE_URL)\(apiUrl)"
            var headers: HTTPHeaders = [
                /* "Authorization": "your_access_token",  in case you need authorization header */
                "Content-type": "multipart/form-data",
                ]
            
            if isPost{
                headers["Auth"] = "Bearer \(Defaults.getAccessToken())"
            }
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                for (key, value) in parameters {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
                
                
                
                if isPost{
                    for image in imageData!{
                        multipartFormData.append(image.fileData!, withName: "poimage[]", fileName: image.fileName ?? "", mimeType: image.fileExtension!)
                    }
                    for video in videoData!{
                        multipartFormData.append(video.fileData!, withName: "poallvid[]", fileName: video.fileName ?? "", mimeType: video.fileExtension!)
                    }
                    for file in fileData!{
                        multipartFormData.append(file.fileData!, withName: "podocu[]", fileName: file.fileName ?? "", mimeType: file.fileExtension!)
                    }
                    for ppt in pptData!{
                        multipartFormData.append(ppt.fileData!, withName: "poppt[]", fileName: ppt.fileName ?? "", mimeType: ppt.fileExtension!)
                    }
                }
                
                
                if userProfileImage != nil{
                    multipartFormData.append(userProfileImage!, withName: "pimage", fileName: "image.jpg" , mimeType: "image/jpeg")
                }
                
                
            }, usingThreshold: UInt64.init(), to: completeUrl, method: .post, headers: headers) { (result) in
                switch result{
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        if let err = response.error{
                            completion(nil, err)
                            return
                        }
                        print("Succesfully uploaded")
                        
                        completion(response.result.value! as! [String : Any],nil)
                    }
                case .failure(let error):
                    print("Error in upload: \(error.localizedDescription)")
                    // onError?(error)
                }
            }
        }
        }
    
    
    func postComment(apiUrl: String,attatchments:[MediaAttatchment]? = nil,images:[MediaAttatchment]? = nil,videos:[MediaAttatchment]? = nil,parameters: [String : Any],completion:@escaping((_ response:[String : Any]?,_ error:Error?) -> Void)){
        
        if isInternetAvailable{
            let completeUrl = "\(BASE_URL)\(apiUrl)"
            let headers: HTTPHeaders = [
                /* "Authorization": "your_access_token",  in case you need authorization header */
                "Content-type": "multipart/form-data",
                "Auth" : "Bearer \(Defaults.getAccessToken())"
                ]
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                for (key, value) in parameters {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
             
                if attatchments != nil{
                    for attatchment in attatchments!{
                        multipartFormData.append(attatchment.fileData!, withName: "attach[]", fileName: attatchment.fileName ?? "", mimeType: "")
                    }
                   
                }
                if images != nil{
                    for image in images!{
                        multipartFormData.append(image.fileData!, withName: "images[]", fileName: image.fileName ?? "", mimeType: image.fileExtension ?? "")
                    }
                    
                }
                if videos != nil{
                    for video in videos!{
                        multipartFormData.append(video.fileData!, withName: "videos[]", fileName: video.fileName ?? "", mimeType: video.fileExtension ?? "")
                    }
                    
                }
                
                
            }, usingThreshold: UInt64.init(), to: completeUrl, method: .post, headers: headers) { (result) in
                switch result{
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        if let err = response.error{
                            completion(nil, err)
                            return
                        }
                        print("Succesfully uploaded")
                        
                        completion(response.result.value! as! [String : Any],nil)
                    }
                case .failure(let error):
                    print("Error in upload: \(error.localizedDescription)")
                    // onError?(error)
                }
            }
        }
    }
    
    
    func applyForJob(jobId: Int,attatchments:[MediaAttatchment]? = nil,parameters: [String : Any]? = nil,completion:@escaping((_ response:[String : Any]?,_ error:Error?) -> Void)){
        
        let apiUrl = APPLY_FOR_JOB(jobId)
        if isInternetAvailable{
            let completeUrl = "\(BASE_URL)\(apiUrl)"
            let headers: HTTPHeaders = [
                /* "Authorization": "your_access_token",  in case you need authorization header */
                "Content-type": "multipart/form-data",
                "Auth" : "Bearer \(Defaults.getAccessToken())"
            ]
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                if parameters != nil{
                    for (key, value) in parameters! {
                        multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                    }
                    
                }
                
                if attatchments != nil{
                    for attatchment in attatchments!{
                        multipartFormData.append(attatchment.fileData!, withName: "resume", fileName: attatchment.fileName ?? "", mimeType: "")
                    }
                    
                }
                
                
            }, usingThreshold: UInt64.init(), to: completeUrl, method: .post, headers: headers) { (result) in
                switch result{
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        if let err = response.error{
                            completion(nil, err)
                            return
                        }
                        print("Succesfully uploaded")
                        
                        completion(response.result.value! as! [String : Any],nil)
                    }
                case .failure(let error):
                    print("Error in upload: \(error.localizedDescription)")
                    // onError?(error)
                }
            }
        }
    }
    
    func updateUserProfile(apiUrl: String,userProfileImage:Data? = nil, imageData: [MediaAttatchment]? = nil,videoData:[MediaAttatchment]? = nil ,fileData: [MediaAttatchment]? = nil ,pptData:[MediaAttatchment]? = nil ,attatchments:[MediaAttatchment]? = nil,parameters: [String : Any],completion:@escaping((_ response:[String : Any]?,_ error:Error?) -> Void)){
        
        if isInternetAvailable{
            let completeUrl = "\(BASE_URL)\(apiUrl)"
            let headers: HTTPHeaders = [
                /* "Authorization": "your_access_token",  in case you need authorization header */
                "Content-type": "multipart/form-data",
                "Auth" : "Bearer \(Defaults.getAccessToken())"
                ]
            
            
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                for (key, value) in parameters {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
                
               for image in imageData!{
                        multipartFormData.append(image.fileData!, withName: "pphot[]", fileName: image.fileName ?? "", mimeType: image.fileExtension!)
                    }
//                    for video in videoData!{
//                        multipartFormData.append(video.fileData!, withName: "poallvid[]", fileName: video.fileName ?? "", mimeType: video.fileExtension!)
//                    }
                    for file in fileData!{
                        multipartFormData.append(file.fileData!, withName: "pfle[]", fileName: file.fileName ?? "", mimeType: file.fileExtension!)
                    }
                    for ppt in pptData!{
                        multipartFormData.append(ppt.fileData!, withName: "pprest[]", fileName: ppt.fileName ?? "", mimeType: ppt.fileExtension!)
                    }
                
                if userProfileImage != nil{
                    multipartFormData.append(userProfileImage!, withName: "pimage", fileName: "image.jpg" , mimeType: "image/jpeg")
                }
                
            }, usingThreshold: UInt64.init(), to: completeUrl, method: .post, headers: headers) { (result) in
                switch result{
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        if let err = response.error{
                            completion(nil, err)
                            return
                        }
                        print("Succesfully uploaded")
                        
                        completion(response.result.value! as! [String : Any],nil)
                    }
                case .failure(let error):
                    print("Error in upload: \(error.localizedDescription)")
                    // onError?(error)
                }
            }
        }
        
    }
    
    
    
    func updateUserWorkProfile(apiUrl: String, imageData: [MediaAttatchment]? = nil,videoData:[MediaAttatchment]? = nil ,fileData: [MediaAttatchment]? = nil ,pptData:[MediaAttatchment]? = nil ,attatchments:[MediaAttatchment]? = nil,parameters: [String : Any],completion:@escaping((_ response:[String : Any]?,_ error:Error?) -> Void)){
        
        if isInternetAvailable{
            let completeUrl = "\(BASE_URL)\(apiUrl)"
            let headers: HTTPHeaders = [
                /* "Authorization": "your_access_token",  in case you need authorization header */
                "Content-type": "multipart/form-data",
                "Auth" : "Bearer \(Defaults.getAccessToken())"
            ]
            
            
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                for (key, value) in parameters {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
                
                if imageData != nil{
                    for image in imageData!{
                        multipartFormData.append(image.fileData!, withName: "pho[]", fileName: image.fileName ?? "", mimeType: image.fileExtension!)
                    }
                }
                
                if fileData != nil{
                    for file in fileData!{
                        multipartFormData.append(file.fileData!, withName: "fle[]", fileName: file.fileName ?? "", mimeType: file.fileExtension!)
                    }
                }
               
                
                if pptData != nil{
                    for ppt in pptData!{
                        multipartFormData.append(ppt.fileData!, withName: "pres[]", fileName: ppt.fileName ?? "", mimeType: ppt.fileExtension!)
                    }
                }
               
                
                
                
            }, usingThreshold: UInt64.init(), to: completeUrl, method: .post, headers: headers) { (result) in
                switch result{
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        if let err = response.error{
                            completion(nil, err)
                            return
                        }
                        print("Succesfully uploaded")
                        
                        completion(response.result.value! as! [String : Any],nil)
                    }
                case .failure(let error):
                    print("Error in upload: \(error.localizedDescription)")
                    // onError?(error)
                }
            }
        }
        
    }
    
    
    func uploadSponsorContent(apiUrl: String, imageData: [MediaAttatchment]? = nil,videoData:[MediaAttatchment]? = nil ,fileData: [MediaAttatchment]? = nil ,pptData:[MediaAttatchment]? = nil ,attatchments:[MediaAttatchment]? = nil,parameters: [String : Any],completion:@escaping((_ response:[String : Any]?,_ error:Error?) -> Void)){
        
        if isInternetAvailable{
            let completeUrl = "\(BASE_URL)\(apiUrl)"
            let headers: HTTPHeaders = [
                /* "Authorization": "your_access_token",  in case you need authorization header */
                "Content-type": "multipart/form-data",
                "Auth" : "Bearer \(Defaults.getAccessToken())"
            ]
            
            
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                for (key, value) in parameters {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
                
                if imageData != nil{
                    for image in imageData!{
                        multipartFormData.append(image.fileData!, withName: "scimage[]", fileName: image.fileName ?? "", mimeType: image.fileExtension!)
                    }
                }
                
                if fileData != nil{
                    for file in fileData!{
                        multipartFormData.append(file.fileData!, withName: "scfile[]", fileName: file.fileName ?? "", mimeType: file.fileExtension!)
                    }
                }
                
                
                if pptData != nil{
                    for ppt in pptData!{
                        multipartFormData.append(ppt.fileData!, withName: "scppt[]", fileName: ppt.fileName ?? "", mimeType: ppt.fileExtension!)
                    }
                }
                
                if videoData != nil{
                    for video in videoData!{
                        multipartFormData.append(video.fileData!, withName: "scvideo[]", fileName: video.fileName ?? "", mimeType: video.fileExtension!)
                    }
                }
             
            }, usingThreshold: UInt64.init(), to: completeUrl, method: .post, headers: headers) { (result) in
                switch result{
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        if let err = response.error{
                            completion(nil, err)
                            return
                        }
                        print("Succesfully uploaded")
                        
                        completion(response.result.value! as! [String : Any],nil)
                    }
                case .failure(let error):
                    print("Error in upload: \(error.localizedDescription)")
                    // onError?(error)
                }
            }
        }
        
    }
    
    
    
    func uploadServicesContent(apiUrl: String,attachmentArray:[String:[MediaAttatchment]?]? = nil ,parameters: [String : Any],completion:@escaping((_ response:[String : Any]?,_ error:Error?) -> Void)){
        START_LOADING_VIEW()
        if isInternetAvailable{
            let completeUrl = "\(BASE_URL)\(apiUrl)"
            let headers: HTTPHeaders = [
                /* "Authorization": "your_access_token",  in case you need authorization header */
                "Content-type": "multipart/form-data",
                "Auth" : "Bearer \(Defaults.getAccessToken())"
            ]
            
           
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                for (key, value) in parameters {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
                if attachmentArray != nil{
                    for (key, value) in attachmentArray!{
                        if value != nil{
                            for object in value!{
//                                let image = UIImage.init(data: object.fileData!)
                                print("Image is --->",object.fileName)
                                multipartFormData.append((object.fileData!), withName: key, fileName: (object.fileName ?? ""), mimeType: (object.fileExtension)!)
                            }
                            
                        }
                    }
                }
                
                

            }, usingThreshold: UInt64.init(), to: completeUrl, method: .post, headers: headers) { (result) in
                switch result{
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        if let err = response.error{
                            completion(nil, err)
                            return
                        }
                        print("Succesfully uploaded")
                        
                        completion(response.result.value! as! [String : Any],nil)
                    }
                case .failure(let error):
                    print("Error in upload: \(error.localizedDescription)")
                    // onError?(error)
                }
            }
        }
        
    }
    
    
    func updateCompanyProfile(apiUrl: String, fileData: MediaAttatchment? = nil,completion:@escaping((_ response:[String : Any]?,_ error:Error?) -> Void)){
        
        if isInternetAvailable{
            let completeUrl = "\(BASE_URL)\(apiUrl)"
            let headers: HTTPHeaders = [
                /* "Authorization": "your_access_token",  in case you need authorization header */
                "Content-type": "multipart/form-data",
                "Auth" : "Bearer \(Defaults.getAccessToken())"
            ]
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in

                multipartFormData.append((fileData?.fileData!)!, withName: "fle", fileName: (fileData?.fileName!)! ?? "", mimeType: (fileData?.fileExtension!)!)
     
            }, usingThreshold: UInt64.init(), to: completeUrl, method: .post, headers: headers) { (result) in
                switch result{
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        if let err = response.error{
                            completion(nil, err)
                            return
                        }
                        print("Succesfully uploaded")
                        
                        completion(response.result.value! as! [String : Any],nil)
                    }
                case .failure(let error):
                    print("Error in upload: \(error.localizedDescription)")
                    // onError?(error)
                }
            }
        }
        
    }
    
}

