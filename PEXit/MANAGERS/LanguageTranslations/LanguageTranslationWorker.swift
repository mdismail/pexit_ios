//
//  LanguageTranslationWorker.swift
//  PEXit
//
//  Created by ats on 01/12/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation
import UIKit

struct ReturnedJson: Codable {
    var translations: [TranslatedStrings]
}
struct TranslatedStrings: Codable {
    var text: String
    var to: String
}

class LanguageTranslationWoker : NSObject{
    func translateCurrentString(newLanguageCode : String , stringsToTranslate: [String],textType : String? = "plain",completionHandler: @escaping(_ response : AnyObject?) -> Void){
        struct encodeText: Codable {
            var text = String()
            
        }
        
      //  let azureKey = "0f56be04ebd54c76944ed2ac39b45944"
        let azureKey = "8a79f6941a954bc88ee90f78feda7b26"
        let contentType = "application/json"
        let traceID = "A14C9DB9-0DED-48D7-8BBE-C517A1A8DBB0"
        let host = "dev.microsofttranslator.com"
//        let apiURL = "https://dev.microsofttranslator.com/translate?api-version=3.0&from=" + previosuLanguageCode + "&to=" + newLanguageCode
        let apiURL = "https://dev.microsofttranslator.com/translate?api-version=3.0"  + "&to=" + newLanguageCode + "&textType=" + textType!
    //    let text2Translate = stringToTranslate
    
//       print("API URL ----->",apiURL)
        var toTranslate = [encodeText]()
        for strToTranslate in stringsToTranslate{
             var encodeTextObj = encodeText()
             encodeTextObj.text = strToTranslate
             toTranslate.append(encodeTextObj)
        }
//        print("TEXT TO TRANSLATE ---->", toTranslate)
       
        let jsonEncoder = JSONEncoder()
        let jsonToTranslate = try? jsonEncoder.encode(toTranslate)
        let url = URL(string: apiURL)
        var request = URLRequest(url: url!)
        
        request.httpMethod = "POST"
        request.addValue(azureKey, forHTTPHeaderField: "Ocp-Apim-Subscription-Key")
        request.addValue(contentType, forHTTPHeaderField: "Content-Type")
        request.addValue(traceID, forHTTPHeaderField: "X-ClientTraceID")
        request.addValue(host, forHTTPHeaderField: "Host")
        request.addValue(String(describing: jsonToTranslate?.count), forHTTPHeaderField: "Content-Length")
        request.httpBody = jsonToTranslate
        
        let config = URLSessionConfiguration.default
        let session =  URLSession(configuration: config)
        
        let task = session.dataTask(with: request) { (responseData, response, responseError) in
            
            if responseError != nil {
                print("this is the error ", responseError!)
                //KEY_WINDOW?.makeToast(NO_INTERNET_CONNECTION)
                completionHandler(nil)
            }
            print("*****")
//            let json = try? JSONSerialization.jsonObject(with: responseData!, options: .init(rawValue: 0))
//            var localResp = responseData
//            localResp = nil
            if let responseLanguage = responseData{
                self.parseJson(jsonData: responseLanguage, completionHandler: { (response) in
                    completionHandler(response)
                })
            }
           
        }
        task.resume()
//
        
    }
    
    
    func parseJson(jsonData: Data,completionHandler: @escaping(_ response : AnyObject?) -> Void) {
        
        //*****TRANSLATION RETURNED DATA*****
    
        let jsonDecoder = JSONDecoder()
        let langTranslations = try? jsonDecoder.decode(Array<ReturnedJson>.self, from: jsonData)
       // let numberOfTranslations = langTranslations!.count - 1
        completionHandler(langTranslations as AnyObject)
       // print(langTranslations!.count)

    }
    
    
}
