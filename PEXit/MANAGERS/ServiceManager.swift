

import UIKit
private var manager: ServiceManager? = nil

class ServiceManager: NSObject {
    
    static var imagesCount : Int? = 0

    class func shared() -> ServiceManager {
        //    Singleton creation
        if manager == nil {
            manager = ServiceManager()
        }
        return manager ?? ServiceManager()
    }
    
    var isInternetAvailable: Bool {
        if currentReachabilityStatus == .notReachable {
            STOP_LOADING_VIEW()
            KEY_WINDOW?.makeToast(NO_INTERNET_CONNECTION)
            return false;
        } else {
            return true
        }
    }
    
    var internetConnectivity : Bool{
        if currentReachabilityStatus == .notReachable {
            STOP_LOADING_VIEW()
           
            return false;
        } else {
            return true
        }
    }
    
    //MARK:- Methods
    
    class  func methodType(requestType: String,  url: String, params: NSDictionary?, paramsData: Data?,  completion:((_ response : Any?,_ responseData : Data?, _ statusCode: Int)->Void)?, failure:((_ response : Any?,  _ statusCode: Int)->Void)?) -> Void
    {
        let urlWithBaseUrl = "\(BASE_URL)\(url)"
        let completeURL = urlWithBaseUrl.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)
        var urlRequest = URLRequest.init(url: URL.init(string: completeURL!)!)
        
        if completeURL?.contains("secure") == true {
            if url == LOGIN_URL, requestType == GET_REQUEST {
                let authStr = String(format: "%@:%@", params?.value(forKey: "username") as! String, params?.value(forKey: "password") as! String)
                let authData = authStr.data(using: String.Encoding.utf8)!
                let authValue = authData.base64EncodedString()
                urlRequest.setValue("Basic \(String(describing: authValue))", forHTTPHeaderField: "Authorization")
            } else {
                let accessTocken = getLoginData()["accessToken"] as! String
                urlRequest.setValue("Bearer \(accessTocken)", forHTTPHeaderField: "Authorization")
                
                if url == UPDATE_DEVICE_URL {
                    let udId: UUID? = UIDevice.current.identifierForVendor
                    var pushRegParams = [AnyHashable: Any]()
                    pushRegParams["platformId"] = "\(2)"
                    pushRegParams["imei"] = udId?.uuidString
                    pushRegParams["pushRegId"] = Defaults.getPushRegistrationId()
                    
                    let postData = try? JSONSerialization.data(withJSONObject: pushRegParams as Any, options: .init(rawValue: 0))
                    urlRequest.httpBody = postData
                } else {
                    if params != nil{
                        let dict = params as! NSDictionary
                        let postData = try? JSONSerialization.data(withJSONObject: dict as Any, options: .init(rawValue: 0))
                        urlRequest.httpBody = postData
                    }else if paramsData != nil
                    {
                        urlRequest.httpBody = paramsData
                    }
                }
            }
        }
        else {
            if params != nil{
                let postData = try? JSONSerialization.data(withJSONObject: params as Any, options: .init(rawValue: 0))
                urlRequest.httpBody = postData
            }else if paramsData != nil
            {
                urlRequest.httpBody = paramsData
            }
        }
        if Defaults.getLoginStatus() == true{
            let accessTocken = Defaults.getAccessToken()
            print("Access Token iss--->", accessTocken)
            urlRequest.setValue("Bearer \(accessTocken)", forHTTPHeaderField: "Auth")
        }
        
        urlRequest.httpMethod = requestType
        urlRequest.setValue("application/json", forHTTPHeaderField: "Accept")
        urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest.timeoutInterval = 60
        
        self.sessionWithRequest(urlRequest: urlRequest, completion: completion, failure: failure)
    }
    
    //MARK:- SessionRequest
    class func sessionWithRequest(urlRequest: URLRequest, completion:((_ response : Any?,_ responseData : Data?, _ statusCode: Int) -> Void)?, failure:((_ response : Any?, _ statusCode: Int)->Void)?) -> Void
    {
        if ServiceManager.shared().isInternetAvailable == true {
            
            let sessionConfiguration =  URLSessionConfiguration.default
            let session = URLSession.init(configuration: sessionConfiguration)
            
            let dataTask = session.dataTask(with: urlRequest) { (data, response, error) in
                
                let httpResponse = response as? HTTPURLResponse
                
                let code = httpResponse?.statusCode
                //                let statusMsg = HTTPURLResponse.localizedString(forStatusCode: code!) ?? ""
                //                print(statusMsg )
                if error == nil && data != nil {
                    let json = try? JSONSerialization.jsonObject(with: data!, options: .init(rawValue: 0))
                    if  json != nil {
                        
                        if completion != nil {
                            if json is [AnyHashable:Any]
                            {
                                let jsonResponse = json as! [AnyHashable:Any]
                                let responseCode = !(json is NSNumber) ? (jsonResponse["code"] as? Int):0
                                if (responseCode != nil)
                                {
                                    if responseCode == INVALID_CREDENTIALS
                                    {
                                        Defaults.setUserLoggedIn(false)
                                        Utils.removeNSUserDefaultsPersistanceStorage()
                                        ServiceManager.shared().switchToLoginClass()
                                        STOP_LOADING_VIEW()
                                        completion! (json,data, code!)
                                        //edited for bad credentials
                                        
                                    }
                                    else if responseCode == 9000
                                    {
                                        STOP_LOADING_VIEW()
                                        if failure != nil {
                                            failure! (json, code!)
                                        }
                                    }
                                    else if responseCode != SUCCESS_CODE && responseCode != INVALID_USER && String(describing: jsonResponse["message"]).count > 0
                                    {
                                        STOP_LOADING_VIEW()
                                        APP_DELEGATE.currentViewController.showErrorToastMessage(json)
                                        if failure != nil {
                                            failure! (json, code!)
                                        }
                                    }
                                    else
                                    {
                                        completion! (json,data, code!)
                                    }
                                }
                                else
                                {
                                    completion! (json,data, code!)
                                }
                            }
                            else if json is Int64
                            {
                                completion! (json,data, code!)
                            }
                            else
                            {
                                completion! (json,data, code!)
                            }
                            
                        }
                    } else {
                        if failure != nil {
                            failure! (json, code!)
                        }
                    }
                } else {
                    if failure != nil && code != nil {
                        failure! (error, code!)
                    } else if failure != nil {
                        DispatchQueue.main.async{
                             KEY_WINDOW?.makeToast(error?.localizedDescription)
                        }
                        
                        failure! (error, 0)
                    }
                }
            }
            dataTask.resume()
            session.finishTasksAndInvalidate()
        }
       
    }
  
    
    class func uploadMultipleImages(imagesArray: Array<Any>?, array: NSMutableArray?, url : String,  completion:((_ response : Any?)-> Void)?, failure:((_ response : Any?) -> Void)?) -> Void
    {
        
        
        let completeURL = url.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)
        let image_ = self.fixOrientationWithImage(image: imagesArray![imagesCount!] as! UIImage)
        let imageData = UIImageJPEGRepresentation(image_, 0.3)
        let request = NSMutableURLRequest.init()
        request.cachePolicy = .reloadIgnoringLocalAndRemoteCacheData
        request.httpShouldHandleCookies = false
        request.timeoutInterval = 50
        request.httpMethod = POST_REQUEST
        
        let boundary = "------VohpleBoundary4QuqLuM1cE5lMwCy"
        let contentType = String.init(format: "multipart/form-data; boundary=%@", boundary)
        request.setValue(contentType, forHTTPHeaderField: "Content-Type")
        
        let body = NSMutableData.init()
        
        if imageData != nil {
            body.append((String.init(format: "--%@\r\n", boundary).data(using: String.Encoding.utf8))!)
            body.append((String.init(format: "Content-Disposition: form-data; name=\"%@\"; filename=\"image.jpg\"\r\n", "file").data(using: String.Encoding.utf8))!)
            body.append((String.init(format: "Content-Type: image/jpg\r\n\r\n").data(using: String.Encoding.utf8))!)
            body.append(imageData!)
            body.append((String.init(format: "\r\n").data(using: String.Encoding.utf8))!)
        }
        body.append((String.init(format: "--%@--\r\n", boundary).data(using: String.Encoding.utf8))!)
        request.httpBody = body as Data
        request.url = URL.init(string: completeURL!)
        request.setValue("Bearer \(getLoginData()["accessToken"] as! String))", forHTTPHeaderField: "Authorization")
        let sessionConfiguration = URLSessionConfiguration.default
        let session = URLSession.init(configuration: sessionConfiguration)
        
        var uploadTask = URLSessionUploadTask()
        uploadTask = session.uploadTask(with: request as URLRequest, from: nil) { (data, response, error) in
            
            if error == nil && data != nil {
                let json = try? JSONSerialization.jsonObject(with: data!, options: .init(rawValue: 0))
                if  json != nil {
                    
                    array?.add(json!)
                    if array?.count == (imagesArray?.count)! || (imagesArray?.count)! == imagesCount! {
                        completion! (array)
                        imagesCount = 0
                    }else{
                        imagesCount = imagesCount! + 1
                        ServiceManager.uploadMultipleImages(imagesArray: imagesArray,array: array, url: url, completion: completion, failure: failure)
                    }
                } else {
                    if (imagesArray?.count)! == imagesCount {
                        completion! (array)
                    }
                }
            } else {
                if (imagesArray?.count)!  == imagesCount {
                    completion! (array)
                }
            }
        }
        uploadTask.resume()
        session.finishTasksAndInvalidate()
    }
    
    
    class func uploadImage(image: UIImage!, url: String, completion:((_ response : Any?)-> Void)?, failure:((_ response : Any?) -> Void)? ) ->Void
    {
        let completeURL = url.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)
        let image_ = self.fixOrientationWithImage(image: image)
        let imageData = UIImageJPEGRepresentation(image_, 0.2)
        let request = NSMutableURLRequest.init()
        request.cachePolicy = .reloadIgnoringLocalAndRemoteCacheData
        request.httpShouldHandleCookies = false
        request.timeoutInterval = 60
        request.httpMethod = POST_REQUEST
        
        let boundary = "------VohpleBoundary4QuqLuM1cE5lMwCy"
        let contentType = String.init(format: "multipart/form-data; boundary=%@", boundary)
        request.setValue(contentType, forHTTPHeaderField: "Content-Type")
        
        let body = NSMutableData.init()
        
        if imageData != nil {
            body.append((String.init(format: "--%@\r\n", boundary).data(using: String.Encoding.utf8))!)
            body.append((String.init(format: "Content-Disposition: form-data; name=\"%@\"; filename=\"image.jpg\"\r\n", "file").data(using: String.Encoding.utf8))!)
            body.append((String.init(format: "Content-Type: image/jpg\r\n\r\n").data(using: String.Encoding.utf8))!)
            body.append(imageData!)
            body.append((String.init(format: "\r\n").data(using: String.Encoding.utf8))!)
        }
        body.append((String.init(format: "--%@--\r\n", boundary).data(using: String.Encoding.utf8))!)
        request.httpBody = body as Data
        request.url = URL.init(string: completeURL!)
        request.setValue("Bearer \(getLoginData()["accessToken"] as! String))", forHTTPHeaderField: "Authorization")
        let sessionConfiguration = URLSessionConfiguration.default
        let session = URLSession.init(configuration: sessionConfiguration)
        
        var uploadTask = URLSessionUploadTask()
        uploadTask = session.uploadTask(with: request as URLRequest, from: nil) { (data, response, error) in
            if error == nil && data != nil {
                let json = try? JSONSerialization.jsonObject(with: data!, options: .init(rawValue: 0))
                if  json != nil {
                    if completion != nil { completion! (json) }
                } else {
                    if failure != nil { failure! (json) }
                }
            } else {
                if failure != nil { failure! (error) }
            }
        }
        uploadTask.resume()
        session.finishTasksAndInvalidate()
    }
    
    
    class func fixOrientationWithImage(image: UIImage) -> UIImage {
        
        if image.imageOrientation == UIImageOrientation.up { return image }
        
        var transform = CGAffineTransform.identity
        
        switch image.imageOrientation {
        case .down, .downMirrored:
            transform = transform.translatedBy(x: image.size.width, y: image.size.height)
            transform = transform.rotated(by: CGFloat(Double.pi));
            
        case .left, .leftMirrored:
            transform = transform.translatedBy(x: image.size.width, y: 0);
            transform = transform.rotated(by: CGFloat(Double.pi / 2));
            
        case .right, .rightMirrored:
            transform = transform.translatedBy(x: 0, y: image.size.height);
            transform = transform.rotated(by: CGFloat(-Double.pi / 2));
            
        case .up, .upMirrored:
            break
        }
        
        switch image.imageOrientation {
            
        case .upMirrored, .downMirrored:
            transform = transform.translatedBy(x: image.size.width, y: 0)
            transform = transform.scaledBy(x: -1, y: 1)
            
        case .leftMirrored, .rightMirrored:
            transform = transform.translatedBy(x: image.size.height, y: 0)
            transform = transform.scaledBy(x: -1, y: 1);
            
        default:
            break;
        }
        
        // Now we draw the underlying CGImage into a new context, applying the transform
        // calculated above.
        let ctx = CGContext(
            data: nil,
            width: Int(image.size.width),
            height: Int(image.size.height),
            bitsPerComponent: image.cgImage!.bitsPerComponent,
            bytesPerRow: 0,
            space: image.cgImage!.colorSpace!,
            bitmapInfo: UInt32(image.cgImage!.bitmapInfo.rawValue)
        )
        
        ctx!.concatenate(transform);
        
        switch image.imageOrientation {
            
        case .left, .leftMirrored, .right, .rightMirrored:
            // Grr...
            ctx?.draw(image.cgImage!, in: CGRect(x:0 ,y: 0 ,width: image.size.height ,height:image.size.width))
            
        default:
            ctx?.draw(image.cgImage!, in: CGRect(x:0 ,y: 0 ,width: image.size.width ,height:image.size.height))
            break;
        }
        
        // And now we just create a new UIImage from the drawing context
        let cgimg = ctx!.makeImage()
        let img = UIImage(cgImage: cgimg!)
        return img;
    }
    

    @objc func switchToLoginClass()
    {
        APP_DELEGATE.currentViewController.moveToLoginScreen()
    }
    
}
