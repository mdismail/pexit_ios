
//
//  RealmDBManager.swift
//  SecurelyShare
//
//  Created by Md Ismail on 5/29/18.
//  Copyright © 2018 SecurelyShare. All rights reserved.
//

import Foundation
import RealmSwift

class RealmDBManager: NSObject {
    
    static let sharedManager = RealmDBManager()
    
    var realmURL: String!
    var config : Realm.Configuration!
    var realm  : Realm!
    override init() {
        super.init()
        self.configureRealmDatabase()
       
    }
    
    // MARK: - CONFIGURATION
    func configureRealmDatabase() {
        
//        For encryption
//        let finalKey = getKey()
//        config = Realm.Configuration(encryptionKey: finalKey as Data)
        
       
        config = Realm.Configuration(
            // Set the new schema version. This must be greater than the previously used
            // version (if you've never set a schema version before, the version is 0).
            schemaVersion: 1,
            
            // Set the block which will be called automatically when opening a Realm with
            // a schema version lower than the one set above
            migrationBlock: { migration, oldSchemaVersion in
                // We haven’t migrated anything yet, so oldSchemaVersion == 0
                if (oldSchemaVersion < 1) {
                    // Nothing to do!
                    // Realm will automatically detect new properties and removed properties
                    // And will update the schema on disk automatically
                }
        })
       
        config.fileURL = config.fileURL!.deletingLastPathComponent()
        // Creating custom path to save Realm files in document directory
        let customPath =  config.fileURL?.appendingPathComponent("MyRealm")
        
        do
        {
            try FileManager.default.createDirectory(atPath: customPath!.path, withIntermediateDirectories: true, attributes: nil)
        }
        catch let error as NSError
        {
            NSLog("Unable to create directory \(error.debugDescription)")
        }
         config.fileURL = config.fileURL!.appendingPathComponent("MyRealm",isDirectory:true).appendingPathComponent("pexit.realm")
        // Attempt to open the encrypted Realm
        
       
        do {
            _ = try Realm(configuration: config)
            realmURL = config.fileURL!.absoluteString
            //print("realmURLrealmURLrealmURL",realmURL);
        }
        catch let error as NSError {
            // If the encryption key was not accepted, the error will state that the database was invalid
            fatalError("Error opening Realm: \(error)")
        }
    }
    
    // MARK: - Get Realm
    func getRealm() -> Realm? {
        
        if  self.realm != nil{
            return realm
        }else{
            do {
                
                if let url = URL(string: realmURL) {
                    self.realm = try Realm(configuration: config)
                    return self.realm
                }
            } catch (let error as NSError) {
                print("ERROR: Realm: new Realm was not created: \(error.localizedDescription)")
            } catch {
                print("ERROR: Realm: new Realm was not UnknownError")
            }
        }
        print("getting ralm to save",realmURL)
        
        return nil
    }
    
    // MARK: - Add
    func addObject(object: Object) {
        
        do {
            try realm.write({
               // REPLACING REALM ADD WITH CREATE TO CHECK IF EXCEPTION IS AVOIDED
                realm.add(object, update: false)
              //  realm.create(Object.self, value: object, update: false)
                //print("RLM Saved new Object")
            })
        }catch let error {
            print(error)
        }
    }
    
    func addOrUpdateObject(object: Object) {
        
        do {
            try realm.write({
                 // REPLACING REALM ADD WITH CREATE TO CHECK IF EXCEPTION IS AVOIDED
                realm.add(object, update: true)
               // realm.create(Object.self, value: object, update: true)
                //print("RLM NEW Saved/Updated")
            })
        }catch let error {
            print(error)
        }
    }
    
    // MARK: - Delete
    
    func deleteObjectSynchronously(object: Object) {
        if let realm = self.getRealm() {
            do {
                try realm.write() {
                    realm.delete(object)
                    realm.refresh()
                }
            } catch (let error as NSError) {
                
                //print("ERROR: Realm: writing to database, ERROR: \(error.localizedDescription)")
            }
        }
    }
    
    func deleteRealmDb(){
        do {
            try! realm.write {
                realm.deleteAll()
            }
        }catch let error {
            print(error)
        }
        
    }
    

    
    // MARK: - Fetch
    func fetchObjects(object: Object.Type) -> Array<Any> {
        
        if let realm = self.getRealm() {
            return Array(realm.objects(object.self))
        }
        
        return []
    }
    
    func fetchObject(object: Object.Type) -> Object? {
        
        if let realm = self.getRealm() {
            return realm.objects(object.self).first
        }
        
        return nil
    }
    
    func fetchObjectwithKey(object: Object.Type,key:String) -> Object? {
        
        if let realm = self.getRealm() {
            return realm.object(ofType: object.self, forPrimaryKey: key)
        }
        
        return nil
    }
    
    func fetchObjectsWithPredicate(object: Object.Type,pred:NSPredicate) -> Results<Object>? {
        
        if let realm = self.getRealm() {
            return realm.objects(object.self).filter(pred)
        }
        return nil
    }
    
    func getKey() -> Data {
        // Identifier for our keychain entry - should be unique for your application
        let keychainIdentifier = Utils.getRealmDBEncryptionKey();
        
        let keychainIdentifierData = keychainIdentifier.data(using: String.Encoding.utf8, allowLossyConversion: false)!
        
        // First check in the keychain for an existing key
        var query: [NSString: AnyObject] = [
            kSecClass: kSecClassKey,
            kSecAttrApplicationTag: keychainIdentifierData as AnyObject,
            kSecAttrKeySizeInBits: 512 as AnyObject,
            kSecReturnData: true as AnyObject
        ]
        
        // To avoid Swift optimization bug, should use withUnsafeMutablePointer() function to retrieve the keychain item
        // See also: http://stackoverflow.com/questions/24145838/querying-ios-keychain-using-swift/27721328#27721328
        var dataTypeRef: AnyObject?
        var status = withUnsafeMutablePointer(to: &dataTypeRef) { SecItemCopyMatching(query as CFDictionary, UnsafeMutablePointer($0)) }
        if status == errSecSuccess {
            //print("*******keyDatakeyDatakeyDatakeyData",(dataTypeRef as! Data).hexEncodedString(),"\n \n");
            //print("queryqueryqueryquery",query);
            return dataTypeRef as! Data
        }
        
        // No pre-existing key from this application, so generate a new one
        let keyData = NSMutableData(length: 64)!
        let result = SecRandomCopyBytes(kSecRandomDefault, 64, keyData.mutableBytes.bindMemory(to: UInt8.self, capacity: 64))
        assert(result == 0, "Failed to get random bytes")
        
        // Store the key in the keychain
        query = [
            kSecClass: kSecClassKey,
            kSecAttrApplicationTag: keychainIdentifierData as AnyObject,
            kSecAttrKeySizeInBits: 512 as AnyObject,
            kSecValueData: keyData
        ]
        
        status = SecItemAdd(query as CFDictionary, nil)
        assert(status == errSecSuccess, "Failed to insert the new key in the keychain")
        print("keyDatakeyDatakeyDatakeyData",keyData);
        print("queryqueryqueryquery",query);
        return keyData as! Data
    }
}

