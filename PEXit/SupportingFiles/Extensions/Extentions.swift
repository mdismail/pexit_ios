


import Foundation
import UIKit
import Presentr
import AVKit
import SafariServices
import MIBadgeButton
import ActionSheetPicker_3_0


extension UIViewController {
    
    func moveToVCAccordingToLoginObject() -> Void {
        DispatchQueue.main.async {
            if  Defaults.getLoginStatus() == true {
                self.pushToTabBarController()
        }
    }
    }
    
    func moveToLoginScreen() {
        if !(APP_DELEGATE.currentViewController is LoginViewController) {
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                UIApplication.shared.applicationIconBadgeNumber = 0// set badge count 0
                
                let viewControllers:Array = (APP_DELEGATE.currentViewController.navigationController?.tabBarController?.navigationController?.viewControllers)!
                for i in 0..<viewControllers.count {
                    let obj = viewControllers[i]
                    if (obj is LoginViewController) {
                        APP_DELEGATE.currentViewController.navigationController?.tabBarController?.navigationController?.popToViewController(obj, animated: true)
                        return
                    }
                }
                DispatchQueue.main.async {
                    let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                    APP_DELEGATE.currentViewController.navigationController?.pushViewController(viewController, animated: true)
                }
            }
        }
    }
    
    //MARK:- For Showing CustomAlert
    func showCustomAlert(_ title: String, message: String, okButtonTitle: String?, cancelButtonTitle: String?, completionHandler:((_ buttonAction: UIAlertActionTitle)->Void)?) {
       /* let alert = CustomAlertViewController.init(nibName: "CustomAlertViewController", bundle: nil)

        alert.showAlertWith(title, message: message, image: image, buttonTitle: buttonTitle, alertType: alertType, action: action)
        present(alert, animated: true, completion: nil)  */
        
        let presenter: Presentr = {
            let presenter = Presentr(presentationType: .alert)
            presenter.transitionType = .coverHorizontalFromLeft
            presenter.dismissTransitionType = .coverHorizontalFromRight
            return presenter
        }()
        print("OKAY-->",okButtonTitle!.capitalized)
        let okTitle = okButtonTitle!.capitalized
        let cancelTitle = cancelButtonTitle!.capitalized
        
        let alertViewController = AlertViewController()
        alertViewController.titleText = title
        alertViewController.bodyText = message
        let deleteAction = AlertAction(title: okTitle , style: .custom(textColor: .black)) { (action) in
            completionHandler!(UIAlertActionTitle.DELETE)
        }
        
        let okAction = AlertAction(title: cancelTitle, style: .custom(textColor: .black)) { (action) in
            completionHandler!(UIAlertActionTitle.CANCEL)
        }
        
        alertViewController.addAction(deleteAction)
        alertViewController.addAction(okAction)
        
        presenter.presentationType = .alert
        customPresentViewController(presenter, viewController: alertViewController, animated: true, completion: nil)
        
        
    }
    
    //MARK:- For Pushing To Specified ViewController
    func push(toViewControllerNamed viewController: String?) {
        navigationController?.pushViewController((storyboard?.instantiateViewController(withIdentifier: viewController ?? ""))!, animated: true)
    }
    
     //MARK:- For Pushing To TAB Bar Controller
    func pushToTabBarController() {
        DispatchQueue.main.async {
            print("Access token is----->",Defaults.getAccessToken())
            let params = ["deviceType":"ios","fcmToken":Defaults.getFcmToken()]
            let worker = SettingsWorker()
            worker.updateFCMToken(params: params)
            
            ////self.navigationController?.pushViewController((self.storyboard?.instantiateViewController(withIdentifier: "TabBarController"))!, animated: false)
            let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let controller = storyboard.instantiateViewController(withIdentifier: "TabBarController")
            controller.modalTransitionStyle = .crossDissolve
            KEY_WINDOW?.rootViewController = controller
            KEY_WINDOW?.makeKeyAndVisible()
        }
        
    }

    
//    func pushToConditionsViewController(with type: DATA_CONTENT_TYPE) {
//        let vc = storyboard?.instantiateViewController(withIdentifier: "ConditionsViewController") as? ConditionsViewController
//        vc!.indexValue = type
//        if let aVc = vc {
//            navigationController?.pushViewController(aVc, animated: true)
//        }
//    }
//
//
//    func pushToProjectsViewController(with type: PROJECT_TYPE) {
//        let vc = storyboard?.instantiateViewController(withIdentifier: "ProjectsViewController") as? ProjectsViewController
//        vc!.selectedProjectType = type
//        if let aVc = vc {
//            navigationController?.pushViewController(aVc, animated: true)
//        }
//    }
//
//    func pushToAttachmentsViewController(with attachmentId: Int64) {
//        let vc = storyboard?.instantiateViewController(withIdentifier: "AttachmentsViewController") as? AttachmentsViewController
//        vc?.attachmentId = attachmentId
//        if let aVc = vc {
//            navigationController?.pushViewController(aVc, animated: true)
//        }
//    }
    
    func showAttatchmentView(mediaUrl: String) {
        DispatchQueue.main.async {
            //let presenter = Presentr(presentationType: .popup)
           // let presenter = Presentr(presentationType: cus)
            let width = ModalSize.fluid(percentage: 0.85)
            let height = ModalSize.fluid(percentage: 0.85)
            let center = ModalCenterPosition.center
            let customType = PresentationType.custom(width: width, height: height, center: center)
            let presenter = Presentr(presentationType: customType)
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "webViewController") as! WebViewController
            controller.urlString = mediaUrl
            self.customPresentViewController(presenter, viewController: controller, animated: true)
        }
    }
    
    func openVideoPlayer(videoUrl : String){
        guard let url = URL(string: videoUrl) else {
            return
        }
        // Create an AVPlayer, passing it the HTTP Live Streaming URL.
        let player = AVPlayer(url: url)
        // Create a new AVPlayerViewController and pass it a reference to the player.
        let controller = AVPlayerViewController()
        controller.player = player
        // Modally present the player and call the player's play() method when complete.
        present(controller, animated: true) {
            player.play()
        }
    }
    
    func openLinks(linkUrl : String){
        let validUrlString = linkUrl.contains("http") || linkUrl.contains("https") ? linkUrl : "http://\(linkUrl)"
        guard let url = URL(string: validUrlString) else {
            return
        }
        let svc = SFSafariViewController(url: url)
        present(svc, animated: true, completion: nil)
    }
    
    
     func add(asChildViewController viewController: UIViewController) {
        // Add Child View Controller
        addChildViewController(viewController)
        // Add Child View as Subview
        view.addSubview(viewController.view)
        // Configure Child View
        viewController.view.frame = view.bounds
        viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        // Notify Child View Controller
        viewController.didMove(toParentViewController: self)
    }
    
     func remove(asChildViewController viewController: UIViewController) {
        // Notify Child View Controller
        viewController.willMove(toParentViewController: nil)
        
        // Remove Child View From Superview
        viewController.view.removeFromSuperview()
        
        // Notify Child View Controller
        viewController.removeFromParentViewController()
    }
    
    //MARK:- ForAddingBackButton
    func addBackButton() {
        navigationController?.navigationBar.tintColor = .white
        //self.navigationItem.hidesBackButton = YES;
        let backButton = UIBarButtonItem(image: UIImage(named: "back"), style: .plain, target: self, action: #selector(self.backAction))
        //    backButton.tintColor = BACKGROUND_WHITE_COLOR;
        
        //--------------------------//
        let imageView = UIImageView(frame: CGRect(x: 0, y: 12, width: SCREEN_WIDTH - 100, height: 40))
        imageView.image = #imageLiteral(resourceName: "PEXit.png")
        imageView.contentMode = .scaleAspectFit
        navigationItem.titleView = imageView
        //-------------------------//
        navigationItem.leftBarButtonItem = backButton
    }
    
    func addBackButton(withTitle backButtonTitle: String?) {
        navigationController?.navigationBar.tintColor = .white
        let backImage = UIBarButtonItem(image: UIImage(named: "back"), style: .plain, target: self, action: #selector(self.backAction))
        //    [backImage setTintColor:BACKGROUND_WHITE_COLOR];
        //    [backImage setImageInsets:UIEdgeInsetsMake(4,-10, 0, 0)];  //Bold
        navigationController?.navigationBar.tintColor = UIColor.white
        let lbNavTitle = UILabel(frame: CGRect(x: 0, y: 12, width: SCREEN_WIDTH - 100, height: 40))
        lbNavTitle.textAlignment = .center
        lbNavTitle.font = SFUIDisplay_Bold(n: 17)
        lbNavTitle.textColor = .white
        lbNavTitle.text = backButtonTitle
        lbNavTitle.numberOfLines = 2
        navigationItem.titleView = lbNavTitle
        navigationItem.leftBarButtonItem = backImage
    }
    
    func addBlockUserOption(){
        var rightBarButtons = [UIBarButtonItem]()
        let blockUserButton = UIBarButtonItem(image: UIImage(named: "more"), style: .plain, target: self, action: #selector(self.showPopOver))
        rightBarButtons.append(blockUserButton)
        self.navigationItem.rightBarButtonItems = rightBarButtons
    }
    
    @objc func showConfirmationOptionToBlock(type:String?="",itemToMarkAbuse:String? = "",completion: @escaping (Int) -> Void){
        var alertMessage = ""
        var actionTitle = ""
        if type == "block"{
            alertMessage = BLOCK_USER_ALERT
            actionTitle = UIAlertActionTitle.BLOCK.rawValue
        }else if type == "unblock"{
            alertMessage = UNBLOCK_USER_ALERT
            actionTitle = UIAlertActionTitle.UNBLOCK.rawValue
        }
        else{
            alertMessage = "You want to report this \(itemToMarkAbuse ?? "") abuse"
            actionTitle = UIAlertActionTitle.REPORT.rawValue
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.showCustomAlert(CONFIRMATION_ALERT, message: alertMessage, okButtonTitle: actionTitle, cancelButtonTitle: UIAlertActionTitle.CANCEL.rawValue) { (action) in
                switch action{
                case .DELETE:
                    completion(0)
                   // print("User choose to report abuse")
                case .CANCEL:
                    print("Dismiss Alert")
                default:
                    print("Test")
                }
            }
        }
        
    }
    
    @objc func showPopOver(){
        
    }
    
    
    
    func showAbuseCategoryOptions(completionHandler : @escaping ((String) -> Void)){
        let optionsArray = ["Violence","Harassment","Nudity","Spam","Terrorism","False news","Suicide or self-injury","Hate Speech","Other"]
        ActionSheetStringPicker.show(withTitle: "Give Feedback", rows: optionsArray, initialSelection: 0, doneBlock: { (key, index, value) in
            let optionChoosen = value
            if (optionChoosen as? String) == "Other"{
                self.showOtherCategoryInput(completionHandler: completionHandler)
            }else{
                completionHandler(optionChoosen as! String)
            }
            }, cancel: { (picker) in
                
        }, origin: self.view)
    }
    
    func showOtherCategoryInput(completionHandler : @escaping ((String) -> Void)){
        let alertController = UIAlertController(title: "Enter Category", message: "Please enter name of category under which you want to report", preferredStyle: .alert)
        alertController.addTextField { textField in
            textField.placeholder = "Category"
        }
        let confirmAction = UIAlertAction(title: "OK", style: .default) { [weak alertController] _ in
            guard let alertController = alertController, let textField = alertController.textFields?.first else { return }
            completionHandler(textField.text ?? "")
            //compare the current password and do action here
        }
        alertController.addAction(confirmAction)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        present(alertController, animated: true, completion: nil)
    }
    
    
    func addWhiteBackButton() {
        //    [self hideNavigationBar];
        //    self.navigationController.navigationBar.translucent = YES;
        let backButton = UIBarButtonItem(image: UIImage(named: "leftIcon"), style: .plain, target: self, action: #selector(self.backAction))
        //        backButton.tintColor = BACKGROUND_WHITE_COLOR;
        navigationItem.leftBarButtonItem = backButton
        navigationController?.hidesBarsOnSwipe = true
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
    }
    
    func makeNavigationbarTransparent() {
        navigationController?.hidesBarsOnSwipe = true
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
    }
    
    func makeNavigationbarVisible() {
        navigationController?.hidesBarsOnSwipe = false
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
    }

    func addBackButtonForNavigatingToCorrespondingPage(withTitle backButtonTitle: String?) {
        let backImage = UIBarButtonItem(image: UIImage(named: "back"), style: .plain, target: self, action: #selector(self.backActionForNavigatingToCorrespondingPage))
        //    [backImage setTintColor:BACKGROUND_WHITE_COLOR];
        //    [backImage setImageInsets:UIEdgeInsetsMake(4,-10, 0, 0)];  //Bold
        let lbNavTitle = UILabel(frame: CGRect(x: 0, y: 12, width: SCREEN_WIDTH - 100, height: 40))
        lbNavTitle.textAlignment = .center
        lbNavTitle.font = SFUIDisplay_Bold(n: 17)
        lbNavTitle.textColor = BLACK_COLOR
        lbNavTitle.text = backButtonTitle
        navigationItem.titleView = lbNavTitle
        navigationItem.leftBarButtonItem = backImage
    }
    
    func removeBackButton() {
        navigationItem.leftBarButtonItem = nil
        navigationItem.titleView = nil
        navigationItem.rightBarButtonItem = nil
        // self.navigationItem.hidesBackButton = YES;
    }
    
    @objc func backAction() {
        DispatchQueue.main.async {
            unowned let me = self
            me.view.endEditing(true)
            me.navigationController?.view.endEditing(true)
            me.navigationController?.popViewController(animated: true)
        }
    }
    
    @objc func backActionForNavigatingToCorrespondingPage() {
        //    [self.view endEditing:YES];
        //    [self.navigationController.view endEditing:YES];
        //    [self.navigationController popViewControllerAnimated:YES];
    }
    
    //MARK:- For hiding/unhiding navigation bar
    func hideNavigationBar() {
        navigationController?.isNavigationBarHidden = true
    }
    
    func showNavigationBar(withTitle title:String? = nil) {
        navigationController?.isNavigationBarHidden = false
        navigationController?.navigationBar.barStyle = .black
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.tintColor = UIColor.white
        if let title = title{
            navigationController?.navigationItem.title = title
            navigationItem.titleView?.tintColor = .white
        }
    }
    
    
    
    func hideNavigationBarWithAnimation() {
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    func showNavigationBarWithAnimation() {
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    // For navigation bar with notification icon and language translator and search functionality
    func showNavigationBarWithLeftAndRightBarButtonItems(leftBarButtonImage:String? = nil,searchBarButton:String? = nil,languageTranslator:String? = nil,titleViewImage:String? = nil, showRefresh : Bool? = false, showMessages:Bool? = false){
        showNavigationBar()
        if let leftImg = leftBarButtonImage{
            appNotificationBarButton = MIBadgeButton(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
            appNotificationBarButton.setImage(UIImage(named: leftImg), for: .normal)
            //appNotificationBarButton.backgroundColor = UIColor.red
            appNotificationBarButton.badgeEdgeInsets = UIEdgeInsetsMake(10, 0, 0, 5)
            appNotificationBarButton.badgeTextColor = UIColor.white
            appNotificationBarButton.badgeBackgroundColor = THEME_RED_COLOR
            appNotificationBarButton.addTarget(self, action: #selector(self.showNotifications), for: .touchUpInside)
            let App_NotificationBarButton : UIBarButtonItem = UIBarButtonItem(customView: appNotificationBarButton)
            appNotificationBarButton.contentHorizontalAlignment = .left
           
            self.navigationItem.leftBarButtonItem = App_NotificationBarButton


            
        }
        var rightBarButtons = [UIBarButtonItem]()
        if let leftImg = searchBarButton{
            let searchBarButtonItem = UIBarButtonItem(image: UIImage(named: leftImg), style: .plain, target: self, action: #selector(self.globalSearch))
            rightBarButtons.append(searchBarButtonItem)
           
        }
        if let leftImg = languageTranslator{
            let translatorBarButtonItem = UIBarButtonItem(image: UIImage(named: leftImg), style: .plain, target: self, action: #selector(self.showLanguages))
            rightBarButtons.append(translatorBarButtonItem)
        }

        if let hedaerImage = titleViewImage{
            let imageView = UIImageView(frame: CGRect(x: 0, y: 12, width: 66, height: 27))
            imageView.image = UIImage(named:hedaerImage)
            imageView.contentMode = .scaleAspectFit
            navigationItem.titleView = imageView
        }
        if globalRefreshButton != nil && showRefresh == true{
            rightBarButtons.append(globalRefreshButton)
        }
        if showMessages == true{
             let messageBarButtonItem =  UIBarButtonItem(image: UIImage(named: "homeChat"), style: .plain, target: self, action: #selector(self.showMessages))
            rightBarButtons.append(messageBarButtonItem)
        }
        
        self.navigationItem.setRightBarButtonItems(rightBarButtons, animated: true)
    }
    
    
    
    
    func addGlobalSearchRefreshBtn(){
        if globalRefreshButton == nil {
            globalRefreshButton = UIBarButtonItem(image: UIImage(named: "refresh"), style: .plain, target: self, action: #selector(self.clearGlobalSearch))
            self.navigationItem.rightBarButtonItems?.append(globalRefreshButton)
            self.navigationItem.setRightBarButtonItems(self.navigationItem.rightBarButtonItems, animated: true)
           
        }
       
    }
    
    @objc func clearGlobalSearch(){
        self.cleanUpGlobalSearch(previousSearchOption : CURRENT_GLOBAL_SEARCH_OPTION)
        self.refreshView(previousSearchOption : CURRENT_GLOBAL_SEARCH_OPTION)
        CURRENT_GLOBAL_SEARCH_OPTION = ""
        CURRENT_GLOBAL_SEARCH_KEYWORD = ""
    }
    
    @objc func removeGlobalSearchRefreshBtn(){
        if (globalRefreshButton != nil) {
        self.navigationItem.rightBarButtonItems?.remove(globalRefreshButton)
        self.navigationItem.setRightBarButtonItems(self.navigationItem.rightBarButtonItems, animated: true)
            
            globalRefreshButton = nil
        }
    }
    
    @objc func refreshView(previousSearchOption : String?){
        
    }
    
    @objc func refresh_SubView(){}
    
   @objc func showNotifications(_ sender:UIButton){
        
    }
    @objc func showMessages(_ sender:UIButton){
        
    }
    
    @objc func globalSearch(){
        let globalSearchController = self.storyboard?.instantiateViewController(withIdentifier: "globalSearchViewController") as! GlobalSearchViewController
        present(globalSearchController, animated: true, completion: nil)
    }
   
    @objc func showLanguages(){
        
    }
  
    
    func addNavigationBarWithTitleAndSubtitle(title:String? = nil,subTitle:String? = nil,size:CGFloat? = 200){
        self.navigationItem.titleView = nil
        let navigationView = UIView()
       // navigationView.backgroundColor = UIColor.red
        
        navigationView.frame = CGRect(x: 0, y: 0, width: size!, height: 44)
        let label = UILabel()
        
        label.frame = CGRect(x: 0, y: 0, width: size!, height: 20)
        label.text = title
        label.textColor = .white
        label.textAlignment = .center
        let button = UIButton()
        button.frame = CGRect(x: 0, y: label.frame.size.height + 8, width: size!, height: 20)
        button.titleLabel?.font = UIFont.init(name: "SFUIDisplay-Medium", size: 10)
        button.titleLabel?.textColor = UIColor.white
        button.setTitle(subTitle, for: .normal)
        button.setRightImage(imageName: "downArrow", padding: 10)
        button.contentEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 10.0, right: 0.0)
        let button1 = UIButton()
        button1.frame = CGRect(x: 0, y: 0, width: navigationView.frame.size.width, height: 44)
        button1.setTitle("", for: .normal)
        navigationView.addSubview(label)
        navigationView.addSubview(button)
        navigationView.addSubview(button1)
        navigationView.bringSubview(toFront: button1)
        button1.addTarget(self, action: #selector(self.clickOnSubtitleButton), for: .touchUpInside)
        self.navigationItem.titleView = navigationView
//        self.navigationItem.titleView.align
    }
    
    func addNavigationRightBarButton(){
        let moreActionBtn = UIBarButtonItem(image: UIImage(named: "Plus_white"), style: .plain, target: self, action: #selector(self.addNewData))
        self.navigationItem.setRightBarButton(moreActionBtn, animated: true)
    }
    
    @objc func addNewData(){
        
    }
    
    @objc func clickOnSubtitleButton(_ sender:UIButton){
        
    }
    
    
    //MARK:- For Showing the error messages
    func showErrorToastMessage(_ response:Any!)  {
        DispatchQueue.main.async {
            guard let message =  (response as! [String : AnyObject])["message"] as? String else { return }
            if message.count != 0 { self.displayToast(message) } else {self.displayToast(UNKNOWN_ERROR_MSG)}
        }
    }
    
    //MARK:- For hiding/unhiding the tabbar
    func hideTabBar() {
        tabBarController?.tabBar.isTranslucent = true
        tabBarController?.tabBar.isHidden = true
    }
    
    func showTabBar() {
        tabBarController?.tabBar.isHidden = false
        tabBarController?.tabBar.isTranslucent = false
    }
    
    func dismissViewController() -> Void {
        DispatchQueue.main.async {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    // Hide Keyboard
    func hideKeyboardWhenTappedOnView() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    //MARK:- For SpoofStatusBar
    func setSpoofStatusBar() {
        let statusBar = UIView(frame: CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: 20))
        statusBar.backgroundColor = APP_BG_COLOR
        view.addSubview(statusBar)
        view.bringSubview(toFront: statusBar)
    }
    
    //MARK:- For InternetConnectivity Observers
    func addObserverForInternetConnectivity() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.gotInternetConnectivity), name: NSNotification.Name(rawValue: INTERNET_CONNECTION), object: nil)
    }
    
    func removeObserverForInternetConnectivity() {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: INTERNET_CONNECTION), object: nil)
    }
    
    @objc func gotInternetConnectivity() {
        Utils.writeToast("Connection established")
//        CONNECTION_ESTABLISHED
        //    [[APP_DELEGATE.currentViewController.view viewWithTag:NoRecordFoundView_Tag] removeFromSuperview];
        KEY_WINDOW?.viewWithTag(NoRecordFoundView_Tag)?.removeFromSuperview()
    }
    
    //MARK:- Toast
    func displayToast(_ message : String) {
        DispatchQueue.main.async {
            KEY_WINDOW?.makeToast(message) // self.view .makeToast(message)
        }
    }
    
    
    
    func getLanguages(completionHandler: @escaping(_ languages : [String],_ languageArray:[AllLangDetails]) -> Void) {
        let worker = HomeWorker()
        var arrayLangInfo = [AllLangDetails]()
        worker.getLanguagesFromMicrosoftServer { (response) in
          //  print("Response is ---->", response)
            if response != nil{
                var languageArray = [String]()
                var eachLangInfo = AllLangDetails(code: " ", name: " ", nativeName: " ", dir: " ") //Use this instance to populate and then append to the array instance
                
                for languageValues in response.translation.values {
                    eachLangInfo.name = languageValues.name
                    eachLangInfo.nativeName = languageValues.nativeName
                    eachLangInfo.dir = languageValues.dir
                    arrayLangInfo.append(eachLangInfo)
                    languageArray.append(languageValues.name)
                }
                
                let countOfLanguages = response.translation.count
                var counter = 0
                
                for languageKey in response.translation.keys {
                    
                    if counter < countOfLanguages {
                        arrayLangInfo[counter].code = languageKey
                        counter += 1
                    }
                }
                
                languageArray = languageArray.sorted{$1 > $0}
                completionHandler(languageArray,arrayLangInfo)
                //self.presentLanguagesView(languageArray: languageArray)
            }
        }
    }
    
    @objc func searchLocallyUsingGlobal(searchKeyword : String, searchOptions : String){
        
    }
    @objc func searchLocallyUsingGlobal_subview(searchKeyword : String, searchOptions : String){}
    
    @objc func cleanUpGlobalSearch(previousSearchOption : String?){
        
    }
    @objc func cleanUpGlobalSearch_subview(){}
    
//    func presentLanguagesView(languageArray : [String]){
//        DispatchQueue.main.async {
//            let presenter = Presentr(presentationType: .popup)
//            let controller = self.storyboard?.instantiateViewController(withIdentifier: "commonViewController") as! CommonViewController
//            controller.isFromHome = true
//            controller.isLanguageUpdation = true
//            controller.itemsArray = languageArray
//            controller.delegate = self
//            self.customPresentViewController(presenter, viewController: controller, animated: true)
//        }
//    }
}




