//
//  AllExtensions.swift
//  Sqoolink
//
//  Created by Narendar N on 03/11/17.
//  Copyright © 2017 Narendar N. All rights reserved.
//

import Foundation
import UIKit
import SystemConfiguration
typealias UnixTime = Int

protocol Utilities {
}




//MARK:- Utilities Extension
extension NSObject:Utilities {
    
    enum ReachabilityStatus {
        case notReachable
        case reachableViaWWAN
        case reachableViaWiFi
    }
    
    var currentReachabilityStatus: ReachabilityStatus {
        
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        }) else {
            return .notReachable
        }
        
        var flags: SCNetworkReachabilityFlags = []
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
            return .notReachable
        }
        if flags.contains(.reachable) == false {
            return .notReachable
        }
        else if flags.contains(.isWWAN) == true {
            return .reachableViaWWAN
        }
        else if flags.contains(.connectionRequired) == false {
            return .reachableViaWiFi
        }
        else if (flags.contains(.connectionOnDemand) == true || flags.contains(.connectionOnTraffic) == true) && flags.contains(.interventionRequired) == false {
            return .reachableViaWiFi
        }
        else {
            return .notReachable
        }
    }
    
}

extension UIImage {
    
    public func fixedOrientation() -> UIImage {
        
        if imageOrientation == UIImageOrientation.up {
            return self
        }
        
        var transform: CGAffineTransform = CGAffineTransform.identity
        
        switch imageOrientation {
        case UIImageOrientation.down, UIImageOrientation.downMirrored:
            transform = transform.translatedBy(x: size.width, y: size.height)
            transform = transform.rotated(by: CGFloat(M_PI))
            break
        case UIImageOrientation.left, UIImageOrientation.leftMirrored:
            transform = transform.translatedBy(x: size.width, y: 0)
            transform = transform.rotated(by: CGFloat(M_PI_2))
            break
        case UIImageOrientation.right, UIImageOrientation.rightMirrored:
            transform = transform.translatedBy(x: 0, y: size.height)
            transform = transform.rotated(by: CGFloat(-M_PI_2))
            break
        case UIImageOrientation.up, UIImageOrientation.upMirrored:
            break
        }
        
        switch imageOrientation {
        case UIImageOrientation.upMirrored, UIImageOrientation.downMirrored:
            transform.translatedBy(x: size.width, y: 0)
            transform.scaledBy(x: -1, y: 1)
            break
        case UIImageOrientation.leftMirrored, UIImageOrientation.rightMirrored:
            transform.translatedBy(x: size.height, y: 0)
            transform.scaledBy(x: -1, y: 1)
        case UIImageOrientation.up, UIImageOrientation.down, UIImageOrientation.left, UIImageOrientation.right:
            break
        }
        
        let ctx: CGContext = CGContext(data: nil,
                                       width: Int(size.width),
                                       height: Int(size.height),
                                       bitsPerComponent: self.cgImage!.bitsPerComponent,
                                       bytesPerRow: 0,
                                       space: self.cgImage!.colorSpace!,
                                       bitmapInfo: CGImageAlphaInfo.premultipliedLast.rawValue)!
        
        ctx.concatenate(transform)
        
        switch imageOrientation {
        case UIImageOrientation.left, UIImageOrientation.leftMirrored, UIImageOrientation.right, UIImageOrientation.rightMirrored:
            ctx.draw(self.cgImage!, in: CGRect(x: 0, y: 0, width: size.height, height: size.width))
        default:
            ctx.draw(self.cgImage!, in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
            break
        }
        
        let cgImage: CGImage = ctx.makeImage()!
        
        return UIImage(cgImage: cgImage)
    }
}

//MARK:- UnixTime Extension
extension UnixTime {
    private func formatType(form: String) -> DateFormatter {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        dateFormatter.dateFormat = form
        return dateFormatter
    }
    var dateFull: NSDate
    {
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat = "dd/MM/yyyy"
        
        return NSDate(timeIntervalSince1970: Double(self))
    }
    var toHourFormat: String
    {
        return formatType(form: "HH:mm:ss").string(from: dateFull as Date)
    }
    var toDateFormat: String
    {
        return formatType(form: "dd/MM/yyyy").string(from: dateFull as Date)
    }
}
//MARK:- UIView Extension

extension Array where Element: Equatable {
    func removeDuplicates() -> Array {
        return reduce(into: []) { result, element in
            if !result.contains(element) {
                result.append(element)
            }
        }
    }
}

extension UITableView {
    func scrollToLastCell(animated : Bool,countOfLasRow:Int) {
        let lastSectionIndex = self.numberOfSections - 1 // last section
        let lastRowIndex = self.numberOfRows(inSection: lastSectionIndex) - countOfLasRow // last row
        self.scrollToRow(at: IndexPath(row: lastRowIndex, section: lastSectionIndex), at: .top, animated: animated)
    }
    
    func scrollToFirstRow() {
        let indexPath = IndexPath.init(row: 0, section: 0)
        self.scrollToRow(at: indexPath, at: .top, animated: true)
    }
}
extension UIView {
    
   
        func blink() {
            self.alpha = 0.2
            UIView.animate(withDuration: 1, delay: 0.0, options: [.curveLinear, .repeat, .autoreverse], animations: {self.alpha = 1.0}, completion: nil)
        }
   
    
        @IBInspectable var dropShadow: Bool {
            set{
                if newValue {
                    layer.shadowColor = UIColor.black.cgColor
                    layer.shadowOpacity = 0.4
                    layer.shadowRadius = 1
                    layer.shadowOffset = CGSize.zero
                } else {
                    layer.shadowColor = UIColor.clear.cgColor
                    layer.shadowOpacity = 0
                    layer.shadowRadius = 0
                    layer.shadowOffset = CGSize.zero
                }
            }
            get {
                return layer.shadowOpacity > 0
            }
        }
   
    enum ViewBorder: String {
        case left, right, top, bottom
    }
    func add(border: ViewBorder, color: UIColor, width: CGFloat) {
        let borderLayer = CALayer()
        borderLayer.backgroundColor = color.cgColor
        borderLayer.name = border.rawValue
        switch border {
        case .left:
            borderLayer.frame = CGRect(x: 0, y: 0, width: width, height: self.frame.size.height)
        case .right:
            borderLayer.frame = CGRect(x: self.frame.size.width - width, y: 0, width: width, height: self.frame.size.height)
        case .top:
            borderLayer.frame = CGRect(x: 0, y: 0, width: self.frame.size.width, height: width)
        case .bottom:
            borderLayer.frame = CGRect(x: 0, y: self.frame.size.height - width, width: self.frame.size.width, height: width)
        }
        self.layer.addSublayer(borderLayer)
    }
    func remove(border: ViewBorder) {
        guard let sublayers = self.layer.sublayers else { return }
        var layerForRemove: CALayer?
        for layer in sublayers {
            if layer.name == border.rawValue {
                layerForRemove = layer
            }
        }
        if let layer = layerForRemove {
            layer.removeFromSuperlayer()
        }
    }
    
    func createGradientLayer(frame: CGRect) {
        DispatchQueue.main.async {
            let layers = self.layer.sublayers
            if layers != nil, layers?.count != 0{
                for index in 0 ... layers!.count-1 {
                    let layer = self.layer.sublayers![index]
                    if layer .isKind(of: CAGradientLayer.self) {
                        self.layer.sublayers?.remove(at: index)
                        break
                    }
                }
            }
            var gradientLayer: CAGradientLayer!
            gradientLayer = CAGradientLayer()
            gradientLayer.frame = frame
            gradientLayer.colors = [UIColor.clear.cgColor, BLACK_COLOR_WITH_OPACITY.cgColor]
            self.layer.addSublayer(gradientLayer)
        }
        
    }
    

    
    func addConstraints(_ format: String, constraintViews: [UIView]) {
        var viewsDictionary = [String: Any]()
        for view: UIView in constraintViews {
            let key = "v\((constraintViews as NSArray).index(of: view))"
            view.translatesAutoresizingMaskIntoConstraints = false
            viewsDictionary[key] = view
        }
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: format, options: [], metrics: nil, views: viewsDictionary))
    }
    
}



//MARK:-UIColor Extension
extension UIColor {
    public convenience init?(hexString: String, withOpacity: CGFloat) {
        let r, g, b : CGFloat
        
        if hexString.hasPrefix("#") {
            let start = hexString.index(hexString.startIndex, offsetBy: 1)
            let hexColor = String(hexString[start...])
            
            if hexColor.count == 8 {
                let scanner = Scanner(string: hexColor)
                var hexNumber: UInt64 = 0
                
                if scanner.scanHexInt64(&hexNumber) {
                    r = CGFloat((hexNumber & 0xff000000) >> 24) / 255
                    g = CGFloat((hexNumber & 0x00ff0000) >> 16) / 255
                    b = CGFloat((hexNumber & 0x0000ff00) >> 8) / 255
                    
                    self.init(red: r, green: g, blue: b, alpha: withOpacity)
                    return
                }
            }
        }
        return nil
    }
    
   class func hexStringToUIColor (hex:String, withOpacity: CGFloat) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(withOpacity)
        )
    }
    
    static func rgb(red: CGFloat, green: CGFloat, blue: CGFloat)->UIColor{
        return UIColor(red:red/255 , green: green/255, blue: blue/255, alpha: 1)
    }
}

extension UIButton {
    func textChanged(title:String, iconName: String, widthConstraints: NSLayoutConstraint){
        
        self.setTitle(title, for: .normal)
        self.setImage(UIImage(named: iconName), for: .normal)
        let imageWidth = self.imageView!.frame.width
        let textWidth = (title as NSString).size(withAttributes:[NSAttributedStringKey.font:self.titleLabel!.font!]).width
        let width = textWidth + imageWidth + 24
        //24 - the sum of your insets from left and right
        widthConstraints.constant = width
        self.layoutIfNeeded()
    }
}

extension String {
    func slice(from: String, to: String) -> String? {
        
        return (range(of: from)?.upperBound).flatMap { substringFrom in
            (range(of: to, range: substringFrom..<endIndex)?.lowerBound).map { substringTo in
                String(self[substringFrom..<substringTo])
            }
        }
    }
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: font], context: nil)
        
        return ceil(boundingBox.height)
    }
    
    public func htmlDecodedText() -> String{
        return self.stringByStrippingHTMLTags.stringByDecodingHTMLEntities
    }
    
    func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: font], context: nil)
        
        return ceil(boundingBox.width)
    }
    
    func urlEncodedString() -> String{
        return self.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed)!
    }
    
    var isValidURL: Bool {
            let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
            if let match = detector.firstMatch(in: self, options: [], range: NSRange(location: 0, length: self.endIndex.encodedOffset)) {
                // it is a link, if the match covers the whole string
                return match.range.length == self.endIndex.encodedOffset
            } else {
                return false
            }
    }
    
    var isValidYouTubeUrl : Bool{
        return self.range(of: "(http(s)??\\:\\/\\/)?(www\\.)?((youtube\\.com\\/watch\\?v=)|(youtu.be\\/))([a-zA-Z0-9\\-_])+",
                          options: String.CompareOptions.regularExpression,
                           range: nil, locale: nil) != nil
    }
    
    func getDate(dateFormat: String) ->String {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss.0"
        var date: Date? = formatter.date(from: self)
        if date == nil {
            formatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
            date = formatter.date(from: self)
        }
        if date == nil {
            formatter.dateFormat = "yyyy-MM-dd"
            date = formatter.date(from: self)
        }
        if date == nil {
            formatter.dateFormat = "HH:mm:ss"
            date = formatter.date(from: self)
        }
        if date == nil {
            formatter.dateFormat = "hh:mm a"
            date = formatter.date(from: self)
        }
        if date == nil {
            formatter.dateFormat = "yyyy-MM-dd"
            date = formatter.date(from: self)
        }
        if date == nil {
            formatter.dateFormat = "dd-MMM-yyyy"
            date = formatter.date(from: self)
        }
        if date == nil {
            formatter.dateFormat = "dd-MM-yyyy"
            date = formatter.date(from: self)
        }
        if date == nil {
            formatter.dateFormat = "dd-MMMM-yy"
            date = formatter.date(from: self)
        }
        if date == nil {
            formatter.dateFormat = "MM dd yyyy hh:mm.0"
            date = formatter.date(from: self)
        }
        if date == nil {
            formatter.dateFormat = "dd-MM-yyyy hh:mm a"
            date = formatter.date(from: self)
        }

        formatter.dateFormat = dateFormat
        if (dateFormat == "ddddd MMMM, yyyy mm:ss") {
            formatter.dateFormat = "dd MMMM,yyyy mm:ss"
            let dateS: String = formatter.string(from: date!)
            let array = dateS.components(separatedBy: " ")
            let dd: String? = array.first
            let dateSt = "\(dd!)\(getDayNumberSuffix(Int(dd ?? "") ?? 0)) \(array[1]) \(array[2])"
            return dateSt
        }
        return formatter.string(from: date!)
    }
    
    func getDayNumberSuffix(_ day: Int) -> String {
        if day >= 11 && day <= 13 {
            return "th"
        }
        switch day % 10 {
        case 1:
            return "st"
        case 2:
            return "nd"
        case 3:
            return "rd"
        default:
            return "th"
        }
    }
}

extension UINavigationController {

    
    /**Hiding keyboard by using tapgesture for specific viewcontrollers*/
    func hidekeyboard(viewController : UIViewController) {
        viewController.hideKeyboardWhenTappedOnView()
    }
   
    
}

extension UITextField{
    func addPaddingAndBorder() {
        self.layer.cornerRadius =  1
        self.layer.borderColor = UIColor.black.withAlphaComponent(0.55).cgColor
        self.layer.borderWidth = 1
        let leftView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: 20.0, height: 2.0))
        self.leftView = leftView
        self.leftViewMode = .always
    }
}


extension UITextView
{
    
    func leftAlignTextView(){
        self.textContainer.lineFragmentPadding = 0
    }
    
    /// Resize the placeholder when the UITextView bounds change
    override open var bounds: CGRect {
        didSet {
            self.resizePlaceholder()
        }
    }
    
    /// The UITextView placeholder text
    public var placeholder: String? {
        get {
            var placeholderText: String?
            
            if let placeholderLabel = self.viewWithTag(100) as? UILabel {
                placeholderText = placeholderLabel.text
            }
            
            return placeholderText
        }
        set {
            if let placeholderLabel = self.viewWithTag(100) as! UILabel? {
                placeholderLabel.text = newValue
                placeholderLabel.sizeToFit()
            } else {
                self.addPlaceholder(newValue!)
            }
        }
    }
    
    /// When the UITextView did change, show or hide the label based on if the UITextView is empty or not
    ///
    /// - Parameter textView: The UITextView that got updated
//    public func textViewDidChange(_ textView: UITextView) {
//        if let placeholderLabel = self.viewWithTag(100) as? UILabel {
//            placeholderLabel.isHidden = self.text.count > 0
//        }
//    }
    
    public func updateTextViewPlacholder(){
        if let placeholderLabel = self.viewWithTag(100) as? UILabel {
            placeholderLabel.isHidden = self.text.count > 0
        }
    }
    
    /// Resize the placeholder UILabel to make sure it's in the same position as the UITextView text
    private func resizePlaceholder() {
        if let placeholderLabel = self.viewWithTag(100) as! UILabel? {
            let labelX = self.textContainer.lineFragmentPadding
            let labelY = self.textContainerInset.top - 2
            let labelWidth = self.frame.width - (labelX * 2)
            let labelHeight = placeholderLabel.frame.height
            
            placeholderLabel.frame = CGRect(x: labelX, y: labelY, width: labelWidth, height: labelHeight)
        }
    }
    
    /// Adds a placeholder UILabel to this UITextView
    private func addPlaceholder(_ placeholderText: String) {
        let placeholderLabel = UILabel()
        
        placeholderLabel.text = placeholderText
        placeholderLabel.sizeToFit()
        
        placeholderLabel.font = self.font
        placeholderLabel.textColor = UIColor.lightGray
        placeholderLabel.tag = 100
        
        placeholderLabel.isHidden = self.text.count > 0
        
        self.addSubview(placeholderLabel)
        self.resizePlaceholder()
//        self.delegate = self
    }
}


extension UITableView {
    
    func registerCellNib(with cellName:String!) {
        register(UINib(nibName: cellName, bundle: nil), forCellReuseIdentifier: cellName)
    }
    
    func showPlaceholder(_ string: String) -> Void {
        hidePlaceholder()
        DispatchQueue.main.async {
            var placeholder = self.viewWithTag(989) as? UILabel
            let height = string.height(withConstrainedWidth: self.frame.size.width - 40, font: SFUIDisplay_Light(n: 16))
            if placeholder == nil {
                placeholder = UILabel.init(frame: CGRect.init(x: 20, y: 40, width: self.frame.size.width - 40, height: height + 20))
                placeholder?.font = SFUIDisplay_Light(n: 16)
                placeholder?.textColor = LIGHT_GRAY_COLOR
                placeholder?.textAlignment = .center
                placeholder?.numberOfLines = 0
                placeholder?.lineBreakMode = .byWordWrapping
                self.addSubview(placeholder!)
            }
            var size = placeholder?.frame.size
            size?.height = height
            placeholder?.text = string
            placeholder?.frame.size = size!
            placeholder?.tag = 989
            placeholder?.isHidden = false
//            self.bringSubview(toFront: placeholder!)
        }
        
    }
    func hidePlaceholder() {
        DispatchQueue.main.async {
            let placeholder = self.viewWithTag(989) as? UILabel
            if placeholder != nil {
                placeholder?.isHidden = true
            }
        }
    }
    
}


extension DateFormatter{
    func getMonthName(monthNumber : Int) -> String{
        return self.monthSymbols[monthNumber]
    }
    func getAllMonths() -> [String]{
        return self.monthSymbols
    }
}


extension UICollectionView {
    /**Registering ALL UICollectionViewCells*/
    func registerCellNib(with cellName:String!) {
        register(UINib(nibName: cellName, bundle: nil), forCellWithReuseIdentifier: cellName)
    }
    
    func showPlaceholder(_ string: String) -> Void {
        hidePlaceholder()
        DispatchQueue.main.async {
            var placeholder = self.viewWithTag(989) as? UILabel
            let height = string.height(withConstrainedWidth: self.frame.size.width - 40, font: SFUIDisplay_Light(n: 16))
            if placeholder == nil {
                placeholder = UILabel.init(frame: CGRect.init(x: 20, y: 40, width: self.frame.size.width - 40, height: height + 20))
                placeholder?.font = SFUIDisplay_Light(n: 16)
                placeholder?.textColor = LIGHT_GRAY_COLOR
                placeholder?.textAlignment = .center
                placeholder?.numberOfLines = 0
                placeholder?.lineBreakMode = .byWordWrapping
                self.addSubview(placeholder!)
            }
            var size = placeholder?.frame.size
            size?.height = height
            placeholder?.text = string
            placeholder?.frame.size = size!
            placeholder?.tag = 989
            placeholder?.isHidden = false
//            self.bringSubview(toFront: placeholder!)
        }
    }
    func hidePlaceholder() {
        DispatchQueue.main.async {
            let placeholder = self.viewWithTag(989) as? UILabel
            if placeholder != nil {
                placeholder?.isHidden = true
            }
        }
    }
}

func showBlurLoader(){
    DispatchQueue.main.async {
        let blurEffectView = UIVisualEffectView(effect: nil)
        blurEffectView.frame = BOUNDS
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        blurEffectView.backgroundColor = UIColor.clear
        
        let logo  = UIImageView.init(frame: CGRect.init(x: SCREEN_WIDTH/2-25, y: SCREEN_HEIGHT/2-25, width: 50, height: 50))
        logo.layer.masksToBounds = true
        logo.layer.cornerRadius = 8
        logo.image = UIImage.init(named: "jpgDoc")
        blurEffectView.contentView.addSubview(logo)
        
        let rotations = 1.0
        let duration = 5.0
        let rotationAnimation = CABasicAnimation.init(keyPath: "transform.rotation.y")
        rotationAnimation.toValue = NSNumber.init(value: Double.pi * 2.0 * rotations * duration)
        rotationAnimation.duration = duration
        rotationAnimation.isCumulative = true
        rotationAnimation.repeatCount = Float(INT_MAX)
        logo.layer.add(rotationAnimation, forKey: "rotationAnimation")
        KEY_WINDOW?.addSubview(blurEffectView)
    }
}

func removeBluerLoader(){
    DispatchQueue.main.async {
        _ = KEY_WINDOW?.subviews.map {
            if let blurEffectView = $0 as? UIVisualEffectView{
                _ = blurEffectView.subviews.map({ $0.removeFromSuperview() })
                $0.removeFromSuperview()
            }
        }
    }
}


//Extension Date

extension Date {
    var ticks: UInt64 {
        return UInt64((self.timeIntervalSince1970 + 62_135_596_800) * 10_000_000)
    }
    
    func timeAgoSinceDate(numericDates:Bool) -> String {
        let calendar = Calendar.current
        let now = Date()
        let earliest = self.toLocalTime() < now.toLocalTime() ? self.toLocalTime() : now.toLocalTime()
        let latest =  self.toLocalTime() > now.toLocalTime() ? self.toLocalTime() : now.toLocalTime()
        
        let unitFlags: Set<Calendar.Component> = [.second, .minute, .hour, .day, .month, .year]
        let components: DateComponents = calendar.dateComponents(unitFlags, from: earliest, to: latest)
        
        if let year = components.year {
            if (year >= 2) {
                return "\(year) years ago"
            } else if (year >= 1) {
                return stringToReturn(flag: numericDates, strings: ("1 year ago", "Last year"))
            }
        }
        
        if let month = components.month {
            if (month >= 2) {
                return "\(month) months ago"
            } else if (month >= 1) {
                return stringToReturn(flag: numericDates, strings: ("1 month ago", "Last month"))
            }
        }
        
        if let weekOfYear = components.weekOfYear {
            if (weekOfYear >= 2) {
                return "\(weekOfYear) months ago"
            } else if (weekOfYear >= 2) {
                return stringToReturn(flag: numericDates, strings: ("1 week ago", "Last week"))
            }
        }
        
        if let day = components.day {
            if (day >= 2) {
                return "\(day) days ago"
            } else if (day >= 2) {
                return stringToReturn(flag: numericDates, strings: ("1 day ago", "Yesterday"))
            }
        }
        
        if let hour = components.hour {
            if (hour >= 2) {
                return "\(hour) hours ago"
            } else if (hour >= 2) {
                return stringToReturn(flag: numericDates, strings: ("1 hour ago", "An hour ago"))
            }
        }
        
        if let minute = components.minute {
            if (minute >= 2) {
                return "\(minute) minutes ago"
            } else if (minute >= 2) {
                return stringToReturn(flag: numericDates, strings: ("1 minute ago", "A minute ago"))
            }
        }
        
        if let second = components.second {
            if (second >= 3) {
                return "\(second) seconds ago"
            }
        }
        
        return "Just now"
    }
    
    private func stringToReturn(flag:Bool, strings: (String, String)) -> String {
        if (flag){
            return strings.0
        } else {
            return strings.1
        }
    }
    
   
    
    func toLocalTime() -> Date {
        let timezone = TimeZone.current
        let seconds = TimeInterval(timezone.secondsFromGMT(for: self))
        return Date(timeInterval: seconds, since: self)
    }
}
// DATA Extensions

extension Data {
    struct HexEncodingOptions: OptionSet {
        let rawValue: Int
        static let upperCase = HexEncodingOptions(rawValue: 1 << 0)
    }
    
    func hexEncodedString(options: HexEncodingOptions = []) -> String {
        let hexDigits = Array((options.contains(.upperCase) ? "0123456789ABCDEF" : "0123456789abcdef").utf16)
        var chars: [unichar] = []
        chars.reserveCapacity(2 * count)
        for byte in self {
            chars.append(hexDigits[Int(byte / 16)])
            chars.append(hexDigits[Int(byte % 16)])
        }
        return String(utf16CodeUnits: chars, count: chars.count)
    }
}



import Foundation

internal let DEFAULT_MIME_TYPE = "application/octet-stream"

internal let mimeTypes = [
    "md": "text/markdown",
    "html": "text/html",
    "htm": "text/html",
    "shtml": "text/html",
    "css": "text/css",
    "xml": "text/xml",
    "gif": "image/gif",
    "jpeg": "image/jpeg",
    "jpg": "image/jpeg",
    "js": "application/javascript",
    "atom": "application/atom+xml",
    "rss": "application/rss+xml",
    "mml": "text/mathml",
    "txt": "text/plain",
    "jad": "text/vnd.sun.j2me.app-descriptor",
    "wml": "text/vnd.wap.wml",
    "htc": "text/x-component",
    "png": "image/png",
    "tif": "image/tiff",
    "tiff": "image/tiff",
    "wbmp": "image/vnd.wap.wbmp",
    "ico": "image/x-icon",
    "jng": "image/x-jng",
    "bmp": "image/x-ms-bmp",
    "svg": "image/svg+xml",
    "svgz": "image/svg+xml",
    "webp": "image/webp",
    "woff": "application/font-woff",
    "jar": "application/java-archive",
    "war": "application/java-archive",
    "ear": "application/java-archive",
    "json": "application/json",
    "hqx": "application/mac-binhex40",
    "doc": "application/msword",
    "pdf": "application/pdf",
    "ps": "application/postscript",
    "eps": "application/postscript",
    "ai": "application/postscript",
    "rtf": "application/rtf",
    "m3u8": "application/vnd.apple.mpegurl",
    "xls": "application/vnd.ms-excel",
    "eot": "application/vnd.ms-fontobject",
    "ppt": "application/vnd.ms-powerpoint",
    "wmlc": "application/vnd.wap.wmlc",
    "kml": "application/vnd.google-earth.kml+xml",
    "kmz": "application/vnd.google-earth.kmz",
    "7z": "application/x-7z-compressed",
    "cco": "application/x-cocoa",
    "jardiff": "application/x-java-archive-diff",
    "jnlp": "application/x-java-jnlp-file",
    "run": "application/x-makeself",
    "pl": "application/x-perl",
    "pm": "application/x-perl",
    "prc": "application/x-pilot",
    "pdb": "application/x-pilot",
    "rar": "application/x-rar-compressed",
    "rpm": "application/x-redhat-package-manager",
    "sea": "application/x-sea",
    "swf": "application/x-shockwave-flash",
    "sit": "application/x-stuffit",
    "tcl": "application/x-tcl",
    "tk": "application/x-tcl",
    "der": "application/x-x509-ca-cert",
    "pem": "application/x-x509-ca-cert",
    "crt": "application/x-x509-ca-cert",
    "xpi": "application/x-xpinstall",
    "xhtml": "application/xhtml+xml",
    "xspf": "application/xspf+xml",
    "zip": "application/zip",
    "bin": "application/octet-stream",
    "exe": "application/octet-stream",
    "dll": "application/octet-stream",
    "deb": "application/octet-stream",
    "dmg": "application/octet-stream",
    "iso": "application/octet-stream",
    "img": "application/octet-stream",
    "msi": "application/octet-stream",
    "msp": "application/octet-stream",
    "msm": "application/octet-stream",
    "docx": "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
    "xlsx": "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
    "pptx": "application/vnd.openxmlformats-officedocument.presentationml.presentation",
    "mid": "audio/midi",
    "midi": "audio/midi",
    "kar": "audio/midi",
    "mp3": "audio/mpeg",
    "ogg": "audio/ogg",
    "m4a": "audio/x-m4a",
    "ra": "audio/x-realaudio",
    "3gpp": "video/3gpp",
    "3gp": "video/3gpp",
    "ts": "video/mp2t",
    "mp4": "video/mp4",
    "mpeg": "video/mpeg",
    "mpg": "video/mpeg",
    "mov": "video/quicktime",
    "webm": "video/webm",
    "flv": "video/x-flv",
    "m4v": "video/x-m4v",
    "mng": "video/x-mng",
    "asx": "video/x-ms-asf",
    "asf": "video/x-ms-asf",
    "wmv": "video/x-ms-wmv",
    "avi": "video/x-msvideo"
]

internal func MimeType(ext: String?) -> String {
    let lowercase_ext: String = ext!.lowercased()
    if ext != nil && mimeTypes.contains(where: { $0.0 == lowercase_ext }) {
        return mimeTypes[lowercase_ext]!
    }
    return DEFAULT_MIME_TYPE
}

extension URL {
    public func mimeType() -> String {
        return MimeType(ext: self.pathExtension)
    }
    
    func expandURLWithCompletionHandler(completionHandler: @escaping (URL?) -> Void) {
        let dataTask = URLSession.shared.dataTask(with: self, completionHandler: {
                _, response, _ in
            if let expandedURL = response?.url {
                    completionHandler(expandedURL)
                }
            })
            dataTask.resume()
    }
    
}


extension UISearchBar{
    func customizeSearchBar(placeholderStr:String,rightImageStr:String? = nil,color:UIColor? = .black,placeholderColor:UIColor? = .gray){
        
        let searchTextField:UITextField = self.subviews[0].subviews.last as! UITextField
        searchTextField.layer.cornerRadius = 0
        searchTextField.textAlignment = NSTextAlignment.left
        if rightImageStr != nil{
            let image:UIImage = UIImage(named: rightImageStr!)!
            let imageView:UIImageView = UIImageView.init(image: image)
            searchTextField.rightView = imageView
        }
        let paddingView = UIView(frame: CGRect(x:0,y:0,width:2,height:10))
        searchTextField.leftView = paddingView
//        nameTextField.leftViewMode = .always
//        searchTextField.leftView = paddingView
        searchTextField.placeholder = placeholderStr
        searchTextField.rightViewMode = UITextFieldViewMode.always
        // SearchBar text
       // let textFieldInsideUISearchBar = self.value(forKey: "searchField") as? UITextField
        searchTextField.textColor = color
        searchTextField.font = UIFont(name: "Arial", size: 14)
        
        // SearchBar placeholder
        let textFieldInsideUISearchBarLabel = searchTextField.value(forKey: "placeholderLabel") as? UILabel
        textFieldInsideUISearchBarLabel?.textColor = placeholderColor
        textFieldInsideUISearchBarLabel?.font = UIFont(name: "Arial", size: 12)
    }
}


extension NSString {
    public func mimeType() -> String {
        return MimeType(ext: self.pathExtension)
    }
}

extension UITableView {
    func scrollToBottom(animated: Bool = true) {
        let section = self.numberOfSections
        if section > 0 {
            let row = self.numberOfRows(inSection: section - 1)
            if row > 0 {
                self.scrollToRow(at: IndexPath(item: row - 1, section: section - 1), at: .bottom, animated: animated)
            }
        }
    }
}

extension UILabel {
    
    func setLineSpacing(lineSpacing: CGFloat = 0.0, lineHeightMultiple: CGFloat = 0.0) {
        
        guard let labelText = self.text else { return }
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = lineSpacing
        paragraphStyle.lineHeightMultiple = lineHeightMultiple
        
        let attributedString:NSMutableAttributedString
        if let labelattributedText = self.attributedText {
            attributedString = NSMutableAttributedString(attributedString: labelattributedText)
        } else {
            attributedString = NSMutableAttributedString(string: labelText)
        }
        
        // Line spacing attribute
        attributedString.addAttribute(NSAttributedStringKey.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))
        
        self.attributedText = attributedString
    }
}


extension String {
    
   /* Returns you tube id from youtube url */
   var youtubeID: String? {
            let pattern = "((?<=(v|V)/)|(?<=be/)|(?<=(\\?|\\&)v=)|(?<=embed/))([\\w-]++)"
            
            let regex = try? NSRegularExpression(pattern: pattern, options: .caseInsensitive)
            let range = NSRange(location: 0, length: count)
            
            guard let result = regex?.firstMatch(in: self, options: [], range: range) else {
                return nil
            }
            
            return (self as NSString).substring(with: result.range)
        }
   
    public func mimeType() -> String {
        return (self as NSString).mimeType()
    }
   
        /**
         Generates a unique string that can be used as a filename for storing data objects that need to ensure they have a unique filename. It is guranteed to be unique.
         
         - parameter prefix: The prefix of the filename that will be added to every generated string.
         - returns: A string that will consists of a prefix (if available) and a generated unique string.
         */
        static func uniqueFilename(withPrefix prefix: String? = nil) -> String {
            let uniqueString = ProcessInfo.processInfo.globallyUniqueString
            
            if prefix != nil {
                var timeStamp = String(Date().ticks)
                //print("Unique name is-->","\(prefix!)-\(uniqueString)-\(timeStamp)")
                return "\(prefix!)-\(timeStamp)"
                //return "\(prefix!)-\(uniqueString)"

            }
            
            return uniqueString
        }
    
    
    func getHtmlAttributedStr() -> NSAttributedString?{
        let attributedStr =   try? NSAttributedString(htmlString: self)
        let mutableAttributedStr = NSMutableAttributedString.init(attributedString: attributedStr ?? NSAttributedString.init(string: ""))
        
        let softwareAttributedStr = mutableAttributedStr.trimmedAttributedString(set: CharacterSet.whitespacesAndNewlines)
        let currentFont = UIFont().Arial_Regular(n:14)
        softwareAttributedStr.setFontFace(font: currentFont, color: .black)
        return softwareAttributedStr
    }
        
    
    
}

extension NSAttributedString {
    
    convenience init(htmlString html: String) throws {
        try self.init(data: Data(html.utf8), options: [
            .documentType: NSAttributedString.DocumentType.html,
            .characterEncoding: String.Encoding.utf8.rawValue
            ], documentAttributes: nil)
    }
    
    
    
    
}


extension NSMutableAttributedString {
    
    func trimmedAttributedString(set: CharacterSet) -> NSMutableAttributedString {
        
        let invertedSet = set.inverted
        
        var range = (string as NSString).rangeOfCharacter(from: invertedSet)
        let loc = range.length > 0 ? range.location : 0
        
        range = (string as NSString).rangeOfCharacter(
            from: invertedSet, options: .backwards)
        let len = (range.length > 0 ? NSMaxRange(range) : string.characters.count) - loc
        
        let r = self.attributedSubstring(from: NSMakeRange(loc, len))
        return NSMutableAttributedString(attributedString: r)
    }
    
    
    func htmlAttributed(family: String?, size: CGFloat, color: UIColor) -> NSAttributedString? {
        do {
            let htmlCSSString = "<style>" +
                "html *" +
                "{" +
                "font-size: \(size)pt !important;" +
                "color: #\(color) !important;" +
                "font-family: \(family ?? "Arial"), Arial !important;" +
            "}</style> \(self)"
            
            guard let data = htmlCSSString.data(using: String.Encoding.utf8) else {
                return nil
            }
            
            return try NSAttributedString(data: data,
                                          options: [.documentType: NSAttributedString.DocumentType.html,
                                                    .characterEncoding: String.Encoding.utf8.rawValue],
                                          documentAttributes: nil)
        } catch {
            print("error: ", error)
            return nil
        }
    }
    
    func setFontFace(font: UIFont, color: UIColor? = nil) {
        beginEditing()
        self.enumerateAttribute(.font, in: NSRange(location: 0, length: self.length)) { (value, range, stop) in
            if let f = value as? UIFont, let newFontDescriptor = f.fontDescriptor.withFamily(font.familyName).withSymbolicTraits(f.fontDescriptor.symbolicTraits) {
                let newFont = UIFont(descriptor: newFontDescriptor, size: font.pointSize)
                removeAttribute(.font, range: range)
                addAttribute(.font, value: newFont, range: range)
                if let color = color {
                    removeAttribute(.foregroundColor, range: range)
                    addAttribute(.foregroundColor, value: color, range: range)
                }
            }
        }
        endEditing()
    }
    
    
    public convenience init?(HTMLString html: String, font: UIFont? = nil) throws {
        let options : [NSAttributedString.DocumentReadingOptionKey : Any] =
            [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html,
             NSAttributedString.DocumentReadingOptionKey.characterEncoding: String.Encoding.utf8.rawValue]
        
        guard let data = html.data(using: .utf8, allowLossyConversion: true) else {
            throw NSError(domain: "Parse Error", code: 0, userInfo: nil)
        }
        
        if let font = font {
            guard let attr = try? NSMutableAttributedString(data: data, options: options, documentAttributes: nil) else {
                throw NSError(domain: "Parse Error", code: 0, userInfo: nil)
            }
            var attrs = attr.attributes(at: 0, effectiveRange: nil)
            attrs[NSAttributedStringKey.font] = font
            attr.setAttributes(attrs, range: NSRange(location: 0, length: attr.length))
            self.init(attributedString: attr)
        } else {
            try? self.init(data: data, options: options, documentAttributes: nil)
        }
    }

    

}


extension UITapGestureRecognizer {
    
    func didTapAttributedTextInLabel(label: UILabel, inRange targetRange: NSRange) -> Bool {
        // Create instances of NSLayoutManager, NSTextContainer and NSTextStorage
        let layoutManager = NSLayoutManager()
        let textContainer = NSTextContainer(size: CGSize.zero)
        let textStorage = NSTextStorage(attributedString: label.attributedText!)
        
        // Configure layoutManager and textStorage
        layoutManager.addTextContainer(textContainer)
        textStorage.addLayoutManager(layoutManager)
        
        // Configure textContainer
        textContainer.lineFragmentPadding = 0.0
        textContainer.lineBreakMode = label.lineBreakMode
        textContainer.maximumNumberOfLines = label.numberOfLines
        let labelSize = label.bounds.size
        textContainer.size = labelSize
        
        // Find the tapped character location and compare it to the specified range
        let locationOfTouchInLabel = self.location(in: label)
        let textBoundingBox = layoutManager.usedRect(for: textContainer)
        
        
        let textContainerOffset = CGPoint(x: (labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x, y: (labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y)
        
        let locationOfTouchInTextContainer = CGPoint(x: locationOfTouchInLabel.x - textContainerOffset.x, y: locationOfTouchInLabel.y - textContainerOffset.y)
        let indexOfCharacter = layoutManager.characterIndex(for: locationOfTouchInTextContainer, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
        
        return NSLocationInRange(indexOfCharacter, targetRange)
    }
    
}

extension UITableViewDataSource where Self:UITableView {
    /**
     * EXAMPLE: tableView.cells//list of cells in a tableview
     */
    var cells:[UITableViewCell] {
        return (0..<self.numberOfSections).indices.map { (sectionIndex:Int) -> [UITableViewCell] in
            return (0..<self.numberOfRows(inSection: sectionIndex)).indices.compactMap{ (rowIndex:Int) -> UITableViewCell? in
                return self.cellForRow(at: IndexPath(row: rowIndex, section: sectionIndex))
            }
            }.flatMap{$0}
    }
}


extension UITableView{
    var cells:[UITableViewCell] {
        return (0..<self.numberOfSections).indices.map { (sectionIndex:Int) -> [UITableViewCell] in
            return (0..<self.numberOfRows(inSection: sectionIndex)).indices.compactMap{ (rowIndex:Int) -> UITableViewCell? in
                return self.cellForRow(at: IndexPath(row: rowIndex, section: sectionIndex))
            }
            }.flatMap{$0}
    }
}
