//
//  Utils.swift
//  BFP
//
//  Created by Eightfolds on 5/2/18.
//  Copyright © 2018 Orfeostory. All rights reserved.
//

import Foundation
import ObjectiveC
import UIKit
import Material


private var utils: Utils? = nil

class Utils: NSObject {
    
    class func singleInstanceUtils() -> Utils {
        if utils == nil {
            utils = Utils()
        }
        return utils ?? Utils()
    }
    
    class func isLoggedInUserTypeIsCustomer() -> Bool {
        
        switch(Utils.getLoggedInUserType()){
            
        case .CUSTOMER_TYPE:
            return true
        case .SUPPLIER_TYPE:
            return false
        }
    }
    
    class func getAttributedStringWithUnits(from tempString: String?, andAmountString units: String? ,with attributes:[NSAttributedStringKey : Any]!) -> NSAttributedString? {
        let location: Int =  (tempString?.count ?? 0) - (units?.count ?? 0)
        let stringText = NSMutableAttributedString(string: tempString ?? "")
        stringText.addAttributes(attributes!, range: NSRange(location: location, length: units?.count ?? 0))
        return stringText
    }
    
   class func numberOfLinesInLabel(yourString: String, labelWidth: CGFloat, labelHeight: CGFloat, font: UIFont) -> Int {
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.minimumLineHeight = labelHeight
        paragraphStyle.maximumLineHeight = labelHeight
        paragraphStyle.lineBreakMode = .byWordWrapping
        
        let attributes: [NSAttributedStringKey: Any] = [NSAttributedStringKey.font: font, NSAttributedStringKey.paragraphStyle: paragraphStyle]
        
        // let constrain = CGSizeMake(labelWidth, CGFloat(Float.infinity))
        let constrain = CGSize.init(width: labelWidth, height: CGFloat(Float.greatestFiniteMagnitude))
        let size = yourString.size(withAttributes: attributes)
        let stringWidth = size.width
        
        let numberOfLines = ceil(Double(stringWidth/constrain.width))
        
        return Int(numberOfLines)
    }
    
    class func getUserCountry() -> String?{
        let countryCode = NSLocale.current.regionCode
        return countryCode
    }
    
    class func getClientToken() -> String?{
        let clientToken = "eyJ2ZXJzaW9uIjoyLCJhdXRob3JpemF0aW9uRmluZ2VycHJpbnQiOiIzNGQ1M2MxNDE5MzM5NTNlMWZiNjQ0ZTJlMDQ4YjMyOTEzYjQ0ZmNhYjA5ODBkYjM1NTEzYTg1YWI0N2VjZTNkfGNyZWF0ZWRfYXQ9MjAxOC0xMS0wNFQxMjozMzozNi45Mzk2MTExMTUrMDAwMFx1MDAyNm1lcmNoYW50X2lkPTM0OHBrOWNnZjNiZ3l3MmJcdTAwMjZwdWJsaWNfa2V5PTJuMjQ3ZHY4OWJxOXZtcHIiLCJjb25maWdVcmwiOiJodHRwczovL2FwaS5zYW5kYm94LmJyYWludHJlZWdhdGV3YXkuY29tOjQ0My9tZXJjaGFudHMvMzQ4cGs5Y2dmM2JneXcyYi9jbGllbnRfYXBpL3YxL2NvbmZpZ3VyYXRpb24iLCJncmFwaFFMIjp7InVybCI6Imh0dHBzOi8vcGF5bWVudHMuc2FuZGJveC5icmFpbnRyZWUtYXBpLmNvbS9ncmFwaHFsIiwiZGF0ZSI6IjIwMTgtMDUtMDgifSwiY2hhbGxlbmdlcyI6W10sImVudmlyb25tZW50Ijoic2FuZGJveCIsImNsaWVudEFwaVVybCI6Imh0dHBzOi8vYXBpLnNhbmRib3guYnJhaW50cmVlZ2F0ZXdheS5jb206NDQzL21lcmNoYW50cy8zNDhwazljZ2YzYmd5dzJiL2NsaWVudF9hcGkiLCJhc3NldHNVcmwiOiJodHRwczovL2Fzc2V0cy5icmFpbnRyZWVnYXRld2F5LmNvbSIsImF1dGhVcmwiOiJodHRwczovL2F1dGgudmVubW8uc2FuZGJveC5icmFpbnRyZWVnYXRld2F5LmNvbSIsImFuYWx5dGljcyI6eyJ1cmwiOiJodHRwczovL29yaWdpbi1hbmFseXRpY3Mtc2FuZC5zYW5kYm94LmJyYWludHJlZS1hcGkuY29tLzM0OHBrOWNnZjNiZ3l3MmIifSwidGhyZWVEU2VjdXJlRW5hYmxlZCI6dHJ1ZSwicGF5cGFsRW5hYmxlZCI6dHJ1ZSwicGF5cGFsIjp7ImRpc3BsYXlOYW1lIjoiQWNtZSBXaWRnZXRzLCBMdGQuIChTYW5kYm94KSIsImNsaWVudElkIjpudWxsLCJwcml2YWN5VXJsIjoiaHR0cDovL2V4YW1wbGUuY29tL3BwIiwidXNlckFncmVlbWVudFVybCI6Imh0dHA6Ly9leGFtcGxlLmNvbS90b3MiLCJiYXNlVXJsIjoiaHR0cHM6Ly9hc3NldHMuYnJhaW50cmVlZ2F0ZXdheS5jb20iLCJhc3NldHNVcmwiOiJodHRwczovL2NoZWNrb3V0LnBheXBhbC5jb20iLCJkaXJlY3RCYXNlVXJsIjpudWxsLCJhbGxvd0h0dHAiOnRydWUsImVudmlyb25tZW50Tm9OZXR3b3JrIjp0cnVlLCJlbnZpcm9ubWVudCI6Im9mZmxpbmUiLCJ1bnZldHRlZE1lcmNoYW50IjpmYWxzZSwiYnJhaW50cmVlQ2xpZW50SWQiOiJtYXN0ZXJjbGllbnQzIiwiYmlsbGluZ0FncmVlbWVudHNFbmFibGVkIjp0cnVlLCJtZXJjaGFudEFjY291bnRJZCI6ImFjbWV3aWRnZXRzbHRkc2FuZGJveCIsImN1cnJlbmN5SXNvQ29kZSI6IlVTRCJ9LCJtZXJjaGFudElkIjoiMzQ4cGs5Y2dmM2JneXcyYiIsInZlbm1vIjoib2ZmIn0="
        return clientToken
    }
    
    class func removeNSUserDefaultsPersistanceStorage()  {
        
        let udid = Defaults .getUDID()
        let pushRegId = Defaults .getPushRegistrationId()
        let baseURL = Defaults .getBaseUrl()
        UserDefaults.standard.removePersistentDomain(forName: (Bundle.main.bundleIdentifier)!)
        Defaults .setUDID(udid)
        Defaults .setPushRegistrationId(pushRegId)
        Defaults .setBaseUrl(baseURL)
    }
    
    struct Platform {
        static let isSimulator: Bool = {
            var isSim = false
            #if (arch(i386) || arch(x86_64)) && os(iOS)
                isSim = true
            #endif
            return isSim
        }()
    }
    
    class func registerDeviceForRemoteNotifications()  {
        
        if !Platform.isSimulator {
            ServiceManager.methodType(requestType: POST_REQUEST, url: UPDATE_DEVICE_URL, params: nil, paramsData: nil, completion: { (_ response,_ responseData, _ statusCode) in
                if statusCode == 200 {
                    print("Device Registration Success")
                } else {
                    print("Device registration failure")
                }
            }) { (_ response, _ statusCode) in
                print("Device registration failure")
            }
        }
    }
    
    class func validateEmail(mail:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: mail)
    }
    
    class func validatePhone(phone: String) -> Bool {
        let PHONE_REGEX = "[789][0-9]{9}"  //"^((\\+)|(00))[0-9]{6,14}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        return phoneTest.evaluate(with: phone)
    }
    
    class func validatePostalCode(pinCode: String) -> Bool {
        let PINCODE_REGEX = "^[0-9]{6}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PINCODE_REGEX)
        return phoneTest.evaluate(with: pinCode)
    }
    
    class func validateSingaporeMobile(mobile: String) -> Bool {
        let mobileNumberPattern = "[0-9]{8}"
        let mobileNumberPred = NSPredicate(format: "SELF MATCHES %@", mobileNumberPattern)
        return mobileNumberPred.evaluate(with: mobile)
    }
    
    class func validateUserName(userName: String) -> Bool {
        
        let `set` = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789 ").inverted
        if (userName as NSString).rangeOfCharacter(from: `set`).location == NSNotFound && (userName as NSString).rangeOfCharacter(from: CharacterSet.letters).location != NSNotFound {
            return true
        } else {
            return false
        }
    }
    
    class func validateUserNameAsEmail(userName: String) -> Bool {
        
        let `set` = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789@#._ ").inverted
        if (userName as NSString).rangeOfCharacter(from: `set`).location == NSNotFound && (userName as NSString).rangeOfCharacter(from: CharacterSet.letters).location != NSNotFound {
            return true
        } else {
            return false
        }
    }
    
    
    
    class func validateName(name: String) -> Bool {
        
        let `set` = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ ").inverted
        if (name as NSString).rangeOfCharacter(from: `set`).location == NSNotFound && (name as NSString).rangeOfCharacter(from: CharacterSet.letters).location != NSNotFound {
            return true
        } else {
            return false
        }
    }
    
    class func isPasswordSame(password: String , confirmPassword : String) -> Bool {
        if password == confirmPassword{
            return true
        }else{
            return false
        }
    }
    
    class func isPwdLenth(password: String) -> Bool {
        if password.count >= 5 { //password.characters.count,confirmPassword.characters.count
            return true
        }else{
            return false
        }
    }
    
    class func isEmptyValue(_ object: Any) -> Bool {
        if (object is NSNull) || (object as AnyObject).isEqual("") || object as! _OptionalNilComparisonType == NSNull() {
            return true
        }
        return false
    }
    
    class func getLocalTimeStringfromUTC(_ dateStr: String, newDateFormat: String) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss.0"
        if dateStr.count == 10 {
            dateFormatter.dateFormat = "yyyy-MM-dd"
        }
        dateFormatter.timeZone = NSTimeZone(name: "UTC")! as TimeZone
        let date = dateFormatter.date(from: dateStr)
        
        dateFormatter.dateFormat = newDateFormat
        dateFormatter.timeZone = NSTimeZone.local
        let timeStamp = dateFormatter.string(from: date!)
        return timeStamp
    }
    
    class func getDateComponentsArray(from dateString: String, with newDateFormat: String) -> [String]
    {
        let localDateString = getLocalTimeStringfromUTC(dateString, newDateFormat: newDateFormat)
        let dateComponentsArray = localDateString.components(separatedBy: " ")
        return dateComponentsArray
    }
    
    
    class func convertDateToString(dateStr : Date) -> String?{
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd"
        let dateInString = df.string(from:dateStr)
        return dateInString
        
  }
    
    class func generalDateWithString(_ dateStr: String, newDateFormat: String) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let date = dateFormatter.date(from: dateStr)
        dateFormatter.dateFormat = newDateFormat
        if date != nil{
            let timeStamp = dateFormatter.string(from: date!)
            return timeStamp
        }
        return ""
        
        
    }
    
    class func getRemainingTimer(_ remainingTime: String, needFeatureTime: Bool) -> Int {
        
        let startTime = Date()
        let endTime = Utils.getLocalTimeStringfromUTC(remainingTime, newDateFormat: "yyyy-MM-dd HH:mm:ss")
        let timeFormatter = DateFormatter()
        timeFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        timeFormatter.timeZone = NSTimeZone.system
        let fromTime: Date? = timeFormatter.date(from: timeFormatter.string(from: startTime))
        let toTime: Date? = timeFormatter.date(from: endTime)
        var timeinterval = TimeInterval()
        if needFeatureTime { timeinterval = (toTime?.timeIntervalSince(fromTime!))! }
        else { timeinterval = (fromTime?.timeIntervalSince(toTime!))! }
        return Int(timeinterval)
    }
    
    class func isAppAlreadyLaunched() -> Bool {
        return UserDefaults.standard.bool(forKey: "appLaunched")
    }
    
    class func setIsAppAlreadyLaunched(_ firstTime: Bool) {
        UserDefaults.standard.set(firstTime, forKey: "appLaunched")
        UserDefaults.standard.synchronize()
    }

    //MARK:- StoreLoginUserType
    class func getLoggedInUserType() -> LOGGED_IN_USER_TYPE {
        return LOGGED_IN_USER_TYPE(rawValue: UserDefaults.standard.value(forKey: "loggedInUserType") as! String)!
    }
    
    class func setLoggedInUserType(_ userType: LOGGED_IN_USER_TYPE) {
        UserDefaults.standard.set(userType.rawValue, forKey: "loggedInUserType")
        UserDefaults.standard.synchronize()
    }
    
    //MARK:- For showing toast
    class func writeToast(_ message: String?) {
        KEY_WINDOW?.makeToast(message)
    }
    
    
    
    class func addShadowByUsingBezierPathToView(view:UIView){
        let shadowSize : CGFloat = 5.0
        
        let shadowPath = UIBezierPath(rect: CGRect(x: -shadowSize / 2,y: -shadowSize / 2,width: view.frame.size.width + shadowSize,height: view.frame.size.height + shadowSize))
        
        view.layer.masksToBounds = false
        
        view.layer.shadowColor = UIColor.lightGray.cgColor
        
        view.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        
        view.layer.shadowOpacity = 0.4
        
        view.layer.shadowPath = shadowPath.cgPath
    }
    
    //MARK:- For  AddShadowToView
    
    class func addShadow(to view: UIView?) {
        view?.layer.shadowColor = BLACK_COLOR.cgColor
        view?.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        //    view.layer.shadowRadius = 3.0f;
        //    view.layer.shadowOpacity = 2.0f;
        view?.layer.shadowRadius = 2.0
        view?.layer.shadowOpacity = 0.7
        view?.layer.masksToBounds = false
    }
    
    
    //MARK:- get all UITableViewCell
    
    class func cellsForTableView(sender:UIViewController?=nil,_ tableViewx : UITableView) -> [Any]? {
        let tableView: UITableView? = tableViewx
        let sections: Int? = tableView?.numberOfSections
        var cells: [AnyHashable] = []
        for section in 0..<(sections ?? 0) {
            let rows: Int? = tableView?.numberOfRows(inSection: section)
            for row in 0..<(rows ?? 0) {
                let indexPath = IndexPath(row: row, section: section)
                var cell: UITableViewCell?
                cell = tableViewx.cellForRow(at: indexPath)
//                if sender != nil{
//                    if sender is AddNewServiceViewController{
//            cell = (sender as! AddNewServiceViewController).tblNewServices(cellForRowAt: indexPath)
//                    }
//                 
//                }
                
                //**here, for those cells not in current screen, cell is nil**
                if let aCell = cell {
                    cells.append(aCell)
                }
            }
        }
        return cells
    }
    
    //MARK: For validating the Textfields
   class func validateFormAndShowToastWithRequiredFields(_ textFieldsArray:[TJTextField]) -> Bool {
        for textFieldDetail in textFieldsArray {
            if (textFieldDetail.text?.isEmpty)!{
                KEY_WINDOW?.makeToast("Please enter \(textFieldDetail.placeholder ??? textFieldDetail.accessibilityLabel ?? "")")
                return false
            }else
            {
                if textFieldDetail.accessibilityLabel != nil{
                    switch textFieldDetail.accessibilityLabel {
                        
                    case "Email","Email Address","Friend's E-Mail":
                        if !(self.validateEmail(mail: textFieldDetail.text!)){
                            KEY_WINDOW?.makeToast(ENTER_VALID_EMAIL_TOAST)
                            return false
                        }
                        
                    default:
                        break
                        
                    }
                }else{
                    switch textFieldDetail.placeholder {
                        
                    case "Email","Email Address":
                        if !(self.validateEmail(mail: textFieldDetail.text!)){
                            KEY_WINDOW?.makeToast(ENTER_VALID_EMAIL_TOAST)
                            return false
                        }
                        
                    default:
                        break
                        
                    }
                }
           
            }
        }
        return true
    }
    
    
    class func validateFormAndShowToastWithRequiredTextFields(_ textFieldsArray:[UITextField]) -> Bool {
        for textFieldDetail in textFieldsArray {
            if (textFieldDetail.text?.isEmpty)!{
                KEY_WINDOW?.makeToast("Please enter \(textFieldDetail.placeholder ?? "")")
                return false
            }
           }
        
        return true
    }
    
    
    class func getListOfYears(addCount : Int = 0) -> [String]{
        let currentDate = Date()
        let calendar = Calendar.current
        let currentYear = calendar.component(.year, from: currentDate)
        let years = (1970...currentYear + addCount).map { String($0) }
        return years
    }
    
    class func getAllMonths() -> [String]{
         let dateFormatter = DateFormatter()
         return dateFormatter.getAllMonths()
        
    }
    
    //MARK: For validating the Textfields
    class func validateTextAndShowToastWithRequiredFields(_ textFieldsArray:[TextField]) -> Bool {
        for textFieldDetail in textFieldsArray {
            if (textFieldDetail.text?.isEmpty)!{
                KEY_WINDOW?.makeToast("Please enter \(textFieldDetail.placeholder ?? "")")
                return false
            }else
            {
                switch textFieldDetail.placeholder {
                 
              case "Your E-Mail":
                    if !(self.validateEmail(mail: textFieldDetail.text!)){
                        KEY_WINDOW?.makeToast(ENTER_VALID_EMAIL_TOAST)
                        return false
                    }
                    
                    //            case "Subject"
                    
                case "Friend's E-Mail":
                    let friendsEmailList = textFieldDetail.text!.components(separatedBy: ",")
                    for email in friendsEmailList{
                        if !(self.validateEmail(mail: email)){
                            KEY_WINDOW?.makeToast(ENTER_VALID_EMAIL_TOAST)
                            return false
                        }
                    }
                case "Link" :
                    let linksList = textFieldDetail.text!.components(separatedBy: ",")
                    for link in linksList{
                        var linkStr = link
                    linkStr = linkStr.trimmingCharacters(in: .whitespacesAndNewlines)
                        if !(linkStr.isValidURL){
                            KEY_WINDOW?.makeToast(ENTER_VALID_LINK_TOAST)
                            return false
                        }
                    }
                case "Youtube Link" :
                    let linksList = textFieldDetail.text!.components(separatedBy: ",")
                    for link in linksList{
                        var youtubelinkStr = link
                        youtubelinkStr = youtubelinkStr.trimmingCharacters(in: .whitespacesAndNewlines)
                        if !(youtubelinkStr.isValidYouTubeUrl){
                            KEY_WINDOW?.makeToast(ENTER_VALID_YOUTUBELINK_TOAST)
                            return false
                        }
                    }
                    
               
                default:
                    break
                    
                }
            }
        }
        return true
    }
    
   class func getDateFromString(dateStr:String?,timeZone:String?) -> Date?{
        let dateFormat = "yyyy-MM-dd HH:mm:ss"
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat
        dateFormatter.timeZone = TimeZone.init(identifier: timeZone ?? "UTC")
        let startDate = dateFormatter.date(from: dateStr ?? "")
        return startDate
    }
    
    class func getRealmDBEncryptionKey() -> String{
        let uid = Defaults.getDBEncryptionKey()
        
        if uid == nil{
            let uuid = self.getUUID()
            
            Defaults.setdbEncryptionKey(ebEncryptionKey: uuid)
            return uuid
            
        }else{
            return uid!
        }
    }
    
    static func getUUID() -> String{
        return NSUUID().uuidString
    }
    
    class func redirectToLoginScreen(){
        DispatchQueue.main.async{
            let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let controller = storyboard.instantiateViewController(withIdentifier: "NavigationController")
            controller.modalTransitionStyle = .crossDissolve
           // Defaults.clearAccessToken()
            let appDelegate = AppDelegate.sharedDelegate()
            appDelegate.window?.rootViewController = controller
            appDelegate.window?.makeKeyAndVisible()
        }
       
    }
    
    class func showErrorFilesAlert(errorFiles : String?,corruptFiles : String?){
        var errorMessage = ""
        if let corruptFile = corruptFiles,corruptFile.count > 0{
            errorMessage = errorMessage + "Infected Files: " + corruptFile
        }
        if let errorFile = errorFiles,errorFile.count > 0{
            errorMessage = errorMessage + "\n Corrupt Files: " + errorFile
        }
        let alert = UIAlertController(title: "Need Attention!", message: errorMessage, preferredStyle: .alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            let appDelegate = AppDelegate.sharedDelegate()
//            (appDelegate.window?.rootViewController as? UINavigationController)?.popViewController(animated: true)
            
            guard let currentViewController = appDelegate.window?.rootViewController else{
                return
            }
            if currentViewController.childViewControllers != nil && currentViewController.childViewControllers.count > 0 {
                for controller in currentViewController.childViewControllers{
                    if  controller is UINavigationController && (controller as? UINavigationController)?.childViewControllers.count > 1{
                            (controller as! UINavigationController).popViewController(animated: true)
                    }
                }
            }
            
            
            
        }))

        let appDelegate = AppDelegate.sharedDelegate()
        // show the alert
        appDelegate.window?.rootViewController?.present(alert, animated: true, completion: nil)
    }
    
    
    
}
