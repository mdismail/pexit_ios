//
//  AlertMessages.swift
//  PEXit
//
//  Created by Apple on 24/08/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation


let CONFIRMATION_ALERT = "Are you sure?"
let DELETE_ATTATCHMENT = "Once deleted you can't undo the action."
let LEAVE_GROUP_ALERT = "Leave the group"
let ADMIN_LEAVE_GROUP = "Leave & Delete the group"
let LOGOUT_ALERT = "You want to logout"
let REPORT_ABUSE_ALERT = "You want to report this post abuse"
let BLOCK_USER_ALERT = "You want to block the user"
let UNBLOCK_USER_ALERT = "You want to unblock the user"

let NO_PRODUCT_FOUND = "No product matches.Please refine your search."
let NO_SUPPLIER_FOUND = "No supplier matches.Please refine your search"
