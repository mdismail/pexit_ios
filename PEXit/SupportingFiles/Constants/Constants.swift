


import Foundation
import UIKit
import MIBadgeButton

let DEFAULT_PAGE_SIZE = 50


//MARK:- For DATA_CONTENT_TYPE for ConditionsViewController

enum DATA_CONTENT_TYPE {
    case HOW_TO_USE_TYPE,FAQ_TYPE ,PRIVACY_POLICY_TYPE, TERMS_OF_USE_TYPE, VERSION_TYPE
}

 var appNotificationBarButton: MIBadgeButton!      // Make it global
var globalRefreshButton : UIBarButtonItem!
//MARK: - APP LANGUAGE

var CURRENT_APP_LANGUAGE = "en"
//var PREVIOUS_APP_LANGUAGE = "en"
var SELECTED_LANGUAGE = "English"

let BRAINTREE_IDENTIFIER = "com.recrosoft.pexit.payments"
let CCAVENUE_ACCESS_CODE = "AVBZ71EF36BA67ZBAB"
let CCAVENUE_MERCHANT_ID = "137823"
let RSA_KEY_URL = "https://pexit.net/ccavenue-mobile/GetRSA.php"
let REDIRECT_URL = "https://pexit.net/ccavenue-mobile/ccavResponseHandler.php"
//let LANG_UPDATED_NOTIFICATION = "LanguageUpdated"

var userProfileSummary = ""

var LOADED_ADS = ""
var LOADED_SPONSOR = ""


//MARK:- For LoggedInUserType
enum LOGGED_IN_USER_TYPE: String {
    case CUSTOMER_TYPE = "customer"
    case SUPPLIER_TYPE = "supplier"
}

enum TEMPLATE_TYPES: Int {
    case POST_TEMPLATE = 0
    case ADVERTISMENT_TEMPLATE = 2
}

enum GLOBAL_SEARCH_TYPE : String{
//    ["Posts","Connections","Jobs","Products","Services","Softwares","Product Suppliers"]
    case POSTS = "Posts"
    case CONNECTIONS = "Connections"
    case JOBS = "Jobs"
    case PRODUCTS = "Products"
    case SERVICES = "Services"
    case SOFTWARES = "Softwares"
    case PRODUCTSUPPLIERS = "Product Suppliers"
    
}


//MARK:- PICKER TITLES

enum PICKER_TITLES : String{
    case COUNTRY_PICKER = "Country"
    case STATE_PICKER = "State"
    case CITY_PICKER = "City"
}


enum PROFILE_TYPE : String{
    
    case PROFESSIONAL = "Professional"
    case STUDENT = "Student"
    case ORGANIZATION = "Organization"
    case OTHERS = "Others"
    case NONE = ""
}

//MARK:- For PROJECT_STATUS_TYPES

enum PROJECT_STATUS_TYPE: Int {
    case NOTHING = 0
    case NEW = 1
    case URGENT = 2
}


//MARK:- For BIDED_PROJECT_STATUS_TYPE

enum BIDED_PROJECT_STATUS_TYPE: Int {
    case PENDING = 1  //Creation State
    case AWARDED = 2
    case CANCELLED = 3
}

//MARK:- For PROJECT_STATUS_TYPES

enum BUTTON_TAGS: Int {
    case NO_TAG = 0
    case SIGN_IN_TAG = 1
    case FB_TAG = 2
    case LINKED_IN_TAG = 3
    case GOOGLE_TAG = 4
    case REGISTER_NOW_TAG = 5
    case FORGOT_USERNAME_TAG = 6
    case FORGOT_PASSWORD_TAG = 7
    case PROFILE_VIEWS = 8
    case CONNECTIONS = 9
    case PENDING_INVITES = 10
    case SORTBY = 11
    case PRODUCTCONDITION = 12
    case IMAGEATTATCHMENT = 13
    case FILEATTATCHMENT = 14
    case VIDEOATTATCHMENT = 15
    case PPTATTATCHMENT = 16
    case LINKATTATCHMENT = 17
    case YOUTUBELINKATTATCHMENT = 18
    case PRODUCTCATEGORY = 19
    
}


//MARK:- For MenuOptions

enum MenuOptions :Int{
    case PackageDetails = 0
    case HowItWorks = 1
    case NewsAndInfo = 2
    case ContactUs = 3
    case SwitchAccount = 4
    case Logout = 5
}


enum UIAlertActionTitle : String{
    case DELETE = "Delete"
    case CANCEL = "Cancel"
    case LEAVE = "LEAVE"
    case ADMINLEAVE = "LEAVE & Delete"
    case LOGOUT = "Logout"
    case APPLY = "Apply"
    case REPORT = "Report"
    case BLOCK = "Block"
    case UNBLOCK = "Unblock"
}

//MARK:- For LoginType

struct USER_LOGIN_TYPE {
    static let NORMAL_LOGIN = 1
    static let FACEBOOK_LOGIN = 2
}

//MARK: Others
let KEY_WINDOW = UIApplication.shared.keyWindow
let APP_DELEGATE = AppDelegate.sharedDelegate()
//let DATA_MANAGER  = DataManager.shared()

let GET_REQUEST = "GET"
let POST_REQUEST = "POST"
let PUT_REQUEST = "PUT"
let DELETE_REQUEST = "DELETE"

let ADD_NEW_COMMENT = "add"
let REPLY_TO_COMMENT = "reply"

//let APP_STORE_LINK : String = "https://itunes.apple.com/in/app/id123456789"
//func APP_VERSION() -> String {
//    let dictionary = Bundle.main.infoDictionary!
//    let version = dictionary["CFBundleShortVersionString"] as! String
//    let build = dictionary["CFBundleVersion"] as! String
//    return "\(version) (\(build))"
//}

var Timestamp: String {
    return "\(NSDate().timeIntervalSince1970 * 1000)"
}
let LOADING_VIEW_TAG = 123456789
let LOADING_IMAGE_TAG = 987654321

let INVALID_CREDENTIALS = 4401
let INVALID_USER = 4002
let NOT_REGISTRED_USER = 5032
let SUCCESS_CODE = 2000
let NAVIGATIONBAR_LOADING_SLIDER_TAG = 5545
let BACKEND_ERROR = 9000

let INTERNET_CONNECTION = "InternetConnection"
let NoRecordFoundView_Tag = 185
let NO_DATA_LABEL_TAG = 88888
let PHONE_NUMBER_LENGTH = 10
let COMMON_CONTROLLER_ROW_HEIGHT = 44


let FROM_YEAR_CONSTANT = 0
let TO_YEAR_CONSTANT = 10
let DEGREE_OPTIONS = ["High School","Associate's Degree","Bachelor's Degree","Master's Degree","Master Of Business Administration(M.B.A)","Juris Doctor","Doctor Of Medicine","Doctor Of Philosophy","Engineer's Degree","Other"]
let MODE_OF_STUDY_OPTIONS = ["FullTime","Part Time","Distant Education","Evening Colleges","Other"]


enum WORK_PROFILE_TYPE : String{
    case Education = "education"
    case Experience = "experience"
    case Company = "company"
    case Honors = "honors"
}

struct AlertType {
    static let Default = 0
    static let Confirmation = 1
}

//LINkED in developer credentials
let LINKED_IN_CLIENT_ID = "81lz0afjj9kwzl"
let LINKED_IN_CLIENT_SECRET = "zGjn81K807KpXQaq"
let LINKED_IN_STATE = "DLKDJF46ikMMZADfdfds"

//SEGUE IDENTIFIERS
let HOME_TO_DETAILS = "homeToDetail"


// default key constants

struct Default_Constants{
    static let DBEncryptionKey = "DBEncryptionKEY";
    
}

let textViewPlaceholder = "Description"

let PUBLIC_GROUP_DISCLAIMER = "* Any PEXit user can find the group; see its members and their posts."
let PRIVATE_GROUP_DISCLAIMER = "* Only Group members can see its members and their posts"
let CLOSED_GROUP_DISCLAIMER = "* Any PEXit user can find the group and see its members. Only members can see posts."

enum GROUPTYPES : String{
    case PUBLIC = "public"
    case PRIVATE = "private"
    case CLOSED = "closed"
}

enum TYPE_OF_REQUEST_PRODUCT : String{
    case PRODUCT = "Product"
    case SUPPLIER = "Suppliers"
}

enum MESSAGE_ACTIONS : String{
    case ADD_PEOPLE = "Add People"
    case LEAVE_CONVERSATION = "Leave Conversation"
    case DELETE_CONVERSATION = "Delete Conversation"
    case FORWARD_CONVERSTAION = "Forward Conversation"
    case MARK_AS_UNREAD = "Mark As Unread"
}

enum PRODUCT_CONDITION : String{
    case Used = "Used Refurbished"
    case New = "New"
    case AllTypes = "All Types"
    case NotUsed = "Used-Not Refurbished"
    
}

enum ControllerType : String{
    case SSTSoftware
    case Services
    case OtherSoftware
    case Software
    case Product
    case Jobs
    case Sponsor
    case Ads
}


enum SERVICE_TYPE : String{
    case ADS = "ads"
    case SPONSOR = "sponsor"
    case SERVICE = "service"
    case SOFTWARE = "software"
    case PRODUCT = "product"
    case JOB = "job"
}

enum SERVICE_TYPE_API : String{
    case PRODUCTS = "pr"
    case SERVICES = "sv"
    case SPONSORS = "sc"
    case SOFTWARES = "sw"
    case JOBS = "jb"
    case AD = "ad"
}

enum LAYOUT_TYPES : String{
    case Layout_1 = "Layout_1"
    case Layout_2 = "Layout_2"
    case Layout_3 = "Layout_3"
}

enum SERVICE_PAYMENT_TYPE : String{
    case PAID = "paid"
    case FREE = "free"
}

let MESSAGE_ACTION_ARRAY = [MESSAGE_ACTIONS.ADD_PEOPLE.rawValue,MESSAGE_ACTIONS.MARK_AS_UNREAD.rawValue,MESSAGE_ACTIONS.FORWARD_CONVERSTAION.rawValue,MESSAGE_ACTIONS.LEAVE_CONVERSATION.rawValue,MESSAGE_ACTIONS.DELETE_CONVERSATION.rawValue]

let PRODUCT_CONDITION_ARRAY = [PRODUCT_CONDITION.AllTypes.rawValue,PRODUCT_CONDITION.New.rawValue,PRODUCT_CONDITION.Used.rawValue,PRODUCT_CONDITION.NotUsed.rawValue]

var CURRENT_GLOBAL_SEARCH_OPTION  = ""
var CURRENT_GLOBAL_SEARCH_KEYWORD  = ""


let FREE_DURATION_PRODUCTS = ""
let FREE_DURATION_SERVICES = ""
let FREE_DURATION_JOBS = ""
let FREE_DURATION_SPONSOREDCONTENT = ""
let FREE_DURATION_ADS = ""
let FREE_DURATION_SOFTWARE = ""

let REPORT_ABUSE = "Report Abuse"
