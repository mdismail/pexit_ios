//
//  UserDefaults.swift
//  eemagic
//
//  Created by vinod neeloju on 22/12/17.
//  Copyright © 2017 vinod neeloju. All rights reserved.
//

import Foundation
import UIKit
import Foundation
import ObjectiveC



//MARK:- Set
/**Saving LOGIN data in UserDefaults*/
func saveLoginData(_ response: Any?){
    UserDefaults.standard.set(response, forKey: "loginData")
    UserDefaults.standard.synchronize()
}

func getCountryCode() -> String?{
    if let countryCode = (Locale.current as NSLocale).object(forKey: .countryCode) as? String {
        print(countryCode)
        return countryCode
    }
    return nil
}



func setUserType(_ type : Int) {
    UserDefaults.standard.set(type, forKey: "userType")
    UserDefaults.standard.synchronize()
}

func getUserType() -> Int {
    let userType = UserDefaults.standard.value(forKey: "userType") as? Int
    return userType ?? 1
}

//MARK:- Get

/**Returns Userdata as Any*/
func getLoginData() -> [String : Any] {
    let loginData = UserDefaults.standard.object(forKey: "loginData") as? [String : Any]
    return loginData ?? [String : Any]()
}



func getServiceSelectionState() -> Bool {
    let dict =  getLoginData()
    let hier = ((dict["profileInfoResponse"] as! [String: Any])["profile"] as! [String: Any])["hier"] as! Bool
    let serviceProvider = ((dict["profileInfoResponse"] as! [String: Any])["profile"] as! [String: Any])["serviceProvider"] as! Bool
    if hier == true || serviceProvider == true {
        return true
    }
    return false
}

func getRegisteredAsService() -> Bool {
    let dict =  getLoginData()
    let serviceProvider = ((dict["profileInfoResponse"] as! [String: Any])["profile"] as! [String: Any])["serviceProvider"] as? Bool
    if serviceProvider != nil, serviceProvider == false {
        return serviceProvider!
    }
    return true
}

func getRegiterAsHeirer() -> Bool {
    let dict =  getLoginData()
    let hier = ((dict["profileInfoResponse"] as! [String: Any])["profile"] as! [String: Any])["hier"] as? Bool
    if hier != nil, hier == false {
        return false
    }
    return true
}


func getCompanySelectedState() -> Bool {
    let profileInfoResponse = getLoginData()["profileInfoResponse"] as? [String : Any]
    if profileInfoResponse != nil {
        let profile = profileInfoResponse!["profile"] as? [String : Any]
        if profile != nil {
            let companyName = profile!["businessName"] as? String
            if companyName != nil, companyName! != "" {
                return true
            }
        }
    }
    return false
}

func setToastStatus(flag: Bool) -> Void{
    UserDefaults.standard.set(flag, forKey: "toastStatus")
    UserDefaults.standard.synchronize()
}
func getToastStatus() -> Bool {
    return UserDefaults.standard.bool(forKey: "toastStatus")
}



private var defaults: Defaults? = nil
class Defaults: NSObject {
    
    class func saveFcmToken(token: String){
        UserDefaults.standard.set(token, forKey: "fcmtoken")
        UserDefaults.standard.synchronize()
    }
   class func getFcmToken() -> String {
        return UserDefaults.standard.string(forKey: "fcmtoken") ?? ""
    }
    
    class func setLoginStatus(_ flag: Bool){
        UserDefaults.standard.set(flag, forKey: "loginStatus")
        UserDefaults.standard.synchronize()
    }
    
    class func getLoginStatus() -> Bool {
        return UserDefaults.standard.bool(forKey: "loginStatus")
    }
    
    class func singleInstanaceDefaults() -> Defaults {
        if defaults == nil {
            defaults = Defaults()
        }
        return defaults ?? Defaults()
    }
    
    //==================================================================//
    class func getBaseUrl() -> String {
        if UserDefaults.standard.value(forKey: "baseURL") != nil {
            return UserDefaults.standard.value(forKey: "baseURL") as! String
        }
        return "" // eightfolds server
    }
    class func setBaseUrl(_ baseUrl: String) {
        //        UserDefaults.standard
        UserDefaults.standard.set(baseUrl, forKey: "baseURL")
        UserDefaults.standard.synchronize()
    }
    
    //==================================================================//
    class func setProfilePicId(_ profilePicId: Int) {
        UserDefaults.standard.set(profilePicId, forKey: "profilePicId")
        UserDefaults.standard.synchronize()
    }
    class func getProfilePicId() -> Int {
        return UserDefaults.standard.integer(forKey: "profilePicId")
    }
    
    //==================================================================//
    class func isUserLoggedIn() -> Bool {
        return UserDefaults.standard.bool(forKey: "loggedIn")
    }
    
    class func setUserLoggedIn(_ loggedIn: Bool) {
        UserDefaults.standard.set(loggedIn, forKey: "loggedIn")
        UserDefaults.standard.synchronize()
    }
    
    //==================================================================//
    class func isUserRegisteredIn() -> Bool {
        return UserDefaults.standard.bool(forKey: "registeredIn")
    }
    
    class func setUserRegisteredIn(_ registeredIn: Bool) {
        UserDefaults.standard.set(registeredIn, forKey: "registeredIn")
        UserDefaults.standard.synchronize()
    }
    
    //==================================================================//
    class func isFormSubmitted() -> Bool {
        return UserDefaults.standard.bool(forKey: "formSubmission")
    }
    
    class func setFormSubmitted(_ formSubmission: Bool) {
        UserDefaults.standard.set(formSubmission, forKey: "formSubmission")
        UserDefaults.standard.synchronize()
    }
    
    //==================================================================//
    class func isFacebookLogin() -> Bool {
        return UserDefaults.standard.bool(forKey: "fblogin")
    }
    class func setFacebookLogin(_ flag: Bool) {
        UserDefaults.standard.set(flag, forKey: "fblogin")
        UserDefaults.standard.synchronize()
    }
    
    //==================================================================//
    class func isStudent() -> Bool {
        return UserDefaults.standard.bool(forKey: "student")
    }
    class func setStudent(_ student: Bool) {
        UserDefaults.standard.set(student, forKey: "student")
        UserDefaults.standard.synchronize()
    }
    
    //==================================================================//
    class func setEmail(_ email: String) {
        UserDefaults.standard.setValue(email, forKey: "email")
        UserDefaults.standard.synchronize()
    }
    class func getEmail() -> String {
        return UserDefaults.standard.value(forKey: "email") as? String ?? ""
    }
    
    //==================================================================//
    class func setPassword(_ password: String) {
        UserDefaults.standard.setValue(password, forKey: "password")
        UserDefaults.standard.synchronize()
    }
    class func getPassword() -> String {
        return UserDefaults.standard.value(forKey: "password") as? String ?? ""
    }
    
    //==================================================================//
    class func setUserID(_ userID: Int) {
        UserDefaults.standard.set(userID, forKey: "userId")
        UserDefaults.standard.synchronize()
    }
    class func getUserID() -> Int {
        return UserDefaults.standard.integer(forKey: "userId")
    }
    
    //==================================================================//
    class func setCartBadge(_ badgeValue: Int) {
        UserDefaults.standard.set(badgeValue, forKey: "badgeValue")
        UserDefaults.standard.synchronize()
    }
    class func getBadgeValue() -> Int {
        return UserDefaults.standard.integer(forKey: "badgeValue")
    }
    
    //==================================================================//
    class func setUserName(_ userName: String) {
        UserDefaults.standard.setValue(userName, forKey: "username")
        UserDefaults.standard.synchronize()
    }
    class func getUserName() -> String {
        return UserDefaults.standard.value(forKey: "username") as? String ?? ""
    }
    
    //==================================================================//
    class func setUserFullName(_ userFullName: String) {
        UserDefaults.standard.setValue(userFullName, forKey: "userFullName")
        UserDefaults.standard.synchronize()
    }
    class func getUserFullName() -> String {
        return UserDefaults.standard.value(forKey: "userFullName") as? String ?? ""
    }
    
    //==================================================================//
    class func setUserMobile(_ userMobile: String) {
        UserDefaults.standard.setValue(userMobile, forKey: "userMobile")
        UserDefaults.standard.synchronize()
    }
    class func getUserMobile() -> String {
        return UserDefaults.standard.value(forKey: "userMobile") as? String ?? ""
    }
    
    //==================================================================//
    class func setAccessToken(_ accessToken: String) {
        UserDefaults.standard.setValue(accessToken, forKey: "accessToken")
        UserDefaults.standard.synchronize()
    }
    class func getAccessToken() -> String {
        return UserDefaults.standard.value(forKey: "accessToken") as? String ?? ""
    }
    
    class func clearAccessToken(){
         UserDefaults.standard.removeObject(forKey: "accessToken")
    }
    
    //==================================================================//
    class func setPushRegistrationId(_ registrationId: String) {
        UserDefaults.standard.setValue(registrationId, forKey: "pushRegId")
        UserDefaults.standard.synchronize()
    }
    class func getPushRegistrationId() -> String {
        return UserDefaults.standard.value(forKey: "pushRegId") as? String ?? ""
    }
    
    //==================================================================//
    class func setUDID(_ UDID: String) {
        UserDefaults.standard.setValue(UDID, forKey: "UDID")
        UserDefaults.standard.synchronize()
    }
    class func getUDID() -> String {
        return UserDefaults.standard.value(forKey: "UDID") as? String ?? ""
    }
    
    //==================================================================//
    class func getUserProfileDict() -> [AnyHashable: Any] {
        return UserDefaults.standard.object(forKey: "profileDict") as? [AnyHashable: Any] ?? [AnyHashable: Any]()
    }
    class func setProfileDict(_ profileDict: [AnyHashable: Any]) {
        UserDefaults.standard.set(profileDict, forKey: "profileDict")
        UserDefaults.standard.synchronize()
    }
    //==================================================================//
    class func setSelectedLocation(_ location: String? ) {
        UserDefaults.standard.set(location, forKey: "location")
        UserDefaults.standard.synchronize()
    }
    class func getSelectedLocation() -> String? {
        return UserDefaults.standard.object(forKey: "location") as? String
    }
    
    class func getDBEncryptionKey() -> String? {
        return UserDefaults.standard.object(forKey: Default_Constants.DBEncryptionKey) as? String
    }
    
    class func setdbEncryptionKey(ebEncryptionKey:String){
        UserDefaults.standard.setValue(ebEncryptionKey, forKey: Default_Constants.DBEncryptionKey)
        UserDefaults.standard.synchronize()    }
}





