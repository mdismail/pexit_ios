//
//  ToastMessages.swift
//  ValetConnect
//
//  Created by Narendar N on 14/03/18.
//  Copyright © 2018 Narendar N. All rights reserved.
//

import Foundation
import UIKit

//TODO:Login & ForgotPassword Validations
let EMAIL_TOAST = "Please Enter Email"
let EMAIL_VALID_TOAST = "Please Enter Valid Email"
let PASSWORD_TOAST = "Please Enter Password"

//TODO: Registraion Validations
let COMPANY_NAME_TOAST = "Please Enter Company Name"
let ADD_COMPANY_PROFILE = "Please add company profile"
let ROC_NO_TOAST = "Please Enter Roc No"

let PRIVATE_OWNER_NAME_TOAST = "Please Enter Private Owner Name"
let NRIC_NO_TOAST = "Please Enter NRIC NO"

let CONTACT_PERSON_TOAST = "Please Enter Contact Person Name"
let DESIGNATION_TOAST = "Please Enter Designation"
let CONTACT_NO_TOAST = "Please Enter Contact No"
let MOBILE_NO_TOAST = "Please Enter Mobile No"
let PRIMARY_EMAIL_TOAST = "Please Enter Primary Email"

let CONFIRM_PASSWORD_TOAST = "Please Confirm Password"
let SECONDARY_EMAIL_TOAST = "Please Enter Secondary Email"
let ADDRESS_TOAST = "Please Enter Address"
let POSTAL_CODE_TOAST = "Please Enter Postal Code"
let COUNTRY_TOAST = "Please Select Country"
let CITY_TOAST = "Please Select City"

let TYPE_OF_CUSTOMER_TOAST = "Please Select Type Of Customer"
let CHOOSE_LAYOUT = "Please choose a layout"

let WEBSITE_TOAST = "Please Enter Website"
let TYPE_OF_SUPPLIER_TOAST = "Please Select Type Of Supplier"
let CATEGORY_OF_SUPPLIER_TOAST = "Please Select Category Of Supplier"
let BUILDER_LICENSE_CATEGORY_TOAST = "Please Select Builder License Category"

let WRONG_LOGIN_ID_PASSWORD_TOAST : String = "Invalid email and/or password"

//MARK:- Common Toast Messages
let UNKNOWN_ERROR_MSG : String = "Oops! Something went wrong, Please try again."
let NO_INTERNET_CONNECTION : String = "Please check your Internet connection"
let ENTER_VALID_MOBILE_TOAST : String = "Please enter valid phone number"
let ENTER_VALID_USERNAME : String = "Please enter valid username"
let ENTER_VALID_EMAIL_TOAST : String = "Please enter valid email"
let FORGOT_PWD_INVALID_EMAIL : String = "The e-mail is not registered."
let ENTER_COUNTRY_CODE_TOAST : String = "Please enter your country code."
let ENTER_PASSWORD_TOAST : String = "Please enter password"
let ENTER_RE_PASSWORD_TOAST : String = "Please enter re-enter password"
let PASSWORD_LENGTH_TOAST : String = "Password length minimum of 5 characters"
let PASSWORD_NOT_MATCH_TOAST : String = "Passwords do not match"
let ENTER_NAME_TOAST : String = "Please enter your display name"
let ENTER_VALID_LINK_TOAST : String = "Please enter valid link"
let ENTER_VALID_YOUTUBELINK_TOAST : String = "Please enter valid youtube link"
let CURRENT_PWD_TOAST = "Enter your old password."
let CURRENT_PWD_WRONG_TOAST = "Entered old password is incorrect."
let NEW_PWD_TOAST = "Enter your new password."
let CONFIRM_PWD_TOAST = "Confirm your new password."
let PASSWORD_CAHNGE_TOAST = "Password changed successfully."
let PROFILE_UPDATE_SUCCESS_TOAST = "Profile updated successfully."
let DRIVER_INFO_SUCCESS_TOAST = "Updated driver info successfully."
let CANT_UPDATE_PASSWORD = "You cannot change the password."
let ENTER_SEARCH_KEYWORDS = "Please enter search keywords"
let CHOOSE_OPTION = "Please choose an option"

let ENTER_MESSAGE = "Please enter a message"
let SELECT_USER = "Please choose a user"

let CHOOSE_COUNTRY_TOAST : String = "Please select country"
let CHOOSE_STATE_TOAST : String = "Please select state"

let SELECT_START_LOCATION_TOAST : String = "Please select start location"
let ENTER_DESTINATIONS_TOAST : String = "Enter at least 1 destination"
let SELECT_CAR_NO_TOAST : String = "Please enter car plate number"
let SELECT_CAR_TRANISMISSION_TYPE_TOAST : String = "Please select car transmission type"
let SELECT_PAYMENT_METHOD_TOAST : String = "Please select payment method"
let VALET_BOOKING_SUCCESS : String = "Valet has been booked."

let ADD_RATE_TOAST : String = "Please add your rating."
let REVIEW_ADDED_SUCCESS_TOAST = "Review has been submitted."


let SELECT_COUNTRY_TOAST : String = "Please select country"
let SELECT_TERMS_POLICY_TOAST : String = "Please select Terms & Conditions"



let VERIFY_OTP_TOAST = "Please verify OTP"

let ACCOUNT_SUSPENDED_TOAST = "Your account is suspended, please contact admin."
let ACCOUNT_TERMINATED_TOAST = "Your account is terminated, please contact admin."

let REGISTRATION_SUCCESS_TOAST : String = "Registration success."
let FORGOT_PWD_SUCCESS_TOAST : String = "Please check the email you entered for a link to reset your password."

let ENTER_OTP_TOAST : String = "Please enter a valid 6-digit verification code"
let ENTER_OTP_ERROR_TOAST : String =  "Enter the 6-digit verification code sent via SMS"
let SEND_OTP_SUCCESS_TOAST : String = "Verification code sent via SMS"
let RESEND_OTP_SUCCESS_TOAST : String = "Verification code resent"
let VERIFICATION_SUCCES_TOAST : String = "Verification success"

let LOGIN_SUCCESS_TOAST : String = "Login success."


let VALID_BANK = "Please Enter Valid Bank Name"
let CORRECT_NAME = "Please Enter Correct Name"
let VALID_ACCOUNT = "Please Enter Valid Account No"


let CHOOSE_CHECKOUT_OPTION = "Please select the checkout option"
