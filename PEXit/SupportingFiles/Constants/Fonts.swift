//
//  Fonts.swift
//  BFP
//
//  Created by Eightfolds on 5/2/18.
//  Copyright © 2018 Orfeostory. All rights reserved.
//

import Foundation
import UIKit

func SFUIDisplay_Bold(n: CGFloat) -> UIFont {
    return UIFont(name: "SFUIDisplay-Bold", size: n)!
}

func SFUIDisplay_Thin(n: CGFloat) -> UIFont {
    return UIFont(name: "SFUIDisplay-Thin", size: n)!
}

func SFUIDisplay_Medium(n: CGFloat) -> UIFont {
    return UIFont(name: "SFUIDisplay-Medium", size: n)!
}

func SFUIDisplay_Heavy(n: CGFloat) -> UIFont {
    return UIFont(name: "SFUIDisplay-Heavy", size: n)!
}

func SFUIDisplay_Ultralight(n: CGFloat) -> UIFont {
    return UIFont(name: "SFUIDisplay-Ultralight", size: n)!
}

func SFUIDisplay_Semibold(n: CGFloat) -> UIFont {
    return UIFont(name: "SFUIDisplay-Semibold", size: n)!
}

func SFUIDisplay_Light(n: CGFloat) -> UIFont {
    return UIFont(name: "SFUIDisplay-Light", size: n)!
}

extension UIFont{
    func Arial_Regular(n: CGFloat) -> UIFont {
        return UIFont(name: "Arial", size: n)!
    }
//    func MyriadPro_Semibold(n: CGFloat) -> UIFont {
//        return UIFont(name: "MyriadPro-Semibold", size: n)!
//    }
    
   
    

}

