


import Foundation


//MARK:- App APIS


let BASE_URL = "https://pexit.net/api/v1/"   // LIVE SERVER
//let BASE_URL = "http://52.33.29.151/api/v1/" //TEST SERVER
//let BASE_URL = "http://54.218.186.166/api/v1/" // NEW TEST SERVER

//TODO:Login & Register

func Get_LOGIN_URL(_ socialLoginType: String? = nil)-> String
{
    guard let socialType = socialLoginType else { return  "login" }
    return "login/\(socialType)"
}


let LOGIN_URL = "/api/secure/user/login"
let REGISTER_URL = "/api/user/registration"
func LOGOUT_URL(_ imei: String) -> String {
    return "/api/secure/user/logout?imei=\(imei)"
}
let UPDATE_FCM = "fcm/token"

//TODO:Data For Register Page
let GET_COUNTRIES = "countries"
let GET_AD_STATES = "ad/states"
let GET_COUNTRY_CODES = "countrycodes"
func GET_STATES(countryName : String) -> String{
    return "states/\(countryName)"
}
func GET_CITIES(stateName : String) -> String{
    return "cities/\(stateName)"
}
let REGISTER_USER = "register"
let HOME_SEARCH = "search"
let SEND_POST = "post"

//Dashboard Api
let INVITE_FRIENDS = "invitefriends"
let LIKE_POST = "post/like"
//let FETCH_COMMENTS = "post/comments"
let POST_COMMENT = "post/comments"
func GET_FETCH_COMMENTS_URL(_ postId : Int) -> String{
    return "post/comments/\(postId)"
}

func GET_LIKE_USERS_URL(_ postId : Int) -> String{
    return "post/like/\(postId)"
}

func DELETE_POST(_ postId : Int) -> String{
    return "post/\(postId)"
}

func DELETE_ATTATCHMENT(_ postId : Int,fileType : String, fileName : String) -> String{
    return "post/file/\(postId)/\(fileType)/\(fileName)"
}

func DELETE_ATTATCHMENT_FILE(_ fileId : Int,fileFolder : String, fileType : String, fileName : String) -> String{
    return "profile/file/\(fileId)/\(fileFolder)/\(fileType)/\(fileName)"
}

func DELETE_PROFILE_ATTATCHMENT(fileType : String, fileName : String) -> String{
    return "profile/file/\(fileType)/\(fileName)"
}

func DELETE_ORGANIZATION(type : String, idOfOrg : Int) -> String{
    return "profile/delete/\(type)/\(idOfOrg)"
}


func UPDATE_POST(_ postId : Int) -> String{
    return "post/update/\(postId)"
}

func SHARE_POST(_ postId : Int) -> String{
    return "post/share/\(postId)"
}

// PROFILE APIS

func WORKPROFILE_INSERT(_ type : String) -> String{
    return "profile/insert/\(type)"
}

func WORKPROFILE_UPDATE(_ type : String , idOfProfile : Int) -> String{
    return "profile/update/\(type)/\(idOfProfile)"
}
let GET_PROFILE = "profile"
let PROFILE_UPDATE = "profile/update"

// Job Related Apis

func GET_JOB_DETAILS(_ jobId : Int) -> String{
    return "job/view/\(jobId)"
}

func APPLY_FOR_JOB(_ jobId : Int) -> String{
    return "job/apply/\(jobId)"
}
let GET_LOCATIONS = "job/location"

// My Newtork Apis

let GET_PENDING_CONNECTIONS = "pendings"
let GET_MY_CONNECTIONS = "connections"
let CREATE_GROUP = "group/create"
let CONNECT_TO_GROUP = "group/connect"
let LEAVE_GROUP = "group/leave"
let USERS_SUGGESTIONS = "suggestions"
let USERS_LIST = "users/list"

func EDIT_GROUP(groupId : Int) -> String{
    return "group/update/\(groupId)"
}

func GET_GROUP_PROFILE_DETAILS(_ type : String,id:Int) -> String{
    return "show/\(type)/\(id)"
}

func GET_GROUP_LIST(_ groupType : String) -> String{
    if groupType == ""{
        return "group/list"
    }else{
        return "group/list/\(groupType)"
    }
    
}

func GET_OTHER_CONNECTIONS(_ type : String , id : Int) -> String{
    return "other/\(type)/\(id)"
}

func FRIEND_REQUEST(_ friendId : Int) -> String{
    return "friend/request/\(friendId)"
}

func REMOVE_FRIEND_REQUEST(_ friendId : Int) -> String{
    return "friend/remove/\(friendId)"
}

//STATIC PAGE Api
let STATIC_PAGES = "urls"

//New Post Api
let SHARE_WITH = "sharewith"
let LIST_OF_INDIVIDUALS = "individual"
let TRANSACTION_HISTORY = "payments"

func GET_COUNT_API(_ connectionType: String? = nil) -> String{
    guard let connectionType = connectionType else {return "profile/counts/1"}
    return "profile/counts/\(connectionType)"
}

//REPORT ABUSE APIS

let REPORT_ABUSE_API = "report"
let BLOCK_USER = "block"
let BLOCKED_USERS_LIST = "block/list"


//PRODUCT APIS

let GET_CATEGORIES = "product/keyword"
let PRODUCT_LOCATIONS = "product/location"
let SERVICE_LOCATIONS = "service/location"
let CONTACT_SUPPLIER = "contact/supplier"

func GET_PRODUCT_DETAILS(productId : Int) -> String{
    return "product/\(productId)"
}

func GET_FULL_PROFILE(supplierID : Int) -> String{
    return "supplier/\(supplierID)"
}

let GET_SOFTWARE_LINKS = "software/serial"

func GET_SERVICE_DETAILS(serviceID : Int) -> String{
    return "service/view/\(serviceID)"
}

let CONTACT_SERVICE = "contact/service"

//SERVICE APIS

func LIST_OF_FREE_PAID_SERVICES(serviceType:String) -> String{
    return "fpservices/list/\(serviceType)"
}

let CHECKOUT = "fpservices/checkout"
let GET_PRICES = "fpservices/price"
let GET_LIST_OF_COMPANIES = "ad/companies"
let UPLOAD_COMPANY_PROFILE = "fpservices/company"
let POST_SPONSOR_CONTENT = "fpservices/sponsor"
let POST_PRODUCT = "fpservices/product"
let POST_SERVICE = "fpservices/service"
let UPLOAD_NEW_SOFTWARE = "fpservices/software"
let POST_NEW_JOB = "fpservices/job"
let POST_NEW_AD = "fpservices/ads"
let APPLY_PROMO_CODE = "fpservices/promocode"
let PAYMENT_UPDATE = "fpservices/payment"

func DELETE_SERVICE_MEDIA(serviceId:Int,serviceFolder:String,serviceType:String,serviceName:String) -> String{
    return "fpservices/file/\(serviceId)/\(serviceFolder)/\(serviceType)/\(serviceName)"
}

func UPDATE_PRODUCT(productId : Int) -> String{
    return "fpservices/product/\(productId)"
}
func UPDATE_SERVICE(serviceID : Int) -> String{
    return "fpservices/service/\(serviceID)"
}
func UPDATE_SPONSOR_CONTENT(sponsorID : Int) -> String{
    return "fpservices/sponsor/\(sponsorID)"
}
func UPDATE_SOFTWARE(softwareId : Int) -> String{
    return "fpservices/software/\(softwareId)"
}

func UPDATE_JOB(jobId : Int) -> String{
    return "fpservices/job/\(jobId)"
}

func UPDATE_AD(adID : Int) -> String{
    return "fpservices/ads/\(adID)"
}


// MESSAGE APIS

let GET_MESSAGE_LIST = "message/chat/list"
let GET_LIST_OF_CHAT_USERS = "message/friends/search"
let SEND_MESSAGE = "message/send"
let ADD_GROUP_MEMBER = "group/member/add"
let FORWARD_MESSAGE = "forward/group/message"

//NOTIFICATION APIS
let GET_NOTIFICATIONS = "notification/list"
let ACCEPT_REJECT_FRIEND_REQUEST = "friend/request"
let ACCEPT_REJECT_GROUP_REQUEST = "group/request"


func GET_USER_CHAT(_ userID : String) -> String{
    return "message/chat/\(userID)"
}
func LEAVE_GROUP_CHAT(_ groupID : String) -> String{
    return "leave/group/\(groupID)"
}
func MARK_CONVERSATION_UNREAD(_ type : String , idToBeUnread : String) -> String{
    return "message/\(type)/\(idToBeUnread)"
}

func DELETE_CONVERSATION(_ type : String , idToBeDeleted : String) -> String{
    return "message/\(type)/\(idToBeDeleted)"
}

//TODO: Forgot Password
func GET_FORGOT_USERNAME_OR_PASSWORD_URL(_ pathParameter: String?)-> String
{
    guard let pathParam = pathParameter else { return  "" }
    return "forgot/\(pathParam)"
}

let CHANGE_PASSWORD = "password"

let UPDATE_DEVICE_URL = "/api/secure/user/device"
//let GET_PROFILE = "/api/secure/profile/get"
let CONTACT_US = "/api/secure/contactUs"


func CHANGE_PWD_URL(_ oldPassword : String,_ password: String) -> String {
    return "/api/secure/user/password/change?oldPassword=\(oldPassword)&password=\(password)"
}




func START_LOADING_VIEW()  {
    DispatchQueue.main.async {
         if ServiceManager.shared().internetConnectivity == true {
//            let loadingView = LoadingView()
//            loadingView.tag = LOADING_VIEW_TAG
//            KEY_WINDOW?.addSubview(loadingView)
//            loadingView.startAnimation()
            
            
            let activityIndicator = DGActivityIndicatorView.init(type: .ballClipRotate, tintColor: THEME_RED_COLOR)
            activityIndicator?.frame =  CGRect.init(x: SCREEN_WIDTH/2-25, y: SCREEN_HEIGHT/2-25, width: 50, height: 50)
            activityIndicator?.tag = LOADING_VIEW_TAG
            //activityIndicator?.backgroundColor = UIColor.blue
            KEY_WINDOW?.addSubview(activityIndicator!)
            activityIndicator?.startAnimating()
            UIApplication.shared.beginIgnoringInteractionEvents()

            
            
        }
       
    }
}

func STOP_LOADING_VIEW()  {
    DispatchQueue.main.async {
//        let loadingView = KEY_WINDOW?.viewWithTag(LOADING_VIEW_TAG) as? LoadingView
//        loadingView?.stopAnimation()
        let loadingView = KEY_WINDOW?.viewWithTag(LOADING_VIEW_TAG) as? DGActivityIndicatorView
        loadingView?.removeFromSuperview()
        loadingView?.stopAnimating()
        UIApplication.shared.endIgnoringInteractionEvents()

    }
}




