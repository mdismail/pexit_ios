//
//  APICalls.swift
//  Wallet
//
//  Created by Apple on 23/03/18.
//  Copyright © 2018 Orfeostory. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {

    //MARK: Login
    func apiLogin(completion:@escaping ((_ LoginViewModel : Any?)-> Void)) -> Void {
        let loginVC = self as! LoginViewController
        let params = ["username": loginVC.userNameTextfield.text!, "password" : loginVC.passwordTextfield.text!] as NSMutableDictionary
        START_LOADING_VIEW()
        ServiceManager.methodType(requestType: POST_REQUEST, url: Get_LOGIN_URL(), params: params, paramsData: nil, completion: { (_ response,_ responseData, statusCode) in
            if response != nil{
                STOP_LOADING_VIEW()
                if ((response as! [String : Any])["token"] as? String) != nil {
                    Defaults.setAccessToken((response as! [String : Any])["token"] as! String)
                    let decoder = JSONDecoder()
                    do{
                        let loginModel = try decoder.decode(LoginResponse.self, from: responseData!)
                        saveLoginData(response)
                        Defaults.setLoginStatus(loginModel.status ?? false)
                        completion (loginModel)
                    } catch{
                        print("Catch")
                    }
                   
                }
                else if let code = ((response as! [String : Any])["code"] as? Int), code == 4401
                {
                    self.displayToast("Not a valid user credentials")
                }
                else if let message = ((response as! [String : Any])["message"] as? String){
                    self.displayToast(message)
                }
                else
                {
                    self.displayToast(UNKNOWN_ERROR_MSG)
                }
            }
            else {
                self.displayToast(UNKNOWN_ERROR_MSG)
            }
            
        }, failure: { (failure, code) in
            STOP_LOADING_VIEW()
        })
        
    }
    
    func apiSignUp(completion:@escaping ((_ response : Any?)-> Void)) -> Void {
         let registrationViewController = self as! RegistrationViewController
         let keys = ["name","username","password","email","country","phone","company"]
        var params = [String:Any]()
        for (index, element) in registrationViewController.displayedItems.enumerated() {
            print("Item \(index): \(element)")
            params[keys[index]] = element.textfieldText
        }
       
        START_LOADING_VIEW()
        ServiceManager.methodType(requestType: POST_REQUEST, url: REGISTER_USER, params: params as NSDictionary, paramsData: nil, completion: { (_ response, _ responseData,_ statusCode) in
            STOP_LOADING_VIEW()
            if response != nil {
                if ((response as! [String : Any])["status"] as? Int) == 1{
                    self.displayToast(((response as! [String : Any])["message"] as! String))
                    completion (response)
                    self.backAction()
                }
                else if let message = ((response as! [String : Any])["message"] as? String) {
                    self.displayToast(message)
                }
            }
        }, failure: { (failure, code) in
            STOP_LOADING_VIEW()
        })
    }
    
    func apiSocialLoginIn(_ params: [String:String],socialLoginType:String!, profileImageData:Data? = nil,completion:@escaping ((_ response : Any?)-> Void)) -> Void {
        START_LOADING_VIEW()
        print("Params are --->",params)
        AlamofireManager.shared().requestWith(apiUrl: Get_LOGIN_URL(socialLoginType), userProfileImage:profileImageData,parameters:params) { (response, error) in
            STOP_LOADING_VIEW()
            print(response)
            if response != nil{
                if ((response as! [String : Any])["token"] as? String) != nil{
                    saveLoginData(response!)
                    Defaults.setLoginStatus(true)
                    Defaults.setAccessToken((response as! [String : Any])["token"] as! String)
                    completion(response)
                    
                    
                }else if let message = ((response as! [String : Any])["message"] as? String) {
                    self.displayToast(message)
                    if(socialLoginType == "facebook"){
                        completion(response)
                    }
                }
            }else{
                self.displayToast("Server could not be reached")
            }
            
            //                    self.pushToTabBarController()
        }
        
        
        
        
//        ServiceManager.methodType(requestType: POST_REQUEST, url: Get_LOGIN_URL(socialLoginType), params: params as NSDictionary, paramsData: nil, completion: { (_ response,_ responseData, _ statusCode) in
//            STOP_LOADING_VIEW()
//            if response != nil {
//                if ((response as! [String : Any])["token"] as? String) != nil{
//                    saveLoginData(response!)
//                    Defaults.setLoginStatus(true)
//                    Defaults.setAccessToken((response as! [String : Any])["token"] as! String)
//                    completion (response)
////                    setUserType(3)
////                    self.moveToVCAccordingToLoginObject()
////                    self.updateUserDeviceToken()
//                }
////                else if ((response as! [String : Any])["code"] as? Int) == INVALID_USER {
////                    completion (response)
////                }
//                else if let message = ((response as! [String : Any])["message"] as? String) {
//
//                    self.displayToast(message)
//                    if(socialLoginType == "facebook"){
//                        completion(response)
//                    }
//                }
//            }
//        }, failure: { (_failure, _ statusCode) in
//            STOP_LOADING_VIEW()
//        })
    }
    
//    func apiGoogle(_ params: [String:String?], completion:@escaping ((_ response : Any?)-> Void)) -> Void {
//        START_LOADING_VIEW()
//
//        ServiceManager.methodType(requestType: POST_REQUEST, url: Get_LOGIN_URL("google"), params: params as NSDictionary  , paramsData: nil, completion: { [weak self](_ response, _ responseData ,_  statusCode) in
//            STOP_LOADING_VIEW()
//            if response != nil {
//                if ((response as! [String : Any])["token"] as? String) != nil {
//                    completion (response)
//                }
//                else if ((response as! [String : Any])["code"] as? Int) == INVALID_USER {
//                    completion (response)
//                }
//                else if let message = ((response as! [String : Any])["message"] as? String) {
//                    self?.displayToast(message)
//                }
//            }
//        }, failure: { (_ failure, _ statusCode) in
//            STOP_LOADING_VIEW()
//        })
//    }
//
//    func apiLinkedIn(_ params: [String:String?], completion:@escaping ((_ response : Any?)-> Void)) -> Void {
//        START_LOADING_VIEW()
//        ServiceManager.methodType(requestType: POST_REQUEST, url: Get_LOGIN_URL("linkedin"), params: params as NSDictionary  , paramsData: nil, completion: { [weak self](_ response, _ responseData ,_  statusCode) in
//            STOP_LOADING_VIEW()
//            if response != nil {
//                if ((response as! [String : Any])["token"] as? String) != nil {
//                    completion (response)
//                }
//                else if ((response as! [String : Any])["code"] as? Int) == INVALID_USER {
//                    completion (response)
//                }
//                else if let message = ((response as! [String : Any])["message"] as? String) {
//                    self?.displayToast(message)
//                }
//            }
//            }, failure: { (_ failure, _ statusCode) in
//                STOP_LOADING_VIEW()
//        })
//    }
    
    func changePassword(oldPassword : String, newPassword : String) {
        START_LOADING_VIEW()
        ServiceManager.methodType(requestType: PUT_REQUEST, url: CHANGE_PWD_URL(oldPassword,newPassword), params: nil, paramsData: nil, completion: { (_ response,_ responseData, _ statusCode) in
            STOP_LOADING_VIEW()
            if response != nil {
                if ((response as! [String : Any])["authority"] as? String) != nil{
                    saveLoginData(response!)
                    self.displayToast("Password has been changed succesfully")
                    self.backAction()
                }
                else if let message = ((response as! [String : Any])["message"] as? String) {
                    self.displayToast(message)
                }
            }
        }, failure: { (_ failure, _ statusCode) in
            STOP_LOADING_VIEW()
        })
    }
    
    
    func inviteFriends(_ params: [String:String?]) -> Void {
        START_LOADING_VIEW()
        ServiceManager.methodType(requestType: POST_REQUEST, url: INVITE_FRIENDS, params: params as NSDictionary, paramsData: nil, completion: { (_ response,_ responseData, _ statusCode) in
            STOP_LOADING_VIEW()
            if response != nil {
                if ((response as! [String : Any])["status"] as? Bool) == true{
                   
                    self.displayToast(((response as! [String : Any])["message"] as? String)!)
                    self.backAction()
                }
                else if let message = ((response as! [String : Any])["message"] as? String) {
                    self.displayToast(message)
                }
            }
        }, failure: { (_ failure, _ statusCode) in
            STOP_LOADING_VIEW()
        })
    }
    
    
    func updateUserDeviceToken() {
        
        var params = [String : Any]()
        params["platformId"] = 2 // For iOS 2 that is fixed
        params["imei"] = Defaults.getUDID()
        params["pushRegId"] = Defaults.getPushRegistrationId()
        
        ServiceManager.methodType(requestType: POST_REQUEST, url: UPDATE_DEVICE_URL, params: params as NSDictionary, paramsData: nil, completion: { (_ response,_ responseData, _ status) in}) { (_ response, _ code) in
            let code = (response as! [String: Any])["code"] as? Int
            if code != nil, code == SUCCESS_CODE {
                print("Success while registering device for notifications")
            }else
            {
                print("Failure while registering device")
            }
        }
    }
    func logoutRequest() {
        START_LOADING_VIEW()
        ServiceManager.methodType(requestType: GET_REQUEST, url: LOGOUT_URL(Defaults.getUDID()), params: nil, paramsData: nil, completion: { (_ response,_ responseData, _ code) in
            STOP_LOADING_VIEW()
            let code = (response as! [String: Any])["code"] as? Int
            if code != nil, code == SUCCESS_CODE {
                Defaults.setLoginStatus(false)
                saveLoginData(nil)
                self.moveToLoginScreen()
            }
        }) { (_ response , _ code) in
            STOP_LOADING_VIEW()
            
        }
    }
    
    
    func forgotPasswordOrUsername(params: NSDictionary?, completion:@escaping((_ responseObj : Any?) -> Void )) -> Void {
        var urlString: String?
        guard let paramsValue = params?.value(forKey: "taskExecutionName") as? String else { return }
        if paramsValue == "Forgot username"{
            urlString = GET_FORGOT_USERNAME_OR_PASSWORD_URL("username")
        } else if paramsValue == "Forgot password"{
            urlString = GET_FORGOT_USERNAME_OR_PASSWORD_URL("password")
        }
        let mutableParams = params?.mutableCopy() as! NSMutableDictionary
        mutableParams.removeObject(forKey: "taskExecutionName")
        START_LOADING_VIEW()
        ServiceManager.methodType(requestType: POST_REQUEST, url: urlString!, params: mutableParams, paramsData: nil, completion: { (response,_ responseData, statusCode) in
            STOP_LOADING_VIEW()
            completion(response)
        }) { (response, statusCode) in
         STOP_LOADING_VIEW()
        }
    }

    func getCountries(params: NSDictionary?, completion:@escaping((_ responseObj : Any?) -> Void )) -> Void {
        
        ServiceManager.methodType(requestType: GET_REQUEST, url: GET_COUNTRIES, params: nil, paramsData: nil, completion: { (response,_ responseData, statusCode) in
            completion(response)
        }) { (response, statusCode) in
            
        }
    }
    
//    func supplierTypes(params: NSDictionary?, completion:@escaping((_ responseObj : Any?) -> Void )) -> Void {
//        
//        ServiceManager.methodType(requestType: GET_REQUEST, url: SUPPLIER_TYPES_URL, params: nil, completion: { (response,_ responseData, statusCode) in
//            completion(response)
//        }) { (response, statusCode) in
//            
//        }
//    }
//
//    func customerTypes(params: NSDictionary?, completion:@escaping((_ responseObj : Any?) -> Void )) -> Void {
//        
//        ServiceManager.methodType(requestType: GET_REQUEST, url: CUSTOMER_TYPES_URL, params: nil, completion: { (response,_ responseData, statusCode) in
//            completion(response)
//        }) { (response, statusCode) in
//            
//        }
//    }
    
    
    
  
    

}

