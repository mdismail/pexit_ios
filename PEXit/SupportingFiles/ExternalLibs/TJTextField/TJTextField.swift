//
//  TJTextField.swift
//  UnderLineTextField
//
//  Created by Tejas Ardeshna on 31/12/15.
//  Copyright © 2015 Tejas Ardeshna. All rights reserved.
//

import UIKit


@IBDesignable class TJTextField: UITextField {
//@IBDesignable class TJTextField: IQDropDownTextField {

    fileprivate var ImgIcon: UIImageView?
    
    @IBInspectable var errorEntry: Bool = false {
        didSet {
            self.setNeedsDisplay()
        }
    }
    @IBInspectable var leftTextPedding: Int = 15 {   //0
        didSet {
            self.setNeedsDisplay()
        }
    }
    @IBInspectable var lineColor: UIColor = UIColor.black {
        didSet {
            self.setNeedsDisplay()
        }
    }
    @IBInspectable var placeHolerColor: UIColor = UIColor(red: 199.0/255.0, green: 199.0/255.0, blue: 205.0/255.0, alpha: 1.0) {
        didSet {
            self.setNeedsDisplay()
        }
    }
    @IBInspectable var errorColor: UIColor = UIColor.red {
        didSet {
            self.setNeedsDisplay()
        }
    }
    
    @IBInspectable var imageWidth: Int = 20 {   //0
        didSet {
            self.updateView()
        }
    }
    @IBInspectable var leftImage : UIImage? {
        didSet {
            self.updateView()
        }
    }
    
//    @IBInspectable var leftTxtView : UIView? {
//        didSet {
//            self.updateView()
//        }
//    }
    
    @IBInspectable var rightImage: UIImage? {
        didSet {
            updateRightView()
        }
    }
    
    @IBInspectable var rightPadding: CGFloat = 0{
        didSet {
            self.setNeedsDisplay()
        }
    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return self.newBounds(bounds)
    }
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return self.newBounds(bounds)
    }
    
    fileprivate func newBounds(_ bounds: CGRect) -> CGRect {

//        var newBounds = bounds
//        newBounds.origin.x += CGFloat(leftTextPedding) + CGFloat(imageWidth)
//        return newBounds   //Not working
        
        switch (leftImage,rightImage) {
        case (nil,nil):
            return bounds
            
        case (leftImage,nil):
            return CGRect(x: bounds.origin.x + CGFloat(leftTextPedding) + CGFloat(imageWidth), y: bounds.origin.y, width: bounds.width - CGFloat(leftTextPedding) - CGFloat(imageWidth), height: bounds.height)

        case (nil,rightImage):
            return CGRect(x: bounds.origin.x , y: bounds.origin.y, width: bounds.width - CGFloat(rightPadding) - CGFloat(imageWidth), height: bounds.height)

        case (leftImage,rightImage):
            return bounds.insetBy(dx: CGFloat(leftTextPedding) + CGFloat(imageWidth), dy: CGFloat(rightPadding) + CGFloat(imageWidth))
            
        default:
            return bounds
        }
        }
    
    var errorMessage: String?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //setting left image
       /* if (leftImage != nil)
        {
            let imgView = UIImageView(image: leftImage)
            imgView.frame = CGRect(x: 0, y: 0, width: CGFloat(imageWidth), height: self.frame.height)
            imgView.contentMode = .center
            self.leftViewMode = UITextFieldViewMode.always
            self.leftView = imgView
        }   */
    }
    
    func updateView() {
        if let image = leftImage {
            leftViewMode = UITextFieldViewMode.always
            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
            imageView.contentMode = .scaleAspectFit
            imageView.image = image
            // Note: In order for your image to use the tint color, you have to select the image in the Assets.xcassets and change the "Render As" property to "Template Image".
//            imageView.tintColor = color
            leftView = imageView
        }
//        else if let btn = leftTxtView{
//            leftViewMode = UITextFieldViewMode.always
//            let leftBtn = UIView(frame: CGRect(x: 20, y: 0, width: 50, height: 20))
//            leftBtn.addSubview(btn)
//           // imageView.contentMode = .scaleAspectFit
////            imageView.image = image
//            // Note: In order for your image to use the tint color, you have to select the image in the Assets.xcassets and change the "Render As" property to "Template Image".
//            //            imageView.tintColor = color
//            leftView = leftBtn
//        }
        else {
            leftViewMode = UITextFieldViewMode.never
            leftView = nil
        }
        
        // Placeholder text color
        //        attributedPlaceholder = NSAttributedString(string: placeholder != nil ?  placeholder! : "", attributes:[NSAttributedStringKey.foregroundColor: color])
    }
    
    
    func updateRightView() {
        if let image = rightImage {
            rightViewMode = UITextFieldViewMode.always
            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
            imageView.contentMode = .scaleAspectFit
            imageView.image = image
            // Note: In order for your image to use the tint color, you have to select the image in the Assets.xcassets and change the "Render As" property to "Template Image".
//            imageView.tintColor = colorRight
            rightView = imageView
        } else {
            rightViewMode = UITextFieldViewMode.never
            rightView = nil
        }
        
        // Placeholder text color
        //        attributedPlaceholder = NSAttributedString(string: placeholder != nil ?  placeholder! : "", attributes:[NSAttributedStringKey.foregroundColor: color])
    }
    
    override func draw(_ rect: CGRect)
    {
        setUpKeyBoardTypeAndSecureTextEntry()
        let height = self.bounds.height
        
        // get the current drawing context
        let context = UIGraphicsGetCurrentContext()
        
        // set the line color and width
        if errorEntry {
            context?.setStrokeColor(errorColor.cgColor)
            context?.setLineWidth(1.5)
        } else {
            context?.setStrokeColor(lineColor.cgColor)
            context?.setLineWidth(0.5)
        }
        
        // start a new Path
        context?.beginPath()
        
        context!.move(to: CGPoint(x: self.bounds.origin.x, y: height - 0.5))
        context!.addLine(to: CGPoint(x: self.bounds.size.width, y: height - 0.5))
        // close and stroke (draw) it
        context?.closePath()
        context?.strokePath()
        
        //Setting custom placeholder color
        if let strPlaceHolder: String = self.placeholder
        {
            self.attributedPlaceholder = NSAttributedString(string:strPlaceHolder,
                                                            attributes:[NSAttributedStringKey.foregroundColor:placeHolerColor])
        }
    }
    
    func setUpKeyBoardTypeAndSecureTextEntry() {
        switch placeholder {
        case "Username","Email":
            keyboardType = .emailAddress
            self.isSecureTextEntry = false
        case "Phone Number":
            keyboardType = .numberPad
            self.isSecureTextEntry = false
        case "Password":
            keyboardType = .asciiCapable
            self.isSecureTextEntry = true
        default:
            keyboardType = .asciiCapable
            self.isSecureTextEntry = false
        }
    }
    override func leftViewRect(forBounds bounds: CGRect) -> CGRect
    {
        return CGRect(x: 0, y: 0, width: CGFloat(imageWidth), height: self.frame.height)
    }
    
    override func rightViewRect(forBounds bounds: CGRect) -> CGRect {
        var textRect = super.rightViewRect(forBounds: bounds)
        textRect.origin.x += rightPadding
        return textRect
    }
    
 

}

