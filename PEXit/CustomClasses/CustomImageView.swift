//
//  CustomImageView.swift
//  PEXit
//
//  Created by Apple on 30/07/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit


extension UIImageView {
    
    func setImage(from url: URL, placeholder: UIImage? = nil) {
        image = placeholder               // use placeholder (or if `nil`, remove any old image, before initiating asynchronous retrieval
        ImageCache.shared.imageFor(url: url) { (result, error) in
            if error == nil{
                DispatchQueue.main.async {
                    self.image = result
                }
                
            }
            
        }
    }
    
    func makeImageRounded(){
        self.layer.cornerRadius = self.frame.size.width / 2
        self.layer.borderWidth = 1.0
        self.layer.borderColor = UIColor.black.cgColor
        self.clipsToBounds = true
    }
}

extension UIView{
    
    func makeViewRounded(){
        self.layer.cornerRadius = self.frame.size.width / 2
        self.layer.borderWidth = 1.0
        self.layer.borderColor = UIColor.black.cgColor
        self.clipsToBounds = true
    }
}

class ImageCache {
    
    static let shared = ImageCache()
    
    private let cache = NSCache<NSString, UIImage>()
    var task = URLSessionDataTask()
    var session = URLSession.shared
    
    func imageFor(url: URL, completionHandler: @escaping (_ image: UIImage?, _ error: Error?) -> Void) {
        if let imageInCache = self.cache.object(forKey: url.absoluteString as NSString)  {
            completionHandler(imageInCache, nil)
            return
        }
        
        self.task = self.session.dataTask(with: url) { data, response, error in
            
            if let error = error {
                completionHandler(nil, error)
                return
            }
            
            let image = UIImage(data: data!)
            if let image = image{
                self.cache.setObject(image, forKey: url.absoluteString as NSString)
                completionHandler(image, nil)
            }
            
        }
        
        self.task.resume()
    }
}
